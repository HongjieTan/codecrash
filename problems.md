# [Dynamic Programming](DynamicProgramming/)
2 conditions for dp: "optimal substructure", "overlapping of subproblems".  
most cases:  
  - solution based on sub-solution   
  - contains recomputation using recursion.  

* 53\. [Maximum Subarray](https://leetcode.com/problems/maximum-subarray/description) -f
* 62\. [Unique Paths](https://leetcode.com/problems/unique-paths/description) -f
* 63\. [Unique Paths II](https://leetcode.com/problems/unique-paths-ii/description) -f
* 96\. Unique Binary Search Trees -f
* 95\. Unique Binary Search Trees II -f
* 64\. [Minimum Path Sum](https://leetcode.com/problems/minimum-path-sum/description)  -f
* 70\. [Climbing Stairs](https://leetcode.com/problems/climbing-stairs/description) -f
* 121\. [Best Time to Buy and Sell Stock](https://leetcode.com/problems/best-time-to-buy-and-sell-stock/description) -f
* 123\. [Best Time to Buy and Sell Stock III](https://leetcode.com/problems/best-time-to-buy-and-sell-stock-iii/description)  -f／dp+tree
* 198\. [House Robber](https://leetcode.com/problems/house-robber/description) -f
* 213\. [House Robber II](https://leetcode.com/problems/house-robber-ii/description) -f
* 337\. House Robber III -f
* 32\. [Longest Valid Parentheses](https://leetcode.com/problems/longest-valid-parentheses/description)  -f/stack or dp／h

* 221\. [Maximal Square](https://leetcode.com/problems/maximal-square/description) -f!/
* 322\. [Coin Change](https://leetcode.com/problems/coin-change/description/) -f!!/dp(best)/bfs_naive(LTE),bfs_with_cache(good!)/(此题直接dp较合适，用bfs有点绕)
* 264\. [Ugly Number II](https://leetcode.com/problems/ugly-number-ii/description) -f!
* 486\. Predict the Winner  -f!!!
* 877\. Stone Game -f!!

##### max problem:
* 152\. [Maximum Product Subarray](https://leetcode.com/problems/maximum-product-subarray/description)  -f!!!


##### least steps:
* 72\. [Edit Distance](https://leetcode.com/problems/edit-distance/description)  -f!!

##### string+dp:
* xxx\. Longest Common Subsequence(LCS). -f!!
* xxx\. Longest Common Substring -f!!
* xxx\. Longest Palindromic Subsequence. 
* xxx\. [Longest Palindromic Substring](https://leetcode.com/problems/longest-palindromic-substring/)  -f!!!/dp
* 139\. [Word Break](https://leetcode.com/problems/word-break/description) -f!!!!!!!!!!/dp!!!!
* 300\. Longest Increasing Subsequence.  -f!!!!!!/very good&hard problem!!!/
* 115\. [Distinct Subsequences](https://leetcode.com/problems/distinct-subsequences/description) -f!!

##### reg exp matching
* 44\. [Wildcard Matching](https://leetcode.com/problems/wildcard-matching/description) -f!
* 10\. [Regular Expression Matching](https://leetcode.com/problems/regular-expression-matching/description)  -f!!

==
* 87\. [Scramble String](https://leetcode.com/problems/scramble-string/description)
* 91\. [Decode Ways](https://leetcode.com/problems/decode-ways/description)
* 97\. [Interleaving String](https://leetcode.com/problems/interleaving-string/description)
* 120\. [dp.Triangle](https://leetcode.com/problems/triangle/description)
* 132\. [Palindrome Partitioning II](https://leetcode.com/problems/palindrome-partitioning-ii/description)
* 174\. [Dungeon Game](https://leetcode.com/problems/dungeon-game/description)
* 188\. [Best Time to Buy and Sell Stock IV](https://leetcode.com/problems/best-time-to-buy-and-sell-stock-iv/description)
* 279\. [Perfect Squares](https://leetcode.com/problems/perfect-squares/description)
* 309\. [Best Time to Buy and Sell Stock with Cooldown](https://leetcode.com/problems/best-time-to-buy-and-sell-stock-with-cooldown/description)
* 357\. [Count Numbers with Unique Digits](https://leetcode.com/problems/count-numbers-with-unique-digits/description)
* 368\. [Largest Divisible Subset](https://leetcode.com/problems/largest-divisible-subset/description)

# [Array](Array/)

##### interval processing!!! / 'flag' array or treemap /...
* xxx\.[Array Manipulation](https://www.hackerrank.com/challenges/crush/problem) -f!
* 370\. [Range Addition](https://www.lintcode.com/problem/range-addition/description/) -f!
* 252\. [meeting rooms](https://www.lintcode.com/en/problem/meeting-rooms/) -f!!/
* 253\. [meeting rooms ii](https://www.lintcode.com/problem/meeting-rooms-ii/description) -f!!!!!/good!/
* 56\. [Merge Intervals](https://leetcode.com/problems/merge-intervals/description) -f!!!!!!!!!!!!/ greedy_and_tricks! prove it!/...
* 57\. [Insert Interval](https://leetcode.com/problems/insert-interval/description)

===
* 26\. [Remove Duplicates from Sorted Array](https://leetcode.com/problems/remove-duplicates-from-sorted-array/description) -p
* 27\. [Remove Element](https://leetcode.com/problems/remove-element/description) -s
* 31\. [Next Permutation](https://leetcode.com/problems/next-permutation/description) -f!!!/ag!!
* 169\. [Majority Element](https://leetcode.com/problems/majority-element/description) - f/vote version ok/bit op version?/
* 229\. Majority Element II -f/vote algorithm/

* 645\. Set Mismatch  -f!!/InPlaceHash using sign(-1)/XOR?/...

===


===

* 54\. [Spiral Matrix](https://leetcode.com/problems/spiral-matrix/description)

* 59\. [Spiral Matrix II](https://leetcode.com/problems/spiral-matrix-ii/description)
* 73\. [Set Matrix Zeroes](https://leetcode.com/problems/set-matrix-zeroes/description)
* 118\. [Pascal's dp.Triangle](https://leetcode.com/problems/pascals-triangle/description)
* 119\. [Pascal's dp.Triangle II](https://leetcode.com/problems/pascals-triangle-ii/description)
* 189\. [Rotate Array](https://leetcode.com/problems/rotate-array/description)
* 228\. [Summary Ranges](https://leetcode.com/problems/summary-ranges/description)
* 283\. [Move Zeroes](https://leetcode.com/problems/move-zeroes/description)
* 289\. [Game of Life](https://leetcode.com/problems/game-of-life/description)
* 670\. [Maximum Swap](https://leetcode.com/problems/maximum-swap/description)

# [String](String/)

* 8\. [String to Integer](https://leetcode.com/problems/string-to-integer-atoi/description) -f/ag/
* 151\. [Reverse Words in a String](https://leetcode.com/problems/reverse-words-in-a-string/description) -p/reverse twice...
* 68\. [Text Justification](https://leetcode.com/problems/text-justification/description) -f/ag/
* 819\.[most common word](https://leetcode.com/problems/most-common-word/description/) -f!!!/重点在于用regex split字符串！/ ex. words = paragraph.split("[!?',;. ]+")/...


== 
* 6\. [ZigZag Conversion](https://leetcode.com/problems/zigzag-conversion/description)
* 14\. [Longest Common Prefix](https://leetcode.com/problems/longest-common-prefix/description)
* 17\. [Letter Combinations of a Phone Number](https://leetcode.com/problems/letter-combinations-of-a-phone-number/description)
* 28\. [Implement strStr()](https://leetcode.com/problems/implement-strstr/description)
* 38\. [Count and Say](https://leetcode.com/problems/count-and-say/description)
* 58\. [Length of Last Word](https://leetcode.com/problems/length-of-last-word/description)
* 67\. [Add Binary](https://leetcode.com/problems/add-binary/description)

* 165\. [Compare Version Numbers](https://leetcode.com/problems/compare-version-numbers/description)
* 344\. [Reverse String](https://leetcode.com/problems/reverse-string/description)

# [Linked List](linkedlist_queue/)

* 138\. [Copy List with Random Pointer](https://leetcode.com/problems/copy-list-with-random-pointer/description) -f!!
* 599\. [Insert into a Cyclic Sorted List](https://www.lintcode.com/problem/insert-into-a-cyclic-sorted-list/) -f!!

#### linked list reverse 
* 206\. [Reverse Linked List](https://leetcode.com/problems/reverse-linked-list/description) -f
* 92\. [Reverse Linked List II](https://leetcode.com/problems/reverse-linked-list-ii/description) -f

==
* 21\. [Merge Two Sorted Lists](https://leetcode.com/problems/merge-two-sorted-lists/description) -f

* 2\. [Add Two Numbers](https://leetcode.com/problems/add-two-numbers/description)
* 24\. [Swap Nodes in Pairs](https://leetcode.com/problems/swap-nodes-in-pairs/description)
* 25\. [Reverse Nodes in k-Group](https://leetcode.com/problems/reverse-nodes-in-k-group/description)
* 61\. [Rotate List](https://leetcode.com/problems/rotate-list/description)
* 82\. [Remove Duplicates from Sorted List II](https://leetcode.com/problems/remove-duplicates-from-sorted-list-ii/description)
* 83\. [Remove Duplicates from Sorted List](https://leetcode.com/problems/remove-duplicates-from-sorted-list/description)
* 86\. [Partition List](https://leetcode.com/problems/partition-list/description)

* 141\. [Linked List Cycle](https://leetcode.com/problems/linked-list-cycle/description)
* 143\. [Reorder List](https://leetcode.com/problems/reorder-list/description)
* 147\. [Insertion Sort List](https://leetcode.com/problems/insertion-sort-list/description)
* 148\. [Sort List](https://leetcode.com/problems/sort-list/description)
* 160\. [Intersection of Two Linked Lists](https://leetcode.com/problems/intersection-of-two-linked-lists/description)
* 203\. [Remove Linked List Elements](https://leetcode.com/problems/remove-linked-list-elements/description)

* 234\. [Palindrome Linked List](https://leetcode.com/problems/palindrome-linked-list/description)
* 237\. [Delete Node in a Linked List](https://leetcode.com/problems/delete-node-in-a-linked-list/description)
* 328\. [Odd Even Linked List](https://leetcode.com/problems/odd-even-linked-list/description)

# [Hash Table]
most cases: find according to content


* xxx\. [rehashing](https://www.lintcode.com/problem/rehashing/description)

##### inplacehash
* 268\. Missing Number -f!!!/ XOR,Gauss,InplaceHash:map array index to content!/
* 41\. [First Missing Positive](https://leetcode.com/problems/first-missing-positive/description) -f!!/InplaceHash:map array index to content!/ag!/
* 347\. [Top K Frequent Elements](https://leetcode.com/problems/top-k-frequent-elements/description)  -f!!!/heap, InPlaceHash:map array index to freq!/ag!/   
* 692\. Top K Frequent Words -f/similar to 347/  
* 451\. Sort Characters By Frequency -f/similar to 347/   
* 347+692+451\: Because freq is in range 0,1,2,...,n !!!!!!! All frequency problem can use this way!!! Also called bucketsort**

##### counting!
* 750\. Number Of Corner Rectangles.  -f!!!!!!!/ms interview!!/


##### counting sort!!!
* 274\. [H-Index](https://leetcode.com/problems/h-index/description) -f/


==
* 1\. [Two Sum](https://leetcode.com/problems/two-sum/description) -p
* 18\. [4Sum](https://leetcode.com/problems/4sum/description)  -f
* 560\.  Subarray Sum Equals K  -f!!!/ag!!!/PrefixSum/
* 523\. Continuous Subarray Sum. -f!!!/ag!!!/PrefixSum/
* 36\. [Valid Sudoku](https://leetcode.com/problems/valid-sudoku/description) -p/ag
* 49\. [Group Anagrams](https://leetcode.com/problems/group-anagrams/description) -f/
* 149\. [Max Points on a Line](https://leetcode.com/problems/max-points-on-a-line/description) -f
* 187\. [Repeated DNA Sequences](https://leetcode.com/problems/repeated-dna-sequences/description) -f/better?/
* 336\. Palindrome Pairs  -p/
* 356\. [Line Reflection](https://www.lintcode.com/problem/line-reflection/description) -f/hashtable
* 94\. [Binary Tree Inorder Traversal](https://leetcode.com/problems/binary-tree-inorder-traversal/description)


* 653\. Two Sum IV - Input is a BST
* 205\. [Isomorphic Strings](https://leetcode.com/problems/isomorphic-strings/description)
* 217\. [Contains Duplicate](https://leetcode.com/problems/contains-duplicate/description)
* 219\. [Contains Duplicate II](https://leetcode.com/problems/contains-duplicate-ii/description)
* 242\. [Valid Anagram](https://leetcode.com/problems/valid-anagram/description)
* 290\. [Word Pattern](https://leetcode.com/problems/word-pattern/description)
* 299\. [Bulls and Cows](https://leetcode.com/problems/bulls-and-cows/description)
* 349\. [Intersection of Two Arrays](https://leetcode.com/problems/intersection-of-two-arrays/description)
* 350\. [Intersection of Two Arrays II](https://leetcode.com/problems/intersection-of-two-arrays/description)




# [Heap](Heap/)
* 23\. [Merge k Sorted Lists](https://leetcode.com/problems/merge-k-sorted-lists/description)  -f
* 295\. [Find Median from Data Stream](https://leetcode.com/problems/find-median-from-data-stream/description)  -f/2heaps.../
* 347\. [Top K Frequent Elements](https://leetcode.com/problems/top-k-frequent-elements/description) -f/heap,inplacehash.../
* 239\. Sliding Window Maximum.  -f!!!!/ monotonic queue!!! O(n)/ag!!


# [Stack](Stack/)

##### max area problems:
* 84\. [Largest Rectangle in Histogram](https://leetcode.com/problems/largest-rectangle-in-histogram/description) -f!!!!/divide&conquer:O(nlogn)/stack:O(n)!!/ag
* 221. Maximal Square -f/dp!!/
* 85\. [Maximal Rectangle](https://leetcode.com/problems/maximal-rectangle/description). -f/84+dp!!/hard...

##### evaluate expression
* 150\. [Evaluate Reverse Polish Notation](https://leetcode.com/problems/evaluate-reverse-polish-notation/description) -f
* 394\. Decode String   -f

==
* 20\. [Valid Parentheses](https://leetcode.com/problems/valid-parentheses/description)   -s
* 71\. [Simplify Path](https://leetcode.com/problems/simplify-path/description) -p
* 155\. [Min Stack](https://leetcode.com/problems/min-stack/description) -f
* 94\. [Binary Tree Inorder Traversal](https://leetcode.com/problems/binary-tree-inorder-traversal/description) -f
* 144\. [Binary Tree Preorder Traversal](https://leetcode.com/problems/binary-tree-preorder-traversal/description) -f
* 145\. [Binary Tree Postorder Traversal](https://leetcode.com/problems/binary-tree-postorder-traversal/description) -f
* 225\. [Implement Stack using Queues](https://leetcode.com/problems/implement-stack-using-queues/description) -p
* 232\. [Implement Queue using Stacks](https://leetcode.com/problems/implement-queue-using-stacks/description) -p/use 2 stacks/
* 341\. [Flatten Nested List Iterator](https://leetcode.com/problems/flatten-nested-list-iterator/description) -p

later:  
* 316\. [Remove Duplicate Letters](https://leetcode.com/problems/remove-duplicate-letters/description) -？
* 224\. [Basic Calculator](https://leetcode.com/problems/basic-calculator/description)


# [Two Pointers](TwoPointers/)

##### combine sum problems:  
* 167\. Two Sum II (two pointers problem) -f
* 15\. [3Sum](https://leetcode.com/problems/3sum/description) -f!/ag
* 18\. [4Sum](https://leetcode.com/problems/4sum/description) -f/ag
* 16\. [3Sum Closest](https://leetcode.com/problems/3sum-closest/description) -f!!!/ag/
* 918\. [3Sum Smaller](https://www.lintcode.com/problem/3sum-smaller/description). -f!!!/ag!!!   

##### substring problems: hash+twopointers
* 3\. [Longest Substring Without Repeating Characters](https://leetcode.com/problems/longest-substring-without-repeating-characters/description) -f!!!/slide window.../
* 76\. [Minimun Window Substring](https://leetcode.com/problems/minimum-window-substring/description/) -f!!!/
* xx\. [Longest Substring with at most K distinct charactors](https://www.lintcode.com/problem/longest-substring-with-at-most-k-distinct-characters/description) -f!!!

##### cycle detect:
* 141\. [Linked List Cycle](https://leetcode.com/problems/linked-list-cycle/description) -f
* 142\. [Linked List Cycle II](https://leetcode.com/problems/linked-list-cycle-ii/description) -f!!!
* 287\. [Find the Duplicate Number](https://leetcode.com/problems/find-the-duplicate-number/description)  -f!!


* 11\. [Container With Most Water](https://leetcode.com/problems/container-with-most-water/description) -f
(42+238!!!)
* 42\. [Trapping Rain Water](https://leetcode.com/problems/trapping-rain-water/description) -f!!!!!!/ag!!!!!/bruteforce->dp->2p
* 238\. Product of Array Except Self f!!!!/ag!!!!/bruteforce->dp->2p/



==

* 19\. [Remove Nth Node From End of List](https://leetcode.com/problems/remove-nth-node-from-end-of-list/description)

* 75\. [Sort Colors](https://leetcode.com/problems/sort-colors/description)
* 80\. [Remove Duplicates from Sorted Array II](https://leetcode.com/problems/remove-duplicates-from-sorted-array-ii/description) p
* 88\. [Merge Sorted Array](https://leetcode.com/problems/merge-sorted-array/description)
* 125\. [Valid Palindrome](https://leetcode.com/problems/valid-palindrome/description)

* 209\. [Minimum Size Subarray Sum](https://leetcode.com/problems/minimum-size-subarray-sum/description)

* 345\. [Reverse Vowels of a String](https://leetcode.com/problems/reverse-vowels-of-a-string/description)

# [Tree](Tree/)

##### traverse

dfs: preorder, inorder, postorder(recursive,stack,morris)   
bfs: levelorder(use queue)  
notice!! you must be able to use the iterative version!!!!!   

 
* 783\. [Minimum Distance Between BST Nodes](https://leetcode.com/problems/minimum-distance-between-bst-nodes/description/)   -f!!
* 94\. [Binary Tree Inorder Traversal](https://leetcode.com/problems/binary-tree-inorder-traversal/description) -f!!!/ iterative and morris version/
* 144\. Binary Tree Preorder Traversal -f
* 145\. Binary Tree Postorder Traversal -f/ag
* 98\. [Validate Binary Search Tree](https://leetcode.com/problems/validate-binary-search-tree/description) -f
* 96\. [Unique Binary Search Trees](https://leetcode.com/problems/unique-binary-search-trees/description) -f!/tree+dp/
* 95\. [Unique Binary Search Trees II](https://leetcode.com/problems/unique-binary-search-trees-ii/description) -f!!/tree+dp/
* 99\. [Recover Binary Search Tree](https://leetcode.com/problems/recover-binary-search-tree/description).  -f/
* 102\. [Binary Tree Level Order Traversal](https://leetcode.com/problems/binary-tree-level-order-traversal/description) -f/bfs, use queue/
* 100\. [Same Tree](https://leetcode.com/problems/same-tree/description) -p
* 104\. [Maximum Depth of Binary Tree](https://leetcode.com/problems/maximum-depth-of-binary-tree/description) -p
* 111\. [Minimum Depth of Binary Tree](https://leetcode.com/problems/minimum-depth-of-binary-tree/description) -p
* 112\. [Path Sum](https://leetcode.com/problems/path-sum/description) -p
* 113\. [Path Sum II](https://leetcode.com/problems/path-sum-ii/description) -p
* 437\. Path Sum III  -f!!!!/ag!/nb/ -p.   
* 226\. [Invert Binary Tree](https://leetcode.com/problems/invert-binary-tree/description).    -f!/ the iterative version!!!!/
* 543\. [Diameter of Binary Tree](https://leetcode.com/problems/diameter-of-binary-tree/description/) -f!!/ variant of tree height problem!!!
* 572\. Subtree of Another Tree -f!


#####  LCA(lowest common ancestor)
* 236\. [Lowest Common Ancestor of a Binary Tree](https://leetcode.com/problems/lowest-common-ancestor-of-a-binary-tree/description)  -f!!/ LCA of BT/ iterative version?
* 235\. [Lowest Common Ancestor of a Binary Search Tree](https://leetcode.com/problems/lowest-common-ancestor-of-a-binary-search-tree/description) -f/ LCA of BST/
* xxx\. [Distance Between Two Tree Nodes](https://www.geeksforgeeks.org/find-distance-between-two-nodes-of-a-binary-tree/)  -f!!!!/use LCA/


##### construction
* 297\. Serialize and Deserialize Binary Tree  -f!!!!!!/ag!!!!!!!
* 108\. [Convert Sorted Array to Binary Search Tree](https://leetcode.com/problems/convert-sorted-array-to-binary-search-tree/description) -f!/iter_version!!!/
* 109\. [Convert Sorted List to Binary Search Tree](https://leetcode.com/problems/convert-sorted-list-to-binary-search-tree/description) 


* 105\. [Construct Binary Tree from Preorder and Inorder Traversal] (https://leetcode.com/problems/construct-binary-tree-from-preorder-and-inorder-traversal/description)

* 106\. [Construct Binary Tree from Inorder and Postorder Traversal](https://leetcode.com/problems/construct-binary-tree-from-inorder-and-postorder-traversal/description)

##### range sum query!! special tree:  segment tree/ binary indexed tree!!!
* 303\. Range Sum Query - Immutable
* 307\. Range Sum Query - Mutable
* 304\. Range Sum Query 2D - Immutable
* 308\. Range Sum Query 2D - Mutable

Summary: consider 3 cases:
  1. number of query is large, number of update is few  -> query should be fast  -> pre sum: query O(1), update O(n)  
  2. number of query is few, number of update is large  -> update should be fast -> brute force: query O(n), update O(1)   
  3. number of query and update is distributed evenly   -> both should be fast   -> segment tree or binary indexed tree: query O(logn), update O(logn)   
       note: binary indexed tree is simpler to implement!!

==



==




* 110\. [Balanced Binary Tree](https://leetcode.com/problems/balanced-binary-tree/description)

* 114\. [Flatten Binary Tree to Linked List](https://leetcode.com/problems/flatten-binary-tree-to-linked-list/description)
* 116\. [Populating Next Right Pointers in Each Node](https://leetcode.com/problems/populating-next-right-pointers-in-each-node/description)
* 117\. [Populating Next Right Pointers in Each Node II](https://leetcode.com/problems/populating-next-right-pointers-in-each-node-ii/description)
* 124\. [Binary Tree Maximum Path Sum](https://leetcode.com/problems/binary-tree-maximum-path-sum/description)
* 144\. [Binary Tree Preorder Traversal](https://leetcode.com/problems/binary-tree-preorder-traversal/description)
* 145\. [Binary Tree Postorder Traversal](https://leetcode.com/problems/binary-tree-postorder-traversal/description)
* 173\. [Binary Search Tree Iterator](https://leetcode.com/problems/binary-search-tree-iterator/description)
* 222\. [Count Complete Tree Nodes](https://leetcode.com/problems/count-complete-tree-nodes/description)



* 257\. [Binary Tree Paths](https://leetcode.com/problems/binary-tree-paths/description)

# [Binary Search](BinarySearch/)

* 69\. [Sqrt(x)](https://leetcode.com/problems/sqrtx/description)  -f
* 50\. Pow(x, n) -f/ag/
* 4\. [Median of Two Sorted Arrays](https://leetcode.com/problems/median-of-two-sorted-arrays/description) -f!!!!!!/ag/
* 33\. [Search in Rotated Sorted Array](https://leetcode.com/problems/search-in-rotated-sorted-array/description)  -f/ag
* 81\. [Search in Rotated Sorted Array II](https://leetcode.com/problems/search-in-rotated-sorted-array-ii/description) -f/ag

* 34\. [Search for a Range](https://leetcode.com/problems/search-for-a-range/description)   -f/ag!
* 278\. [First Bad Version](https://leetcode.com/problems/first-bad-version/description) -f
* 74\. [Search a 2D Matrix](https://leetcode.com/problems/search-a-2d-matrix/description) -f
* 153\. [Find Minimum in Rotated Sorted Array](https://leetcode.com/problems/find-minimum-in-rotated-sorted-array/description) -p
* 154\. [Find Minimum in Rotated Sorted Array II](https://leetcode.com/problems/find-minimum-in-rotated-sorted-array-ii/description) -f/ag/
* 162\. [Find Peak Element](https://leetcode.com/problems/find-peak-element/description) -f
* 275\. [H-Index II](https://leetcode.com/problems/h-index-ii/description) -f/

* 230\. [Kth Smallest Element in a BST](https://leetcode.com/problems/kth-smallest-element-in-a-bst/description)

* 367\. [Valid Perfect Square](https://leetcode.com/problems/valid-perfect-square/description)

# [Depth-first Search](DepthFirstSearch/)

* 98\. [Validate Binary Search Tree](https://leetcode.com/problems/validate-binary-search-tree/description) -f/
* 200\. [Number of Islands](https://leetcode.com/problems/number-of-islands/description) -f

* 129\. [Sum Root to Leaf Numbers](https://leetcode.com/problems/sum-root-to-leaf-numbers/description)
* 301\. [Remove Invalid Parentheses](https://leetcode.com/problems/remove-invalid-parentheses/description)

# [Breadth-first Search](BreadthFirstSearch/)

most case 'distance/depth/height' problem

* 103\. [Binary Tree Zigzag Level OrderTraversal](https://leetcode.com/problems/binary-tree-zigzag-level-order-traversal/description) -f 
* 102\. [Binary Tree Level Order Traversal](https://leetcode.com/problems/binary-tree-level-order-traversal/description)   -f  
* 127\. [Word Ladder](https://leetcode.com/problems/word-ladder/description)   -f!/notice find neighors can be O(1)/
* 126\. [Word Ladder II](https://leetcode.com/problems/word-ladder-ii/description). -fffff!!!!!!!!/bfs!/store 'path information' in queue instead of word!!!!!!!!/



* 107\. [Binary Tree Level Order Traversal II](https://leetcode.com/problems/binary-tree-level-order-traversal-ii/description)

* 130\. [Surrounded Regions](https://leetcode.com/problems/surrounded-regions/description)
* 199\. [Binary Tree Right Side View](https://leetcode.com/problems/binary-tree-right-side-view/description)
* 310\. [Minimum Height Trees](https://leetcode.com/problems/minimum-height-trees/description)



# [Graph](Graph/)
##### BFS/DFS (Shortest distance -> BST) 
* xxx\. has route from source to target -f/simple bfs or dfs
* xxx\. Shortest distance from source to target/word_ladder/  -f/bfs!
* xxx\. Shortest paths from source to target/word_ladder_ii/  -f/bfs!!
* xxx\. All paths from source to target.  -f/dfs/dfs with cache!!!!!
* 542\. [01Matrix](https://leetcode.com/problems/01-matrix/solution/) -?/
* 733\. [Flood Fill](https://leetcode.com/problems/flood-fill/)  -p/
* 133\. [Clone Graph](https://leetcode.com/problems/clone-graph/description) -f/ag/
* 200\. [Number of Islands](https://leetcode.com/problems/number-of-islands/description) -f/
* 322\. [Coin Change](https://leetcode.com/problems/coin-change/description/) -f!!/dp(best)/bfs_naive(LTE),bfs_with_cache(good!)/(此题直接dp较合适，用bfs有点绕)
* 675\. [Cut Off Trees](https://leetcode.com/problems/cut-off-trees-for-golf-event/description/). -f!!!!!/good!/shortest distance -> BST!!!/
* 490\. The Maze  -f!/dfs or bfs/ the ball go alone one direction until it stops!!
* 505\. The Maze II  -f!!
* 499\. The Maze III  -?
* 753\. Cracking the Safe -f!! / find hamilton path or find euler path.../
* 286\. Walls and Gates  -f!!!/ multi sources shortest path problem -> use parallel bfs!!


##### All paths
* xxx\. target sum

##### least steps (bfs with level order)
* 815\. Bus Routes -f!!/ treat each bus as node...
* xxx\. Sliding Puzzle  -f!

##### DFS+Cache (idea like DP) / 各种找出所有路径的问题
* xxx\. All paths from source to targer.  -f/dfs/dfs with cache!!!!!
* 140\. Word Break II. -f/dfs with cache!!!!

##### directed graph, topological sort:
* 207\. [Course Schedule](https://leetcode.com/problems/course-schedule/description) -f!/
* 210\. [Course Schedule II](https://leetcode.com/problems/course-schedule-ii/description) -f!/
* 269\. Alien Dictionary -f!!

##### detect cycle in directed gragh
1. dfs: using a recursion stack (localVisited)  (the choice)
2. colors

##### detect cycle in undirected graph
1. dfs
2. union-find(the choice!!!!)
3. topological sort

##### MST(minimum spanning tree)
1. kruskal (the choice, the code is clearer) 
2. prim 

##### Union-Find
主要用于判环，
用于查询 399 ！！
用于聚类(721, xxx.Longest Consecutive SubSequence, 305)
* 261\. Graph Valid Tree   
* 399\. [Evaluate Equations](https://leetcode.com/problems/evaluate-division/description/) -f!!!!!!/dfs version: O(e*q)/ union-find (pass compression and union by rank): O(e+q)!!!/一般要用union-find的这种方法，因为查询的需求是比较多的，需要查询高效率！！！！
* 305\. [Number of Islands II](https://www.lintcode.com/problem/number-of-islands-ii/description) -f!!!!!/union-find with path compression, O(ops)!!
* 684\. Redundant Connection -f!!!
* 685\. Redundant Connection II -f!!!(google high frequency)
* 721\. Accounts Merge  -f!!
* xxx\. [Longest Consecutive SubSequence](https://www.geeksforgeeks.org/longest-consecutive-subsequence/)   -f



# [Backtracking](Backtracking/)

##### dfs+backtrack
* 79\.  [Word Search](https://leetcode.com/problems/word-search/description) -f!!!!/backtrack+dfs!! (backtrack the status of visited...)
* 797\. [All Paths From Source to Target](https://leetcode.com/problems/all-paths-from-source-to-target/description/).  -f/dfs+backtrack(LTE)/ dfs+cache(better!!!!)
* 140\. Work Break II. -f!/dfs+backtrack(LTE)/ dfs+cache(better!!!!)

== 
* 39\. [Combination Sum](https://leetcode.com/problems/combination-sum/description) -f/ag
* 39\. Combination Sum2 -f/ag/
* 216\. [Combination Sum III](https://leetcode.com/problems/combination-sum-iii/description) -f/ag/
* 46\. [Permutations](https://leetcode.com/problems/permutations/description)  -f/ag/
* 47\. [Permutations II](https://leetcode.com/problems/permutations-ii/description) -f/ag/
* 90\. [Subsets II](https://leetcode.com/problems/subsets-ii/description) -f/ag/
* 78\. [Subsets](https://leetcode.com/problems/subsets/description) -f/ag/
* 131\. [Palindrome Partitioning](https://leetcode.com/problems/palindrome-partitioning/description) -f/ag


* 37\. Sudoku Solver ??
* 51\. [N-Queens](https://leetcode.com/problems/n-queens/description)
* 52\. [N-Queens II](https://leetcode.com/problems/n-queens-ii/description)
* 22\. [Generate Parentheses](https://leetcode.com/problems/generate-parentheses/description) 

* 93\. [Restore IP Addresses](https://leetcode.com/problems/restore-ip-addresses/description)



# [Divide and Conquer](DivideConquer/)

* 215\. [Kth Largest Element in an Array](https://leetcode.com/problems/kth-largest-element-in-an-array/description) -f
* 240\. [Search a 2D Matrix II](https://leetcode.com/problems/search-a-2d-matrix-ii/description).  -f!!!!!!!/

* 241\. [Different Ways to Add Parentheses](https://leetcode.com/problems/different-ways-to-add-parentheses/description)

# [Greedy](Greedy/)

* 253\. [meeting rooms ii](https://www.lintcode.com/problem/meeting-rooms-ii/description) -f!!!!!!!!!!/区间的处理，见Array板块总结/
* 134\. [Gas Station](https://leetcode.com/problems/gas-station/description) -f!!!/ greedy_and_tricks solution based on 2 facts: 1). A can not reach B, any station(A can reach) between A and B can not reach B 2). Total gas is bigger(>=) than Total cost  <=>  There must be a solution. / prove the 2 facts.../


* 135\. [Candy] (https://leetcode.com/problems/candy/description/) -f!/use two arrays(dp)/like "Trapping Rain Water" problem/
* 45\. [Jump Game II](https://leetcode.com/problems/jump-game-ii/description)
* 55\. [Jump Game](https://leetcode.com/problems/jump-game/description)
* 122\. [Best Time to Buy and Sell Stock II](https://leetcode.com/problems/best-time-to-buy-and-sell-stock-ii/description)
* 316\. [Remove Duplicate Letters](https://leetcode.com/problems/remove-duplicate-letters/description)
* 330\. [Patching Array](https://leetcode.com/problems/patching-array/description)



# [Math](Math/)

##### prime number
* 204\. [Count Primes](https://leetcode.com/problems/count-primes/description) -f!/
* 762\. [Prime Number of Set Bits in Binary Representation](https://leetcode.com/problems/prime-number-of-set-bits-in-binary-representation/description/) -f!/


##### count using math method (用排列组合来计算数量)
* 750\. Number Of Corner Rectangles.  -f!!!!!!!/ms interview!!/
* xxx\. Number of monotonic segements.  -f/Autonomic Interview/

==
* 166\. [Fraction to Recurring Decimal](https://leetcode.com/problems/fraction-to-recurring-decimal/description) -p/
* 7\. [Reverse Integer](https://leetcode.com/problems/reverse-integer/description)  -p
* 9\. [Palindrome Number](https://leetcode.com/problems/palindrome-number/description)  -p
* 12\. [Integer to Roman](https://leetcode.com/problems/integer-to-roman/description)  -p
* 13\. [Roman to Integer](https://leetcode.com/problems/roman-to-integer/description) -p
* 43\. [Multiply Strings](https://leetcode.com/problems/multiply-strings/description) -p
* 50\. [Pow(x, n)](https://leetcode.com/problems/powx-n/description) -p/binarysearch
* 69\. [Sqrt(x)](https://leetcode.com/problems/sqrtx/description)  -p/binarysearch
* 60\. [Permutation Sequence](https://leetcode.com/problems/permutation-sequence/description) -p!
* 66\. [Plus One](https://leetcode.com/problems/plus-one/description) -s
* 168\. [Excel Sheet Column Title](https://leetcode.com/problems/excel-sheet-column-title/description) -f!!!/ag!!!/
* 171\. [Excel Sheet Column Number](https://leetcode.com/problems/excel-sheet-column-number/description) -p
* 172\. [Factorial Trailing Zeroes](https://leetcode.com/problems/factorial-trailing-zeroes/description) -p
* 202\. [Happy Number](https://leetcode.com/problems/happy-number/description) -p!/hash/CycleDetect-O(1)space/
* 223\. [Rectangle Area](https://leetcode.com/problems/rectangle-area/description) -p

==
* 343\. [Integer Break](https://leetcode.com/problems/integer-break/description) 
* 233\. [Number of Digit One](https://leetcode.com/problems/number-of-digit-one/description)
* 263\. [Ugly Number](https://leetcode.com/problems/ugly-number/description)
* 273\. [Integer to English Words](https://leetcode.com/problems/integer-to-english-words/description)
* 319\. [Bulb Switcher](https://leetcode.com/problems/bulb-switcher/description)
* 326\. [Power of Three](https://leetcode.com/problems/power-of-three/description)
* 335\. [Self Crossing](https://leetcode.com/problems/self-crossing/description)
* 258\. [Add Digits](https://leetcode.com/problems/add-digits/description)


# [Bit Manipulation](BitManipulation/)

##### number of bits:
* 191\. [Number of 1 Bits](https://leetcode.com/problems/number-of-1-bits/description) -f!!!/ need to remember the classic solution!!!!/ n&(n-1) flip the last '1' to '0'/
* 762\. [Prime Number of Set Bits in Binary Representation](https://leetcode.com/problems/prime-number-of-set-bits-in-binary-representation/description/) -f!/


* 78\. [Subsets](https://leetcode.com/problems/subsets/description) -f/
* 136\. [Single Number](https://leetcode.com/problems/single-number/description) -p/
* 137\. [Single Number II](https://leetcode.com/problems/single-number-ii/description) - f/bit op version?/
* 190\. [Reverse Bits](https://leetcode.com/problems/reverse-bits/description) -?


==

* 201\. [Bitwise AND of Numbers Range](https://leetcode.com/problems/bitwise-and-of-numbers-range/description)
* 231\. [Power of Two](https://leetcode.com/problems/power-of-two/description)
* 260\. [Single Number III](https://leetcode.com/problems/single-number-iii/description)
* 268\. [Missing Number](https://leetcode.com/problems/missing-number/description)
* 318\. [Maximum Product of Word Lengths](https://leetcode.com/problems/maximum-product-of-word-lengths/description)
* 342\. [Power of Four](https://leetcode.com/problems/power-of-four/description)
* 371\. [Sum of Two Integers](https://leetcode.com/problems/sum-of-two-integers/description)
* 338\. [Counting Bits](https://leetcode.com/problems/counting-bits/description)


# [Design](Design/)

* 146\. [LRU Cache](https://leetcode.com/problems/lru-cache/description) -f/ag/
* 355\. [Design Twitter](https://leetcode.com/problems/design-twitter/description)

# [Trie](https://leetcode.com/tag/string._DataStructure_Trie_PrefixTree/)
* 211\. Add and Search Word - Data structure design. -f
* 79\. Word Search   -f!!!!/string._DataStructure_Trie_PrefixTree+dfs+backtrack!!!
* 212\. [Word Search II](https://leetcode.com/problems/word-search-ii/description/).   -f!!!!/string._DataStructure_Trie_PrefixTree+dfs+backtrack!!!

# NoTag
later:  
* 407\. Trapping Rain Water II -later?/gg.../.  
* 659\. Split Array into Consecutive Subsequences.  
* xxx. 给定一个整形数组，数组是无重复随机无序的，要求打印出所有元素左边第一个大于该元素的值 ？


# System Design
可扩展性: google三架马车
