package dp;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by thj on 2018/8/6.
 *
 *
 *  1. recursion:  will cause TLE
 *  2. DP: good! O(mn) or O(n^2)
 *  3. DFS with cache (also DP): good!
 *
 */
public class WordBreak {

    public boolean wordBreak_v3(String s, List<String> wordDict) {
        boolean[] dp = new boolean[s.length()+1];
        dp[0]=true;

        // DP_1: O(n*m)
//        for (int i = 1; i <= s.length(); i++) {
//            for (String word: wordDict) {
//                if (i-word.length()>=0 && dp[i-word.length()]
//                        && word.equals(s.substring(i-word.length(), i))) {
//                    dp[i] = true;
//                    break;
//                }
//            }
//        }


        // DP_2: O(n*n)
        Set<String> dic = new HashSet<>(wordDict);
        for (int i = 1; i <= s.length(); i++) {

            for (int j = 0; j <= i-1 ; j++) { // notice the condition!!, not j<i-1
                if (dp[j] && dic.contains(s.substring(j, i))) {
                    dp[i] = true;
                    break;
                }
            }
        }

        return dp[s.length()];
    }

    public boolean wordBreak_v2(String s, List<String> wordDict) {
        Set<String> failCache = new HashSet<>();
        return dfs_with_cache(s, 0, wordDict, failCache);
    }

    private boolean dfs_with_cache(String s, int start, List<String> wordDict, Set<String> cache) {
        if(cache.contains(s.substring(start, s.length()))) return false;
        if(start == s.length()) return true;
        if(start > s.length()) return false;

        for(String word: wordDict) {
            if(s.substring(start, s.length()).startsWith(word)) {
                if(dfs_with_cache(s, start+word.length(), wordDict, cache)) return true;
            }
        }
        cache.add(s.substring(start, s.length()));
        return false;
    }

    // recursion_version -> will cause LTE
//    public boolean wordBreak_v1(String s, List<String> wordDict) {
//        return  helper(s, new HashSet<>(wordDict));
//    }
//
//    private boolean helper(String s, Set<String> dic) {
//        if (dic.contains(s)) {
//            return true;
//        }
//        for (int i = 0; i < s.length(); i++) {
//            String prefix = s.substring(0, i+1);
//            if (dic.contains(prefix)) {
//                if (helper(s.substring(i+1, s.length()), dic)) {
//                    return true;
//                }
//            }
//        }
//        return false;
//    }

    public static void main(String[] args) {
        System.out.println(new WordBreak().wordBreak_v2("leetcode", Arrays.asList("leet", "code")));
        System.out.println(new WordBreak().wordBreak_v2("catsandog", Arrays.asList("cats", "dog", "sand", "and", "cat")));
        System.out.println(new WordBreak().wordBreak_v2("applepenapple", Arrays.asList("apple", "pen")));

    }
}
