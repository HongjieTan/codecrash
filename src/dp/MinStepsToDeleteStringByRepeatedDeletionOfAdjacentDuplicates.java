// Given a string str. You are allowed to delete only some contiguous characters if all the characters are same in a single operation.
// The task is to find the minimum number of operations required to completely delete the string.
//
// Examples:
//
// Input: str = “abcddcba”
// Output: 4
// Delete dd, then the string is “abccba”
// Delete cc, then the string is “abba”
// Delete bb, then the string is “aa”
// Delete aa, then the string is null.
//
// Input: str = “abc”
// Output: 3
//
// www.geeksforgeeks.org/minimum-steps-to-delete-a-string-by-deleting-substring-comprising-of-same-characters/
package dp;

public class MinStepsToDeleteStringByRepeatedDeletionOfAdjacentDuplicates {
    // O(n^3)
    //    Let dp[l][r] be the answer for sub-string s[l, l+1, l+2, …r]. Then we have two cases:
    //    The first letter of the sub-string is deleted separately from the rest, then dp[l][r] = 1 + dp[l+1][r].
    //    The first letter of the sub-string is deleted alongside with some other letter (both letters must be equal), then dp[l][r] = dp[l+1][i-1] + dp[i][r], given that l ≤ i ≤ r and s[i] = s[l].
    public int minimumMoves(int[] arr) {
        int n = arr.length;
        int[][] dp = new int[n+1][n+1];
        for(int len=1;len<=n;len++) {
            for(int i=0,j=len-1;j<n;i++,j++) {
                if(len==1) dp[i][j]=1;
                else {
                    dp[i][j] = 1+dp[i+1][j];
                    for(int k=i+1;k<=j;k++) {
                        if(arr[i]==arr[k]) {
                            dp[i][j] = Math.min(dp[i][j], dp[i+1][k-1] + dp[k][j]);
                        }
                    }
                }
            }
        }
        return dp[0][n-1];
    }

    public static void main(String[] args) {
        System.out.println(new MinStepsToDeleteStringByRepeatedDeletionOfAdjacentDuplicates().minimumMoves(new int[]{1,2,3,4,4,3,2,1}));
    }
}
