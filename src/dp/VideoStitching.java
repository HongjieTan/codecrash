package dp;

import java.util.Arrays;

/**
 *  lc1024
 *
 *  - dp: O（n * T）
 *  - greedy:  O（n * logn）
 *
 */
public class VideoStitching {

    // dp, O（n * T）
    public int videoStitching(int[][] clips, int T) {
        int[] dp = new int[T+1];
        Arrays.fill(dp, 101);
        dp[0]=0;
        for(int i=1;i<=T;i++) {
            for(int[] c: clips) {
                if(c[0]<=i&&c[1]>=i) dp[i]=Math.min(dp[i], dp[c[0]]+1);
            }
            if(dp[i]==101) return -1;
        }
        return dp[T];
    }

    // dp-2d, O(n*T)
//    public int videoStitching(int[][] clips, int T) {
//        int n=clips.length;
//        int[][] dp = new int[T+1][n+1];
//        for(int i=0;i<=T;i++) Arrays.fill(dp[i], 101);
//        for(int i=0;i<=T;i++) {
//            for(int j=0;j<=n;j++) {
//                if (i==0 && j==0) dp[i][j]=0;
//                else if (i==0) dp[i][j]=0;
//                else if (j==0) dp[i][j]=101;
//                else {
//                    int[] c = clips[j-1];
//                    if (c[0]<=i && c[1]>=i) dp[i][j] = Math.min(dp[i][j-1], dp[c[0]][n]+1); // notice here...
//                    else dp[i][j] = dp[i][j-1];
//                }
//            }
//            if (dp[i][n]==101) {
//                return -1;
//            }
//        }
//        return dp[T][n];
//    }


    // greedy, O(nlogn)
    public int videoStitching_greedy(int[][] clips, int T) {
        Arrays.sort(clips, (a, b)->a[0]-b[0]);
        int start = 0, steps=0, idx=0;
        while(start<T) {
            int maxEnd = -1;
            while(idx<clips.length && clips[idx][0]<=start) maxEnd = Math.max(maxEnd, clips[idx++][1]);
            if (maxEnd==-1) return -1;
            start = maxEnd;
            steps++;
        }
        return steps;
    }

    /*
    def videoStitching(self, clips, T):
        clips = sorted(clips)
        start, steps, idx = 0,0,0
        while start<T:
            end = -1
            while idx<len(clips) and clips[idx][0]<=start: end=max(end, clips[idx++][1])
            if end==-1; return -1
            start = end
            steps++
        return  steps
    */

    public static void main(String[] as) {
        int[][] clips = new int[][]{
                {0,2},{4,6},{8,10},{1,9},{1,5},{5,9},
//                {0,2},{4,6},{8,10},{1,9},
//                {0,1}, {1,2}
        };
        int T = 10;
        System.out.println(new VideoStitching().videoStitching(clips, T));
        System.out.println(new VideoStitching().videoStitching_greedy(clips, T));
    }
}

/*
You are given a series of video clips from a sporting event that lasted T seconds.
These video clips can be overlapping with each other and have varied lengths.

Each video clip clips[i] is an interval: it starts at time clips[i][0] and ends at time clips[i][1].
We can cut these clips into segments freely: for example, a clip [0, 7] can be cut into segments [0, 1] + [1, 3] + [3, 7].

Return the minimum number of clips needed so that we can cut the clips into segments that cover the entire sporting event ([0, T]).
If the task is impossible, return -1.



Example 1:

Input: clips = [[0,2],[4,6],[8,10],[1,9],[1,5],[5,9]], T = 10
Output: 3
Explanation:
We take the clips [0,2], [8,10], [1,9]; a total of 3 clips.
Then, we can reconstruct the sporting event as follows:
We cut [1,9] into segments [1,2] + [2,8] + [8,9].
Now we have segments [0,2] + [2,8] + [8,10] which cover the sporting event [0, 10].
Example 2:

Input: clips = [[0,1],[1,2]], T = 5
Output: -1
Explanation:
We can't cover [0,5] with only [0,1] and [0,2].
Example 3:

Input: clips = [[0,1],[6,8],[0,2],[5,6],[0,4],[0,3],[6,7],[1,3],[4,7],[1,4],[2,5],[2,6],[3,4],[4,5],[5,7],[6,9]], T = 9
Output: 3
Explanation:
We can take clips [0,4], [4,7], and [6,9].
Example 4:

Input: clips = [[0,4],[2,8]], T = 5
Output: 2
Explanation:
Notice you can have extra video after the event ends.


Note:

1 <= clips.length <= 100
0 <= clips[i][0], clips[i][1] <= 100
0 <= T <= 100
*/

/*
hint1:
What if we sort the intervals? Considering the sorted intervals, how can we solve the problem with dynamic programming?
hint2:
Let's consider a DP(pos, limit) where pos represents the position of the current interval we are gonna take the decision
and limit is the current covered area from [0 - limit]. This DP returns the minimum number of taken intervals or infinite
if it's not possible to cover the [0 - T] section.


public int videoStitching(int[][] clips, int T) {
        int[] dp = new int[T+ 1];
        Arrays.fill(dp, T+1);
        dp[0] = 0;
        for(int i = 0; i <= T; i++) {
            for(int[] c : clips) {
                if(i >= c[0] && i <= c[1]) dp[i] = Math.min(dp[i], dp[c[0]] + 1);
            }
            if(dp[i] == T+1) return -1;
        }
        return dp[T];
    }
*/