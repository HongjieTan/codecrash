//Given an integer array arr and an integer difference, return the length of the longest subsequence in arr which is
// an arithmetic sequence such that the difference between adjacent elements in the subsequence equals difference.
//
//
// Example 1:
//
//
//Input: arr = [1,2,3,4], difference = 1
//Output: 4
//Explanation: The longest arithmetic subsequence is [1,2,3,4].
//
// Example 2:
//
//
//Input: arr = [1,3,5,7], difference = 1
//Output: 1
//Explanation: The longest arithmetic subsequence is any single element.
//
//
// Example 3:
//
//
//Input: arr = [1,5,7,8,5,3,4,2,1], difference = -2
//Output: 4
//Explanation: The longest arithmetic subsequence is [7,5,3,1].
//
//
//
// Constraints:
//
//
// 1 <= arr.length <= 10^5
// -10^4 <= arr[i], difference <= 10^4
//
package dp.Topic_SubSeq;

import java.util.HashMap;
import java.util.Map;


// Notice the difference with LIS problem !
public class LongestArithmeticSubsequenceOfGivenDifference {

    // dp v2: O(n), good!
    public int longestSubsequence(int[] arr, int difference) {
        Map<Integer, Integer> dp = new HashMap<>();
        int res = 0;
        for(int x: arr) {
            if(dp.containsKey(x-difference)) {
                dp.put(x, dp.get(x-difference)+1);
            } else {
                dp.put(x, 1);
            }
            res = Math.max(res, dp.get(x));
        }
        return res;
    }


    // dp v1: O(n^2), TLE
    // dp[i]: longest length of subseq ending at i
//    public int longestSubsequence(int[] arr, int difference) {
//        int n = arr.length;
//        int[] dp = new int[n];
//        dp[0]=1;
//        int res=1;
//        for(int i=1;i<n;i++) {
//            int max = 1;
//            for(int j=0;j<i;j++) {
//                if(arr[i]-arr[j]==difference) max = Math.max(max, dp[j]+1);
//            }
//            dp[i] = max;
//            res = Math.max(res, dp[i]);
//        }
//        return res;
//    }

    public static void main(String[] as) {
        int[] arr = {1,2,3,4};
        int diff = 1;
        System.out.println(new LongestArithmeticSubsequenceOfGivenDifference().longestSubsequence(arr, diff));
    }
}
