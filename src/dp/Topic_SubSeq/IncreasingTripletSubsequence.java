//Given an unsorted array return whether an increasing subsequence of length 3 exists or not in the array.
//
// Formally the function should:
//
// Return true if there exists i, j, k
//such that arr[i] < arr[j] < arr[k] given 0 ≤ i < j < k ≤ n-1 else return false.
//
// Note: Your algorithm should run in O(n) time complexity and O(1) space complexity.
//
//
// Example 1:
//
//
//Input: [1,2,3,4,5]
//Output: true
//
//
//
// Example 2:
//
//
//Input: [5,4,3,2,1]
//Output: false
//
//
//
package dp.Topic_SubSeq;

public class IncreasingTripletSubsequence {


    // O(n), space:O(1)   also check LIS problem!!
    public boolean increasingTriplet(int[] nums) {
        if(nums.length<3) return false;
        int t1=nums[0], t2=Integer.MAX_VALUE;
        for (int i = 1; i < nums.length; i++) {
            if (nums[i]<=t1) {
                t1=nums[i];
            } else if (nums[i]<=t2) {
                t2=nums[i];
            } else if (nums[i]>t2) {
                return true;
            }
        }
        return false;
    }

    // O(n), space:O(n)
    public boolean increasingTriplet_v0(int[] nums) {
        if(nums.length<3) return false;
        int n=nums.length;
        int[] left = new int[n];
        int[] right = new int[n];

        left[0]=Integer.MAX_VALUE;
        for(int i=1;i<n;i++)  left[i]=Math.min(left[i-1], nums[i-1]);

        right[n-1]=Integer.MIN_VALUE;
        for(int i=n-2;i>=0;i--) right[i]=Math.max(right[i+1], nums[i+1]);

        for(int i=1;i<n-1;i++) {
            if(nums[i]>left[i] && nums[i]<right[i]) return true;
        }
        return false;
    }



}
