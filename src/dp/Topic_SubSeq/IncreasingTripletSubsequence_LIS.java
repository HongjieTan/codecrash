package dp.Topic_SubSeq;

/**
 *  greedy_and_tricks
 *
 *  use trick ...
 */
public class IncreasingTripletSubsequence_LIS {
    public boolean increasingTriplet(int[] nums) {
        int small=Integer.MAX_VALUE, big=Integer.MAX_VALUE;
        for (int num: nums) {
            if (num<=small) small=num;
            else if (num<=big) big=num;
            else return true;
        }
        return false;
    }

    public static void main(String[] args) {
        int[] nums = new int[]{1,4,3};
        System.out.println(new IncreasingTripletSubsequence_LIS().increasingTriplet(nums));
    }
}
