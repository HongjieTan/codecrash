package dp.Topic_SubSeq;

/**
 *
 *  - 2Pointer: ? TODO
 *  - DP: O(m*n)
 *  - bruteforce: O(n^2)
 *
 *
 *
 */
public class MinWindowSubsequence {

    /*

       dp[i,j]  RIGHTMOST START POINT of window for S[0...i-1], T[0...j-1]  !!

       i==0 && j==0: -1
       i >0 && j==0: i (ngotice!!!!!)
       i==0 && j >0: -1
       i >0 && j >0:
            S[i-1]==T[j-1]: dp[i-1][j-1]
            S[i-1]!=T[j-1]: dp[i-1][j]
     */
    public String minWindow(String S, String T) {
        int[][] dp = new int[S.length()+1][T.length()+1];
        for (int i = 0; i < S.length() + 1; i++) {
            for (int j = 0; j < T.length() + 1; j++) {
                if (i==0) dp[i][j]=-1;
                else if (j==0) dp[i][j]=i; //!!!
                else {
                    if (S.charAt(i-1)==T.charAt(j-1)) dp[i][j]=dp[i-1][j-1];
                    else dp[i][j]=dp[i-1][j];
                }
            }
        }

        int minLen=Integer.MAX_VALUE, start=0;
        for (int end = 0; end < S.length(); end++) {
            if (dp[end+1][T.length()]!=-1) {
                int len = end-dp[end+1][T.length()]+1;
                if (len<minLen) {
                    minLen = Math.min(minLen, len);
                    start = dp[end+1][T.length()];
                }
            }
        }
        return minLen==Integer.MAX_VALUE?"":S.substring(start, start+minLen);
    }

    public static void main(String[] args) {
        String S = "ab";
        String T = "b";
        System.out.println(new MinWindowSubsequence().minWindow(S, T));
    }
}
