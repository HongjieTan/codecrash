//Given an unsorted array of integers, find the length of longest increasing subsequence.
//
// Example:
//
//
//Input: [10,9,2,5,3,7,101,18]
//Output: 4
//Explanation: The longest increasing subsequence is [2,3,7,101], therefore the length is 4.
//
// Note:
//
//
// There may be more than one LIS combination, it is only necessary for you to return the length.
// Your algorithm should run in O(n2) complexity.
//
//
// Follow up: Could you improve it to O(n log n) time complexity?
// Related Topics Binary Search Dynamic Programming
package dp.Topic_SubSeq;

import java.util.Arrays;

/**
 * Created by thj on 2018/8/12.
 *
 *   4. dp_v2: O(nlogn)  best solution!
 *
 *   3. dp_v1: O(n^2)  good!
 *
 *   2. solution1 + memorization: O(n^2)
 *   1. brute force:  O(2^n) LTE
 *
 */
public class LongestIncreasingSubsequence_LIS {

    // cool and clear !!!
    public int LIS_x3(int[] nums) {
        if(nums==null || nums.length==0) return 0;
        int[] tails = new int[nums.length];
        tails[0]=nums[0];
        int len=1;
        for (int i=0;i<nums.length;i++) {
            if(nums[i]>tails[len-1]) {
                tails[len++]=nums[i];
            } else {
                int index = Arrays.binarySearch(tails, 0, len, nums[i]);
                if(index<0) index=-index-1;
                tails[index]=nums[i];
            }
        }
        return len;
    }

    // the concise version...cool!
    int LIS(int[] nums) {
        int[] tail = new int[nums.length];
        int len=0;
        for(int num:nums) {
            int pos=Arrays.binarySearch(tail, 0, len, num);
            if(pos<0) pos=-(pos+1);
            tail[pos]=num;
            if (pos==len)len++;
        }
        return len;
    }

    /*
    4. dp with binary search: O(nlogn), best solution!!! but a little hard to think at first time!!!
         refs: https://leetcode.com/problems/longest-increasing-subsequence/discuss/74824/JavaPython-Binary-search-O(nlogn)-time-with-explanation

       tails[i] store the smallest tail of all increasing subsequences with length i+1
       for each x in nums, do:
        (1) if x is larger than all tails, append it, increase the size by 1
        (2) if tails[i-1] < x <= tails[i], update tails[i]
            (We can easily prove that tails is a increasing array, Therefore it is possible to do a binary search in tails array to find the one needs update.)
    */
    public int lengthOfLIS_dp2(int[] nums) {
        int[] tails = new int[nums.length];
        int len = 0;
        for (int num: nums) {
            if (len==0 || num > tails[len-1]) {
                tails[len++] = num;
            } else {
                int posToUpdate = findUpdatePos(tails, 0, len-1, num);
                tails[posToUpdate] = num;
            }
        }
        return len;
    }
    private int findUpdatePos(int[] tails, int l, int r, int target) {
        while (l<=r) {
            int mid=l+(r-l)/2;
            if ((mid==0||tails[mid-1]<target) && target<=tails[mid]) return mid; // target point!!!!!
            if (target < tails[mid]) r=mid-1;
            else l=mid+1;
        }
        return l;
    }


    /*
        3. dp_v1: O(n^2), easy to understand!

        dp[i] mean LIS of nums[0,...,i] and nums[i] as MaxElement(tail) in the LIS!!!
        i=0: dp[i]=1
        i>0: max of dp[k], k: k=0...i-1 and num[i]>num[k]

        final LIS = max of dp[0,...,n-1]
    */
    public int lengthOfLIS_dp(int[] nums) {
        if (nums==null || nums.length==0) return 0;

        int[] dp = new int[nums.length];
        dp[0] = 1;
        for (int i = 1; i < nums.length; i++) {
            int maxLen = 0;
            for (int j = 0; j < i; j++) {
                if (nums[i] > nums[j]) {
                    maxLen = Math.max(maxLen, dp[j]);
                }
            }
            dp[i] = maxLen+1;
        }


        int res = 0;
        for (int i = 0; i < dp.length; i++) {
            res = Math.max(res, dp[i]);
        }
        return res;
    }



//    // 2. Recursion with memorization： O(n^2）
//    public int lengthOfLIS_recursion_with_cache(int[] nums) {
//        int[][] cache = new int[nums.length][nums.length+1];  // cache[i][j]: nums[i] is currentPos, nums[j-1] is prevMax,
//        for (int[] arr: cache) Arrays.fill(arr, -1); // init
//        return LIS_with_cache(nums, 0, -1, cache);
//    }
//    private int LIS_with_cache(int[] nums, int curPos, int prevMaxIdx, int[][] cache) {
//        if (curPos == nums.length) return 0;
//        if (cache[curPos][prevMaxIdx+1]!=-1) {
//            return cache[curPos][prevMaxIdx];
//        }
//        int taken = 0;
//        if (prevMaxIdx<0 || nums[curPos] > nums[prevMaxIdx]) {
//            taken = 1 + LIS_with_cache(nums, curPos+1, curPos, cache);
//        }
//        int unTaken = LIS_with_cache(nums, curPos+1, prevMaxIdx, cache);
//        int res = Math.max(taken, unTaken);
//        cache[curPos][prevMaxIdx+1] = res;
//        return res;
//    }
//
//    // 1. brute force with recursion:  O(2^n) LTE
//    public int lengthOfLIS_recursion(int[] nums) {
//        return LIS(nums, 0, Integer.MIN_VALUE);
//    }
//
//    // prevMax: max of lis for nums[startPos, ..., n-1]
//    private int LIS(int[] nums, int startPos, int prevMax) {
//        if (startPos==nums.length) return 0;
//        int taken = 0;
//        if (nums[startPos]>prevMax) {
//            taken = 1 + LIS(nums, startPos+1, nums[startPos]);
//        }
//        int unTaken = LIS(nums, startPos+1, prevMax);
//        return Math.max(taken, unTaken);
//    }


    public static void main(String[] args) {
//        int[] nums = new int[]{10,9,2,5,3,4};
        int[] nums = new int[]{4,10,4,3,8,9};
        System.out.println(new LongestIncreasingSubsequence_LIS().LIS(nums));
    }

}
