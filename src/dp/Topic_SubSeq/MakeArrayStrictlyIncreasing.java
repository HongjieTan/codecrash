//Given two integer arrays arr1 and arr2, return the minimum number of operations (possibly zero) needed to make arr1 strictly increasing.
//
// In one operation, you can choose two indices 0 <= i < arr1.length and 0 <= j < arr2.length and do the assignment arr1[i] = arr2[j].
//
// If there is no way to make arr1 strictly increasing, return -1.
//
//
// Example 1:
//
//
//Input: arr1 = [1,5,3,6,7], arr2 = [1,3,2,4]
//Output: 1
//Explanation: Replace 5 with 2, then arr1 = [1, 2, 3, 6, 7].
//
//
// Example 2:
//
//
//Input: arr1 = [1,5,3,6,7], arr2 = [4,3,1]
//Output: 2
//Explanation: Replace 5 with 3 and then replace 3 with 4. arr1 = [1, 3, 4, 6, 7].
//
//
// Example 3:
//
//
//Input: arr1 = [1,5,3,6,7], arr2 = [1,6,3,3]
//Output: -1
//Explanation: You can't make arr1 strictly increasing.
//
//
// Constraints:
//
//
// 1 <= arr1.length, arr2.length <= 2000
// 0 <= arr1[i], arr2[i] <= 10^9
//
//
// Related Topics Dynamic Programming

package dp.Topic_SubSeq;


import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class MakeArrayStrictlyIncreasing {

    // dp[i][j]: min operations for arr1[0...i-1] and arr1[i-1] is set to j
    public int makeArrayIncreasing(int[] arr1, int[] arr2) {
        int m = arr1.length, n = arr2.length;
        Map<Integer, Integer> dp = new HashMap<>();
        Arrays.sort(arr2);
        dp.put(-1, 0);
        for(int i=0;i<m;i++) {
            Map<Integer, Integer> tmp = new HashMap<>();
            for(int tail: dp.keySet()) {
                // no assign at arr1[i]
                if(arr1[i]>tail) {
                    tmp.put(arr1[i], Math.min(dp.get(tail), tmp.getOrDefault(arr1[i], Integer.MAX_VALUE)));
                }

                // assign at arr1[i]
                int index = bisect_right(arr2, tail);
                if(index<n) {
                    tmp.put(arr2[index], Math.min(dp.get(tail)+1, tmp.getOrDefault(arr2[index], Integer.MAX_VALUE)));
                }
            }
            dp = tmp;
        }

        if (dp.isEmpty()) return -1;
        int res = Integer.MAX_VALUE;
        for(int v: dp.values()) res = Math.min(res, v);
        return res;
    }

    int bisect_right(int[] a, int x) {
        int l=0, r=a.length-1;
        while(l<=r) {
            int mid = l+(r-l)/2;
            if(x>=a[mid])  l=mid+1;
            else  r=mid-1;
        }
        return l;
    }

    /*
    class Solution:
        def makeArrayIncreasing(self, arr1: List[int], arr2: List[int]) -> int:
            dp = {-1: 0}
            arr2.sort()
            for x in arr1:
                tmp = collections.defaultdict(lambda: float("inf"))
                for tail in dp.keys():
                    # no assign
                    if x > tail: tmp[x] = min(dp[tail], tmp[x])
                    # assign
                    pos = bisect.bisect_right(arr2, tail)
                    if pos < len(arr2): tmp[arr2[pos]] = min(dp[tail] + 1, tmp[arr2[pos]])
                dp = tmp
            return min(dp.values()) if dp else -1
    */

    public static void main(String[] as) {
//        System.out.println(new MakeArrayStrictlyIncreasing().makeArrayIncreasing(new int[]{1,5,3,6,7}, new int[]{1,3,2,4})); //1
//        System.out.println(new MakeArrayStrictlyIncreasing().makeArrayIncreasing(new int[]{1,5,3,6,7}, new int[]{4,3,1}));   //2
//        System.out.println(new MakeArrayStrictlyIncreasing().makeArrayIncreasing(new int[]{1,5,3,6,7}, new int[]{1,6,3,3})); //-1
        System.out.println(new MakeArrayStrictlyIncreasing().makeArrayIncreasing(new int[]{0,11,6,1,4,3}, new int[]{5,4,11,10,1,0})); //5
    }
}


/*
by top post:
Similar to Longest-increasing-sub-sequence problem.
Record the possible states of each position and number of operations to get this state.
When we check i-th element in the arr1, dp record the possible values we can place at this position, and the number of operations to get to this state.
Now, we need to build dp for (i+1)-th position, so for (i+1)-th element,
if it's larger than the possible state from i-th state, we have two choices:
1, keep it so no operation needs to be made.
2, choose from arr2 a smaller element that is larger than i-th element and add one operation.
If it's not larger than the i-th state, we definitely need to make a possible operation.

class Solution:
    def makeArrayIncreasing(self, arr1: List[int], arr2: List[int]) -> int:
        dp = {-1:0}
        arr2.sort()
        for i in arr1:
            tmp = collections.defaultdict(lambda: float('inf'))
            for key in dp:
                if i > key:
                    tmp[i] = min(tmp[i],dp[key])
                loc = bisect.bisect_right(arr2,key)
                if loc < len(arr2):
                    tmp[arr2[loc]] = min(tmp[arr2[loc]],dp[key]+1)
            dp = tmp
        if dp:
            return min(dp.values())
        return -1


by 2nd post:
This problem is similiar to the Longest Increasing Subsequence (LIS) problem (LC300).
The only difference is that, while we are not only calculating the longest increasing subsequence,
we also need to make sure that, base on this sequence, we can form a new arr1 from swapping with arr2,to be a complete increasing array.

So to start, the dp relationship in LIS problem is:

//dp[i] -> longest increasing subsequence length ending at arr[i]
if (arr1[j] < arr1[i])
	dp[i] = Math.max(dp[i], dp[j] + 1)
We can start from this relationship, just add a simple check function:
the function checks if arr1[j] to arr1[i] can be swapped from arr2, to make a complete increasing array, ending at arr1[i].
So now the relationship becomes:

//dp[i] -> the answer we need (nubmer of swapping operations), ending at arr1[i]
if (arr1[j] < arr1[i] && dp[j] != Integer.MAX_VALUE){
	int change = check(arr1, arr2, j, i);
	if (change >= 0){
		dp[i] = Math.min(dp[i], dp[j] + change);
    }
}
Now things are done! The check function is actually simple to implement after sorting arr2 and binary search.
Some tricky edge cases here are we have to add one min and one max to the both side of arr1.
And we need to deal with the duplicates in arr2. Sorry I didn't came up with a more elegant way to do this. I just created two new arrays.

class Solution {
    public int makeArrayIncreasing(int[] arr1, int[] arr2) {
        int n = arr1.length;

        //sort and generate new arr2
        Arrays.sort(arr2);
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < arr2.length; i++){
            if (i+1 < arr2.length && arr2[i] == arr2[i+1])
                continue;
            list.add(arr2[i]);
        }
        int[] newarr2 = new int[list.size()];
        for (int i = 0; i < list.size(); i++)
            newarr2[i] = list.get(i);
        arr2 = newarr2;

		//generate new arr1
        int[] newarr1 = new int[n+2];
        for (int i = 0; i < n; i++)
            newarr1[i+1] = arr1[i];
        newarr1[n+1] = Integer.MAX_VALUE;
        newarr1[0] = Integer.MIN_VALUE;
        arr1 = newarr1;

		//perform dp based on LIS
        int[] dp = new int[n+2];
        Arrays.fill(dp, Integer.MAX_VALUE);
        //dp[i] -> answer to change array 0 to i
        dp[0] = 0;
        for (int i = 1; i < n+2; i++){
            for (int j = 0; j < i; j++){
                if (arr1[j] < arr1[i] && dp[j] != Integer.MAX_VALUE){
                    int change = check(arr1, arr2, j, i);
                    if (change >= 0){
                        dp[i] = Math.min(dp[i], dp[j] + change);
                    }
                }
            }
        }
        return dp[n+1] == Integer.MAX_VALUE? -1:dp[n+1];
    }

    //change number from start+1 to end-1
    private int check(int[] arr1, int[] arr2, int start, int end){
        if (start+1 == end)
            return 0;
        int min = arr1[start];
        int max = arr1[end];
        int idx = Arrays.binarySearch(arr2, min);
        if (idx < 0)
            idx = -idx-1;
        else
            idx = idx+1;

        int maxcount = end-start-1;
        int endi = idx + maxcount-1;
        if (endi < arr2.length && arr2[endi] < max)
            return maxcount;
        else
            return -1;
    }
}

*/
