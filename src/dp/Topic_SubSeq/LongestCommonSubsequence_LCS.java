package dp.Topic_SubSeq;

/**
 * Created by thj on 01/06/2018.
 *
 * https://www.lintcode.com/problem/longest-common-subsequence/description
 *
 *
 *   dp[i][j] means   LongestCommonSubsequence_LCS of A[0...i-1] and B[0...j-1]
 *
 *   i==0 || j==0:     dp[i][j] = 0
 *   other       :     if A[i-1]==B[j-1],   dp[i][j] = dp[i-1][j-1] + 1
 *                     if A[i-1]!=B[j-1],   dp[i][j] = max(dp[i-1][j], dp[i][j-1])
 *
 *
 */
public class LongestCommonSubsequence_LCS {

    public int longestCommonSubsequence(String A, String B) {
        int[][] dp = new int[A.length()+1][B.length()+1];
        for (int i=0; i<=A.length(); i++) {
            for (int j=0; j<=B.length(); j++) {
                if (i==0 || j==0) {
                    dp[i][j] = 0;
                } else if (A.charAt(i-1) == B.charAt(j-1)) {
                    dp[i][j] = dp[i-1][j-1]+1;
                } else {
                    dp[i][j] = Math.max(dp[i-1][j], dp[i][j-1]);
                }
            }

        }
        return dp[A.length()][B.length()];
    }


    // follow up: reconstruct a lcs
    public String lcs(String a, String b) {
        int m=a.length(), n=b.length(), dp[][]=new int[m+1][n+1];
        char[] ca = a.toCharArray(), cb=b.toCharArray();
        for (int i = 1; i <=m; i++) {
            for (int j = 1; j <=n ; j++) {
                if (ca[i-1]==cb[j-1]) dp[i][j]=dp[i-1][j-1]+1;
                else dp[i][j]=Math.max(dp[i-1][j], dp[i][j-1]);
            }
        }

        int lcs_len = dp[m][n];
        char[] lcs = new char[lcs_len];
        int i = m, j = n, index=lcs.length-1;
        while (i > 0 && j > 0) {
            if (ca[i-1]==cb[j-1]) {
                lcs[index--] = ca[i-1];
                i--;
                j--;
            }
            else if (dp[i-1][j] > dp[i][j-1]) i--;
            else j--;
        }
        return String.valueOf(lcs);
    }



    public static void main(String[] args) {
//        LCS for input Sequences “ABCDGH” and “AEDFHR” is “ADH” of length 3.
//        LCS for input Sequences “AGGTAB” and “GXTXAYB” is “GTAB” of length 4.
        String a = "ABCDGH";
        String b = "AEDFHR";
        LongestCommonSubsequence_LCS su = new LongestCommonSubsequence_LCS();
        System.out.println(su.longestCommonSubsequence(a, b));
        System.out.println(su.lcs(a, b));
    }

}
