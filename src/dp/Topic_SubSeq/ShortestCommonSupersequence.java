/*
Given two strings str1 and str2, return the shortest string that has both str1 and str2 as subsequences. If multiple answers exist, you may return any of them.

(A string S is a subsequence of string T if deleting some number of characters from T (possibly 0, and the characters are chosen anywhere from T) results in the string S.)



Example 1:

Input: str1 = "abac", str2 = "cab"
Output: "cabac"
Explanation:
str1 = "abac" is a substring of "cabac" because we can delete the first "c".
str2 = "cab" is a substring of "cabac" because we can delete the last "ac".
The answer provided is the shortest such string that satisfies these properties.


Note:

1 <= str1.length, str2.length <= 1000
str1 and str2 consist of lowercase English letters.


Hint:
We can find the length of the longest common subsequence between str1[i:] and str2[j:] (for all (i, j)) by using dynamic programming. We can use this information to recover the longest common supersequence.
*/
package dp.Topic_SubSeq;

/*
     good!!
     solution2: dp directly (similar with lcs)
     solution1: based on lcs
*/
public class ShortestCommonSupersequence {

    // solution2: dp directly (same idea as lcs)
    public String shortestCommonSupersequence(String str1, String str2) {
        char[] s1 = str1.toCharArray(), s2 = str2.toCharArray();
        int m=s1.length, n=s2.length, dp[][] = new int[m+1][n+1];

        for(int i=0;i<=m;i++) {
            for(int j=0;j<=n;j++) {
                if(i==0) dp[i][j]=j;
                else if(j==0) dp[i][j]=i;
                else if(s1[i-1]==s2[j-1]) dp[i][j]=1+dp[i-1][j-1];
                else dp[i][j]=1+Math.min(dp[i-1][j], dp[i][j-1]);
            }
        }

        char[] res = new char[dp[m][n]];
        int index = res.length-1;
        int i=m,j=n;
        while(i>0 && j>0) {
            if(s1[i-1]==s2[j-1]) {
                res[index--] = s1[i-1];
                i--;
                j--;
            } else if(dp[i-1][j]>dp[i][j-1]) {
                res[index--] = s2[j-1];
                j--;
            } else {
                res[index--] = s1[i-1];
                i--;
            }
        }
        while (i>0) res[index--] = s1[(i--)-1];
        while (j>0) res[index--] = s2[(j--)-1];
        return String.valueOf(res);
    }


    // solution1: based on lcs
    char[] lcs(String a, String b) {
        int m=a.length(), n=b.length(), dp[][] = new int[m+1][n+1];
        char[] ca = a.toCharArray(), cb = b.toCharArray();

        for(int i=1;i<=m;i++) {
            for(int j=1;j<=n;j++) {
                if(ca[i-1]==cb[j-1]) dp[i][j]=dp[i-1][j-1]+1;
                else dp[i][j]=Math.max(dp[i-1][j], dp[i][j-1]);
            }
        }
        int len = dp[m][n], i=m,j=n,idx=len-1;
        char[] lcs = new char[len];
        while(i>0 && j>0) {
            if(ca[i-1]==cb[j-1]) {
                lcs[idx--] = ca[i-1];
                i--;j--;
            }
            else if(dp[i-1][j]>dp[i][j-1]) i--;
            else j--;
        }
        return lcs;
    }

    public String shortestCommonSupersequence_v1(String str1, String str2) {
        char[] cs1 = str1.toCharArray(), cs2 = str2.toCharArray(), lcs = lcs(str1, str2);
        StringBuilder sb = new StringBuilder();
        int p1=0, p2=0, p=0;
        while(p<lcs.length) {
            while(cs1[p1]!=lcs[p]) sb.append(cs1[p1++]);
            while(cs2[p2]!=lcs[p]) sb.append(cs2[p2++]);
            sb.append(lcs[p]);
            p++;p1++;p2++;
        }
        while(p1<cs1.length) sb.append(cs1[p1++]);
        while(p2<cs2.length) sb.append(cs2[p2++]);
        return sb.toString();
    }


    public static void main(String[] as) {
        System.out.println(new ShortestCommonSupersequence().shortestCommonSupersequence("abac","cab"));
    }

}
