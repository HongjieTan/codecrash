package dp.Topic_SubSeq;

/**
 *
 *  - combination: O(C(n,k))
 *  - dp???
 *
 */
public class DistinctSubsequences {

    /*

        dp[i,j]  num of subseqs of  s[0,...,i-1],  t[0,...,j-1]

        i=0 && j=0: 1
        i=0 && j>0: 0
        i>0 && j=0: 1
        i<0 && j>0:
            s[i-1]==t[j-1]: dp[i,j] = dp[i-1, j-1] + dp[i-1, j]
            s[i-1]!=t[j-1]: dp[i,j] = dp[i-1, j]

     */
    public int numDistinct(String s, String t) {
        int[][] dp = new int[s.length()+1][t.length()+1];
        for (int i = 0; i < s.length() + 1; i++) {
            for (int j = 0; j < t.length() + 1; j++) {
                if (j==0) dp[i][j]=1;
                else if (i==0) dp[i][j]=0;
                else {
                    if (s.charAt(i-1)==t.charAt(j-1)) dp[i][j]=dp[i-1][j-1]+dp[i-1][j];
                    else dp[i][j]=dp[i-1][j];
                }
            }
        }
        return dp[s.length()][t.length()];
    }

    public static void main(String[] args) {
        System.out.println(new DistinctSubsequences().numDistinct("a", "b"));
    }
}
