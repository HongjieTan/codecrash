//Write a program to find the n-th ugly number.
//
// Ugly numbers are positive numbers whose prime factors only include 2, 3, 5.
//
// Example:
//
//
//Input: n = 10
//Output: 12
//Explanation: 1, 2, 3, 4, 5, 6, 8, 9, 10, 12 is the sequence of the first 10 ugly numbers.
//
// Note:
//
//
// 1 is typically treated as an ugly number.
// n does not exceed 1690.
// Related Topics Math Dynamic Programming Heap
package dp;

import java.util.PriorityQueue;
/**
 * math + dp + heap
 * https://leetcode.com/problems/ugly-number-ii/discuss/69364/My-16ms-C%2B%2B-DP-solution-with-short-explanation
 * https://leetcode.com/problems/ugly-number-ii/discuss/69362/O(n)-Java-solution
 *
 */
public class UglyNumberII {

    public int nthUglyNumber_v1(int n) {
        int[] ugly = new int[n];
        ugly[0] = 1;
        int p2=0, p3=0, p5=0;
        for (int i = 1; i < n; i++) {
            int min = Math.min(ugly[p2]*2, Math.min(ugly[p3]*3, ugly[p5]*5));
            if (min == ugly[p2]*2) p2++;
            if (min == ugly[p3]*3) p3++;
            if (min == ugly[p5]*5) p5++;
            ugly[i] = min;
        }
        return ugly[n-1];
    }

    // heap version ...
    public int nthUglyNumber_v2(int n) {
        int[] ugly = new int[n];
        ugly[0] = 1;
        PriorityQueue<int[]> pq = new PriorityQueue<>((a,b)->a[2]-b[2]); // a[0]: prime, a[1]: index, a[2] next value
        pq.add(new int[]{2,0,2});
        pq.add(new int[]{3,0,3});
        pq.add(new int[]{5,0,5});
        for (int i = 1; i < n; i++) {
            ugly[i] = pq.peek()[2];
            while (pq.peek()[2] == ugly[i]) {
                int[] tmp = pq.remove();
                pq.add(new int[]{tmp[0], tmp[1]+1, tmp[0]*ugly[tmp[1]+1]});
            }
        }
        return ugly[n-1];
    }

//    // pq
//    public int nthUglyNumber_v2(int n) {
//        long ugly=0;
//        int k=0;
//
//        PriorityQueue<Long> pq = new PriorityQueue<>();
//        pq.add(1l);
//        while(k<n) {
//            ugly = pq.remove();
//            while(!pq.isEmpty() && pq.peek()==ugly) pq.remove();
//            pq.add(ugly*2l);
//            pq.add(ugly*3l);
//            pq.add(ugly*5l);
//            k++;
//        }
//        return (int)ugly;
//    }

    public static void main(String[] args) {
//        System.out.println(new UglyNumberII().nthUglyNumber(11));
//        System.out.println(new UglyNumberII().nthUglyNumber_v2(11));
    }


}
