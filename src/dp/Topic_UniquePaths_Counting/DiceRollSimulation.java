// A die simulator generates a random number from 1 to 6 for each roll.
// You introduced a constraint to the generator such that it cannot roll the number i more than rollMax[i] (1-indexed) consecutive times.
//
// Given an array of integers rollMax and an integer n, return the number of distinct sequences that can be obtained with exact n rolls.
//
// Two sequences are considered different if at least one element differs from each other. Since the answer may be too large, return it modulo 10^9 + 7.
//
//
// Example 1:
//
//
//Input: n = 2, rollMax = [1,1,2,2,2,3]
//Output: 34
//Explanation: There will be 2 rolls of die, if there are no constraints on the die, there are 6 * 6 = 36 possible combinations. In this case, looking at rollMax array, the numbers 1 and 2 appear at most once consecutively, therefore sequences (1,1) and (2,2) cannot occur, so the final answer is 36-2 = 34.
//
//
// Example 2:
//
//
//Input: n = 2, rollMax = [1,1,1,1,1,1]
//Output: 30
//
//
// Example 3:
//
//
//Input: n = 3, rollMax = [1,1,1,2,2,3]
//Output: 181
//
//
//
// Constraints:
//
//
// 1 <= n <= 5000
// rollMax.length == 6
// 1 <= rollMax[i] <= 15
//
//

package dp.Topic_UniquePaths_Counting;


// dp...good
public class DiceRollSimulation {
    public int dieSimulator(int n, int[] rollMax) {
        int mod = 1000000007;
        long[][] dp = new long[6][16];
        for(int i=0;i<6;i++) {
            dp[i][1]=1;
        }

        for(int i=2;i<=n;i++) {
            long[][] ndp = new long[6][16];
            for(int j=0;j<6;j++) {
                for(int k=0; k<16; k++) {
                    for (int l=0;l<6;l++) {
                        if(l==j) {
                            if (k+1<=rollMax[j]) {
                                ndp[j][k+1] += dp[l][k];
                                ndp[j][k+1] %= mod;
                            }
                        } else {
                            ndp[j][1] += dp[l][k];
                            ndp[j][1] %= mod;
                        }
                    }
                }
            }
            dp = ndp;
        }

        long ret = 0;
        for(int i=0;i<6;i++) {
            for(int j=0;j<16;j++) {
                ret += dp[i][j];
                ret %= mod;
            }
        }
        return (int)ret;
    }

    public static void main(String[] as) {
        System.out.println(new DiceRollSimulation().dieSimulator(2, new int[]{1,1,2,2,2,3}));
    }
}


/*
by uwi:
public int dieSimulator(int n, int[] rollMax) {
    int mod = 1000000007;
    long[][] dp = new long[6][16];
    for(int i = 0;i < 6;i++){
        dp[i][1] = 1;
    }
    for(int i = 2;i <= n;i++){
        long[][] ndp = new long[6][16];
        for(int j = 0;j < 6;j++){
            for(int k = 0;k < 16;k++){
                for(int l = 0;l < 6;l++){
                    if(j == l){
                        if(k+1 <= rollMax[l]){
                            ndp[l][k+1] += dp[j][k];
                            ndp[l][k+1] %= mod;
                        }
                    }else{
                        ndp[l][1] += dp[j][k];
                        ndp[l][1] %= mod;
                    }
                }
            }
        }
        dp = ndp;
    }

    long ret = 0;
    for(int i = 0;i < 6;i++){
        for(int j = 0;j < 16;j++){
            ret += dp[i][j];
        }
    }
    return (int)(ret%mod);
}

by lee:
dp[i][k] means after several rolls,
the number of sequence that ends with k times i.

def dieSimulator(self, n, rollMax):
    mod = 10**9 + 7
    dp = [[0, 1] + [0] * 15 for i in xrange(6)]
    for _ in xrange(n - 1):
        dp2 = [[0] * 17 for i in xrange(6)]
        for i in xrange(6):
            for k in xrange(1, rollMax[i] + 1):
                for j in xrange(6):
                    if i == j:
                        if k < rollMax[i]:
                            dp2[j][k + 1] += dp[i][k] % mod
                    else:
                        dp2[j][1] += dp[i][k] % mod
        dp = dp2
    return sum(sum(row) for row in dp) % mod
*/
