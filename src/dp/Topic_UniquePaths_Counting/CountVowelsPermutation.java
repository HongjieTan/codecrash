//Given an integer n, your task is to count how many strings of length n can be formed under the following rules:
//
//
// Each character is a lower case vowel ('a', 'e', 'i', 'o', 'u')
// Each vowel 'a' may only be followed by an 'e'.
// Each vowel 'e' may only be followed by an 'a' or an 'i'.
// Each vowel 'i' may not be followed by another 'i'.
// Each vowel 'o' may only be followed by an 'i' or a 'u'.
// Each vowel 'u' may only be followed by an 'a'.
//
//
// Since the answer may be too large, return it modulo 10^9 + 7.
//
//
// Example 1:
//
//
//Input: n = 1
//Output: 5
//Explanation: All possible strings are: "a", "e", "i" , "o" and "u".
//
//
// Example 2:
//
//
//Input: n = 2
//Output: 10
//Explanation: All possible strings are: "ae", "ea", "ei", "ia", "ie", "io", "iu", "oi", "ou" and "ua".
//
//
// Example 3:
//
//
//Input: n = 5
//Output: 68
//
//
// Constraints:
//
//
// 1 <= n <= 2 * 10^4
//

package dp.Topic_UniquePaths_Counting;


import java.util.Arrays;


public class CountVowelsPermutation {
    public int countVowelPermutation_v2(int n) {
        long mod = 1000000007;
        char[] vowels = {'a', 'e', 'i', 'o', 'u'};
        long[] dp = new long[128];  // dp[i,j] res when len is i and end is j
        Arrays.fill(dp,1);
        for(int i=2;i<=n;i++) {
            long[] ndp = new long[128];
            ndp['a'] = (dp['e'] + dp['i'] + dp['u'])%mod;
            ndp['e'] = (dp['a'] + dp['i'])%mod;
            ndp['i'] = (dp['e'] + dp['o'])%mod;
            ndp['o'] = dp['i'];
            ndp['u'] = (dp['i'] + dp['o'])%mod;
            dp = ndp;
        }

        long ret = 0;
        for(char c: vowels) {
            ret += dp[c];
            ret %= mod;
        }
        return (int)ret;
    }

    // a->e
    // e->a,i
    // i->a,e,o,u
    // o->i,u
    // u->a
    public int countVowelPermutation_v1(int n) {
        long mod = 1000000007;
        char[] vowels = {'a', 'e', 'i', 'o', 'u'};
        long[][] dp = new long[n+1][128]; // dp[i,j]  res when len is i and start is j
        for(int i=1;i<=n;i++) {
            for(char c: vowels) {
                if(i==1) dp[i][c]=1;
                else {
                         if(c=='a') dp[i][c] = dp[i-1]['e'];
                    else if(c=='e') dp[i][c] = dp[i-1]['a'] + dp[i-1]['i'];
                    else if(c=='i') dp[i][c] = dp[i-1]['a'] + dp[i-1]['e'] + dp[i-1]['o'] + dp[i-1]['u'];
                    else if(c=='o') dp[i][c] = dp[i-1]['i'] + dp[i-1]['u'];
                    else if(c=='u') dp[i][c] = dp[i-1]['a'];
                }
                dp[i][c] %= mod;
            }
        }

        long ret=0;
        for(char c: vowels) {
            ret += dp[n][c];
            ret %= mod;
        }
        return (int)ret;
    }

    public static void main(String[] as) {
//        System.out.println(new CountVowelsPermutation().countVowelPermutation(1));
//        System.out.println(new CountVowelsPermutation().countVowelPermutation(2));
//        System.out.println(new CountVowelsPermutation().countVowelPermutation(5));
//        System.out.println(new CountVowelsPermutation().countVowelPermutation(144));
    }
}

/*
by uwi:
class Solution {
    public int countVowelPermutation(int n) {
        long[] dp = new long[5];
        int mod = 1000000007;
        Arrays.fill(dp, 1);
        for(int i = 2;i <= n;i++){
            long[] ndp = new long[5];
            ndp[0] = dp[1]  ;
            ndp[1] = (dp[0] + dp[2]) % mod;
            ndp[2] = (dp[0] + dp[1] + dp[3] + dp[4]) % mod;
            ndp[3] = (dp[2] + dp[4]) % mod;
            ndp[4] = dp[0];
            dp = ndp;
        }
        long ret = 0;
        for(long v : dp)ret += v;
        return (int)(ret%mod);
    }
}

*/