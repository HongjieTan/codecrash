package dp.Topic_UniquePaths_Counting;

public class MinimumPathSum {
    // LC64: Minimum Path Sum: find min cost path from top left to bottom right. You can only move  down or right.
    public int minPathSum(int[][] grid) {
        int[][] dp=new int[grid.length][grid[0].length];
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[0].length; j++) {
                if (i==0 && j==0) dp[i][j]=grid[i][j];
                else {
                    int part1 = i-1>=0?dp[i-1][j]:Integer.MAX_VALUE;
                    int part2 = j-1>=0?dp[i][j-1]:Integer.MAX_VALUE;
                    dp[i][j]=Math.min(part1, part2)+grid[i][j];
                }
            }
        }
        return dp[grid.length-1][grid[0].length-1];
    }

    //  Follow up 1：要求重建从end 到 start的路径：
    //      只需用另一个额外数组记录每一步选择的parent。。。

    //  Follow up 2: 现在要求空间复杂度为O（1），dp且重建路径，空间复杂度不算返回路径时需要的空间:
    //      利用原数组，重建路径时用正负号表示每一步的选择
}
