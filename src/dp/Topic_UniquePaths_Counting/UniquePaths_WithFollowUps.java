package dp.Topic_UniquePaths_Counting;

import java.util.*;

/**
     https://www.1point3acres.com/bbs/thread-423857-1-1.html
     给定一个矩形的宽和长，求所有可能的路径数量
     Rules：
     1. 从左上角走到右上角
     2. 要求每一步只能向正右、右上或右下走，即 →↗↘
     followup1: 优化空间复杂度至 O(n)
     followup2: 给定矩形里的三个点，判断是否存在遍历这三个点的路经
     followup3: 给定矩形里的三个点，找到遍历这三个点的所有路径数量
     followup4: 给定一个下界 (x == H)，找到能经过给定下界的所有路径数量 (x >= H)
     followup5: 起点和终点改成从左上到左下，每一步只能 ↓↘↙，求所有可能的路径数量
 */
public class UniquePaths_WithFollowUps {

    // base case: 从左上角走到右上角，要求每一步只能向正右、右上或右下走，即 →↗↘
    // 遍历顺序需要换一下，因为每个格子只能从左方，左上，左下过来，也就是必须把 dp[x][j - 1] 的所有格子算好，才能继续算 dp[x][j]
    public int numOfUniquePaths(int rows, int cols) {
        int[][] dp = new int[rows][cols];
        for (int col = 0; col < cols; col++) {
            for (int row = 0; row < rows; row++) {
                if (col==0) dp[row][col]=row==0?1:0;
                else {
                    int part1 = row-1>=0?dp[row-1][col-1]:0;
                    int part2 = dp[row][col-1];
                    int part3 = row+1<rows?dp[row+1][col-1]:0;
                    dp[row][col] = part1+part2+part3;
                }
            }
        }
        return dp[0][cols-1];
    }

    // followup1: 优化空间复杂度至 O(n)
    //  用2个数组来滚动
    public int numOfUniquePaths_followup1(int rows, int cols) {
        int[] dp = new int[rows];
        int[] temp = new int[rows];
        dp[0]=1;
        for (int col = 1; col < cols; col++) {
            for (int row = 0; row < rows; row++) {
                int part1 = row-1>=0?dp[row-1]:0;
                int part2 = dp[row];
                int part3 = row+1<rows?dp[row+1]:0;
                temp[row] = part1+part2+part3;
            }
            System.arraycopy(temp, 0, dp,0,temp.length);
        }
        return dp[0];
    }

    // followup2: 给定矩形里的三个点，判断是否存在遍历这三个点的路经
    //    考虑两个格子 (x, y) 和 (i, j)，当 (x, y) 能走到 (i, j) 的时候，(x, y) 会位于 (i, j) 向后张的扇形面积里面
    public boolean numOfUniquePaths_followup2(int rows, int cols, int[][] points) {
        List<int[]> allPoints = new ArrayList<>();
        for(int[] p:points) allPoints.add(p); //
        allPoints.add(new int[]{0,0});
        allPoints.add(new int[]{0,cols-1});
        Collections.sort(allPoints, (a,b)->a[1]-b[1]); // sort by column
        for(int i=1;i<allPoints.size();i++) {
            int[] prev=allPoints.get(i-1);
            int[] cur=allPoints.get(i);
            int dy=cur[1]-prev[1];
            if(cur[0]>prev[0]+dy || cur[0]<prev[0]-dy) return false;
        }
        return true;
    }

    // followup3: 给定矩形里的三个点，找到遍历这三个点的所有路径数量
    //   - 解法1：感觉没有什么特殊的，唯一的区别就是在算完 y - 1 之后，要进 y 之前，把非必经点归零
    //   - 解法2：或者用三个点切割，每块分别计算，再相乘, 类似count(start, p1) * count(p1, p2) * ... * count(p_{k - 1}, p_k)这种.
    public int numOfUniquePaths_followup3(int rows, int cols, int[][] points) {
        int[][] dp=new int[rows][cols];
        Map<Integer, Integer> pointMap = new HashMap<>();
        for (int[] p:points) pointMap.put(p[1],p[0]);

        dp[0][0]=1;
        for (int j = 1; j < cols; j++) {
            for (int i = 0; i < rows; i++) {
                int part1 = i-1>=0?dp[i-1][j-1]:0;
                int part2 = dp[i][j-1];
                int part3 = i+1<rows?dp[i+1][j-1]:0;
                dp[i][j]=part1+part2+part3;
                if(pointMap.containsKey(j) && i!=pointMap.get(j)) dp[i][j]=0;
            }
        }
        return dp[0][cols-1];
    }

    // followup4: 给定一个下界 (x == H)，找到能经过给定下界的所有路径数量 (x >= H)
    //  2遍dp...
    public int numOfUniquePaths_followup4(int rows, int cols, int h) {
        int[][] dp=new int[rows][cols];
        dp[0][0]=1;
        for (int j = 1; j < cols; j++) {
            for (int i = 0; i < rows; i++) {
                int part1 = i-1>=0?dp[i-1][j-1]:0;
                int part2 = dp[i][j-1];
                int part3 = i+1<rows?dp[i+1][j-1]:0;
                dp[i][j]=part1+part2+part3;
            }
        }
        int allPaths =  dp[0][cols-1];

        Arrays.fill(dp[0],0);
        dp[0][0]=1;
        for (int j = 1; j < cols; j++) {
            for (int i = 0; i < rows; i++) {
                int part1 = i-1>=0?dp[i-1][j-1]:0;
                int part2 = dp[i][j-1];
                int part3 = i+1<rows?dp[i+1][j-1]:0;
                dp[i][j]=part1+part2+part3;
                if (i>=h) dp[i][j]=0;
            }
        }
        return allPaths-dp[0][cols-1];
    }

    // followup5: 起点和终点改成从左上到左下，每一步只能 ↓↘↙，求所有可能的路径数量
    public int numOfUniquePaths_followup5(int rows, int cols) {
        int[][] dp = new int[rows][cols];
        dp[0][0]=1;
        for (int i = 1; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                int part1 = j-1>=0?dp[i-1][j-1]:0;
                int part2 = dp[i-1][j];
                int part3 = j+1<cols?dp[i-1][j+1]:0;
                dp[i][j]=part1+part2+part3;
            }
        }
        return dp[rows-1][0];
    }

    // LC 62
    public int uniquePaths(int m, int n) {
        int[][] dp = new int[m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (i == 0 || j == 0) dp[i][j] = 1;
                else dp[i][j] = dp[i - 1][j] + dp[i][j - 1];
            }
        }
        return dp[m - 1][n - 1];
    }

    // LC 62 followup: 带空间优化, 用两个数组滚动dp
    public int uniquePaths_space_improve(int rows, int cols) {
        int[] dp = new int[rows];
        int[] temp = new int[rows];
        for (int col = 0; col < cols; col++) {
            for (int row = 0; row < rows; row++) {
                if (row == 0 && col == 0) temp[row] = 1;
                else {
                    int part1 = row - 1 >= 0 ? temp[row - 1] : 0;
                    int part2 = col - 1 >= 0 ? dp[row] : 0;
                    temp[row] = part1 + part2;
                }

            }
            System.arraycopy(temp, 0, dp, 0, temp.length);
        }
        return dp[rows - 1];
    }

    // LC 63: with obstacles
    public int uniquePathsWithObstacles(int[][] obstacleGrid) {
        int m = obstacleGrid.length, n = obstacleGrid[0].length;
        int[][] dp = new int[m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (obstacleGrid[i][j] == 1) dp[i][j] = 0;
                else if (i == 0 && j == 0) dp[i][j] = 1;
                else {
                    int part1 = i - 1 >= 0 ? dp[i - 1][j] : 0;
                    int part2 = j - 1 >= 0 ? dp[i][j - 1] : 0;
                    dp[i][j] = part1 + part2;
                }
            }
        }
        return dp[m - 1][n - 1];
    }


    public static void main(String[] args) {
        UniquePaths_WithFollowUps solution = new UniquePaths_WithFollowUps();

        // base cas3, followup1
//    ((2, 2), 1), ((2, 3), 2), ((3, 3), 2), ((5, 5), 9), ((7, 6), 21), ((6, 7), 51),
//        System.out.println(solution.numOfUniquePaths_followup1(2,2));
//        System.out.println(solution.numOfUniquePaths_followup1(2,3));
//        System.out.println(solution.numOfUniquePaths_followup1(3,3));
//        System.out.println(solution.numOfUniquePaths_followup1(5,5));
//        System.out.println(solution.numOfUniquePaths_followup1(7,6));
//        System.out.println(solution.numOfUniquePaths_followup1(6,7));


        // followup2
        // ((2, 3, [[1, 0], [1, 1], [1, 2]]), False),
        // ((3, 3, [[1, 0], [2, 1], [1, 2]]), False),
        // ((5, 5, [[0, 1], [2, 2], [1, 3]]), False),
        // ((5, 5, [[1, 1], [2, 2], [1, 3]]), True),
        // ((5, 5, [[2, 2], [1, 1], [1, 3]]), True),
        // ((6, 8, [[0, 0], [4, 3], [0, 7]]), False),
        // ((8, 6, [[1, 1], [0, 4], [1, 3]]), True),
        // ((8, 6, [[1, 1], [0, 4], [2, 3]]), False),
//        System.out.println(solution.numOfUniquePaths_followup2(2, 3, new int[][]{{1,0},{1,1},{1,2}}));
//        System.out.println(solution.numOfUniquePaths_followup2(3, 3, new int[][]{{1,0},{2,1},{1,2}}));
//        System.out.println(solution.numOfUniquePaths_followup2(5, 5, new int[][]{{0,1},{2,2},{1,3}}));
//        System.out.println(solution.numOfUniquePaths_followup2(5, 5, new int[][]{{1,1},{2,2},{1,3}}));
//        System.out.println(solution.numOfUniquePaths_followup2(5, 5, new int[][]{{2,2},{1,1},{1,3}}));
//        System.out.println(solution.numOfUniquePaths_followup2(6, 8, new int[][]{{0,0},{4,3},{0,7}}));
//        System.out.println(solution.numOfUniquePaths_followup2(8, 6, new int[][]{{1,1},{0,4},{1,3}}));
//        System.out.println(solution.numOfUniquePaths_followup2(8, 6, new int[][]{{1,1},{0,4},{2,3}}));

        // followup3
        // ((2, 3, [[1, 0], [1, 1], [1, 2]]), 0),
        // ((3, 3, [[1, 0], [2, 1], [1, 2]]), 0),
        // ((5, 5, [[0, 1], [2, 2], [1, 3]]), 0),
        // ((5, 5, [[1, 1], [2, 2], [1, 3]]), 1),
        // ((5, 5, [[2, 2], [1, 1], [1, 3]]), 1),
        // ((6, 8, [[0, 0], [4, 3], [0, 7]]), 0),
        // ((8, 6, [[0, 0], [0, 5], [0, 4]]), 9),
        // ((8, 6, [[1, 1], [0, 4], [2, 3]]), 0),
//        System.out.println(solution.numOfUniquePaths_followup3(2, 3, new int[][]{{1,0},{1,1},{1,2}}));
//        System.out.println(solution.numOfUniquePaths_followup3(3, 3, new int[][]{{1,0},{2,1},{1,2}}));
//        System.out.println(solution.numOfUniquePaths_followup3(5, 5, new int[][]{{0,1},{2,2},{1,3}}));
//        System.out.println(solution.numOfUniquePaths_followup3(5, 5, new int[][]{{1,1},{2,2},{1,3}}));
//        System.out.println(solution.numOfUniquePaths_followup3(5, 5, new int[][]{{2,2},{1,1},{1,3}}));
//        System.out.println(solution.numOfUniquePaths_followup3(6, 8, new int[][]{{0,0},{4,3},{0,7}}));
//        System.out.println(solution.numOfUniquePaths_followup3(8, 6, new int[][]{{0,0},{0,5},{0,4}}));
//        System.out.println(solution.numOfUniquePaths_followup3(8, 6, new int[][]{{1,1},{0,4},{2,3}}));

        // followup4
        // ((2, 3, 1), 1), ((3, 3, 1), 1), ((3, 3, 2), 0), ((4, 4, 0), 4), ((4, 4, 1), 3), ((4, 4, 2), 0),
        // ((6, 7, 0), 51), ((6, 7, 1), 50), ((6, 7, 2), 19), ((6, 7, 3), 1), ((6, 7, 4), 0), ((6, 7, 5), 0)
//        System.out.println(solution.numOfUniquePaths_followup4(2, 3, 1)); // 1
//        System.out.println(solution.numOfUniquePaths_followup4(3, 3, 1)); // 1
//        System.out.println(solution.numOfUniquePaths_followup4(3, 3, 2)); // 0
//        System.out.println(solution.numOfUniquePaths_followup4(4, 4, 0)); // 4
//        System.out.println(solution.numOfUniquePaths_followup4(4, 4, 1)); // 3
//        System.out.println(solution.numOfUniquePaths_followup4(4, 4, 2)); // 0
//        System.out.println(solution.numOfUniquePaths_followup4(6, 7, 0)); // 51
//        System.out.println(solution.numOfUniquePaths_followup4(6, 7, 1)); // 50
//        System.out.println(solution.numOfUniquePaths_followup4(6, 7, 2)); // 19
//        System.out.println(solution.numOfUniquePaths_followup4(6, 7, 3)); // 1
//        System.out.println(solution.numOfUniquePaths_followup4(6, 7, 4)); // 0
//        System.out.println(solution.numOfUniquePaths_followup4(6, 7, 5)); // 0

        // followup5
        //((2, 2), 1), ((2, 3), 1), ((3, 3), 2),
        //((5, 5), 9), ((7, 6), 51), ((6, 7), 21),
        System.out.println(solution.numOfUniquePaths_followup5(2, 2)); // 1
        System.out.println(solution.numOfUniquePaths_followup5(2, 3)); // 1
        System.out.println(solution.numOfUniquePaths_followup5(3, 3)); // 2
        System.out.println(solution.numOfUniquePaths_followup5(5, 5)); // 9
        System.out.println(solution.numOfUniquePaths_followup5(7, 6)); // 51
        System.out.println(solution.numOfUniquePaths_followup5(6, 7)); // 21



    }
}