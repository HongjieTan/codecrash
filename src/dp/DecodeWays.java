package dp;

/**
 * Created by thj on 2018/7/10.
 *
 *  careful about the processing of digit "0"
 */
public class DecodeWays {

    /*
     dp[i]: decodeways of s0,s2,...,si

     dp[i]:  i==0,
                   si==0: dp[i]=0
                   si!=0: dp[i]=1
             i>0,
                   consider si only:
                        si!=0: dp[i] += dp[i-1]
                        si==0: dp[i] += 0
                   consider si-1,si:
                        si-1si==10~26: dp[i]+= dp[i-2]
                        else: dp[i] +=0
     */
    public int numDecodingsX2(String s) {
        int[] dp = new int[s.length()];
        dp[0] = s.charAt(0)=='0'?0:1;

        for(int i=1; i<s.length(); i++) {
            // si only
            if(s.charAt(i)!='0') dp[i] += dp[i-1];

            // si-1, si
            String numStr = s.charAt(i-1)+""+s.charAt(i);
            int num = Integer.valueOf(numStr);
            if(num>=10 && num<=26) dp[i] += (i>=2?dp[i-2]:1);
        }

        return dp[s.length()-1];
    }



    public int numDecodings(String s) {
        int dp[] = new int[s.length()]; // dpi: num of decodings of  "s0 s1 ... si"
        dp[0] = s.charAt(0)-'0'==0?0:1;
        for (int i=1;i<s.length();i++) {
            // case 1: si only
            dp[i] = s.charAt(i)-'0'==0?0:dp[i-1];

            // case 2: si-1, si together
            int cur_digit = s.charAt(i)-'0';
            int pre_digit = s.charAt(i-1)-'0';
            if (pre_digit == 1 || (pre_digit==2 && cur_digit<=6)) {
                dp[i] += (i>1?dp[i-2]:1);
            }
        }
        return dp[s.length()-1];
    }


    // TODO ???
    // https://leetcode.com/problems/decode-ways-ii/description/
    // s can contain "*" which can be treated as one of the numbers from 1 to 9.
    public int numDecodings2(String s) {

        int dp[] = new int[s.length()];


        dp[0] = getSingleDecodes(s.charAt(0));

        for (int i=1; i<s.length(); i++) {
            char preDigit = s.charAt(i-1);
            char curDigit = s.charAt(i);

            dp[i] = 0;
            // si only
            dp[i] += getSingleDecodes(curDigit) * dp[i-1];

            // si-1 si together
            if ( preDigit == '*') {
                if (curDigit == '*') dp[i] += 96* (i>1?dp[i-2]:1);
                else if (curDigit-'0' >6) dp[i] += 9 * (i>1?dp[i-2]:1);
                else dp[i] += 2 * (i>1?dp[i-2]:1);
            }
            else if (preDigit=='1') dp[i] += (curDigit=='*'? 9:1) * (i>1?dp[i-2]:1);
            else if (preDigit=='2') {
                if (curDigit == '*') dp[i] += 9*(i>1?dp[i-2]:1);
                else if (curDigit-'0'<=6) dp[i] += (i>1?dp[i-2]:1);
            }
            else dp[i] += 0;
        }

        return dp[s.length()-1];
    }

    private int getSingleDecodes(char c) {
        int res=0;
        if (c == '0' ) res = 0;
        else if (c == '*') res = 9;
        else res = 1;
        return res;
    }


    public static void main(String[] args) {
        System.out.println(new DecodeWays().numDecodings2("**"));
//        System.out.println(new DecodeWays().numDecodings2("1*"));
//        System.out.println(new DecodeWays().numDecodings("226"));
    }
}
