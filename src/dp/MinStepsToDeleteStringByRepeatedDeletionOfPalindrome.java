// Given an integer array arr, in one move you can select a palindromic subarray arr[i], arr[i+1], ..., arr[j] where i <= j,
// and remove that subarray from the given array. Note that after removing a subarray, the elements on the left and on the right of that subarray move to fill the gap left by the removal.
//
// Return the minimum number of moves needed to remove all numbers from the array.
//
//
// Example 1:
//
//
// Input: arr = [1,2]
// Output: 2
//
//
// Example 2:
//
//
// Input: arr = [1,3,4,1,5]
// Output: 3
// Explanation: Remove [4] then remove [1,3,1] then remove [5].
//
//
//
// Constraints:
//
//
// 1 <= arr.length <= 100
// 1 <= arr[i] <= 20
//
//

package dp;



// lc 1246
public class MinStepsToDeleteStringByRepeatedDeletionOfPalindrome {
    // O(n^3)
    // dp[i,j] = min(1+dp[i+1,j],  1+dp[i+2,j] where arr[i]==arr[i+1], dp[i+1,k-1] + dp[k+1][j] where arr[i]==arr[k])
    public int minimumMoves(int[] arr) {
        int n = arr.length;
        int[][] dp = new int[n+1][n+1];
        for(int len=1;len<=n;len++) {
            for(int i=0,j=len-1;j<n;i++,j++) {
                if(len==1) dp[i][j]=1;
                else {
                    // case1. remove as single
                    dp[i][j] = 1+dp[i+1][j];
                    // case2. remove as pair
                    if (arr[i]==arr[i+1]) dp[i][j] = Math.min(dp[i][j], 1+ dp[i+2][j]); // special case!
                    for (int k=i+2;k<=j;k++) {
                        if (arr[i]==arr[k]) {
                            dp[i][j] = Math.min(dp[i][j], dp[i+1][k-1] + dp[k+1][j]);
                        }
                    }
                }
            }
        }
        return dp[0][n-1];
    }

    public static void main(String[] as) {
        System.out.println(new MinStepsToDeleteStringByRepeatedDeletionOfPalindrome().minimumMoves(new int[]{1,1}));
        System.out.println(new MinStepsToDeleteStringByRepeatedDeletionOfPalindrome().minimumMoves(new int[]{1,2}));
        System.out.println(new MinStepsToDeleteStringByRepeatedDeletionOfPalindrome().minimumMoves(new int[]{1,3,4,1,5}));
        System.out.println(new MinStepsToDeleteStringByRepeatedDeletionOfPalindrome().minimumMoves(new int[]{2,5,5,3,4,3,2}));
    }

}
