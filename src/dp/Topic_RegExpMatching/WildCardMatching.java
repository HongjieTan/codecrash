package dp.Topic_RegExpMatching;


/*

'?' Matches any single character.
'*' Matches any sequence of characters (including the empty sequence).


dp[i][j]    s[0...i-1], p[0...j-1]


i==0 && j==0:   true

i==0 && j >0:
            p[j-1]=='*':  dp[i][j-1]
            p[j-1]!='*':  false

i >0 && j==0:   false

i >0 && j >0:
            s[i-1]==p[j-1] or p[j-1]=='?':  dp[i-1][j-1]
            p[j-1]=='*':    dp[i-1][j] or dp[i][j-1]
            else:   false

 */


public class WildCardMatching {
    public boolean isMatch(String s, String p) {
        boolean[][] dp = new boolean[s.length()+1][p.length()+1];
        for (int i = 0; i < s.length()+1; i++) {
            for (int j = 0; j < p.length() + 1; j++) {
                if (i==0 && j==0) dp[i][j] = true;
                else if (i==0) dp[i][j] = p.charAt(j-1)=='*'?dp[i][j-1]:false;
                else if (j==0) dp[i][j] = false;
                else {
                    if (s.charAt(i-1)==p.charAt(j-1) || p.charAt(j-1)=='?') dp[i][j] = dp[i-1][j-1];
                    else if (p.charAt(j-1)=='*') dp[i][j] = dp[i-1][j] || dp[i][j-1];
                    else dp[i][j] = false;
                }
            }
        }

        return dp[s.length()][p.length()];
    }
}
