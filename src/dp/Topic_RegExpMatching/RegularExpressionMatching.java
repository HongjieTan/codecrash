package dp.Topic_RegExpMatching;

/**

 dp[i][j]:  s[0,...i-1]  p[0,...j-1]

 i==0 && j==0:  true

 i==0 && j>0:
            p[j-1] == '*', dp[i][j-2]
            p[j-1] != '*', false

 i>0 && j==0: false

 i>0 && j>0:
            p[j-1] == s[i-1] or  p[j-1]=='.':  dp[i-1][j-1]
            p[j-1]=='*',
                p[j-2] == s[i-1] or  p[j-2]=='.': dp[i-1][j] || dp[i][j-2]   (notice!!!! don't forget: 1. p[j-2]=='.' , 2. dp[i][j-2])
                p[j-2] != s[i-1]:  dp[i][j-2]

 */
public class RegularExpressionMatching {
    /*
        p[j-1] == s[i-1]: dp[i,j] = dp[i-1][j-1]
        p[j-1] == '.'	: dp[i,j] = dp[i-1][j-1]
        p[j-1] == '*'	:
            p[j-2] == s[i-1]: dp[i,j] = dp[i][j-2] || dp[i-1][j]
            p[j-2] == 	 '.': dp[i,j] = dp[i][j-2] || dp[i-1][j]
            p[j-2] != s[i-1]: dp[i,j] = dp[i][j-2]
    */
    public boolean isMatch(String ss, String pp) {
        int n=ss.length(), m=pp.length();
        char[] s = ss.toCharArray(), p = pp.toCharArray();
        boolean[][] dp = new boolean[n+1][m+1];
        for(int i=0;i<=n;i++) {
            for(int j=0;j<=m;j++) {
                if(i==0 && j==0) dp[i][j] = true;
                else if(j==0) dp[i][j] = false;
                else if(i==0) dp[i][j] = j>=2 && p[j-1]=='*' && dp[i][j-2];
                else {
                    if(p[j-1] == s[i-1]) dp[i][j] = dp[i-1][j-1];
                    else if(p[j-1] == '.') dp[i][j] = dp[i-1][j-1];
                    else if(p[j-1] == '*') {
                        if(p[j-2] == s[i-1]) dp[i][j] = dp[i][j-2] || dp[i-1][j];
                        else if(p[j-2] == 	 '.') dp[i][j] = dp[i][j-2] || dp[i-1][j];
                        else if(p[j-2] != s[i-1]) dp[i][j] = dp[i][j-2];
                    }
                }
            }
        }
        return dp[n][m];
    }


//    public boolean isMatch(String s, String p) {
//        boolean[][] dp = new boolean[s.length()+1][p.length()+1];
//        for (int i = 0; i < s.length() + 1; i++) {
//            for (int j = 0; j < p.length() + 1; j++) {
//                if (i==0 && j==0) {
//                    dp[i][j]=true;
//                } else if (i==0 && j>0) {
//                    dp[i][j] = p.charAt(j-1)=='*'?dp[i][j-2]:false;
//                } else if (i>0 && j==0) {
//                    dp[i][j] = false;
//                } else {
//                    if (p.charAt(j-1) == s.charAt(i-1) || p.charAt(j-1) == '.') {
//                        dp[i][j] = dp[i-1][j-1];
//                    } else if (p.charAt(j-1)=='*') {
//                        dp[i][j] = (p.charAt(j-2)==s.charAt(i-1) ||  p.charAt(j-2)=='.') ? (dp[i-1][j]||dp[i][j-2]) : dp[i][j-2];
//                    } else {
//                        dp[i][j] = false;
//                    }
//                }
//            }
//        }
//        return dp[s.length()][p.length()];
//    }

    /**
     * f(i,j):
     *   i=0:  j==0||(j>1&&pj-1='*' && f(i,j-2))
     *   j=0:  i==0,
     *   i>0,j>0:
     *              singleMatch(si-1, pj-1) : f(i-1, j-1)
     *              pj-1=='*' && singleMatch(pj-2, si-1) : f(i-1, j)||f(i,j-2)
     *              pj-1=='*' && !singleMatch(pj-2, si-1) : f(i,j-2)
     *              else: false
     */
    public boolean isMatch_v1(String s, String p) {
        boolean[][] dp = new boolean[s.length()+1][p.length()+1];
        for (int i = 0; i <= s.length(); i++) {
            for (int j = 0; j <= p.length(); j++) {
                if (i==0) dp[i][j] = j==0||(j>1&&p.charAt(j-1)=='*'&&dp[i][j-2]);
                else if (j==0) dp[i][j] = false; //i>0
                else { //i>0,j>0
                    if (singleMatch(s.charAt(i-1), p.charAt(j-1))) dp[i][j] = dp[i-1][j-1];
                    else if (p.charAt(j-1)=='*'&& singleMatch(s.charAt(i-1), p.charAt(j-2))) dp[i][j] = dp[i-1][j] || dp[i][j-2];
                    else if (p.charAt(j-1)=='*'&& !singleMatch(s.charAt(i-1), p.charAt(j-2))) dp[i][j] = dp[i][j-2];
                    else dp[i][j] = false;
                }
            }
        }
        return dp[s.length()][p.length()];
    }

    private boolean singleMatch(char sc, char pc) {
        return sc==pc || pc =='.';
    }

    public static void main(String[] args) {
        System.out.println(new RegularExpressionMatching().isMatch("aa","a")); // false
        System.out.println(new RegularExpressionMatching().isMatch("aa","a*")); // true
        System.out.println(new RegularExpressionMatching().isMatch("ab",".*")); // true
        System.out.println(new RegularExpressionMatching().isMatch("aab","c*a*b")); // true
        System.out.println(new RegularExpressionMatching().isMatch("mississippi","mis*is*p*.")); // false
    }

}
// 输入:
//s = "aa"
//p = "a"
//输出: false
//解释: "a" 无法匹配 "aa" 整个字符串。
//
//
// 示例 2:
//
// 输入:
//s = "aa"
//p = "a*"
//输出: true
//解释: 因为 '*' 代表可以匹配零个或多个前面的那一个元素, 在这里前面的元素就是 'a'。因此，字符串 "aa" 可被视为 'a' 重复了一次。
//
//
// 示例 3:
//
// 输入:
//s = "ab"
//p = ".*"
//输出: true
//解释: ".*" 表示可匹配零个或多个（'*'）任意字符（'.'）。
//
//
// 示例 4:
//
// 输入:
//s = "aab"
//p = "c*a*b"
//输出: true
//解释: 因为 '*' 表示零个或多个，这里 'c' 为 0 个, 'a' 被重复一次。因此可以匹配字符串 "aab"。
//
//
// 示例 5:
//
// 输入:
//s = "mississippi"
//p = "mis*is*p*."
//输出: false
// Related Topics 字符串 动态规划 回溯算法
