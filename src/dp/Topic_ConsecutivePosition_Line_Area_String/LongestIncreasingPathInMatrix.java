package dp.Topic_ConsecutivePosition_Line_Area_String;


/**
 *
 *  Notice the difference between LongestIncreasingPathInMatrix vs LongestLineOfConsecutiveOneInMatrix!!!
 *  One thinking: 构建dp需要某种偏序关系（子解->解）,LongestIncreasingPathInMatrix存在偏序关系，LongestLineOfConsecutiveOneInMatrix需要拆分成四个方向做DP...
 *
 *  dfs/bfs with memo!! (top-down dp)
 *   - the right version!!!: O(m*n) ,  no duplication!!  每个子问题都是最优解，不会导致重复计算！
 *   - my naive version:  O((m*n)^2),  skip shorter path, but it may still cause duplication!
 *
 */
public class LongestIncreasingPathInMatrix {

    // O(m*n)!
    public int longestIncreasingPath(int[][] matrix ){
        if (matrix.length==0) return 0;
        int m=matrix.length;
        int n=matrix[0].length;
        int[][] memo = new int[m][n]; // memo[i][j]: max path starting from (i,j)
        int ans = 0;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                int len = dfs(matrix, i, j, memo);
                ans = Math.max(ans, len);
            }
        }
        return ans;
    }

    final static int[][] dirs = {{0, 1}, {1, 0}, {0, -1}, {-1, 0}};
    int dfs(int[][] matrix, int x, int y, int[][] memo) {
        if (memo[x][y]!=0) return memo[x][y];
        int maxLen=1;
        for (int[] dir: dirs) {
            int nx=x+dir[0], ny=y+dir[1];
            if (nx>=0&&nx<matrix.length&&ny>=0&&ny<matrix[0].length&&matrix[nx][ny]>matrix[x][y]) { // As matrix[nx][ny]>matrix[x][y], there is no cycle visit, so no need 'visited'
                maxLen = Math.max(maxLen, 1+dfs(matrix, nx, ny, memo));
            }
        }
        memo[x][y]=maxLen;
        return maxLen;
    }

    // the naive version:  O((m*n)^2), not goof! skip shorter path, but it may still cause duplication!!
//    public int longestIncreasingPath_dfs_with_cache(int[][] matrix) {
//        if (matrix.length==0) return 0;
//        int m=matrix.length;
//        int n=matrix[0].length;
//        int[][] memo = new int[m][n];
//        for(int i=0;i<m;i++){
//            for (int j=0;j<n;j++){
//                dfs(matrix, i, j, 1, memo);
//            }
//        }
//        int max = 1;
//        for(int i=0;i<m;i++) for(int j=0;j<n;j++)max=Math.max(memo[i][j], max);
//        return max;
//    }
//
//    void dfs(int[][] matrix, int x, int y, int depth, int[][] memo) {
//        if (depth<=memo[x][y]) return;
//        memo[x][y]=depth;
//        int m=matrix.length;
//        int n=matrix[0].length;
//        int[][] directions = new int[][]{{-1,0},{1,0},{0,-1},{0,1}};
//        for(int[] dir: directions) {
//            int nx=x+dir[0];
//            int ny=y+dir[1];
//            if(nx>=0&&nx<m&&ny>=0&&ny<n&&matrix[nx][ny]>matrix[x][y]) {
//                dfs(matrix, nx, ny, depth+1, memo);
//            }
//        }
//    }

    public static void main(String[] args) {
        int[][] matrix = new int[][]{
//                {9,9,4},
//                {6,6,8},
//                {2,1,1}
                {3,4,5},
                {3,2,6},
                {2,2,1}
        };
        System.out.println(new LongestIncreasingPathInMatrix().longestIncreasingPath(matrix));
//        System.out.println(new dp.Topic_ConsecutivePosition_Line_Area_String.LongestIncreasingPathInMatrix().longestIncreasingPath_dfs_with_cache(matrix));
    }
}

/*
Given an integer matrix, find the length of the longest increasing path.
From each cell, you can either move to four directions: left, right, up or down.
You may NOT move diagonally or move outside of the boundary (i.e. wrap-around is not allowed).
Example 1:
Input: nums =
[
  [9,9,4],
  [6,6,8],
  [2,1,1]
]
Output: 4
Explanation: The longest increasing path is [1, 2, 6, 9].
Example 2:
Input: nums =
[
  [3,4,5],
  [3,2,6],
  [2,2,1]
]
Output: 4
Explanation: The longest increasing path is [3, 4, 5, 6]. Moving diagonally is not allowed.
*/
/*
To get max length of increasing sequences:

Do DFS from every cell
Compare every 4 direction and skip cells that are out of boundary or smaller
Get matrix max from every cell's max
Use matrix[x][y] <= matrix[i][j] so we don't need a visited[m][n] array
The key is to cache the distance because it's highly possible to revisit a cell
Hope it helps!

public static final int[][] dirs = {{0, 1}, {1, 0}, {0, -1}, {-1, 0}};

public int longestIncreasingPath(int[][] matrix) {
    if(matrix.length == 0) return 0;
    int m = matrix.length, n = matrix[0].length;
    int[][] cache = new int[m][n];
    int max = 1;
    for(int i = 0; i < m; i++) {
        for(int j = 0; j < n; j++) {
            int len = dfs(matrix, i, j, m, n, cache);
            max = Math.max(max, len);
        }
    }
    return max;
}

public int dfs(int[][] matrix, int i, int j, int m, int n, int[][] cache) {
    if(cache[i][j] != 0) return cache[i][j];
    int max = 1;
    for(int[] dir: dirs) {
        int x = i + dir[0], y = j + dir[1];
        if(x < 0 || x >= m || y < 0 || y >= n || matrix[x][y] <= matrix[i][j]) continue;
        int len = 1 + dfs(matrix, x, y, m, n, cache);
        max = Math.max(max, len);
    }
    cache[i][j] = max;
    return max;
}
*/
/*
topo sort version ???
class Solution:
    def longestIncreasingPath(self, matrix):
        # idea, use topological sort, search around to find the # of incoming nodes, start with zero indegree with queue, pop from queue, search around and reduce the indegree by 1; push to queue if indegree is 0. output the steps. Time O(mn) and Space O(mn).
        if not matrix:
            return 0
        m, n = len(matrix), len(matrix[0])
        directions = [(1,0), (-1,0), (0,1), (0,-1)]
        hmap = {}
        queue = collections.deque()
        for i in range(m):
            for j in range(n):
                cnt = 0
                for dx, dy in directions:
                    x = i + dx
                    y = j + dy
                    if 0 <= x <= m-1 and 0 <= y <= n-1 and matrix[x][y] < matrix[i][j]:
                        cnt += 1
                hmap[(i, j)] = cnt # map point to the # of incoming degree
                if cnt == 0:
                    queue.append((i, j)) # append point (i,j) to queue
        # bfs with queue, and update the step until queue is empty
        step = 0
        while queue:
            size = len(queue)
            for k in range(size):
                i, j = queue.popleft()
                for dx, dy in directions:
                    x = i + dx
                    y = j + dy
                    if 0 <= x <= m-1 and 0 <= y <= n-1 and matrix[x][y] > matrix[i][j] and (x, y) in hmap:
                        hmap[(x, y)] -= 1
                        if hmap[(x, y)] == 0:
                            queue.append((x, y))
            step += 1
        return step
*/
