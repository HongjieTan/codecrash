package dp.Topic_ConsecutivePosition_Line_Area_String;

/**
 * Created by thj on 31/05/2018.
 *
 *  1. brute force: O((mn)^2)
 *  2. dp: O(mn)
 *      dp(i,j) = min(dp(i-1,j-1), dp(i-1,j), dp(i,j-1)) +1
 *      dp(i,j) mean max square side length whose rightbottom point is (i,j)
 *
 */
public class MaxSquare {

    public int maximalSquare(char[][] matrix) {
        int maxLen = 0;
        int m = matrix.length;
        int n = m!=0?matrix[0].length:0;
        int[][] dp = new int[m][n];

        for (int i=0;i<m;i++) {
            for (int j=0;j<n;j++) {
                if ((i==0||j==0)) {
                    dp[i][j]= matrix[i][j]=='1'?1:0;
                } else if (matrix[i][j] == '1') {
                    dp[i][j] = Math.min(dp[i-1][j-1], Math.min(dp[i-1][j], dp[i][j-1])) +1;
                }
                maxLen = Math.max(dp[i][j], maxLen);
            }
        }
        return maxLen*maxLen;
    }

}
