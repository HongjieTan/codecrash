package dp.Topic_ConsecutivePosition_Line_Area_String;

/**
 * Created by thj on 2018/8/2.
 *
 *  1. dp  O(m*n)
 *
 *   dp[i][j] means length of common string with A[i-1] and B[j-1] as suffix
 *
 *   i==0 || j==0 :    dp[i][j] = 0
 *   other :
 *       if A[i-1] == B[j-1]:  dp[i][j] = dp[i-1][j-1]+1
 *       if        !=       :  dp[i][j] = 0
 *
 *
 *
 *  2. TODO The longest substring can also be solved in O(n+m) time using Suffix Tree.
 */
public class LongestCommonSubstring {

    public int longestCommonSubstring(String A, String B) {
        // write your code here
        int m=A.length(), n=B.length();

        int[][] dp = new int[m+1][n+1];
        int max = 0;

        for (int i = 0; i <= m; i++) {
            for (int j = 0; j <= n; j++) {
                if (i==0 || j==0) {
                    dp[i][j] =0;
                } else if (A.charAt(i-1) == B.charAt(j-1)) {
                    dp[i][j] = dp[i-1][j-1]+1;
                } else {
                    dp[i][j] = 0;
                }

                max = Math.max(max, dp[i][j]);
            }
        }


        return max;
    }
}
