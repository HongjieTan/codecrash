package dp;

import java.util.HashMap;
import java.util.Map;

public class UncrossedLines {

    // just LCS !!
    public int maxUncrossedLines(int[] A, int[] B) {
        int m=A.length, n=B.length, dp[][] = new int[m+1][n+1];
        for(int i=1;i<m+1;i++) {
            for(int j=1;j<n+1;j++) {
                if(A[i-1]==B[j-1]) dp[i][j]=dp[i-1][j-1]+1;
                else dp[i][j] = Math.max(dp[i][j-1], dp[i-1][j]);
            }
        }
        return dp[m][n];
    }

    // my first version...
    //    dp[i,j]: max lines of A[0...i-1], B[0...j-1]
    //    dp[i,j] = max(dp[i-1, k-1]+1, dp[l-1,j-1]+1, dp[i-1,j-1])
    public int maxUncrossedLines_v0(int[] A, int[] B) {
        int m=A.length, n=B.length;
        int[][] dp = new int[m+1][n+1];
        Map<Integer,Integer> amap = new HashMap<>();
        Map<Integer,Integer> bmap = new HashMap<>();
        for(int i=0;i<m+1;i++){
            if (i>0) amap.put(A[i-1], i-1);
            for(int j=0;j<n+1;j++) {
                if(i==0||j==0) dp[i][j]=0;
                else {
                    if(bmap.containsKey(A[i-1])) dp[i][j] = Math.max(dp[i][j], dp[i-1][bmap.get(A[i-1])]+1);
                    if(amap.containsKey(B[j-1])) dp[i][j] = Math.max(dp[i][j], dp[amap.get(B[j-1])][j-1]+1);
                    dp[i][j] = Math.max(dp[i][j], dp[i-1][j-1]);
                    bmap.put(B[j-1], j-1);
                }
            }
            bmap.clear();
        }
        return dp[m][n];
    }

    public static void main(String[] args){
        UncrossedLines so = new UncrossedLines();
        System.out.println(so.maxUncrossedLines(new int[]{1,4,2}, new int[]{1,2,4}));
        System.out.println(so.maxUncrossedLines(new int[]{2,5,1,2,5}, new int[]{10,5,2,1,5,2}));
        System.out.println(so.maxUncrossedLines(new int[]{1,3,7,1,7,5}, new int[]{1,9,2,5,1}));
//        A = [2,5,1,2,5], B = [10,5,2,1,5,2]
    }
}

/*
We write the integers of A and B (in the order they are given) on two separate horizontal lines.

Now, we may draw a straight line connecting two numbers A[i] and B[j] as long as A[i] == B[j], and the line we draw does not intersect any other connecting (non-horizontal) line.

Return the maximum number of connecting lines we can draw in this way.



Example 1:


Input: A = [1,4,2], B = [1,2,4]
Output: 2
Explanation: We can draw 2 uncrossed lines as in the diagram.
We cannot draw 3 uncrossed lines, because the line from A[1]=4 to B[2]=4 will intersect the line from A[2]=2 to B[1]=2.
Example 2:

Input: A = [2,5,1,2,5], B = [10,5,2,1,5,2]
Output: 3
Example 3:

Input: A = [1,3,7,1,7,5], B = [1,9,2,5,1]
Output: 2


Note:

1 <= A.length <= 500
1 <= B.length <= 500
1 <= A[i], B[i] <= 2000
*/