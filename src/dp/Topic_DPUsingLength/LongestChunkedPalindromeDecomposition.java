/*
Return the largest possible k such that there exists a_1, a_2, ..., a_k such that:

Each a_i is a non-empty string;
Their concatenation a_1 + a_2 + ... + a_k is equal to text;
For all 1 <= i <= k,  a_i = a_{k+1 - i}.


Example 1:

Input: text = "ghiabcdefhelloadamhelloabcdefghi"
Output: 7
Explanation: We can split the string on "(ghi)(abcdef)(hello)(adam)(hello)(abcdef)(ghi)".
Example 2:

Input: text = "merchant"
Output: 1
Explanation: We can split the string on "(merchant)".
Example 3:

Input: text = "antaprezatepzapreanta"
Output: 11
Explanation: We can split the string on "(a)(nt)(a)(pre)(za)(tpe)(za)(pre)(a)(nt)(a)".
Example 4:

Input: text = "aaa"
Output: 3
Explanation: We can split the string on "(a)(a)(a)".

Constraints:

text consists only of lowercase English characters.
1 <= text.length <= 200
*/

package dp.Topic_DPUsingLength;
import java.util.HashMap;
import java.util.Map;

/*
    dp: O(n^3)
    greedy:  O(n^2)
 */
public class LongestChunkedPalindromeDecomposition {
    // greedy iterative version
    public int longestDecomposition(String text) {
        char[] s = text.toCharArray();
        int n = text.length(), res=0;
        String l="", r="";
        for(int i=0;i<n;i++) {
            l = l+s[i];
            r = s[n-i-1]+r;
            if (l.equals(r)) {
                ++res;
                l="";
                r="";
            }
        }
        return res;
//        int i=0, res=0, cut=1;
//        while((i+cut)*2 <= n) {
//            String left = text.substring(i, i+cut);
//            String right = text.substring(n-i-cut, n-i);
//            if(left.equals(right)) {
//                res += 2;
//                i = i+cut;
//                cut = 1;
//            } else {
//                cut ++;
//            }
//        }
//        if(2*i!=n) res++;
//        return res;
    }

    /*
    // greedy recursive version
    public int longestDecomposition(String text) {
        int n = text.length();
        if(n==0) return 0;
        if(n==1) return 1;
        for(int cut=1;cut<=n/2;cut++) {
            if(text.substring(0, cut) == text.substring(n-cut, n)) {
               return 2 + longestDecomposition(text.substring(cut, n-cut));
            }
        }
        return 1;
    }
    */

    // bottom up dp
    public int longestDecomposition_v2(String text) {
        int n = text.length();
        char[] s = text.toCharArray();
        int[] dp = new int[n+1];

        // conciser code
        dp[1] = 1;
        for(int len=2; len<=n; len++) {
            if (n%2 != len%2) continue;
            dp[len] = 1;
            for(int cut=1; 2*cut<=len; cut++) {
                boolean isP = true;
                for(int l=n/2-len/2, r=l+len-cut, move=0; move<cut; l++,r++,move++){
                    if(s[l] != s[r]) {isP=false; break;}
                }
                if(isP) dp[len] = Math.max(dp[len] ,2 + dp[len-2*cut]);
            }
        }

//        if(n%2==0) {
//            for(int len=2; len<=n; len+=2) {
//                dp[len] = 1;
//                for(int cut=1; 2*cut<=len; cut++) {
//                    boolean isP = true;
//                    for(int l=n/2-len/2, r=l+len-cut, move=0; move<cut; l++,r++,move++){
//                        if(s[l] != s[r]) {isP=false; break;}
//                    }
//                    if(isP) dp[len] = Math.max(dp[len] ,2 + dp[len-2*cut]);
//                }
//            }
//        }
//
//        if(n%2==1) {
//            dp[1] = 1;
//            for(int len=3; len<=n; len+=2) {
//                dp[len] = 1;
//                for(int cut=1; 2*cut<=len; cut++) {
//                    boolean isP = true;
//                    for(int l=n/2-len/2, r=l+len-cut, move=0; move<cut; l++,r++,move++){
//                        if(s[l] != s[r]) {isP=false; break;}
//                    }
//                    if(isP) dp[len] = Math.max(dp[len] ,2 + dp[len-2*cut]);
//                }
//            }
//        }

        return dp[n];
    }


    // top down dp
    Map<String, Integer> memo = new HashMap<>();
    public int longestDecomposition_v1(String text) {
        if(text.isEmpty()) return 0;
        if(memo.containsKey(text)) return memo.get(text);
        char[] s = text.toCharArray();
        int res = 1;
        for(int len=1;len<=text.length()/2;len++) {
            int l=0, r=text.length()-len;
            boolean isP = true;
            while(l<len) {
                if(s[l]!=s[r]) {isP=false;break;}
                l++;r++;
            }
            if(isP) res = Math.max(res, 2 + longestDecomposition_v1(text.substring(len, text.length()-len)));
        }
        memo.put(text, res);
        return res;
    }

    public static void main(String[] as) {
        LongestChunkedPalindromeDecomposition su = new LongestChunkedPalindromeDecomposition();
        int res = su.longestDecomposition("ghiabcdefhelloadamhelloabcdefghi");
        System.out.println(res);
    }
}


/*
by uwi:
class Solution {
    public int longestDecomposition(String text) {
        char[] s = text.toCharArray();
        int n = s.length;
        int[] dp = new int[n+1];
        Arrays.fill(dp, -999999999);
        dp[0] = 0;
        int ans = 1;
        for(int i = 0;i*2 <= n;i++){
            inner:
            for(int j = 1;(i+j)*2 <= n;j++){
                for(int k = i, l = n-i-j, m = 0;m < j;k++,l++,m++){
                    if(s[k] != s[l])continue inner;
                }
                dp[i+j] = Math.max(dp[i+j], dp[i] + 1);
            }
            if(n-i*2 > 0){
                ans = Math.max(ans, dp[i] * 2 + 1);
            }else{
                ans = Math.max(ans, dp[i] * 2);
            }
        }
        return ans;
    }
}


by lee:
Intuition
Honestly I wrote a DP solution first, to ensure it get accepted.
Then I realized greedy solution is right.
Give a quick prove here.
If we have long prefix matched and a shorter prefix matched at the same time.
The longer prefix can always be divided in to smaller part.

Assume we have a longer blue matched and a shorter red matched.
As definition of the statement, we have B1 = B2, R1 = R4.
Because B1 = B2,
the end part of B1 = the end part of B2,
equal to R2 = R4,
So we have R1 = R4 = R2.
B is in a pattern of R + middle part + R.
Instead take a longer B with 1 point,
we can cut it in to 3 parts to gain more points.
This proves that greedily take shorter matched it right.
Note that the above diagram shows cases when shorter length <= longer length/ 2
When shorter length > longer length/ 2, this conclusion is still correct.

To be more general,
the longer prefix and shorter prefix will alway be in these patter:
longer = a + ab * N
shorter = a + ab * (N - 1)
for example:
longer = "abc" + "def" + "abc"
shorter = "abc"
for example:
longer = "abc" * M
shorter = "abc" * N
where M > N

Solution 1, very brute force
When we know the greedy solution is right,
the coding is easier.
Just take letters from the left and right side,
Whenever they match, res++.

Complexity
I just very brute force generate new string and loop the whole string.
Complexity can be improve on these two aspects.
Pardon that I choose the concise over the performance.
Time O(N) * O(string)
Space O(N)

Java:
    public int longestDecomposition(String S) {
        int res = 0, n = S.length();
        String l = "", r = "";
        for (int i = 0; i < n; ++i) {
            l = l + S.charAt(i);
            r = S.charAt(n - i - 1) + r;
            if (l.equals(r)) {
                ++res;
                l = "";
                r = "";
            }
        }
        return res;
    }

C++:
    int longestDecomposition(string S) {
        int res = 0, n = S.length();
        string l = "", r = "";
        for (int i = 0; i < n; ++i) {
            l = l + S[i], r = S[n - i - 1] + r;
            if (l == r)
                ++res, l = "", r = "";
        }
        return res;
    }

Python:
    def longestDecomposition(self, S):
        res, l, r = 0, "", ""
        for i, j in zip(S, S[::-1]):
            l, r = l + i, j + r
            if l == r:
                res, l, r = res + 1, "", ""
        return res


Solution 2, Tail Recursion
Same idea, just apply tail recursion.
And add a quick check before we slice the string.


C++:
    int longestDecomposition(string S, int res = 0) {
        int n = S.length();
        for (int l = 1; l <= n / 2; ++l)
            if (S[0] == S[n - l] && S[l - 1] == S[n - 1])
                if (S.substr(0, l) == S.substr(n - l))
                    return longestDecomposition(S.substr(l, n - l - l), res + 2);
        return n ? res + 1 : res;
    }

Python:
    def longestDecomposition(self, S, res=0):
        n = len(S)
        for l in xrange(1, n / 2 + 1):
            if S[0] == S[n - l] and S[l - 1] == S[n - 1]:
                if S[:l] == S[n - l:]:
                    return self.longestDecomposition(S[l:n - l], res + 2)
        return res + 1 if S else res

*/
