package dp.Topic_DPUsingLength;

import java.util.*;

/*
I. check if num is strobogrammatic(flippable)
    - easy..

II: find all strobogrammatic nums with given length n:
    - backtrack   O(2^n)
    - FollowUp!!!! if we only need the count, use DP! see countOfMirrorNum() method!!: O(n)

III: Count of strobogrammatic nums in given range [low, high]:
    (As we just need count, usually consider DP)
    - dp+dfs: good!!!!!  O(n), code is a little complex...
    - dfs, count one by one using II:  O(2^n)

 */
public class StrobogrammaticNumber_I_II_III {

    // III. dp
    public int strobogrammaticInRange(String low, String high) {
        Map<Character, Character> dic = initDic();
        int maxLen = high.length();
        int[] f = new int[maxLen+1]; // f[i] count of valid mirror nums with length i
        int[] g = new int[maxLen+1]; // g[i] count of invalid(start with '0' and len>1) mirror nums with length i
        for (int i = 0; i <= maxLen; i++) {
            if (i==0) {f[i]=1; g[i]=0;}
            else if (i==1) {f[i]=3; g[i]=0;}  // 0,1,8
            else {
                f[i] = 4*(f[i-2]+g[i-2]);
                g[i] = f[i-2]+g[i-2];
            }
        }

        int highCount = countOfSmaller(high, dic, f, g);
        int lowCount = countOfSmaller(low,dic,f,g);

        return highCount-lowCount + (isStrobogrammaticI(low)?1:0);
    }
    int countOfSmaller(String high, Map<Character, Character> dic, int[] f, int [] g) {
        int count=0;
        for(int len=1; len<=high.length();len++) {
            if (len<high.length()) {
                count+=f[len];
            } else {
                count+= countOfSmallerWithLen(high, len, dic, f, g, true);
            }
        }
        return count;
    }


    int countOfSmallerWithLen(String high, int len, Map<Character, Character> dic, int[] f, int [] g, boolean skipZero) {
        if (len==0) return 0;
        int count=0;
        if (len==1) {
            // high: x, candidates   ->  c is all '0','1','8' smaller then x
            for(char c: Arrays.asList('0','1','8')) {
                if (c<=high.charAt(0)) count++;
            }
        } else {
            // high: hxxxh', candidates: cyyyc'
            for (char c: Arrays.asList('0','1','6','8','9')) {
                // the first level recursion should have no zero...
                if (skipZero && c=='0') continue;

                // h<c -> all yyy
                if (c<high.charAt(0)) {
                    count+=f[len-2]+g[len-2]; // cxxxxc'
                }
                // h==c
                else if (high.charAt(0)==c) {

                    String subHigh = high.substring(1,high.length()-1); // high去掉头尾
                    // h'>=c'  ->  all yyy smaller than xxx (recursive)
                    if (high.charAt(high.length()-1)>=dic.get(c)) {
                        count += countOfSmallerWithLen(subHigh, len-2, dic, f,g, false);
                    }
                    // h'<c'
                    else {
                        // h'往前借一位，使得h'>=c', ex: 9_8_0 -> 9_7_19 (应该用string算，这里临时用Integer)
                        if(subHigh.isEmpty() || Integer.valueOf(subHigh)==0) continue; // edge case， 000 无法借位
                        subHigh = String.valueOf(Integer.valueOf(subHigh)-1);

                        count+=countOfSmallerWithLen(subHigh, len-2, dic, f,g, false);
                    }
                }
                // h>c -> just skip...
            }
        }
        return count;

    }


    // III. brute force version: O(2^n), list all and count
    int count=0;
    public int strobogrammaticInRange_dfs(String low, String high) {
        Map<Character, Character> dic = new HashMap<>();
        dic.put('0','0');
        dic.put('1','1');
        dic.put('6','9');
        dic.put('8','8');
        dic.put('9','6');
        // odd
        helper("0", low, high, dic);
        helper("1", low, high, dic);
        helper("8", low, high, dic);
        // even
        helper("", low, high, dic);
        return count;
    }
    void helper(String path, String low, String high, Map<Character, Character> dic) {
        if (compare(path, high)>0) return;
        if (compare(path, low)>=0) {
            if (path.length()>1 && path.charAt(0)=='0') return;
            count++;
        }
        for(char c: dic.keySet()) {
            helper(c+path+dic.get(c), low, high, dic);
        }
    }
    int compare(String path, String num) {
        if (path.length()!=num.length())
            return path.length()-num.length();
        return path.compareTo(num);
    }



    /*
     II followup: count of all strobogrammatic nums of given length!

        0,1,6,8,9

        f[i]:  count of strobogrammatic nums(valid) when len=i
        g[i]:  count of strobogrammatic nums(invalid: start with '0') when len=i

            f[i] = 4 * (f[i-2]+g[i-2])           // 1xxx1, 6xxx9, 8xxx8, 9xxx9
            g[i] = f[i-2] + g[i-2]    // 0xxx0 (xxx:valid), 0yyy0(yyy: invalid)

            f[0] = 1
            f[1] = 3
            g[0] = 0
            g[1] = 0
     */
    int countOfMirrorNum(int n) { // n: length
        int[] f = new int[n+1];
        int[] g = new int[n+1];

        for (int i = 0; i <= n; i++) {
            if (i==0) {
                f[i]=1;
                g[i]=0;
            } else if (i==1) {
                f[i]=3;
                g[i]=0;
            } else {
                f[i]=4*(f[i-2]+g[i-2]);
                g[i]=f[i-2]+g[i-2];
            }
        }
        return f[n];
    }


    public List<String> findStrobogrammaticII(int n) {
        List<String> ans = new ArrayList<>();
        backtrack(ans, new char[n], 0, n, initDic());
        return ans;
    }
    private void backtrack(List<String> ans, char[] path, int index, int n, Map<Character, Character> dic) {
        if (index==(n+1)/2) {
            ans.add(String.valueOf(path));
            return;
        }
        for (char key: dic.keySet()) {
            if (index==0 && n>1 && key=='0') continue;
            if (n%2==1 && index==n/2 && (key=='6'||key=='9')) continue;
            path[index] = key;
            path[n-1-index] = dic.get(key);
            backtrack(ans, path, index+1, n, dic);
            // it is ok to skip backtracking status here
        }
    }

    public boolean isStrobogrammaticI(String num) {
        Map<Character, Character> dic = initDic();
        int l=0, r=num.length()-1;
        while (l<=r) {
            if (dic.containsKey(num.charAt(l)) && dic.get(num.charAt(l))==num.charAt(r)) {
                l++;r--;
            } else {
                return false;
            }
        }
        return true;
     }

     Map<Character, Character> initDic(){
         Map<Character, Character> dic = new HashMap<>();
         dic.put('0','0');
         dic.put('1','1');
         dic.put('6','9');
         dic.put('8','8');
         dic.put('9','6');
         return dic;
     }

     public static void main(String[] args) {
        StrobogrammaticNumber_I_II_III solution = new StrobogrammaticNumber_I_II_III();

//         int n=20;
//         List<String> res = solution.findStrobogrammaticII(1);
//         for (String s:res) System.out.println(s);
//         List<String> res2 = solution.findStrobogrammaticII(2);
//         for (String s:res2) System.out.println(s);
//         long t2=System.currentTimeMillis();
//         System.out.println(res.size());
//         System.out.println("time:"+(t2-t1));
//
//         long t3=System.currentTimeMillis();
//         System.out.println(solution.countOfMirrorNum(n));
//         long t4=System.currentTimeMillis();
//         System.out.println("time:"+(t4-t3));

//         System.out.println(new StrobogrammaticNumber_I_II_III().isStrobogrammatic("69"));
         String low="10";
         String high="200000";

         long t;
         t=System.currentTimeMillis();
         System.out.println(solution.strobogrammaticInRange(low, high));
         System.out.println("dp+dfs time:"+(System.currentTimeMillis()-t));

         t=System.currentTimeMillis();
         System.out.println(solution.strobogrammaticInRange_dfs(low, high));
         System.out.println("dfs time:"+(System.currentTimeMillis()-t));

     }
}



/*
I.
Description
A mirror number is a number that looks the same when rotated 180 degrees (looked at upside down).
For example, the numbers "69", "88", and "818" are all mirror numbers.

Write a function to determine if a number is mirror. The number is represented as a string.

Example
Example 1:

Input : "69"
Output : true
Example 2:

Input : "68"
Output : false

II.
Description
A strobogrammatic number is a number that looks the same when rotated 180 degrees (looked at upside down).
Find all strobogrammatic numbers that are of length = n.

Example
Example 1:

Input: n = 2,
Output: ["11","69","88","96"]
Example 2:

Input: n = 1,
Output: ["0","1","8"]

III.
A strobogrammatic number is a number that looks the same when rotated 180 degrees (looked at upside down).

Write a function to count the total strobogrammatic numbers that exist in the range of low <= num <= high.

Example:

Input: low = "50", high = "100"
Output: 3
Explanation: 69, 88, and 96 are three strobogrammatic numbers.
Note:
Because the range might be a large number, the low and high numbers are represented as string.
*/

