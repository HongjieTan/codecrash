package dp.Topic_DPUsingLength;

/*
国人。 一个只有正整数的list， 其中插入+， * 或者（），求得到式子最大的值。 e.g. [1，2，1，2 ]->  (1+2)*(1+2)=9.  dp解，
思路：类似burst balloon dp[i][j] = max of for (k : i ~ j  max(dp[i][k - 1] * dp[k][j], dp[i][k - 1] + dp[k][j]))


follow up， 如果有负数该怎么办， 如果想要拿到最大的式子该怎么办。

tan: Max和Min都要算。。最后更具符号来确定最大值。。（？）

*/
public class MaxExpression {


    public int maxValue(int[] nums) {
        int n=nums.length;
        int[][] dp = new int[n][n];


        for(int i=0;i<n;i++) dp[i][i]=nums[i];

        for(int len=2;len<=n;len++) {
            for(int i=0; i+len-1<n;i++) {
                int j=i+len-1;
                for(int  k=i;k<j;k++) {
                    dp[i][j] = Math.max(dp[i][j], Math.max(dp[i][k]*dp[k+1][j], dp[i][k]+dp[k+1][j]));
                }

            }
        }
        return dp[0][n-1];
    }

    public static void main(String[] args) {
        System.out.println(new MaxExpression().maxValue(new int[]{1,2,1,2}));
    }


}
