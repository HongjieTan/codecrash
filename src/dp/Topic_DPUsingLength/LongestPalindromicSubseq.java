package dp.Topic_DPUsingLength;

/**
 * Created by thj on 2018/8/2.
 *
 *  dp_v2: the choice
 *
 *   dp[i][j]: max len from [i...j]
 *
 *
 *   len=1:  dp[i][i]=1
 *   len>1:  (j=i+len-1)
 *           if s[i]==s[j]: dp[i][j]=2+dp[i+1][j-1]
 *           if s[i]!=s[j]: dp[i][j]=max(dp[i+1][j], dp[i][j-1])
 *
 *
 *  dp_v1:
 *
 *  dp[i][j]:     i:length  j: start index,   store the max len of s[j, j+1, ..., j+i-1]
 *
 *
 *  dp[0][j]: 0
 *  dp[1][j]: 1
 *  dp[i][j]: i>=2
 *       s[j] == s[j+i-1]: 2 + dp[i-2][j+1]
 *       other           : max(dp[i-1][j], dp[i-1][j+1])
 *
 *
 *
 *
 */
public class LongestPalindromicSubseq {
    public int longestPalindromeSubseq_v2(String s) {
        int n =s.length();
        int[][] dp=new int[n][n];

        for(int i=0;i<n;i++) dp[i][i]=1;
        for(int len=2;len<=n;len++) {
            for(int i=0; i+len-1<n;i++) {
                int j=i+len-1;
                if(s.charAt(i)==s.charAt(j)){
                    dp[i][j]=2+dp[i+1][j-1];
                } else {
                    dp[i][j]=Math.max(dp[i+1][j], dp[i][j-1]);
                }
            }
        }
        return dp[0][n-1];
    }

    public int longestPalindromeSubseq(String s) {
        int[][] dp = new int[s.length()+1][s.length()];

        for (int j = 0; j < s.length(); j++) {
            dp[0][j] = 0;
        }
        for (int j = 0; j < s.length(); j++) {
            dp[1][j] = 1;
        }

        for (int i=2; i <= s.length(); i++) {
            for (int j=0; j<=s.length()-i; j++) {
                dp[i][j] = s.charAt(j)==s.charAt(j+i-1)?
                        (2+dp[i-2][j+1]) : Math.max(dp[i-1][j], dp[i-1][j+1]);
            }
        }

        return dp[s.length()][0];
    }

    public static void main(String[] args) {
        System.out.println(new LongestPalindromicSubseq().longestPalindromeSubseq("bbbab"));
    }
}
