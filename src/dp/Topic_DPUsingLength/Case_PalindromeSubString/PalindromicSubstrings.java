//Given a string, your task is to count how many palindromic substrings in this string.
//
// The substrings with different start indexes or end indexes are counted as different substrings even they consist of same characters.
//
// Example 1:
//
//
//Input: "abc"
//Output: 3
//Explanation: Three palindromic strings: "a", "b", "c".
//
//
//
//
// Example 2:
//
//
//Input: "aaa"
//Output: 6
//Explanation: Six palindromic strings: "a", "a", "a", "aa", "aa", "aaa".
//
//
//
//
// Note:
//
//
// The input string length won't exceed 1000.
//
//
//
//
//
// hint1:
// How can we reuse a previously computed palindrome to compute a larger palindrome?
// hint2:
// If “aba” is a palindrome, is “xabax” and palindrome? Similarly is “xabay” a palindrome?
// hint3:
// Complexity based hint:
// If we use brute-force and check whether for every start and end position a substring is a palindrome we have O(n^2) start - end pairs and O(n) palindromic checks.
// Can we reduce the time for palindromic checks to O(1) by reusing some previous computation?

package dp.Topic_DPUsingLength.Case_PalindromeSubString;


import java.util.Arrays;

/*
  brute force: O(n^3)
  expand around center: O(n^2)
  dp: O(n^2)
 */
public class PalindromicSubstrings {
    // expand around center
    public int countSubstrings(String s) {
        char[] chs = s.toCharArray();
        int n = s.length(), res = 0;

        for(int i=0; i<n; i++) {
            // set s[i] as center
            int l=i, r=i;
            while(l>=0 && r<n) {
                if(chs[l] == chs[r]) { res++;l--;r++; }
                else break;
            }

            // set s[i] s[i+1] as center
            l=i; r=i+1;
            while(l>=0 && r<n) {
                if(chs[l] == chs[r]) { res++;l--;r++;}
                else break;
            }
        }
        return res;
    }

    // dp
    public int countSubstrings_dp(String s) {
        int n = s.length(), res = 0;
        char[] chs = s.toCharArray();
        boolean dp[][] = new boolean[n+1][n];
        Arrays.fill(dp[0], true);

        for(int len=1;len<=n;len++) {
            for(int i=0;i+len-1<n;i++) {
                int j=i+len-1;
                if(len==1) dp[len][i]=true;
                else dp[len][i] = dp[len-2][i+1] && (chs[i]==chs[j]);

                if(dp[len][i]) res++;
            }
        }
        return res;

    }



    public static void main(String[] a) {
        System.out.println(new PalindromicSubstrings().countSubstrings("aaa"));
    }
}

