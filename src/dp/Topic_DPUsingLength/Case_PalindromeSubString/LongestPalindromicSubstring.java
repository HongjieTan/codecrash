//Given a string s, find the longest palindromic substring in s. You may assume that the maximum length of s is 1000.
//
// Example 1:
//
//
//Input: "babad"
//Output: "bab"
//Note: "aba" is also a valid answer.
//
//
// Example 2:
//
//
//Input: "cbbd"
//Output: "bb"
//

package dp.Topic_DPUsingLength.Case_PalindromeSubString;

/**
 * Created by thj on 2018/8/2.
 *
 *
 * 1. brute force: O(n^3)
 * 2. 对每个元素中心发散验证：O(n^2), also good!!
 * 3. dp_v1:   O(n^2)  （the choice...）
 *
 *    dp[l][r]:  if S[l...r] is palindrome
 *
 *    dp[l][r] = true,                        len<=2, s[l]=s[r]
 *    dp[l][r] = dp[l+1][r-1] && s[l]==s[r],  len>2
 *
 *  4.dp_v2:  O(n^2)
 *
 *    dp[len][start]
 *
 *    len==0, dp[len][start]=true
 *    len==1, dp[len][start]=true
 *    len>=2,
 *          s[start]==s[start+len-1]:  dp[len-2][start+1]
 *          other:                     false
 *
 *  5. Manacher's Algorithm 马拉车算法 - 可以O(n）时间求最长回文子串 ！ later...
 */
public class LongestPalindromicSubstring {

    public String longestPalindrome_dp1(String s) {
        if (s==null) return null;
        String res = "";
        int maxLen = 0;
        int n = s.length();
        boolean dp[][] = new boolean[n][n];

        for (int len=1; len<=n; len++) {
            for (int l = 0; l+len-1 < n; l++) {
                int r=l+len-1;
                if (len<=2) {
                    dp[l][r] = s.charAt(l)==s.charAt(r);
                } else {
                    dp[l][r] = dp[l+1][r-1] && s.charAt(l)==s.charAt(r);
                }
                if (dp[l][r] && len>maxLen) {
                    maxLen = len;
                    res = s.substring(l, r+1);
                }
            }
        }

        return res;
    }


    public String longestPalindrome_dp2(String s) {
        boolean[][] dp=new boolean[s.length()+1][s.length()];

        for (int start = 0; start < s.length(); start++) dp[0][start] = true;
        for (int start = 0; start < s.length(); start++) dp[1][start] = true;

        for (int len=2; len<=s.length(); len++) {
            for (int start = 0; start+len-1 < s.length(); start++) {
                dp[len][start] = s.charAt(start) == s.charAt(start+len-1)? dp[len-2][start+1]:false;
            }
        }

        String res="";
        for (int len = 0; len <= s.length(); len++) {
            for (int start = 0; start<s.length(); start++) {
                if(dp[len][start]) {
                    res = s.substring(start, start+len);
                }
            }
        }
        return res;
    }


    public static void main(String[] args) {
        System.out.println(new LongestPalindromicSubstring().longestPalindrome_dp2("a"));
    }

}
