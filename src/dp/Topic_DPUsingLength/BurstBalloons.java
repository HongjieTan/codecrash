package dp.Topic_DPUsingLength;
/*
Given n balloons, indexed from 0 to n-1. Each balloon is painted with a number on it represented by array nums. You are asked to burst all the balloons. If the you burst balloon i you will get nums[left] * nums[i] * nums[right] coins. Here left and right are adjacent indices of i. After the burst, the left and right then becomes adjacent.

Find the maximum coins you can collect by bursting the balloons wisely.

Note:

You may imagine nums[-1] = nums[n] = 1. They are not real therefore you can not burst them.
0 ≤ n ≤ 500, 0 ≤ nums[i] ≤ 100
Example:

Input: [3,1,5,8]
Output: 167
Explanation: nums = [3,1,5,8] --> [3,5,8] -->   [3,8]   -->  [8]  --> []
             coins =  3*1*5      +  3*5*8    +  1*3*8      + 1*8*1   = 167
* */

/**
 *
 *  - brute force: O(n!)
 *  - dp: O(n^3)
 *
 *
 */
public class BurstBalloons {
    /*
        dp[i][j]  maxCoins of range [i,...,j]

        len = j-i+1
        len=1: dp[i][j] = num[i-1]*num[i]*num[i+1]
        len>1: dp[i][j] = max{ dp[i][k-1] + dp[k+1][j] + num[i-1]*num[k]*num[j+1]}, k=i...j // assume kth balloon is burst at last!!

        In brief:
        build solution in order of len=1->n: dp[i][j] = max{ dp[i][k-1] + dp[k+1][j] + num[i-1]*num[k]*num[j+1]}, k=i...j
     */
    public int maxCoins(int[] nums) {
        int n=nums.length;
        int[] arr = new int[n+2];
        int[][] dp = new int[n+2][n+2];
        arr[0] = 1;
        for(int i=0; i<n; i++) arr[i+1] = nums[i];
        arr[n+1] = 1;

        for(int len=1; len<=n; len++) {
            for (int i = 1; i+len-1 <=n ; i++) {
                int j = i+len-1;
                int max = 0;
                for (int k=i;k<=j;k++) {
                    max = Math.max(dp[i][k-1]+dp[k+1][j]+arr[i-1]*arr[k]*arr[j+1], max); //len=1 case is included..
                }
                dp[i][j] = max;
            }
        }
        return dp[1][n];
    }

    public static void main(String[] args) {
        System.out.println(new BurstBalloons().maxCoins(new int[]{3,1,5,8}));
    }
}
