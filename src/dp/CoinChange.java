//You are given coins of different denominations and a total amount of money amount.
// Write a function to compute the fewest number of coins that you need to make up that amount.
// If that amount of money cannot be made up by any combination of the coins, return -1.
//
// Example 1:
//
//
//Input: coins = [1, 2, 5], amount = 11
//Output: 3
//Explanation: 11 = 5 + 5 + 1
//
// Example 2:
//
//
//Input: coins = [2], amount = 3
//Output: -1
//
//
// Note:
//You may assume that you have an infinite number of each kind of coin.

package dp;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

/**
 *
 * Created by thj on 2018/8/25.
 *
 *  1. bfs:(as it is MinStepsProblem...)
 *     - naive bfs: will cause LTE
 *     - bfs with cache: good!!!
 *
 *  2. DP: good!!
 *
 *
 *
 */
public class CoinChange {

    // good!!! O(amount * coins )
    public int coinChange_dp(int[] coins, int amount) {
        int[] dp = new int[amount+1];
        Arrays.fill(dp, -1);

        dp[0] = 0;

        for (int i = 1; i <= amount; i++) {
            int min = Integer.MAX_VALUE;
            for (int coin: coins) {
                if (i-coin>=0 && dp[i-coin]!=-1) {
                    min = Math.min(min, dp[i-coin]);
                }
            }
            dp[i] = min!=Integer.MAX_VALUE? min+1: -1;
        }

        return dp[amount];
    }

    // bfs with cache
    public int coinChange_bfs_cache(int[] coins, int amount) {
        LinkedList<Integer> que = new LinkedList<>();
        que.add(0);

        int depth = -1;
        Set<Integer> visited = new HashSet<>(); // cache to remove repeated computations

        while (!que.isEmpty()) {
            depth++;

            int levelSize = que.size();
            for (int i = 0; i < levelSize; i++) {
                int curAmount = que.pop();

                if (curAmount == amount) return depth;
                else if (curAmount<amount && !visited.contains(curAmount)) {
                    visited.add(curAmount);
                    for (int coin: coins) {
                        que.add(curAmount+coin);
                    }
                }
            }
        }
        return -1;
    }




    // fewest -> bfs,  but it will cause LTE
    public int coinChange_bfs_naive(int[] coins, int amount) {
        LinkedList<Integer> que = new LinkedList<>();

        int depth = -1;
        que.add(0);

        while (!que.isEmpty()) {
            depth++;

            int levelSize = que.size();
            for (int i = 0; i < levelSize; i++) {
                int curAmount = que.pop();

                if (curAmount == amount) return depth;
                if (curAmount < amount) {
                    for (int coin: coins) {
                        que.add(curAmount+coin);
                    }
                }
            }

        }

        return -1;
    }

    public static void main(String[] args) {
        System.out.println(new CoinChange().coinChange_bfs_cache(new int[]{1,2,5}, 11));
    }

}

