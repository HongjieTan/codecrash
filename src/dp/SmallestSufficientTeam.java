//In a project, you have a list of required skills req_skills, and a list of people. The i-th person people[i] contains a list of skills that person has.
//
// Consider a sufficient team: a set of people such that for every required skill in req_skills, there is at least one person in the team who has that skill.
// We can represent these teams by the index of each person: for example, team = [0, 1, 3] represents the people with skills people[0], people[1], and people[3].
//
// Return any sufficient team of the smallest possible size, represented by the index of each person.
//
// You may return the answer in any order. It is guaranteed an answer exists.
//
//
// Example 1:
// Input: req_skills = ["java","nodejs","reactjs"], people = [["java"],["nodejs"],["nodejs","reactjs"]]
//Output: [0,2]
// Example 2:
// Input: req_skills = ["algorithms","math","java","reactjs","csharp","aws"], people = [["algorithms","math","java"],["algorithms","math","reactjs"],["java","csharp","aws"],["reactjs","csharp"],["csharp","math"],["aws","java"]]
//Output: [1,2]
//
//
// Constraints:
//
//
// 1 <= req_skills.length <= 16
// 1 <= people.length <= 60
// 1 <= people[i].length, req_skills[i].length, people[i][j].length <= 16
// Elements of req_skills and people[i] are (respectively) distinct.
// req_skills[i][j], people[i][j][k] are lowercase English letters.
// It is guaranteed a sufficient team exists.
//
//
package dp;

import java.util.*;

/*

    This is a classical NP problem - "Set Cover Problem" (https://en.wikipedia.org/wiki/Set_cover_problem)s
    (hard...maybe not suitable for interview...)

    dp + bit manipulation...
    - brute force: O(2^people * skills), all combinations of subset of people...
    - dp:  O(people * 2^skills), all combination of subset of skills...使用二进制对状态进行压缩(也可不压缩...)

 */

public class SmallestSufficientTeam {

    // https://www.youtube.com/watch?v=Up8iyOrq-5Y
    public int[] smallestSufficientTeam(String[] req_skills, List<List<String>> people) {
        int m=req_skills.length, n=people.size();
        Map<String, Integer> map = new HashMap<>();
        for(int i=0; i<m; i++)  map.put(req_skills[i], i);

        List<Integer>[] dp = new List[1<<m];
        dp[0] = new ArrayList<>();
        for(int i=0;i<n;i++) {
            int his_skill = 0;
            for(String s: people.get(i)) his_skill |= (1<<map.getOrDefault(s,0));

            for(int prev=0;prev<dp.length;prev++) {
                if(dp[prev]==null) continue;
                int with_him = prev | his_skill;
                if(dp[with_him]==null || dp[with_him].size() > dp[prev].size()+1) {
                    dp[with_him] = new ArrayList(dp[prev]);
                    dp[with_him].add(i);
                }
            }
        }

        List<Integer> l = dp[dp.length-1];
        int[] res = new int[l.size()];
        for(int i=0;i<l.size();i++) res[i]=l.get(i);
        return res;
    }

    public static void main(String[] a) {
        String[] req_skills = {"java","nodejs","reactjs"};
        List<List<String>> people = new ArrayList<>();
        people.add(Arrays.asList("java"));
        people.add(Arrays.asList("nodejs"));
        people.add(Arrays.asList("nodejs","reactjs"));

        SmallestSufficientTeam su = new SmallestSufficientTeam();
        int[] res = su.smallestSufficientTeam(req_skills, people);
        for(int x:res) System.out.println(x);

    }
}


/*
by lee:

Explanation
dp[skill_set] is a sufficient team to cover the skill_set.
For each people,
update his_skill with all current combinations of skill_set in dp.

Complexity
Time O(people * 2^skill)
Space O(2^skill)


Python:

    def smallestSufficientTeam(self, req_skills, people):
        n, m = len(req_skills), len(people)
        key = {v: i for i, v in enumerate(req_skills)}
        dp = {0: []}
        for i, p in enumerate(people):
            his_skill = 0
            for skill in p:
                if skill in key:
                    his_skill |= 1 << key[skill]
            for skill_set, need in dp.items():
                with_him = skill_set | his_skill
                if with_him == skill_set: continue
                if with_him not in dp or len(dp[with_him]) > len(need) + 1:
                    dp[with_him] = need + [i]
        return dp[(1 << n) - 1]


    comment:
        -beautiful solution. people is like nodes, his_skill is like edges, in the loop, we are relaxing all the available edges. this is like Bellman-Ford
            - This is so thought-provoking! The interpretation as Bellman-Ford searching for shortest-path here is really insightful.
              I just have a little different understanding here. skill_set is more like node here, and people are like edges.
              We are looking for a shortest path (using the least number of people) to get from node 00...00 (no required skills in the team) to 11..11 (has all the required skills in the team).

some java:
python reference: https://leetcode.com/problems/smallest-sufficient-team/discuss/334572/Python-DP-Solution

class Solution {
    public int[] smallestSufficientTeam(String[] req_skills, List<List<String>> people) {
        int ns = req_skills.length, np = people.size();
        HashMap<String, Integer> map = new HashMap<>();
        for (int i = 0; i < ns; ++i) map.put(req_skills[i], i);
        List<Integer>[] suff = new List[1 << ns];
        suff[0] = new ArrayList<>();
        for (int i = 0; i < np; ++i) {
            int skill = 0;
            for (String s : people.get(i)) skill |= (1 << map.get(s));
            for (int prev = 0; prev < suff.length; ++prev) {
                if (suff[prev] == null) continue;
                int comb = prev | skill;
                if (suff[comb] == null || suff[prev].size() + 1 < suff[comb].size()) {
                    suff[comb] = new ArrayList<>(suff[prev]);
                    suff[comb].add(i);
                }
            }
        }
        List<Integer> res = suff[(1 << ns) - 1];
        int[] arr = new int[res.size()];
        for (int i = 0; i < arr.length; ++i) arr[i] = res.get(i);
        return arr;
    }
}


by a chinese version:
[Chinese/C++] 1125. 经典动态规划之状态压缩

比赛145题解：
1122 Relative Sort Array
1123 Lowest Common Ancestor of Deepest Leaves
1124 Longest Well-Performing Interval
1125 Smallest Sufficient Team

题意：我们要从 K 个人里面挑选若干人组建一个有 N 个技能组成的全能团队。
问最少选择多少人可以组建这个全能团队。

起初看这道题，很像背包问题，这个人选择或者不选择。
但是会发现状态是若干个技能，没办法储存。
观察技能总个数，只有 16 个，2^16=65536，所以可以使用状态压缩来储存状态。

状态定义：dp[i][j] 为前i个人组建一个技能为j的团队需要的最少人数。
其中j是使用bit位压缩的。

状态转移：对于第i个人可以选择，也可以不选择。
选择了更新 bit状态，不选择了使用旧的bit状态，取最优值。

这样我们就能够进行 递归找到答案了。

由于最终是输出最优答案的路径，所以需要记录状态转移的路径preIndex和preSkill。

class Solution {
    struct Node{
        Node(int _num = 0):num(_num){

        }
        int num;
        int state; //1 select, 0 not select
        int nowIndex;
        int preIndex;
        int preSkill;
    };
    map<pair<int, int>, Node> dp;
    vector<int> personToBit;

    //给每个技能分配一个 bit 位
    int bulid(vector<string>& req_skills, vector<vector<string>>& peoples){
        int i=0, reqBit = 0;
        map<string, int> skillToBit;
        for(auto& v: req_skills){
            skillToBit[v] = i++;
            reqBit = (reqBit<<1) | 1;
        }

        //计算一个人的技能值
        for(auto&people: peoples){
            int bit = 0;
            for(auto& v: people){
                if(skillToBit.count(v)){
                    bit = bit|(1<< skillToBit[v]);
                }
            }
            personToBit.push_back(bit);
        }
        return reqBit;
    }

    //计算当前状态的答案
    Node check(const int index, const int leftBit, int state){
        Node node;

        Node preNode = dfs(index, leftBit);
        if(preNode.num == -1){
            node.num = -1; //选择没有答案
            node.state = 0;
            return node;
        }

        node.num = preNode.num + state;
        node.state = state;
        if(preNode.state == 1){
            node.preIndex = index;
            node.preSkill = leftBit;
        }else{
            node.preIndex = preNode.preIndex;
            node.preSkill = preNode.preSkill;
        }
        return node;
    }

    Node dfs(const int index, const int bitSkill){
        if(index < 0) return Node(-1);
        pair<int, int> now = {index, bitSkill};
        if(dp.count(now) > 0){
            return dp[now];
        }

        Node node;

        const int nowBit = personToBit[index];
        const int leftBit =  bitSkill & ~nowBit; //通过位操作，减去当前人的技能

        //选择
        if(leftBit){
            node = check(index-1, leftBit, 1);
        }else{
            node.num = 1; //选择,人够了
            node.state = 1;
        }

        //不选择
        Node notNode = check(index-1, bitSkill, 0);

        node.nowIndex = notNode.nowIndex = index; //记录当前状态属于哪个人

        if(node.num == -1 || notNode.num == -1){
            return dp[now] = node.num == -1 ? notNode: node;
        }else{
            return dp[now] = node.num >= notNode.num? notNode : node;
        }

    }
public:
    vector<int> smallestSufficientTeam(vector<string>& req_skills, vector<vector<string>>& people) {
        int reqBit = bulid(req_skills, people);
        Node node = dfs(people.size()-1, reqBit);
        vector<int> ans;

        int num = node.num;
        do{
            while(node.state == 0){
                node = dfs(node.preIndex, node.preSkill);
            }
            ans.push_back(node.nowIndex);
            if(node.num == 1){
                break;
            }
            node = dfs(node.preIndex, node.preSkill);
        }while(true);

        return ans;
    }
};
*/