package dp;

import java.util.HashMap;
import java.util.Map;

// 1027,  also check pathSum...
public class LongestArithmeticSequence {
    // dp[i][diff]: max length of seq ending at A[i] and with diff
    //   dp[i][diff] = max{dp[j][diff]+1}  (j=0...i-1 and diff=A[i]-A[j])
    public int longestArithSeqLength(int[] A) {
        int n = A.length;
        Map<Integer,Integer>[] dp = new Map[n];
        for (int i = 0; i < n; i++) dp[i] = new HashMap<>();
        int ans = 2;
        for(int i=1; i<n; i++)
            for(int j=0; j<i; j++) {
                int diff = A[i]-A[j];
                int len = dp[j].getOrDefault(diff, 1)+1;
                dp[i].put(diff, Math.max(len, dp[i].getOrDefault(diff,0)));
                ans = Math.max(ans, dp[i].get(diff));
            }
        return ans;
    }

    /* python version
    def longestArithSeqLength(self, A):
        dp = collections.defaultdict(int)
        for i in xrange(len(A)):
            for j in xrange(0, i):
                diff = A[i]-A[j]
                dp[i, diff] = max(dp[i, diff], dp[j, diff]+1)
        return max(dp.values())+1
    */

    public static void main(String[] args) {
        System.out.println(new LongestArithmeticSequence().longestArithSeqLength(new int[]{83,20,17,43,52,78,68,45})); // 2
    }

    /*
    my first version, TLE ...
    public int longestArithSeqLength(int[] A) {
        int n=A.length;
        int max = A[0];
        for(int x:A) max=Math.max(x, max);
        int[][] dp = new int[n+1][2*max+1]; // diff: -max~max, offset:max
        int ans = 0;
        for(int i=0;i<n;i++) {
            for(int j=0;j<2*max+1;j++) {
                int diff = j-max;
                dp[i][j]=1;
                for(int k=0;k<i;k++) {
                    if(A[i]-A[k]==diff) {
                        dp[i][j] = Math.max(dp[i][j], dp[k][j]+1);
                    }
                }
                ans = Math.max(ans, dp[i][j]);
            }
        }
        return ans;
    }
    */
}

/*
Given an array A of integers, return the length of the longest arithmetic subsequence in A.
Recall that a subsequence of A is a list A[i_1], A[i_2], ..., A[i_k] with 0 <= i_1 < i_2 < ... < i_k <= A.length - 1,
and that a sequence B is arithmetic if B[i+1] - B[i] are all the same value (for 0 <= i < B.length - 1).

Example 1:

Input: [3,6,9,12]
Output: 4
Explanation:
The whole array is an arithmetic sequence with steps of length = 3.
Example 2:

Input: [9,4,7,2,10]
Output: 3
Explanation:
The longest arithmetic subsequence is [4,7,10].
Example 3:

Input: [20,1,15,3,10,5,8]
Output: 4
Explanation:
The longest arithmetic subsequence is [20,15,10,5].


Note:

2 <= A.length <= 2000
0 <= A[i] <= 10000
*/


/*
dp[diff][index] + 1 equals to the length of arithmetic sequence at index with difference diff.
C++
    int longestArithSeqLength(vector<int>& A) {
        unordered_map<int, unordered_map<int, int>> dp;
        int res = 2;
        for (int i = 0; i < A.size(); ++i)
            for (int j = i + 1; j < A.size(); ++j)  {
                int a = A[i], b = A[j];
                dp[b - a][j] = max(dp[b - a][j], dp[b - a][i] + 1);
                res = max(res, dp[b - a][j] + 1);
            }
        return res;
    }
Python:
    def longestArithSeqLength(self, A):
        dp = collections.defaultdict(int)
        for i in xrange(len(A)):
            for j in xrange(i + 1, len(A)):
                a, b = A[i], A[j]
                dp[b - a, j] = max(dp[b - a, j], dp[b - a, i] + 1)
        return max(dp.values()) + 1

Java from @billycheung
    public int longestArithSeqLength(int[] A) {
        Map<Integer, Map<Integer, Integer>> dp = new HashMap<>(); // <difference, <Index of Element for this difference, count of sequence>>
        int max = 2;
        for (int i = 0; i < A.length; i++) {
            for (int j = i + 1; j < A.length; j++) {
                int a = A[i], b = A[j];
                Map<Integer, Integer> diffMap = dp.computeIfAbsent(b - a, c -> new HashMap<>());
                int currMax = Math.max(diffMap.getOrDefault(j, 0), diffMap.getOrDefault(i, 0) + 1); // if the element for the difference is i (variable a = A[i]), then we can add 1 to the count of sequence
                diffMap.put(j, currMax);
                max = Math.max(max, currMax + 1);
            }
        }
        return max;
    }


    int n = A.length;
    int ret = 0;
    int[] dpt = new int[10001];
    int[] dp = new int[n];
    for(int d = -10000;d <= 10000;d++){
        for(int i = n-1;i >= 0;i--){
            int ne = A[i] + d;
            if(0 <= ne && ne <= 10000){
                dp[i] = dpt[ne] + 1;
            }else{
                dp[i] = 1;
            }
            dpt[A[i]] = Math.max(dpt[A[i]], dp[i]);
            ret = Math.max(ret, dp[i]);
        }
        for(int i = n-1;i >= 0;i--){
            dpt[A[i]] = 0;
        }
    }
    return ret;

The main idea is to maintain a map of differences seen at each index.

We iteratively build the map for a new index i, by considering all elements to the left one-by-one.
For each pair of indices (i,j) and difference d = A[i]-A[j] considered, we check if there was an existing chain at the index j with difference d already.

If yes, we can then extend the existing chain length by 1.
Else, if not, then we can start a new chain of length 2 with this new difference d and (A[j], A[i]) as its elements.
At the end, we can then return the maximum chain length that we have seen so far.

Time Complexity: O(n^2)
Space Complexity: O(n^2)
class Solution {
    public int longestArithSeqLength(int[] A) {
        if (A.length <= 1) return A.length;

        int longest = 0;

        // Declare a dp array that is an array of hashmaps.
        // The map for each index maintains an element of the form-
        //   (difference, length of max chain ending at that index with that difference).
        HashMap<Integer, Integer>[] dp = new HashMap[A.length];

        for (int i = 0; i < A.length; ++i) {
            // Initialize the map.
            dp[i] = new HashMap<Integer, Integer>();
        }

        for (int i = 1; i < A.length; ++i) {
            int x = A[i];
            // Iterate over values to the left of i.
            for (int j = 0; j < i; ++j) {
                int y = A[j];
                int d = x - y;

                // We at least have a minimum chain length of 2 now,
                // given that (A[j], A[i]) with the difference d,
                // by default forms a chain of length 2.
                int len = 2;

                if (dp[j].containsKey(d)) {
                    // At index j, if we had already seen a difference d,
                    // then potentially, we can add A[i] to the same chain
                    // and extend it by length 1.
                    len = dp[j].get(d) + 1;
                }

                // Obtain the maximum chain length already seen so far at index i
                // for the given differene d;
                int curr = dp[i].getOrDefault(d, 0);

                // Update the max chain length for difference d at index i.
                dp[i].put(d, Math.max(curr, len));

                // Update the global max.
                longest = Math.max(longest, dp[i].get(d));
            }
        }

        return longest;
    }
}
*/