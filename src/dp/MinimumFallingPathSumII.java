/*
Given a square grid of integers arr, a falling path with non-zero shifts is a choice of exactly one element from each row of arr, such that no two elements chosen in adjacent rows are in the same column.

Return the minimum sum of a falling path with non-zero shifts.



Example 1:

Input: arr = [[1,2,3],[4,5,6],[7,8,9]]
Output: 13
Explanation:
The possible falling paths are:
[1,5,9], [1,5,7], [1,6,7], [1,6,8],
[2,4,8], [2,4,9], [2,6,7], [2,6,8],
[3,4,8], [3,4,9], [3,5,7], [3,5,9]
The falling path with the smallest sum is [1,5,7], so the answer is 13.


Constraints:

1 <= arr.length == arr[i].length <= 200
-99 <= arr[i][j] <= 99
*/

package dp;

public class MinimumFallingPathSumII {
    public int minFallingPathSum(int[][] arr) {
        int n = arr.length, m = arr[0].length;
        int[] dp = new int[m];
        for(int i=0;i<n;i++) {
            int[] ndp = new int[m];
            for(int j=0;j<m;j++) {
                ndp[j] = Integer.MAX_VALUE;
                for(int k=0;k<m;k++) {
                    if(k==j) continue;
                    ndp[j] = Math.min(ndp[j], dp[k]+arr[i][j]);
                }
            }
            dp = ndp;
        }

        int ret = Integer.MAX_VALUE;
        for(int x: dp) ret=Math.min(ret, x);
        return ret;
    }
}


/*
by uwi:
public int minFallingPathSum(int[][] a) {
    int n = a.length;
    int[] dp = new int[n];
    for(int i = 0;i < n;i++){
        int[] ndp = new int[n];
        Arrays.fill(ndp, 999999999);
        for(int j = 0;j < n;j++){
            for(int k = 0;k < n;k++){
                if(j != k){
                    ndp[j] = Math.min(ndp[j], dp[k] + a[i][j]);
                }
            }
        }
        dp = ndp;
    }
    int min = 999999999;
    for(int v : dp){
        min = Math.min(min, v);
    }
    return min;
}

*/