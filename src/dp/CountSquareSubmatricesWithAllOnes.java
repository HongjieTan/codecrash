/*
Given a m * n matrix of ones and zeros, return how many square submatrices have all ones.

Example 1:

Input: matrix =
[
  [0,1,1,1],
  [1,1,1,1],
  [0,1,1,1]
]
Output: 15
Explanation:
There are 10 squares of side 1.
There are 4 squares of side 2.
There is  1 square of side 3.
Total number of squares = 10 + 4 + 1 = 15.
Example 2:

Input: matrix =
[
  [1,0,1],
  [1,1,0],
  [1,1,0]
]
Output: 7
Explanation:
There are 6 squares of side 1.
There is 1 square of side 2.
Total number of squares = 6 + 1 = 7.


Constraints:

1 <= arr.length <= 300
1 <= arr[0].length <= 300
0 <= arr[i][j] <= 1
*/
package dp;

/*
    brute force: O(n^4)
    dp: cumulative sum: O(n^3)
    dp: max square ending at (i,j):  O(n^2)
*/
public class CountSquareSubmatricesWithAllOnes {
    public int countSquares_dp2(int[][] matrix) {
        int n = matrix.length, m = matrix[0].length;
        int[][] dp = new int[n][m]; // dp[i,j] max square size ending at (i,j)
        int cnt=0;
        for(int i=0;i<n;i++) {
            for(int j=0;j<m;j++) {
                if(i==0 || j==0) {
                    dp[i][j] = matrix[i][j];
                }
                else {
                    if(matrix[i][j]==0) {
                        dp[i][j] = 0;
                    } else {
                        dp[i][j] = Math.min(dp[i-1][j-1], Math.min(dp[i-1][j], dp[i][j-1])) + 1;
                    }
                }
                cnt += dp[i][j];
            }
        }
        return cnt;
    }

    public int countSquares_dp1(int[][] matrix) {
        int m = matrix.length, n = matrix[0].length;
        int[][] cum= new int[m+1][n+1];
        for(int i=0;i<m;i++) {
            for(int j=0;j<n;j++) {
                cum[i+1][j+1]= matrix[i][j] + cum[i][j+1] + cum[i+1][j] - cum[i][j];
            }
        }

        int cnt = 0;
        for(int i=0;i<m;i++) {
            for(int j=0;j<n;j++) {
                for(int side=1; i+side-1<m&&j+side-1<n; side++) {
                    int ones = cum[i+side][j+side]-cum[i][j+side]-cum[i+side][j]+cum[i][j];
                    if(ones == side*side) {
                        cnt++;
                    }
                }
            }
        }
        return cnt;
    }

}

/*
by lee:
Explanation
dp[i][j] means the biggest square with A[i][j] as bottom-right corner.

If A[i][j] == 0, no square.

If A[i][j] == 1,
we compare the size of square dp[i-1][j-1], dp[i-1][j] and dp[i][j-1].
min(dp[i-1][j-1], dp[i-1][j], dp[i][j-1]) + 1 is the maximum size of square that we can find.


Complexity
Time O(MN)
Space O(1)


Java:

    public int countSquares(int[][] A) {
        int res = 0;
        for (int i = 0; i < A.length; ++i) {
            for (int j = 0; j < A[0].length; ++j) {
                if (A[i][j] > 0 && i > 0 && j > 0) {
                    A[i][j] = Math.min(A[i - 1][j - 1], Math.min(A[i - 1][j], A[i][j - 1])) + 1;
                }
                res += A[i][j];
            }
        }
        return res;
    }
C++:

    int countSquares(vector<vector<int>>& A) {
        int res = 0;
        for (int i = 0; i < A.size(); ++i)
            for (int j = 0; j < A[0].size(); res += A[i][j++])
                if (A[i][j] && i && j)
                    A[i][j] += min({A[i - 1][j - 1], A[i - 1][j], A[i][j - 1]});
        return res;
    }
Python:

    def countSquares(self, A):
        for i in xrange(1, len(A)):
            for j in xrange(1, len(A[0])):
                A[i][j] *= min(A[i - 1][j], A[i][j - 1], A[i - 1][j - 1]) + 1
        return sum(map(sum, A))



by uwi:
class Solution {
    public int countSquares(int[][] a) {
        int n = a.length ,m = a[0].length;
        int[][] cum = new int[n+1][m+1];
        for(int i = 0;i < n;i++){
            for(int j = 0;j < m;j++){
                cum[i+1][j+1] = cum[i+1][j] + cum[i][j+1] - cum[i][j] +
                        a[i][j];
            }
        }
        int ct = 0;
        for(int i = 0;i < n;i++){
            for(int j = 0;j < m;j++){
                for(int k = 1;i+k <= n && j+k <= m;k++){
                    int s =
                    cum[i+k][j+k] - cum[i][j+k] - cum[i+k][j]
                            + cum[i][j];
                    if(s == k*k){
                        ct++;
                    }
                }
            }
        }
        return ct;
    }
}
*/