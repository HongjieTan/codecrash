package dp;
import java.util.*;
public class LongestStringChain {


    // very good problem!
    // dp: O(n*logn + n * maxLen)
    public int longestStrChain(String[] words) {
        Arrays.sort(words, (a,b)->a.length()-b.length());
        Map<String, Integer> dp = new HashMap<>();
        for(String w: words) {
            int max = 1;
            for(int i=0;i<w.length();i++) {
                String prev = w.substring(0,i)+w.substring(i+1);
                max = Math.max(max, dp.getOrDefault(prev, 0)+1);
            }
            dp.put(w, max);
        }

        int res = 0;
        for(int v: dp.values()) res = Math.max(res, v);
        return res;
    }


    // brute force: TLE...
    class Node {
        String w; int l; String prev;
        public Node(String w,int l,String prev) {this.w=w;this.l=l;this.prev=prev;}
        public Node(String w,int l) {this.w=w;this.l=l;this.prev="";}
    }
    public int longestStrChain_bt(String[] words) {
        Set<String> dic = new HashSet<>();
        for(String w: words) dic.add(w);
        LinkedList<Node> q = new LinkedList<>();
        for(String w: dic) q.add(new Node(w,1));
        int res=0;
        while(!q.isEmpty()) {
            Node cur = q.remove();
            String w = cur.w;
            int len = cur.l;
            res = Math.max(len, res);
            for(String nb: getNbs(w, dic)) {
                q.add(new Node(nb, len+1, w));
            }
        }
        return res;
    }

    List<String> getNbs(String w, Set<String> dic) {
        List<String> nbs = new ArrayList<>();
        for(int i=0;i<=w.length();i++) {
            for(char c='a'; c<='z'; c++) {
                String nw = w.substring(0,i) + c + w.substring(i);
                if(dic.contains(nw)) nbs.add(nw);
            }
        }
        return nbs;
    }

    public static void main(String[] args) {
//        System.out.println(new LongestStringChain().longestStrChain(new String[]{
//                 "ksqvsyq","ks","kss","czvh","zczpzvdhx","zczpzvh","zczpzvhx","zcpzvh","zczvh","gr"
//        }));
    }

}
/*
Given a list of words, each word consists of English lowercase letters.
Let's say word1 is a predecessor of word2 if and only if we can add exactly one letter anywhere in word1 to make it equal to word2.
For example, "abc" is a predecessor of "abac".

A word chain is a sequence of words [word_1, word_2, ..., word_k] with k >= 1, where word_1 is a predecessor of word_2, word_2 is a predecessor of word_3, and so on.
Return the longest possible length of a word chain with words chosen from the given list of words.

Example 1:

Input: ["a","b","ba","bca","bda","bdca"]
Output: 4
Explanation: one of the longest word chain is "a","ba","bda","bdca".


Note:

1 <= words.length <= 1000
1 <= words[i].length <= 16
words[i] only consists of English lowercase letters.

*/