/*
Given an array nums of integers, we need to find the maximum possible sum of elements of the array such that it is divisible by three.

Example 1:

Input: nums = [3,6,5,1,8]
Output: 18
Explanation: Pick numbers 3, 6, 1 and 8 their sum is 18 (maximum sum divisible by 3).
Example 2:

Input: nums = [4]
Output: 0
Explanation: Since 4 is not divisible by 3, do not pick any number.
Example 3:

Input: nums = [1,2,3,4,4]
Output: 12
Explanation: Pick numbers 1, 3, 4 and 4 their sum is 12 (maximum sum divisible by 3).


Constraints:

1 <= nums.length <= 4 * 10^4
1 <= nums[i] <= 10^4
*/

package dp.Topic_MultiPartDP;

import java.util.Arrays;

public class GreatestSumDivisibleByThree {
    public int maxSumDivThree(int[] nums) {
        return maxSumDivK(nums, 3);
    }

    // dp[i] means the current maximum possible sum that sum % k = i
    int maxSumDivK(int[] nums, int k) {
        int[] dp = new int[k];
        Arrays.fill(dp, Integer.MIN_VALUE);
        dp[0] = 0;
        for(int x:nums) {
            int[] ndp = new int[k];
            for(int i=0;i<k;i++) {
                if (dp[i]!=Integer.MIN_VALUE) {
                    ndp[(x+i)%k] = Math.max(dp[(x+i)%k], x+dp[i]);
                } else {
                    ndp[(x+i)%k] = dp[(x+i)%k];
                }
            }
            dp = ndp;
        }
        return dp[0];
    }

//    def maxSumDivThree(self, A):
//    seen = [0, 0, 0]
//    for a in A:
//      for i in seen[:]:
//          seen[(i + a) % 3] = max(seen[(i + a) % 3], i + a)
//    return seen[0]

    // brute force， O(2^n)
//    int[] nums;
//    int n;
//    int ret;
//    public int maxSumDivThree(int[] nums) {
//        this.nums = nums;
//        n = nums.length;
//        dfs(0, 0);
//        return ret;
//    }
//    void dfs(int idx, int sum) {
//        if(sum%3==0) ret = Math.max(ret, sum);
//        if(idx>=n) return;
//        dfs(idx+1, sum);
//        dfs(idx+1, sum+nums[idx]);
//    }

    public static void main(String[] a) {
//        System.out.println(new GreatestSumDivisibleByThree().maxSumDivThree(new int[]{3}));
        System.out.println(new GreatestSumDivisibleByThree().maxSumDivThree(new int[]{3,6,5,1,8}));
//        System.out.println(new GreatestSumDivisibleByThree().maxSumDivThree(new int[]{4}));
//        System.out.println(new GreatestSumDivisibleByThree().maxSumDivThree(new int[]{1,2,3,4,4}));
    }
}



/*
by lee:
Explanation
seen[i] means the current maximum possible sum that sum % 3 = i


Complexity
Time O(N)
Space O(1)


Java

    public int maxSumDivThree(int[] A) {
        int[] dp = new int[]{0, Integer.MIN_VALUE, Integer.MIN_VALUE};
        for (int a : A) {
            int[] dp2 = new int[3];
            for (int i = 0; i < 3; ++i)
                dp2[(i + a) % 3] = Math.max(dp[(i + a) % 3], dp[i] + a);
            dp = dp2;
        }
        return dp[0];
    }
C++

    int maxSumDivThree(vector<int>& A) {
        vector<int> dp = {0, 0, 0}, dp2;
        for (int a : A) {
            dp2 = dp;
            for (int i: dp2) {
                dp[(i + a) % 3] = max(dp[(i + a) % 3], i + a);
            }
        }
        return dp[0];
    }
Python:

    def maxSumDivThree(self, A):
        seen = [0, 0, 0]
        for a in A:
            for i in seen[:]:
                seen[(i + a) % 3] = max(seen[(i + a) % 3], i + a)
        return seen[0]


by comment:
Thanks very much for sharing (@lee215)
Here are some detailed explanations of the posted method. I hope them help you understand lee215's brilliant post if you have doubts.

    Intuition:
        1.The last maximum possible sum that it is divisible by three could only depends
        on 3 kinds of "subroutines/subproblems":
            1. previous maximum possible sum that it is divisible by three
               preSum % 3 == 0       (example: preSum=12 if lastNum=3)
            2. preSum % 3 == 1       (example: preSum=13 if lastNum=2)
            3. preSum % 3 == 2       (example: preSum=14 if lastNum=1)
        2. This recusion + "subroutines" pattern hints Dynamic Programming

    dp state:
        dp[i] = max sum such that the reminder == i when sum / 3
    Transition:
        dp_cur[(rem + num) % 3]
            = max(dp_prev[(rem + num) % 3], dp_prev[rem]+num)
            where "rem" stands for reminder for shorter naming
        meaning:
            "Current max sum with reminder 0 or 1 or 2" could be from
            EITHER prevSum with reminder 0 or 1 or 2 consecutively
            OR     prevSum with some reminder "rem" + current number "num"

            Since (dp_prev[rem]+num) % 3 = (rem+num) % 3 = i, we are able to correctly
            update dp[i] for i = 1,2,3 each time

//method1: DP (disc) O(n)time O(1)space {6 ms, faster than 78.87%}
public int maxSumDivThree(int[] nums) {
    //init: dp[i] = max sum such that the reminder == i when sum / 3
    //dp[0]=0: max sum such that the reminder == 0 when 0 / 3 is 0
    //dp[1]=-Inf: max sum such that the reminder == 1 when 0 / 3 does not exist
    //dp[2]=-Inf: max sum such that the reminder == 2 when 0 / 3 does not exist
    int[] dp = new int[]{0, Integer.MIN_VALUE, Integer.MIN_VALUE};

    for(int num : nums){
        int[] temp = new int[3];
        //dp transition
        for(int reminder=0; reminder<3; reminder++){
            //updating each reminder for current "num"
            temp[(num+reminder)%3] = Math.max(dp[(num+reminder)%3], dp[reminder]+num);
        }
        //rotating array
        dp = temp;
    }
    //return: max sum such that the reminder == 0 when sum / 3
    return dp[0];
}


by second:
Add all together, if sum%3==0, return sum.
if sum%3==1, remove the smallest number which has n%3==1.
if sum%3==2, remove the smallest number which has n%3==2.

one pass, and we need to keep the smallest two numbers that have n1%3==1 and n2%3==2.

class Solution {
    public int maxSumDivThree(int[] nums) {
        int res = 0, leftOne = 20000, leftTwo = 20000;
        for(int n:nums){
            res+=n;
            if(n%3==1){
                leftTwo = Math.min(leftTwo,leftOne+n);
                leftOne = Math.min(leftOne,n);
            }
            if(n%3==2) {
                leftOne = Math.min(leftOne,leftTwo+n);
                leftTwo = Math.min(leftTwo,n);
            }
        }
        if(res%3==0) return res;
        if(res%3==1) return res-leftOne;
        return res - leftTwo;

    }
}
Dp solution for K problem:

class Solution {
    public int maxSumDivThree(int[] nums) {
        return maxSumDivK(nums,3);
    }
    public int maxSumDivK(int[] nums, int k){
        if(k==0) return -1;
        int[] dp = new int[k];
        for(int num : nums){
            int tmp[] = Arrays.copyOf(dp,k);
            for(int i=0;i<k;i++){
                dp[(num+tmp[i])%k] = Math.max(dp[(num+tmp[i])%k],num+tmp[i]);
            }
        }
        return dp[0];
    }
}
*/
