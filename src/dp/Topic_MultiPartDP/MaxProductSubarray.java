package dp.Topic_MultiPartDP;

/**
 *
 *  good!!!! (a little trick...)
 *
 *
 *  dp: O(n)
 *  brute force: O(n^2)
 *
 */
public class MaxProductSubarray {



    /*

        dpmax[i]: max product whose subarray ends at i
        dpmin[i]: min product whose subarray ends at i

        dpmax:
            i==0: nums[0]
            i >0: max(nums[i], nums[i]>0 ? dpmax[i-1]*nums[i]:dpmin[i-1]*nums[i])
        dpmin:
            i==0: nums[0]
            i >0: min(nums[i], nums[i]>0 ? dpmin[i-1]*nums[i]:dpmax[i-1]*nums[i])

        the answer:  max of dpmax[i]

     */
    public int maxProduct(int[] nums) {
        if(nums==null || nums.length==0) return 0;
        int ans=nums[0], max=nums[0], min=nums[0];
        for (int i=1; i<nums.length; i++) {
            int nmax=Math.max(nums[i], nums[i]>0?max*nums[i]:min*nums[i]);
            int nmin=Math.min(nums[i], nums[i]>0?min*nums[i]:max*nums[i]);
            max=nmax; min=nmin;
            ans=Math.max(ans, max);
        }
        return ans;
    }

    public static void main(String[] args) {
        System.out.println(new MaxProductSubarray().maxProduct(new int[]{-4,-3,-2}));
    }
}
