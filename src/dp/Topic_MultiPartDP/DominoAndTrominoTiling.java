package dp.Topic_MultiPartDP;

public class DominoAndTrominoTiling {
    public int numTilings(int N) {
        if(N==1) return 1;
        if(N==2) return 2;
        int mod = (int)Math.pow(10,9) + 7;

        long[] f = new long[N+1]; // 2*N
        long[] g = new long[N+1]; // 2*N-1
        f[0]=1; f[1]=1; f[2]=2;
        g[0]=0; g[1]=0; g[2]=2;
        for(int i=3;i<=N;i++) {
            f[i]=f[i-1]+f[i-2]+g[i-1];
            g[i]=g[i-1]+f[i-2]*2;
            f[i]%=mod;
            g[i]%=mod;
        }
        return (int)f[N];
    }

    /*
        类似问题（Google Interview）
        给一个2*n的board，每个格子有两种情况，为空或者被block了。现在有大小为1*2的多米诺骨牌，问这个board最多能放多少个骨牌
        思路：
        动态规划
        dp[i] 表示[0, i] 的最多骨牌
        case 1: i列上没有block
            dp[i] = dp[i - 1] + 1;
            case 1.1: 如果dp[i-1]也没有block
                dp[i] = max(dp[i], dp[i-2] + 2)

                （  dp[i-1]已经判断过了 如果没有block
                    dp[i - 1] = dp[i - 2] + 1
                    所以此时dp[i] = dp[i - 1] + 1= dp[i - 2] + 2
                    所以case1.1是多余的 对么？）
                （ 嗯，你是对的，这里可以不需要这个case ）

        case 2: i列上有一个block
            dp[i] = dp[i-1]
            case 2.1: 如果i-1列上没有block or i-1上有一个block在同侧
                dp[i] = max(dp[i], dp[i-2] + 1)
        case 3: i列上有两个block
            dp[i] = dp[i - 1]
     */


}

