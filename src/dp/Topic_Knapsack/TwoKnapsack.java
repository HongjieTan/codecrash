package dp.Topic_Knapsack;

/*
https://www.geeksforgeeks.org/double-knapsack-dynamic-programming/

Given an array ‘arr’ containing the weight of ‘N’ distinct items, and two knapsacks that can withstand ‘W1’ and ‘W2’ weights,
the task is to find the sum of the largest subset of the array ‘arr’, that can be fit in the two knapsacks.
Its not allowed to break any items in two, i.e an item should be put in one of the bags as a whole.

Examples:

Input : arr[] = {8, 3, 2}
W1 = 10, W2 = 3
Output : 13
First and third objects go in the first knapsack. The second object goes in the second knapsack. Thus, the total weight becomes 13.

Input : arr[] = {8, 5, 3}
W1 = 10, W2 = 3
Output : 11

*/
public class TwoKnapsack {
    // dp[k][i][j]  k: arr[0..i-1], i: first knapsack weight capacity, j: second knapsack weight capacity
    // dp[k,i,j] = max(dp[k-1,i,j], dp[k-1, i-w[k-1], j],  dp[k-1, i, j-w[k-1]])
    public int maxWeight(int[] w, int W1, int W2) {
        int[][][] dp = new int[w.length+1][W1+1][W2+1];

        for(int k=1;k<=w.length;k++) {
            for (int i=0;i<=W1;i++) {
                for(int j=0;j<=W2;j++) {
                    dp[k][i][j] = dp[k-1][i][j];
                    dp[k][i][j] = Math.max(dp[k][i][j], i>=w[k-1]?dp[k-1][i-w[k-1]][j]+w[k-1]:0);
                    dp[k][i][j] = Math.max(dp[k][i][j], j>=w[k-1]?dp[k-1][i][j-w[k-1]]+w[k-1]:0);
//                    System.out.println(dp[k][i][j]);
                }
            }
        }
        return dp[w.length][W1][W2];
    }

    public static void main(String[] as) {
        System.out.println(new TwoKnapsack().maxWeight(new int[]{8,2,3}, 10,3));
        System.out.println(new TwoKnapsack().maxWeight(new int[]{8,5,3}, 10,3));
    }
}
