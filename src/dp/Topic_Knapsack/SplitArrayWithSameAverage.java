package dp.Topic_Knapsack;

import java.util.HashSet;
import java.util.Set;

/**
 *
 *  - brute force: O(2^n)
 *  - prune + dp(subset sums with k elements, somehow like knapsack...): O(n^3)
 *
 */
public class SplitArrayWithSameAverage {

    public boolean splitArraySameAverage_x2(int[] A) {
        int n=A.length;
        int sum = 0;
        for(int x:A)sum+=x;

        Set<Integer>[][] dp = new Set[n+1][n/2+1];
        for(int i=0;i<=n;i++) {
            for(int j=0;j<=n/2;j++) {
                if(dp[i][j]==null) dp[i][j]=new HashSet<>();
                if(i>0 && j>0){
                    // add A[i-1]
                    if(j==1) dp[i][j].add(A[i-1]);
                    else for(int s: dp[i-1][j-1]) dp[i][j].add(s+A[i-1]);

                    // not add A[i-1]
                    dp[i][j].addAll(dp[i-1][j]);
                }
            }
        }

        for(int k=1;k<=n/2;k++) {
            if(sum*k%n==0 && dp[n][k].contains(sum*k/n)) return true;
        }
        return false;
    }

    /*
       Step1:(can skip...)
       early pruning:  TotalSum/n = BSum/k,  k=1...n/2
          => BSum = TotalSum*k/n   must be integer
          => TotalSum * k % n ==0  and 1<=k<=n/2  (In general, not many k are valid.)

       Step2:!!!!
       dp for subset sum with subset size k
       dp[i][j]  set of sums when candidates are A[0...i-1] and subset size is j

       i==0||j==0, {}
       i>0,j>0:
            dp[i,j] =  dp[i,j]
                       union (each sum in dp[i-1, j-1] added A[i-1])    (case1: include A[i-1])
                       union dp[i-1][j]                                 (case2: not include A[i-1])
     */
    public boolean splitArraySameAverage(int[] A) {
        int sum=0, n=A.length;
        for(int x:A) sum+=x;

        // early pruning, can skip...
//        boolean isPossible=false;
//        for (int k = 1; k <= n / 2; k++) {
//            if (sum*k%n==0) isPossible=true;
//        }
//        if (!isPossible) return false;

        Set<Integer>[][] dp = new Set[n+1][n/2+1];
        for (int i = 0; i < n + 1; i++) {
            for (int j = 0; j < n / 2 + 1; j++) {
                if (dp[i][j]==null) dp[i][j] = new HashSet<>();
                if (i>0 && j>0){
                    // case1: include A[i-1]
                    if (j==1) dp[i][j].add(A[i-1]); // be careful here!
                    else for (int s: dp[i-1][j-1]) dp[i][j].add(s+A[i-1]);
                    // case2: not include A[i-1]
                    dp[i][j].addAll(dp[i-1][j]);
                }
            }
        }

        for (int k = 1; k <= n / 2; k++) {
            if (sum*k%n==0 && dp[n][k].contains(sum*k/n)) return true;
        }
        return false;
    }

    public static void main(String[] args) {
//        System.out.println(new SplitArrayWithSameAverage().splitArraySameAverage(new int[]{5,3,11,19,2}));
//        System.out.println(new SplitArrayWithSameAverage().splitArraySameAverage(new int[]{1,2,3,4,5,6,7,8}));
        System.out.println(new SplitArrayWithSameAverage().splitArraySameAverage(new int[]{18,10,5,3}));

    }

}
