package dp.Topic_Knapsack;

/**
 * https://www.geeksforgeeks.org/0-1-knapsack-problem-dp-10/
 */
public class Knapsack01 {
    public int knapSack01(int W, int[] weights,int values[], int n) {
        int[][] dp = new int[W+1][n+1];
        for (int i = 0; i <= n ; i++) {
            for (int w = 0; w <= W ; w++) {
                if (i==0 || w==0) dp[i][w]=0;
                else if (weights[i-1]>w) dp[i][w] = dp[i-1][w];
                else dp[i][w]=Math.max(dp[i-1][w], values[i-1]+dp[i-1][w-weights[i-1]]);
            }
        }
        return dp[n][W];
    }

}
