package dp.Topic_Knapsack;

public class LastStoneWeightII {

    // convert to: max weight to fill sum/2 sized knapsack
    public int lastStoneWeightII(int[] stones) {
        int sum = 0;
        for (int s:stones) sum+=s;

        int[][] dp = new int[stones.length+1][sum/2+1];

        for (int i = 1; i <=stones.length; i++) {
            for (int j = 1; j <= sum/2; j++) {
                dp[i][j] = Math.max(dp[i-1][j], j-stones[i-1]>=0?dp[i-1][j-stones[i-1]]+stones[i-1]:0);
            }
        }

        return sum-2*dp[stones.length][sum/2];

    }
}


/*
We have a collection of rocks, each rock has a positive integer weight.

Each turn, we choose any two rocks and smash them together.
Suppose the stones have weights x and y with x <= y.  The result of this smash is:

If x == y, both stones are totally destroyed;
If x != y, the stone of weight x is totally destroyed, and the stone of weight y has new weight y-x.
At the end, there is at most 1 stone left.  Return the smallest possible weight of this stone
(the weight is 0 if there are no stones left.)



Example 1:

Input: [2,7,4,1,8,1]
Output: 1
Explanation:
We can combine 2 and 4 to get 2 so the array converts to [2,7,1,8,1] then,
we can combine 7 and 8 to get 1 so the array converts to [2,1,1,1] then,
we can combine 2 and 1 to get 1 so the array converts to [1,1,1] then,
we can combine 1 and 1 to get 0 so the array converts to [1] then that's the optimal value.


Note:

1 <= stones.length <= 30
1 <= stones[i] <= 100
*/

/*
Chinese explanation for contest this week on bilibili.

Intuition
Same problem as:
Divide all numbers into two groups,
what is the minimum difference between the sum of two groups.
Now it's a easy classic knapsack problem.


Brief Prove
All cases of "cancellation of rocks" can be expressed by two knapsacks.
And the last stone value equals to the difference of these two knapsacks
It needs to be noticed that the opposite proposition is wrong.

Solution 1
Explanation:
Very classic knapsack problem solved by DP.
In this solution, I use dp to record the achievable sum of the smaller group.
dp[x] = 1 means the sum x is possible.


Time Complexity:
O(NS) time,
O(S) space, where S = sum(A).


Java, use array:

    public int lastStoneWeightII(int[] A) {
        boolean[] dp = new boolean[1501];
        dp[0] = true;
        int sumA = 0, res = 100;
        for (int a : A) {
            sumA += a;
            for (int i = 1500; i >= a; --i)
                dp[i] |= dp[i - a];
        }
        for (int i = sumA / 2; i > 0; --i)
            if (dp[i]) return sumA - i - i;
        return 0;
    }
C++, use bitset:

    int lastStoneWeightII(vector<int> A) {
        bitset<1501> dp = {1};
        int sumA = 0, res = 100;
        for (int a : A) {
            sumA += a;
            for (int i = 1500; i >= a; --i)
                dp[i] = dp[i] + dp[i - a];
        }
        for (int i = sumA / 2; i > 0; --i)
            if (dp[i]) return sumA - i - i;
        return 0;
    }
Python, use set:

    def lastStoneWeightII(self, A):
        dp = {0}
        sumA = sum(A)
        for a in A:
            dp |= {a + i for i in dp}
        return min(abs(sumA - i - i) for i in dp)

Solution 2
Explanation:
Adapted dp to this problem.
In this solution, I use dp to record the achievable diff of one group.
If x in the set dp, it means the difference x is achievable currently.


Time Complexity:
O(NS) time,
O(S) space, where S = sum(A).


Python, use set:

    def lastStoneWeightII(self, A):
        dp, sumA = {0}, sum(A)
        for a in A:
            dp = {a + x for x in dp} | {a - x for x in dp}
        return min(abs(x) for x in dp)

Follow-up:
As this problem will be quite boring as Q4 (if you read my post),
I'll leave you a slightly harder problem as follow-up just for more fun.
Question: Return the biggest possible weight of this stone?
I'll talk about this in my video next week.


FAQ (Some high voted)
Q: Why the minimum result of cancellation is equal to minimum knapsack partition?
A:

One cancellation can be represented as one grouping.
One grouping can be represented as one knapsack partition.
If the grouping difference < max of bigger groups, it can be realized by a cancellation.
With the 2 conclusions above,
we can know that the minimum result of cancellation is equal to minimum grouping difference,
which we solved by knapsack problem.

Q:
In some version of solution, what does the magic number 1500 mean?
A:
The dp[i] present if the sum of one group can be i.
So we can only record the smaller one for less space cost,
which less than half of the upper bound of total sum 3000.
*/