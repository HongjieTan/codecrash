package dp.Topic_Knapsack;

public class TaskAndCpu {

    public int maxPriority(int[] cpus, int[] prioritys, int totalCpus) {
        int tasks = cpus.length;
        int[][] dp = new int[tasks+1][totalCpus+1];

        for (int i = 0; i <=tasks; i++) {
            for (int j = 0; j <=totalCpus; j++) {
                if (i==0 || j==0) dp[i][j]=0;
                else if (j-cpus[i-1]<0) dp[i][j]=dp[i-1][j];
                else {
                    dp[i][j] = Math.max(dp[i-1][j-cpus[i-1]]+prioritys[i-1], dp[i-1][j]);
                }
            }
        }
        return dp[tasks][totalCpus];
    }

    // follow up是返回这个combination, 只要把每一步的选择存起来。。。

}


/*
https://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=463515
第二轮是典型背包问题，给一些task，以及每个task的priority和需要用cpu的个数，
假设有10个cpu，求问能达到的最大priority的和，
follow up是返回这个combination
*/