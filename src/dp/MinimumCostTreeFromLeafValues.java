//Given an array arr of positive integers, consider all binary trees such that:
//
//
// Each node has either 0 or 2 children;
// The values of arr correspond to the values of each leaf in an in-order traversal of the tree. (Recall that a node is a leaf if and only if it has 0 children.)
// The value of each non-leaf node is equal to the product of the largest leaf value in its left and right subtree respectively.
//
//
// Among all possible binary trees considered, return the smallest possible sum of the values of each non-leaf node. It is guaranteed this sum fits into a 32-bit integer.
//
//
// Example 1:
//
//
//Input: arr = [6,2,4]
//Output: 32
//Explanation:
//There are two possible trees.  The first has non-leaf node sum 36, and the second has non-leaf node sum 32.
//
//    24            24
//   /  \          /  \
//  12   4        6    8
// /  \               / \
//6    2             2   4
//
//
//
// Constraints:
//
//
// 2 <= arr.length <= 40
// 1 <= arr[i] <= 15
// It is guaranteed that the answer fits into a 32-bit signed integer (ie. it is less than 2^31).
//
//
// hint1: Do a DP, where dp(i, j) is the answer for the subarray arr[i]..arr[j].
// hint2: For each possible way to partition the subarray i <= k < j, the answer is max(arr[i]..arr[k]) * max(arr[k+1]..arr[j]) + dp(i, k) + dp(k+1, j).
package dp;

import java.util.Arrays;


/*
   - dp: O(n^2), this is enough...
   - stack: O(n), check lee's solution..
 */
public class MinimumCostTreeFromLeafValues {
    // bottom up dp
    //  dp(i,j) = dp(i,k) + dp(k+1,j) + max(A[i...k])*max(A[k+1...j])
    //  O(n^3) or O(n^2) by caching range max...
    public int mctFromLeafValues(int[] arr) {
        int n =  arr.length;
        int[][] dp = new int[n][n];
        for(int len=1;len<=n;len++) {
            for(int i=0; i+len-1<n; i++) {
                int j=i+len-1;
                if(len==1) dp[i][j] = 0;
                else {
                    dp[i][j]=Integer.MAX_VALUE;
                    for(int k=i;k<j;k++) {
                        int maxl = 0;
                        for(int p=i;p<=k;p++) maxl = Math.max(maxl, arr[p]);  // this loop can be improved to achieve O(n^2)
                        int maxr = 0;
                        for(int p=k+1;p<=j;p++) maxr = Math.max(maxr, arr[p]);
                        dp[i][j] = Math.min(dp[i][j], dp[i][k]+dp[k+1][j]+maxl*maxr);
                    }
                }
            }
        }
        return dp[0][n-1];
    }

    // top down version
    int[][][] dp;
    public int mctFromLeafValues_v1(int[] arr) {
        int n=arr.length;
        dp = new int[n][n][2];
        for(int i=0;i<n;i++) for(int j=0;j<n;j++)  Arrays.fill(dp[i][j], -1);
        int[] res = helper(arr, 0, n-1);
        return res[0];
    }
    int[] helper(int[] arr, int l, int r) {
        if(l>r) return new int[]{0,0};
        if(l==r) return new int[]{0, arr[r]};
        if(dp[l][r][0]!=-1) return dp[l][r];

        int maxleaf = 0, minsum=Integer.MAX_VALUE;
        for(int i=l;i<r;i++) {
            int[] left = helper(arr, l, i);
            int[] right = helper(arr, i+1, r);
            minsum = Math.min(minsum, left[0]+right[0]+left[1]*right[1]);
            maxleaf = Math.max(maxleaf, Math.max(left[1], right[1]));
        }

        dp[l][r][0] = minsum;
        dp[l][r][1] = maxleaf;
        return dp[l][r];
    }

    public static void main(String[] as) {
        int[] arr = {6,2,4};
        System.out.println(new MinimumCostTreeFromLeafValues().mctFromLeafValues(arr));
    }
}


/*
by uwi:
class Solution {
    public int mctFromLeafValues(int[] arr) {
        int n =arr.length;
        long[][] dp = new long[n][n];
        for(int len = 1;len <= n;len++){
            for(int i = 0;i+len-1 < n;i++){
                int j = i+len-1;
                if(len == 1){
                    dp[i][i] = 0;
                }else{
                    dp[i][j] = Long.MAX_VALUE;
                    for(int k = i+1;k <= j;k++){
                        int maxl = 0;
                        for(int l = i;l < k;l++){
                            maxl = Math.max(maxl, arr[l]);
                        }
                        int maxr = 0;
                        for(int l = k;l <= j;l++){
                            maxr = Math.max(maxr, arr[l]);
                        }
                        dp[i][j] = Math.min(dp[i][j], dp[i][k-1] + dp[k][j] + maxl*maxr);
                    }
                }
            }
        }
        return (int)dp[0][n-1];
    }
}

*/

/*
216

Solution DP
Find the cost for the interval [i,j].
To build up the interval [i,j],
we need to split it into left subtree and sub tree,
dp[i, j] = dp[i, k] + dp[k + 1, j] + max(A[i, k]) * max(A[k + 1, j])

This solution is O(N^3) time and O(N^2) space. You think it's fine.
After several nested for loop, you get a green accepted and release a sigh.

Great practice for DP!
Then you think that's it for a medium problem, nothing can stop you.

so you didn't read and up-vote my post.
One day, you bring this solution to an interview and probably fail.


Intuition
When we build a node in the tree, we compared the two numbers a and b.
In this process,
the smaller one is removed and we won't use it anymore,
and the bigger one actually stays.

The problem can translated as following:
Given an array A, choose two neighbors in the array a and b,
we can remove the smaller one min(a,b) and the cost is a * b.
What is the minimum cost to remove the whole array until only one left?

To remove a number a, it needs a cost a * b, where b >= a.
So a has to be removed by a bigger number.
We want minimize this cost, so we need to minimize b.

b has two candidates, the first bigger number on the left,
the first bigger number on the right.

The cost to remove a is a * min(left, right).


Explanation
Now we know that, this is not a dp problem.
(Because dp solution test all ways to build up the tree, it's kinda of brute force)

With the intuition above in mind,
we decompose a hard problem into reasonable easy one:
Just find the next greater element in the array, on the left and one right.
Refer to 1019. Next Greater Node In Linked List


Complexity
Time O(N) for one pass
Space O(N) for stack in the worst cases


Java:

    public int mctFromLeafValues(int[] A) {
        int res = 0, n = A.length;
        Stack<Integer> stack = new Stack<>();
        stack.push(Integer.MAX_VALUE);
        for (int a : A) {
            while (stack.peek() <= a) {
                int mid = stack.pop();
                res += mid * Math.min(stack.peek(), a);
            }
            stack.push(a);
        }
        while (stack.size() > 2) {
            res += stack.pop() * stack.peek();
        }
        return res;
    }
C++:

    int mctFromLeafValues(vector<int>& A) {
        int res = 0, n = A.size();
        vector<int> stack = {INT_MAX};
        for (int a : A) {
            while (stack.back() <= a) {
                int mid = stack.back();
                stack.pop_back();
                res += mid * min(stack.back(), a);
            }
            stack.push_back(a);
        }
        for (int i = 2; i < stack.size(); ++i) {
            res += stack[i] * stack[i - 1];
        }
        return res;
    }
Python:

    def mctFromLeafValues(self, A):
        res, n = 0, len(A)
        stack = [float('inf')]
        for a in A:
            while stack[-1] <= a:
                mid = stack.pop()
                res += mid * min(stack[-1], a)
            stack.append(a)
        while len(stack)  >2:
            res += stack.pop() * stack[-1]
        return res


Upvote as usual. Maybe for most companies, DP is enough, haha.

7
Hide 4 replies
Reply
Share
Report
drunkpiano's avatar
drunkpiano
19
July 22, 2019 11:47 AM

Read More
@migfulcrum algorithm is tiring, so they just want to bring some humorous vibes to make you relax and interested

0
Reply
Share
Report
migfulcrum's avatar
migfulcrum
253
Last Edit: July 22, 2019 3:37 AM

Read More
@lee215 Guys could you please help me understand - what is fun about reading the problem description which has length of Stephen King's novel? I just don't get it, isn't it annoying to you?
Edit: I just clicked on the random question: "Every person likes prime numbers. Alice is a person, thus she also shares the love for them. Bob wanted to give her an affectionate gift but couldn't think of anything inventive. Hence, he will be giving her a graph. How original, Bob! Alice will surely be thrilled!" - WTF??

1
Reply
Share
Report
lee215's avatar
lee215
20164
Last Edit: July 22, 2019 1:06 AM

Read More
@pavitra_ag Answer to your question: yes I should practice and cf has more fun ... just don't get time for it.

0
Reply
Share
Report
pavitra_ag's avatar
pavitra_ag
29
July 21, 2019 4:25 PM

Read More
@lee215 are you on codeforces??
*/