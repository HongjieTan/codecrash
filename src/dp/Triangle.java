package dp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Triangle {

    // dp （space 优化就用双数组滚动。。。）, 注意DP的顺序！！
    public int minimumTotal(List<List<Integer>> triangle) {
        int n = triangle.size();
        List<List<Integer>> dp = new ArrayList<>();
        for(int i=0;i<n;i++) dp.add(new ArrayList<>());
        for(int i=n-1;i>=0;i--) {
            for (int j = 0; j < triangle.get(i).size(); j++) {
                if (i==n-1) dp.get(i).add(triangle.get(i).get(j));
                else dp.get(i).add(Math.min(dp.get(i+1).get(j), dp.get(i+1).get(j+1)) + triangle.get(i).get(j));
            }
        }
        return dp.get(0).get(0);
    }

    public static void main(String[] args) {
        List<List<Integer>> triangle = new ArrayList<>();
        triangle.add(Arrays.asList(2));
        triangle.add(Arrays.asList(3,4));
        triangle.add(Arrays.asList(6,5,7));
        triangle.add(Arrays.asList(4,1,8,3));
        System.out.println(new Triangle().minimumTotal(triangle));
    }
}

/*
Given a triangle, find the minimum path sum from top to bottom. Each step you may move to adjacent numbers on the row below.

For example, given the following triangle

[
     [2],
    [3,4],
   [6,5,7],
  [4,1,8,3]
]
The minimum path sum from top to bottom is 11 (i.e., 2 + 3 + 5 + 1 = 11).

Note:

Bonus point if you are able to do this using only O(n) extra space, where n is the total number of rows in the triangle.
*/