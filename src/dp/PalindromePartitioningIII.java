/*
You are given a string s containing lowercase letters and an integer k. You need to :

First, change some characters of s to other lowercase English letters.
Then divide s into k non-empty disjoint substrings such that each substring is palindrome.
Return the minimal number of characters that you need to change to divide the string.



Example 1:

Input: s = "abc", k = 2
Output: 1
Explanation: You can split the string into "ab" and "c", and change 1 character in "ab" to make it palindrome.


Example 2:

Input: s = "aabbc", k = 3
Output: 0
Explanation: You can split the string into "aa", "bb" and "c", all of them are palindrome.


Example 3:

Input: s = "leetcode", k = 8
Output: 0


Constraints:

1 <= k <= s.length <= 100.
s only contains lowercase English letters.
*/
package dp;

import java.util.Arrays;

public class PalindromePartitioningIII {
    // two dp
    public int palindromePartition(String s, int k) {
        int n = s.length();
        char[] chs = s.toCharArray();

        // cost[i,j] stores min changes needed for each substring s[i...j]
        int[][] cost = new int[n][n];
        for(int len=1;len<=n;len++) {
            for(int i=0,j=i+len-1;j<n;i++,j++) {
                if(len<=2){
                    cost[i][j] = chs[i]==chs[j]?0:1;
                } else {
                    cost[i][j] = chs[i]==chs[j]?cost[i+1][j-1]:cost[i+1][j-1]+1;
                }
            }
        }

        // dp2
        int[] dp = new int[n+1];
        Arrays.fill(dp, 999999999);
        dp[0] = 0;
        for(int p=0;p<k;p++) {
            int[] ndp = new int[n+1];
            Arrays.fill(ndp, 999999999);
            for(int i=1;i<=n;i++) {
                for(int j=1;j<=i;j++) {
                    ndp[i] = Math.min(ndp[i], dp[j-1]+cost[j-1][i-1]);
                }
            }
            dp = ndp;
        }
        return dp[n];

//        // dp[i,j] store min changes needed to make j palindromes from s[0...i]
//        int[][] dp = new int[n][k+1];
//        for(int i=0;i<n;i++) {
//            for(int j=1;j<=k;j++) {
//                if (j==1) dp[i][j] = cost[0][i];
//                else {
//                    dp[i][j] = Integer.MAX_VALUE;
//                    for(int p=0;p<=i;p++) {
//                        if(j-1>p) continue;
//                        dp[i][j] = Math.min(dp[i][j], dp[p-1][j-1]+cost[p][i]);
//                    }
//                }
//            }
//        }
//        return dp[n-1][k];
    }

    public static void main(String[] as) {
        System.out.println(new PalindromePartitioningIII().palindromePartition("a", 1));
        System.out.println(new PalindromePartitioningIII().palindromePartition("ab", 1));
        System.out.println(new PalindromePartitioningIII().palindromePartition("abc", 2));
        System.out.println(new PalindromePartitioningIII().palindromePartition("aabbc", 3));
        System.out.println(new PalindromePartitioningIII().palindromePartition("leetcode", 8));
    }
}











/*

by top:
class Solution:
    def palindromePartition(self, s: str, k: int) -> int:
        n = len(s)
        memo = {}

        def cost(s,i,j): #calculate the cost of transferring one substring into palindrome string
            r = 0
            while i < j:
                if s[i] != s[j]:
                    r += 1
                i += 1
                j -= 1
            return r

        def dfs(i, k):
            if (i, k) in memo: return memo[(i, k)] #case already in memo
            if n - i == k: #base case that each substring just have one character
                return 0
            if k == 1:    #base case that need to transfer whole substring into palidrome
                return cost(s, i, n - 1)
            res = float('inf')
            for j in range(i + 1, n - k + 2): # keep making next part of substring into palidrome
                res = min(res, dfs(j, k - 1) + cost(s,i, j - 1)) #compare different divisions to get the minimum cost
            memo[(i, k)] = res
            return res
        return dfs(0 , k)
# I really appreciate it if u vote up! （￣︶￣）↗



by uwi:
	class Solution {
	    public int palindromePartition(String S, int K) {
	        char[] s = S.toCharArray();
	        int n = s.length;
	        int[][] cost = new int[n][n+1];
	        for(int i = 0;i < n;i++){
	        	for(int j = i+1;j <= n;j++){
	        		for(int k = i, l = j-1;k < l;k++,l--){
	        			if(s[k] != s[l]){
	        				cost[i][j]++;
	        			}
	        		}
	        	}
	        }

	        int[] dp = new int[n+1];
	        Arrays.fill(dp, 99999999);
	        dp[0] = 0;
	        for(int z = 0;z < K;z++){
	        	int[] ndp = new int[n+1];
		        Arrays.fill(ndp, 99999999);
	        	for(int i = 0;i <= n;i++){
	        		for(int j = i+1;j <= n;j++){
	        			ndp[j] = Math.min(ndp[j], dp[i] + cost[i][j]);
	        		}
	        	}
	        	dp = ndp;
	        }
	        return dp[n];
	    }
	}
*/
