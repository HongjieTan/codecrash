有些场景下，Top Down DP 的方式更适合：
如：

1. ...
    TargetSum (bottom-up也方便...)
    ColaMachine
    WaterAndJug

2. 罗列所有路径问题（子问题会重复...）：
    WordBreakII
    AllPathsBetweenNodes/AllPaths

