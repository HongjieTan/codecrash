package dp.Topic_TopDownDP_DfsWithMemo;

import java.util.HashMap;
import java.util.Map;

/**
 *  All paths related problem.... dfs with memo...
 *
 *  dfs with memo or dp: O(n * rangeOfSum)   cool!!
 *  dfs: O(2^n)
 *
 */
public class TargetSum {

    public static int findTargetSumWays_topdown_x2(int[] nums, int S) {
        Map<String, Integer> cache = new HashMap<>();
        return ways(nums, nums.length-1, S, cache);
    }

    static int ways(int[] nums, int idx, int s, Map<String, Integer> cache) {
        if(idx<0) return s==0?1:0;
        String key = idx+"#"+s;
        if(cache.containsKey(key)) return cache.get(key);

        int sum = 0;
        sum+= ways(nums, idx-1, s-nums[idx], cache);
        sum+= ways(nums, idx-1, s+nums[idx], cache);
        cache.put(key, sum);
        return sum;
    }

    public static int findTargetSumWays_bottomup_x2(int[] nums, int S) {
        int n = nums.length;
        int sum = 0;
        for(int x: nums) sum+=x;
        if (S>sum || S<-sum) return 0;
        int[][] dp = new int[n+1][2*sum+1]; // dp[i][j]: ways of nums[0...i-1] with S equals j-sum

        // dp[i][j] = dp[i-1][j-nums[i-1]]+dp[i-1][j+nums[i-1]]
        for(int i=0;i<=n;i++) {
            for(int j=0; j<=2*sum; j++) {
                if (i==0) dp[i][j]=j==sum?1:0;
                else {
                    if (j-nums[i-1]>=0) dp[i][j]+=dp[i-1][j-nums[i-1]];
                    if (j+nums[i-1]<=2*sum) dp[i][j]+=dp[i-1][j+nums[i-1]];
                }
            }
        }
        return dp[n][S+sum];
    }


    // dfs with memo
    Map<String, Integer> cache = new HashMap<>(); // key: index#pathSum,  value: count
    public int findTargetSumWays_dfs_with_memo(int[] nums, int S) {
        return dfs(nums, 0, 0, S);
    }
    private int dfs(int[] nums, int pathSum, int index, int S) {
        String key = index+"#"+pathSum;
        if (cache.containsKey(key)) return cache.get(key);
        if (index == nums.length ) return S==pathSum?1:0;

        int count1 = dfs(nums, pathSum+nums[index], index+1, S);
        int count2 = dfs(nums, pathSum-nums[index], index+1, S);
        cache.put(key, count1+count2);
        return cache.get(key);
    }

    /*
        DP    Note: The sum of elements in the given array will not exceed 1000.
        dp[i][j]   i: index of nums,    j: pathSum of current index 0~2000(-1000~1000 add offset 1000)

        i==n && j==S+1000, dp[i][j]=1
        i==n && j!=S+1000, dp[i][j]=0
        i<n, j=0-2000:
            dp[i][j] = dp[i+1][j+nums[i]] + dp[i+1][j-nums[i]]
     */
    public int findTargetSumWays(int[] nums, int S) {
        if (S>1000 || S<-1000) return 0;
        int n = nums.length;
        int[][] dp = new int[n+1][2001]; //...
        for (int i = n; i>=0; i--) {
            for(int j=0; j<=2000; j++) {
                if (i==n) {
                    dp[i][j] = j==(S+1000)?1:0;
                } else {
                    dp[i][j] = ((j+nums[i]<=2000)?dp[i+1][j+nums[i]]:0)
                            + ((j-nums[i]>=0)?dp[i+1][j-nums[i]]:0);
                }
            }
        }
        return dp[0][0+1000];
    }

    // dfs naive  707 ms, will cause TLE!!
    int ans = 0;
    public int findTargetSumWays_naive(int[] nums, int S) {
        dfs(0, nums, 0, S);
        return ans;
    }
    private void dfs(int pathSum, int[] nums, int index, int S) {
        if (index==nums.length && pathSum==S) ans++;
        if (index>=nums.length) return;
        dfs(pathSum+nums[index], nums,index+1, S);
        dfs(pathSum-nums[index], nums,index+1, S);
    }

    public static void main(String[] args) {
        int[] nums = new int[]{1,1,1,1,1};
        System.out.println(new TargetSum().findTargetSumWays(nums, 3));
    }


}
