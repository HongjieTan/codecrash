package dp.Topic_MultiDirectionDP;

import java.util.Arrays;

/**
 * Created by thj on 2018/7/10.
 *
 * https://leetcode.com/problems/candy/description/
 *
 */
public class Candy {


    // 2. dp(like presum) in 2 directions: O(n)
    public int candy(int[] ratings) {
        int[] left = new int[ratings.length];
        int[] right = new int[ratings.length];
        Arrays.fill(left, 1);
        Arrays.fill(right, 1);

        for (int i = 0; i < left.length; i++) {
            if (i-1>=0 && ratings[i]>ratings[i-1]) left[i] = left[i-1]+1;
        }

        for (int i = right.length-1; i >=0 ; i--) {
            if (i+1<right.length && ratings[i]>ratings[i+1]) right[i] = right[i+1]+1;
        }

        int sum=0;
        for (int i = 0; i < ratings.length; i++) {
            sum+= Math.max(left[i], right[i]);
        }

        return sum;
    }

    // 1. brute force  O(n^2)
    public int candy_bt(int[] ratings) {
        int[] arr = new int[ratings.length];
        Arrays.fill(arr, 1);
        boolean flag = true;
        while (flag) {
            flag = false;
            for(int i=0; i<arr.length; i++) {
                if (i-1>=0 && ratings[i]>ratings[i-1] && arr[i]<=arr[i-1]) {arr[i] = arr[i-1]+1; flag=true;}
                if (i+1<arr.length && ratings[i]>ratings[i+1] && arr[i]<=arr[i+1]) {arr[i] = arr[i+1]+1; flag=true;}
            }
        }
        int sum=0;
        for (int i = 0; i < arr.length; i++) sum += arr[i];
        return sum;
    }


    public static void main(String[] args) {
//        System.out.println(new Candy().candy_bt(new int[]{1,2,87,87,87,2,1}));
//        System.out.println(new Candy().candy_bt(new int[]{1,2,2}));
        System.out.println(new Candy().candy(new int[]{1,2,2}));

    }
}

/*
There are N children standing in a line. Each child is assigned a rating value.

You are giving candies to these children subjected to the following requirements:

Each child must have at least one candy.
Children with a higher rating get more candies than their neighbors.
What is the minimum candies you must give?

Example 1:

Input: [1,0,2]
Output: 5
Explanation: You can allocate to the first, second and third child with 2, 1, 2 candies respectively.
Example 2:

Input: [1,2,2]
Output: 4
Explanation: You can allocate to the first, second and third child with 1, 2, 1 candies respectively.
             The third child gets 1 candy because it satisfies the above two conditions.
*/
