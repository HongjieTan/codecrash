package dp.Topic_MultiDirectionDP;

/**
 *
 *  Notice the difference between LongestIncreasingPathInMatrix vs LongestLineOfConsecutiveOneInMatrix!!!
 *  One thinking:
 *      -构建dp需要某种偏序关系（子解->解）,LongestIncreasingPathInMatrix存在偏序关系，LongestLineOfConsecutiveOneInMatrix需要拆分成四个方向做DP...
 *      -本质是子解已是最优，不会改变。。。
 *  1. 直观方法，沿4个方向各扫一遍：  O（m*n）, 注意对角方向遍历写法。。。
 *  2. 沿4个方向DP写法： O(m*n)， 代码上好写一些。。。 dp[i][j][k] k=0,1,2,3分别表示4各方向。。。
 *
 */
public class LongestLineOfConsecutiveOneInMatrix {
    /*
    public int longestLine(int[][] M) {
        // Write your code here
    }
    */
}


/*
Given a 01 matrix m, find the longest line of consecutive one in the matrix.
The line could be horizontal, vertical, diagonal or anti-diagonal.

The number of elements in the given matrix will not exceed 10,000.

Example
Given m =
[
    [0,1,1,0],
    [0,1,1,0],
    [0,0,0,1]
]
return 3
*/