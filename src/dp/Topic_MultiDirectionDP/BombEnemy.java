package dp.Topic_MultiDirectionDP;
/*
Given a 2D grid, each cell is either a wall 'W', an enemy 'E' or empty '0' (the number zero),
return the maximum enemies you can kill using one bomb.
The bomb kills all the enemies in the same row and column from the planted point until it hits
the wall since the wall is too strong to be destroyed.

Example
Given a grid:

0 E 0 0
E 0 W E
0 E 0 0
return 3. (Placing a bomb at (1,1) kills 3 enemies)

Notice
You can only put the bomb at an empty cell.
 */

/**

 - dp: O(n^2),  use dp to calculate enemy in four direction (like prefix sum...)
 - brute force: O(n^3)

 */
public class BombEnemy {

    public int maxKilledEnemies(char[][] grid) {
        if(grid==null || grid.length==0 || grid[0].length==0) return 0;

        int m=grid.length, n=grid[0].length;
        int[][] left = new int[m][n];
        int[][] right = new int[m][n];
        int[][] up = new int[m][n];
        int[][] down = new int[m][n];

        for(int i=0; i<m; i++) {
            left[i][0] = grid[i][0]=='E'?1:0;
            for(int j=1; j<n; j++) {
                if(grid[i][j]=='W') left[i][j]=0;
                else if (grid[i][j]=='E') left[i][j] = left[i][j-1]+1;
                else left[i][j] = left[i][j-1];
            }
        }

        for(int i=0; i<m; i++) {
            right[i][n-1] = grid[i][n-1]=='E'?1:0;
            for(int j=n-2; j>=0; j--) {
                if(grid[i][j]=='W') right[i][j]=0;
                else if (grid[i][j]=='E') right[i][j] = right[i][j+1]+1;
                else right[i][j] = right[i][j+1];
            }
        }

        for(int j=0; j<n; j++) {
            up[0][j] = grid[0][j]=='E'?1:0;
            for(int i=1; i<m; i++) {
                if(grid[i][j]=='W') up[i][j]=0;
                else if(grid[i][j]=='E') up[i][j] = up[i-1][j]+1;
                else up[i][j]=up[i-1][j];
            }
        }

        for(int j=0; j<n; j++) {
            down[m-1][j] = grid[m-1][j]=='E'?1:0;
            for(int i=m-2; i>=0; i--) {
                if(grid[i][j] == 'W') down[i][j] = 0;
                else if (grid[i][j]=='E') down[i][j] = down[i+1][j]+1;
                else down[i][j] = down[i+1][j];
            }
        }

        int max = 0;
        for (int i=0; i<m; i++) {
            for(int j=0; j<n; j++) {
                if (grid[i][j]=='0') {
                    int sum = left[i][j]+right[i][j]+up[i][j] + down[i][j];
                    max = Math.max(sum, max);
                }
            }
        }
        return max;
    }


    public static void main(String[] args) {
        char[][] grid = new char[][]{
//                "0E00","E0WE","0E00"
                "0E".toCharArray(),
//                "E0WE".toCharArray(),
//                "0E00".toCharArray()
        };
        System.out.println(new BombEnemy().maxKilledEnemies(grid));
    }
}
