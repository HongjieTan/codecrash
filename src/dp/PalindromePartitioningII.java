/*
Given a string s, partition s such that every substring of the partition is a palindrome.

Return the minimum cuts needed for a palindrome partitioning of s.

Example:

Input: "aab"
Output: 1
Explanation: The palindrome partitioning ["aa","b"] could be produced using 1 cut.
*/
package dp;

public class PalindromePartitioningII {

    // two dp
    public int minCut(String s) {
        char[] chs = s.toCharArray();
        int n = s.length();

        // dp1
        boolean[][] P = new boolean[n][n];
        for(int len=1;len<=n;len++) {
            for(int i=0,j=i+len-1; j<n; i++,j++) {
                P[i][j] = chs[i]==chs[j] && (len<=2 || P[i+1][j-1]);
            }
        }

        // dp2
        int[] dp = new int[n];
        for(int i=1;i<n;i++) {
            dp[i] = 1 + dp[i-1];
            for(int j=0;j<=i;j++) {
                if(P[j][i]) {
                    dp[i] = Math.min(dp[i], j>0?dp[j-1]+1:0);
                }
            }
        }
        return dp[n-1];
    }

    public static void main(String[] a) {
        System.out.println(new PalindromePartitioningII().minCut("aab"));
    }
}
