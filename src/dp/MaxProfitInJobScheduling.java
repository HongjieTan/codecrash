// We have n jobs, where every job is scheduled to be done from startTime[i] to endTime[i], obtaining a profit of profit[i].
//
// You're given the startTime , endTime and profit arrays, you need to output the maximum profit you can take such that there are no 2 jobs in the subset with overlapping time range.
//
// If you choose a job that ends at time X you will be able to start another job that starts at time X.
//
//
// Example 1:
//
//
//
//
//Input: startTime = [1,2,3,3], endTime = [3,4,5,6], profit = [50,10,40,70]
//Output: 120
//Explanation: The subset chosen is the first and fourth job.
//Time range [1-3]+[3-6] , we get profit of 120 = 50 + 70.
//
//
// Example 2:
//
//
//
//
//
//Input: startTime = [1,2,3,4,6], endTime = [3,5,10,6,9], profit = [20,20,100,70,60]
//Output: 150
//Explanation: The subset chosen is the first, fourth and fifth job.
//Profit obtained 150 = 20 + 70 + 60.
//
//
// Example 3:
//
//
//
//
//Input: startTime = [1,1,1], endTime = [2,3,4], profit = [5,6,4]
//Output: 6
//
//
//
// Constraints:
//
//
// 1 <= startTime.length == endTime.length == profit.length <= 5 * 10^4
// 1 <= startTime[i] < endTime[i] <= 10^9
// 1 <= profit[i] <= 10^4
//

package dp;


import java.util.Arrays;

// good!!
public class MaxProfitInJobScheduling {
    public int jobScheduling(int[] startTime, int[] endTime, int[] profit) {
        int n = profit.length;
        int[][] jobs = new int[n][3];
        for(int i=0;i<n;i++) jobs[i] = new int[]{startTime[i], endTime[i], profit[i]};

        Arrays.sort(jobs, (a,b)->a[1]-b[1]);
        int[] dp = new int[n+1];

        for(int i=1;i<=n;i++) {
            int[] job = jobs[i-1];
            int pos = Arrays.binarySearch(jobs, new int[]{0,job[0],0}, (a,b)->a[1]-b[1]);
            if(pos<0) {  pos=-(pos+1); pos--;}
            dp[i] = Math.max(dp[i-1], dp[pos+1]+job[2]);
        }
        return dp[n];
    }

    public static void main(String[] as) {
        int[] startTime, endTime, profit;
        startTime = new int[]{1,2,3,3};
        endTime = new int[]{3,4,5,6};
        profit = new int[]{50,10,40,70};
        System.out.println(new MaxProfitInJobScheduling().jobScheduling(startTime, endTime, profit));

        startTime = new int[]{1,2,3,4,6};
        endTime = new int[]{3,5,10,6,9};
        profit = new int[]{20,20,100,70,60};
        System.out.println(new MaxProfitInJobScheduling().jobScheduling(startTime, endTime, profit));

        startTime = new int[]{1,1,1};
        endTime = new int[]{2,3,4};
        profit = new int[]{5,6,4};
        System.out.println(new MaxProfitInJobScheduling().jobScheduling(startTime, endTime, profit));

        startTime = new int[]{ 3, 5, 3, 7,4 };
        endTime =   new int[]{10, 8, 8,10,9 };
        profit =    new int[]{10, 8,10, 9,9 };
        System.out.println(new MaxProfitInJobScheduling().jobScheduling(startTime, endTime, profit));
    }
}






/*
by lee:
Explanation
Sort the jobs by endTime.

dp[time] = profit means that within the first time duration,
we cam make at most profit money.
Intial dp[0] = 0, as we make profit = 0 at time = 0.

For each job = [s, e, p], where s,e,p are its start time, end time and profit,
Then the logic is similar to the knapsack problem.
If we don't do this job, nothing will be changed.
If we do this job, binary search in the dp to find the largest profit we can make before start time s.
So we also know the maximum cuurent profit that we can make doing this job.

Compare with last element in the dp,
we make more money,
it worth doing this job,
then we add the pair of [e, cur] to the back of dp.
Otherwise, we'd like not to do this job.


Complexity
Time O(NlogN) for sorting
Time O(NlogN) for binary search for each job
Space O(N)


Java, using ArrayList
From @JokerLoveAllen
    public int jobScheduling(int[] startTime, int[] endTime, int[] profit) {
        // sort by endTime
        int[][] items = new int[startTime.length][3];
        for (int i = 0; i < startTime.length; i++) {
            items[i] = new int[] {startTime[i], endTime[i], profit[i]};
        }
        Arrays.sort(items, (a1, a2)->a1[1] - a2[1]);
        List<Integer> dpEndTime = new ArrayList<>();
        List<Integer> dpProfit = new ArrayList<>();
        // init value to avoid IndexOutBoundExp
        dpEndTime.add(0);
        dpProfit.add(0);
        for (int[] item : items) {
            int s = item[0], e = item[1], p = item[2];
            // find previous endTime index
            int prevIdx = Collections.binarySearch(dpEndTime, s + 1);
            if (prevIdx < 0) {
                prevIdx = -prevIdx - 1;
            }
            prevIdx--;
            int currProfit = dpProfit.get(prevIdx) + p, maxProfit = dpProfit.get(dpProfit.size() - 1);
            if (currProfit > maxProfit) {
                dpProfit.add(currProfit);
                dpEndTime.add(e);
            }
        }
        return dpProfit.get(dpProfit.size() - 1);
    }

C++, using map
public:
    int jobScheduling(vector<int>& startTime, vector<int>& endTime, vector<int>& profit) {
        int n = startTime.size();
        vector<vector<int>> jobs;
        for (int i = 0; i < n; ++i) {
            jobs.push_back({endTime[i], startTime[i], profit[i]});
        }
        sort(jobs.begin(), jobs.end());
        map<int, int> dp = {{0, 0}};
        for (auto& job : jobs) {
            int cur = prev(dp.upper_bound(job[1]))->second + job[2];
            if (cur > dp.rbegin()->second)
                dp[job[0]] = cur;
        }
        return dp.rbegin()->second;
    }
Java solution using TreeMap

    public int jobScheduling(int[] startTime, int[] endTime, int[] profit) {
        int n = startTime.length;
        int[][] jobs = new int[n][3];
        for (int i = 0; i < n; i++) {
            jobs[i] = new int[] {startTime[i], endTime[i], profit[i]};
        }
        Arrays.sort(jobs, (a, b)->a[1] - b[1]);
        TreeMap<Integer, Integer> dp = new TreeMap<>();
        dp.put(0, 0);
        for (int[] job : jobs) {
            int cur = dp.floorEntry(job[0]).getValue() + job[2];
            if (cur > dp.lastEntry().getValue())
                dp.put(job[1], cur);
        }
        return dp.lastEntry().getValue();
    }
Python:

    def jobScheduling(self, startTime, endTime, profit):
        jobs = sorted(zip(startTime, endTime, profit), key=lambda v: v[1])
        dp = [[0, 0]]
        for s, e, p in jobs:
            i = bisect.bisect(dp, [s + 1]) - 1
            if dp[i][1] + p > dp[-1][1]:
                dp.append([e, dp[i][1] + p])
        return dp[-1][1]

by uwi:
class Solution {
    public int jobScheduling(int[] startTime, int[] endTime, int[] profit) {
    	int n = startTime.length;
        int[][] rs = new int[n][];
        for(int i = 0;i < n;i++){
        	rs[i] = new int[]{startTime[i], endTime[i], profit[i]};
        }
        Arrays.sort(rs, new Comparator<int[]>() {
			public int compare(int[] a, int[] b) {
				return a[0] - b[0];
			}
		});
        int[] xs = new int[n];
        for(int i = 0;i < n;i++){
        	xs[i] = endTime[i];
        }
        Arrays.sort(xs);
        int p = 1;
        for(int i = 1;i < n;i++){
        	if(xs[i] != xs[i-1]){
        		xs[p++] = xs[i];
        	}
        }
        xs = Arrays.copyOf(xs, p);

        long ans = 0;
        SegmentTreeRMQ st = new SegmentTreeRMQ(n+1);
        for(int i = 0;i < n;i++){
        	int ind = Arrays.binarySearch(xs, rs[i][0]);
        	if(ind < 0)ind = -ind-2;
        	int val = Math.max(0, -st.minx(0, ind+1)) + rs[i][2];
        	int to = Arrays.binarySearch(xs, rs[i][1]);
        	int ex = -st.minx(to, to+1);
        	ans = Math.max(ans, val);
        	if(val > ex){
        		st.update(to, -val);
        	}
        }
        return (int)ans;
    }

    public class SegmentTreeRMQ {
    	public int M, H, N;
    	public int[] st;

    	public SegmentTreeRMQ(int n)
    	{
    		N = n;
    		M = Integer.highestOneBit(Math.max(N-1, 1))<<2;
    		H = M>>>1;
    		st = new int[M];
    		Arrays.fill(st, 0, M, Integer.MAX_VALUE);
    	}

    	public SegmentTreeRMQ(int[] a)
    	{
    		N = a.length;
    		M = Integer.highestOneBit(Math.max(N-1, 1))<<2;
    		H = M>>>1;
    		st = new int[M];
    		for(int i = 0;i < N;i++){
    			st[H+i] = a[i];
    		}
    		Arrays.fill(st, H+N, M, Integer.MAX_VALUE);
    		for(int i = H-1;i >= 1;i--)propagate(i);
    	}

    	public void update(int pos, int x)
    	{
    		st[H+pos] = x;
    		for(int i = (H+pos)>>>1;i >= 1;i >>>= 1)propagate(i);
    	}

    	private void propagate(int i)
    	{
    		st[i] = Math.min(st[2*i], st[2*i+1]);
    	}

    	public int minx(int l, int r){
    		int min = Integer.MAX_VALUE;
    		if(l >= r)return min;
    		while(l != 0){
    			int f = l&-l;
    			if(l+f > r)break;
    			int v = st[(H+l)/f];
    			if(v < min)min = v;
    			l += f;
    		}

    		while(l < r){
    			int f = r&-r;
    			int v = st[(H+r)/f-1];
    			if(v < min)min = v;
    			r -= f;
    		}
    		return min;
    	}

    	public int min(int l, int r){ return l >= r ? 0 : min(l, r, 0, H, 1);}

    	private int min(int l, int r, int cl, int cr, int cur)
    	{
    		if(l <= cl && cr <= r){
    			return st[cur];
    		}else{
    			int mid = cl+cr>>>1;
    			int ret = Integer.MAX_VALUE;
    			if(cl < r && l < mid){
    				ret = Math.min(ret, min(l, r, cl, mid, 2*cur));
    			}
    			if(mid < r && l < cr){
    				ret = Math.min(ret, min(l, r, mid, cr, 2*cur+1));
    			}
    			return ret;
    		}
    	}

    }
}

by cuiaoxiang:
class Solution {
public:
    int jobScheduling(vector<int>& s, vector<int>& t, vector<int>& profit) {
        int n = s.size();
        vector<int> pos;
        for (int i = 0; i < n; ++i) {
            pos.push_back(s[i]);
            pos.push_back(t[i]);
        }
        sort(pos.begin(), pos.end());
        pos.erase(unique(pos.begin(), pos.end()), pos.end());
        int m = pos.size();
        vector<vector<int>> a(m);
        for (int i = 0; i < n; ++i) {
            s[i] = lower_bound(pos.begin(), pos.end(), s[i]) - pos.begin();
            t[i] = lower_bound(pos.begin(), pos.end(), t[i]) - pos.begin();
            a[t[i]].push_back(i);
        }
        vector<int> dp(m);
        for (int i = 0; i < m; ++i) {
            if (i > 0) dp[i] = dp[i - 1];
            for (auto& k : a[i]) {
                dp[i] = max(dp[i], dp[s[k]] + profit[k]);
            }
        }
        return dp[m - 1];
    }
};

*/