package dp.Topic_GameTheory;

public class CoinsInALineII {

    // Classical GameTeory DP!!!!
    public boolean firstWillWin(int[] values) {
        int n =values.length;
        if (n<=2) return true;

        int[] dp = new int[n]; // dp[i]: means the relative score of first-move player of values[i...n-1]
        dp[n-1]=values[n-1];
        dp[n-2]=values[n-1]+values[n-2];

        // !!!!注意这里的DP顺序！！ 因为是从左边拿coins。。。 典型错误：如果是从左到右DP,所依赖到子问题的结果会受到新的选择的影响（子问题的解会变），导致错误！！
        for (int i=n-3; i >= 0; i--) {
            dp[i] = Math.max(values[i]-dp[i+1], values[i]+values[i+1]-dp[i+2]);
        }
        return dp[0]>=0;
    }

    public static void main(String[] args) {
        System.out.println(new CoinsInALineII().firstWillWin(new int[]{1,2,4}));
    }
}

/*
There are n coins with different value in a line. Two players take turns to take one or two coins from left side
 until there are no more coins left. The player who take the coins with the most value wins.

Could you please decide the first player will win or lose?

If the first player wins, return true, otherwise return false.

Example
Example 1:

Input: [1, 2, 2]
Output: true
Explanation: The first player takes 2 coins.
Example 2:

Input: [1, 2, 4]
Output: false
Explanation: Whether the first player takes 1 coin or 2, the second player will gain more value.
*/