package dp.Topic_GameTheory;

/**
 * - dp: O(n^2)!!!
 * - recursion with memo (like dp): O(n^2)
 * - recursion (brute force): O(2^n)
 */
public class PredictTheWinner {


    // dp v2 : very good!!!! Classical GameTeory DP!!!!
    //    The dp[i][j] saves how much more scores that the first-in-action player will get from i to j than the second player.
    //    First-in-action means whomever moves first. You can still make the code even shorter but I think it looks clean in this way.
    public boolean PredictTheWinner_dp2(int[] nums) {
        int n = nums.length;
        int[][] dp = new int[n][n];
        for (int i = 0; i < n; i++) { dp[i][i] = nums[i]; }
        for (int len = 2; len <= n; len++) {
            for (int i = 0; i+len-1 < n; i++) {
                int j = i+len-1;
                dp[i][j] = Math.max(nums[i] - dp[i + 1][j], nums[j] - dp[i][j - 1]);
            }
        }
        return dp[0][n - 1] >= 0;
    }

    // dp v1: O(n^2)!!!
    public boolean PredictTheWinner_dp1(int[] nums) {
        int[][] dp = new int[nums.length][nums.length];
        int turn = nums.length%2==0?-1:1;
        for (int i = 0; i < nums.length; i++) dp[i][i] = turn*nums[i];

        for(int len=2; len<=nums.length; len++) {
            turn*=-1;
            for (int start=0; start<=nums.length-len; start++) {
                int end = start+len-1;
                int score1 = dp[start+1][end] + turn*nums[start];
                int score2 = dp[start][end-1] + turn*nums[end];
                dp[start][end] = turn==1?Math.max(score1, score2):Math.min(score1, score2);
            }
        }
        return dp[0][nums.length-1]>=0;
    }

    // brute force: O(2^n),  improvement: use memo, O(n^2)...
    public boolean PredictTheWinner_v1(int[] nums) {
        int score = play(nums, 0, nums.length-1, 1);
        return score>=0;
    }

    private int play(int[] nums, int start, int end, int turn) {
        if (start == end) return turn*nums[start];
        int score1 = play(nums, start+1, end, -turn) + turn*nums[start];
        int score2 = play(nums, start, end-1, -turn) + turn*nums[end];
        return turn==1?Math.max(score1, score2):Math.min(score1, score2);
    }

    public static void main(String[] args) {
//        int[] nums = new int[]{1,5,233,7};
//        int[] nums = new int[]{1,5,2};
//        int[] nums = new int[]{1,1,1};
//        System.out.println(new PredictTheWinner().PredictTheWinner( new int[]{1,5,233,7}));
//        System.out.println(new PredictTheWinner().PredictTheWinner( new int[]{1,5,2}));
//        System.out.println(new PredictTheWinner().PredictTheWinner( new int[]{1,1,1}));
    }
}


/*
Given an array of scores that are non-negative integers. Player 1 picks one of the numbers from either end of the array
followed by the player 2 and then player 1 and so on. Each time a player picks a number, that number will not be available
for the next player. This continues until all the scores have been chosen. The player with the maximum score wins.

Given an array of scores, predict whether player 1 is the winner. You can assume each player plays to maximize his score.

Example 1:
Input: [1, 5, 2]
Output: False
Explanation: Initially, player 1 can choose between 1 and 2.
If he chooses 2 (or 1), then player 2 can choose from 1 (or 2) and 5. If player 2 chooses 5, then player 1 will be left with 1 (or 2).
So, final score of player 1 is 1 + 2 = 3, and player 2 is 5.
Hence, player 1 will never be the winner and you need to return False.
Example 2:
Input: [1, 5, 233, 7]
Output: True
Explanation: Player 1 first chooses 1. Then player 2 have to choose between 5 and 7. No matter which number player 2 choose, player 1 can choose 233.
Finally, player 1 has more score (234) than player 2 (12), so you need to return True representing player1 can win.
Note:
1 <= length of the array <= 20.
Any scores in the given array are non-negative integers and will not exceed 10,000,000.
If the scores of both players are equal, then player 1 is still the winner.
*/