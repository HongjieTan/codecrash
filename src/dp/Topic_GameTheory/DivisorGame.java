package dp.Topic_GameTheory;

public class DivisorGame {
    public boolean divisorGame(int N) {
        boolean[] dp = new boolean[N+1];
        dp[1] = false;
        for(int i=2;i<=N;i++) {
            for(int x=1;x<i;x++) {
                if(i%x==0 && !dp[i-x]) {
                    dp[i]=true;
                    break;
                }
            }
        }
        return dp[N];
    }
}
