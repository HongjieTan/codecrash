package dp.Topic_GameTheory;

public class FlipCards {
    // same as others...
}
/*
3. Flipping cards: given n cards on table. Two players take turns to flip cards from the left. They can flip 1-3 cards each time and add up points on the card to their score.
Points are integers. Cards already flipped cannot be flipped again.
Question: suppose the two players play in optimal strategy to get as much score as possible, how much score can player1 get in the end?
*/