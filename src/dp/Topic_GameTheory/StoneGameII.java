//Alex and Lee continue their games with piles of stones. There are a number of piles arranged in a row, and each pile has a positive integer number of stones piles[i].
// The objective of the game is to end with the most stones.
//
// Alex and Lee take turns, with Alex starting first. Initially, M = 1.
//
// On each player's turn, that player can take all the stones in the first X remaining piles, where 1 <= X <= 2M. Then, we set M = max(M, X).
//
// The game continues until all the stones have been taken.
//
// Assuming Alex and Lee play optimally, return the maximum number of stones Alex can get.
//
//
// Example 1:
//
//
//Input: piles = [2,7,9,4,4]
//Output: 10
//Explanation:  If Alex takes one pile at the beginning, Lee takes two piles, then Alex takes 2 piles again. Alex can get 2 + 4 + 4 = 10 piles in total.
// If Alex takes two piles at the beginning, then Lee can take all three piles left. In this case, Alex get 2 + 7 = 9 piles in total. So we return 10 since it's larger.
//
//
//
// Constraints:
//
//
// 1 <= piles.length <= 100
// 1 <= piles[i] <= 10 ^ 4
//
//

package dp.Topic_GameTheory;

import java.util.Arrays;

public class StoneGameII {
    public int stoneGameII(int[] piles) {
        int n = piles.length;
        int dp[][] = new int[n+1][n+1];
        int cum[] = new int[n+1];
        for(int i=0;i<n;i++) cum[i+1] = cum[i] + piles[i];

        Arrays.fill(dp[n-1], piles[n-1]);
        for(int i=n-2;i>=0;i--) {
            for(int m=1; m<=n;m++) {
                dp[i][m] = Integer.MIN_VALUE;
                for(int x=1; x<=2*m && i+x-1<n; x++) {
                    dp[i][m] = Math.max(dp[i][m], cum[i+x]-cum[i] - dp[i+x][Math.max(x, m)]);
                }
            }
        }

        int total = 0;
        for(int x: piles) total+=x;
        return (dp[0][1]+total)/2;
    }

    public static void main(String[] as) {
        int[] ps = {1,2,3,4,5,100}; // 104
        System.out.println(new StoneGameII().stoneGameII(ps));
    }
}


/*
by uwi:
class Solution {
    int[][][] dp;
    int I = -99999999;
    int n;
    int[] cum;

    int dfs(int t, int pos, int M)
    {
        if(pos == n){
            return dp[t][pos][M] = 0;
        }

        if(dp[t][pos][M] != I)return dp[t][pos][M];

        if(t == 0){
            int mx = I;
            for(int j = 1;j <= 2*M && pos+j <= n;j++){
                int res = dfs(t^1, pos+j, Math.min(n, Math.max(M, j))) + cum[pos+j] - cum[pos];
                mx = Math.max(mx, res);
            }
            return dp[t][pos][M] = mx;
        }else{
            int mn = -I;
            for(int j = 1;j <= 2*M && pos+j <= n;j++){
                int res = dfs(t^1, pos+j, Math.min(n, Math.max(M, j)));
                mn = Math.min(mn, res);
            }
            return dp[t][pos][M] = mn;
        }
    }

    public int stoneGameII(int[] piles) {
        n = piles.length;;
        cum = new int[n+1];
        for(int i = 0;i < n;i++){
            cum[i+1] = cum[i] + piles[i];
        }
        dp = new int[2][n+1][n+1];
        for(int j = 0;j < n+1;j++){
            Arrays.fill(dp[0][j], I);
            Arrays.fill(dp[1][j], I);
        }
        return dfs(0, 0, 1);
    }
}

by lee:
def stoneGameII(self, A: List[int]) -> int:
    N = len(A)
    for i in range(N - 2, -1, -1):
        A[i] += A[i + 1]
    from functools import lru_cache
    @lru_cache(None)
    def dp(i, m):
        if i + 2 * m >= N: return A[i]
        return A[i] - min(dp(i + x, max(m, x)) for x in range(1, 2 * m + 1))
    return dp(0, 1)


by second post:
class Solution {
    private int[] sums;//the sum from piles[i] to the end
    private int[][] hash;
    public int stoneGameII(int[] piles) {
        if(piles == null || piles.length == 0) return 0;
        int n = piles.length;
        sums = new int[n];
        sums[n-1] = piles[n-1];
        for(int i = n -2; i>=0;i--) {
            sums[i] = sums[i+1] + piles[i]; //the sum from piles[i] to the end
        }

        hash = new int[n][n];
        return helper(piles, 0, 1);
    }

    private int helper(int[] a, int i, int M) {
        if(i == a.length) return 0;
        if(2*M >= a.length - i) {
            return sums[i];
        }
        if(hash[i][M] != 0) return hash[i][M];
        int min = Integer.MAX_VALUE;//the min value the next player can get
        for(int x=1;x<=2*M;x++){
            min = Math.min(min, helper(a, i+x, Math.max(M,x)));
        }
        hash[i][M] = sums[i] - min;  //max stones = all the left stones - the min stones next player can get
        return hash[i][M];
    }
}
*/