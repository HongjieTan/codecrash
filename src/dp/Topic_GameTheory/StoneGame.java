package dp.Topic_GameTheory;

public class StoneGame {

    public boolean stoneGame(int[] piles) {
        int n = piles.length;
        int[][] dp = new int[n][n];
        for(int len=1;len<=n;len++) {
            for(int i=0; i+len-1<n; i++) {
                int j=i+len-1;
                if(len==1) dp[i][j] = piles[i];
                else {
                    dp[i][j] = Integer.MIN_VALUE;
                    dp[i][j] = Math.max(dp[i][j], piles[i]-dp[i+1][j]);
                    dp[i][j] = Math.max(dp[i][j], piles[j]-dp[i][j-1]);
                }
            }
        }
        return dp[0][n-1]>0;
    }

    public boolean stoneGame_dp(int[] piles) {
        int n = piles.length;
        int[][] dp = new int[n][n];
        int turn = -1; // piles len is even...
        for (int i = 0; i < n; i++) dp[i][i] = turn * piles[i];
        for (int len = 2; len <= n; len++) {
            turn *= -1;
            for (int start = 0; start <= n - len; start++) {
                int end = start + len - 1;
                int score1 = dp[start + 1][end] + piles[start] * turn;
                int score2 = dp[start][end - 1] + piles[end] * turn;
                dp[start][end] = turn == 1 ? Math.max(score1, score2) : Math.min(score1, score2);
            }
        }
        return dp[0][n - 1] >= 0;
    }


    // sum(piles) is odd, len(piles) is even, ... it can be proved first player can always win...
    public boolean stoneGame_math(int[] piles) {
        return true;
    }
}

/*
Alex and Lee play a game with piles of stones.
There are an even number of piles arranged in a row, and each pile has a positive integer number of stones piles[i].
The objective of the game is to end with the most stones.
The total number of stones is odd, so there are no ties.
Alex and Lee take turns, with Alex starting first.
Each turn, a player takes the entire pile of stones from either the beginning or the end of the row.
This continues until there are no more piles left, at which point the person with the most stones wins.

Assuming Alex and Lee play optimally, return True if and only if Alex wins the game.

Example 1:

Input: [5,3,4,5]
Output: true
Explanation:
Alex starts first, and can only take the first 5 or the last 5.
Say he takes the first 5, so that the row becomes [3, 4, 5].
If Lee takes 3, then the board is [4, 5], and Alex takes 5 to win with 10 points.
If Lee takes the last 5, then the board is [3, 4], and Alex takes 4 to win with 9 points.
This demonstrated that taking the first 5 was a winning move for Alex, so we return true.


Note:

2 <= piles.length <= 500
piles.length is even.
1 <= piles[i] <= 500
sum(piles) is odd.
*/