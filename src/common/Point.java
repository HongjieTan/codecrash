package common;

/**
 * Created by thj on 24/05/2018.
 */
public class Point {
    public int x;
    public int y;
    public Point() { x = 0; y = 0; }
    public Point(int a, int b) { x = a; y = b; }
}