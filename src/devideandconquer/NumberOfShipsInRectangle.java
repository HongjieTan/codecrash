/*
(This problem is an interactive problem.)

On the sea represented by a cartesian plane, each ship is located at an integer point, and each integer point may contain at most 1 ship.

You have a function Sea.hasShips(topRight, bottomLeft) which takes two points as arguments and returns true if and only if there is at least one ship in the rectangle represented by the two points, including on the boundary.

Given two points, which are the top right and bottom left corners of a rectangle, return the number of ships present in that rectangle.  It is guaranteed that there are at most 10 ships in that rectangle.

Submissions making more than 400 calls to hasShips will be judged Wrong Answer.  Also, any solutions that attempt to circumvent the judge will result in disqualification.



Example :



Input:
ships = [[1,1],[2,2],[3,3],[5,5]], topRight = [4,4], bottomLeft = [0,0]
Output: 3
Explanation: From [0,0] to [4,4] we can count 3 ships within the range.


Constraints:

On the input ships is only given to initialize the map internally. You must solve this problem "blindfolded". In other words, you must find the answer using the given hasShips API, without knowing the ships position.
0 <= bottomLeft[0] <= topRight[0] <= 1000
0 <= bottomLeft[1] <= topRight[1] <= 1000
*/

package devideandconquer;

/**
 * // This is Sea's API interface.
 * // You should not implement it, or speculate about its implementation
 * class Sea {
 *     public boolean hasShips(int[] topRight, int[] bottomLeft);
 * }
 */
public class NumberOfShipsInRectangle {
    Sea sea;
    public int countShips(Sea sea, int[] topRight, int[] bottomLeft) {
        this.sea = sea;
        return helper(bottomLeft[0], bottomLeft[1], topRight[0], topRight[1]);
    }

    int helper(int x1, int y1, int x2, int y2) {
        if(x1>x2 || y1>y2 || !sea.hasShips(new int[]{x2,y2}, new int[]{x1,y1})) return 0;
        if(x1==x2 && y1==y2 ) return 1;
        int xm = x1+(x2-x1)/2, ym = y1+(y2-y1)/2;
        int ret = 0;
        ret += helper(x1, y1, xm, ym);
        ret += helper(x1, ym+1, xm, y2);
        ret += helper(xm+1, y1, x2, ym);
        ret += helper(xm+1, ym+1, x2,y2);
        return ret;
    }

    public static void main(String[] a) {
        int[] topRight = {4,4}, bottomLeft = {0,0};
        System.out.println(new NumberOfShipsInRectangle().countShips(new Sea(), topRight, bottomLeft));
    }

    static class Sea {
        int[][] ships = {{1,1},{2,2},{3,3},{5,5}};
        public boolean hasShips(int[] tr, int[] bl) {
            for (int[] p: ships) {
                int x=p[0], y=p[1];
                if (bl[0]<=x && x<=tr[0] && bl[1]<=y && y<=tr[1]) return true;
            }
            return false;
        }
    }

}



/*

hints:
Use divide and conquer technique.
Divide the query rectangle into 4 rectangles.
Use recursion to continue with the rectangles that has ships only.


by lee:
Solution 1: Quartered Search
Time O(10logMN)   (tan: why??)

Python:

    def countShips(self, sea, P, Q):
        res = 0
        if P.x >= Q.x and P.y >= Q.y and sea.hasShips(P, Q):
            if P.x == Q.x and P.y == Q.y: return 1
            mx, my = (P.x + Q.x) / 2, (P.y + Q.y) / 2
            res += self.countShips(sea, P, Point(mx + 1, my + 1))
            res += self.countShips(sea, Point(mx, P.y), Point(Q.x, my + 1))
            res += self.countShips(sea, Point(mx, my), Q)
            res += self.countShips(sea, Point(P.x, my), Point(mx + 1, Q.y))
        return res

Solution 2: 1-line Hack
Just for staying fun, to welcome Ste-fan.
Time O(1)


    def countShips(self, sea, P, Q):
        return sum(Q.x <= x <= P.x and Q.y <= y <= P.y for x, y in sea._Sea__ans)


by top:
Return 0 or 1 for trivial cases, otherwise split into four quadrants.

def countShips(self, sea, topRight, bottomLeft):
    def count((x, X), (y, Y)):
        if x > X or y > Y or not sea.hasShips(Point(X, Y), Point(x, y)):
            return 0
        if (x, y) == (X, Y):
            return 1
        xm = (x + X) / 2
        ym = (y + Y) / 2
        xRanges = (x, xm), (xm+1, X)
        yRanges = (y, ym), (ym+1, Y)
        return sum(count(xr, yr) for xr in xRanges for yr in yRanges)
    return count((bottomLeft.x, topRight.x), (bottomLeft.y, topRight.y))

by java:
We divide the current rectangle into 4 pieces in the middle.

Base case: when topRight == bottomLeft, meaning our rectangle reduces into a point in the map. We return 1 if hasShips(topRight, bottomLeft)

class Solution {
    public int countShips(Sea sea, int[] topRight, int[] bottomLeft) {
        if (!sea.hasShips(topRight, bottomLeft)) return 0;
        if (topRight[0] == bottomLeft[0] && topRight[1] == bottomLeft[1]) return 1;

        int midX = (topRight[0] + bottomLeft[0])/2;
        int midY = (topRight[1] + bottomLeft[1])/2;
        return countShips(sea, new int[]{midX, midY}, bottomLeft) +
            countShips(sea, topRight, new int[]{midX+1, midY+1}) +
            countShips(sea, new int[]{midX, topRight[1]}, new int[]{bottomLeft[0], midY+1}) +
            countShips(sea, new int[]{topRight[0], midY}, new int[]{midX+1, bottomLeft[1]});
    }
}

by darrenhp:
class Solution {
public:
  int countShips(Sea sea, vector<int> topRight, vector<int> bottomLeft) {
    int x1 = bottomLeft[0];
    int y1 = bottomLeft[1];
    int x2 = topRight[0];
    int y2 = topRight[1];
    return dfs(sea, x1, y1, x2+1, y2+1);
  }
  int dfs(Sea &sea, int x1, int y1, int x2, int y2) {
    if (x1 >= x2 || y1 >= y2) return 0;
    if (!sea.hasShips({x2-1, y2-1}, {x1, y1})) return 0;
    if (x1 + 1 == x2 && y1 + 1== y2) return 1;
    int x3 = (x1 + x2)/2, y3 = (y1 + y2)/2;
    vector<int> xs = {x1, x3, x2};
    vector<int> ys = {y1, y3, y2};
    int tot = 0;
    for (int i = 0; i < 2; i++) {
      for (int j = 0; j < 2; j++) {
        tot += dfs(sea, xs[i], ys[j], xs[i+1], ys[j+1]);
      }
    }
    return tot;
  }
};
*/