package devideandconquer;

import java.util.Arrays;
import java.util.PriorityQueue;
import java.util.Random;

/**

 - Quick Select(Partition): avg-O(n) / Worst-O(n^2) !!!   improved: shuffle the input first, O(n)!!!!

 - Heap Select(k-sized heap): O(nlogk)

 - the simplest solution: sort it and find by index, O(nlogn)

 */
public class KthLargest {
    // avg:O(n)  worst: O(n^2)
    public int findKthLargest_quick_select(int[] nums, int k) {
        shuffle(nums); // to avoid worst case!!
        k = nums.length-k;
        int l=0, r=nums.length-1;
        while (l<=r) {
            int pivot = partition(nums, l, r);
            if (pivot > k) {
                r = pivot-1;
            } else if (pivot < k) {
                l = pivot+1;
            } else {
                return nums[pivot];
            }
        }
        return -1;
    }

    private int partition(int[] nums, int l, int r) {
        int pivot = l;
        while (l<=r) {
            while (l<=r && nums[l]<=nums[pivot]) l++;
            while (l<=r && nums[r]>=nums[pivot]) r--;
            if (l>r) break;
            swap(nums, l, r);
        }
        swap(nums, pivot, r);
        return r;
    }

    private void shuffle(int[] nums) {
        Random random = new Random();
        for (int i = 0; i < nums.length; i++) {
            swap(nums, i, random.nextInt(nums.length));
        }
    }

    private void swap(int[] nums, int a, int b) {
        int tmp = nums[a];
        nums[a] = nums[b];
        nums[b] = tmp;
    }



    // O(nlogk)
    public int findKthLargest_heap_select(int[] nums, int k) {
        PriorityQueue<Integer> minHeap = new PriorityQueue<>();
        for (int x: nums) {
            minHeap.add(x);
            if (minHeap.size()>k) minHeap.poll();
        }
        return minHeap.peek();
    }


    // O(nlogn)
    public int findKthLargest_simple_sort(int[] nums, int k) {
        Arrays.sort(nums);
        return nums[nums.length-k];
    }

    public static void main(String[] args) {
        int[] nums = new int[]{2,3,6,1,3,0,5};
        System.out.println(new KthLargest().findKthLargest_quick_select(nums, 7));
    }

}
















