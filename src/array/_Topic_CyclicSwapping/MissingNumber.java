package array._Topic_CyclicSwapping;

/**
 * Created by thj on 28/05/2018.
 */
public class MissingNumber {

    // bit manipulation:  XOR ...  O(n)/O(1) best

    // Gauss' Formula :  (n+1) * (0+n)/2 - ...  O(n)/O(1) best

    // inplace hash: O(n)/O(1) , one drawback: changing the origin array
    public int missingNumber(int[] nums) {
        int i=0;
        int n = nums.length;
        while(i<n) {
            if (nums[i]<n && nums[nums[i]]!=nums[i] ) {
                swap(nums, i, nums[i]);
            } else {
                i++;
            }
        }

        i=0;
        while (i<n && nums[i] == i) {
            i++;
        }

        return i;
    }

    private void swap(int nums[], int i, int j){
        int temp = nums[i];
        nums[i] = nums[j];
        nums[j] = temp;
    }
}
