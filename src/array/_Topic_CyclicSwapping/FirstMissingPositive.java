package array._Topic_CyclicSwapping;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by thj on 28/05/2018.
 */
public class FirstMissingPositive {

    /**
     * the max value of first missing positive is nums.length+1
     *
     *  as the edge case: 1,2,3,...,n (this is max case, the missing is n+1)
     *
     */
    // constant space version!!!!! time:O(n), space:O(1)
    public int firstMissingPositive(int[] nums) {
        int n = nums.length;
        int p = 0;
        while(p<n) {
           if (nums[p]>=1 && nums[p]<=n && nums[nums[p]-1]!= nums[p]) { // notice! use nums[nums[p]-1]!= nums[p] instead of nums[p]-1!=p
               swap(nums, p, nums[p]-1);
           } else {
               p++;
           }
        }

        for (int i = 0; i < nums.length; i++) if (nums[i]!=i+1) return i+1;
        return n+1;
    }

    private void swap(int[] nums, int i, int j) {
        int temp = nums[i];
        nums[i] = nums[j];
        nums[j] = temp;
    }


    // naive version: time:O(n), space: O(n)
    public int firstMissingPositive_naive(int[] nums) {
        Set<Integer> set = new HashSet<>();
        for (int i = 0; i < nums.length; i++) set.add(nums[i]);
        for (int i = 1; i <= nums.length; i++) if (!set.contains(i)) return i;
        return nums.length+1;
    }

}
