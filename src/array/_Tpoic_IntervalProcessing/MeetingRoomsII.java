package array._Tpoic_IntervalProcessing;

import common.Interval;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;


/**
 * Created by thj on 2018/8/8.
 *
 *
 */
public class MeetingRoomsII {

    // better... treemap, O(nlogn)
    public int minMeetingRooms_v2(List<Interval> intervals) {
        TreeMap<Integer, Integer> countMap = new TreeMap();

        for (Interval it: intervals) {
            countMap.put(it.start, countMap.getOrDefault(it.start, 0)+1);
            countMap.put(it.end, countMap.getOrDefault(it.end, 0)-1);
        }

        int sum = 0, maxSum =0 ;
        for (int count: countMap.values()) {
            sum+=count;
            maxSum = Math.max(maxSum, sum);
        }

        return maxSum;
    }

    // ok, 但适用度不高。。。比如maxTime非常大。。。。
    public int minMeetingRooms(List<Interval> intervals) {
        int minTime=Integer.MAX_VALUE, maxTime=Integer.MIN_VALUE;
        for (Interval it: intervals) {
            minTime = Math.min(minTime, it.start);
            maxTime = Math.max(maxTime, it.end);
        }

        int[] count = new int[maxTime-minTime+1 +1];
        for (Interval it: intervals) {
            count[it.start-minTime] +=1;
            count[it.end-minTime+1] -=1;
        }

        int sum=0, maxSum=0;
        for (int i = 0; i < count.length; i++) {
            sum+=count[i];
            maxSum = Math.max(maxSum, sum);
        }
        return maxSum;
    }


    public static void main(String[] args) {
//        [(0,30),(5,10),(15,20)], return 2.

        List<Interval> list = new ArrayList<>();
        list.add(new Interval(0,30));
        list.add(new Interval(5,10));
        list.add(new Interval(15,20));

        System.out.println(new MeetingRoomsII().minMeetingRooms_v2(list));

    }


}
/*
Given an array of meeting time intervals consisting of start and end times [[s1,e1],[s2,e2],...] (si < ei),
find the minimum number of conference rooms required.

Have you met this question in a real interview?
Example
Example1

Input: intervals = [(0,30),(5,10),(15,20)]
Output: 2
Explanation:
We need two meeting rooms
room1: (0,30)
room2: (5,10),(15,20)
Example2

Input: intervals = [(2,7)]
Output: 1
Explanation:
Only need one meeting room
*/