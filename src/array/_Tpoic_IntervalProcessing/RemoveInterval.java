/*
Given a sorted list of disjoint intervals, each interval intervals[i] = [a, b] represents the set of real numbers x such that a <= x < b.
We remove the intersections between any interval in intervals and the interval toBeRemoved.
Return a sorted list of intervals after all such removals.

Example 1:
Input: intervals = [[0,2],[3,4],[5,7]], toBeRemoved = [1,6]
Output: [[0,1],[6,7]]
Example 2:
Input: intervals = [[0,5]], toBeRemoved = [2,3]
Output: [[0,2],[3,5]]

Constraints:
1 <= intervals.length <= 10^4
-10^9 <= intervals[i][0] < intervals[i][1] <= 10^9
*/

package array._Tpoic_IntervalProcessing;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RemoveInterval {
    public List<List<Integer>> removeInterval(int[][] intervals, int[] toBeRemoved) {
        List<List<Integer>> ret = new ArrayList<>();
        int s = toBeRemoved[0], e = toBeRemoved[1];
        for(int[] x: intervals) {
            if(x[1]<=s) {
                ret.add(Arrays.asList(x[0], x[1]));
            } else if(x[0]>=e) {
                ret.add(Arrays.asList(x[0], x[1]));
            } else if(x[0]>=s && x[1]<=e){
                continue;
            } else if(x[0]<=s && x[1]>=e) {
                if(x[0]!=s) ret.add(Arrays.asList(x[0], s));
                if(e!=x[1]) ret.add(Arrays.asList(e, x[1]));
            } else if(x[0]<=s && x[1]>=s){
                ret.add(Arrays.asList(x[0], s));
            } else if(x[0]<=e && x[1]>=e) {
                ret.add(Arrays.asList(e, x[1]));
            }
        }
        return ret;
    }
}


/*
by awice:
class Solution(object):
    def removeInterval(self, intervals, toBeRemoved):
        """
        :type intervals: List[List[int]]
        :type toBeRemoved: List[int]
        :rtype: List[List[int]]
        """
        def intersect((x1,y1),(x2,y2)):
            return x1<=x2<=y1 or x1<=y2<=y1 or x2<=x1<=y1<=y2

        A, B = toBeRemoved
        ans = []
        for iv in intervals:
            if not intersect(iv, toBeRemoved):
                ans.append(iv)
            else:
                a,b = iv
                if A <= a <= b <= B:
                    pass
                elif a <= A <= b <= B:
                    ans.append([a, A])
                elif A <= a <= B <= b:
                    ans.append([B, b])
                elif a <= A <= B <= b:
                    ans.append([a,A])
                    ans.append([B,b])
        return ans

by yubowenok:
typedef long long ll;
typedef vector<int> VI;
typedef pair<int,int> PII;

#define REP(i,s,t) for(int i=(s);i<(t);i++)
#define FILL(x,v) memset(x,v,sizeof(x))

const int INF = (int)1E9;
#define MAXN 100005

class Solution {
public:
    vector<vector<int>> removeInterval(vector<vector<int>>& intervals, vector<int>& toBeRemoved) {
        int l = toBeRemoved[0], r = toBeRemoved[1];
        vector<VI> res;
        REP(i,0,intervals.size()) {
          int x = intervals[i][0], y = intervals[i][1];
          if (l >= y || r <= x) {
            res.push_back(intervals[i]);
          } else if (l <= x && r >= y) {
            continue;
         } else if (r > x && l <= x) {
            res.push_back(VI({r, y}));
          } else if (l < y && r >= y) {
            res.push_back(VI({x, l}));
          } else {
            res.push_back(VI({x, l}));
            res.push_back(VI({r, y}));
          }
        }
        return res;
    }
};



by mzchen:
class Solution {
public:
    vector<vector<int>> removeInterval(vector<vector<int>>& A, vector<int>& R) {
        vector<vector<int>> r;
        for (auto &i : A) {
            if (i[1] <= R[0] || i[0] >= R[1])
                r.push_back(i);
            else if (R[0] <= i[0] && i[1] <= R[1])
                continue;
            else {
                if (i[0] < R[0])
                    r.push_back({i[0], R[0]});
                if (i[1] > R[1])
                    r.push_back({R[1], i[1]});
            }
        }
        return r;
    }
};
*/