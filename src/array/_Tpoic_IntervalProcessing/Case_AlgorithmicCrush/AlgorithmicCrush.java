package array._Tpoic_IntervalProcessing.Case_AlgorithmicCrush;

/**
 * Created by thj on 2018/7/9.
 *
 * https://www.hackerrank.com/challenges/crush/problem
 *
 *  1. brute force: o(mn)
 *  2. use PrefixSum: o(n) !!!!!!
 */
public class AlgorithmicCrush {

    static long arrayManipulation(int n, int[][] queries) {

        int[] prefix = new int[n+1];
        for(int i=0;i<prefix.length;i++) prefix[i]=0;

        for (int row=0; row<queries.length; row++) {
            int a = queries[row][0];
            int b = queries[row][1];
            int k = queries[row][2];

            prefix[a] += k;
            if (b<prefix.length-1) prefix[b+1] -= k;
        }

        long max = 0, sum = 0;
        for(int i=0;i<prefix.length;i++) {
            sum+=prefix[i];
            max = Math.max(max, sum);
        }



        return max;
    }

    static long arrayManipulationNaive(int n, int[][] queries) {
        int[] arr = new int[n];
        for (int i=0;i<arr.length;i++) arr[i]=0;

        for (int row=0; row<queries.length; row++) {
            int a = queries[row][0];
            int b = queries[row][1];
            int k = queries[row][2];

            for (int i=a-1;i<=b-1;i++) arr[i] += k;
        }

        long max = Integer.MIN_VALUE;
        for (int i = 0; i < arr.length; i++) max=Math.max(arr[i], max);

        return max;
    }



}
