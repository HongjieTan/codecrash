package array._Tpoic_IntervalProcessing;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

/*
    Treemap(Balanced Tree):
        - O(nlogn)!!  very classical interval problem!!!
    Brute force: O(n^2)
 */
public class MyCalendar {

    TreeMap<Integer, Integer> treeMap;
    public MyCalendar() {
        treeMap = new TreeMap<>();
    }

    // book: O(logn), 这里当前所有intervals都是没有重合的，故可直接用二分搜索！！与之前用累加和的形式有点区别。。。
    public boolean book(int start, int end) {
        Integer prevStart = treeMap.floorKey(start);
        Integer nextStart = treeMap.ceilingKey(start);
        if ( (prevStart==null || treeMap.get(prevStart)<=start) &&  (nextStart==null || nextStart>=end )) {
            treeMap.put(start, end);
            return true;
        }
        return false;
    }
}

// O(n^2)
class MyCalendar_bruteforce_v1 {
    List<int[]> events;
    public MyCalendar_bruteforce_v1() {
        events = new ArrayList<>();
    }
    public boolean book(int start, int end) {
        int[] eventToAdd = new int[]{start, end};
        for (int[] event: events) {
            if (haveInterval(event, eventToAdd)) return false;
        }
        events.add(eventToAdd);
        return true;
    }
    private boolean haveInterval(int[] e1, int[] e2) {
        return !(e1[1]<=e2[0] || e1[0]>=e2[1]);
    }
}

// my origin version:   it is no better than brute force..... O(n^2)  n: num of book operations
class MyCalendar_bruteforce_v0 {
    TreeMap<Integer, Integer> flagMap;
    public MyCalendar_bruteforce_v0() {
        flagMap = new TreeMap<>();
    }
    public boolean book(int start, int end) {
        flagMap.put(start, flagMap.getOrDefault(start, 0) + 1);
        flagMap.put(end, flagMap.getOrDefault(end, 0) - 1);
        int sum = 0;
        for(Integer time: flagMap.keySet()) {
            if (time>end) break;
            sum += flagMap.get(time);
            if (sum > 1) {
                flagMap.put(start, flagMap.getOrDefault(start, 0) - 1);
                flagMap.put(end, flagMap.getOrDefault(end, 0) + 1);
                return false;
            }
        }
        return true;
    }
}


/**
 * Your array._Tpoic_IntervalProcessing.MyCalendar object will be instantiated and called as such:
 * array._Tpoic_IntervalProcessing.MyCalendar obj = new array._Tpoic_IntervalProcessing.MyCalendar();
 * boolean param_1 = obj.book(start,end);
 */


//Intuition
//
//        If we maintained our events in sorted order, we could check whether an event could be booked in O(\log N)O(logN) time
//          (where NN is the number of events already booked) by binary searching for where the event should be placed.
//        We would also have to insert the event in our sorted structure.
//
//        Algorithm
//
//        We need a data structure that keeps elements sorted and supports fast insertion. In Java, a TreeMap is the perfect candidate. In Python, we can build our own binary tree structure.
//
//        For Java, we will have a TreeMap where the keys are the start of each interval, and the values are the ends of those intervals. When inserting the interval [start, end), we check if there is a conflict on each side with neighboring intervals: we would like calendar.get(prev)) <= start <= end <= next for the booking to be valid (or for prev or next to be null respectively.)
//
//        For Python, we will create a binary tree. Each node represents some interval [self.start, self.end) while self.left, self.right represents nodes that are smaller or larger than the current node.
//
//
//        Complexity Analysis
//
//        Time Complexity (Java): O(N \log N)O(NlogN), where NN is the number of events booked. For each new event, we search that the event is legal in O(\log N)O(logN) time, then insert it in O(1)O(1) time.
//
//        Time Complexity (Python): O(N^2)O(N
//        2
//        ) worst case, with O(N \log N)O(NlogN) on random data. For each new event, we insert the event into our binary tree. As this tree may not be balanced, it may take a linear number of steps to add each event.
//
//        Space Complexity: O(N)O(N), the size of the data structures used.


/*
Problem:
Implement a MyCalendar class to store your events. A new event can be added if adding the event will not cause a double booking.
Your class will have the method, book(int start, int end). Formally, this represents a booking on the half open interval [start, end),
the range of real numbers x such that start <= x < end.
A double booking happens when two events have some non-empty intersection (ie., there is some time that is common to both events.)
For each call to the method MyCalendar.book, return true if the event can be added to the calendar successfully without causing a double booking.
Otherwise, return false and do not add the event to the calendar.
Your class will be called like this: MyCalendar cal = new MyCalendar(); MyCalendar.book(start, end)
Example 1:
MyCalendar();
MyCalendar.book(10, 20); // returns true
MyCalendar.book(15, 25); // returns false
MyCalendar.book(20, 30); // returns true
Explanation:
The first event can be booked.  The second can't because time 15 is already booked by another event.
The third event can be booked, as the first event takes every time less than 20, but not including 20.
Note:
The number of calls to MyCalendar.book per test case will be at most 1000.
In calls to MyCalendar.book(start, end), start and end are integers in the range [0, 10^9].
 */