package array._Tpoic_IntervalProcessing;

import common.Interval;

import java.util.Arrays;
import java.util.List;
import java.util.TreeMap;

/**
 * Created by thj on 2018/8/8.
 */
public class MeetingRoom {


    // tree map version: O(nlogn)   452 ms
    public boolean canAttendMeetings_v2(List<Interval> intervals) {
        TreeMap<Integer, Integer> flagMap = new TreeMap<>();

        for (Interval it: intervals) {
            flagMap.put(it.start, flagMap.getOrDefault(it.start,0)+1);
            flagMap.put(it.end, flagMap.getOrDefault(it.end,0)-1);
        }

        int sum=0;
        for (int flag: flagMap.values()) { // 注意，顺序很重要！
            sum+=flag;
            if (sum>1) return false;
        }

        return true;

    }



    // my version: O(n)  402ms / but more space...but space is cheap....
    // 有点问题。。。 如果maxEnd非常大？
//    public boolean canAttendMeetings(List<Interval> intervals) {
//        // Write your code here
//        int maxEnd = 0;
//        for(Interval it: intervals) {
//            maxEnd = Math.max(maxEnd, it.end);
//        }
//
//        int[] flag = new int[maxEnd+1+1]; Arrays.fill(flag, 0);
//        for(Interval it: intervals) {
//            flag[it.start] += 1;
//            flag[it.end] -= 1;
//        }
//
//        int sum=0;
//        for (int i=0; i<=maxEnd; i++) {
//            sum+=flag[i];
//            if (sum>1) return false;
//        }
//
//        return true;
//    }
}

/*
Given an array of meeting time intervals consisting of start and end times [[s1,e1],[s2,e2],...] (si < ei),
determine if a person could attend all meetings.

Have you met this question in a real interview?
Example
Example1

Input: intervals = [(0,30),(5,10),(15,20)]
Output: false
Explanation:
(0,30), (5,10) and (0,30),(15,20) will conflict
Example2

Input: intervals = [(5,8),(9,15)]
Output: true
Explanation:
Two times will not conflict
*/