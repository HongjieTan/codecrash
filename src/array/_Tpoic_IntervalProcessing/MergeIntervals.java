package array._Tpoic_IntervalProcessing;

import common.Interval;

import java.util.*;

/**
 * Created by thj on 2018/8/8.
 *
 *   !!!!!!!!!!
 *
 *
 */
/*
*/
public class MergeIntervals {

    // O(nlogn), treemap...
    public List<Interval> merge_v2(List<Interval> intervals) {
        TreeMap<Integer, Integer> flagMap = new TreeMap<>();

        for (Interval it: intervals) {
            flagMap.put(it.start, flagMap.getOrDefault(it.start, 0)+1);
            flagMap.put(it.end, flagMap.getOrDefault(it.end, 0)-1);
        }

        List<Interval> res = new ArrayList<>();
        int sum = 0;
        Integer start = null, end = null;
        for(int time: flagMap.keySet()) {
            sum += flagMap.get(time);

            if (sum >= 0 && start == null) start = time;
            if (sum == 0 && end == null) end = time;
            if (start!=null && end!=null) {
                res.add(new Interval(start, end));
                start = null;
                end = null;
            }
        }
        return res;
    }


    // O(nlogn), sorting
    public List<Interval> merge_v1(List<Interval> intervals) {
        if (intervals==null || intervals.size()==0) return new ArrayList<>();
        Collections.sort(intervals, (a,b)->Integer.compare(a.start, b.start));
        List<Interval> ans = new ArrayList<>();
        ans.add(intervals.get(0));
        for (int i = 1; i < intervals.size(); i++) {
            Interval prevIntv = ans.get(ans.size()-1);
            Interval curIntv = intervals.get(i);

            if (curIntv.start <= prevIntv.end) {
                prevIntv.end = Math.max(curIntv.end, prevIntv.end);
            } else {
                ans.add(curIntv);
            }
        }
        return ans;
    }


    public static void main(String[] args) {
        List<Interval> list = new ArrayList<>();
//        list.add(new Interval(1, 3));
//        list.add(new Interval(2, 6));
//        list.add(new Interval(8, 10));
//        list.add(new Interval(15, 18));
        list.add(new Interval(1, 4));
        list.add(new Interval(5, 6));
        List<Interval> ans = new MergeIntervals().merge_v2(list);

    }

}
/*
LC56

Given a collection of intervals, merge all overlapping intervals.

Example 1:

Input: [[1,3],[2,6],[8,10],[15,18]]
Output: [[1,6],[8,10],[15,18]]
Explanation: Since intervals [1,3] and [2,6] overlaps, merge them into [1,6].
Example 2:

Input: [[1,4],[4,5]]
Output: [[1,5]]
Explanation: Intervals [1,4] and [4,5] are considered overlapping.
*/