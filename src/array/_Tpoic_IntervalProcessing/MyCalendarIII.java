package array._Tpoic_IntervalProcessing;


import java.util.TreeMap;

// easy...
public class MyCalendarIII {

    TreeMap<Integer, Integer> treeMap;
    public MyCalendarIII() {
        treeMap = new TreeMap<>();
    }

    public int book(int start, int end) {
        treeMap.put(start, treeMap.getOrDefault(start,0)+1);
        treeMap.put(end, treeMap.getOrDefault(end,0)-1);
        int max=0;
        int sum=0;
        for(int value: treeMap.values()) {
            sum+=value;
            max = Math.max(max, sum);
        }
        return max;
    }

}

/**
 * Your MyCalendarIII object will be instantiated and called as such:
 * MyCalendarIII obj = new MyCalendarIII();
 * int param_1 = obj.book(start,end);
 */
/*
Implement a MyCalendarIII class to store your events. A new event can always be added.

Your class will have one method, book(int start, int end). Formally, this represents a booking on the half open interval [start, end), the range of real numbers x such that start <= x < end.

A K-booking happens when K events have some non-empty intersection (ie., there is some time that is common to all K events.)

For each call to the method MyCalendar.book, return an integer K representing the largest integer such that there exists a K-booking in the calendar.

Your class will be called like this: MyCalendarIII cal = new MyCalendarIII(); MyCalendarIII.book(start, end)
Example 1:

MyCalendarIII();
MyCalendarIII.book(10, 20); // returns 1
MyCalendarIII.book(50, 60); // returns 1
MyCalendarIII.book(10, 40); // returns 2
MyCalendarIII.book(5, 15); // returns 3
MyCalendarIII.book(5, 10); // returns 3
MyCalendarIII.book(25, 55); // returns 3
Explanation:
The first two events can be booked and are disjoint, so the maximum K-booking is a 1-booking.
The third event [10, 40) intersects the first event, and the maximum K-booking is a 2-booking.
The remaining events cause the maximum K-booking to be only a 3-booking.
Note that the last event locally causes a 2-booking, but the answer is still 3 because
eg. [10, 20), [10, 40), and [5, 15) are still triple booked.


Note:

The number of calls to MyCalendarIII.book per test case will be at most 400.
In calls to MyCalendarIII.book(start, end), start and end are integers in the range [0, 10^9].
*/