package array._Tpoic_IntervalProcessing;


import java.util.*;

/**
 *
 * brute force: O(n^2)
 * treemap : O(n^2 )
 *
 */
public class MyCalendarII {

    // treemap, O(n*(n+logn)) = O(n^2)
    TreeMap<Integer, Integer> treeMap;
    public MyCalendarII() {
        treeMap = new TreeMap<>();
    }

    // book: O(n+logn)
    public boolean book(int start, int end) {
        treeMap.put(start, treeMap.getOrDefault(start, 0)+1);
        treeMap.put(end, treeMap.getOrDefault(end, 0)-1);
        int sum = 0;
        for (int count: treeMap.values()) {
            sum += count;
            if (sum>2) {
                treeMap.put(start, treeMap.get(start)-1);
                if (treeMap.get(start)==0) treeMap.remove(start);
                treeMap.put(end, treeMap.get(end)+1);
                return false;
            }
        }
        return true;
    }

    // brute force: O(n^2)
//    List<int[]> events;
//    List<int[]> overlaps;
//    public MyCalendarII() {
//        events = new ArrayList<>();
//        overlaps = new ArrayList<>();
//    }
//    public boolean book(int start, int end) {
//        int[] eventToAdd = new int[]{start, end};
//        for (int[] overlap: overlaps) {
//            if (hasOverlap(overlap, eventToAdd)) return false;
//        }
//        for (int[] event: events) {
//            if (hasOverlap(event, eventToAdd)) overlaps.add(new int[]{Math.max(event[0], eventToAdd[0]), Math.min(event[1], eventToAdd[1])});
//        }
//        events.add(eventToAdd);
//        return true;
//    }
//    private boolean hasOverlap(int[] e1, int[] e2) {
//        return !(e1[1]<=e2[0] || e1[0]>=e2[1]);
//    }

    public static void main(String[] args) {
        MyCalendarII ca = new MyCalendarII();
        System.out.println(ca.book(10,20));
        System.out.println(ca.book(50,60));
        System.out.println(ca.book(10,40));
        System.out.println(ca.book(5,15));
        System.out.println(ca.book(5,10));
        System.out.println(ca.book(25,55));
    }

//    Input
//    MyCalendarII()
//    book(10,20)
//    book(50,60)
//    book(10,40)
//    book(5,15)
//    book(5,10)
//    book(25,55)
//    Output
//[true,true,true,false,true,false]
//    Expected
//[true,true,true,false,true,true]
}

/*
Implement a MyCalendarII class to store your events. A new event can be added if adding the event will not cause a triple booking.
Your class will have one method, book(int start, int end). Formally, this represents a booking on the half open interval [start, end),
 the range of real numbers x such that start <= x < end.
A triple booking happens when three events have some non-empty intersection (ie., there is some time that is common to all 3 events.)
For each call to the method MyCalendar.book, return true if the event can be added to the calendar successfully without causing a triple booking.
Otherwise, return false and do not add the event to the calendar.
Your class will be called like this: MyCalendar cal = new MyCalendar(); MyCalendar.book(start, end)
Example 1:

MyCalendar();
MyCalendar.book(10, 20); // returns true
MyCalendar.book(50, 60); // returns true
MyCalendar.book(10, 40); // returns true
MyCalendar.book(5, 15); // returns false
MyCalendar.book(5, 10); // returns true
MyCalendar.book(25, 55); // returns true
Explanation:
The first two events can be booked.  The third event can be double booked.
The fourth event (5, 15) can't be booked, because it would result in a triple booking.
The fifth event (5, 10) can be booked, as it does not use time 10 which is already double booked.
The sixth event (25, 55) can be booked, as the time in [25, 40) will be double booked with the third event;
the time [40, 50) will be single booked, and the time [50, 55) will be double booked with the second event.

Note:

The number of calls to MyCalendar.book per test case will be at most 1000.
In calls to MyCalendar.book(start, end), start and end are integers in the range [0, 10^9].
*/