package array._Topic_2DArray;

/**
 * Created by thj on 2018/8/19.
 */
public class RotateImage {


    /**
     *
     * todo
     *
     * clockwise rotate
     * first reverse up to down, then swap the symmetry(transpose)
     * 1 2 3     7 8 9     7 4 1
     * 4 5 6  => 4 5 6  => 8 5 2
     * 7 8 9     1 2 3     9 6 3
    */
    public void rotate_v2(int[][] matrix) {
        //1. reverse
        int low = 0, high = matrix.length-1;
        while (low<high) {
            int[] temp = matrix[low];
            matrix[low] = matrix[high];
            matrix[high] = temp;
            low++;high--;
        }

        //2. transpose
        for (int row = 0; row < matrix.length; row++) {
            for (int col = row+1; col < matrix[0].length; col++) {
                int temp = matrix[row][col];
                matrix[row][col] = matrix[col][row];
                matrix[col][row] = temp;
            }
        }
    }


    public void rotate_v1(int[][] matrix) {
        int rows = matrix.length, cols = matrix[0].length;
        for (int row = 0; row < (rows+1)/2; row++) {
            for (int col = row; col < cols - row - 1; col++) {
                rotate(matrix, row, col);
            }
        }
    }


    private void rotate(int[][] matrix, int row, int col) {
        int rows = matrix.length, cols = matrix[0].length;

        int row1 = row, col1 = col;
        int row2 = col, col2 = cols-1-row;
        int row3 = rows-1-row, col3 = cols-1-col;
        int row4 = rows-1-col, col4 = row;

        int temp = matrix[row1][col1];

        matrix[row1][col1] = matrix[row4][col4];
        matrix[row4][col4] = matrix[row3][col3];
        matrix[row3][col3] = matrix[row2][col2];
        matrix[row2][col2] = temp;

    }


    public static void main(String[] args) {
        int[][] matrix = new int[][]{{1,2,3},{4,5,6},{7,8,9}};


        new RotateImage().rotate_v2(matrix);
    }

    private void print(int[][] matrix) {
        for (int row = 0; row < matrix.length; row++) {
            for (int col = 0; col < matrix[0].length; col++) {
                System.out.printf(matrix[row][col]+" ");
            }
            System.out.println();
        }
    }
}
