package array._Topic_2DArray;

import java.util.Arrays;

/*

 seems nothing special

Given a positive integer n, generate a square matrix filled with elements from 1 to n2 in spiral order.
Input: 3
Output:
[
 [ 1, 2, 3 ],
 [ 8, 9, 4 ],
 [ 7, 6, 5 ]
]
 */
public class SpiralMatrixII {

    public int[][] generateMatrix(int n) {
        int[][] dir = {{0,1},{1,0},{0,-1},{-1,0}};
        int d = 0;
        int[][] grid = new int[n][n];
        int step = 1, x=0, y=0, total = n*n;
        while(step<=total) {
            grid[x][y] = step++;
            int nx = x+dir[d][0], ny = y+dir[d][1];
            if(!(nx>=0 && nx<n && ny>=0 && ny<n && grid[nx][ny]==0)) {
                d=(d+1)%4;
                nx=x+dir[d][0];
                ny=y+dir[d][1];
            }
            x=nx;
            y=ny;
        }
        return grid;
    }

    // go straight, turn right when hit
//    public int[][] generateMatrix(int n) {
//        int[][] matrix = new int[n][n];
//        for (int i = 0; i < n; i++) Arrays.fill(matrix[i], -1);
//
//        int[][] directions = new int[][]{{0,1},{1,0},{0,-1},{-1,0}}; // the order is important...
//        int dirIdx = 0;
//        int i=0, j=0, step=1, total=n*n;
//        while (step<=total) {
//            matrix[i][j] = step++;
//            int ni = i + directions[dirIdx][0];
//            int nj = j + directions[dirIdx][1];
//            if (!(ni>=0&&ni<n&&nj>=0&&nj<n) || matrix[ni][nj]!=-1) {
//                dirIdx = (dirIdx+1)%4;
//                ni = i + directions[dirIdx][0];
//                nj = j + directions[dirIdx][1];
//            }
//            i=ni;
//            j=nj;
//        }
//        return matrix;
//    }

    public static void main(String[] args) {
        int[][] matrix = new SpiralMatrixII().generateMatrix(3);
        System.out.println("asdf");
    }

// def generateMatrix(self, n):
//    A = [[0] * n for _ in range(n)]
//    i, j, di, dj = 0, 0, 0, 1
//            for k in xrange(n*n):
//    A[i][j] = k + 1
//            if A[(i+di)%n][(j+dj)%n]:
//    di, dj = dj, -di
//    i += di
//    j += dj
//    return A
}
