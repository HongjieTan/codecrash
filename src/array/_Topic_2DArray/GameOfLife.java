package array._Topic_2DArray;

/*

Given a board with m by n cells, each cell has an initial state live (1) or dead (0).
Each cell interacts with its eight neighbors (horizontal, vertical, diagonal) using the following four rules :
Any live cell with fewer than two live neighbors dies, as if caused by under-population.
Any live cell with two or three live neighbors lives on to the next generation.
Any live cell with more than three live neighbors dies, as if by over-population..
Any dead cell with exactly three live neighbors becomes a live cell, as if by reproduction.

follow-up: solve it in-place
 */
public class GameOfLife {
    public void gameOfLife(int[][] board) {
        // 0: dead, 1:live, 2: dead->live  3, live->dead
        int[][] directions = new int[][]{{-1,0},{1, 0},{0,-1},{0,1},{-1,-1}, {-1, 1},{1,-1},{1,1} };
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[0].length; j++) {
                int liveNbCount = 0;
                for (int[] dir: directions) {
                    int nx = i+dir[0], ny = j+dir[1];
                    if (nx>=0 && nx<board.length && ny>=0 && ny<board[0].length
                            && (board[nx][ny]==1 || board[nx][ny]==3)) liveNbCount++;
                }

                if (board[i][j]==1) {
                    if (liveNbCount<2) board[i][j] = 3;
                    else if (liveNbCount<=3) board[i][j] = 1;
                    else board[i][j] = 3;
                } else {
                    if (liveNbCount==3) board[i][j] = 2;
                    else board[i][j] = 0;
                }
            }
        }

        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[0].length; j++) {
                if (board[i][j] == 2) board[i][j] = 1;
                if (board[i][j] == 3) board[i][j] = 0;
            }
        }
    }

    public static void main(String[] args) {
        int[][] board = new int[][] {
                {0,1,0},
                {0,0,1},
                {1,1,1},
                {0,0,0}};
        new GameOfLife().gameOfLife(board);
    }


}
