/*
Given a binary array data, return the minimum number of swaps required to group all 1’s present in the array together in any place in the array.



Example 1:

Input: [1,0,1,0,1]
Output: 1
Explanation:
There are 3 ways to group all 1's together:
[1,1,1,0,0] using 1 swap.
[0,1,1,1,0] using 2 swaps.
[0,0,1,1,1] using 1 swap.
The minimum is 1.
Example 2:

Input: [0,0,0,1,0]
Output: 0
Explanation:
Since there is only one 1 in the array, no swaps needed.
Example 3:

Input: [1,0,1,0,1,0,0,1,1,0,1]
Output: 3
Explanation:
One possible solution that uses 3 swaps is [0,0,0,0,0,1,1,1,1,1,1].


Note:

1 <= data.length <= 10^5
0 <= data[i] <= 1
*/
package array._Topic_PrefixSum_PrefixMax;

/*
   - cumulative sum (range sum)
   - sliding window
 */
public class MinSwapsToGroupAll1sTogether {
    public int minSwaps(int[] data) {
        int n = data.length, cum[]=new int[n+1];
        for(int i=0;i<n;i++) cum[i+1] = cum[i]+ data[i];

        int window=cum[n], max=0;
        for(int i=0; i+window-1<n; i++) {
            max = Math.max(max, cum[i+window]-cum[i]);
        }
        return window-max;
    }

    // or use sliding window...
    // ...

}



/*
by uwi:
public int minSwaps(int[] data) {
    int one = 0;
    for(int v : data){
        one += v;
    }
    int n = data.length;
    int[] cum = new int[n+1];
    for(int i = 0;i < n;i++)cum[i+1] = cum[i] + data[i];
    int ans = 99999999;
    for(int i = 0;i+one <= n;i++){
        ans = Math.min(ans, one - (cum[i+one] - cum[i]));
    }
    return ans;
}

*/