//Given a string text, we are allowed to swap two of the characters in the string. Find the length of the longest substring with repeated characters.
//
//
// Example 1:
//
//
//Input: text = "ababa"
//Output: 3
//Explanation: We can swap the first 'b' with the last 'a', or the last 'b' with the first 'a'. Then, the longest repeated character substring is "aaa", which its length is 3.
//
//
// Example 2:
//
//
//Input: text = "aaabaaa"
//Output: 6
//Explanation: Swap 'b' with the last 'a' (or the first 'a'), and we get longest repeated character substring "aaaaaa", which its length is 6.
//
//
// Example 3:
//
//
//Input: text = "aaabbaaa"
//Output: 4
//
//
// Example 4:
//
//
//Input: text = "aaaaa"
//Output: 5
//Explanation: No need to swap, longest repeated character substring is "aaaaa", length is 5.
//
//
// Example 5:
//
//
//Input: text = "abcdef"
//Output: 1
//
//
//
// Constraints:
//
//
// 1 <= text.length <= 20000
// text consist of lowercase English characters only.
//

package array._Topic_PrefixSum_PrefixMax;


// dp / group by ...(similar with cumulative sum)/
public class SwapForLongestRepeatedCharacterSubstring {
    public int maxRepOpt1_v2(String text) {
        char[] s = text.toCharArray();
        int n = s.length;

        char[] cs = new char[n];
        int[] fs = new int[n];
        int p = 0;
        for(int i=0;i<n;i++) {
            if(i==0 || s[i]!=s[i-1]) {
                fs[p] = 1;
                cs[p] = s[i];
                p++;
            } else {
                fs[p-1] ++;
            }
        }

        int[] freq = new int[26];
        for(char c:s) freq[c-'a']++;

//        cs = Arrays.copyOf(cs, p);
//        fs = Arrays.copyOf(fs, p);

        int res = 0;
        // case1: extend the group by 1
        for(int i=0;i<p;i++) {
            if(freq[cs[i]-'a'] >  fs[i]) {
                res = Math.max(res, fs[i]+1);
            } else {
                res = Math.max(res, fs[i]);
            }
        }
        // case2: merge 2 adjacent groups together, which are separated by only 1 character
        for(int i=1;i<p-1;i++) {
            if(fs[i]==1 && cs[i-1]==cs[i+1]) {
                if(freq[cs[i-1]-'a']>fs[i-1]+fs[i+1]) {
                    res = Math.max(res, fs[i-1]+fs[i+1]+1);
                } else {
                    res = Math.max(res, fs[i-1]+fs[i+1]);
                }
            }
        }
        return res;
    }


    public int maxRepOpt1_v1(String text) {
        char[] s = text.toCharArray();
        int n=s.length;
        int[] counter = new int[26];
        for(char c: s) counter[c-'a']++;
        int res=0;

        for(int k=0;k<26;k++) {
            int[] left = new int[n];
            int[] right = new int[n];

            left[0] = (s[0]=='a'+k)?1:0;
            for(int i=1; i<n; i++) {
                if(s[i]== 'a'+k) left[i] = left[i-1]+1;
            }
            right[n-1] = (s[n-1]=='a'+k)?1:0;
            for(int i=n-2; i>=0; i--) {
                if(s[i]=='a'+k) right[i] = right[i+1]+1;
            }

            for(int i=0;i<n;i++) {
                int len = 0;
                if(i>0) len += left[i-1];
                if(i<n-1) len += right[i+1];
                if(counter[k]>len) len++;
                res = Math.max(len, res);
            }
        }
        return res;
    }

    public static void main(String[] as) {
        System.out.println(new SwapForLongestRepeatedCharacterSubstring().maxRepOpt1_v2("ababa"));
        System.out.println(new SwapForLongestRepeatedCharacterSubstring().maxRepOpt1_v2("aaabaaa"));
        System.out.println(new SwapForLongestRepeatedCharacterSubstring().maxRepOpt1_v2("aaabbaaa"));
        System.out.println(new SwapForLongestRepeatedCharacterSubstring().maxRepOpt1_v2("aaaaa"));
        System.out.println(new SwapForLongestRepeatedCharacterSubstring().maxRepOpt1_v2("abcdef"));
        System.out.println(new SwapForLongestRepeatedCharacterSubstring().maxRepOpt1_v2("aabaaabaaaba"));
        //3
        //6
        //4
        //5
        //1
        //7
    }

}

/*
by uwi:
class Solution {
    public int maxRepOpt1(String text) {
        char[] s = text.toCharArray();
        int n = s.length;
        int[] cs = new int[n];
        int[] fs = new int[n];
        int p = 0;
        for(int i = 0;i < n;i++){
            if(i == 0 || s[i] != s[i-1]){
                cs[p] = s[i];
                fs[p] = 1;
                p++;
            }else{
                fs[p-1]++;
            }
        }

        cs = Arrays.copyOf(cs, p);
        fs = Arrays.copyOf(fs, p);

        int ret = 0;
        for(int f : fs)ret = Math.max(ret, f);

        int[] allf = new int[128];
        for(int i = 0;i < p;i++){
            allf[cs[i]] += fs[i];
        }

        for(int i = 0;i+2 < p;i++){
            if(cs[i] == cs[i+2] && fs[i+1] == 1){
                if(allf[cs[i]] == fs[i] + fs[i+2]){
                    ret = Math.max(ret, fs[i] + fs[i+2]);
                }else{
                    ret = Math.max(ret, fs[i] + fs[i+2] + 1);
                }
            }
        }
        for(int i = 0;i < p;i++){
            if(allf[cs[i]] != fs[i]){
                ret = Math.max(ret, fs[i] + 1);
            }
        }

        return ret;
    }
}


by lee:
Intuition
There are only 2 cases that we need to take care of:

extend the group by 1
merge 2 adjacent groups together, which are separated by only 1 character

Explanation
For S = "AAABBCB"
[[c, len(list(g))] for c, g in groupby(S)] --> [[A,3],[B,2],[C,1],[B,1]]
collections.Counter(S) --> {A:3, B:3, C:1}
With these two data, calculate the best solution for the two cases above.


Complexity
Time O(N)
Space O(N)


Python:
commented by @henry34

    def maxRepOpt1(self, S):
        # We get the group's key and length first, e.g. 'aaabaaa' -> [[a , 3], [b, 1], [a, 3]
        A = [[c, len(list(g))] for c, g in itertools.groupby(S)]
        # We also generate a count dict for easy look up e.g. 'aaabaaa' -> {a: 6, b: 1}
        count = collections.Counter(S)
        # only extend 1 more, use min here to avoid the case that there's no extra char to extend
        res = max(min(k + 1, count[c]) for c, k in A)
        # merge 2 groups together
        for i in xrange(1, len(A) - 1):
            # if both sides have the same char and are separated by only 1 char
            if A[i - 1][0] == A[i + 1][0] and A[i][1] == 1:
                # min here serves the same purpose
                res = max(res, min(A[i - 1][1] + A[i + 1][1] + 1, count[A[i + 1][0]]))
        return res

*/
