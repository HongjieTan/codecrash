//Given a 2D grid of 0s and 1s, return the number of elements in the largest square subgrid that has all 1s on its border,
// or 0 if such a subgrid doesn't exist in the grid.
//
//
// Example 1:
//
//
//Input: grid = [[1,1,1],[1,0,1],[1,1,1]]
//Output: 9
//
//
// Example 2:
//
//
//Input: grid = [[1,1,0,0]]
//Output: 1
//
//
//
// Constraints:
//
//
// 1 <= grid.length <= 100
// 1 <= grid[0].length <= 100
// grid[i][j] is 0 or 1
//
//
package array._Topic_PrefixSum_PrefixMax;

public class Largest1BorderedSquare {

    // O(n^3), presum(cumulative sum)...
    public int largest1BorderedSquare(int[][] grid) {
        int m=grid.length, n=grid[0].length;
        int[][] left = new int[m][n];
        int[][] up = new int[m][n];

        for(int j=0;j<n;j++) {
            for(int i=0;i<m;i++) {
                left[i][j] = (j>0?left[i][j-1]:0) + grid[i][j];
            }
        }
        for(int i=0;i<m;i++) {
            for(int j=0;j<n;j++) {
                up[i][j] = (i>0?up[i-1][j]:0) + grid[i][j];
            }
        }

        int maxlen = 0;
        for(int i=0;i<m;i++) {
            for(int j=0;j<n;j++) {
                for(int len=1; i+len-1<m && j+len-1<n; len++) {
                    int ni=i+len-1, nj=j+len-1;
                    int top = left[i][nj] - (j>0?left[i][j-1]:0);
                    int bottom = left[ni][nj] - (j>0?left[ni][j-1]:0);
                    int l = up[ni][j] - (i>0?up[i-1][j]:0);
                    int r = up[ni][nj] - (i>0?up[i-1][nj]:0);
                    if(top==len && bottom==len && l==len && r==len) {
                        maxlen = Math.max(maxlen, len);
                    }
                }
            }
        }
        return maxlen*maxlen;
    }
}


/*
by uwi:
class Solution {
    public int largest1BorderedSquare(int[][] grid) {
        int n = grid.length, m = grid[0].length;
        int[][] cum = new int[n+1][m+1];
        for(int i = 0;i < n;i++){
            for(int j = 0;j < m;j++){
                cum[i+1][j+1] = grid[i][j] + cum[i][j+1] + cum[i+1][j] - cum[i][j];
            }
        }

        int max = 0;
        for(int d = 1;d <= n && d <= m;d++){
            for(int i = 0;i+d <= n;i++){
                for(int j = 0;j+d <= m;j++){
                    int o = cum[i+d][j+d] - cum[i][j+d] - cum[i+d][j] + cum[i][j];
                    int e = d >= 2 ? cum[i+d-1][j+d-1] - cum[i+1][j+d-1] - cum[i+d-1][j+1] + cum[i+1][j+1] : 0;
                    if(d >= 2){
                        if(o - e == d*d - (d-2) * (d-2)){
                            max = Math.max(max, d*d);
                        }
                    }else{
                        if(o - e == 1){
                            max = Math.max(max, d*d);
                        }
                    }
                }
            }
        }
        return max;
    }
}


by lee:
Explanation
Count the number of consecutive 1s on the top and on the left.
From length of edge l = min(m,n) to l = 1, check if the 1-bordered square exist.

Complexity
Time O(N^3)
Space O(N^2)


Java:

class Solution {
    public int largest1BorderedSquare(int[][] A) {
        int m = A.length, n = A[0].length;
        int[][] left = new int[m][n], top = new int[m][n];
        for (int i = 0; i < m; ++i) {
            for (int j = 0; j < n; ++j) {
                if (A[i][j] > 0) {
                    left[i][j] = j > 0 ? left[i][j - 1] + 1 : 1;
                    top[i][j] = i > 0  ? top[i - 1][j] + 1 : 1;
                }
            }
        }
        for (int l = Math.min(m, n); l > 0; --l)
            for (int i = 0; i < m - l + 1; ++i)
                for (int j = 0; j < n - l + 1; ++j)
                    if (top[i + l - 1][j] >= l &&
                            top[i + l - 1][j + l - 1] >= l &&
                            left[i][j + l - 1] >= l &&
                            left[i + l - 1][j + l - 1] >= l)
                        return l * l;
        return 0;
    }
}
C++:

    int largest1BorderedSquare(vector<vector<int>>& A) {
        int m = A.size(), n = A[0].size();
        vector<vector<int>> left(m, vector<int>(n)), top(m, vector<int>(n));
        for (int i = 0; i < m; ++i) {
            for (int j = 0; j < n; ++j) {
                left[i][j] = A[i][j] + (j && A[i][j] ? left[i][j - 1] : 0);
                top[i][j] = A[i][j] + (i && A[i][j] ? top[i - 1][j] : 0);
            }
        }
        for (int l = min(m, n); l > 0; --l)
            for (int i = 0; i < m - l + 1; ++i)
                for (int j = 0; j < n - l + 1; ++j)
                    if (min({top[i + l - 1][j], top[i + l - 1][j + l - 1], left[i][j + l - 1], left[i + l - 1][j + l - 1]}) >= l)
                        return l * l;
        return 0;
    }
Python:

    def largest1BorderedSquare(self, A):
        m, n = len(A), len(A[0])
        res = 0
        top, left = [a[:] for a in A], [a[:] for a in A]
        for i in xrange(m):
            for j in xrange(n):
                if A[i][j]:
                    if i: top[i][j] = top[i - 1][j] + 1
                    if j: left[i][j] = left[i][j - 1] + 1
        for r in xrange(min(m, n), 0, -1):
            for i in xrange(m - r + 1):
                for j in xrange(n - r + 1):
                    if min(top[i + r - 1][j], top[i + r - 1][j + r - 1], left[i]
                           [j + r - 1], left[i + r - 1][j + r - 1]) >= r:
                        return r * r
        return 0
*/