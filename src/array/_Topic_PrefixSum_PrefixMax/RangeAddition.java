package array._Topic_PrefixSum_PrefixMax;

/**
 * Created by thj on 2018/10/31.
 *
 *
 *  O(max(n, k))
 *
 */
public class RangeAddition {
    public int[] getModifiedArray(int length, int[][] updates) {
        // Write your code here

        int[] flag = new int[length+1];

        for (int i = 0; i < updates.length; i++) {
            int start = updates[i][0];
            int end = updates[i][1];
            int val = updates[i][2];
            flag[start] += val;
            flag[end+1] -= val;
        }

        int[] presum = new int[length];
        for (int i = 0; i < length; i++) {
            if (i==0) presum[i] = flag[i];
            else presum[i] = flag[i]+presum[i-1];
        }
        return presum;
    }
}
/*
Assume you have an array of length n initialized with all 0's and are given k update operations.

Each operation is represented as a triplet: [startIndex, endIndex, inc]
which increments each element of subarray A[startIndex ... endIndex] (startIndex and endIndex inclusive) with inc.

Return the modified array after all k operations were executed.

Example
Given:
length = 5,
updates =
[
[1,  3,  2],
[2,  4,  3],
[0,  2, -2]
]
return [-2, 0, 3, 5, 3]

Explanation:
Initial state:
[ 0, 0, 0, 0, 0 ]
After applying operation [1, 3, 2]:
[ 0, 2, 2, 2, 0 ]
After applying operation [2, 4, 3]:
[ 0, 2, 5, 5, 3 ]
After applying operation [0, 2, -2]:
[-2, 0, 3, 5, 3 ]
*/