package array._Topic_PrefixSum_PrefixMax;

import java.util.List;


/**
 *
 * problem1: split(inclusive) array into two subarrays with same equal sum,
 *          ex.sum(A[0...i])==sum(A[i+1...n-1])
 *          -> presum,
 * problem2: split(inclusive) array into three subarrays with same equal sum
 *          ex. sum(A[0...i])==sum(A[i+1...j])==sum[A[j+1...n-1]]
 *          -> presum, find TotalSum/3 first and then TotalSum*2/3...
 *
 * problem3: split(exclusive) array into four subarrays with same equal sum
 *          ex. find (i,j,k), sum of (0, i - 1), (i + 1, j - 1), (j + 1, k - 1) and (k + 1, n - 1) be equal
 *          -> presum, find j(the middle one) first!
 *
 *
 */
public class SplitArrayIntoSubArraysWithEqualSum {
    public boolean splitArray(List<Integer> nums) {
        return false;
    }
}
