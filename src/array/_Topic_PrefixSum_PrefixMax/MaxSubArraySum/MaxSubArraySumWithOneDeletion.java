//Given an array of integers, return the maximum sum for a non-empty subarray (contiguous elements) with at most one element deletion.
// In other words, you want to choose a subarray and optionally delete one element from it so that there is still at least one element left and the sum of the remaining elements is maximum possible.
//
// Note that the subarray needs to be non-empty after deleting one element.
//
//
// Example 1:
//
//
//Input: arr = [1,-2,0,3]
//Output: 4
//Explanation: Because we can choose [1, -2, 0, 3] and drop -2, thus the subarray [1, 0, 3] becomes the maximum value.
//
// Example 2:
//
//
//Input: arr = [1,-2,-2,3]
//Output: 3
//Explanation: We just choose [3] and it's the maximum sum.
//
//
// Example 3:
//
//
//Input: arr = [-1,-1,-1,-1]
//Output: -1
//Explanation: The final subarray needs to be non-empty. You can't choose [-1] and delete -1 from it, then get an empty subarray to make the sum equals to 0.
//
//
//
// Constraints:
//
//
// 1 <= arr.length <= 10^5
// -10^4 <= arr[i] <= 10^4
// Related Topics Dynamic Programming

package array._Topic_PrefixSum_PrefixMax.MaxSubArraySum;

public class MaxSubArraySumWithOneDeletion {
    public int maximumSum(int[] arr) {
        int res=arr[0], n = arr.length;
        int[] left = new int[n], right = new int[n];
        int sum = 0;
        for(int i=0;i<n;i++) {
            if (sum<0) sum=0;
            sum+=arr[i];
            left[i] = sum;
        }

        sum=0;
        for(int i=n-1;i>=0;i--) {
            if (sum<0) sum=0;
            sum+=arr[i];
            right[i] = sum;
        }

        for(int i=0;i<n;i++) res = Math.max(res, left[i]); // case1: no deletion
        for(int i=1;i<n-1;i++) res = Math.max(res, left[i-1]+right[i+1]); // case2: one deletion
        return res;
    }

    public static void main(String[] as) {
        System.out.println(new MaxSubArraySumWithOneDeletion().maximumSum(new int[]{1,-2,0,3}));
        System.out.println(new MaxSubArraySumWithOneDeletion().maximumSum(new int[]{1,-2,-2,3}));
        System.out.println(new MaxSubArraySumWithOneDeletion().maximumSum(new int[]{-1,-1,-1,-1}));
    }
}


/*
by cuiaoxiang:
class Solution {
public:
    int maximumSum(vector<int>& a) {
        int n = a.size();
        vector<int> L(n), R(n);
        R[n - 1] = a[n - 1];
        for (int i = n - 2; i >= 0; --i) {
            R[i] = max(a[i], R[i + 1] + a[i]);
        }
        L[0] = a[0];
        for (int i = 1; i < n; ++i) {
            L[i] = max(a[i], L[i - 1] + a[i]);
        }
        int ret = *max_element(a.begin(), a.end());
        for (int i = 0; i < n; ++i) {
            if (i - 1 >= 0 && i + 1 < n) {
                ret = max(ret, L[i - 1] + R[i + 1]);
            }
            if (i - 1 >= 0) ret = max(ret, L[i - 1]);
            if (i + 1 < n) ret = max(ret, R[i + 1]);
        }
        return ret;
    }
};


by top post:
Idea
Compute maxEndHere and maxStartHere arrays and also find overall max along with them.
Now, evaluate the case where 1-element can be eliminated, that is at each index, we can make use of maxEndHere[i-1]+maxStartHere[i+1]

Thought process
This approach is a slight improvisation on the idea of https://leetcode.com/problems/maximum-subarray/.
Basically, the difference here is we can eliminate 1 number and still can continue with expanding our subarray.
So imagine a subarray where you removed 1 element, then it forms two subarrays ending at prev index and starting at next index.
 We know how to get maxEndHere from the max sum subarray problem for each index.
 If we reverse our thinking to follow the same logic to solve for subarray at next index, we should be able to see computing maxStartHere is just backwards of maxEndHere.
 So now at each index, it is just about looking at prev and next numbers from the respective arrays to get overall max.

public int maximumSum(int[] a) {
        int n = a.length;
        int[] maxEndHere = new int[n], maxStartHere = new int[n];
        int max = a[0];
        maxEndHere[0] = a[0];
        for(int i=1; i < n; i++){
            maxEndHere[i] = Math.max(a[i], maxEndHere[i-1]+a[i]);
            max = Math.max(max, maxEndHere[i]);
        }
        maxStartHere[n-1] = a[n-1];
        for(int i=n-2; i >= 0; i--)
            maxStartHere[i] = Math.max(a[i], maxStartHere[i+1]+a[i]);
        for(int i=1; i < n-1; i++)
            max = Math.max(max, maxEndHere[i-1]+maxStartHere[i+1]);
        return max;
    }


by votrubac:
Intuition
We can use 53. Maximum Subarray as our starting point.
The idea there is to split our array into subarrays with positive balance.
In other words, when our running sum falls below zero, we start a new subarray.

However, here we can have two cases:

Removed element increases the sum of a subarray.
Removed element joins two subarrays.
Solution
We basically run the classic maximum subarray algorithm twice.

For case 1, we track the minimum negative element for each subarray and subtract it from the running sum.
For case 2, we also track the minimum element but use it to detect whether we need to start a new subarray.

int maximumSum(vector<int>& arr) {
  auto res = INT_MIN, s1 = 0, s2 = 0, m1 = 0, m2 = 0;
  for (auto n : arr)
  {
    if (n > s1 + n) s1 = 0, m1 = 0;
    if (s2 - m2 <= 0) s2 = 0, m2 = 0;
    s1 += n, s2 += n;
    res = max({ res, s1 - m1, s2 - m2 });
    m1 = min(m1, n), m2 = min(m2, n);
  }
  return res;
}
Complexity Analysis
Runtime: O(n)
Memory: O(1)

*/
