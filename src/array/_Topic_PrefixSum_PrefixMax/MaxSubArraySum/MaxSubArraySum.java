//Given an integer array nums, find the contiguous subarray (containing at least one number) which has the largest sum and return its sum.
//
// Example:
//
//
//Input: [-2,1,-3,4,-1,2,1,-5,4],
//Output: 6
//Explanation: [4,-1,2,1] has the largest sum = 6.
//
//
// Follow up:
//
// If you have figured out the O(n) solution, try coding another solution using the divide and conquer approach, which is more subtle.
// Related Topics Array Divide and Conquer Dynamic Programming
package array._Topic_PrefixSum_PrefixMax.MaxSubArraySum;

public class MaxSubArraySum {

    // concise version
    public int maxSubArray(int[] nums) {
        int curmax=0, res=0;
        for(int x: nums) {
            curmax = Math.max(curmax+x, x); // maxsum of subarray ending at current index
            res = Math.max(res, curmax);
        }
        return res;
    }

    // followup, use divide and conquer
    class Item {
        int lmax; // max subarray starting at l
        int rmax; // max subarray ending at r
        int sum; //  sum of array
        int max; //  max subarray
        public Item(int lmax, int rmax, int sum, int max){this.lmax=lmax; this.rmax=rmax;this.sum=sum;this.max=max;}
    }
    public int maxSubArray_dc(int[] nums) {
        return maxSubArray(nums, 0, nums.length-1).max;
    }
    Item maxSubArray(int[] nums, int l, int r ) {
        if(l==r) return new Item(nums[l], nums[l],nums[l],nums[l]);
        else {
            int mid = l+(r-l)/2;
            Item lpart = maxSubArray(nums, l, mid);
            Item rpart = maxSubArray(nums, mid+1, r);

            int lmax = Math.max(lpart.lmax, lpart.sum+rpart.lmax);
            int rmax = Math.max(rpart.rmax, rpart.sum+lpart.rmax);
            int sum = lpart.sum + rpart.sum;
            int max = Math.max(Math.max(lpart.max, rpart.max), lpart.rmax+rpart.lmax);
            return new Item(lmax, rmax, sum, max);
        }
    }


//    public int maxSubArray_v1(int[] nums) {
//        int sum=0, res=Integer.MIN_VALUE;
//        for(int x: nums) {
//            if (sum<0) sum=0;
//            sum+=x;
//            res = Math.max(res, sum);
//        }
//        return res;
//    }
//    public int maxSubArray_v0(int[] nums) {
//        int min = Integer.MAX_VALUE, sum=0, res=Integer.MIN_VALUE;
//        for(int x: nums) {
//            min = Math.min(min, sum);
//            sum += x;
//            res = Math.max(res, sum-min);
//        }
//        return res;
//    }

    public static void main(String[] as) {
//        System.out.println(new MaxSubArraySum().maxSubArray_dc(new int[]{-2,1,-3,4,-1,2,1,-5,4}));
    }
}