//Given an integer array arr and an integer k, modify the array by repeating it k times.
//
// For example, if arr = [1, 2] and k = 3 then the modified array will be [1, 2, 1, 2, 1, 2].
//
// Return the maximum sub-array sum in the modified array. Note that the length of the sub-array can be 0 and its sum in that case is 0.
//
// As the answer can be very large, return the answer modulo 10^9 + 7.
//
//
// Example 1:
//
//
//Input: arr = [1,2], k = 3
//Output: 9
//
//
// Example 2:
//
//
//Input: arr = [1,-2,1], k = 5
//Output: 2
//
//
// Example 3:
//
//
//Input: arr = [-1,-2], k = 7
//Output: 0
//
//
//
// Constraints:
//
//
// 1 <= arr.length <= 10^5
// 1 <= k <= 10^5
// -10^4 <= arr[i] <= 10^4
//

package array._Topic_PrefixSum_PrefixMax.MaxSubArraySum;

public class KConcatenationMaxSum {

    public int kConcatenationMaxSum(int[] arr, int k) {
        int mod = (int)Math.pow(10,9)+7;
        int n = arr.length;

        int tailmax=0, headmax=0, inmax=0, sum=0;
        for(int i=0;i<n;i++) {
            tailmax = Math.max(tailmax+arr[i], arr[i]);
            headmax = Math.max(headmax+arr[n-1-i], arr[n-1-i]);
            inmax = Math.max(inmax, tailmax);
            sum += arr[i];
        }


        if(k==1) return inmax;

        long res = 0;
        res = Math.max(res, inmax);
        res = Math.max(res, tailmax+headmax+(long)(k-2)*sum);
        res = Math.max(res, tailmax+headmax);
        return (int)(res%mod);
    }

    public static void main(String[] as) {
        int[] arr = {1, -2, 1};
        int k=5;
        System.out.println(new KConcatenationMaxSum().kConcatenationMaxSum(arr, k));
    }
}
