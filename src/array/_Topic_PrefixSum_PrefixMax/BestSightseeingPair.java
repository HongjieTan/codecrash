package array._Topic_PrefixSum_PrefixMax;

public class BestSightseeingPair {
    public int maxScoreSightseeingPair(int[] A) {
        int leftBest=0, res=0;
        for(int x:A) {
            res = Math.max(res, leftBest+x);
            leftBest = Math.max(leftBest, x)-1;
        }
        return res;
    }
}
