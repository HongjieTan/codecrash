package array._Topic_PrefixSum_PrefixMax;

import java.util.*;

/*
    一列street block，每个street block上都有POI，比如学校，商店etc 也可能没有。给定一个list of requirements, 比如[grocery，school]，找到距离所有requirement最近的apartment位置。
    Follow up： 是如果只有一些street block有apartment怎么办.

    假设一条街上有多个block，一个block上有多个建筑。求一条街上离几个最近的特定建筑的最远距离最短的block。
    例子： street = [[“store”, “school”, “museum”], [“hospital”, “restaurant”], [“school”, “restaurant”], [], [“museum”]], requirement = [“store”, “museum”, “restaurant”].
    第一个block到最近store是0，到最近museum是0，到最近restaurant是1，所以它的max是1
    第二个block到最近store是1，到最近museum是1，到最近restaurant是0，所以它的max是1
    第三个block到最近store是2，到最近museum是2，到最近restaurant是0，所以它的max是2
    第四个block到最近store是3，到最近museum是1，到最近restaurant是1，所以它的max是3
    第五个block到最近store是4，到最近museum是0，到最近restaurant是2，所以它的max是4
    所以返回的block是第一个和第二个。

    思路：(网友版本)
    - (tan: good solution!) 我的思路是对每个requirement从左到右+从右到左遍历两遍，求min找到每个地点距离最近requirement的距离。然后把所有requirement得到的结果max一下，再取min就是答案。follow up不需要遍历，只需要看valid的位置。

    - 遍历street建立map，key是建筑名字，value是list of integer存block index。然后再遍历一次street，对requirement中的每个建筑，找当前block是否在list里，如果在就删掉之前所有的index（？？？），如果不在就取最近的两个距离中的min。时间复杂度O(m*n)，m为block个数，n为requirement个数。（有没有更好的方法？）

    - 用range做，相当于在一个string中找到包含所有指定字符的最短substring，在一个包含所有requirement的range中，range长度越短，max越小。

    - 先遍历一次street，建立这样一个结构：unordered_map<string, set<int>>, 即给每种建筑建立一个有序表（二叉排序树）；然后遍历block坐标，对每个坐标，在上面的哈希表里对每种建筑对应的排序树里二分找上下界，这样就能log复杂度计算到每种建筑的最近距离

    - 确认一下＂最远的定义＂是一个点到所有最近requirement的点的总和？一个点到所有requirement中最远的距离？
        sum((distance to r) for r in requirements)  or  max((distance to r) for r in requirements)
      我看另一个面经的例子好像是后者

    - (tan: 可解，但有局限...) 如果是后者，用移动窗口模板找最小的包含所有requirement的区间，再找中间应该就行了

 */

/**
 *
 *
 *     solution-1(dp, best choice...)： two side dp， prefix max
 *          - O(n*m + n*k)  n:blocks, m: requirements, k: average size of block
 *          - 对于 sum 和 max 场景都适用，对于followup（只有部分blocks有department）也适用！
 *
 *     solution-2(2p)： sliding window,
 *          - O(n*m + n*k)
 *          - 但貌似适用场景有限，不适用于sum场景和followup
 *
 */
public class ApartmentInStreetBlock {

    // DP,  O( n * m )  n:blocks, m: requirements， 可扩展于sum场景和followup（只有部分blocks有department）
    public int findApartment_dp(List<List<String>> street, List<String> requirements) {
        int n = street.size();
        int m = requirements.size();

        int[][] left = new int[n][m];
        int[][] right = new int[n][m];

        Set[] streetSets = new Set[n];
        for(int i=0;i<n;i++) streetSets[i]=new HashSet<>(street.get(i));

        for(int i=0; i<n; i++) {
            for(int j=0;j<m;j++) {
                if(streetSets[i].contains(requirements.get(j))) left[i][j]=0;
                else if(i==0) left[i][j]=Integer.MAX_VALUE;
                else left[i][j] = left[i-1][j]!=Integer.MAX_VALUE?left[i-1][j]+1:Integer.MAX_VALUE;
            }
        }

        for(int i=n-1; i>=0; i--) {
            for(int j=0;j<m;j++) {
                if(streetSets[i].contains(requirements.get(j))) right[i][j]=0;
                else if(i==n-1) right[i][j]=Integer.MAX_VALUE;
                else right[i][j] = right[i+1][j]!=Integer.MAX_VALUE?right[i+1][j]+1:Integer.MAX_VALUE;
            }
        }

        int ansMin = Integer.MAX_VALUE;
        int ansBlock = 0;
        for(int i=0;i<n;i++) {
            int max = -1;
            for(int j=0;j<m;j++) {
                int dist = Math.min(left[i][j], right[i][j]);
                max = Math.max(dist, max);
            }
//            System.out.println("block "+i+", max "+max);
            if(max < ansMin) {
                ansMin = max;
                ansBlock = i;
            }
        }
        return ansBlock;
    }

    //  2 pointers, sliding window,  O( n * m )  n:blocks, m: requirements,  不如DP版解法，适用场景有限，不适用于sum场景和followup（只有部分blocks有department）  O( n * m )  n:blocks, m: requirements
    public int findApartment_2p(List<List<String>> street, List<String> requirements) {
        int n=street.size();
        List<Set<String>> streetSets = new ArrayList<>();
        for(int i=0; i<n; i++) streetSets.add(new HashSet<>(street.get(i)));

        Map<String, Integer> counter = new HashMap<>();
        int l=0, r=0;
        int minL=0, minR=0, minLen=Integer.MAX_VALUE;
        while(r<n) {
            // move r
            Set<String> poiSet = streetSets.get(r);
            for(String req: requirements) {
                if(poiSet.contains(req)) {
                    counter.put(req, counter.getOrDefault(req, 0)+1);
                }
            }
            if(counter.size()==requirements.size()) {
                if(r-l+1<minLen) {
                    minLen = r-l+1;
                    minL = l;
                    minR = r;
                }
            }
            r++;

            // move l
            while(counter.size()==requirements.size()) {
                poiSet = streetSets.get(l);
                for(String req: requirements) {
                    if(poiSet.contains(req)) {
                        counter.put(req, counter.get(req)-1);
                        if(counter.get(req)==0) counter.remove(req);
                    }
                }
                l++;
            }
        }
        return minL+(minR-minL)/2; // choose the median of minLen range
    }

    // test cases
    public static void main(String[] args) {
//        例子： street = [[“store”, “school”, “museum”], [“hospital”, “restaurant”], [“school”, “restaurant”], [], [“museum”]], requirement = [“store”, “museum”, “restaurant”].
        List<List<String>> street = new ArrayList<>();
        street.add(Arrays.asList("store","school","museum"));
        street.add(Arrays.asList("hospital","restaurant"));
        street.add(Arrays.asList("school","restaurant"));
        street.add(Arrays.asList());
        street.add(Arrays.asList("museum"));
        List<String> requirement = Arrays.asList("store", "museum", "restaurant");
        System.out.println(new ApartmentInStreetBlock().findApartment_dp(street, requirement));
        System.out.println(new ApartmentInStreetBlock().findApartment_2p(street, requirement));
    }


}
