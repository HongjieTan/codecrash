package array._Topic_PrefixSum_PrefixMax;

public class MaxSumOfTwoNonOverlappingSubarrays {

    // dp: O(n)
    public int maxSumTwoNoOverlap(int[] A, int L, int M) {
        return Math.max(maxSum(A,L,M), maxSum(A,M,L));
    }

    int maxSum(int[] A, int L, int M) {
        int n= A.length;
        int[] presum = new int[n+1];
        for(int i=0;i<n;i++) presum[i+1]=presum[i]+A[i];

        // left[i]: max sum with length L before i; right[i]: max sum with length M after i
        int[] left = new int[n], right = new int[n];
        for(int i=L-1;i<n;i++) left[i] = Math.max(i>0?left[i-1]:0, presum[i+1]-presum[i+1-L]);
        for(int i=n-M;i>=0;i--) right[i] = Math.max(i<n-1?right[i+1]:0, presum[i+M]-presum[i]);

        int ans = 0;
        for(int i=L-1;i<n-M;i++) ans=Math.max(ans, left[i]+right[i+1]);
        return ans;
    }

    // brute force: O(n^2)
    public int maxSumTwoNoOverlap_v0(int[] A, int L, int M) {
        int n = A.length;
        int[] presum = new int[n];
        presum[0] = A[0];
        for(int i=1;i<n;i++) presum[i]=presum[i-1]+A[i];
        int ans=0;
        for(int i=0;i+L-1<n;i++) {
            for(int j=0; j+M-1<n;j++) {
                if(i+L-1<j || j+M-1<i) {
                    int lsum = presum[i+L-1] - (i>0?presum[i-1]:0);
                    int msum = presum[j+M-1] - (j>0?presum[j-1]:0);
                    ans = Math.max(ans, lsum+msum);
                }
            }
        }
        return ans;
    }

    public static void main(String[] as) {
//[0,6,5,2,2,5,1,9,4]
//        1
//        2
        int[] A = new int[]{0,6,5,2,2,5,1,9,4};
        int L=1,M=2;
        System.out.println(new MaxSumOfTwoNonOverlappingSubarrays().maxSumTwoNoOverlap_v0(A,L,M));
        System.out.println(new MaxSumOfTwoNonOverlappingSubarrays().maxSumTwoNoOverlap(A,L,M));
    }
}
/*
class Solution(object):
    def maxSumTwoNoOverlap(self, A, L, M):
        """
        :type A: List[int]
        :type L: int
        :type M: int
        :rtype: int
        """
*/

/*
Given an array A of non-negative integers, return the maximum sum of elements in two non-overlapping (contiguous) subarrays,
which have lengths L and M.  (For clarification, the L-length subarray could occur before or after the M-length subarray.)

Formally, return the largest V for which V = (A[i] + A[i+1] + ... + A[i+L-1]) + (A[j] + A[j+1] + ... + A[j+M-1]) and either:

0 <= i < i + L - 1 < j < j + M - 1 < A.length, or
0 <= j < j + M - 1 < i < i + L - 1 < A.length.


Example 1:

Input: A = [0,6,5,2,2,5,1,9,4], L = 1, M = 2
Output: 20
Explanation: One choice of subarrays is [9] with length 1, and [6,5] with length 2.
Example 2:

Input: A = [3,8,1,3,2,1,8,9,0], L = 3, M = 2
Output: 29
Explanation: One choice of subarrays is [3,8,1] with length 3, and [8,9] with length 2.
Example 3:

Input: A = [2,1,5,6,0,9,5,0,3,8], L = 4, M = 3
Output: 31
Explanation: One choice of subarrays is [5,6,0,9] with length 4, and [3,8] with length 3.


Note:

L >= 1
M >= 1
L + M <= A.length <= 1000
0 <= A[i] <= 1000
*/


/*
int maxTwoNoOverlap(vector<int>& A, int L, int M, int sz, int res = 0) {
  vector<int> left(sz + 1), right(sz + 1);
  for (int i = 0, j = sz - 1, s_r = 0, s_l = 0; i < sz; ++i, --j) {
    s_l += A[i], s_r += A[j];
    left[i + 1] = max(left[i], s_l);
    right[j] = max(right[j + 1], s_r);
    if (i + 1 >= L) s_l -= A[i + 1 - L];
    if (i + 1 >= M) s_r -= A[j + M - 1];
  }
  for (auto i = 0; i < A.size(); ++i) {
    res = max(res, left[i] + right[i]);
  }
  return res;
}
int maxSumTwoNoOverlap(vector<int>& A, int L, int M) {
  return max(maxTwoNoOverlap(A, L, M, A.size()), maxTwoNoOverlap(A, M, L, A.size()));
}
*/