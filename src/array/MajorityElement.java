//Given an array of size n, find the majority element. The majority element is the element that appears more than ⌊ n/2 ⌋ times.
//
// You may assume that the array is non-empty and the majority element always exist in the array.
//
// Example 1:
//
//
//Input: [3,2,3]
//Output: 3
//
// Example 2:
//
//
//Input: [2,2,1,1,1,2,2]
//Output: 2
//
//

package array;

/**
 * Created by thj on 28/05/2018.
 *
 */
public class MajorityElement {

    // Boyer–Moore majority vote algorithm
    //   O(n)/O(1)  one pass
    //   Notice: If the existence of majority element is not guaranteed, we should do a second pass to check if it it majority!
    public int majorityElement(int[] nums) {
        int count = 0;
        int majorityNum = -1;

        for (int i=0;i<nums.length;i++) {
            if (count == 0) {
                majorityNum = nums[i];
                count++;
            } else if (nums[i] == majorityNum) {
                count++;
            } else {
                count--;
            }
        }
        return majorityNum;

    }
}
