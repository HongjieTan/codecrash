//Given a fixed length array arr of integers, duplicate each occurrence of zero, shifting the remaining elements to the right.
//
// Note that elements beyond the length of the original array are not written.
//
// Do the above modifications to the input array in place, do not return anything from your function.
//
//
//
// Example 1:
//
//
//Input: [1,0,2,3,0,4,5,0]
//Output: null
//Explanation: After calling your function, the input array is modified to: [1,0,0,2,3,0,0,4]
//
//
// Example 2:
//
//
//Input: [1,2,3]
//Output: null
//Explanation: After calling your function, the input array is modified to: [1,2,3]
//
//
//
//
// Note:
//
//
// 1 <= arr.length <= 10000
// 0 <= arr[i] <= 9
//
package array;


/*
    solution2:
        - two pass, first pass forward and count zeros, second pass backward and assign values
        - time O(n), space O(1)
    solution1:
        - use a array copy.
        - time O(n), space O(n)
 */
public class DuplicateZeros {

    public void duplicateZeros(int[] arr) {
        int zeros = 0, n=arr.length;
        for(int x:arr) if(x==0) zeros++;
        for (int i = n+zeros-1, j=n-1; i >=0 ; i--,j--) {
            if (i<n) arr[i]=arr[j];
            if (arr[j]==0 && --i<n) arr[i]=0;
        }
    }

    public void duplicateZeros_v1(int[] arr) {
        int zeros = 0;
        for(int x:arr) if(x==0) zeros++;
        int index = arr.length+zeros-1;
        while (index>=0) {
            if (arr[index-zeros]==0) {
                if (index<arr.length) arr[index] = arr[index-zeros];
                if (index-1<arr.length) arr[index-1] = 0;
                zeros--;
                index-=2;
            } else {
                if (index<arr.length) arr[index] = arr[index-zeros];
                index--;
            }
        }
    }


    public static void main(String[] a) {
        int[] arr = {8,5,0,9,0,3,4,7};
        new DuplicateZeros().duplicateZeros(arr);
        System.out.println("asdf");
    }
}
