package array;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by thj on 28/05/2018.
 */
public class MajorityElement2 {

    // one pass ...
    public List<Integer> majorityElement(int[] nums) {
        int res1=-1, count1=0, res2=-1, count2=0;

        for (int i=0; i<nums.length; i++) {
            if (nums[i] == res1) {
                count1++;
            } else if (nums[i] == res2) {
                count2++;
            } else if (count1 == 0) {
                res1 = nums[i];
                count1++;
            } else if (count2 == 0) {
                res2 = nums[i];
                count2++;
            } else {
                count1--;
                count2--;
            }
        }
        count1 = 0;
        count2 = 0;

        for (int i=0;i<nums.length;i++) {
            if (res1 == nums[i]) count1++;
            if (res2 == nums[i]) count2++;
        }

        List<Integer> ans = new ArrayList<>();
        if (count1 > nums.length/3) ans.add(res1);
        if (count2 > nums.length/3) ans.add(res2);
        return ans;
    }

}
