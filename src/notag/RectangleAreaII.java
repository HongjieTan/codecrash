package notag;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;


/**
 *
 * - 把重叠长方形打碎成不重叠的长方形: O(n^2), 两种实现见v1,v2, idea is straight forward...
 *
 * - sweep line: TODO, like skyline??
 *
 */
public class RectangleAreaII {

    // 把重叠长方形打碎成不重叠的长方形。。（每次打碎已经要加入的矩形）
    public int rectangleArea_v2(int[][] rectangles) {
        List<int[]> noOverlapRects = new LinkedList<>();

        // step1: 打碎成不重叠矩形
        for(int[] rect: rectangles) {
            add(noOverlapRects, rect);
        }

        // step2: sum area
        long mod = (long) (Math.pow(10,9)+ 7);
        long sum=0;
        for(int[] rect: noOverlapRects) {
            sum+=(long)(rect[2]-rect[0])*(rect[3]-rect[1]);
            sum%=mod;
        }
        return (int)sum;

    }
    void add(List<int[]> noOverlapRects, int[] rect) {
        if(noOverlapRects.size()==0) {
            noOverlapRects.add(rect);
            return;
        }
        Iterator<int[]> it = noOverlapRects.iterator();
        List<int[]> temp = new ArrayList<>();
        while(it.hasNext()) {
            int[] cur = it.next();
            int left = Math.max(rect[0], cur[0]);
            int right = Math.min(rect[2], cur[2]);
            int bottom = Math.max(rect[1], cur[1]);
            int top = Math.min(rect[3], cur[3]);
            if(left<right &&  bottom<top) { // if has overlap, break it to smaller non-overlap parts
                it.remove();
                if(cur[0]<rect[0]) temp.add(new int[]{cur[0], cur[1], rect[0], cur[3]});
                if(cur[2]>rect[2]) temp.add(new int[]{rect[2], cur[1], cur[2], cur[3]});
                if(cur[1]<rect[1]) temp.add(new int[]{left, cur[1], right, rect[1]}); // 用left，right保证这4块也没有重叠。。。
                if(cur[3]>rect[3]) temp.add(new int[]{left, rect[3], right, cur[3]});
            }
        }
        temp.add(rect);
        for(int[] r: temp) noOverlapRects.add(r);
    }


    // 把重叠长方形打碎成不重叠的长方形。。（每次打碎当前要加入的矩形）
    public int rectangleArea_v1(int[][] rectangles) {
        List<int[]> noOverlapRects = new ArrayList<>();

        // step1: 打碎成不重叠矩形
        for (int[] rect: rectangles) {
            add(noOverlapRects, 0, rect);
        }

        // step2: sum area
        long mod = (long) (Math.pow(10,9)+ 7);
        long sum=0;
        for(int[] rect: noOverlapRects) {
            sum+=(long)(rect[2]-rect[0])*(rect[3]-rect[1]);
            sum%=mod;
        }
        return (int)sum;
    }

    void add(List<int[]> noOverlapRects, int index, int[] rect) {
        if(index>=noOverlapRects.size()) { // finish check all, add to list
            noOverlapRects.add(rect);
            return;
        }

        int[] curRect = noOverlapRects.get(index);
        int left = Math.max(curRect[0], rect[0]);
        int right = Math.min(curRect[2], rect[2]);
        int bottom = Math.max(curRect[1], rect[1]);
        int top = Math.min(curRect[3], rect[3]);
        if(left>=right||bottom>=top) { // no overlap, check next...
            add(noOverlapRects, index+1, rect);
        } else { // has overlap, break it to smaller parts.
            if(rect[0]<curRect[0]) add(noOverlapRects, index+1, new int[]{rect[0], rect[1], curRect[0], rect[3]});
            if(rect[2]>curRect[2]) add(noOverlapRects, index+1, new int[]{curRect[2], rect[1], rect[2], rect[3]});
            if(rect[1]<curRect[1]) add(noOverlapRects, index+1, new int[]{rect[0], rect[1], rect[2], curRect[1]});
            if(rect[3]>curRect[3]) add(noOverlapRects, index+1, new int[]{rect[0], curRect[3], rect[2], rect[3]});
        }
    }

    public static void main(String[] args) {
//        int[][] rectangles = new int[][]{{0,0,2,2},{1,1,3,3}};
//        System.out.println(new notag.RectangleAreaII().rectangleArea(rectangles));
    }

}

/*
We are given a list of (axis-aligned) rectangles.  Each rectangle[i] = [x1, y1, x2, y2] ,
where (x1, y1) are the coordinates of the bottom-left corner, and (x2, y2) are the coordinates of the top-right corner of the ith rectangle.

Find the total area covered by all rectangles in the plane.  Since the answer may be too large, return it modulo 10^9 + 7.



Example 1:

Input: [[0,0,2,2],[1,0,2,3],[1,0,3,1]]
Output: 6
Explanation: As illustrated in the picture.
Example 2:

Input: [[0,0,1000000000,1000000000]]
Output: 49
Explanation: The answer is 10^18 modulo (10^9 + 7), which is (10^9)^2 = (-7)^2 = 49.
Note:

1 <= rectangles.length <= 200
rectanges[i].length = 4
0 <= rectangles[i][j] <= 10^9
The total area covered by all rectangles will never exceed 2^63 - 1 and thus will fit in a 64-bit signed integer.
*/