package _Topic_Iterator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

/*
 Created by thj on 25/06/2018.
  - LC 251 Flatten 2D Vector (https://www.lintcode.com/problem/flatten-2d-vector/description)
  - Airbnb Interview Version: ( http://storypku.com/tag/airbnb/)
      Given an array of arrays, implement an iterator class to  allow the client to traverse and remove elements
      in the array list. This iterator should provide three public class member functions:
          boolean has_next() return true or false if there is another element in the set
          int next() return the value of the next element in the set
          void remove() remove the last element returned by the iterator.
          That is, remove the element that the previous next() returned
          This method can be called only once per call to next(), otherwise, an exception will be thrown.

          See http://docs.oracle.com/javase/7/docs/api/java/util/Iterator.html#remove() for details.

          The code should be well structured, and robust enough to handle any access pattern.
          Additionally, write code to demonstrate that the class can be used for the following basic scenarios:
          Print elements Given: [[],[1,2,3],[4,5],[],[],[6],[7,8],[],[9],[10],[]]
          Print: 1 2 3 4 5 6 7 8 9 10
          Remove even elements Given: [[],[1,2,3],[4,5],[],[],[6],[7,8],[],[9],[10],[]]
          Should result in: [[],[1,3],[5],[],[],[],[7],[],[9],[],[]] Print: 1 3 5 7 9
          import java.io.*; import java.util.*; /** To execute Java, please define "static void main" on a class * named Airbnb_Solution.
           If you need more classes, simply define them inline.
*/
// Iterator version
class Vector2D_Itr implements Iterator<Integer> {

    List<List<Integer>> vec2d;
    Iterator<List<Integer>> rowItr;
    Iterator<Integer> colItr;

    public Vector2D_Itr(List<List<Integer>> vec2d){
        this.vec2d = vec2d;
        rowItr = vec2d.iterator();
        moveToNext();
    }

    private void moveToNext() {
        while(rowItr.hasNext() && (colItr==null || !colItr.hasNext()))
            colItr = rowItr.next().iterator();
    }

    @Override
    public Integer next() {
        if (!rowItr.hasNext() && !colItr.hasNext()) throw new NoSuchElementException();
        int res = colItr.next();
        moveToNext();
        return res;
    }

    @Override
    public boolean hasNext() {
        return colItr!=null && colItr.hasNext();
    }

    @Override
    public void remove() {
        // skip...idea is to store lastColItr...
    }
}

// 2p version, concise version
class Vector2D_2p implements Iterator<Integer> {
    int x,y;                      // index of next element to return
    int lastX, lastY;             // index of last element returned; -1 if no such
    List<List<Integer>> vec2d;
    public Vector2D_2p(List<List<Integer>> vec2d) {
        this.vec2d=vec2d;
        lastX=-1;lastY=-1; // for remove
        x=0;y=0;
        if (vec2d.size()>0 && vec2d.get(0).size()==0) moveToNext(); // a corner case...
    }

    private void moveToNext(){
        if (x>=vec2d.size()) return;
        if (y<vec2d.get(x).size()-1) y++;
        else {
            x++;
            while(x<vec2d.size() && vec2d.get(x).size()==0) x++;
            y=0;
        }
    }

    @Override
    public Integer next() {
        if (!hasNext()) throw new NoSuchElementException();
        int res = vec2d.get(x).get(y);
        lastX=x;lastY=y;
        moveToNext();
        return res;
    }

    @Override
    public boolean hasNext() {
        return x < vec2d.size() && y < vec2d.get(x).size();
    }

    @Override
    public void remove() {
        if (lastX==-1) throw new IllegalStateException(); // 每次next()只能删一次！
        vec2d.get(lastX).remove(lastY);
        if (lastX == x) y=lastY; // if same row move back y, else no move...
        lastX=-1;lastY=-1;
    }

    public static void main(String[] args) {
        List<List<Integer>> arr = new ArrayList<>();
        List<Integer> a1 = new ArrayList<>();
        List<Integer> a2 = new ArrayList<>();
        List<Integer> a3 = new ArrayList<>();
        a1.add(1); a1.add(2);a1.add(3);
        a2.add(4);
        arr.add(new ArrayList<>());
        arr.add(a1);
        arr.add(new ArrayList<>());
        arr.add(a2);

//        Vector2D_2p it = new Vector2D_2p(arr);
        Vector2D_Itr it = new Vector2D_Itr(arr);
//        it.next();
//        it.remove(); // remove 1
//        it.next();
//        it.remove();
//        it.next();
//        it.remove();
//        it = new Vector2D_2p(arr);
        while (it.hasNext()) {
            System.out.println(it.next());
        }
    }
}


   // second version, code is not concise
//public class Flatten2DVector_Array2DIterator {
//    private List<List<Integer>> arr;
//    private int p1, p2;
//
//    public Flatten2DVector_Array2DIterator(List<List<Integer>> arr){
//        this.arr = arr;
//        this.p1=this.p2=0;
//
//        while(p1<arr.size() && arr.get(p1).size() == 0) p1++;
//    }
//
//    boolean hasNext() {
//        return p1<arr.size() && p2<arr.get(p1).size();
//    }
//
//    int next() {
//        if (!hasNext()) return -1;
//
//        int res = arr.get(p1).get(p2);
//        if (p2 == arr.get(p1).size()-1) {
//            p1++;p2=0;
//            while(p1<arr.size() && arr.get(p1).size()==0) {p1++;}
//        } else {
//            p2++;
//        }
//        return res;
//    }
//
//    void remove() throws Exception {   // tan: 这里有问题，也无法保证每次next()只能删一次！
//        if (p2>0) {
//            List<Integer> listToRemove = arr.get(p1);
//            listToRemove.remove(p2-1);
//            p2--;
//        } else {
//            int preP1 = p1;
//            preP1--;
//            while(preP1>=0 && arr.get(preP1).size()==0) preP1--;
//            if (preP1<0) throw new Exception("xx");
//            arr.get(preP1).remove(arr.get(preP1).size()-1);
//        }
//    }
//
//    public static void main(String[] args) {
//        List<List<Integer>> arr = new ArrayList<>();
//        arr.add(new ArrayList<>());
//        List<Integer> list2 = new ArrayList<>();list2.add(1);list2.add(2);list2.add(3);
//        arr.add(list2);
//        List<Integer> list3 = new ArrayList<>();list3.add(4);list3.add(5);
//        arr.add(list3);
//
//        Flatten2DVector_Array2DIterator it = new Flatten2DVector_Array2DIterator(arr);
//        it.next();
//        it.next();
//        try {
//            it.remove();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        it = new Flatten2DVector_Array2DIterator(arr);
//        while (it.hasNext()){
//            System.out.println(it.next());
//        }
//    }
//}


// my first fail version...code is mess up....
//public class Flatten2DVector_Array2DIterator {
//    private  int[][] arr;
//    Pos pos;
//    private boolean removed;
//
//    class Pos{
//        int p1;
//        int p2;
//        public Pos(int p1, int p2) {this.p1=p1; this.p2=p2;}
//    }
//
//    public Flatten2DVector_Array2DIterator(int[][] arr) {
//        this.arr = arr;
//        this.pos = new Pos(0,0);
//    }
//
//    boolean hasNext() {
//        Pos temp = this.pos;
//        if (removed) temp = getNextPos(temp);
//        if (temp!=null) temp = getNextPos(temp);
//        return temp!=null;
//    }
//
//    int next() {
//        Pos temp = this.pos;
//        if (removed) temp = getNextPos(temp);
//        if (temp != null) temp = getNextPos(temp);
//        if (temp != null) {
//            this.pos = temp; // move
//        }
//
//        Pos pre = getPrePos(this.pos);
//        return arr[pre.p1][pre.p2];
//    }
//
//
//    public void remove() {
////        this.pos = getPrePos(this.pos);
//        this.removed = true;
//    }
//
//    private Pos getNextPos(Pos pos) {
//        int p1= pos.p1;
//        int p2= pos.p2;
//
//        while(this.arr[p1].length==0 && p1<=this.arr.length-1) p1++;
//
//        if(p2 <= this.arr[p1].length-2) {
//            return new Pos(p1, p2+1);
//        }
//        if (p2 == this.arr[p1].length-1) {
//            if(p1==this.arr.length-1) {
//                return null;
//            } else {
//                return new Pos(p1+1, 0);
//            }
//        }
//        return null;
//    }
//
//    private Pos getPrePos(Pos pos) {
//        int p1=pos.p1;
//        int p2=pos.p2;
//
//
//        if (p2 > 0) {
//            p2--;
//            return new Pos(p1,p2);
//        }
//        if (p2==0) {
//            if (p1 == 0) return null;
//            p1--;
//            while(this.arr[p1].length==0 && p1>0) p1--;
//            p2 = arr[p1].length-1;
//            return new Pos(p1,p2);
//
//        }
//        return null;
//    }
//}

/*
 Implement an iterator to flatten a 2d vector.
     For example,
     Given 2d vector =

     [
       [1,2],
       [3],
       [4,5,6]
     ]
  By calling next repeatedly until hasNext returns false, the order of elements returned by next should be: [1,2,3,4,5,6].

 Hint:
    How many variables do you need to keep track?
    Two variables is all you need. Try with x and y.
    Beware of empty rows. It could be the first few rows.
    To write correct code, think about the invariant to maintain. What is it?
    The invariant is x and y must always point to a valid point in the 2d vector. Should you maintain your invariant ahead of time or right when you need it?
    Not sure? Think about how you would implement hasNext(). Which is more complex?
    Common logic in two different places should be refactored into a common method.
  Follow up:
    As an added challenge, try to code it using only iterators in C++ or iterators in Java.
*/


/*
A good sample solution from https://www.jiuzhang.com/solution/flatten-2d-vector/#tag-other

//I looked through the discussions but didn't find a solution that is robust or implemented remove(). What if the interviewer ask you to do those stuff? Below are my understandings of this problem and a Java Iterator:
//  next() should always return a valid next element even if it is called without hasNext() before it.
//  next() should throw NoSuchElementException if there is no next element.
//  remove() will only be called after next() and can only be called once a time.
//  If vec2d contains null, we should skip it before create its iterator.

// Robust Iterator Implementation
public class Vector2D_2p implements Iterator<Integer> {
    private Iterator<List<Integer>> rowIter;
    private Iterator<Integer> colIter;

    public Vector2D_2p(List<List<Integer>> vec2d) {
        rowIter = vec2d.iterator();
        prepare();
    }

    // o(m) ~ o(1)
    private void prepare() {
        while (rowIter.hasNext()) {
            List<Integer> next = rowIter.next();
            if (next == null) continue;
            colIter = next.iterator(); // Handle if rowIter.next() returns a null.
            if (colIter.hasNext()) break;
        }
    }

    // o(1)
    @Override
    public boolean hasNext() {
        if (colIter == null || !colIter.hasNext()) prepare();
        return colIter != null && colIter.hasNext();
    }

    // o(m) ~ o(1)
    @Override
    public Integer next() {
        if (!hasNext()) throw new NoSuchElementException();
        return colIter.next();
    }

    // Will be called after next()
    @Override
    public void remove() {
        if (colIter == null) prepare();
        if (colIter != null) colIter.remove();
    }
}

// Robust Two Pointers Implementation
public class Vector2D_2p implements Iterator<Integer> {
    private List<List<Integer>> list;
    private int rowId, colId;

    public Vector2D_2p(List<List<Integer>> vec2d) {
        list = vec2d;
        rowId = 0;
        colId = 0;
    }

    private void prepare() {
        while (++rowId < list.size()) {
            List<Integer> next = list.get(rowId);
            if (next == null) continue;
            colId = 0;
            if (colId < next.size()) break;
        }
    }

    @Override
    public boolean hasNext() {
        if (rowId >= list.size()) return false;
        if (colId >= list.get(rowId).size()) prepare();
        return rowId < list.size() && colId < list.get(rowId).size();
    }

    @Override
    public Integer next() {
        if (!hasNext()) throw new NoSuchElementException();
        return list.get(rowId).get(colId++);
    }

    @Override
    public void remove() {  // tan: 这里有问题，无法保证每次next()只能删一次
        if (colId - 1 < 0) throw new IllegalStateException();
        list.get(rowId).remove(--colId);
    }
}
*/
