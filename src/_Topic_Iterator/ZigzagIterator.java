package _Topic_Iterator;

import java.util.*;

// just BFS
public class ZigzagIterator {

    LinkedList<Iterator> que;
    /*
     * @param v1: A 1d vector
     * @param v2: A 1d vector
     */
    public ZigzagIterator(List<Integer> v1, List<Integer> v2) {
        // do intialization if necessary
        que = new LinkedList<>();
        Iterator<Integer> it1 = v1.iterator();
        Iterator<Integer> it2 = v2.iterator();
        if(it1.hasNext()) que.add(v1.iterator());
        if(it2.hasNext()) que.add(v2.iterator());
    }

    /*
     * @return: An integer
     */
    public int next() {
        Iterator<Integer> cur = que.remove();
        int ans = cur.next();
        if (cur.hasNext()) que.add(cur);
        return ans;
    }

    /*
     * @return: True if has next
     */
    public boolean hasNext() {
        return !que.isEmpty();
    }

    public static void main(String[] args) {
        List<Integer> v1 = Arrays.asList(1,2);
        List<Integer> v2 = Arrays.asList(3,4,5,6);
        ZigzagIterator it = new ZigzagIterator(v1,v2);
        while (it.hasNext()) System.out.println(it.next());
    }
}

/**
 * Your _Topic_Iterator.ZigzagIterator object will be instantiated and called as such:
 * _Topic_Iterator.ZigzagIterator solution = new _Topic_Iterator.ZigzagIterator(v1, v2);
 * while (solution.hasNext()) result.add(solution.next());
 * Output result
 */

/*
Given two 1d vectors, implement an iterator to return their elements alternately.

Example
Example1

Input: v1 = [1, 2] and v2 = [3, 4, 5, 6]
Output: [1, 3, 2, 4, 5, 6]
Explanation:
By calling next repeatedly until hasNext returns false, the order of elements returned by next should be: [1, 3, 2, 4, 5, 6].
Example2

Input: v1 = [1, 1, 1, 1] and v2 = [3, 4, 5, 6]
Output: [1, 3, 1, 4, 1, 5, 1, 6]
*/