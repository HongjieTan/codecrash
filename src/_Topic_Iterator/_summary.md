通常处理方式:
- 两种玩法：pointer和iterator...
- 通常look ahead，
    - 如果是pointer方式：cursor point to element which will be return by next()
    - 如果是iterator方式：保存next元素 （如SkipIterator, PeekIterator...）
- 经常需要把cursor moveToNext 的逻辑封装在一个方法里 （因为经常不止next()里面要调用。。。如SkipIterator, 2DArrayIterator, PeekIterator...）
- 如果需要删除， 用一个last指针存上一个元素位置，删除后设置成-1（每次next()只能删一次），cursor回指一个位置（这个视情况，如2DArrayIterator...）

