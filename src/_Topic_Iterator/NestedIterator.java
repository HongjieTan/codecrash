package _Topic_Iterator;

/**
 * Created by thj on 03/06/2018.
 *
 *  iterative version
 *  recursive version ...
 *
 */

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Stack;

// This is the interface that allows for creating nested lists.
// You should not implement it, or speculate about its implementation
interface NestedInteger {
    // @return true if this NestedInteger holds a single integer, rather than a nested list.
    public boolean isInteger();
    // @return the single integer that this NestedInteger holds, if it holds a single integer
    // Return null if this NestedInteger holds a nested list
    public Integer getInteger();
    // @return the nested list that this NestedInteger holds, if it holds a nested list
    // Return null if this NestedInteger holds a single integer
    public List<NestedInteger> getList();
}
public class NestedIterator implements Iterator<Integer> {

    Stack<NestedInteger> stack;
    public NestedIterator(List<NestedInteger> nestedList) {
        stack = new Stack<>();
        for(int i=nestedList.size()-1;i>=0;i--) {
            stack.push(nestedList.get(i));
        }
    }
    @Override
    public Integer next() {
        return stack.pop().getInteger();
    }

    @Override
    public boolean hasNext() {
        while(!stack.isEmpty() && !stack.peek().isInteger()){
            NestedInteger cur = stack.pop();
            for(int i=cur.getList().size()-1; i>=0;i--) {
                stack.push(cur.getList().get(i));
            }
        }
        return !stack.isEmpty();
    }
}
// first naive version... no need to flatten all at constructor
//class NestedIterator_v0 implements Iterator<Integer> {
//
//    private Iterator<Integer> iterator;
//    public NestedIterator_v0(List<NestedInteger> nestedList) {
//        List<Integer> list = flatten(nestedList);
//        this.iterator = list.iterator();
//    }
//
//    private List<Integer> flatten(List<NestedInteger> nestedList) {
//        List<Integer> ans = new ArrayList<>();
//        Stack<NestedInteger> stack = new Stack<>();
//
//        for (int i=nestedList.size()-1; i>=0; i--) {
//            stack.push(nestedList.get(i));
//        }
//
//        while (!stack.isEmpty() ) {
//            NestedInteger temp = stack.pop();
//            if (temp.isInteger()) {
//                ans.add(temp.getInteger());
//            } else {
//                for (int i=temp.getList().size()-1; i>=0; i--) {
//                    stack.push(temp.getList().get(i));
//                }
//            }
//        }
//        return ans;
//    }
//
//    @Override
//    public Integer next() {
//        return iterator.next();
//    }
//
//    @Override
//    public boolean hasNext() {
//        return iterator.hasNext();
//    }
//}

/**
 * Your NestedIterator object will be instantiated and called as such:
 * NestedIterator i = new NestedIterator(nestedList);
 * while (i.hasNext()) v[f()] = i.next();
 */

/*
Given a nested list of integers, implement an iterator to flatten it.

Each element is either an integer, or a list -- whose elements may also be integers or other lists.

Example 1:

Input: [[1,1],2,[1,1]]
Output: [1,1,2,1,1]
Explanation: By calling next repeatedly until hasNext returns false,
             the order of elements returned by next should be: [1,1,2,1,1].
Example 2:

Input: [1,[4,[6]]]
Output: [1,4,6]
Explanation: By calling next repeatedly until hasNext returns false,
             the order of elements returned by next should be: [1,4,6].
*/