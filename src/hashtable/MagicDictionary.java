package hashtable;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 *
 * use pattern (apple -> app*e) as key can improve a little space...
 *
 */
public class MagicDictionary {

    Map<String, Integer> count;
    Set<String> words;

    /** Initialize your data structure here. */
    public MagicDictionary() {
        count =new HashMap<>();
        words=new HashSet<>();
    }

    /** Build a dictionary through a list of words */
    public void buildDict(String[] dict) {
        for(String word: dict) {
            words.add(word);
            char[] arr = word.toCharArray();
            for(int i=0;i<arr.length;i++) {
                char temp = arr[i];
                arr[i] = '*';
                String key = String.valueOf(arr);
                count.put(key, count.getOrDefault(key,0)+1);
                arr[i]=temp;
            }
        }
    }

    /** Returns if there is any word in the string._DataStructure_Trie_PrefixTree that equals to the given word after modifying exactly one character */
    public boolean search(String word) {
        char[] arr = word.toCharArray();
        for (int i = 0; i < arr.length; i++) {
            char temp = arr[i];
            arr[i]='*';
            String key = String.valueOf(arr);
            if (count.containsKey(key)
                    && (count.get(key)>1 || !words.contains(word))) return true;
            arr[i]=temp;
        }
        return false;
    }

    public static void main(String[] args) {
        MagicDictionary dic = new MagicDictionary();
        dic.buildDict(new String[]{"hello", "leetcode"});
        System.out.println(dic.search("hello"));
        System.out.println(dic.search("hhllo"));
        System.out.println(dic.search("hell"));
        System.out.println(dic.search("leetcoded"));

    }


    // brute force
//    Set<String> set;
//    /** Initialize your data structure here. */
//    public MagicDictionary() {
//        set=new HashSet<>();
//    }
//    /** Build a dictionary through a list of words */
//    public void buildDict(String[] dict) {
//        for(String word: dict) {
//            char[] arr = word.toCharArray();
//            for(int i=0;i<arr.length;i++) {
//                for(char c='a';c<'z';c++) {
//                    if(c!=arr[i]) {
//                        char temp=arr[i];
//                        arr[i]=c;
//                        set.add(String.valueOf(arr));
//                        arr[i]=temp;
//                    }
//                }
//            }
//        }
//    }
//    /** Returns if there is any word in the string._DataStructure_Trie_PrefixTree that equals to the given word after modifying exactly one character */
//    public boolean search(String word) {
//        return set.contains(word);
//    }
}
