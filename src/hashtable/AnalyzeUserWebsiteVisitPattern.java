/*
You are given three arrays username, timestamp and website of the same length N where the ith tuple means that the user with name username[i] visited the website website[i] at time timestamp[i].

A 3-sequence is a list of not necessarily different websites of length 3 sorted in ascending order by the time of their visits.

Find the 3-sequence visited at least once by the largest number of users. If there is more than one solution, return the lexicographically minimum solution.

A 3-sequence X is lexicographically smaller than a 3-sequence Y if X[0] < Y[0] or X[0] == Y[0] and (X[1] < Y[1] or X[1] == Y[1] and X[2] < Y[2]).

It is guaranteed that there is at least one user who visited at least 3 websites. No user visits two websites at the same time.



Example 1:

Input: username = ["joe","joe","joe","james","james","james","james","mary","mary","mary"], timestamp = [1,2,3,4,5,6,7,8,9,10], website = ["home","about","career","home","cart","maps","home","home","about","career"]
Output: ["home","about","career"]
Explanation:
The tuples in this example are:
["joe", 1, "home"]
["joe", 2, "about"]
["joe", 3, "career"]
["james", 4, "home"]
["james", 5, "cart"]
["james", 6, "maps"]
["james", 7, "home"]
["mary", 8, "home"]
["mary", 9, "about"]
["mary", 10, "career"]
The 3-sequence ("home", "about", "career") was visited at least once by 2 users.
The 3-sequence ("home", "cart", "maps") was visited at least once by 1 user.
The 3-sequence ("home", "cart", "home") was visited at least once by 1 user.
The 3-sequence ("home", "maps", "home") was visited at least once by 1 user.
The 3-sequence ("cart", "maps", "home") was visited at least once by 1 user.


Note:

3 <= N = username.length = timestamp.length = website.length <= 50
1 <= username[i].length <= 10
0 <= timestamp[i] <= 10^9
1 <= website[i].length <= 10
Both username[i] and website[i] contain only lowercase characters.
*/
package hashtable;

import java.util.*;

/*
    idea is straightforward(use map,set...), just a lot of coding...
 */
public class AnalyzeUserWebsiteVisitPattern {
    class Action {
        String user;
        int time;
        String web;
    }
    public List<String> mostVisitedPattern(String[] username, int[] timestamp, String[] website) {
        int n = username.length;
        List<Action> actions = new ArrayList<>();
        for(int i=0;i<n;i++) {
            Action a = new Action();
            a.user = username[i];
            a.time = timestamp[i];
            a.web = website[i];
            actions.add(a);
        }

        Map<String, List<Action>> map = new HashMap<>();
        for(Action action: actions) {
            if(!map.containsKey(action.user)) map.put(action.user, new ArrayList<>());
            map.get(action.user).add(action);
        }

        Map<String, Integer> counter = new HashMap<>();
        for(List<Action> acts: map.values()) {
            Collections.sort(acts, (a, b)->a.time-b.time);
            Set<String> pset = new HashSet<>();
            int m = acts.size();
            for(int i=0;i<m;i++) {
                for(int j=i+1;j<m;j++) {
                    for(int k=j+1;k<m;k++) {
                        String p = acts.get(i).web + "#" + acts.get(j).web + "#" + acts.get(k).web;
                        pset.add(p);
                    }
                }
            }

            for (String p: pset) counter.put(p, counter.getOrDefault(p, 0)+1);
        }

        String maxp="";
        int maxcnt=0;
        for(String p: counter.keySet()) {
            if(counter.get(p)>maxcnt ||
                    (counter.get(p)==maxcnt && p.compareTo(maxp)<0 )
            ) {
                maxp = p;
                maxcnt = counter.get(p);
            }
        }

        return  Arrays.asList(maxp.split("#"));
    }

    public static void main(String[] as) {
        String username[] = {"h","eiy","cq","h","cq","txldsscx","cq","txldsscx","h","cq","cq"};
        int timestamp[] = {527896567,334462937,517687281,134127993,859112386,159548699,51100299,444082139,926837079,317455832,411747930};
        String website[] = {"hibympufi","hibympufi","hibympufi","hibympufi","hibympufi","hibympufi","hibympufi","hibympufi","yljmntrclw","hibympufi","yljmntrclw"};
        List<String> res = new AnalyzeUserWebsiteVisitPattern().mostVisitedPattern(username, timestamp, website);
        for (String x: res) System.out.println(x);
    }
}




/*

by uwi:
public List<String> mostVisitedPattern(String[] username, int[] timestamp, String[] website) {
        int n = username.length;

        Set<String> ws = new HashSet<>();
        for(String w : website){
            ws.add(w);
        }
        List<String> wl = new ArrayList<>(ws);

        Map<String, List<Datum>> map = new HashMap<>();
        for(int i = 0;i < n;i++){
            Datum d = new Datum(username[i], timestamp[i], website[i]);
            if(!map.containsKey(username[i])){
                map.put(username[i], new ArrayList<Datum>());
            }
            map.get(username[i]).add(d);
        }
        for(List<Datum> l : map.values()){
            Collections.sort(l, new Comparator<Datum>() {
                public int compare(Datum a, Datum b) {
                    return a.t - b.t;
                }
            });
        }

        String[] best = new String[3];
        int bs = -1;
        int m = wl.size();
        for(int i = 0;i < m;i++){
            for(int j = 0;j < m;j++){
                for(int k = 0;k < m;k++){
                    int hit = 0;
                    String[] hi = new String[]{wl.get(i), wl.get(j), wl.get(k)};
                    for(List<Datum> l : map.values()){
                        int s = 0;
                        for(Datum d : l){
                            if(s == 0){
                                if(d.w.equals(wl.get(i)))s = 1;
                            }else if(s == 1){
                                if(d.w.equals(wl.get(j)))s = 2;
                            }else if(s == 2){
                                if(d.w.equals(wl.get(k)))s = 3;
                            }
                        }
                        if(s == 3){
                            hit++;
                        }
                    }
                    if(hit > bs || hit == bs && comp(best, hi) > 0){
                        best = hi;
                        bs = hit;
                    }
                }
            }
        }
        List<String> last = new ArrayList<>();
        for(String u : best)last.add(u);
        return last;
    }

    int comp(String[] a, String[] b)
    {
        for(int i = 0;i < a.length;i++){
            int t = a[i].compareTo(b[i]);
            if(t != 0)return t;
        }
        return 0;
    }

    class Datum
    {
        String u;
        int t;
        String w;
        public Datum(String u, int t, String w) {
            this.u = u;
            this.t = t;
            this.w = w;
        }
    }
}



top time solution:

class Solution {

	private static class Visit implements Comparable<Visit> {
		public int timestamp;
		public String website;
		public Visit(int timestamp , String website) {
			this.timestamp = timestamp;
			this.website = website;
		}
		@Override
		public int compareTo(Visit visit) {
			return this.timestamp - visit.timestamp;
		}
	}

	private static class Tuple implements Comparable<Tuple> {
		public String s1 , s2 , s3;
		public Tuple(String s1 , String s2 , String s3) {
			this.s1 = s1;
			this.s2 = s2;
			this.s3 = s3;
		}
		@Override
		public int compareTo(Tuple tuple) {
			int result = this.s1.compareTo(tuple.s1);
			if (result != 0) {
				return result;
			} else {
				result = this.s2.compareTo(tuple.s2);
				if (result != 0) {
					return result;
				} else {
					return this.s3.compareTo(tuple.s3);
				}
			}
		}
		@Override
		public int hashCode() {
			int ans = 31;
			ans = ans * 37 + s1.hashCode();
			ans = ans * 37 + s2.hashCode();
			ans = ans * 37 + s3.hashCode();
			return ans;
		}
		@Override
		public boolean equals(Object object) {
			Tuple tuple = (Tuple) object;
			return this.s1.equals(tuple.s1) &&
				   this.s2.equals(tuple.s2) &&
				   this.s3.equals(tuple.s3);
		}
	}

	private void parseTuple(List<Visit> list) {
		Set<Tuple> set = new HashSet<>();
		int i , j , k , n = list.size();
		for (i = 0;i < n;i ++) {
			for (j = i + 1;j < n;j ++) {
				for (k = j + 1;k < n;k ++) {
					Tuple tuple = new Tuple(list.get(i).website , list.get(j).website , list.get(k).website);
					set.add(tuple);
				}
			}
		}
		for (Tuple tuple : set) {
			tupleMap.put(tuple , tupleMap.getOrDefault(tuple , 0) + 1);
		}
	}

	private Map<Tuple , Integer> tupleMap = new HashMap<>();

    public List<String> mostVisitedPattern(String[] username, int[] timestamp, String[] website) {
    	Map<String , List<Visit>> map = new HashMap<>();
    	int i , n = username.length;
    	for (i = 0;i < n;i ++) {
    		Visit visit = new Visit(timestamp[i] , website[i]);
    		if (!map.containsKey(username[i])) {
    			map.put(username[i] , new ArrayList<>());
    		}
    		map.get(username[i]).add(visit);
    	}
    	for (List<Visit> list : map.values()) {
    		Collections.sort(list);
    		parseTuple(list);
    	}
    	int max = 0;
    	Tuple ans = null;
    	for (Map.Entry<Tuple , Integer> entry : tupleMap.entrySet()) {
    		if (entry.getValue() > max) {
    			max = entry.getValue();
    			ans = entry.getKey();
    		} else if (entry.getValue() == max) {
    			if (entry.getKey().compareTo(ans) < 0) {
    				ans = entry.getKey();
    			}
    		}
    	}
    	if (ans == null) {
    		return Arrays.asList();
    	} else {
    		return Arrays.asList(ans.s1 , ans.s2 , ans.s3);
    	}
    }

}
*/