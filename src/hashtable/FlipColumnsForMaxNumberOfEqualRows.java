/*
Given a matrix consisting of 0s and 1s, we may choose any number of columns in the matrix and flip every cell in that column.
Flipping a cell changes the value of that cell from 0 to 1 or from 1 to 0.

Return the maximum number of rows that have all values equal after some number of flips.

Example 1:

Input: [[0,1],[1,1]]
Output: 1
Explanation: After flipping no values, 1 row has all values equal.
Example 2:

Input: [[0,1],[1,0]]
Output: 2
Explanation: After flipping values in the first column, both rows have equal values.
Example 3:

Input: [[0,0,0],[0,0,1],[1,1,0]]
Output: 2
Explanation: After flipping values in the first two columns, the last two rows have equal values.


Note:

1 <= matrix.length <= 300
1 <= matrix[i].length <= 300
All matrix[i].length's are equal
matrix[i][j] is 0 or 1
*/
package hashtable;

import java.util.HashMap;
import java.util.Map;


/*
    hashmap, counting by pattern...
 */
public class FlipColumnsForMaxNumberOfEqualRows {
    public int maxEqualRowsAfterFlips(int[][] matrix) {
        Map<String, Integer> map = new HashMap<>();
        int res=0;
        for(int[] r: matrix) {
            String p = getPattern(r);
            map.put(p, map.getOrDefault(p,0)+1);
            res = Math.max(res, map.get(p));
        }
        return res;
    }
    String getPattern(int[] r) {
        StringBuilder sb = new StringBuilder();
        for(int x:r) sb.append(x^r[0]).append("#");
        return sb.toString();
    }

    public static void main(String[] a) {
        int[][] matrix = {{0,0,0},{0,0,1},{1,1,0}};
        System.out.println(new FlipColumnsForMaxNumberOfEqualRows().maxEqualRowsAfterFlips(matrix));
    }
}

/*
    def maxEqualRowsAfterFlips(self, matrix):
        return max(collections.Counter(tuple(x ^ r[0] for x in r) for r in matrix).values())

*/