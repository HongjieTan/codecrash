package hashtable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by thj on 24/05/2018.
 *
 *
 * to improve? bit manipulation ?
 *
 */
public class RepeatedDNASequences {
    public List<String> findRepeatedDnaSequences(String s) {

        List<String> res = new ArrayList<>();
        Map<String, Integer> map = new HashMap<>();
        for (int i=0; i<s.length()-9; i++) {
            String subStr = s.substring(i, i+10);
            map.put(subStr, map.getOrDefault(subStr, 0) + 1);
        }

        for (Map.Entry entry: map.entrySet()) {
            if ((int)entry.getValue() > 1) {
                res.add((String) entry.getKey());
            }
        }

        return res;

    }
}
