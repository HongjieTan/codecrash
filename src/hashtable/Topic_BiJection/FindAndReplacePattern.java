package hashtable.Topic_BiJection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FindAndReplacePattern {

    // 双射，用两个map
    public List<String> findAndReplacePattern(String[] words, String pattern) {
        List<String> ans = new ArrayList<>();
        for(String word: words) {
            Map<Character, Character> dic1 = new HashMap<>();
            Map<Character, Character> dic2 = new HashMap<>();
            boolean match = true;
            for(int i=0;i<word.length();i++) {

                if ( (dic1.containsKey(word.charAt(i)) && dic1.get(word.charAt(i))!= pattern.charAt(i))
                    || (dic2.containsKey(pattern.charAt(i)) && dic2.get(pattern.charAt(i))!= word.charAt(i)) ) {
                    match = false;
                    break;
                } else {
                    dic1.put(word.charAt(i), pattern.charAt(i));
                    dic2.put(pattern.charAt(i), word.charAt(i));
                }
            }
            if(match) ans.add(word);
        }
        return ans;
    }

    public static void main(String[] args){
//        ["abc","deq","mee","aqq","dkd","ccc"]
//        "abb"
//        Output ["abc","deq","mee","aqq"]
//        Expected ["mee","aqq"]
        String[] words = new String[]{"abc","deq","mee","aqq","dkd","ccc"};
        String pattern = "abb";
        List<String> ans = new FindAndReplacePattern().findAndReplacePattern(words, pattern);
        System.out.println(ans.size());
    }
}

/*
You have a list of words and a pattern, and you want to know which words in words matches the pattern.

A word matches the pattern if there exists a permutation of letters p so that after replacing every letter x in the pattern with p(x), we get the desired word.

(Recall that a permutation of letters is a bijection from letters to letters: every letter maps to another letter, and no two letters map to the same letter.)

Return a list of the words in words that match the given pattern.

You may return the answer in any order.



Example 1:

Input: words = ["abc","deq","mee","aqq","dkd","ccc"], pattern = "abb"
Output: ["mee","aqq"]
Explanation: "mee" matches the pattern because there is a permutation {a -> m, b -> e, ...}.
"ccc" does not match the pattern because {a -> c, b -> c, ...} is not a permutation,
since a and b map to the same letter.


Note:

1 <= words.length <= 50
1 <= pattern.length = words[i].length <= 20
*/