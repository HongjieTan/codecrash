package hashtable.Topic_BiJection;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class IsomorphicStrings {

    public boolean isIsomorphic(String s, String t) {
        int[] table1 = new int[256], table2 = new int[256];
        for (int i = 0; i < s.length(); i++) {
            char c1 = s.charAt(i), c2 = t.charAt(i);
            if (table1[c1] != table2[c2]) return false;
            table1[c1] = i+1;
            table2[c2] = i+1;
        }
        return true;
    }

    public boolean isIsomorphic_v1(String s, String t) {
        Map<Character, Character> transferDic = new HashMap<>();
        Set<Character> used = new HashSet<>();
        for (int i = 0; i < s.length(); i++) {
            char sc = s.charAt(i);
            char tc = t.charAt(i);
            if (!transferDic.containsKey(sc) ) {
                if (used.contains(tc)) return false;
                transferDic.put(sc, tc);
                used.add(tc);
            } else {
                if (transferDic.get(sc) != tc) return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        //...
    }

}

/*
Given two strings s and t, determine if they are isomorphic.

Two strings are isomorphic if the characters in s can be replaced to get t.

All occurrences of a character must be replaced with another character while preserving the order of characters.
No two characters may map to the same character but a character may map to itself.

Example 1:

Input: s = "egg", t = "add"
Output: true
Example 2:

Input: s = "foo", t = "bar"
Output: false
Example 3:

Input: s = "paper", t = "title"
Output: true
Note:
You may assume both s and t have the same length.
*/