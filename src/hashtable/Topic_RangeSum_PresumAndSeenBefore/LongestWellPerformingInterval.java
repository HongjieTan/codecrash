/*
We are given hours, a list of the number of hours worked per day for a given employee.

A day is considered to be a tiring day if and only if the number of hours worked is (strictly) greater than 8.

A well-performing interval is an interval of days for which the number of tiring days is strictly larger than the number of non-tiring days.

Return the length of the longest well-performing interval.



Example 1:

Input: hours = [9,9,6,0,6,6,9]
Output: 3
Explanation: The longest well-performing interval is [9,9,6].


Constraints:

1 <= hours.length <= 10000
0 <= hours[i] <= 16
*/
package hashtable.Topic_RangeSum_PresumAndSeenBefore;


import java.util.HashMap;
import java.util.Map;


// Note: sliding window not work.... the reason, check twopointers._Topic_SubStringSearch_SlidingWindow._readme

public class LongestWellPerformingInterval {

    int longestWPI(int[] hours) {
        int n=hours.length, res=0, presum=0;
        Map<Integer, Integer> seen = new HashMap<>();
        seen.put(0, -1);
        for(int i=0;i<n;i++) {
            presum += hours[i]>8?+1:-1;
            seen.putIfAbsent(presum, i);
            if (presum>0) {
                res = i+1;
            } else if(seen.containsKey(presum-1)) {
                res = Math.max(res, i-seen.get(presum-1));
            }
        }
        return res;
    }


    public static void main(String[] a) {
        System.out.println(new LongestWellPerformingInterval().longestWPI(new int[]{9,9,6,0,6,6,9}));
        System.out.println(new LongestWellPerformingInterval().longestWPI(new int[]{9,6,9,6,9}));
    }
}

/*
by lee:
Intuition
If working hour > 8 hours, yes it's tiring day.
But I doubt if 996 is a well-performing interval.
Life needs not only 996 but also 669.


Explanation
We starts with a score = 0,
If the working hour > 8, we plus 1 point.
Otherwise we minus 1 point.
We want find the maximum interval that have strict positive score.

After one day of work, if we find the total score > 0,
the whole interval has positive score,
so we set res = i + 1.

If the score is a new lowest score, we record the day by seen[cur] = i.
seen[score] means the first time we see the score is seen[score]th day.

We want a positive score, so we want to know the first occurrence of score - 1.
score - x also works, but it comes later than score - 1.
So the maximum interval is i - seen[score - 1]


Complexity
Time O(N) for one pass.
Space O(N) in worst case if no tiring day.


Java:

    public int longestWPI(int[] hours) {
        int res = 0, score = 0, n = hours.length;
        Map<Integer, Integer> seen = new HashMap<>();
        seen.put(0, -1);
        for (int i = 0; i < n; ++i) {
            score += hours[i] > 8 ? 1 : -1;
            if (score > 0) {
                res = i + 1;
            } else {
                seen.putIfAbsent(score, i);
                if (seen.containsKey(score - 1))
                    res = Math.max(res, i - seen.get(score - 1));
            }
        }
        return res;
    }
C++:

    int longestWPI(vector<int>& hours) {
        int res = 0, score = 0, n = hours.size();
        unordered_map<int, int> seen = {{0, -1}};
        for (int i = 0; i < n; ++i) {
            score += hours[i] > 8 ? 1 : -1;
            if (score > 0) {
                res = i + 1;
            } else {
                if (seen.find(score) == seen.end())
                    seen[score] = i;
                if (seen.find(score - 1) != seen.end())
                    res = max(res, i - seen[score - 1]);
            }
        }
        return res;
    }
Python:

    def longestWPI(self, hours):
        res = score = 0
        seen = {}
        for i, h in enumerate(hours):
            score = score + 1 if h > 8 else score - 1
            if score > 0:
                res = i + 1
            seen.setdefault(score, i)
            if score - 1 in seen:
                res = max(res, i - seen[score - 1])
        return res

*/