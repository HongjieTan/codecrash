package hashtable.Topic_RangeSum_PresumAndSeenBefore;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by thj on 23/05/2018.
 *
 *    LC 523
 *
 *
 *    similar with SubarraySumEqualsK,  expect the math part here is:  y-x=nk <=> y%k-x%k=nk%k=0 <=> y%k==x%k
 */
public class SubArrSum2 {

    public boolean checkSubarraySum(int[] nums, int k) {
        Map<Integer, Integer> seen = new HashMap<>();
        seen.put(0,-1);
        int sum=0;
        for(int i=0;i<nums.length;i++) {
            sum+=nums[i];
            if(k!=0) sum%=k;
            Integer prev = seen.getOrDefault(sum, null);
            if(prev!=null) {
                if (i-prev>1)
                    return true;
            } else {
                seen.put(sum, i);
            }
        }
        return false;
    }

    public static void main(String[] args) {
        System.out.println(new SubArrSum2().checkSubarraySum(new int[]{1,0,0}, 0));
    }
}


/*
Given a list of non-negative numbers and a target integer k, write a function to check if the array has a continuous
subarray of size at least 2 that sums up to the multiple of k, that is, sums up to n*k where n is also an integer.



Example 1:

Input: [23, 2, 4, 6, 7],  k=6
Output: True
Explanation: Because [2, 4] is a continuous subarray of size 2 and sums up to 6.
Example 2:

Input: [23, 2, 6, 4, 7],  k=6
Output: True
Explanation: Because [23, 2, 6, 4, 7] is an continuous subarray of size 5 and sums up to 42.


Note:

The length of the array won't exceed 10,000.
You may assume the sum of all the numbers is in the range of a signed 32-bit integer.
*/