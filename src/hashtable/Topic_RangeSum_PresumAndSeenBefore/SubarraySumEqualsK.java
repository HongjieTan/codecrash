/*
Given an array of integers and an integer k, you need to find the total number of continuous subarrays whose sum equals to k.

Example 1:
Input:nums = [1,1,1], k = 2
Output: 2
Note:
The length of the array is in range [1, 20,000].
The range of numbers in the array is [-1000, 1000] and the range of the integer k is [-1e7, 1e7].
*/

//    I tried sliding window, working well in all-positive array, but not working for negative elements.

package hashtable.Topic_RangeSum_PresumAndSeenBefore;

import java.util.HashMap;
import java.util.Map;

/**
 *  - presum + hashtable: O(n) !!  cool!!  check during traverse using seen
 *  - brute force using presum: O(n^2)
 *  - brute force: O(n^3)
 *
 *  - Notice!!! "2 pointers"  is not suitable here, as the elements of the array are not all positives.
 *            so, making end increase doesn't mean the sum increase, and making start doesn't mean the sum decrease.
 *
 *
 */
public class SubarraySumEqualsK {

    // memory improved version...
    public int subarraySum(int[] nums, int k) {
        int presum = 0, count=0;
        Map<Integer, Integer> map = new HashMap<>();
        map.put(0,1);
        for (int i=0;i<nums.length;i++) {
            presum += nums[i];
            if (map.containsKey(presum-k)) {
                count += map.get(presum-k);
            }
            map.put(presum, map.getOrDefault(presum,0)+1);
        }
        return count;
    }

    public int subarraySum_v1(int[] nums, int k) {
        int[] presum = new int[nums.length];
        presum[0] = nums[0];
        for (int i = 1; i < nums.length; i++) presum[i] = presum[i-1]+nums[i];
        int res = 0;
        Map<Integer, Integer> seen = new HashMap<>();
        seen.put(0, 1);
        for (int sum: presum) {
            int target = sum-k;
            if (seen.containsKey(target)) res+=seen.get(target);
            seen.put(sum, seen.getOrDefault(sum,0)+1);
        }
        return res;
    }





    public static void main(String[] args) {
        System.out.println(new SubarraySumEqualsK().subarraySum(new int[]{1,1,1}, 2));
    }
}

