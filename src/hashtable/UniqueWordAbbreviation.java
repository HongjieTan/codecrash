package hashtable;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class UniqueWordAbbreviation {

}

class ValidWordAbbr {

    Map<String, Set<String>> map = new HashMap<>();
    public ValidWordAbbr(String[] dictionary) {
        for(String word: dictionary) {
            String abbrev = getAbbrev(word);
            if (!map.containsKey(abbrev)) map.put(abbrev, new HashSet<>());
            map.get(abbrev).add(word);
        }
    }
    public boolean isUnique(String word) {
        String abbrev = getAbbrev(word);
        if (!map.containsKey(abbrev)) return true;  // case1: abbrev is new
        else if (map.get(abbrev).size()==1 && map.get(abbrev).contains(word)) return true; // case2: abbrev exist but the word is itself and only itself in dictionary...
        return false;
    }
    private String getAbbrev(String word) {
        if (word.length()<=2) return word;
        StringBuffer sb = new StringBuffer();
        sb.append(word.charAt(0)).append(word.length()-2).append(word.charAt(word.length()-1));
        return sb.toString();
    }
}

/**
 * Your hashtable.ValidWordAbbr object will be instantiated and called as such:
 * hashtable.ValidWordAbbr obj = new hashtable.ValidWordAbbr(dictionary);
 * boolean param = obj.isUnique(word);
 *
 * Assume you have a dictionary and given a word, find whether its abbreviation is unique in the dictionary.
 * A word's abbreviation is unique if no other word from the dictionary has the same abbreviation.
 */
//An abbreviation of a word follows the form . Below are some examples of word abbreviations:
//
//        a) it                      --> it    (no abbreviation)
//
//        1
//        b) d|o|g                   --> d1g
//
//        1    1  1
//        1---5----0----5--8
//        c) i|nternationalizatio|n  --> i18n
//
//        1
//        1---5----0
//        d) l|ocalizatio|n          --> l10n
//Input
//        ["a","a"]
//        isUnique("a")
//        Output
//        false
//        Expected
//        true

//Input
//        ["deer","door","cake","card"]
//        isUnique("dear")
//        isUnique("cart")
//        isUnique("cane")
//        isUnique("make")
//        Output
//        false
//        false
//        true
//        false
//        Expected
//        false
//        true
//        false
//        true

//Input
//        ["dog"]
//        isUnique("dig")
//        isUnique("dug")
//        isUnique("dag")
//        isUnique("dog")
//        isUnique("doge")
//        Output
//        false
//        false
//        false
//        false
//        true
//        Expected
//        false
//        false
//        false
//        true
//        true

//Input
//        ["a","a"]
//        isUnique("a")
//        Output
//        false
//        Expected
//        true