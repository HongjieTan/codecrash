package hashtable.Topic_BucketSort;

import java.util.Arrays;

/**
 * Created by thj on 2018/9/1.
 *
 *  1. bucket sort（counting sort）: o(n)*
 *  2. sort: o(nlogn)
 *
 *
 */
public class HIndex {

    // bucket sort !! good!
    public int hIndex_x2(int[] citations) {
        int n = citations.length;
        int[] buckets = new int[n+1];
        for(int cit: citations) {
            if(cit>=n) ++buckets[n];
            else  ++buckets[cit];
        }

        int sum = 0;
        for(int h=n;h>=0;h--) {
            sum+=buckets[h];
            if(sum>=h) return h;
        }
        return 0;
    }

    // bucket sort, O(n), best!!!
    public int hIndex_v2(int[] citations) {
        int n = citations.length;
        int[] count = new int[n+1];
        for (int i = 0; i < n; i++) {
            count[citations[i]<n?citations[i]:n]++;
        }
        int sum = 0;
        for (int h=n; h>=0; h--) {
            sum+=count[h];
            if (sum >= h) return h;
        }
        return 0;
    }

    // sort: O(nlogn)
//    public int hIndex(int[] citations) {
//        Arrays.sort(citations);
//
//        int n = citations.length;
//        for (int h = n; h > 0 ; h--) {
//            if (citations[n-h]>=h) return h;
//        }
//        return 0;
//    }

    public int hIndex(int[] citations) {
        Arrays.sort(citations);
        int n=citations.length;
        for(int i=n-1;i>=0;i--) {
            int h=citations[i];
            if(n-i>=h && i>=n-h) return h;
        }
        return -1;
    }

    public static void main(String[] args) {
        int[] citations = new int[]{3,0,6,1,5};
//        System.out.println(new HIndex().hIndex_v2(citations));
        System.out.println(new HIndex().hIndex(citations));
    }


}




/*
Given an array of citations (each citation is a non-negative integer) of a researcher, write a function to compute the researcher's h-index.
According to the definition of h-index on Wikipedia:
"A scientist has index h if h of his/her N papers have at least h citations each,and the other N − h papers have no more than h citations each."

Example:

Input: citations = [3,0,6,1,5]
Output: 3
Explanation: [3,0,6,1,5] means the researcher has 5 papers in total and each of them had
             received 3, 0, 6, 1, 5 citations respectively.
             Since the researcher has 3 papers with at least 3 citations each and the remaining
             two with no more than 3 citations each, her h-index is 3.
Note: If there are several possible values for h, the maximum one is taken as the h-index.

hint1: An easy approach is to sort the array first.
hint2: What are the possible values of h-index?
hint3: A faster approach is to use extra space.
*/


/*
This type of problems always throw me off, but it just takes some getting used to. The idea behind it is some bucket sort mechanisms.
First, you may ask why bucket sort. Well, the h-index is defined as the number of papers with reference greater than the number.
So assume n is the total number of papers, if we have n+1 buckets, number from 0 to n, then for any paper with reference corresponding to the index of the bucket, we increment the count for that bucket.
The only exception is that for any paper with larger number of reference than n, we put in the n-th bucket.

Then we iterate from the back to the front of the buckets, whenever the total count exceeds the index of the bucket,
meaning that we have the index number of papers that have reference greater than or equal to the index.
Which will be our h-index result.
The reason to scan from the end of the array is that we are looking for the greatest h-index.
 For example, given array [3,0,6,5,1], we have 6 buckets to contain how many papers have the corresponding index.
 Hope to image and explanation help.

Buckets

public int hIndex(int[] citations) {
    int n = citations.length;
    int[] buckets = new int[n+1];
    for(int c : citations) {
        if(c >= n) {
            buckets[n]++;
        } else {
            buckets[c]++;
        }
    }
    int count = 0;
    for(int i = n; i >= 0; i--) {
        count += buckets[i];
        if(count >= i) {
            return i;
        }
    }
    return 0;
}

*/



/*


*/






