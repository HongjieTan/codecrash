package hashtable.Topic_PointsInCoordinateSystem;

/**
 * Created by thj on 24/05/2018.
 */

import common.Point;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * Definition for a point.
 * class Point {
 *     int x;
 *     int y;
 *     Point() { x = 0; y = 0; }
 *     Point(int a, int b) { x = a; y = b; }
 * }
 */


public class MaxPointsOnLine {


    public int maxPoints(Point[] points) {

        int max = 0;
        Map<BigDecimal, Integer> kMap = new HashMap<>();

        for (int i=0; i<points.length; i++) {
            int sameCount=0;
            int infiniteKCount = 0;
            Point basePoint = points[i];

            kMap.clear();
            for (int j=i+1; j<points.length;j++) {
                Point rPoint = points[j];
                if (basePoint.x == rPoint.x && basePoint.y==rPoint.y) {sameCount++;}
                else if (basePoint.x == rPoint.x) {
//                    kMap.put(BigDecimal.valueOf(Double.MAX_VALUE), kMap.getOrDefault(BigDecimal.valueOf(Double.MAX_VALUE), 0)+1);
                    // MAX_VALUE may also be a valid k, so user addtional counter to record it.
                    infiniteKCount++;
                } else {
                    BigDecimal k = getK(basePoint, rPoint);
                    kMap.put(k, kMap.getOrDefault(k, 0)+1);
                }
            }

            int maxKCount = 0;
            for (Map.Entry entry: kMap.entrySet()) {
                maxKCount = Math.max(maxKCount, (int)entry.getValue());
            }


            max = Math.max(max, 1+maxKCount+sameCount+infiniteKCount);

        }

        return max;

    }


    private BigDecimal getK(Point a, Point b) {
//        return (double)(a.y -b.y)/(double)(a.x-b.x); // double will cause precision problem
        return BigDecimal.valueOf(a.y-b.y).divide(BigDecimal.valueOf(a.x-b.x));
    }
}
