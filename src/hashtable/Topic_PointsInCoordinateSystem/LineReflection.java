package hashtable.Topic_PointsInCoordinateSystem;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by thj on 2018/9/3.
 *
 *  time: O(n), space O(n)
 *
 *
 */
public class LineReflection {

    public boolean isReflected(int[][] points) {
        // Write your code here
        if (points==null || points.length==0) return true;

        int xMax = Integer.MIN_VALUE;
        int xMin = Integer.MAX_VALUE;
        Set<String> set = new HashSet<>();
        for(int[] p: points) {
            xMax = Math.max(xMax, p[0]);
            xMin = Math.min(xMin, p[0]);
            set.add(getKey(p[0], p[1]));
        }

        int mid_x_2 = xMax + xMin;
        for(int[] p: points) {
            if (!set.contains(getKey(mid_x_2-p[0], p[1]))) return false;
        }
        return true;
    }

    private String getKey(int x, int y) {
        return x+"#"+y;
    }

    public static void main(String[] args) {
        int[][] pts = new int[][]{
                {0, 0},
                {1, 0},
                {3, 0}
        };
        System.out.println(new LineReflection().isReflected(pts));
    }
}
