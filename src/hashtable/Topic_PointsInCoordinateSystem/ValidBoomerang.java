//A boomerang is a set of 3 points that are all distinct and not in a straight line.
//
// Given a list of three points in the plane, return whether these points are a boomerang.
//
//
//
// Example 1:
//
//
//Input: [[1,1],[2,3],[3,2]]
//Output: true
//
//
//
// Example 2:
//
//
//Input: [[1,1],[2,2],[3,3]]
//Output: false
//
//
//
//
// Note:
//
//
// points.length == 3
// points[i].length == 2
// 0 <= points[i][j] <= 100
//
//
//
//
//
package hashtable.Topic_PointsInCoordinateSystem;

public class ValidBoomerang {
    public boolean isBoomerang(int[][] points) {
        int x0=points[0][0], y0=points[0][1];
        int x1=points[1][0], y1=points[1][1];
        int x2=points[2][0], y2=points[2][1];
        return (y2-y0)*(x1-x0) != (y1-y0)*(x2-x0);
    }
}
