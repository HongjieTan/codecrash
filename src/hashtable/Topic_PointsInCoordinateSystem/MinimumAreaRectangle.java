package hashtable.Topic_PointsInCoordinateSystem;

import java.util.*;

/**
 *  每两个对角点可以唯一确定一个矩形，遍历所有这些对： O（n^2）
 *
 */
public class MinimumAreaRectangle {
    public int minAreaRect(int[][] points) {
        Set<String> set = new HashSet<>();
        for(int[] p: points) {
            set.add(p[0]+"#"+p[1]);
        }
        int minArea = Integer.MAX_VALUE;
        for (int i = 0; i < points.length; i++) {
            for (int j = i+1; j < points.length; j++) {
                int[] p1=points[i], p2=points[j];
                if (p1[0]!=p2[0] && p1[1]!=p2[1]
                        && set.contains(p1[0]+"#"+p2[1]) && set.contains(p2[0]+"#"+p1[1])) {
                    int area = Math.abs( (p1[0]-p2[0])*(p1[1]-p2[1]));
                    minArea = Math.min(minArea, area);

                }
            }
        }
        return minArea==Integer.MAX_VALUE?0:minArea;
    }

    public static void main(String[] args) {
        int[][] points = new int[][]{{0,1},{1,3},{3,3},{4,4},{1,4},{2,3},{1,0},{3,4}};
        System.out.println(new MinimumAreaRectangle().minAreaRect(points));
    }
}


/*
Given a set of points in the xy-plane, determine the minimum area of a rectangle formed from these points, with sides parallel to the x and y axes.
If there isn't any rectangle, return 0.

Example 1:
Input: [[1,1],[1,3],[3,1],[3,3],[2,2]]
Output: 4
Example 2:
Input: [[1,1],[1,3],[3,1],[3,3],[4,1],[4,3]]
Output: 2

Note:
1 <= points.length <= 500
0 <= points[i][0] <= 40000
0 <= points[i][1] <= 40000
All points are distinct.
 */
