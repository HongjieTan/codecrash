/*
Given two strings str1 and str2 of the same length, determine whether you can transform str1 into str2 by doing zero or more conversions.

In one conversion you can convert all occurrences of one character in str1 to any other lowercase English character.

Return true if and only if you can transform str1 into str2.



Example 1:

Input: str1 = "aabcc", str2 = "ccdee"
Output: true
Explanation: Convert 'c' to 'e' then 'b' to 'd' then 'a' to 'c'. Note that the order of conversions matter.
Example 2:

Input: str1 = "leetcode", str2 = "codeleet"
Output: false
Explanation: There is no way to transform str1 to str2.


Note:

1 <= str1.length == str2.length <= 10^4
Both str1 and str2 contain only lowercase English letters.
*/
package hashtable;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;


// good question! (hashtable + connected components)
// followup1: transform it in place
// followup2: minimum steps to transform

public class StringTransformsIntoAnotherString {
    public boolean canConvert(String str1, String str2) {
        if (str1.equals(str2)) return true;
        char[] a = str1.toCharArray(), b = str2.toCharArray();
        int[] f = new int[26];
        Arrays.fill(f, -1);
        for(int i=0;i<a.length;i++) {
            if(f[a[i]-'a'] == -1) {
                f[a[i]-'a'] = b[i]-'a';
            } else if (f[a[i]-'a'] !=  b[i]-'a'){
                return false;
            }
        }

        Set<Integer> set = new HashSet<>();
        for(int v:f) if (v!=-1) set.add(v);
        return set.size()<26;
    }

    // followup: minimum steps to transform
    public int minStepsToConvert(String str1, String str2) {
        if (!canConvert(str1, str2)) return -1;

        char[] a = str1.toCharArray(), b = str2.toCharArray();
        int[] f = new int[26];
        Arrays.fill(f, -1);
        for(int i=0;i<a.length;i++) {
            if(a[i]==b[i]) continue;
            f[a[i]-'a']=b[i]-'a';
        }

        // in each connected component, if it has cycle, then it need a 3-way swap, otherwise only 2-way swap is needed
        // eg.  abcuv -> cabxy
        //   abc->cab:   a->c, b->a, c->b,  cycle exist, need a 3-way swap, so total 4 steps: a->t, b->a, c->b, t->a
        //   uv -> xy:   u>v, x->y,  no cycle, total 2 steps
        int steps = 0;
        boolean[] visited = new boolean[26];
        for(int i=0;i<26;i++) {
            if(visited[i]) continue;
            if(f[i]==-1) continue;

            visited[i] = true;
            int cur = f[i];
            while(cur!=i && cur!=-1 && !visited[cur]) {
                ++steps;
                visited[cur] = true;
                cur = f[cur];
            }
            if(cur==i) steps+=2;
        }
        return steps;
    }

    public static void main(String[] as) {
        StringTransformsIntoAnotherString su = new StringTransformsIntoAnotherString();

//        String a="abcdefghijklmnopqrstuvwxyz";
//        String b="bcdefghijklmnopqrstuvwxyzq";
//        System.out.println(su.canConvert(a,b));

        System.out.println(su.minStepsToConvert("abc", "abc"));//0
        System.out.println(su.minStepsToConvert("abc", "aba"));//1
        System.out.println(su.minStepsToConvert("ab", "ba"));//3
        System.out.println(su.minStepsToConvert("ab", "xy"));//2
        System.out.println(su.minStepsToConvert("abcuv", "cabxy"));//6
        System.out.println(su.minStepsToConvert("leetcode", "codeleet"));//-1
        System.out.println(su.minStepsToConvert("abcdefghijklmnopqrstuvwxyz", "bcdefghijklmnopqrstuvwxyzq"));//25


    }
}


/*
G Interview: https://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=507962
string conversion: given two strings A, B of same length. Define an operation that replaces all occurances of a character from A to another character,
for example, replacing a in adac with d gives dddc.
First question: can you convert string A to string B after a finite number of operations.
Second question: find the minimal number of operations to convert A to B.

Also check  CouplesHoldingHands good!!!

Tan: 计算出所有联通组，每个联通组内需要size+1次operations...

- 第一题我有个想法但不确定有没有盲点，首先应该是确认有无一对多的状况，假设一个Ａ里面的a 已经对应到B的b了，Ａ就不能再对应到b以外的char，
  但A里面的其他char是能对应到b的，用个hashmap 确认就好，只能多对一，不能一对多．
  如果要算最小的replace次数，难点应该在 A = abcde, B = bcaed 这种情况，不能直接把一个char做replace，而要用到一个 3 way swap和一个 2 way swap．
    像这样：
    a->t, b->a, c->b, t->c
    abc => tbc => tac => tab => cab
    所以我觉得可以用 UF 或DFS 找有多少个这样的环，每个group的节点数量加一为该group需要的节点数量，最后加总，感觉和利口一道情侣手牵少的题思路有点像. check 1point3acres for more.

    补充内容 (2019-4-2 07:43):
    想到一个盲点：条件是要有剩余能替换的temporary char，如果只有26个字母又都在环里就办不到了

    补充内容 (2019-4-2 07:50):
    例子上下文沒對到。。。
    b改t, a改b, c改a, t改c
    abc-> atc-> btc-> bta-> bca

-  "没太懂这样的好处啊？每个char做替换的话是5次。按照你说的好像不只5次啊？"
    reply:
    A: abc
    B: bca
    如果把a替换成b
    A: bbc
    下一步要把b换成c的时候，第一个b也会被跟着换了，所以像这种有环状conflict的不能直接五个替换

    补充内容 (2019-4-2 15:15):
    为了避免conflict, 想到用swap的概念来做一个temporary variable这样

 - lz reply:
    你思路跟我差不多，我记得当时说的是找出有冲突的字符对，然后对他们进行3 way，对剩下的直接替换，最后没时间了，也不知道对不对

    补充内容 (2019-4-2 08:21):
    先判断能不能transform，如果可以的话造一个以字母为节点的有向图，a连b代表字符串A中的a需要对应到字符串B中的b，如果是个无环图，那就不需要3 way了，如果是个有环图，肯定是简答环，因为每个节点出度最多为1
*/


/*
by uwi:
class Solution {
    public boolean canConvert(String str1, String str2) {
        int[] f = new int[26];
        Arrays.fill(f, -1);
        int n = str1.length();
        for(int i = 0;i < n;i++){
            int s = str1.charAt(i)-'a';
            int g = str2.charAt(i)-'a';
            if(f[s] == -1){
                f[s] = g;
            }else if(f[s] != g){
                return false;
            }
        }

        int non = 0;
        int two = 0;

        outer:
        for(int i = 0;i < 26;i++){
            if(f[i] == -1){
                non++;
                continue;
            }
            int x = i;
            Set<Integer> hist = new HashSet<>();
            for(int j = 0;j < 26;j++){
                hist.add(x);
                x = f[x];
                if(x == -1)break;
                if(x == i){
                    if(j == 0){
                    }else{
                        for(int k = 0;k < 26;k++){
                            if(!hist.contains(k) && hist.contains(f[k])){
                                continue outer;
                            }
                        }
                        two++;
                    }
                    break;
                }
            }
        }

        return two > 0 && non > 0 || two == 0;
    }
}


by cuiaoxiang:
class Solution {
public:
    string normalize(string s) {
        vector<int> flag(26, -1);
        int n = s.size();
        int last = 0;
        string ret;
        for (int i = 0; i < n; ++i) {
            int k = s[i] - 'a';
            if (flag[k] < 0) {
                flag[k] = last;
                ret += (char)('a' + last);
                last++;
            } else {
                ret += (char)('a' + flag[k]);
            }
        }
        return ret;
    }
    int alphabets(string s) {
        vector<bool> cnt(26);
        for (auto& c : s) {
            cnt[c - 'a'] = true;
        }
        int ret = 0;
        for (int k = 0; k < 26; ++k) ret += cnt[k];
        return ret;
    }
    vector<int> a[26];
    bool canConvert(string str1, string str2) {
        if (str1 == str2) return true;
        if (alphabets(str1) == 26 && alphabets(str2) == 26) return false;
        int n = str1.size();
        for (int k = 0; k < 26; ++k) a[k].clear();
        for (int i = 0; i < n; ++i) {
            int k = str1[i] - 'a';
            a[k].push_back(i);
        }
        bool found = false;
        for (int k = 0; k < 26; ++k) {
            if (a[k].size() < 2) continue;
            int x = str2[a[k][0]] - 'a';
            for (int i = 1; i < a[k].size(); ++i) {
                int y = str2[a[k][i]] - 'a';
                if (x != y) found = true;
            }
        }
        return !found;
    }
};



by a concise version:
class Solution {
public:
    bool canConvert(string A, string B) {
        if (A == B) return true;
        map<char, char> m;
        set<char> s;
        for (int i = 0; i < A.size(); i++) {
            s.insert(B[i]);
            if (m.count(A[i])) {
                if (m[A[i]] != B[i])
                    return false;
            }
            else {
                m[A[i]] = B[i];
            }
        }
        return s.size() < 26;
    }
};
*/