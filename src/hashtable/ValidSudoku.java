package hashtable;

import java.util.HashSet;
import java.util.Set;

public class ValidSudoku {

    // conciser code
    public boolean isValidSudoku_v2(char[][] board) {
        Set<String> seen = new HashSet<>();
        for (int row = 0; row < board.length; row++) {
            for (int col = 0; col < board[0].length; col++) {
                char c = board[row][col];
                if (c!='.') {
                    String valueWithRow = c+"#row#"+row;
                    String valueWithCol = c+"#col#"+col;
                    String valueWithBlock = c+"#blockrow#"+ row/3 + "#blockcol#" + col/3;
                    if (seen.contains(valueWithRow)
                            || seen.contains(valueWithCol)
                            || seen.contains(valueWithBlock))
                        return false;
                    seen.add(valueWithRow);
                    seen.add(valueWithCol);
                    seen.add(valueWithBlock);
                }
            }
        }
        return true;
    }

    public boolean isValidSudoku(char[][] board) {
        Set<Character> rowSet = new HashSet<>();
        for (int i = 0; i < board.length; i++) {
            rowSet.clear();
            for (int j = 0; j < board[0].length; j++) {
                char c = board[i][j];
                if (c == '.') continue;
                if (rowSet.contains(c)) return false;
                rowSet.add(c);
            }
        }

        Set<Character> colSet = new HashSet<>();
        for (int col = 0; col < board[0].length; col++) {
            colSet.clear();
            for (int row = 0; row < board.length; row++) {
                char c = board[row][col];
                if (c=='.') continue;
                if (colSet.contains(c)) return false;
                colSet.add(c);
            }
        }

        Set<Character> subSet = new HashSet<>();
        for (int i = 0; i < 9; i++) {
            int rowstart = (i/3)*3, rowend = rowstart+2;
            int colstart = (i%3)*3, colend = colstart+2;
            subSet.clear();
            for (int row = rowstart; row <= rowend; row++) {
                for (int col = colstart; col <= colend ; col++) {
                    char c = board[row][col];
                    if (c=='.') continue;
                    if (subSet.contains(c)) return false;
                    subSet.add(c);
                }
            }
        }
        return true;
    }

    public static void main(String[] args) {
        char[][] board = new char[][]{
                {'5','3','.','.','7','.','.','.','.'},
                {'6','.','.','1','9','5','.','.','.'},
                {'.','9','8','.','.','.','.','6','.'},
                {'8','.','.','.','6','.','.','.','3'},
                {'4','.','.','8','.','3','.','.','1'},
                {'7','.','.','.','2','.','.','.','6'},
                {'.','6','.','.','.','.','2','8','.'},
                {'.','.','.','4','1','9','.','.','5'},
                {'.','.','.','.','8','.','.','7','9'}};

//        System.out.println(new hashtable.ValidSudoku().isValidSudoku(board));
        System.out.println(new ValidSudoku().isValidSudoku_v2(board));
    }



}
