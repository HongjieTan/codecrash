// Given an array nums of positive integers, return the longest possible length of an array prefix of nums,
// such that it is possible to remove exactly one element from this prefix so that every number that has appeared in it
// will have the same number of occurrences.
//
// If after removing one element there are no remaining elements,
// it's still considered that every appeared number has the same number of ocurrences (0).
//
//
// Example 1:
//
//
//Input: nums = [2,2,1,1,5,3,3,5]
//Output: 7
//Explanation: For the subarray [2,2,1,1,5,3,3] of length 7, if we remove nums[4]=5, we will get [2,2,1,1,3,3], so that each number will appear exactly twice.
//
//
// Example 2:
//
//
//Input: nums = [1,1,1,2,2,2,3,3,3,4,4,4,5]
//Output: 13
//
//
// Example 3:
//
//
//Input: nums = [1,1,1,2,2,2]
//Output: 5
//
//
// Example 4:
//
//
//Input: nums = [10,2,8,9,3,8,1,5,2,3,7,6]
//Output: 8
//
//
//
// Constraints:
//
//
// 2 <= nums.length <= 10^5
// 1 <= nums[i] <= 10^5
//

package hashtable;

public class MaxEqualFrequency {

    public int maxEqualFreq(int[] nums) {
        int[] f = new int[100001];
        int[] ff = new int[100001];
        int maxf=0, standff=0, ret=0;
        for(int i=0;i<nums.length;i++) {
            int v = nums[i];
            ff[f[v]]--;
            if(ff[f[v]]==0) standff--;
            f[v]++;
            ff[f[v]]++;
            if(ff[f[v]]==1) standff++;
            maxf = Math.max(maxf, f[v]);

            if(ok(f,ff,maxf,standff,i+1)) {
                ret=i+1;
            }
        }
        return ret;
    }

    boolean ok(int[] f, int[] ff, int maxf, int standff, int n) {
        if(standff==1) {
            if(maxf==1) return true;
            if(ff[maxf]==1) return true;
        }
        if(standff==2) {
            if(ff[maxf]==1 && ff[maxf-1]>0) return true;
            if(ff[maxf]*maxf == n-1) return true;
        }
        return false;
    }

    // first version
//    public int maxEqualFreq(int[] nums) {
//        int ret = 1, n=nums.length;
//        Map<Integer, Integer> n2c = new HashMap<>(); // num   -> count
//        Map<Integer, Integer> c2n = new HashMap<>(); // count -> nums
//        for(int i=0;i<n;i++) {
//            int c = n2c.getOrDefault(nums[i], 0);
//            n2c.put(nums[i], c+1);
//            c2n.put(c+1, c2n.getOrDefault(c+1,0)+1);
//            if(c2n.containsKey(c)) {
//                if(c2n.get(c)==1) c2n.remove(c);
//                else c2n.put(c, c2n.get(c)-1);
//            }
//
//            // case1,2:
//            if(c2n.size()==2) {
//                int maxcount = Integer.MIN_VALUE, mincount = Integer.MAX_VALUE;
//                for (int cnt:c2n.keySet()) {
//                    maxcount = Math.max(maxcount, cnt);
//                    mincount = Math.min(mincount, cnt);
//                }
//
//                if (mincount==1 && c2n.get(mincount)==1) ret=i+1;
//                if (maxcount-mincount==1 && c2n.get(maxcount)==1) ret=i+1;
//            }
//
//            // case3,4:
//            if (c2n.size()==1) {
//                if (c2n.containsKey(1))  ret = i+1;
//                for (int num:c2n.values()) if (num==1) ret = i+1;
//            }
//        }
//        return ret;
//    }

    public static void main(String[] as) {
        System.out.println(new MaxEqualFrequency().maxEqualFreq(new int[]{1,1})); //2
        System.out.println(new MaxEqualFrequency().maxEqualFreq(new int[]{2,2,1,1,5,3,3,5})); //7
        System.out.println(new MaxEqualFrequency().maxEqualFreq(new int[]{1,1,1,2,2,2,3,3,3,4,4,4,5})); //13
        System.out.println(new MaxEqualFrequency().maxEqualFreq(new int[]{1,1,1,2,2,2})); //5
        System.out.println(new MaxEqualFrequency().maxEqualFreq(new int[]{10,2,8,9,3,8,1,5,2,3,7,6})); //8
    }
}

/*
BY UWI:
class Solution {
 public int maxEqualFreq(int[] nums) {
     int[] f = new int[100005];
     int[] ff = new int[100005];
     ff[0] = 100000;
     int ans = 0;

     int max = 0;
     int stand = 0;
     for(int i = 0;i < nums.length;i++){
      int v = nums[i];
      ff[f[v]]--;
      f[v]++;
      if(f[v] == 1)stand++;
      max = Math.max(max, f[v]);
      ff[f[v]]++;

      if(ok(f, ff, max, stand)){
       ans = i+1;
      }
     }
     return ans;
 }

 boolean ok(int[] f, int[] ff, int max, int stand)
 {
  if(max == 0)return true;
  if(ff[max] == 1){
   if(stand == 1 || max == 1)return true;
   if(stand == 2 && ff[1] == 1)return true;
   if(ff[max-1] == stand-1)return true;
   return false;
  }
  if(max == 1)return true;
  if(ff[max] == stand-1 && ff[1] == 1)return true;
  return false;
 }
}

By lee:
Intuition
We need to count the frequency of numbers in A
Also we need to know, for each frequency, we have how many different numbers.
count[a] means the frequency of number a
freq[c] means how many numbers that occur c times.


Explanation
Iterate the input array A and we count the n first numbers.

There actually only 2 situation to discuss:

we delete the current number a.
In this case, the n - 1 first numbers have the same frequency,
and we can easier detect this case when we iterate the previous number A[n - 1]

we don't delete the current number a
the current a occurs c times.
So except all numbers that also occurs c times,
it should leave one single number, or c + 1 same numeber.

That's it, done.


Complexity
Time O(N)
Space O(K), where K = set(A).length


Java

    public int maxEqualFreq(int[] A) {
        int[] count = new int[100001], freq = new int[100001];
        int res = 0, N = A.length, a,c,d;
        for (int n = 1; n <= N; ++n) {
            a = A[n - 1];
            --freq[count[a]];
            c = ++count[a];
            ++freq[count[a]];

            if (freq[c] * c == n && n < N)
                res = n + 1;
            d = n - freq[c] * c;
            if ((d == c + 1 || d == 1) && freq[d] == 1)
                res = n;
        }
        return res;
    }
C++

    int maxEqualFreq(vector<int>& A) {
        vector<int> count(100001), freq(100001);
        int res = 0, N = A.size(), a,c,d;
        for (int n = 1; n <= N; ++n) {
            a = A[n - 1];
            --freq[count[a]];
            c = ++count[a];
            ++freq[count[a]];

            if (freq[c] * c == n && n < N)
                res = n + 1;
            d = n - freq[c] * c;
            if ((d == c + 1 || d == 1) && freq[d] == 1)
                res = n;
        }
        return res;
    }
Python:

    def maxEqualFreq(self, A):
        count = collections.Counter()
        freq = [0 for _ in xrange(len(A) + 1)]
        res = 0
        for n, a in enumerate(A, 1):
            freq[count[a]] -= 1
            freq[count[a] + 1] += 1
            c = count[a] = count[a] + 1
            if freq[c] * c == n and n < len(A):
                res = n + 1
            d = n - freq[c] * c
            if d in [c + 1, 1] and freq[d] == 1:
                res = n
        return res

Missing Test Case
One more test case: [1, 1, 2, 2, 3, 4, 3]


by top:
cnt records the occurence of each num, freq records the frequence of number of occurences.
max_F is the largest frequence.
There are three cases which satify the condition:

all elements appear exact once.
all elements appear max_F times, except one appears once.
all elements appear max_F-1 times, except one appears max_F.
class Solution:
    def maxEqualFreq(self, nums: List[int]) -> int:
        cnt,freq,maxF,res = collections.defaultdict(int), collections.defaultdict(int),0,0
        for i,num in enumerate(nums):
            cnt[num] += 1
            freq[cnt[num]-1] -= 1
            freq[cnt[num]] += 1
            maxF = max(maxF,cnt[num])
            if maxF*freq[maxF] == i or (maxF-1)*(freq[maxF-1]+1) == i or maxF == 1:
                res = i + 1
        return res

*/