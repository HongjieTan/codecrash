package hashtable;

import common.ListNode;


/**
 * out of scope: check consistent hashing in distributed storage system ...
 */
public class Rehashing {
    /**
     * @param hashTable: A list of The first node of linked list
     * @return: A list of The first node of linked list which have twice size
     */

    public ListNode[] rehashing(ListNode[] hashTable) {
        // write your code here
        ListNode[] newhashtable = new ListNode[hashTable.length * 2];

        for(ListNode node: hashTable) {
            while (node!=null) {
                int hashcode = hashcode(node.val, newhashtable.length);
                if (newhashtable[hashcode]==null) {
                    newhashtable[hashcode] = node;
                } else {
                    ListNode temp = newhashtable[hashcode];
                    while (temp.next!=null) temp = temp.next;
                    temp.next = node;
                }
                ListNode nextNode = node.next;
                node.next = null;
                node = nextNode;
            }
        }
        return newhashtable;
    }

    int hashcode(int key, int capacity) {
        return (key % capacity + capacity) % capacity;
    }
}

