package hashtable;

import java.util.*;

/**
 *
 * http://massivealgorithms.blogspot.com/2017/04/leetcode-527-word-abbreviation.html
 *
 *  hashmap(counter):   O(n*L)  concise!!!  L:average word length...
 *  hashmap + string._DataStructure_Trie_PrefixTree:     O(n*L)  code is complex, no need here...
 */
public class WordAbbreviation {

    // good！
    // 直接搜，全部取缩一位，用map<string, int> 看有无重复, 相同缩写value++，再只检查计数大于1的，是重复的，改成缩两位，直到没有重复。
    public String[] wordsAbbreviation(String[] dict) {
        int[] prefix = new int[dict.length];
        String[] res = new String[dict.length];
        Map<String, Integer> countMap = new HashMap<>();
        for (int i = 0; i < dict.length; i++) {
            prefix[i] = 1;
            res[i] = getAbbrev(dict[i], prefix[i]);
            countMap.put(res[i], countMap.getOrDefault(res[i], 0)+1);
        }
        while (true) {  // note: this loop is at most MaxWordLen times!!
            boolean hasDup = false;
            for (int i = 0; i < dict.length; i++) {
                if (countMap.get(res[i])>1) {
                    ++prefix[i];
                    res[i] = getAbbrev(dict[i], prefix[i]);
                    countMap.put(res[i], countMap.getOrDefault(res[i],0)+1);
                    hasDup = true;
                }
            }
            if (!hasDup) break;
        }
        return res;
    }

    // another solution in a straight forward way...this seems O(n^2)
//    public String[] wordsAbbreviation_v0(String[] dict) {
//        int[] prefix = new int[dict.length];
//        String[] abbrev = new String[dict.length];
//        for (int i = 0; i < dict.length; i++) {
//            prefix[i] = 1;
//            abbrev[i] = getAbbrev(dict[i], prefix[i]);
//        }
//        for (int i = 0; i < dict.length; i++) {
//            while (true) {
//                List<Integer> conflicts = new ArrayList<>();
//                conflicts.add(i);
//                for (int j = i+1; j < dict.length; j++) {
//                    if (abbrev[j].equals(abbrev[i])) conflicts.add(j);
//                }
//                if (conflicts.size()==1)  break;
//                for(int idx: conflicts) {
//                    prefix[idx]++;
//                    abbrev[idx]=getAbbrev(dict[idx], prefix[idx]);
//                }
//            }
//        }
//        return abbrev;
//    }

    private String getAbbrev(String word, int prefixLen) {
        if (word.length()-prefixLen <=2 ) return word;
        StringBuffer sb = new StringBuffer();
        sb.append(word, 0, prefixLen);
        sb.append(word.length()-prefixLen-1);
        sb.append(word.charAt(word.length()-1));
        return sb.toString();
    }

    /*
      1. group all words with same shortest abbreviation(group key), For example:
         "internal", "interval" => grouped by "i6l"
         "intension", "intrusion" => grouped by "i7n"
         "god" => grouped by "god"
         we can notice that only words with the same length and the same start and end letter could be grouped together

      2. resolve conflicts in each group
     */
    public String[] wordsAbbreviation_v2(String[] dict) {
        String[] res = new String[dict.length];
        // step1 group words
        Map<String, List<Integer>> groupMap = new HashMap<>();
        for (int i = 0; i < dict.length; i++) {
            String key = getGroupKey(dict[i]);
            if (!groupMap.containsKey(key)) groupMap.put(key, new ArrayList<>());
            groupMap.get(key).add(i);
        }
        // step2 resolve conflicts in each group using Trie!
        for (String key: groupMap.keySet()) {
            resolve_by_trie(dict, key, groupMap.get(key), res);
        }
        return res;
    }

    class Trie {
        int count=0;
        String word="";
        Trie[] children = new Trie[26];
    }
    private void resolve_by_trie(String[] dict, String groupKey, List<Integer> indexList, String[] res) {
        if (indexList.size()==1) {
            res[indexList.get(0)] = groupKey;
            return;
        }

        Trie root = new Trie();
        // build string._DataStructure_Trie_PrefixTree
        for (int idx: indexList) {
            String word = dict[idx];
            Trie node = root;
            for (char c: word.toCharArray()) {
                if (node.children[c-'a']==null) node.children[c-'a'] = new Trie();
                node = node.children[c-'a'];
                node.count++;
            }
            node.word = word;
        }

        // make addbrev
        for (int idx: indexList) {
            StringBuffer sb = new StringBuffer();
            String word = dict[idx];
            Trie node = root;
            for (int i = 0; i < word.length(); i++) {
                char c = word.charAt(i);
                node = node.children[c-'a'];
                sb.append(c);
                if (node.count==1) { // found diff point
                    int abbrevCount = word.length()-(i+1)-1;
                    if (abbrevCount>1) sb.append(abbrevCount);
                    else if (abbrevCount==1) sb.append(word.charAt(i+1));
                    sb.append(word.charAt(word.length()-1));
                    break;
                }
            }
            res[idx] = sb.toString();
        }
    }

    private String getGroupKey(String word) {
        if (word.length()<=3) return word;
        StringBuffer sb = new StringBuffer();
        sb.append(word.charAt(0)).append(word.length()-2).append(word.charAt(word.length()-1));
        return sb.toString();
    }

    public static void main(String[] args) {
        String[] words = new String[]{"like", "god", "internal", "me", "internet", "interval", "intension", "face", "intrusion"};
//        Output: ["l2e","god","internal","me","i6t","interval","inte4n","f2e","intr4n"]
//        String[] words = new String[]{"abcdef", "axyzef", "axhhef"};
        String[] abbrevs = new WordAbbreviation().wordsAbbreviation(words);
        for (String abbrev: abbrevs) System.out.println(abbrev);
    }
}
