//Reverse a linked list from position m to n. Do it in one-pass.
//
// Note: 1 ≤ m ≤ n ≤ length of list.
//
// Example:
//
//
//Input: 1->2->3->4->5->NULL, m = 2, n = 4
//Output: 1->4->3->2->5->NULL
//
package linkedlist_queue;

import common.ListNode;

/**
 * Created by thj on 2018/7/30.
 */
public class ReverseLinkedList2 {

    // clean code!
    public static ListNode reverseBetween(ListNode head, int m, int n) {
        ListNode dummy = new ListNode(-1);
        dummy.next = head;

        ListNode cur, prev, part1Tail, part2Head, part2Tail, part3Head;

        // part1
        cur = dummy;
        for (int i = 1; i <= m-1; i++) {
            cur = cur.next;
        }
        part1Tail = cur;

        // part2 -> reverse it
        prev = part1Tail;
        cur = prev.next;
        for (int i = m; i <= n; i++) {
            if (i>m) {
                ListNode tempNext = cur.next;
                cur.next = prev;
                prev = cur;
                cur = tempNext;
            } else {
                prev = cur;
                cur = cur.next;
            }
        }
        part2Head = prev;
        part2Tail = part1Tail.next;
        part3Head = cur;

        // connect
        part1Tail.next = part2Head;
        part2Tail.next = part3Head;

        return dummy.next;
    }

//    public static ListNode reverseBetween(ListNode head, int m, int n) {
//        if (head == null) return null;
//
//        int index = 1;
//        ListNode prev = null, curr = head;
//
//        ListNode before=null, after=null, reversedTail=null;
//
//        while (curr!=null && index<=n) {
//            if (index == m-1) before = curr;
//            if (index == n) after = curr.next;
//            if (index == m) reversedTail = curr;
//            if (index>=m+1 && index<=n) {
//                ListNode tempNext = curr.next;
//                curr.next = prev;
//                prev = curr;
//                curr = tempNext;
//            } else {
//                prev = curr;
//                curr = curr.next;
//            }
//            index++;
//        }
//
//        if (before!=null) before.next=prev;
//        if (reversedTail!=null) reversedTail.next=after;
//
//
//        return m>1?head:prev;
//    }

    public static void main(String[] args) {
        ListNode n1 = new ListNode(1);
        ListNode n2 = new ListNode(2);
        ListNode n3 = new ListNode(3);
        ListNode n4 = new ListNode(4);
        ListNode n5 = new ListNode(5);
        n1.next=n2;
        n2.next=n3;
        n3.next=n4;
        n4.next=n5;

        reverseBetween(n1, 2, 4);

        System.out.println();

    }


}
