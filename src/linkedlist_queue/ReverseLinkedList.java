package linkedlist_queue;

import common.ListNode;

/**
 * Created by thj on 2018/7/30.
 */
public class ReverseLinkedList {

    public static ListNode reverse(ListNode head) {
        if(head==null) return null;

        ListNode prev=null, curr=head;
        while (curr!=null) {
            ListNode nextTemp = curr.next;
            curr.next = prev;
            prev=curr;
            curr=nextTemp;
        }
        return prev;
    }


    public static void main(String[] args) {
        ListNode node1 = new ListNode(1);
        ListNode node2 = new ListNode(2);
        ListNode node3 = new ListNode(3);
        node1.next = node2;
        node2.next = node3;

        reverse(node1);

        System.out.println("sadf");
    }
}
