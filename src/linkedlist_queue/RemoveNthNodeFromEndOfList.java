//Given a linked list, remove the n-th node from the end of list and return its head.
//
// Example:
//
//
//Given linked list: 1->2->3->4->5, and n = 2.
//
//After removing the second node from the end, the linked list becomes 1->2->3->5.
//
//
// Note:
//
// Given n will always be valid.
//
// Follow up:
//
// Could you do this in one pass?
//
package linkedlist_queue;
import common.ListNode;

/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */
public class RemoveNthNodeFromEndOfList {
    // or use dummy for cleaner code...
    public ListNode removeNthFromEnd(ListNode head, int n) {
        ListNode dummy = new ListNode(-1);
        dummy.next = head;
        ListNode p = dummy, q = dummy;
        while(n>1) {
            p = p.next;
            n--;
        }
        while(p.next.next!=null) {
            p=p.next;
            q=q.next;
        }
        q.next = q.next.next;
        return dummy.next;
    }

    public ListNode removeNthFromEnd_v1(ListNode head, int n) {
        ListNode p=null,q=head,r=head;
        while(n>0) {
            r = r.next;
            n--;
        }
        while(r.next!=null) {
            r=r.next;
            p=q;
            q=q.next;
        }
        if(p==null) return q.next;
        p.next = q.next;
        return head;
    }
}
