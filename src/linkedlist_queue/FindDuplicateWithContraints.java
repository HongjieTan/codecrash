package linkedlist_queue;

/**
 * Created by thj on 28/05/2018.
 */
public class FindDuplicateWithContraints {
    /**
     *
     * constaints:
     *   1. time: O(n)
     *   2. space: O(1)
     *   3. not change origin array
     *
     *
     * given 1,2,3 => use cycle detection
     * given 1,2 => we can use inplace hash
     *
     * other: sorting, hash, ...
     *
     * @param nums
     * @return
     */

    // O(n) / O(1),  not change origin array
    public int findDuplicate(int[] nums) {
        int faster=0, slower=0, entry=0;
        while(true) {
            slower = nums[slower];
            faster = nums[nums[faster]];
            if (slower == faster) { // cycle exist
                while(entry != slower){
                    entry = nums[entry];
                    slower = nums[slower];
                }
                return entry;
            }
        }
    }

}
