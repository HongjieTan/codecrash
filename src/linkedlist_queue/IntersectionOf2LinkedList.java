package linkedlist_queue;

import common.ListNode;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by thj on 2018/7/24.
 */
public class IntersectionOf2LinkedList {


    // O(n)/O(1) use two pointers...


    // O(n)/O(n)
    public ListNode getIntersectionNode(ListNode headA, ListNode headB) {

        Set<ListNode> set = new HashSet<>();

        while(headA!=null) {
            set.add(headA);
            headA = headA.next;
        }

        while (headB!=null) {
            if (set.contains(headB)) return headB;
            headB = headB.next;
        }

        return null;
    }
}
