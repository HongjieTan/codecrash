package linkedlist_queue;

import common.ListNode;

/**
 * Created by thj on 2018/8/21.
 */
public class InsertCyclicSortedList {


    // code is clean!!!!! good!!
    public ListNode insert(ListNode node, int x) {
        ListNode xNode = new ListNode(x); xNode.next=xNode;

        ListNode start = node;
        while (node!=null) {

            // target point!!! 3 cases...
            if (node.val <= x && x < node.next.val) break;
            if (node.val > node.next.val && (x>=node.val || x<=node.next.val)) break;
            if (node.next == start) break;

            node = node.next;
        }


        if (node!=null){
            xNode.next = node.next;
            node.next = xNode;
        }

        return xNode;
    }


    // code not pretty...
    public ListNode insert_v2(ListNode node, int x) {
        ListNode newNode = new ListNode(x);

        if (node==null) {
            newNode.next = newNode;
            return newNode;
        }


        ListNode head, tail, prev;

        ListNode start = node;
        while (node.val <= node.next.val && node.next!=start) node = node.next;
        tail = node;
        head = tail.next;


        if (x<head.val || x>tail.val) {
            newNode.next = head;
            tail.next = newNode;
        } else {
            prev = head;
            node = head.next;
            while (node.val < x) {
                prev = node;
                node = node.next;
            }
            prev.next = newNode;
            newNode.next = node;
        }

        return newNode;
    }


    public static void main(String[] args) {
        //.4->4->8->10->13->15
        ListNode n1 = new ListNode(4);
        ListNode n2 = new ListNode(4);
        ListNode n3 = new ListNode(8);
        ListNode n4 = new ListNode(10);
        ListNode n5 = new ListNode(13);
        ListNode n6 = new ListNode(15);
        n1.next=n2;
        n2.next=n3;
        n3.next=n4;
        n4.next=n5;
        n5.next=n6;
        n6.next=n1;

        ListNode res = new InsertCyclicSortedList().insert(n1, 13);

        System.out.println();


    }
}
