package linkedlist_queue;

import common.ListNode;

import java.util.PriorityQueue;

/**
 * Created by thj on 2018/8/1.
 */
public class MergeKSortedLists {

    public ListNode mergeKLists(ListNode[] lists) {
        if (lists==null || lists.length==0) return null;

        PriorityQueue<ListNode> heap = new PriorityQueue<>((a,b)-> (Integer.compare(a.val, b.val)));


        for (int i = 0; i < lists.length; i++) {
            if (lists[i]!=null) {
                heap.add(lists[i]);
            }
        }


        ListNode dummy = new ListNode(0);
        ListNode prev = dummy;
        while (!heap.isEmpty()) {
            ListNode min = heap.poll();

            prev.next = min;

            if (min.next!=null) {
                heap.add(min.next);
            }

            prev = prev.next;
        }


        return dummy.next;


    }
}
