package linkedlist_queue;

/**
 * Created by thj on 2018/8/1.
 */
public class MergeTwoSortedLists {

    public static class Node{
        public int data;
        public Node next;
        public Node(int data) {this.data = data;}
    }

    public Node mergeTwoLists(Node head1, Node head2) {
        if (head1==null) return head2;
        if (head2==null) return head1;

        Node dummy = new Node(0);
        Node prev = dummy;
        while (head1!=null && head2!=null) {
            if (head1.data < head2.data) {
                prev.next = head1;
                head1=head1.next;
            } else {
                prev.next = head2;
                head2=head2.next;
            }

            prev=prev.next;
        }

        prev.next = head1!=null?head1:head2;

        return dummy.next;
    }

    public static void main(String[] args) {
        // one case: -1->3/5->7 => -1->3->5->7
        Node l1= new Node(-1); l1.next = new Node(3);
        Node l2= new Node(5); l2.next = new Node(7);
        Node head= new MergeTwoSortedLists().mergeTwoLists(l1, l2);
        while (head!=null) {
            System.out.println(head.data);
            head=head.next;
        }
    }
}
