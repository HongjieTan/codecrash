package linkedlist_queue;

import common.ListNode;

/**
 * Created by thj on 28/05/2018.
 */
public class LinkedListCycle2 {
    public ListNode detectCycle(ListNode head) {
        if (head == null) return null;

        ListNode faster = head, slower = head, entry = head;

        while(true) {

            if (faster.next == null || faster.next.next == null) return null;

            faster = faster.next.next;
            slower = slower.next;

            if (faster == slower) { // cycle exist
                // move to cycle entry:
                // to prove: https://leetcode.com/problems/linked-list-cycle-ii/discuss/44781/Concise-O(n)-solution-by-using-C++-with-Detailed-Alogrithm-Description
                while (entry != slower) {
                    entry = entry.next;
                    slower = slower.next;
                }
                return entry;
            }
        }
    }
}
