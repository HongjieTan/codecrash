//Return the lexicographically smallest subsequence of text that contains all the distinct characters of text exactly once.
//
//
//
// Example 1:
//
//
//Input: "cdadabcc"
//Output: "adbc"
//
//
//
// Example 2:
//
//
//Input: "abcd"
//Output: "abcd"
//
//
//
// Example 3:
//
//
//Input: "ecbacba"
//Output: "eacb"
//
//
//
// Example 4:
//
//
//Input: "leetcode"
//Output: "letcod"
//
//
//
//
// Note:
//
//
// 1 <= text.length <= 1000
// text consists of lowercase English letters.
//
//
//
//
//
//
package stack._Topic_NextGreater;

import java.util.*;


public class SmallestSubsequenceOfDistinctCharacters {
    public String smallestSubsequence(String text) {
        char[] s = text.toCharArray();
        Map<Character, Integer> last = new HashMap<>();
        for(int i=0;i<s.length;i++) last.put(s[i], i);

        Set<Character> seen = new HashSet<>();
        Stack<Character> st = new Stack<>();
        for(int i=0;i<s.length;i++) {
            if (seen.contains(s[i])) continue;
            while(!st.isEmpty() && s[i] <= st.peek() && last.get(st.peek())>=i) {
                seen.remove(st.peek());
                st.pop();
            }
            st.push(s[i]);
            seen.add(s[i]);
        }

        StringBuilder sb = new StringBuilder();
        for(char x:st) sb.append(x);
        return sb.toString();
    }

    public static void main(String[] s) {
//Input: "cdadabcc"
//Output: "adbc"
//Input: "abcd"
//Output: "abcd"
//Input: "ecbacba"
//Output: "eacb"
//Input: "leetcode"
//Output: "letcod"
        System.out.println(new SmallestSubsequenceOfDistinctCharacters().smallestSubsequence("cdadabcc"));
        System.out.println(new SmallestSubsequenceOfDistinctCharacters().smallestSubsequence("abcd"));
        System.out.println(new SmallestSubsequenceOfDistinctCharacters().smallestSubsequence("ecbacba"));
        System.out.println(new SmallestSubsequenceOfDistinctCharacters().smallestSubsequence("leetcode"));
        System.out.println(new SmallestSubsequenceOfDistinctCharacters().smallestSubsequence("cbaacabcaaccaacababa"));
    }


}




/*
by uwi:
class Solution {
    public String smallestSubsequence(String text) {
        char[] s = text.toCharArray();
        int ptn = 0;
        for(char c : s)ptn |= 1<<c-'a';
        int n = s.length;

        char[] ret = new char[26];
        int cur = 0;
        int pos = -1;

        int[] cum = new int[n+1];
        for(int i = n-1;i >= 0;i--){
            cum[i] = cum[i+1] | 1<<s[i]-'a';
        }

        outer:
        for(int p = 0;p < 26;p++){
            for(int i = 0;i < 26;i++){
                if(cur<<~i>=0){
                    for(int j = pos+1;j < n;j++){
                        if(s[j]-'a' == i){
                            int nex = cur|cum[j];
                            if(nex == ptn){
                                ret[p] = (char)('a'+i);
                                pos = j;
                                cur |= 1<<i;
                                continue outer;
                            }
                        }
                    }
                }
            }
            ret = Arrays.copyOf(ret, p);
            break;
        }
        return new String(ret);
    }
}


by lee:

Explanation:
Find the index of last occurrence for each character.
Use a stack to keep the characters for result.
Loop on each character in the input string S,
if the current character is smaller than the last character in the stack,
and the last character exists in the following stream,
we can pop the last character to get a smaller result.


Time Complexity:
Time O(N)
Space O(26)

Java:

    public String smallestSubsequence(String S) {
        Stack<Integer> stack = new Stack<>();
        int[] last = new int[26];
        boolean[] seen = new boolean[26];
        for (int i = 0; i < S.length(); ++i)
            last[S.charAt(i) - 'a'] = i;
        for (int i = 0; i < S.length(); ++i) {
            int c = S.charAt(i) - 'a';
            if (seen[c]) continue;
            while (!stack.isEmpty() && stack.peek() > c && i < last[stack.peek()]) {
                seen[stack.pop()] = false;
            }
            stack.push(c);
            seen[c] = true;
        }
        StringBuilder sb = new StringBuilder();
        for (int i : stack) sb.append((char)('a' + i));
        return sb.toString();
    }

Python:

    def smallestSubsequence(self, S):
        last = {c: i for i, c in enumerate(S)}
        stack = []
        for i, c in enumerate(S):
            if c in stack: continue
            while stack and stack[-1] > c and i < last[stack[-1]]:
                stack.pop()
            stack.append(c)
        return "".join(stack)


by top post:
The solution is exactly the same as for 316. Remove Duplicate Letters.

From the input string, we are trying to greedily build a monotonically increasing result string.
If the next character is smaller than the back of the result string, we remove larger characters from the back providing there are more occurrences of that character later in the input string.

string smallestSubsequence(string s, string res = "") {
  int cnt[26] = {}, used[26] = {};
  for (auto ch : s) ++cnt[ch - 'a'];
  for (auto ch : s) {
    --cnt[ch - 'a'];
    if (used[ch - 'a']++ > 0) continue;
    while (!res.empty() && res.back() > ch && cnt[res.back() - 'a'] > 0) {
      used[res.back() - 'a'] = 0;
      res.pop_back();
    }
    res.push_back(ch);
  }
  return res;
}
Complexity Analysis
Runtime: O(n). We process each input character no more than 2 times.
Memory: O(1). We need the constant memory to track up to 26 unique letters.
*/