//Given a string which contains only lowercase letters, remove duplicate letters so that every letter appear once and only once.
// You must make sure your result is the smallest in lexicographical order among all possible results.
//
// Example 1:
//
//
//Input: "bcabc"
//Output: "abc"
//
//
// Example 2:
//
//
//Input: "cbacdcbc"
//Output: "acdb"
//

package stack._Topic_NextGreater;

import java.util.Stack;

public class RemoveDuplicateLetters {

    // stack
    public String removeDuplicateLetters(String s) {
        char[] ch = s.toCharArray();
        int last[] = new int[26];
        for(int i=0;i<ch.length;i++) last[ch[i]-'a']=i;
        boolean seen[] = new boolean[26];

        Stack<Character> st = new Stack<>();
        for(int i=0;i<ch.length;i++) {
            if (seen[ch[i]-'a']) continue;
            while(!st.isEmpty() && ch[i]<st.peek() && i<last[st.peek()-'a']) {
                seen[st.pop()-'a'] = false;
            }
            st.push(ch[i]);
            seen[ch[i]-'a'] = true;
        }

        StringBuilder sb = new StringBuilder();
        for(char x:st) sb.append(x);
        return sb.toString();
    }
}
