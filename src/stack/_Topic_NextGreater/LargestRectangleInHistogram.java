package stack._Topic_NextGreater;

import java.util.Arrays;
import java.util.Stack;

/**
 * Created by thj on 31/05/2018.
 *
 * 3. dp+stack: O(n)   good!!!
 * 1. brute force: O(n^2)
 * 2. divide and conquer: O(nlogn)    ps: to avoid worst case like quicksort? Segment Tree?
 *
 */
public class LargestRectangleInHistogram {

    // 2-side DP + stack : O(n)
    //  use stack to calculate first smaller element from left/right, see NextGreaterElement!!!
    public int largestRectangleArea_X2(int[] heights) {
        int n=heights.length;

        int[] left = new int[n];
        int[] right = new int[n];
        Arrays.fill(left, -1);
        Arrays.fill(right, n);

        Stack<Integer> stack = new Stack<>();
        for(int i=0;i<n;i++) {
            while(!stack.isEmpty() && heights[stack.peek()]>heights[i]) {
                right[stack.pop()] = i;
            }
            stack.push(i);
        }

        stack.clear();
        for(int i=n-1;i>=0;i--) {
            while(!stack.isEmpty() && heights[stack.peek()]>heights[i]) {
                left[stack.pop()] = i;
            }
            stack.push(i);
        }

        int ans = 0;
        for(int i=0;i<n;i++) {
            int area = heights[i]*(right[i]-left[i]-1);
            ans = Math.max(area, ans);
        }
        return ans;
    }

    // divide and conquer
    public int largestRectangleArea(int[] heights) {
        return helper(heights, 0, heights.length-1);
    }

    private int helper(int[] heights, int l, int r) {
        if (l>r) return 0;
        int min = heights[l], minIndex = l;
        for (int i=l;i<=r;i++) {
            if (heights[i]<min) {
                min = heights[i];
                minIndex = i;
            }
        }
        int leftMax = helper(heights, l, minIndex-1);
        int rightMax = helper(heights, minIndex+1, r);
        int allMax = min * (r-l+1);
        return Math.max(allMax,Math.max(leftMax, rightMax));
    }


    // stack version
    // hard: explanation https://www.geeksforgeeks.org/largest-rectangle-under-histogram/
    public int largestRectangleArea_v2(int[] heights) {
        // Create an empty stack. The stack holds indexes of hist[] array
        // The bars stored in stack are always in increasing order of their
        // heights.
        Stack<Integer> s = new Stack<>();

        int max_area = 0; // Initialize max area
        int tp;  // To store top of stack
        int area_with_top; // To store area with top bar as the smallest bar

        // Run through all bars of given histogram
        int i = 0;
        while (i < heights.length)
        {
            // If this bar is higher than the bar on top stack, push it to stack
            if (s.empty() || heights[s.peek()] <= heights[i])
                s.push(i++);

                // If this bar is lower than top of stack, then calculate area of rectangle
                // with stack top as the smallest (or minimum height) bar. 'i' is
                // 'right index' for the top and element before top in stack is 'left index'
            else
            {
                tp = s.peek();  // store the top index
                s.pop();  // pop the top

                // Calculate the area with hist[tp] stack as smallest bar
                area_with_top = heights[tp] * (s.empty() ? i : i - s.peek() - 1);

                // update max area, if needed
                if (max_area < area_with_top)
                    max_area = area_with_top;
            }
        }

        // Now pop the remaining bars from stack and calculate area with every
        // popped bar as the smallest bar
        while (s.empty() == false)
        {
            tp = s.peek();
            s.pop();
            area_with_top = heights[tp] * (s.empty() ? i : i - s.peek() - 1);

            if (max_area < area_with_top)
                max_area = area_with_top;
        }
        return max_area;
    }



}
