package stack;

import java.util.Stack;

// almost same with stack.BackspaceStringCompare !
public class StringDeduplication {

    // 2p to stimulate stack: O(n) time and O(1) space
    String deduplicate_inplace(String str) {
        char[] arr = str.toCharArray();
        int l=0,r=0;
        while (r<arr.length) {
            if (l==0
                || Character.isUpperCase(arr[l-1])&&Character.isUpperCase(arr[r])
                || Character.isLowerCase(arr[l-1])&&Character.isLowerCase(arr[r])
                || Character.toLowerCase(arr[l-1])!=Character.toLowerCase(arr[r])) {
                arr[l++] = arr[r++];
            } else {
                l--;r++;
            }
        }
        return new String(arr, 0, l);
    }


    // stack, time:O(n) space:O(n)
    String deduplicate(String str) {
        Stack<Character> stack = new Stack<>();
        for(int i=str.length()-1;i>=0;i--) {
            if (stack.isEmpty()
                || Character.isLowerCase(str.charAt(i))&&Character.isLowerCase(stack.peek())
                || Character.isUpperCase(str.charAt(i))&&Character.isUpperCase(stack.peek())
                || Character.toLowerCase(str.charAt(i))!=Character.toLowerCase(stack.peek())) {
                stack.push(str.charAt(i));
            } else {
                stack.pop();
            }
        }
        StringBuilder sb = new StringBuilder();
        while (!stack.isEmpty()) sb.append(stack.pop());
        return sb.toString();
    }

    public static void main(String[] args) {
        System.out.println(new StringDeduplication().deduplicate("abBCcd"));
        System.out.println(new StringDeduplication().deduplicate("abBCcAd"));
        System.out.println(new StringDeduplication().deduplicate("aab"));

        System.out.println(new StringDeduplication().deduplicate_inplace("abBCcd"));
        System.out.println(new StringDeduplication().deduplicate_inplace("abBCcAd"));
        System.out.println(new StringDeduplication().deduplicate_inplace("aab"));

    }
}

/*
然后是做题，string deduplication的变种，给一个string，保证全是大小写英文字母，当出现同一个字母的一个大写挨着一个小写时候，消除掉这个pair。
比如：
abBCcd -> return ad
abBCcAd -> return d

思路：双指针直接o(n)时间o(1)空间解出来，slow指针用来模拟栈的结构，每次检查slow-1字母和当前字母是否是同一个字母的大小写，是的话弹栈，不是的话当前字母进栈。很常规的字符串处理方法。

当时写的code：
String deduplicate(String str) {
	// sanity check, 也是和面试官讨论了下，说不用写了，直接写主函数
	char[] s = str.toCharArray();
    int slow = 0;
    int fast = 0;
    while(fast < s.length) {
        if(slow == 0) {
            s[slow++] = s[fast];
        } else {
            if(Character.isUpperCase(s[slow -1]) && Character.isLowerCase(s[fast]) || Character.isLowerCase(s[slow-1]) && Character.isUpperCase(s[fast])) {
                char c1 = Character.toLowerCase(s[slow-1]);
                char c2 = Character.toLowerCase(s[fast]);
                if(c1 == c2) {
                    slow--;
                } else {
                    s[slow++] = s[fast];
                }
            }
        }
        fast++;
    }

    return new String(s, 0, slow);
    }
}

*/