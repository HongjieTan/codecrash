package stack._Topic_EvaluateExpression_ExpressionParsing;

import java.util.*;

/*

desc1:
给一个String “a{1,2}b{3,4}d”输出 a1b3d,a2b3d,a2b3d,a2b4d
{a,b}.txt 输出a.txt, b.txt
a.java,b.py,c{.java,.py}, 它应该输出的是a.java,b.py,c.java,c.py
对大括号中的以逗号分; 大括号外部也是以逗号分隔，
输入是string,输出也是string

desc2:
{a, b}c{d,e}f 需要return 所有可能的组合，acdf，acef，bcdf，bcef。
followup 怎么处理 nested case:    a{b{c, d}e{f}}


Tan: good!  hard in coding!!!
输入是string, 比如：DecodeString，EvaluateReversePolishNotation，。。。
 - 要么用stack来做parsing，
 - 要么找到左右子边界用recursion(会比stack多一遍pass,问题似乎也不大)

*/
public class StringCombinations {
    // stack
    public String listAllCombinations_stack(String str) {
        Stack<StringBuilder> resStack = new Stack<>();
        Stack<List<StringBuilder>> prefixStack = new Stack<>(); //  {a,b}c <--> a,bc 区别不出，要用Stack<List<StringBuilder>>
        resStack.push(new StringBuilder());
        prefixStack.push(new ArrayList<>());
        prefixStack.peek().add(new StringBuilder());

        for(char c: str.toCharArray()) {
            if (c=='{') {
                resStack.push(new StringBuilder());
                prefixStack.push(new ArrayList<>());
                prefixStack.peek().add(new StringBuilder());
            } else if (c=='}') { // a{b,c} -> prefix: a -> ab,ac
                StringBuilder sb = new StringBuilder();
                for(StringBuilder prefix: prefixStack.peek()) sb.append(prefix).append(",");
                resStack.peek().append(sb);

                StringBuilder sub = resStack.peek();
                resStack.pop();
                prefixStack.pop();

                List<StringBuilder> temp = new ArrayList<>();
                List<StringBuilder> prefixList = prefixStack.peek();
                String[] splits = sub.toString().split(",");
                for (StringBuilder prefix: prefixList) {
                    for(String split: splits) {
                        temp.add(new StringBuilder(prefix.toString()+split));
                    }
                }

                prefixStack.pop();prefixStack.push(temp);
            } else if (c==',') { // start new prefix...
                StringBuilder sb = new StringBuilder();
                for(StringBuilder prefix: prefixStack.peek()) sb.append(prefix).append(",");
                resStack.peek().append(sb);

                prefixStack.peek().clear();
                prefixStack.peek().add(new StringBuilder());
            } else { // character
                for (StringBuilder prefix: prefixStack.peek()) prefix.append(c);
            }
        }

        StringBuilder sb = new StringBuilder();
        for(StringBuilder prefix: prefixStack.peek()) sb.append(prefix).append(",");
        resStack.peek().append(sb);

        if (resStack.peek().length()>0) resStack.peek().deleteCharAt(resStack.peek().length()-1);
        return resStack.peek().toString();
    }

    // recursion
    public String listAllCombinations_recursion(String str) {
        return combinations(str, 0, str.length()-1).toString();
    }

    StringBuilder combinations(String str, int l, int r) {
        StringBuilder res = new StringBuilder();
        List<StringBuilder> prefixList = new ArrayList<>();
        prefixList.add(new StringBuilder());
        int idx=l;
        while(idx<=r) {
            if(str.charAt(idx)=='{') {
                int subL = idx+1;
                int subR = findSubR(str, subL);
                StringBuilder sub = combinations(str, subL, subR); // 支持嵌套
                List<StringBuilder> temp = new ArrayList<>();
                for(StringBuilder curPath: prefixList) {
                    for(String split: sub.toString().split(",")) {
                        temp.add(new StringBuilder(curPath.toString()+split));
                    }
                }
                prefixList = temp;
                idx=subR+2;
            } else if (str.charAt(idx)==',') {
                for (StringBuilder prefix: prefixList) res.append(prefix).append(",");

                prefixList = new ArrayList<>();
                prefixList.add(new StringBuilder());
                idx++;
            } else {
                for (StringBuilder curPath: prefixList) curPath.append(str.charAt(idx));
                idx++;
            }
        }

        for (StringBuilder prefix: prefixList) res.append(prefix).append(",");

        if (res.length()>0) res.deleteCharAt(res.length()-1);
        return res;
    }

    int findSubR(String str, int subR) {
        int leftBrackets=1;
        while(leftBrackets>0){
            if(str.charAt(subR)=='}') leftBrackets--;
            else if(str.charAt(subR)=='{') leftBrackets++;
            subR++;
        }
        subR--;
        return subR-1;
    }


//


    public static void main(String[] args) {
        System.out.println(new StringCombinations().listAllCombinations_stack("a{1,2}b{3,4}d"));
        System.out.println(new StringCombinations().listAllCombinations_stack("a{b,c},{1,2}"));
        System.out.println(new StringCombinations().listAllCombinations_stack("a{b{c,d,e},{x,y}}"));
        System.out.println(new StringCombinations().listAllCombinations_stack("b{c,d,e}"));
        System.out.println(new StringCombinations().listAllCombinations_stack("1,2"));
        System.out.println(new StringCombinations().listAllCombinations_stack("{1,2}"));
        System.out.println(new StringCombinations().listAllCombinations_stack("{a,b}.txt"));
        System.out.println(new StringCombinations().listAllCombinations_stack("a.java,b.py,c{.java,.py}"));

        System.out.println();
        System.out.println(new StringCombinations().listAllCombinations_recursion("a{1,2}b{3,4}d"));
        System.out.println(new StringCombinations().listAllCombinations_recursion("a{b,c},{1,2}"));
        System.out.println(new StringCombinations().listAllCombinations_recursion("a{b{c,d,e},{x,y}}"));
        System.out.println(new StringCombinations().listAllCombinations_recursion("b{c,d,e}"));
        System.out.println(new StringCombinations().listAllCombinations_recursion("1,2"));
        System.out.println(new StringCombinations().listAllCombinations_recursion("{1,2}"));
        System.out.println(new StringCombinations().listAllCombinations_recursion("{a,b}.txt"));
        System.out.println(new StringCombinations().listAllCombinations_recursion("a.java,b.py,c{.java,.py}"));

    }
}


/*
ref:
public static void generate(String s){
        Deque<String> dq = new LinkedList<>();
        dq.add("");
        String cur = "";
        for(int i = 0; i < s.length(); i++){
            char c = s.charAt(i);
            if (c == '{') {
                processQueue(dq, cur);
                cur = "";
                List<String> temp = brackets(s, i + 1);
                i = Integer.parseInt(temp.get(temp.size() - 1)) - 1;
                temp.remove(temp.size() - 1);
                int size = dq.size();
                while(size-- > 0){
                    String prefix = dq.pollFirst();
                    for(String tmp: temp){
                        dq.add(prefix + tmp);
                    }
                }
            }else{
                cur += c;
            }
        }
        if(cur != null){
            processQueue(dq, cur);
        }
        while (!dq.isEmpty()) {
            System.out.println(dq.poll());
        }
    }

    public static Deque<String> processQueue(Deque<String> dq, String toBeAdded){
        int size = dq.size();
        while(size-- > 0){
            dq.add(dq.pollFirst() + toBeAdded);
        }
        return dq;
    }
    public static List<String> brackets(String s, int start){
        List<String> res = new ArrayList<>();
        String cur = "";
        int left = 1;
        int i;
        for(i = start; i < s.length() && left > 0; i++){
            char c = s.charAt(i);
            if(c == ','){
                res.add(cur);
                cur = "";
            }else if(c == '{'){
                List<String> temp = brackets(s, i + 1);
                i = Integer.parseInt(temp.get(temp.size() - 1)) - 1;
                temp.remove(temp.size() - 1);
                res.addAll(temp);
            }else if(c == '}'){
                left--;
                if(cur.length() != 0){
                    res.add(cur);
                    cur = "";
                }
            }else{
                cur += c;
            }
        }
        res.add(String.valueOf(i));
        return res;
    }
*/


/*
def parse_string(string):
    def find_sub(s, i) -> List[str]:
        sub = ''
        while i < len(s) and '}' not in sub:
            sub += s[i]
            i += 1
        return sub[:-1].split(','), i - 1
    if not string: return []
    prefix, res = '', ['']
    index = 0
    while index < len(string):
        if string[index] == '{':
            sub, index = find_sub(string, index + 1)
            res = [i + j for i in res for j in sub]
        else:
            res = [i + string[index] for i in res]
        index += 1
    return res
*/

/*
def parseStr(s):
    finalResult = ''
    result = ['']
    inBrace = False
    currentWord = ''
    currentQueue = []
    for i in s:
            if i is '{':
                    inBrace = True
            elif i is '}':
                    inBrace = False
                    currentQueue.append(currentWord)
                    currentWord = ''
                    result = [ a + b for b in currentQueue for a in result]
                    currentQueue = []
            elif i is ',' and inBrace:
                    currentQueue.append(currentWord)
                    currentWord = ''
            elif i is ',':
                    result = [ a + i for a in result]
                    finalResult = finalResult+ ','.join(result)
                    result = ['']
            else:
                    if not inBrace:
                            result = [ a + i for a in result]
                    else:
                            currentWord += i
    return finalResult+ ','.join(result)

print parseStr('a{1,2}b{2,3}c')
print parseStr('a{.py,.java}')
print parseStr('a.java,b.py,c{.java,.py}')
print parseStr('a.java,b.py,c{.java,.py},d{.java,.py}')

如果沒有nested braces的話, 可以直接用iteration做.

所有在{}裏面的東西可以split with ','
然後把它們放到list裏, 接著就把它們append to previous result 1 by 1.
*/

