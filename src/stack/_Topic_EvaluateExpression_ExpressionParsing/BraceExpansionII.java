//Under a grammar given below, strings can represent a set of lowercase words. Let's use R(expr) to denote the set of words the expression represents.
//
// Grammar can best be understood through simple examples:
//
//
// Single letters represent a singleton set containing that word.
//
// R("a") = {"a"}
// R("w") = {"w"}
//
//
// When we take a comma delimited list of 2 or more expressions, we take the union of possibilities.
//
// R("{a,b,c}") = {"a","b","c"}
// R("{{a,b},{b,c}}") = {"a","b","c"} (notice the final set only contains each word at most once)
//
//
// When we concatenate two expressions, we take the set of possible concatenations between two words where the first word comes from the first expression and the second word comes from the second expression.
//
// R("{a,b}{c,d}") = {"ac","ad","bc","bd"}
// R("a{b,c}{d,e}f{g,h}") = {"abdfg", "abdfh", "abefg", "abefh", "acdfg", "acdfh", "acefg", "acefh"}
//
//
//
//
// Formally, the 3 rules for our grammar:
//
//
// For every lowercase letter x, we have R(x) = {x}
// For expressions e_1, e_2, ... , e_k with k >= 2, we have R({e_1,e_2,...}) = R(e_1) ∪ R(e_2) ∪ ...
// For expressions e_1 and e_2, we have R(e_1 + e_2) = {a + b for (a, b) in R(e_1) × R(e_2)}, where + denotes concatenation, and × denotes the cartesian product.
//
//
// Given an expression representing a set of words under the given grammar, return the sorted list of words that the expression represents.
//
//
//
//
// Example 1:
//
//
//Input: "{a,b}{c,{d,e}}"
//Output: ["ac","ad","ae","bc","bd","be"]
//
//
//
// Example 2:
//
//
//Input: "{{a,z},a{b,c},{ab,z}}"
//Output: ["a","ab","ac","z"]
//Explanation: Each distinct word is written only once in the final answer.
//
//
//
//
// Constraints:
//
//
// 1 <= expression.length <= 50
// expression[i] consists of '{', '}', ','or lowercase English letters.
// The given expression represents a set of words based on the grammar given in the description.
//
//
//
//
package stack._Topic_EvaluateExpression_ExpressionParsing;

import java.util.*;


/*
    recursion:  once brace expression encountered, solve the sub expression recursively
    iterative:  use stack...
 */
public class BraceExpansionII {

    // once brace expression encountered, solve it recursively
    public List<String> braceExpansionII_v2(String expression) {
        char[] s = expression.toCharArray();
        List<List<String>> groups = new ArrayList<>();
        groups.add(Arrays.asList(""));
        int i = 0;
        while(i<s.length) {
            if(s[i]=='{') {
                int end = matchingBrace(s, i);
                List<String> sg = braceExpansionII_v2(expression.substring(i+1, end));  // recursive...
                List<String> pg = groups.get(groups.size()-1);
                List<String> ng = new ArrayList<>();
                for(String prefix: pg) {
                    for(String suffix: sg) {
                        ng.add(prefix+suffix);
                    }
                }
                groups.set(groups.size()-1, ng);
                i=end;
            } else if (s[i]==',') {
                groups.add(Arrays.asList(""));
            } else {
                List<String> pg = groups.get(groups.size()-1);
                List<String> ng = new ArrayList<>();
                for(String prefix: pg) ng.add(prefix+s[i]);
                groups.set(groups.size()-1, ng);
            }
            i++;
        }

        Set<String> set = new HashSet<>();
        for(List<String> g: groups) set.addAll(g);
        List<String> res = new ArrayList<>(set);
        Collections.sort(res);
        return res;
    }

    int matchingBrace(char[] s, int i) {
        int cnt=1;
        while(cnt>0) {
            ++i;
            if(s[i]=='{') cnt++;
            else if(s[i]=='}') cnt--;
        }
        return i;
    }

    public List<String> braceExpansionII_v1(String expression) {
        List<List<String>> groups = new ArrayList<>();
        groups.add(Arrays.asList(""));
        char[] s = expression.toCharArray();
        int level=0, start=0;
        for(int i=0;i<s.length;i++) {
            if(s[i]=='{') {
                if (level==0) start=i+1;
                level++;
            } else if(s[i]=='}') {
                level--;
                if (level==0) {
                    List<String> suffixGroup = braceExpansionII_v1(expression.substring(start, i));
                    List<String> prefixGroup = groups.get(groups.size()-1);
                    List<String> ng = new ArrayList<>();
                    for(String prefix: prefixGroup) {
                        for(String suffix: suffixGroup) {
                            ng.add(prefix+suffix);
                        }
                    }
                    groups.set(groups.size()-1, ng);
                }
            } else if(s[i]==',' && level==0) {
                groups.add(Arrays.asList(""));
            } else if (level==0){
                List<String> ng = new ArrayList<>();
                for(String prefix: groups.get(groups.size()-1)) {
                    ng.add(prefix+s[i]);
                }
                groups.set(groups.size()-1, ng);
            }
        }

        Set<String> set = new HashSet<>();
        for(List<String> sl: groups) set.addAll(sl);
        List<String> res = new ArrayList<>(set);
        Collections.sort(res);
        return res;
    }

//    public List<String> braceExpansionII(String expression) {
//        Stack<Set<String>> st = new Stack<>();
//        st.push(new HashSet<>());
//
//        StringBuilder tmp = new StringBuilder();
//        char[] expr = expression.toCharArray();
//        for(int i=0;i<expr.length;i++) {
//            char c = expr[i];
//            if(c == '{') {
//                st.push(new HashSet<>());
//            } else if(c == '}') {
//                Set<String> cur = st.pop();
//                Set<String> prev = st.pop();
//                Set<String> concat = new HashSet<>();
//                if(prev.isEmpty()) concat.addAll(cur);
//                else {
//                    for(String x: prev) {
//                        for(String y: cur) {
//                            concat.add(x+y);
//                        }
//                    }
//                }
//                st.push(concat);
//            } else if (c == ',') {
////                st.push(new HashSet<>());
//                // do nothing
//            } else {
//                tmp.append(c);
//                if(expr[i+1]<'a' || expr[i+1]>'z') {
//                    st.peek().add(tmp.toString());
//                    tmp = new StringBuilder();
//                }
//            }
//        }
//
//        while(st.size()>1)  for(String x: st.pop()) st.peek().add(x);
//        return new ArrayList<>(st.peek());
//    }


    public static void main(String[] a) {
        List<String> res = new BraceExpansionII().braceExpansionII_v1("a{b,c}");
        for (String x:res) System.out.println(x);
     }
}


/*
by uwi:
 class Solution {
		char[] s;
		int pos;
		int len;

	    public List<String> braceExpansionII(String expression) {
	        s = expression.toCharArray();
	        pos = 0;
	        len = s.length;
	        List<String> expr = conc();
	        assert pos == len;
	        Collections.sort(expr);
	        return expr;
	    }

	    List<String> conc()
	    {
	    	List<String> ret = new ArrayList<>();
	    	ret.add("");
	    	while(pos < len && s[pos] != ',' && s[pos] != '}'){
	    		List<String> res = expr();
	    		List<String> nret = new ArrayList<>();
	    		for(String x : ret){
	    			for(String y : res){
	    				nret.add(x+y);
	    			}
	    		}
	    		ret = nret;
	    	}
	    	return ret;
	    }

	    List<String> expr()
	    {
	    	if(s[pos] == '{'){
	    		pos++;
	    		Set<String> items = new HashSet<>();
	    		while(true){
		    		items.addAll(conc());
		    		if(pos < len && s[pos] == ','){
		    			pos++;
		    		}else{
		    			break;
		    		}
	    		}
	    		assert s[pos] == '}';
	    		pos++;
	    		return new ArrayList<>(items);
	    	}else{
	    		assert s[pos] >= 'a' && s[pos] <= 'z';
	    		StringBuilder sb = new StringBuilder();
	    		while(pos < len && s[pos] >= 'a' && s[pos] <= 'z'){
	    			sb.append(s[pos++]);
	    		}
	    		List<String> ret = new ArrayList<>();
	    		ret.add(sb.toString());
	    		return ret;
	    	}
	    }
	}


by top-voted:
The general idea
Split expressions into groups separated by top level ',';
for each top-level sub expression (substrings with braces), process it and add it to the corresponding group;
finally combine the groups and sort.

Thought process
in each call of the function, try to remove one level of braces
maintain a list of groups separated by top level ','
when meets ',': create a new empty group as the current group
when meets '{': check whether current level is 0, if so, record the starting index of the sub expression;
always increase level by 1, no matter whether current level is 0
when meets '}': decrease level by 1; then check whether current level is 0, if so, recursively call braceExpansionII to get the set of words from expresion[start: end], where end is the current index (exclusive).
add the expanded word set to the current group
when meets a letter: check whether the current level is 0, if so, add [letter] to the current group
base case: when there is no brace in the expression.
finally, after processing all sub expressions and collect all groups (seperated by ','), we initialize an empty word_set, and then iterate through all groups
for each group, find the product of the elements inside this group
compute the union of all groups
sort and return
note that itertools.product(*group) returns an iterator of tuples of characters, so we use''.join() to convert them to strings
class Solution:
    def braceExpansionII(self, expression: str) -> List[str]:
        groups = [[]]
        level = 0
        for i, c in enumerate(expression):
            if c == '{':
                if level == 0:
                    start = i+1
                level += 1
            elif c == '}':
                level -= 1
                if level == 0:
                    groups[-1].append(self.braceExpansionII(expression[start:i]))
            elif c == ',' and level == 0:
                groups.append([])
            elif level == 0:
                groups[-1].append([c])
        word_set = set()
        for group in groups:
            word_set |= set(map(''.join, itertools.product(*group)))
        return sorted(word_set)

*/
