/*
Given an encoded string, return it's decoded string.

The encoding rule is: k[encoded_string], where the encoded_string inside the square brackets is being repeated exactly k times.
Note that k is guaranteed to be a positive integer.

You may assume that the input string is always valid; No extra white spaces, square brackets are well-formed, etc.

Furthermore, you may assume that the original data does not contain any digits and that digits are only for those repeat numbers, k.
For example, there won't be input like 3a or 2[4].

Examples:

s = "3[a]2[bc]", return "aaabcbc".
s = "3[a2[c]]", return "accaccacc".
s = "2[abc]3[cd]ef", return "abcabccdcdcdef".
*/

package stack._Topic_EvaluateExpression_ExpressionParsing;

import java.util.Stack;

public class DecodeString {

    //  concise version...
    public String decodeString_x2(String s) {
        Stack<StringBuilder> stack = new Stack<>();
        Stack<StringBuilder> tStack = new Stack<>();
        stack.push(new StringBuilder());
        int i=0;
        while(i<s.length()) {
            if(Character.isDigit(s.charAt(i))) {
                stack.push(new StringBuilder());
                tStack.push(new StringBuilder());
                while(Character.isDigit(s.charAt(i))) {
                    tStack.peek().append(s.charAt(i++));
                }
            } else if (s.charAt(i)=='['){
                // nothing
                i++;
            } else if (s.charAt(i)==']') {
                String unit = stack.pop().toString();
                int times = Integer.valueOf(tStack.pop().toString());
                for(int j=0;j<times;j++) stack.peek().append(unit);
                i++;
            } else {
                stack.peek().append(s.charAt(i));
                i++;
            }
        }
        return stack.peek().toString();
    }


//    // more concise
//    public String decodeString_v2(String s) {
//        if (s==null || s.isEmpty()) return "";
//        Stack<Integer> count = new Stack<>();
//        Stack<String> res = new Stack<>();
//        int idx=0;
//        res.push("");
//        while (idx<s.length()) {
//            char c = s.charAt(idx);
//            if (Character.isDigit(c)) {
//                int k=0;
//                while (Character.isDigit(s.charAt(idx))) {
//                    k = k*10 + s.charAt(idx) - '0';
//                    idx++;
//                }
//                count.push(k);
//                continue;
//            } else if (c=='[') {
//                res.push("");
//            } else if (Character.isLetter(c)) {
//                res.push(res.pop()+c);
//            } else if (c==']') {
//                int k = count.pop();
//                String unitStr = res.pop();
//                StringBuffer sb = new StringBuffer();
//                for (int i = 0; i < k; i++) {
//                    sb.append(unitStr);
//                }
//                res.push(res.pop()+sb.toString()); // notice...
//            }
//            idx++;
//        }
//        return res.pop();
//    }
//
//    // my first solution
//    public String decodeString_v0(String s) {
//        Stack<String> stack = new Stack<>();
//        for (int i = 0; i < s.length(); i++) {
//            char c = s.charAt(i);
//            if (stack.isEmpty()) {
//                stack.push(c+"");
//            } else {
//                if (Character.isDigit(c)) {
//                    if (Character.isDigit(stack.peek().charAt(0))) {
//                        int preNum = Integer.valueOf(stack.pop());
//                        int newNum = preNum*10 + c-'0';
//                        stack.push(String.valueOf(newNum));
//                    } else {
//                        stack.push(c+"");
//                    }
//                } else if (c=='[') {
//                    stack.push(c+"");
//                } else if (Character.isLetter(c)) {
//                    stack.push(c+"");
//                } else if (c==']') {
//                    StringBuffer encodedStr = new StringBuffer();
//                    while (Character.isLetter(stack.peek().charAt(0))) encodedStr.insert(0, stack.pop());
//                    stack.pop();// pop '['
//                    int k = Integer.valueOf(stack.pop());
//                    StringBuffer newStr = new StringBuffer();
//                    for (int j = 0; j < k; j++) newStr.append(encodedStr);
//                    stack.push(newStr.toString());
//                }
//            }
//        }
//
//        StringBuffer ans = new StringBuffer();
//        while (!stack.isEmpty()) ans.insert(0, stack.pop());
//        return ans.toString();
//    }

    public static void main(String[] args) {
        System.out.println(new stack._Topic_EvaluateExpression_ExpressionParsing.DecodeString().decodeString_x2("3[z]2[2[y]pq4[2[jk]e1[f]]]ef"));
//        System.out.println(new DecodeString().decodeString_v2("asdf"));
//        System.out.println(new DecodeString().decodeString_v2("9[a]"));
    }
}

