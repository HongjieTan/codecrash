//You are given a string s that consists of lower case English letters and brackets.
//
// Reverse the strings in each pair of matching parentheses, starting from the innermost one.
//
// Your result should not contain any brackets.
//
//
// Example 1:
//
//
//Input: s = "(abcd)"
//Output: "dcba"
//
//
// Example 2:
//
//
//Input: s = "(u(love)i)"
//Output: "iloveu"
//Explanation: The substring "love" is reversed first, then the whole string is reversed.
//
//
// Example 3:
//
//
//Input: s = "(ed(et(oc))el)"
//Output: "leetcode"
//Explanation: First, we reverse the substring "oc", then "etco", and finally, the whole string.
//
//
// Example 4:
//
//
//Input: s = "a(bcdefghijkl(mno)p)q"
//Output: "apmnolkjihgfedcbq"
//
//
//
// Constraints:
//
//
// 0 <= s.length <= 2000
// s only contains lower case English characters and parentheses.
// It's guaranteed that all parentheses are balanced.
//
// Related Topics Stack

package stack._Topic_EvaluateExpression_ExpressionParsing;
import java.util.Stack;

public class ReverseSubstringsBetweenEachPairOfParentheses {

    // O(n), two pass...cool!
    public String reverseParentheses(String s) {
        int n = s.length();
        StringBuilder sb = new StringBuilder();
        int[] pair = new int[n];
        Stack<Integer> st = new Stack<>();

        // first pass: find all pairs
        for(int i=0;i<n;i++) {
            char c = s.charAt(i);
            if(c=='(') st.push(i);
            else if(c==')') {
                int j = st.pop();
                pair[i]=j;
                pair[j]=i;
            }
        }

        // second pass: build result string
        for(int i=0,d=1;i<n;i+=d) {
            char c = s.charAt(i);
            if(c=='(' || c==')') {
                d = -d;
                i = pair[i];
            } else {
                sb.append(c);
            }
        }
        return sb.toString();
    }

    // O(n^2), iterative, bruteforce
    public String reverseParentheses_v2(String s) {
        int n = s.length();
        Stack<StringBuilder> st = new Stack<>();
        st.push(new StringBuilder());

        for(int i=0;i<n;i++) {
            char c = s.charAt(i);
            if(c=='(') {
                st.push(new StringBuilder());
            }
            else if(c==')') {
                StringBuilder temp = st.pop();
                st.peek().append(temp.reverse());
            }
            else {
                st.peek().append(c);
            }
        }
        return st.peek().toString();
    }

    // O(n^2), recursive, bruteforce
    public String reverseParentheses_v1(String s) {
        int n = s.length();
        char[] chs = s.toCharArray();
        StringBuilder sb = new StringBuilder();

        int i = 0;
        while(i<n) {
            if(chs[i]=='(') {
                int l=i, r=matchingParenth(chs, l);
                String rev = reverse(s.substring(l+1,r));
                sb.append(reverseParentheses_v1(rev));
                i=r+1;
            } else {
                sb.append(chs[i]);
                i++;
            }
        }
        return sb.toString();
    }

    String reverse(String s) {
        StringBuilder sb = new StringBuilder();
        for(int i=s.length()-1;i>=0;i--) {
            char c = s.charAt(i);
            if(c=='(') sb.append(')');
            else if(c==')') sb.append('(');
            else sb.append(c);
        }
        return sb.toString();
    }

    int matchingParenth(char[] chs, int index) {
        int level=1, i=index+1;
        while(i<chs.length) {
            if(chs[i]=='(') level++;
            else if(chs[i]==')') {
                level--;
                if(level==0) return i;
            }
            i++;
        }
        return -1;
    }
}

/*
by lee:
Solution 1: Brute Force
Sorry that I missed the contest.
brute force O(N^2) seem easy to write for this problem.


Python

    def reverseParentheses(self, s):
        res = ['']
        for c in s:
            if c == '(':
                res.append('')
            elif c == ')':
                res[len(res) - 2] += res.pop()[::-1]
            else:
                res[-1] += c
        return "".join(res)
C++

    string reverseParentheses(string s) {
        vector<int> opened;
        string res;
        for (int i = 0; i < s.length(); ++i) {
            if (s[i] == '(')
                opened.push_back(res.length());
            else if (s[i] == ')') {
                int j = opened.back();
                opened.pop_back();
                reverse(res.begin() + j, res.end());
            } else {
                res += s[i];
            }
        }
        return res;
    }

Solution 2: Wormholes
Nice. I got a green accpeted with solution 1.
Let me check the discuss before I move on to next problem.
Hey wait, all solutions are brute force?
Hmm...why not O(N)?

Fine.Here comes an easy O(N) solution.

In the first pass, use a stack to find all paired parentheses.

Now just imgine that all parentheses are wormholes.
And the paired parentheses are connected to each other.

In the second pass,
traverse through the paired parentheses
and generate the result string res.

i is the index of current position.
d is the direction of traversing.


Complexity
Time O(N) for two passes
Space O(N)


Java:

    public String reverseParentheses(String s) {
        int n = s.length();
        Stack<Integer> opened = new Stack<>();
        int[] pair = new int[n];
        for (int i = 0; i < n; ++i) {
            if (s.charAt(i) == '(')
                opened.push(i);
            if (s.charAt(i) == ')') {
                int j = opened.pop();
                pair[i] = j;
                pair[j] = i;
            }
        }
        StringBuilder sb = new StringBuilder();
        for (int i = 0, d = 1; i < n; i += d) {
            if (s.charAt(i) == '(' || s.charAt(i) == ')') {
                i = pair[i];
                d = -d;
            } else {
                sb.append(s.charAt(i));
            }
        }
        return sb.toString();
    }
C++:

    string reverseParentheses(string s) {
        int n = s.length();
        vector<int> opened, pair(n);
        for (int i = 0; i < n; ++i) {
            if (s[i] == '(')
                opened.push_back(i);
            if (s[i] == ')') {
                int j = opened.back();
                opened.pop_back();
                pair[i] = j;
                pair[j] = i;
            }
        }
        string res;
        for (int i = 0, d = 1; i < n; i += d) {
            if (s[i] == '(' || s[i] == ')')
                i = pair[i], d = -d;
            else
                res += s[i];
        }
        return res;
    }
Python:

    def reverseParentheses(self, s):
        opened = []
        pair = {}
        for i, c in enumerate(s):
            if c == '(':
                opened.append(i)
            if c == ')':
                j = opened.pop()
                pair[i], pair[j] = j, i
        res = []
        i, d = 0, 1
        while i < len(s):
            if s[i] in '()':
                i = pair[i]
                d = -d
            else:
                res.append(s[i])
            i += d
        return ''.join(res)




by python:
class Solution:
    def reverseParentheses(self, s: str) -> str:
        ans=""
        st=ed=cnt=0
        while st<len(s):
            if s[ed]=='(':
                cnt+=1
            elif s[ed]==')':
                cnt-=1
            if cnt==0:
                if st==ed:
                    ans+=s[ed]
                    st+=1
                else:
                    ans+=self.reverseParentheses(s[st+1:ed])[::-1]
                    st=ed+1
            ed+=1
        return ans


by neal_wu:
class Solution {
public:
    string reverseParentheses(string s) {
        int n = s.size();

        if (s.find('(') == string::npos)
            return s;

        int position = s.find('(');
        int level = 0;

        for (int i = position; i < n; i++)
            if (s[i] == '(')
                level++;
            else if (s[i] == ')') {
                level--;

                if (level == 0) {
                    string answer = s.substr(0, position);
                    string sub = s.substr(position + 1, i - (position + 1));
                    sub = reverseParentheses(sub);
                    reverse(sub.begin(), sub.end());
                    answer += sub;
                    answer += reverseParentheses(s.substr(i + 1));
                    return answer;
                }
            }

        assert(false);
    }
};

by uwi:
class Solution {
    public String reverseParentheses(String S) {
        outer:
        while(true){
            int n = S.length();
            int open = -1;
            for(int i = 0;i < n;i++){
                if(S.charAt(i) == '('){
                    open = i;
                }else if(S.charAt(i) == ')'){
                    StringBuilder sb = new StringBuilder();
                    sb.append(S.substring(0, open));
                    for(int j = i-1;j >= open+1;j--){
                        if(S.charAt(j) == ')'){
                            sb.append('(');
                        }else if(S.charAt(j) == '('){
                            sb.append(')');
                        }else{
                            sb.append(S.charAt(j));
                        }
                    }
                    sb.append(S.substring(i+1));
                    S = sb.toString();
                    continue outer;
                }
            }
            return S;
        }
    }
}
*/

