//Given a string containing just the characters '(', ')', '{', '}', '[' and ']', determine if the input string is valid.
//
// An input string is valid if:
//
//
// Open brackets must be closed by the same type of brackets.
// Open brackets must be closed in the correct order.
//
//
// Note that an empty string is also considered valid.
//
// Example 1:
//
//
//Input: "()"
//Output: true
//
//
// Example 2:
//
//
//Input: "()[]{}"
//Output: true
//
//
// Example 3:
//
//
//Input: "(]"
//Output: false
//
//
// Example 4:
//
//
//Input: "([)]"
//Output: false
//
//
// Example 5:
//
//
//Input: "{[]}"
//Output: true
//
//

package stack._Topic_EvaluateExpression_ExpressionParsing;

import java.util.Stack;


/*

   - recursive: ...
   - iterative: ...

 */
public class ValidParentheses {
    // recursive
    public boolean isValid(String s) {
        int idx=0;
        while(idx<s.length()) {
            char c = s.charAt(idx);
            if(c=='(' || c=='[' || c=='{') {
                int end = matchingEnd(s, idx, c);
                if(end==-1 || !isValid(s.substring(idx+1, end))) return false;
                idx = end+1;
            }
            else return false;
        }
        return true;
    }

    int matchingEnd(String s, int start, char p) {
        int level=0;
        int rp = ')';
        if(p=='[') rp=']';
        if(p=='{') rp='}';
        for(int i=start; i<s.length(); i++) {
            char c = s.charAt(i);
            if(c==p) {
                level++;
            } else if(c==rp) {
                level--;
                if(level==0) return i;
            }
        }
        return -1;
    }


    // iterative
    public boolean isValid_v1(String s) {
        Stack<Character> st = new Stack<>();
        for(char c: s.toCharArray()) {
            if(c == '(') st.push(c);
            if(c == '[') st.push(c);
            if(c == '{') st.push(c);
            if(c == ')') {
                if(!st.isEmpty() && st.peek()=='(') st.pop();
                else return false;
            }
            if(c == ']') {
                if(!st.isEmpty() && st.peek()=='[') st.pop();
                else return false;
            }
            if(c == '}') {
                if(!st.isEmpty() && st.peek()=='{') st.pop();
                else return false;
            }
        }
        return st.isEmpty();
    }

    public static void main(String[] a) {
        System.out.println(new ValidParentheses().isValid("()"));
    }
}
