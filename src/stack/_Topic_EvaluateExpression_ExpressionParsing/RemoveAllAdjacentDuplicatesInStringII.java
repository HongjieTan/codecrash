// Given a string s, a k duplicate removal consists of choosing k adjacent and equal letters from s and removing them causing the left and the right side of the deleted substring to concatenate together.
//
// We repeatedly make k duplicate removals on s until we no longer can.
//
// Return the final string after all such duplicate removals have been made.
//
// It is guaranteed that the answer is unique.
//
//
// Example 1:
//
//
//Input: s = "abcd", k = 2
//Output: "abcd"
//Explanation: There's nothing to delete.
//
// Example 2:
//
//
//Input: s = "deeedbbcccbdaa", k = 3
//Output: "aa"
//Explanation:
//First delete "eee" and "ccc", get "ddbbbdaa"
//Then delete "bbb", get "dddaa"
//Finally delete "ddd", get "aa"
//
// Example 3:
//
//
//Input: s = "pbbcggttciiippooaais", k = 2
//Output: "ps"
//
//
//
// Constraints:
//
//
// 1 <= s.length <= 10^5
// 2 <= k <= 10^4
// s only contains lower case English letters.
//
package stack._Topic_EvaluateExpression_ExpressionParsing;

import java.util.Stack;

public class RemoveAllAdjacentDuplicatesInStringII {
    class Node {
        char c;
        int cnt;
        public Node(char c, int cnt) {this.c=c;this.cnt=cnt;}
    }
    public String removeDuplicates(String s, int k) {
        Stack<Node> st = new Stack<>();
        for(char c:s.toCharArray()){
            if(st.isEmpty()) st.add(new Node(c,1));
            else if(st.peek().c!=c) st.add(new Node(c,1));
            else { //st.peek().c==c
                if(++st.peek().cnt == k) st.pop();
            }
        }

        StringBuilder sb = new StringBuilder();
        while(!st.isEmpty()) {
            for(int i=0;i<st.peek().cnt;i++) sb.append(st.peek().c);
            st.pop();
        }
        return sb.reverse().toString();
    }

    public static void main(String[] as) {
//        System.out.println(new RemoveAllAdjacentDuplicatesInStringII().removeDuplicates("abcd", 2));
//        System.out.println(new RemoveAllAdjacentDuplicatesInStringII().removeDuplicates("deeedbbcccbdaa", 3));
        System.out.println(new RemoveAllAdjacentDuplicatesInStringII().removeDuplicates("pbbcggttciiippooaais", 2)); // ps or is 有歧义？？？？
    }
}


/*
by uwi:
class Solution {
    public String removeDuplicates(String s, int k) {
        int n = s.length();
        char[] cs = new char[n];
        int[] cts = new int[n];
        int p = 0;
        for(char c : s.toCharArray()){
            if(p > 0 && cs[p-1] == c){
                if(++cts[p-1] == k){
                    p--;
                }
            }else{
                cs[p] = c;
                cts[p] = 1;
                p++;
            }
        }
        StringBuilder sb = new StringBuilder();
        for(int i = 0;i < p;i++){
            for(int j = 0;j < cts[i];j++){
                sb.append(cs[i]);
            }
        }
        return sb.toString();
    }
}

*/