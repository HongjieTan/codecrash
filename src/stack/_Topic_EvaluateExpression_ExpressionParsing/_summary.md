表达式解析问题，输入一般是string格式的表达式, 比如：DecodeString，EvaluateReversePolishNotation，StringCombinations,...

两种方式: (复杂情况prefer递归方式，逻辑比较清楚些)
- recursion: 递归来解析子表达式。 （找到子表达式左右子边界后用recursion, 会比stack多一遍pass, 时间复杂度还是线性)
- stack: 用stack来做parsing
