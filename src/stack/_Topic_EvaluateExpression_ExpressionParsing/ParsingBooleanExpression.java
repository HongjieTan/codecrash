//Return the result of evaluating a given boolean expression, represented as a string.
//
// An expression can either be:
//
//
// "t", evaluating to True;
// "f", evaluating to False;
// "!(expr)", evaluating to the logical NOT of the inner expression expr;
// "&(expr1,expr2,...)", evaluating to the logical AND of 2 or more inner expressions expr1, expr2, ...;
// "|(expr1,expr2,...)", evaluating to the logical OR of 2 or more inner expressions expr1, expr2, ...
//
//
//
// Example 1:
//
//
//Input: expression = "!(f)"
//Output: true
//
//
// Example 2:
//
//
//Input: expression = "|(f,t)"
//Output: true
//
//
// Example 3:
//
//
//Input: expression = "&(t,f)"
//Output: false
//
//
// Example 4:
//
//
//Input: expression = "|(&(t,f,t),!(t))"
//Output: false
//
//
//
// Constraints:
//
//
// 1 <= expression.length <= 20000
// expression[i] consists of characters in {'(', ')', '&', '|', '!', 't', 'f', ','}.
// expression is a valid expression representing a boolean, as given in the description.
//

// hint: Write a function "parse" which calls helper functions "parse_or", "parse_and", "parse_not".

package stack._Topic_EvaluateExpression_ExpressionParsing;

import java.util.ArrayList;
import java.util.List;

public class ParsingBooleanExpression {

    public boolean parseBoolExpr(String expression) {
        if(expression.equals("t")) return true;
        if(expression.equals("f")) return false;

        char s[] = expression.toCharArray(), op=s[0];
        boolean res = false;
        int level=0, start=0;
        List<String> exprList = new ArrayList<>();
        for(int i=1;i<s.length;i++) {
            char c=s[i];
            if(c=='(') {
                if(level==0) start=i+1;
                level++;
            }
            if(c==')') {
                level--;
                if(level==0) exprList.add(expression.substring(start, i));
            }
            if(c==',') {
                if(level==1) {
                    exprList.add(expression.substring(start, i));
                    start=i+1;
                }
            }
        }

        if(op=='&') {
            boolean val = true;
            for(String expr: exprList)  val = val && parseBoolExpr(expr);
            res = val;
        }
        if(op=='|') {
            boolean val = false;
            for(String expr: exprList)  val = val || parseBoolExpr(expr);
            res = val;
        }
        if(op=='!') {
            res = !parseBoolExpr(exprList.get(0));
        }
        return res;
    }

    public static void main(String[] a) {
        String expression1 = "!(f)";
        String expression2 = "|(f,t)";
        String expression3 = "&(t,f)";
        String expression4 = "|(&(t,f,t),!(t))";
        String expression5 = "|(t,f,f)";
//        System.out.println(new ParsingBooleanExpression().parseBoolExpr(expression1));
//        System.out.println(new ParsingBooleanExpression().parseBoolExpr(expression2));
//        System.out.println(new ParsingBooleanExpression().parseBoolExpr(expression3));
//        System.out.println(new ParsingBooleanExpression().parseBoolExpr(expression4));
        System.out.println(new ParsingBooleanExpression().parseBoolExpr(expression5));
    }
}


/*
by uwi:

class Solution {
    char[] s;
    int len, pos;

    public boolean parseBoolExpr(String expression) {
        s = expression.toCharArray();
        len = s.length;
        pos = 0;
        boolean res = expr();
        assert pos == len;
        return res;
    }

    boolean expr()
    {
        if(s[pos] == 't'){
            pos++;
            return true;
        }
        if(s[pos] == 'f'){
            pos++;
            return false;
        }
        if(s[pos] == '!'){
            pos++;
            assert s[pos] == '(';
            pos++;
            boolean res = expr();
            assert s[pos] == ')';
            pos++;
            return !res;
        }
        if(s[pos] == '&'){
            pos++;
            assert s[pos] == '(';
            pos++;
            boolean q = true;
            while(true){
                boolean res = expr();
                q &= res;
                if(s[pos] == ')')break;
                assert s[pos] == ',';
                pos++;
            }
            pos++;
            return q;
        }
        if(s[pos] == '|'){
            pos++;
            assert s[pos] == '(';
            pos++;
            boolean q = false;
            while(true){
                boolean res = expr();
                q |= res;
                if(s[pos] == ')')break;
                assert s[pos] == ',';
                pos++;
            }
            pos++;
            return q;
        }
        throw new RuntimeException("" + pos);
    }
}


By second post:
 Method 1: Iterative version - Use Stack and Set.

Loop through the input String:

Use a stack to store chars except ',' and ')';
If we find a ')', keep popping out the chars from the stack till find a '('; add the popped-out into a Set.
Pop out the operator after popping ')' out, push into stack the corresponding result according to the operator.
repeat the above till the end, and the remaining is the result.
[Java]

    public boolean parseBoolExpr(String expression) {
        Deque<Character> stk = new ArrayDeque<>();
        for (int i = 0; i < expression.length(); ++i) {
            char c = expression.charAt(i);
            if (c == ')') {
                Set<Character> seen = new HashSet<>();
                while (stk.peek() != '(')
                    seen.add(stk.pop());
                stk.pop();// pop out '('.
                char operator = stk.pop(); // get operator for current expression.
                if (operator == '&') {
                    stk.push(seen.contains('f') ? 'f' : 't'); // if there is any 'f', & expression results to 'f'
                }else if (operator == '|') {
                    stk.push(seen.contains('t') ? 't' : 'f'); // if there is any 't', | expression results to 't'
                }else { // ! expression.
                    stk.push(seen.contains('t') ? 'f' : 't'); // Logical NOT flips the expression.
                }
            }else if (c != ',') {
                stk.push(c);
            }
        }
        return stk.pop() == 't';
    }
[Python 3] credit to: @zengfei216 and @cenkay

    def parseBoolExpr(self, expression: str) -> bool:
        stack = []
        for c in expression:
            if c == ')':
                seen = set()
                while stack[-1] != '(':
                    seen.add(stack.pop())
                stack.pop()
                operator = stack.pop()
                stack.append(all(seen) if operator == '&' else any(seen) if operator == '|' else not seen.pop())
            elif c != ',':
                stack.append(True if c == 't' else False if c == 'f' else c)
        return stack.pop()
Method 2: Recursive version.

Use level to count the non-matched ( and ), together with ,, we can delimit valid sub-expression and hence recurse to sub-problem.

[Java]

    public boolean parseBoolExpr(String expression) {
        return parse(expression, 0, expression.length());
    }
    private boolean parse(String s, int lo, int hi) {
        char c = s.charAt(lo);
        if (hi - lo == 1) return c == 't'; // base case.
        boolean ans = c == '&'; // only when c is &, set ans to true; otherwise false.
        for (int i = lo + 2, start = i, level = 0; i < hi; ++i) {
            char d = s.charAt(i);
            if (level == 0 && (d == ',' || d == ')')) { // locate a valid sub-expression.
                boolean cur = parse(s, start, i); // recurse to sub-problem.
                start = i + 1; // next sub-expression start index.
                if (c == '&') ans &= cur;
                else if (c == '|') ans |= cur;
                else ans = !cur; // c == '!'.
            }
            if (d == '(') ++level;
            if (d == ')') --level;
        }
        return ans;
    }
[Python 3]

    def parseBoolExpr(self, expression: str) -> bool:

        def parse(e: str, lo: int, hi: int) -> bool:
            if hi - lo == 1: # base case
                return e[lo] == 't'
            ans = e[lo] == '&' # only when the first char is '&', ans assigned True.
            level, start = 0, lo + 2 # e[lo + 1] must be '(', so start from lo + 2 to delimit sub-expression.
            for i in range(lo + 2, hi):
                if level == 0  and e[i] in [',', ')']: # found a sub-expression.
                    cur = parse(e, start, i) # recurse to sub-problem.
                    start = i + 1 # start point of next sub-expression.
                    if e[lo] == '&':
                        ans &= cur
                    elif e[lo] == '|':
                        ans |= cur
                    else: # e[lo] is '!'.
                        ans = not cur
                if e[i] == '(':
                    level = level + 1
                elif e[i] == ')':
                    level = level - 1
            return ans;

        return parse(expression, 0, len(expression))
Analysis:
Time & space: O(n), n = expression.length().



By someone:
public boolean parseBoolExpr(String s) {
    if (s.equals("t")) return true;
    if (s.equals("f")) return false;
    char[] arr = s.toCharArray();
    char op = arr[0];
    boolean result = op != '|';
    int count = 0;
    for (int i = 1, pre = 2; i < arr.length; i++) {
      char c = arr[i];
      if (c == '(') count++;
      if (c == ')') count--;
      if (c == ',' && count == 1 || c == ')' && count == 0) {
        boolean next = parseBoolExpr(s.substring(pre, i));
        pre = i + 1;
        if (op == '|') result |= next;
        else if (op == '&') result &= next;
        else result = !next;
      }
    }
    return result;
  }
*/