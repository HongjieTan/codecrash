package stack;

import java.util.Stack;

/**
 * Created by thj on 01/06/2018.
 *
 *
 *
 */
public class MinStack {

    class Node {
        int curMin;
        int val;
        Node(int val, int curMin) {
            this.curMin = curMin;
            this.val = val;
        }
    }

    private Stack<Node> stack;

    /** initialize your data structure here. */
    public MinStack() {
        stack = new Stack<>();
    }

    public void push(int x) {
        if (stack.isEmpty()) {
            stack.push(new Node(x, x));
        } else {
            stack.push(new Node(x, Math.min(x, this.topNode().curMin)));
        }
    }

    public void pop() {
        stack.pop();
    }

    public int top() {
        return this.topNode().val;
    }

    private Node topNode() {
        return stack.peek();
    }

    public int getMin() {
        return topNode().curMin;
    }
}