package _Topic_SlidingWindow;
/*

desc1
面经题，一个integer数组，返回k window的和，除去前后20%

desc2
另外的描述：有一个大小为n的数组，一个大小为k的窗口，1<k<=n, 窗口在数组上slide，每到一个位置去掉数值大小前x% 和后x%的数，
然后求剩下的数的均值。对每一个窗口算一次，最后output出一个数组。求一个 nlogn的解法。。。

https://www.1point3acres.com/bbs/thread-490357-1-1.html
https://www.1point3acres.com/bbs/thread-477503-1-1.html


tan:
-  这题参照 Sliding Window Median，题目用了一个maxheap, 一个minheap分别维护 k/ 2, k - k/2个数。我们将其扩展为HeapX。
    class HeapX(k, x)。表示维护 前x*k 和 k-x*k个数。
    那么这题就是维护两个HeapX，pq1=HeapX(k, x), pq2=HeapX(k, 1 - x)。
    那么sum（中间k - 2 * k *x个数 ）= sum(pq1.minheap) - sum(pq2.minheap)。
    其中求sum可以用一个变量缓存，达到O(1)，最后理论上复杂度可以变成O(nlogk)

      left          right
    [......][....................]
      topk          n-topk


- 或者用3个treeset来维护三段，每加入一个元素，做balance(始终保持left，right大小为k*x)
      left      middle     right
    [......][..........][........]
      20%        60%        20%

   -> 貌似代码比维护两段繁琐。。。。coding 还是用两段吧。。。。。
 */

import java.util.Comparator;
import java.util.TreeSet;



public class SlidingWindowSumOfPortion {

    // the DS support:  DS.add()-O(logk)  DS.delete()-O(logk)  DS.topKSum()-O(1)
    class DS {
        TreeSet<Integer> left;
        TreeSet<Integer> right;
        int[] arr;
        int k;
        int topKSum;
        Comparator<Integer> comparator;
        public DS (int[] arr, int k){
            comparator = (a, b)->arr[a]!=arr[b]?Integer.compare(arr[a],arr[b]):a-b;
            left = new TreeSet<>(comparator);
            right = new TreeSet<>(comparator);
            this.arr = arr;
            topKSum = 0;
            this.k=k;
        }

        public void add(int i) { // O(logk)
            if(left.size()<k || arr[i]<left.last()) {
                left.add(i);
                topKSum+=arr[i];
            } else
                right.add(i);
            balance();
        }

        public void delete(int i) { // O(logk)
            if(comparator.compare(i, left.last())<=0) {
                left.remove(i);
                topKSum-=arr[i];
            }
            else right.remove(i);
            balance();
        }

        void balance(){
            if(left.size()==k+1) {
                topKSum-=arr[left.last()];
                right.add(left.pollLast());
            }
            if(!right.isEmpty() && left.size()==k-1) {
                topKSum+=arr[right.first()];
                left.add(right.pollFirst());
            }
        }

        public int topKSum() { //O(1)
            return topKSum;
        }

    }

    // O(n * log(window))!!!
    public int[] sumSlidingWindow(int[] nums, int window, int x) {
        int n = nums.length;
        int m = window*x/100;
        DS ds1 = new DS(nums, m);
        DS ds2 = new DS(nums, window-m);

        int[] ans = new int[n-window+1];
        for(int i=0;i<window-1;i++) { ds1.add(i); ds2.add(i);}

        int idx=0;
        for(int i=window-1; i<n;i++) {
            ds1.add(i);
            ds2.add(i);
            ans[idx++]= ds2.topKSum()-ds1.topKSum();
            ds1.delete(i-window+1);
            ds2.delete(i-window+1);
        }
        return ans;
    }

    public static void main(String[] args) {
        int[] nums = new int[]{1,2,3,4,5,6,7,8,9,10};
        int window=5;
        int x=20;
        int[] ans = new SlidingWindowSumOfPortion().sumSlidingWindow(nums, window, x);
        for(int a:ans) System.out.printf(a+" ");
    }
}
