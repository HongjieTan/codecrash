package _Topic_SlidingWindow;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.LinkedList;
import java.util.Queue;

public class SlidingWindowMaximum {
    public int[] maxSlidingWindow(int[] nums, int k) {
        if (nums.length == 0) return new int[0];

        int[] ans = new int[nums.length-k+1];
        MonoQueue monoqueue = new MonoQueue();

        for (int i=0;i<k-1;i++) {
            monoqueue.push(nums[i]);
        }

        for (int i=k-1;i<nums.length;i++) {
            monoqueue.push(nums[i]);
            ans[i-k+1] = monoqueue.max();
            monoqueue.pop();
        }
        return ans;
    }
}

//  DS(using MonoQueue): DS.add()- amortized O(1), DS.delete()-O(1), DS.max()-O(1)
//  DS(using treeset): DS.add()- O(logk), DS.delete()-O(logk), DS.max()-O(1)
class MonoQueue {
    class Node{
        int val;
        int deleteCount; // how many elements were deleted between it and the one before it.
        Node(int val, int deleteCount){this.val = val; this.deleteCount = deleteCount;}
    }

    private Deque<Node> deque;

    MonoQueue(){
        deque = new ArrayDeque<>();
    }

    public void push(int num){ // amortized O(1), as each num is pushed and polled only once...
        int count = 0;
        while (!deque.isEmpty() && num > deque.peekLast().val) {
            count += deque.peekLast().deleteCount+1;
            deque.pollLast();
        }
        deque.addLast(new Node(num, count));
    }

    public void pop() {
        if (deque.peekFirst().deleteCount>0) {
            deque.peekFirst().deleteCount--;
        } else {
            deque.pollFirst();
        }
    }

    public int max(){
        return deque.peekFirst().val;
    }



}
/*

*/