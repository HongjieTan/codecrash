package _Topic_SlidingWindow;

import java.util.Comparator;
import java.util.HashSet;
import java.util.TreeSet;



//  use 2-heaps (TreeSet):  as we need remove() in O(logk), so we use TreeSet here!!!

//  为何test case 有个跑不过？？ check later。。。 I got it!! every compare should use the customized comparator !!! 代码以后改，现在没时间。。。

public class SlidingWindowMedian {
    TreeSet<Integer> small ; // A trick: to avoid dup value, we store index here! 否则用TreeMap...
    TreeSet<Integer> large ;
    int[] arr;
    Comparator<Integer> comparator = (a,b)->arr[a]!=arr[b]?Integer.compare(arr[a], arr[b]):a-b;
    public double[] medianSlidingWindow(int[] nums, int k) {
        small = new TreeSet<>(comparator);
        large = new TreeSet<>(comparator);
        arr = nums;
        double[] ans = new double[nums.length-k+1];
        int idx=0;
        for(int i=0;i<nums.length;i++) {
            add(i);
            if (i>=k-1) {
                ans[idx++]=getMedian();
                delete(i-k+1);
            }
        }
        return ans;
    }

    void add(int i){
        if (small.isEmpty()) {small.add(i);return;}
        if (arr[i]<=getMedian()) small.add(i); // FIXME should use the customized comparator !!!
        else large.add(i);
        balance();
    }
    void delete(int i){
        if (arr[i]<=getMedian()) small.remove(i);  // FIXME
        else large.remove(i);
        balance();
    }
    void balance(){
        if(small.size()-large.size()>1) large.add(small.pollLast());
        if(large.size()>small.size()) small.add(large.pollFirst());
    }
    double getMedian() {
        return small.size()>large.size()?
                arr[small.last()]
                :((double)arr[small.last()]+arr[large.first()])*0.5d;
    }

    public static void main(String[] as) {
        double[] ans = new SlidingWindowMedian().medianSlidingWindow(new int[]{-2147483648,-2147483648,2147483647,-2147483648,-2147483648,-2147483648,2147483647,2147483647}, 3);
        for(double x:ans) System.out.println(x);
    }
}
