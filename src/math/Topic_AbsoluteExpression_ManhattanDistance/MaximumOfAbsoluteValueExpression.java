//Given two arrays of integers with equal lengths, return the maximum value of:
//
// |arr1[i] - arr1[j]| + |arr2[i] - arr2[j]| + |i - j|
//
// where the maximum is taken over all 0 <= i, j < arr1.length.
//
//
// Example 1:
//
//
//Input: arr1 = [1,2,3,4], arr2 = [-1,4,5,6]
//Output: 13
//
//
// Example 2:
//
//
//Input: arr1 = [1,-2,-5,0,10], arr2 = [0,-2,-1,-7,-4]
//Output: 20
//
//
//
// Constraints:
//
//
// 2 <= arr1.length == arr2.length <= 40000
// -10^6 <= arr1[i], arr2[i] <= 10^6
//
//
package math.Topic_AbsoluteExpression_ManhattanDistance;

public class MaximumOfAbsoluteValueExpression {
    /*
        |a[i] - a[j]| + |b[i] - b[j]| + |i - j|
        4 cases(assume i>=j):
         when a[i]>=a[j], b[i]>=b[j]: (in this case, f1(i)-f1(j)> fx(i)-fx(j))
            a[i]-a[j] + b[i] - b[j] + i-j  == ( a[i] + b[i] + i) - ( a[j] + b[j] + j) == f1(i)-f1(j), f1(x) =  a[x] + b[x] + x
         when a[i]>=a[j], b[i]< b[j]:
            a[j]-a[i] + b[i] - b[j] + i-j  == (-a[i] + b[i] + i) - (-a[j] + b[j] + j) == f2(i)-f2(j), f2(x) = -a[x] + b[x] + x
         when a[i]< a[j], b[i]>=b[j]:
            a[i]-a[j] + b[j] - b[i] + i-j  == ( a[i] - b[i] + i) - ( a[j] - b[j] + j) == f3(i)-f3(j), f3(x) =  a[x] - b[x] + x
         when a[i]< a[j], b[i]< b[j]:
            a[j]-a[i] + b[j] - b[i] + i-j  == (-a[i] - b[i] + i) - (-a[j] - b[j] + j) == f4(i)-f4(j), f4(x) = -a[x] - b[x] + x

        res = max(max1-min1, max2-min2, max3-min3, max4-min4)

        questions:
          - what if max result is based on a negative min value ?
               It is possible...
          - 是否一定能取到这个最值？(good!)
               Yes, 对于最值对应的i,j, 相应的fx(i)-fx(j) 必定是{f1,f2,f3,f4}中最大的, 从而fx(i)-fx(j)必定是i,j取值下正确的绝对值解析式。
    */
    public int maxAbsValExpr_v2(int[] a, int[] b) {
        int min1 = Integer.MAX_VALUE, max1 = Integer.MIN_VALUE;
        int min2 = Integer.MAX_VALUE, max2 = Integer.MIN_VALUE;
        int min3 = Integer.MAX_VALUE, max3 = Integer.MIN_VALUE;
        int min4 = Integer.MAX_VALUE, max4 = Integer.MIN_VALUE;

        for(int i=0;i<a.length;i++) {
            min1 = Math.min(min1, a[i]+b[i]+i);
            min2 = Math.min(min2, -a[i]+b[i]+i);
            min3 = Math.min(min3, a[i]-b[i]+i);
            min4 = Math.min(min4, -a[i]-b[i]+i);
            max1 = Math.max(max1, a[i]+b[i]+i);
            max2 = Math.max(max2, -a[i]+b[i]+i);
            max3 = Math.max(max3, a[i]-b[i]+i);
            max4 = Math.max(max4, -a[i]-b[i]+i);
        }
        int res = -1;
        res = Math.max(res, max1-min1);
        res = Math.max(res, max2-min2);
        res = Math.max(res, max3-min3);
        res = Math.max(res, max4-min4);
        return res;
    }

    // Inspired by lee's solution, consider it as Manhattan Distance
    //  （https://leetcode.com/problems/maximum-of-absolute-value-expression/discuss/339968/JavaC++Python-Maximum-Manhattan-Distance/311204）
    //   Notice: ||AO|-|BO| <= |AB|, 等式成立的条件是从A到B到O不用绕路（处在一个大方向上）
    public int maxAbsValExpr_v1(int[] a, int[] b) {
        int p[] = {1, -1}, res=0;
        for(int x: p){
            for(int y: p) {
                for(int z: p) {
                    int[] corner = {1000000*x, 1000000*y, 1000000*z};
                    int max=0, min=Integer.MAX_VALUE;
                    for(int i=0;i<a.length;i++) {
                        int dist = Math.abs(corner[0]-a[i]) + Math.abs(corner[1]-b[i])+Math.abs(corner[2]-i);
                        min = Math.min(min, dist);
                        max = Math.max(max, dist);
                    }
                    res = Math.max(res, max-min);
                }
            }
        }
        return res;
    }


    public static void main(String[] as) {
        int[] arr1 = {1,-2,-5,0,10}, arr2 = {0,-2,-1,-7,-4};
        System.out.println(new MaximumOfAbsoluteValueExpression().maxAbsValExpr_v1(arr1, arr2));
    }
}


/*
by uwi:
class Solution {
    public int maxAbsValExpr(int[] a, int[] b) {
        int n = a.length;

        int ans = 0;
        int maxmm = Integer.MIN_VALUE / 2;
        int maxmp = Integer.MIN_VALUE / 2;
        int maxpm = Integer.MIN_VALUE / 2;
        int maxpp = Integer.MIN_VALUE / 2;
        for(int i = 0;i < n;i++){
            maxmm = Math.max(maxmm, -a[i]-b[i]-i);
            maxmp = Math.max(maxmp, -a[i]+b[i]-i);
            maxpm = Math.max(maxpm, a[i]-b[i]-i);
            maxpp = Math.max(maxpp, a[i]+b[i]-i);
            ans = Math.max(ans, maxmm + a[i]+b[i]+i);
            ans = Math.max(ans, maxmp + a[i]-b[i]+i);
            ans = Math.max(ans, maxpm - a[i]+b[i]+i);
            ans = Math.max(ans, maxpp - a[i]-b[i]+i);
        }
        return ans;
    }
}


by lee:
(good! check comment: https://leetcode.com/problems/maximum-of-absolute-value-expression/discuss/339968/JavaC++Python-Maximum-Manhattan-Distance/311204)
Intuition
Take |arr1[i] - arr1[j]| + |arr2[i] - arr2[j]| as Manhattan distance of two points.
arr1 is the coordinate of points on the x-axis,
arr2 is the coordinate of points on the y-axis.

Explanation
For 3 points on the plane, we always have |AO| - |BO| <= |AB|.
When AO and BO are in the same direction, we have ||AO| - |BO|| = |AB|.

We take 4 points for point O, left-top, left-bottom, right-top and right-bottom.
Each time, for each point B, and find the closest A point to O, the Manhattan distance |AB| >= |AO| - |BO|.

Complexity
Time O(N)
Space O(N)


Java:

    public int maxAbsValExpr(int[] x, int[] y) {
        int res = 0, n = x.length, P[] = {-1,1};
        for (int p : P) {
            for (int q : P) {
                int closest = p * x[0] + q * y[0] + 0;
                for (int i = 1; i < n; ++i) {
                    int cur = p * x[i] + q * y[i] + i;
                    res = Math.max(res, cur - closest);
                    closest = Math.min(closest, cur);
                }
            }
        }
        return res;
    }
C++:

    int maxAbsValExpr(vector<int>& x, vector<int>& y) {
        int res = 0, n = x.size(), closest, cur;
        for (int p : {1, -1}) {
            for (int q : {1, -1}) {
                closest = p * x[0] + q * y[0] + 0;
                for (int i = 1; i < n; ++i) {
                    cur = p * x[i] + q * y[i] + i;
                    res = max(res, cur - closest);
                    closest = min(closest, cur);
                }
            }
        }
        return res;
    }
Python:

    def maxAbsValExpr(self, x, y):
        res, n = 0, len(x)
        for p, q in [[1, 1], [1, -1], [-1, 1], [-1, -1]]:
            closest = p * x[0] + q * y[0] + 0
            for i in xrange(n):
                cur = p * x[i] + q * y[i] + i
                res = max(res, cur - closest)
                closest = min(closest, cur)
        return res



by second post:
If u don't get lee's solution, check mine, I am pretty sure u will understand ^ ^

Without loss of generality, we only discuss the scenario when i < j.
below is the discussion about 4 scenarios.

if i < j :
    if arr1[i] > arr1[j]:
        if arr2[i] < arr2[j]:
            value = arr1[i] - arr1[j] + arr2[j] - arr2[i] + j - i
            = (arr1[i] - arr2[i] - i) - (arr1[j] - arr1[j] - j)

        else:
            value = arr1[i] - arr1[j] + arr2[i] - arr2[j] + j - i
            = (arr1[i] + arr2[i] - i) - (arr1[j] + arr2[j] - j)

    elif arr1[i] < arr1[j]:
        if arr2[i] < arr2[j]:
            value = arr1[j] - arr1[i] + arr2[j] - arr2[i] + j - i
            = (arr1[j] + arr2[j] + j) - (arr1[i] + arr2[i] + i)
        else:
            value = arr1[j] - arr1[i] + arr2[i] - arr2[j] + j - i
            = (arr1[j] - arr2[j] + j) - (arr1[i] - arr2[i] + i)
Hence, we only need to consider 4 scenarios to get the maximum.
For the first case, we get the maximum of (arr1[i] - arr2[i] - i) and minimum of (arr1[i] - arr2[i] - i),
then we subtract maximum with minimum, and get the value of possible maximum for the first case.
Then we get the max of 4 cases and return.

def maxAbsValExpr(arr1, arr2):
    max1 = max2 = max3 = max4 = float('-inf')
    min1 = min2 = min3 = min4 = float('inf')

    for i in range(len(arr1)):
        tmp1 = arr1[i] - arr2[i] - i
        max1 = max(max1 , tmp1)
        min1 = min(min1 , tmp1)

        tmp2 = arr1[i] + arr2[i] - i
        max2 = max(max2 , tmp2)
        min2 = min(min2 , tmp2)

        tmp3 = arr1[i] + arr2[i] + i
        max3 = max(max3 , tmp3)
        min3 = min(min3 , tmp3)


        tmp4 = arr1[i] - arr2[i] + i
        max4 = max(max4 , tmp4)
        min4 = min(min4 , tmp4)

    return max((max1 - min1), (max2 - min2),(max3 - min3),(max4 - min4))
*/

/*
We take 4 points for point O, left-top, left-bottom, right-top and right-bottom.

This is the key point to understand the solution.
In the video on Youtube Lee mentioned that imagines that we have these 4 points that are far from all the other points
and they can act as the '0' point and then we can run |AO| - |BO| = |AB|.

The clever part here that the 4 imaginary points are relative and we can simulate them with just the coordinate origin(0,0).
when p,q = [1,1], all points in the first quadrant are counted for max distance.
| * *
| * *
|
(0,0)-----------
when p,q = [1, -1], all points are in the 4th quadrant are counted for distance.
Although there can be points in other quadrants, they will not be able to form a largest sum.
(0,0)-------------
| * *
| * *
|

The rest two quadrants are the same.
This gives us the nice property that if two points are not in the same quadrant at the beginning, they will be in one when p, q is [1, -1] or [-1, 1].
This way as two points are in one quadrant, the absolute value calculation is also redundant.

As for the last part | i - j |, it's covered by p * x[i] + q * y[i] + i
because when we run cur - closest, we get ** i - j ** and since the innermost loop is from 0 to n, i is always greater than j.

Simply elegant!
*/