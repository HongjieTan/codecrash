/*
We distribute some number of candies, to a row of n = num_people people in the following way:

We then give 1 candy to the first person, 2 candies to the second person, and so on until we give n candies to the last person.

Then, we go back to the start of the row, giving n + 1 candies to the first person, n + 2 candies to the second person, and so on until we give 2 * n candies to the last person.

This process repeats (with us giving one more candy each time, and moving to the start of the row after we reach the end) until we run out of candies.
The last person will receive all of our remaining candies (not necessarily one more than the previous gift).

Return an array (of length num_people and sum candies) that represents the final distribution of candies.



Example 1:

Input: candies = 7, num_people = 4
Output: [1,2,3,1]
Explanation:
On the first turn, ans[0] += 1, and the array is [1,0,0,0].
On the second turn, ans[1] += 2, and the array is [1,2,0,0].
On the third turn, ans[2] += 3, and the array is [1,2,3,0].
On the fourth turn, ans[3] += 1 (because there is only one candy left), and the final array is [1,2,3,1].
Example 2:

Input: candies = 10, num_people = 3
Output: [5,2,3]
Explanation:
On the first turn, ans[0] += 1, and the array is [1,0,0].
On the second turn, ans[1] += 2, and the array is [1,2,0].
On the third turn, ans[2] += 3, and the array is [1,2,3].
On the fourth turn, ans[0] += 4, and the final array is [5,2,3].


Constraints:

1 <= candies <= 10^9
1 <= num_people <= 1000


hint: Give candy to everyone each "turn" first [until you can't], then give candy to one person per turn.
*/
package math;

public class DistrubuteCandiesToPeople {

    // brute force: O(sqrt(candies))
    public int[] distributeCandies(int candies, int num_people) {
        int[] res = new int[num_people];
        int p=0, cur=1;
        while(candies>0) {
            int need = candies>cur?cur:candies;
            res[p]+=need;
            candies-=need;
            p=(p+1)%num_people;
            cur++;
        }
        return res;
    }

    // math solution: O(num_people), skip here...
    //  there is math method to calculate the round in O(1) time, check reference solutions in bottom...


    // my first version:
//    public int[] distributeCandies_v0(int candies, int n) {
//        int l=0, r=candies;
//        while(l<=r) {
//            int mid=l+(r-l)/2;
//            int total = (mid+1)*mid/2;
//            if(total == candies) {r=mid;break;}
//            else if(total > candies) r=mid-1;
//            else l=mid+1;
//        }
//        int round = r/n;
//
//        int[] res = new int[n];
//        if(round>0) {
//            for(int i=0;i<n;i++) {
//                res[i] = (i+1+(round-1)*n+i+1)*round/2;
//                candies -= res[i];
//            }
//        }
//
//
//        for(int i=0;i<n;i++) {
//            int needed = round*n+1+i;
//            if(candies<needed) {
//                res[i]+=candies;
//                break;
//            }
//            res[i]+=needed;
//            candies -= needed;
//
//        }
//        return res;
//    }

    public static void main(String[] a) {
        int[] res = new DistrubuteCandiesToPeople().distributeCandies(64,4);
        for(int x: res) System.out.println(x);
    }
}

/*
references: math solution

First check how many complete rounds
then distribute the rest greedy.

class Solution:
    def distributeCandies(self, c: int, n: int) -> List[int]:
        k = math.floor((math.sqrt(1+8*c)-1)/2/n)
        res = [0 for i in range(n)]
        for i in range(n):
            res[i] = (i+1+(k-1)*(n)+i+1)*k/2
        remain = c - sum(res)
        i = 0
        cur = k*n+1
        while remain >= 0:
            res[i] += min(remain,cur)
            remain -= cur

            cur += 1
            i += 1
        return [int(i) for i in res]

Not suggest math solution, but if has to be that:

    def distributeCandies(self, candies, n):
        x = int(math.sqrt(candies * 2 + 0.25) - 0.5)
        res = [0] * n
        for i in xrange(n):
            m = x / n + (x % n > i)
            res[i] = m * (i + 1) + m * (m - 1) / 2 * n
        res[x % n] += candies - x * (x + 1) / 2
        return res
*/