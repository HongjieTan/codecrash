//On an infinite plane, a robot initially stands at (0, 0) and faces north. The robot can receive one of three instructions:
//
//
// "G": go straight 1 unit;
// "L": turn 90 degrees to the left;
// "R": turn 90 degress to the right.
//
//
// The robot performs the instructions given in order, and repeats them forever.
//
// Return true if and only if there exists a circle in the plane such that the robot never leaves the circle.
//
//
//
// Example 1:
//
//
//Input: "GGLLGG"
//Output: true
//Explanation:
//The robot moves from (0,0) to (0,2), turns 180 degrees, and then returns to (0,0).
//When repeating these instructions, the robot remains in the circle of radius 2 centered at the origin.
//
//
// Example 2:
//
//
//Input: "GG"
//Output: false
//Explanation:
//The robot moves north indefinitely.
//
//
// Example 3:
//
//
//Input: "GL"
//Output: true
//Explanation:
//The robot moves from (0, 0) -> (0, 1) -> (-1, 1) -> (-1, 0) -> (0, 0) -> ...
//
//
//
//
// Note:
//
//
// 1 <= instructions.length <= 100
// instructions[i] is in {'G', 'L', 'R'}
//
//
package math;


public class RobotBoundedInCircle {
    public boolean isRobotBounded(String instructions) {
        int x=0, y=0, d=0, dirs[][] = {{0,1},{1,0},{0,-1},{-1,0}};
        for(char c: instructions.toCharArray()) {
            if(c=='R') d = (d+1)%4;
            if(c=='L') d = (d-1+4)%4;
            if(c=='G') { x+=dirs[d][0]; y+=dirs[d][1];}
        }
        return x==0&&y==0 || d!=0;
//        // also work
//        for(int k=0; k<4; k++) {
//            for(char c: instructions.toCharArray()) {
//                if(c=='R') d = (d+1)%4;
//                if(c=='L') d = (d-1+4)%4;
//                if(c=='G') { x+=dirs[d][0]; y+=dirs[d][1];}
//            }
//        }
//        return x==0&&y==0;
    }

    /*

    def isRobotBounded(self, instructions):
        x, y, dx, dy = 0, 0, 0, 1
        for c in instructions:
            if c == 'L': dx, dy = -dy, dx
            if c == 'R': dx, dy = dy, -dx
            if c == 'G': x, y = x+dx, y+dy
        return (x, y) == (0, 0) or (dx, dy) != (0, 1)
     */

    public static void main(String[] args) {
//        解答失败: 测试用例:"GLGLGGLGL" 测试结果:true 期望结果:false
        System.out.println(new RobotBoundedInCircle().isRobotBounded("GLGLGGLGL")); //false
        System.out.println(new RobotBoundedInCircle().isRobotBounded("GLGLGGLGL")); //false
        System.out.println(new RobotBoundedInCircle().isRobotBounded("GGLLGG"));
        System.out.println(new RobotBoundedInCircle().isRobotBounded("GG"));
        System.out.println(new RobotBoundedInCircle().isRobotBounded("GL"));
    }
}