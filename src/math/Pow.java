package math;

/**
 * Created by thj on 04/06/2018.
 *
 *  binary search ... O(logn)
 */
public class Pow {
    public double myPow(double x, int n) {
        if (n==0) return 1d;
        if (n==1) return x;
        if (n==-1) return 1d/x;
        return n%2==0? myPow(x*x, n/2): x * myPow(x*x, (n-1)/2);
    }
}
