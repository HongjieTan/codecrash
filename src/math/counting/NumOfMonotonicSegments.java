package math.counting;

/**
 * Created by thj on 2018/10/1.
 *
 *  给定数组，统计所有单调子串（递增，递减，相等）的数量
 *
 */
public class NumOfMonotonicSegments {

    public int solution(int[] A) {
        // write your code in Java SE 8
        int count = 0, len=0, flag = 0;

        for (int i = 0; i < A.length; i++) {
            ++len;

            // counting point
            if (i==A.length-1 || Integer.compare(A[i+1], A[i]) != flag) {
                int countInSeg = (len-1)*len/2;
                if (countInSeg > 1000000000 - count) return -1;
                count += countInSeg;
                len=1;
            }

            if (i+1<A.length) flag = Integer.compare(A[i+1], A[i]);
        }

        return count;
    }
}
