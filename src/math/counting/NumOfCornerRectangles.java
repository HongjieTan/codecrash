package math.counting;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by thj on 2018/8/10.
 *
 *
 *  - brute force: 一个矩形可由左下和右上两个定点唯一决定，遍历所有这种组合:  四层循环

 *  - 利用排列组合优化: 三层循环
 *
 */
public class NumOfCornerRectangles {

    public int countCornerRectangles(int[][] grid) {
        int m=grid.length, n=grid[0].length;
        int ans=0;
        for(int i=0; i<m; i++) {
            for(int j=i+1; j<m;j++) {
                int count = 0;
                for(int col=0;col<n;col++) {
                    if(grid[i][col]==1 && grid[j][col]==1) count++;
                }
                ans+=count*(count-1)/2;
            }
        }
        return ans;
    }



//    class Pair {
//        int a;
//        int b;
//        Pair(int a, int b) {this.a=a;this.b=b;}
//
//        @Override
//        public int hashCode() {
//            return a*200+b; // row, col max 200
//        }
//
//        @Override
//        public boolean equals(Object obj) {
//            return this.a == ((Pair)obj).a && this.b==((Pair)obj).b;
//        }
//    }
//
//    // O(n * k^2)
//    public int countCornerRectangles(int[][] grid) {
//        Map<Pair, Integer> countMap = new HashMap<>();
//        for (int row=0; row<grid.length; row++) {
//            List<Pair> pairs = findPairsInRow(grid[row]); //O(k^2)
//
//            for (Pair pair: pairs) {
//                countMap.put(pair, countMap.getOrDefault(pair, 0)+1);
//            }
//        }
//        int count=0;
//        for (int val: countMap.values()) count+=(val*(val-1)/2); //!!!!! 注意不是count+=1!!!!!
//        return count;
//    }
//
//
//    // O(n^2 * k^2)  k means max num of 1s in each row, as 1s is limited so it is constant
//    public int countCornerRectangles_v1(int[][] grid) {
//        // Write your code here
//
//        int count=0;
//        for (int row = 0; row < grid.length; row++) {
//            List<Pair> pairs = findPairsInRow(grid[row]); //O(k^2)
//
//            for (int bottomRow=row+1; bottomRow<grid.length; bottomRow++) { // O(n)
//                for (Pair pair: pairs) { // O(k^2)
//                    if (matchInRow(pair, grid[bottomRow])) count++;
//                }
//            }
//        }
//        return count;
//    }
//
//    private boolean matchInRow(Pair pair, int[] row) {
//        return row[pair.a]==1 && row[pair.b]==1;
//    }
//
//    private List<Pair> findPairsInRow(int[] row) {
//        List<Integer> idxs = new ArrayList<>();
//
//        for (int i = 0; i < row.length; i++) {
//            if (row[i]==1) idxs.add(i);
//        }
//
//        List<Pair> pairs = new ArrayList<>();
//        for (int i = 0; i < idxs.size(); i++) {
//            for (int j = i+1; j < idxs.size(); j++) {
//                pairs.add(new Pair(idxs.get(i), idxs.get(j)));
//            }
//        }
//        return pairs;
//
//    }

    public static void main(String[] args) {
        int[][] grid = new int[][] {
                {0,1,0},
                {1,0,1},
                {1,0,1},
                {0,1,0}
        };
//        System.out.println(new NumOfCornerRectangles().countCornerRectangles(grid));
//        System.out.println(new NumOfCornerRectangles().countCornerRectangles_v1(grid));
    }



}
