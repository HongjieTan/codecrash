package math;

/**
 * Created by thj on 04/06/2018.
 *
 *  !!!!! notice n--
 *
 */
public class ExcelSheetColumnTitle {
    public String convertToTitle(int n) {
        StringBuffer sb = new StringBuffer();
        while(n!=0) {
            n--; // Notice here!! every digit start from 'A'
            sb.append((char)('A' + n%26));
            n = n/26;
        }
        return sb.reverse().toString();
    }
}
