//We sampled integers between 0 and 255, and stored the results in an array count: count[k] is the number of integers we sampled equal to k.
//
// Return the minimum, maximum, mean, median, and mode of the sample respectively, as an array of floating point numbers. The mode is guaranteed to be unique.
//
// (Recall that the median of a sample is:
//
//
// The middle element, if the elements of the sample were sorted and the number of elements is odd;
// The average of the middle two elements, if the elements of the sample were sorted and the number of elements is even.)
//
//
//
// Example 1:
// Input: count = [0,1,3,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
//Output: [1.00000,3.00000,2.37500,2.50000,3.00000]
// Example 2:
// Input: count = [0,4,3,2,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
//Output: [1.00000,4.00000,2.18182,2.00000,1.00000]
//
//
// Constraints:
//
//
// count.length == 256
// 1 <= sum(count) <= 10^9
// The mode of the sample that count represents is unique.
// Answers within 10^-5 of the true value will be accepted as correct.
//
//
//. hint: The hard part is the median. Write a helper function which finds the k-th element from the sample.
//
package math;


public class StatisticsFromALargeSample {
    public double[] sampleStats(int[] count) {
        double max=-1, min=Double.MAX_VALUE, sum=0, mean=0, median=0, mode=0, res[]=new double[5];
        int allcount=0, maxcount=0;

        for(int i=0;i<256;i++) {
            if(count[i]!=0) {
                min = Math.min(min, i);
                max = Math.max(max, i);
                sum+= i*count[i];
                maxcount = Math.max(maxcount, count[i]);
                if(maxcount==count[i]) mode=i;
                allcount+=count[i];
            }
        }
        mean=sum*1f/allcount;


        int cur=0, prev=-1;
        for(int i=0;i<256;i++) {
            cur+=count[i];
            if(allcount%2==0) {
                if(prev==-1 && cur==allcount/2) prev=i;
                else if(prev==-1 && cur>allcount/2) {median = (double)i;break;}
                else if(prev!=-1 && cur>allcount/2) {median = (prev+i)/2d;break;}
            } else {
                if(cur>=allcount/2) {median=(double)i;break;}
            }
        }

        res[0]=min;
        res[1]=max;
        res[2]=mean;
        res[3]=median;
        res[4]=mode;
        return res;
    }
}

/*
by uwi:
public double[] sampleStats(int[] count) {
    int n = count.length;
    double min = Double.POSITIVE_INFINITY;
    double max = -1;
    double s = 0;
    int all = 0;
    int mode = -1;
    int best = -1;
    for(int i = 0;i < n;i++){
        if(count[i] > 0){
            min = Math.min(min, i);
            max = Math.max(max, i);
            s += (double)count[i] * i;
            all += count[i];
            if(count[i] > best){
                best = count[i];
                mode = i;
            }
        }
    }
    double med = 0;
    if(all % 2 != 0){
        int u = 0;
        for(int i = 0;i < n;i++){
            u += count[i];
            if(u >= (all+1)/2){
                med = i;
                break;
            }
        }
    }else{
        int u = 0;
        for(int i = 0;i < n;i++){
            u += count[i];
            if(u >= all/2){
                med += i;
                break;
            }
        }
        u = 0;
        for(int i = 0;i < n;i++){
            u += count[i];
            if(u >= all/2+1){
                med += i;
                break;
            }
        }
        med /= 2;
    }
    return new double[]{min, max, s/all, med, mode};
}


by lee:
def sampleStats(self, count):
    n = sum(count)
    mi = next(i for i in xrange(256) if count[i]) * 1.0
    ma = next(i for i in xrange(255, -1, -1) if count[i]) * 1.0
    mean = sum(i * v for i, v in enumerate(count)) * 1.0 / n
    mode = count.index(max(count)) * 1.0
    for i in xrange(255):
        count[i + 1] += count[i]
    median1 = bisect.bisect(count, (n - 1) / 2)
    median2 = bisect.bisect(count, n / 2)
    median = (median1 + median2) / 2.0
    return [mi, ma, mean, median, mode]
*/
