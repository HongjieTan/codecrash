package math;

/**
 * Created by thj on 04/06/2018.
 *
 *
 * TODO: little messy, need to clean code, but it works...
 *
 *
 * careful about 4 cases:
 *  discards all leading whitespaces
 *  sign of the number
 *  overflow
 *  invalid input
 *
 */
public class StringToInteger {
    public int myAtoi(String str) {
        str = str.trim();
        if (str.isEmpty()) return 0;

        char firstC = str.charAt(0);
        if (!Character.isDigit(firstC) && firstC!='+' && firstC!='-') {
            return 0;
        }

        int sign=1, num=0;
        if (!Character.isDigit(firstC)) {
            sign= firstC=='-'?-1:1;
        } else {
            num = firstC-'0';
        }

        int i=1;
        while(i<str.length() && Character.isDigit(str.charAt(i))){

            int x = str.charAt(i)-'0';
            if ((sign>0&&(Integer.MAX_VALUE/(10.0*num+x) < 1))
                    || (sign<0&&(Integer.MAX_VALUE/(Math.abs(10.0*num-1+x) )<1)) ) {
                return sign>0?Integer.MAX_VALUE:Integer.MIN_VALUE;
            }


            num = num*10;
            num += x;
            i++;
        }

        return sign*num;


    }
}
