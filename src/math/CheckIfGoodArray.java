// Given an array nums of positive integers.
// Your task is to select some subset of nums, multiply each element by an integer and add all these numbers.
// The array is said to be good if you can obtain a sum of 1 from the array by any possible subset and multiplicand.
//
// Return True if the array is good otherwise return False.
//
//
// Example 1:
//
//
// Input: nums = [12,5,7,23]
// Output: true
// Explanation: Pick numbers 5 and 7.
// 5*3 + 7*(-2) = 1
//
//
// Example 2:
//
//
// Input: nums = [29,6,10]
// Output: true
// Explanation: Pick numbers 29, 6 and 10.
// 29*1 + 6*(-3) + 10*(-1) = 1
//
//
// Example 3:
//
//
//Input: nums = [3,6]
//Output: false
//
//
//
// Constraints:
//
//
// 1 <= nums.length <= 10^5
// 1 <= nums[i] <= 10^9
//
//
package math;


// Bezout's identity — Let a and b be integers with greatest common divisor d. Then, there exist integers x and y such that ax + by = d.
public class CheckIfGoodArray {
    public boolean isGoodArray(int[] nums) {
        int g = nums[0];
        for(int x:nums) {
            g=gcd(g,x);
            if(g==1) return true;
        }
        return false;
    }
    int gcd(int m, int n) {
        if(n==0) return m;
        return gcd(n, m%n);
    }

    public static void main(String[] as) {
        System.out.println(new CheckIfGoodArray().isGoodArray(new int[]{12,5,7,23}));
        System.out.println(new CheckIfGoodArray().isGoodArray(new int[]{29,6,10}));
        System.out.println(new CheckIfGoodArray().isGoodArray(new int[]{3,6}));
    }


}
