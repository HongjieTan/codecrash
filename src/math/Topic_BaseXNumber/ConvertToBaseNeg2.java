//Given a number N, return a string consisting of "0"s and "1"s that represents its value in base -2 (negative two).
//
// The returned string must have no leading zeroes, unless the string is "0".
//
//
//
//
// Example 1:
//
//
//Input: 2
//Output: "110"
//Explantion: (-2) ^ 2 + (-2) ^ 1 = 2
//
//
//
// Example 2:
//
//
//Input: 3
//Output: "111"
//Explantion: (-2) ^ 2 + (-2) ^ 1 + (-2) ^ 0 = 3
//
//
//
// Example 3:
//
//
//Input: 4
//Output: "100"
//Explantion: (-2) ^ 2 = 4
//
//
//
//
// Note:
//
//
// 0 <= N <= 10^9
//
//
//
//
// hint Figure out whether you need the ones digit placed or not, then shift by two.
package math.Topic_BaseXNumber;



// refs: https://en.wikipedia.org/wiki/Negative_base#Calculation
public class ConvertToBaseNeg2 {
    public String baseNeg2(int N) {
        StringBuilder sb = new StringBuilder();
        int base = -2;
        while(N!=0) {
            int remain = N%base;
            N /= base;
            if(remain<0) {  //  x = base * d + remain = base *(d+1) + (remain-base)
                remain-=base;
                N++;
            }
            sb.append(remain);
        }
        return sb.length()>0?sb.reverse().toString(): "0";
    }
}
