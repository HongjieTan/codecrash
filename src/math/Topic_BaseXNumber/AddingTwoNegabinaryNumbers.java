//Given two numbers arr1 and arr2 in base -2, return the result of adding them together.
//
// Each number is given in array format: as an array of 0s and 1s, from most significant bit to least significant bit.
// For example, arr = [1,1,0,1] represents the number (-2)^3 + (-2)^2 + (-2)^0 = -3.
// A number arr in array format is also guaranteed to have no leading zeros: either arr == [0] or arr[0] == 1.
//
// Return the result of adding arr1 and arr2 in the same format: as an array of 0s and 1s with no leading zeros.
//
//
//
// Example 1:
//
//
//Input: arr1 = [1,1,1,1,1], arr2 = [1,0,1]
//Output: [1,0,0,0,0]
//Explanation: arr1 represents 11, arr2 represents 5, the output represents 16.
//
//
//
//
// Note:
//
//
// 1 <= arr1.length <= 1000
// 1 <= arr2.length <= 1000
// arr1 and arr2 have no leading zeros
// arr1[i] is 0 or 1
// arr2[i] is 0 or 1
//
// Hint: We can try to determine the last digit of the answer, then divide everything by 2 and repeat.

package math.Topic_BaseXNumber;
import java.util.LinkedList;

public class AddingTwoNegabinaryNumbers {
    // refs: https://en.wikipedia.org/wiki/Negative_base#Additionx
    public int[] addNegabinary(int[] arr1, int[] arr2) {
        LinkedList<Integer> res = new LinkedList<>();
        int i=arr1.length-1, j=arr2.length-1, carry=0;
        while (i>=0 || j>=0 || carry!=0) {
            int sum = 0;
            if(i>=0) sum += arr1[i--];
            if(j>=0) sum += arr2[j--];
            sum += carry;  // sum can be -1,0,1,2,3

            res.addFirst(Math.abs(sum%2));
            carry = (sum < 0) ? 1 : (sum > 1) ? -1 : 0;
        }
        while(res.size()>1 && res.getFirst()==0) res.removeFirst(); //  remove leading 0s
        return res.stream().mapToInt(x->x).toArray();
    }

    /*
    def addNegabinary(self, A, B):
        res = []
        carry = 0
        while A or B or carry != 0:
            sum = (A.pop() if A else 0) + (B.pop() if B else 0)
            sum += carry

            res.append(abs(sum % 2))
            carry = 1 if sum < 0 else (0 if sum < 2 else -1)

        while len(res) > 1 and res[-1] == 0: res.pop()
        return res[::-1]
    */
}