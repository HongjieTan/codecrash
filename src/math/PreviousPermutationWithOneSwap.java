package math;

public class PreviousPermutationWithOneSwap {
    public int[] prevPermOpt1(int[] A) {
        int n = A.length;

        int l = n-1, min = 10001;
        while(l>=0) {
            if(A[l]>min)  break;
            min = A[l];
            l--;
        }

        if(l==-1) return A;

        int r = n-1;
        while(r>l) {
            if(A[l]>A[r]) break;
            r--;
        }

        swap(A, l, r);
        return A;

    }

    void swap(int[] A, int a, int b){
        int tmp = A[a];
        A[a] = A[b];
        A[b] = tmp;
    }
}
