package math;

/**
 * Created by thj on 04/06/2018.
 *
 *  O(logn)
 *
 */
public class FactorialTrailingZeroes {
    public int trailingZeroes(int n) {
        int count = 0;
        while(n>0) {
            count+=n/5;
            n=n/5;
        }
        return count;
    }
}
