package math;

/**
 * Created by thj on 03/06/2018.
 *
 *
 * gcd: greatest common divisor, 最大公约数
 * lcm: least common multiple,   最小公倍数
 *
 */
public class gcd_lcm {

    public int lcm(int m, int n) {
        return m*n/gcd(m,n);
    }

    public int gcd(int m, int n) {
        if (n==0) return m;
        return gcd(n, m%n);
    }


    public int gcd_v2(int m, int n) {
        int temp;
        while(n!=0){
            temp = n;
            n = m%n;
            m = temp;
        }
        return m;
    }
}
