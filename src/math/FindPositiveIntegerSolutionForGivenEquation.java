//Given a function f(x, y) and a value z, return all positive integer pairs x and y where f(x,y) == z.
//
// The function is constantly increasing, i.e.:
//
//
// f(x, y) < f(x + 1, y)
// f(x, y) < f(x, y + 1)
//
//
// The function interface is defined like this:
//
//
//interface CustomFunction {
//public:
//  // Returns positive integer f(x, y) for any given positive integer x and y.
//  int f(int x, int y);
//};
//
//
// For custom testing purposes you're given an integer function_id and a target z as input, where function_id represent one function from an secret internal list, on the examples you'll know only two functions from the list.
//
// You may return the solutions in any order.
//
//
// Example 1:
//
//
//Input: function_id = 1, z = 5
//Output: [[1,4],[2,3],[3,2],[4,1]]
//Explanation: function_id = 1 means that f(x, y) = x + y
//
// Example 2:
//
//
//Input: function_id = 2, z = 5
//Output: [[1,5],[5,1]]
//Explanation: function_id = 2 means that f(x, y) = x * y
//
//
//
// Constraints:
//
//
// 1 <= function_id <= 9
// 1 <= z <= 100
// It's guaranteed that the solutions of f(x, y) == z will be on the range 1 <= x, y <= 1000
// It's also guaranteed that f(x, y) will fit in 32 bit signed integer if 1 <= x, y <= 1000
//
// Related Topics Math Binary Search


package math;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

// This is the custom function interface.
// You should not implement it, or speculate about its implementation
interface CustomFunction {
    // Returns f(x, y) for any given positive integers x and y.
    // Note that f(x, y) is increasing with respect to both x and y.
    // i.e. f(x, y) < f(x + 1, y), f(x, y) < f(x, y + 1)
    public int f(int x, int y);
}

public class FindPositiveIntegerSolutionForGivenEquation {
    // O(x+y)
    public List<List<Integer>> findSolution(CustomFunction customfunction, int z) {
        List<List<Integer>> ret = new ArrayList<>();
        int x=1,y=1000;
        while(x<=1000 && y>=1) {
            int v = customfunction.f(x,y);
            if(v==z) {
                ret.add(Arrays.asList(x,y));
                x++;
                y--;
            } else if(v>z) {
                y--;
            } else {
                x++;
            }
        }
        return ret;
    }

    // O(Cx + logy), ...
}


/*
by lee:
Intuition
Rephrase the problem:
Given a matrix, each row and each column is increasing.
Find all coordinates (i,j) that A[i][j] == z

Complexity
In binary search,
time complexity is O(XlogY) or O(YlogX)

In this solution,
time complexity is stable O(X + Y).

Bianry search is really an unnecessay operation,
and it won't help improve the conplexity at all.

Space is O(X)

Java

    public List<List<Integer>> findSolution(CustomFunction customfunction, int z) {
        List<List<Integer>> res = new ArrayList<>();
        int x = 1, y = 1000;
        while (x <= 1000 && y > 0) {
            int v = customfunction.f(x, y);
            if (v > z) --y;
            else if (v < z) ++x;
            else res.add(Arrays.asList(x++, y--));
        }
        return res;
    }
C++:

    vector<vector<int>> findSolution(CustomFunction& customfunction, int z) {
        vector<vector<int>> res;
        int y = 1000;
        for (int x = 1; x <= 1000; ++x) {
            while (y > 1 && customfunction.f(x,y) > z) y--;
            if (customfunction.f(x,y) == z)
                res.push_back({x, y});
        }
        return res;
    }
Python

    def findSolution(self, customfunction, z):
        res = []
        y = 1000
        for x in xrange(1, 1001):
            while y > 1 and customfunction.f(x, y) > z:
                y -= 1
            if customfunction.f(x, y) == z:
                res.append([x, y])
        return res

by second post:
Method 1: Linear time complexity
Time: O(x + y), space: O(1) excluding return list.

    public List<List<Integer>> findSolution(CustomFunction customFunction, int z) {
        List<List<Integer>> ans = new ArrayList<>();
        int x = 1, y = 1000;
        while (x < 1001 && y > 0) {
            int val = customFunction.f(x, y);
            if (val < z) {
                ++x;
            }else if (val > z) {
                --y;
            }else {
                ans.add(Arrays.asList(x++, y--));
            }
        }
        return ans;
    }
    def findSolution(self, customfunction: 'CustomFunction', z: int) -> List[List[int]]:
        x, y, ans = 1, 1000, []
        while x < 1001 and y > 0:
            val = customfunction.f(x, y)
            if val > z:
                y -= 1
            elif val < z:
                x += 1
            else:
                ans += [[x, y]]
                x += 1
                y -= 1
        return ans
Method 2: Binary Search
Time: O((xlogy), space: O(1) excluding return list.

    public List<List<Integer>> findSolution(CustomFunction customFunction, int z) {
        List<List<Integer>> ans = new ArrayList<>();
        for (int x = 1; x < 1001; ++x) {
            int l = 1, r = 1001;
            while (l < r) {
                int y = (l + r) / 2;
                if (customFunction.f(x, y) < z) {
                    l = y + 1;
                }else {
                    r = y;
                }
            }
            if (customFunction.f(x, l) == z) {
                ans.add(Arrays.asList(x, l));
            }
        }
        return ans;
    }
    def findSolution(self, customfunction: 'CustomFunction', z: int) -> List[List[int]]:
        ans = []
        for x in range(1, 1001):
            l, r = 1, 1001
            while l < r:
                y = (l + r) // 2
                if customfunction.f(x, y) < z:
                    l = y + 1;
                else:
                    r = y
            if  customfunction.f(x, l) == z:
                ans += [[x, l]]
        return ans

Method 3: Optimized Binary Search
left and right boundaries change according to result of previous binary search; after first round of binary search,
which cost logy, the remaining binary search only cost amortized O(1) (Denote its average as C) for each value of x.
Therefore the time cost O(Cx + logy)

Time: O(x + logy), space: O(1) excluding return list.

    public List<List<Integer>> findSolution(CustomFunction customFunction, int z) {
        List<List<Integer>> ans = new ArrayList<>();
        int left = 1, right = 1001;
        for (int x = 1; x < 1001; ++x) {
            int l = left, r = right;
            while (l < r) {
                int y = (l + r) / 2;
                if (customFunction.f(x, y) < z) {
                    l = y + 1;
                }else {
                    r = y;
                }
            }
            int val = customFunction.f(x, l);
            if (val >= z) {
                if (val == z) ans.add(Arrays.asList(x, l));
                right = l;
            }else {
                left = l;
            }
        }
        return ans;
    }
    def findSolution(self, customfunction: 'CustomFunction', z: int) -> List[List[int]]:
        left, right, ans = 1, 1001, []
        for x in range(1, 1001):
            l, r = left, right
            while l < r:
                y = (l + r) // 2
                if customfunction.f(x, y) < z:
                    l = y + 1;
                else:
                    r = y
            val = customfunction.f(x, l)
            if val >= z:
                ans += [[x, l]] if val == z else []
                right = l
            else:
                left = l
        return ans

*/