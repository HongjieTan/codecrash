package math.Topic_Date;

public class DayOfSTD {

    int[] mdays = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    // count days from 0000-00-00 to yyyy-mm-dd
    int dayOfStd(int y, int m, int d) {
        int res = 0;

        // y-1 years
        res += (y-1)*365 + (y-1)/4 - (y-1)/100 + (y-1)/400;

        // y-th year
        for(int i=0;i<m-1;i++) res+=mdays[i];
        if (m>=3 && ((y%4==0 && y%100!=0) || y%400==0)) res++;
        res += d;
        return res;  // or res-1, it depends...
    }
}

/*
vector<string> W{"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};
    string dayOfTheWeek(int day, int month, int year) {
        if (month < 3)
        {
            month += 12;
            year--;
        }
        int yobi = (year + year / 4 - year / 100 + year / 400 + (13 * month + 8) / 5 + day) % 7;
        return W[yobi];
    }
*/
