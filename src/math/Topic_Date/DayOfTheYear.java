//Given a string date representing a Gregorian calendar date formatted as YYYY-MM-DD, return the day number of the year.
//
//
// Example 1:
//
//
//Input: date = "2019-01-09"
//Output: 9
//Explanation: Given date is the 9th day of the year in 2019.
//
//
// Example 2:
//
//
//Input: date = "2019-02-10"
//Output: 41
//
//
// Example 3:
//
//
//Input: date = "2003-03-01"
//Output: 60
//
//
// Example 4:
//
//
//Input: date = "2004-03-01"
//Output: 61
//
//
//
// Constraints:
//
//
// date.length == 10
// date[4] == date[7] == '-', and all other date[i]'s are digits
// date represents a calendar date between Jan 1st, 1900 and Dec 31, 2019.
//
package math.Topic_Date;

public class DayOfTheYear {

    public int dayOfYear_v2(String date) {
        String[] parts = date.split("-");
        int y = Integer.valueOf(parts[0]);
        int m = Integer.valueOf(parts[1]);
        int d = Integer.valueOf(parts[2]);
        return dayOfStd(y,m,d)-dayOfStd(y,1,1)+1;
    }
    int dayOfStd(int y, int m, int d) {
        int[] mdays = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        int ret = 0;
        ret += (y-1)*365 + (y-1)/4 - (y-1)/100 + (y-1)/400;
        for (int i = 0; i < m-1; i++) ret += mdays[i];
        if (m>=3 && ((y%4==0 && y%100!=0)|| y%400==0)) ret++;
        ret+=d;
        return ret;
    }

    public int dayOfYear(String date) {
        String[] parts = date.split("-");
        int y = Integer.valueOf(parts[0]);
        int m = Integer.valueOf(parts[1]);
        int d = Integer.valueOf(parts[2]);
        int[] days = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        if((y%4==0 && y%100!=0) || y%400==0) ++ days[1];

        int res = 0;
        for(int i=0;i<m-1;i++) res += days[i];
        res+=d;
        return res;
    }

    public static void main(String[] as) {
        System.out.println(new DayOfTheYear().dayOfYear("2019-02-10"));
        System.out.println(new DayOfTheYear().dayOfYear_v2("2019-02-10"));
    }




}

/*
by uwi:
class Solution {
    public int dayOfYear(String date) {
        String[] s = date.split("-");
        int y = Integer.valueOf(s[0]);
        int m = Integer.valueOf(s[1]);
        int d = Integer.valueOf(s[2]);
        return enc(y, m, d) - enc(y, 1, 1) + 1;
    }

    public int[] DOM = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    public int[] CUMDOM = {0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334};

    public int[] DOM_LEAP = {31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    public int[] CUMDOM_LEAP = {0, 31, 60, 91, 121, 152, 182, 213, 244, 274, 305, 335};

    public int enc(int y, int m, int d)
    {
        int ret = 0;
        ret += (y-1) * 365;
        ret += (y-1) / 4;
        ret -= (y-1) / 100;
        ret += (y-1) / 400;
        ret += CUMDOM[m-1];
        if(m >= 3 && y % 4 == 0 && (y % 400 == 0 || y % 100 != 0)){
            ret++;
        }
        ret += d;
        return ret-1;
    }

}

*/
