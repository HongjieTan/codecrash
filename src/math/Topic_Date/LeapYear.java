package math.Topic_Date;

public class LeapYear {


    // 四年一闰 百年不闰 四百年又闰
    public boolean isLeapYear(int year) {
        return (year % 4 == 0 && year % 100 != 0) || year % 400 ==0;
    }
}
