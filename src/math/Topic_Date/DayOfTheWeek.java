//Given a date, return the corresponding day of the week for that date.
//
// The input is given as three integers representing the day, month and year respectively.
//
// Return the answer as one of the following values {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"}.
//
//
// Example 1:
//
//
//Input: day = 31, month = 8, year = 2019
//Output: "Saturday"
//
//
// Example 2:
//
//
//Input: day = 18, month = 7, year = 1999
//Output: "Sunday"
//
//
// Example 3:
//
//
//Input: day = 15, month = 8, year = 1993
//Output: "Sunday"
//
//
//
// Constraints:
//
//
// The given dates are valid dates between the years 1971 and 2100.
// Related Topics Array
package math.Topic_Date;

public class DayOfTheWeek {
    String[] weeks = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};
    public String dayOfTheWeek(int day, int month, int year) {
        int diff = daysFromStd(year, month, day) - daysFromStd(2019, 9, 9); // 2019-09-09: Monday 1
        diff = diff%7;
        if(diff<0) diff+=7; // notice the diff between java and python
        return weeks[(1+diff)%7];
    }

    int[] M = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    int daysFromStd(int y, int m, int d) {
        int days = (y-1)*365 + (y-1)/4 - (y-1)/100 + (y-1)/400;

        for (int i = 0; i < m-1; i++) days+=M[i];
        if(m>2 && ((y%4==0 && y%100!=0)||y%400==0)) days++;

        days+=d;
        return days;
    }

    public static void main(String[] a) {
//        System.out.println(new DayOfTheWeek().dayOfTheWeek(31,8,2019));
//        System.out.println((-9+7000)%7);
//        y = x + mod*n   =>    (y-x)/mod
    }
}
