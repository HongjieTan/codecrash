package math;

/**
 * Created by thj on 04/06/2018.
 */
public class ExcelSheetColumnNumber {
    public int titleToNumber(String s) {
        int num = 0;
        for (int i=0;i<s.length();i++) {
            int x = s.charAt(i) - 'A' + 1;
            num = num*26;
            num += x;
        }
        return num;
    }
}
