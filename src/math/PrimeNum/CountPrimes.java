package math.PrimeNum;

import java.util.Arrays;

/**
 * Created by thj on 2018/8/21.
 */
public class CountPrimes {


    public int countPrimes(int n) {
        boolean[] isPrime = new boolean[n];
        Arrays.fill(isPrime, true);

        int count=0;
        for (int i = 2; i < n; i++) {
            if (isPrime[i]) {
                count++;
                for (int j=2; i*j<n; j++) isPrime[i*j]=false;
            }
        }
        return count;
    }

}
