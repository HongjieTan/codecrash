package math.PrimeNum;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by thj on 2018/8/22.
 */
public class CountPrimeSetBits {

    public int countPrimeSetBits(int L, int R) {
        Set<Integer> primes = new HashSet<>(Arrays.asList(2, 3, 5, 7, 11, 13, 17, 19)); // R≤10^​6<2^20
        int count=0;
        for (int i = L; i <= R; i++) {
            if (primes.contains(bitsOf(i))) count++;
        }
        return count;
    }


    private int bitsOf(int n) {
//        return Integer.bitCount(n);

        int bits=0;
        while (n!=0) {
            ++bits;
            n = n &(n-1);
        }
        return bits;
    }




}
