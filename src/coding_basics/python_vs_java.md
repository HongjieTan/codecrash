### basic data structure:

list, linkedlist, queue, stack, heap(pq), hashmap(hashset), treemap(treeset),


### Array
sum/max/min:
java:
    int sum=0
    for(int x: arr) sum+=x
python:
    sum(arr), min(arr), max(arr)


### String
substring:
java:   str.substring(0, k), str.substring(k)
python: str[:k], str[k:]


#### Set
diff:
java: set1.removeAll(set2)
python: set1 - set2 , set1.difference(set2)

union:
java: set1.addAll(set2)
python: set1 | set2 , set1.union(set2)

intersection:
java: set1.retainAll(set2)
python: set1 & set2 , set1.intersection(set2)


#### Map
if key not exist:
java:
    Map<String, List<Integer>> map = new HashMap<>();
    List<Integer> v = map.getOrDefault("xx", new ArrayList<>())
python:
    map = collections.defaultdict(list)
    v = map["xx"]

    map = dict()
    v = map.get("xx", [])


#### Queue, Stack
java:
    Stack<Integer> st = new Stack<>();
    st.push(1);
    st.pop();
    int top = st.peek();
    Queue<Integer> q = new LinkedList<>();
    q.add(1);
    q.remove();
    q.peek();

py: (use built-in list)
    st = []
    st.append(1)
    st.pop()
    top = st[len(st)-1]
    q = []
    q.append(i)
    top = q[0]
