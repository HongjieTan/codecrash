### <<design data intensive applications>>
PART I. Foundations of Data Systems
1. Reliable, Scalable, and Maintainable Applications : 0417 x1
2. Data Models and Query Languages: 4029 x1
3. Storage and Retrieval: 0429-0507 x1
4. Encoding and Evolution:  0507-0513 x1
PART II. Distributed Data
5. Replication  0513-0522 x1
6. Partitioning   0522-0529 x1
7. Transactions     0529-0613   x1
8. The Trouble with Distributed Systems     0613-0617  x1
9. Consistency and Consensus    0617-0626  x1
PART III. Derived Data
10. Batch Processing    0626-0704   x1
11. Stream Processing   0704-0712   x1
12. The Future of Data System   0712-0729  x1


### questions：
- how is transaction implemented ?
- how is 2-phrase-commit implemented ?
- how is cassandra nodes organized ?


### <<google三架马车>>
- todo


### 一些推荐资料：
1. http://www.mitbbs.com/article_t/JobHunting/32777529.html
2. https://github.com/donnemartin/system-design-primer
https://leetcode.com/discuss/interview-question/124804/design-pastebin


### Topic: Migrate database without downtime
several solutions:
- https://www.quora.com/How-do-you-migrate-database-data-without-any-downtime
- <<8 Steps to Safely Migrate a Database Without Downtime>> (http://www.aviransplace.com/2015/12/15/safe-database-migration-pattern-without-downtime/)
- <<DDIA>> charper5#Leaders and Followers#Setting Up New Followers, P155
- <<DDIA>> P497

### Topic: Migrate database schema without downtime?
- ...


#### Topic: Design Deduplicator
- 已经有uuid来标识每个job, 现在有一帮子pull job 的worker。想设计一个deduper来让worker识别不要去处理已经处理过的job。
some solutions:
- https://segment.com/blog/exactly-once-delivery/
-

#### problems
    Design: TinyURL – A URL Shorterner Service         
    Design: Uber Backend         
    Design twitter news feed
    Design web crawler         
    Design K/V DB         
    Design: Google Suggestion Service         
    Design a payment processor         
    Design a voice conference system         
    Design slack         
    Design google doc         
    Design gmail         
    Design instagram, a photo sharing app         
    Design Yelp, a location-based system         
    Design an API gateway         
    Design amazon book recommendation system         
    Google autocomplete         
    Design: A URL Redirecting Feature         
    Design Google PageRank         
    Design messaging/notification system         
    Design search post system         
    Design memcache/redis         
    Design typeahead         
    Design Google Adsense fraud detection         
    Design Pastebin.com         
    Design Mint.com         
    Design leetcode