# Two commonly seen classifiers

## SVM:
    - Multiclass Support Vector Machine loss (SVM loss):
        The SVM loss is set up so that the SVM “wants” the correct class for each image to a have a score higher than the incorrect classes by some fixed margin Δ

    - Assume i-th example X[i], label y[i], score for j-th class is s[j] = fj(x[i], W), the svm loss for X[i] is:
           Loss[i] = sum(max(0, s[j]-s[y[i]]) | j!=y[i])
      Example:
      Suppose that we have three classes that receive the scores s=[13,−7,11], and that the first class is the true class (i.e. yi=0)
      Also assume that Δ (a hyperparameter we will go into more detail about soon) is 10.
           Loss[i] = max(0,−7−13+10)+max(0,11−13+10) = 0 + 8

    - hinge loss:
        hinge loss:         max(0,−)
        squared hinge loss: max(0,−)^2

    - summary:
        http://cs231n.github.io/assets/margin.jpg
        The Multiclass Support Vector Machine "wants" the score of the correct class to be higher than all other scores by at least a margin of delta.
        If any class has a score inside the red region (or higher), then there will be accumulated loss. Otherwise the loss will be zero.

    - regulation loss:
        R(W)= sum(w[i][j]^2)

    - Full SVM loss version:
        Loss = data loss + regularization loss = 1/N * sum(Loss[i] for i=all...) + λR(W)

    - Practical Considerations: to read, http://cs231n.github.io/linear-classify/#svm


## Softmax:
    - If you’ve heard of the binary Logistic Regression classifier before, the Softmax classifier is its generalization to multiple classes
    - Outputs meaning:
        - SVM classifier:  scores for each class which maybe difficult to interpret
        - Softmax classifier:  probabilities for each class which sum to one

    - cross-entropy loss : (y[i] is the true class)
        Loss[i] = -log(pi) = -log(e^s[y[i]]/sum(e^s[j] for all j))  or  Loss[i] = -s[y[i]] + log(sum(s[j] for j=all...))

    - softmax function: (is just the squashing function)
        - Assume fj to mean the j-th element of the vector of class scores f
        - fj(z) = e^z[j] / sum(e^z[k] for all k) = pj
        - It takes a vector of arbitrary real-valued scores (in z) and squashes it to a vector of values between zero and one that sum to one.

    - Information theory view:
        - cross-entropy between a “true” distribution p and an estimated distribution q is defined as:
            H(p,q)=−sum( p(x)log(q(x)) for all x )
        - The Softmax classifier is hence minimizing the cross-entropy between the estimated class probabilities ( q=efyi/∑jefj as seen above) and the “true” distribution, which in this interpretation is the distribution where all probability mass is on the correct class (i.e. p=[0,…1,…,0] contains a single 1 at the yi -th position.).

    - Practical issues:
        - softmax(X) = softmax(X + c)
        - Numeric stability:
            f = np.array([123, 456, 789]) # example with 3 classes and each having large scores
            p = np.exp(f) / np.sum(np.exp(f)) # Bad: Numeric problem, potential blowup

            # instead: first shift the values of f so that the highest number is 0:
            f -= np.max(f) # f becomes [-666, -333, 0]
            p = np.exp(f) / np.sum(np.exp(f)) # safe to do, gives the correct answer

    - python code: refs https://deepnotes.io/softmax-crossentropy
        def softmax(X):
            exps = np.exp(X)
            return exps / np.sum(exps)

        def stable_softmax(X):
            exps = np.exp(X - np.max(X))
            return exps / np.sum(exps)

        def cross_entropy(X,y):
            """
            X is the output from fully connected layer (num_examples x num_classes)
            y is labels (num_examples x 1)
            	Note that y is not one-hot encoded vector.
            	It can be computed as y.argmax(axis=1) from one-hot encoded vectors of labels if required.
            """
            m = y.shape[0]
            p = softmax(X)
            # We use multidimensional array indexing to extract
            # softmax probability of the correct label for each sample.
            # Refer to https://docs.scipy.org/doc/numpy/user/basics.indexing.html#indexing-multi-dimensional-arrays for understanding multidimensional array indexing.
            log_likelihood = -np.log(p[range(m),y])
            loss = np.sum(log_likelihood) / m
            return loss


# text classification
    - tfidf
    - bilstm
    - bert + fine tuning
