/*
Given an array of integers nums and a positive integer k, find whether it's possible to divide this array into sets of k consecutive numbers
Return True if its possible otherwise return False.



Example 1:

Input: nums = [1,2,3,3,4,4,5,6], k = 4
Output: true
Explanation: Array can be divided into [1,2,3,4] and [3,4,5,6].
Example 2:

Input: nums = [3,2,1,2,3,4,3,4,5,9,10,11], k = 3
Output: true
Explanation: Array can be divided into [1,2,3] , [2,3,4] , [3,4,5] and [9,10,11].
Example 3:

Input: nums = [3,3,2,2,1,1], k = 3
Output: true
Example 4:

Input: nums = [1,2,3,4], k = 3
Output: false
Explanation: Each array should be divided in subarrays of size 3.


Constraints:

1 <= nums.length <= 10^5
1 <= nums[i] <= 10^9
1 <= k <= nums.length
*/

package _Topic_ConsecutiveValueSeq;

import java.util.*;

// #greedy #array
public class DivideArrayInSetsOfKConsecutiveNumbers {
    public boolean isPossibleDivide(int[] nums, int k) {
        int n = nums.length;
        if(n%k!=0) return false;
        Arrays.sort(nums);

        Map<Integer, Integer> count = new HashMap<>();
        for(int x: nums) count.put(x, count.getOrDefault(x,0)+1);

        for(int x: nums) {
            if (count.get(x)>0) {
                for (int j=0;j<k;j++) {
                    if (!count.containsKey(x+j)) return false;
                    if (count.get(x+j)<=0) return false;
                    count.put(x+j, count.get(x+j)-1);
                }
            }
        }
        return true;
    }

//    public boolean isPossibleDivide_v1(int[] nums, int k) {
//        int n = nums.length;
//        if(n%k!=0) return false;
//        Arrays.sort(nums);
//
//        Map<Integer, Queue<Integer>> need = new HashMap<>();
//        Map<Integer, Integer> count = new HashMap<>();
//
//        for(int i=0;i<n;i++) {
//            int v = nums[i];
//            if(!need.containsKey(v) || need.get(v).isEmpty()) { // start new seq
//                need.putIfAbsent(v+1, new LinkedList<>());
//                need.get(v+1).add(i);
//                count.putIfAbsent(i, 0);
//                count.put(i, count.get(i)+1);
//            } else { // append to existing seq
//                int start = need.get(v).peek();
//                count.put(start, count.get(start)+1);
//                need.get(v).remove();
//                if(count.get(start)!=k) {
//                    need.putIfAbsent(v+1, new LinkedList<>());
//                    need.get(v+1).add(start);
//                }
//            }
//        }
//
//        for(int v: count.values()) {
//            if(v!=k) return false;
//        }
//
//        return true;
//    }
}


/*
by uwi:
public boolean isPossibleDivide(int[] nums, int k) {
    int n = nums.length;
    if(n % k != 0){
        return false;
    }
    Arrays.sort(nums);

    Map<Integer, Integer> map = new HashMap<>();
    for(int v : nums){
        if(map.containsKey(v)){
            map.put(v, map.get(v)+1);
        }else{
            map.put(v, 1);
        }
    }

    for(int i = 0;i < n;i++){
        if(map.get(nums[i]) > 0){
            for(int j = 0;j < k;j++){
                if(!map.containsKey(nums[i]+j)){
                    return false;
                }
                if(map.get(nums[i]+j) <= 0){
                    return false;
                }
                map.put(nums[i] + j, map.get(nums[i]+j)-1);
            }
        }
    }
    return true;
}
*/