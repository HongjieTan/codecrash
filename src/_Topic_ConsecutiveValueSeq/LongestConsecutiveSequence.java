package _Topic_ConsecutiveValueSeq;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 *
 *  - union-find(path compression and union by size/rank): O(n) !!!!
 *  - hashtable: O(n) !! HashSet and Intelligent Sequence Building !!!
 *
 *  - sort: O(nlogn)
 *
 *
 */
public class LongestConsecutiveSequence {

    // union-find
    Map<Integer, Integer> parents = new HashMap<>();
    Map<Integer, Integer> count = new HashMap<>();
    int maxCount = 0;
    private int find(int x) {
        if(x!=parents.get(x)) parents.put(x, find(parents.get(x))); // path compression
        return parents.get(x);
    }
    private void union(int x, int y) {
        int xroot = find(x);
        int yroot = find(y);
        if (xroot!=yroot) {
            int newCount = count.get(xroot)+count.get(yroot);
            if (count.get(xroot) >  count.get(yroot)) {  // union by size(or by rank for another option)
                parents.put(xroot, yroot);
                count.put(yroot, newCount);
            } else {
                parents.put(yroot, xroot);
                count.put(xroot, newCount);
            }
            maxCount = Math.max(maxCount, newCount);
        }
    }
    public int longestConsecutive(int[] nums) {
        // makeset
        for (int num: nums) {
            parents.put(num, num);
            count.put(num, 1);
            maxCount = 1;
        }
        // union all consecutive num
        for (int num: nums) {
            if (parents.containsKey(num+1)) union(num, num+1);
            if (parents.containsKey(num-1)) union(num, num-1);
        }
        return maxCount;
    }

    // hashtable  good!!
    public int longestConsecutive_v1(int[] nums) {
        Set<Integer> set = new HashSet<>();
        for (int num: nums) set.add(num);
        int maxLen = 0;
        for (int i = 0; i < nums.length; i++) {  // Notice: 最内层的while loop会只执行n次，所以复杂度是O(n) !
            if (!set.contains(nums[i]-1)) {
                int curNum = nums[i];
                int curLen = 1;
                while (set.contains(curNum+1)) {
                    curNum++;
                    curLen++;
                }
                maxLen = Math.max(maxLen, curLen);
            }
        }
        return maxLen;
    }

    public static void main(String[] args) {
        System.out.println(new LongestConsecutiveSequence().longestConsecutive(new int[]{100,4,200,1,3,2}));
    }
}
