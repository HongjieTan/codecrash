package _Topic_ConsecutiveValueSeq;

/*

 - 给一个arr，判断这个arr能不能被分为长度为5且连续的数字组合。
 - Followup问size至少大于3的情况。
 - seq 要保持顺序 还是 可以乱序？ 需要跟面试官问清楚。。。

   比如:
     1,2,3,4,5,6 -> false
     1,2,3,4,5,6,7,8,9,10 -> true
     1,2,2,3,3,4,4,5,5,6 -> true

  by 网友。。。
  - lz: "我用的是tail的方法， 然后更新tail和length， size3的我说有hashmap of heap 没时间写了
  - "第一题的意识是，氛围若干组，每隔组是5个数字，然后连续的数字？" "对"
  - "LC原题，用treemap"
  - "那似乎只能先sort，在hashmap里统计了"

  tan:
     - 如果事先有序，可以用greedy的方法，用count+need两个map来构建。。。如：SplitArrayIntoConsecutiveSubsequences
     - 如无序->先排个序？。。。
 */
public class BuildConsecutiveSeq {
    //。。。
}
