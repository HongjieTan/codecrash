package _Topic_ConsecutiveValueSeq;

import java.util.HashMap;
import java.util.Map;

/**
 *  greedy + hashtable: good!!! a little tricky...
 *
 *
 *   对每个num，有两个选择：(array is already in ascending order...)
 *      1. 加入之前构造好的顺子中
 *      2. 开一个新的顺子(新开的时候注意！直接开个长度为3的顺子，来保证顺子长度限制。。)
 *   用贪心策略，如果1能满足总是先满足1 （因为新开顺子可能失败，即使新开顺子成功，当1能满足的时候，将新开顺子加入之前的顺子也能成功，所以能够选择策略1的时候没必要冒风险选择策略2）
 *   目标是用策略1或者2消耗掉所有的元素，如果两个策略都无法选择，直接返回false
 *
 *   实现时注意!! 用另一个map记录已经构造好的顺子中现在需要哪些尾巴，来实现将当前元素加入构造好的顺子中
 *
 */
public class SplitArrayIntoConsecutiveSubsequences {
    public boolean isPossible(int[] nums) {
        Map<Integer, Integer> count = new HashMap<>();
        Map<Integer, Integer> need = new HashMap<>(); // use a need map...
        for (int x: nums) count.put(x, count.getOrDefault(x, 0)+1);
        for (int x: nums) {
            if (count.get(x)==0) continue;
            // append to existing seq first if possible!!(greedy)
            if (need.getOrDefault(x, 0)>0) {
                need.put(x, need.get(x)-1);
                need.put(x+1, need.getOrDefault(x+1, 0)+1);
                count.put(x, count.get(x)-1);
            }
            // start new seq (and satisfy the new seq in advance!!!!!!!!)
            else if(count.getOrDefault(x+1,0)>0 && count.getOrDefault(x+2,0)>0) {
                count.put(x, count.get(x)-1);
                count.put(x+1, count.get(x+1)-1); // satisfy it in advance...
                count.put(x+2, count.get(x+2)-1);
                need.put(x+3, need.getOrDefault(x+3,0)+1);
            }
            // if it can not append to existing seq and it can not start new seq, then false!
            else return false;
        }
        return true;
    }


    public static void main(String[] args) {
        System.out.println(new SplitArrayIntoConsecutiveSubsequences().isPossible(new int[]{1,2,3,3,4,4,5,5}));
    }
}
