package tree;

import common.TreeNode;

import java.util.*;

/**
 * Created by thj on 25/05/2018.
 */




public class PathSum3 {

    int originSum;
    Set<TreeNode> visited;

    public int pathSum(TreeNode root, int sum) {
        visited =  new HashSet<>();
        originSum = sum;
        return find(root, sum, false);
    }

    private int find(TreeNode root, int sum, boolean hasPrePath ) {
        if (root == null) return 0;

        int count=0;

        // shorter code
        if (hasPrePath || !visited.contains(root)) {
            if (!hasPrePath && !visited.contains(root)) {visited.add(root);} // set node as path start
            if (root.val == sum) {count+=1;}
            count += find(root.left, sum-root.val, true);
            count += find(root.right, sum-root.val, true);
            count += find(root.left, originSum, false);
            count += find(root.right, originSum, false);
        }

        // more detailed code
//        if (!hasPrePath) { // no pre path
//            if (!visited.contains(root)) { // set node as path start
//                visited.add(root);
//                if (root.val == sum) {count+=1;}
//                count += find(root.left, sum-root.val, true);
//                count += find(root.right, sum-root.val, true);
//                count += find(root.left, originSum, false);
//                count += find(root.right, originSum, false);
//            } else { // node already act as start
//                // skip
//            }
//        } else { // have pre path
//            if (root.val == sum) {count+=1;}
//            count += find(root.left, sum-root.val, true);
//            count += find(root.right, sum-root.val, true);
//            count += find(root.left, originSum, false);
//            count += find(root.right, originSum, false);
//        }

        return count;
    }

}
