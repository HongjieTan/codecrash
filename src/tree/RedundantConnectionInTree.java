package tree;

import common.TreeNode;

import java.util.*;

/**
 *
 *
 * 删除二叉树中多余的一个edge。假设input是root node，而且删完edge后还是root。
 *            例子：       1
 *                       /  \
 *                      2    3
 *                    /   \ /
 *                   4     5     删除2-5或3-5都可以
 *
 *  followup: 删除不影响树高度的那条边
 *
 */
public class RedundantConnectionInTree {

    // 删除不影响树高度的那条边 （删除离root近的边）
    /*
                例子：       1
                           /  \
                          2    \
                        /   \ /
                       4     5     删除1-5
    */
    public void deleteEdge_followup(TreeNode root) {
        // bfs
        Map<TreeNode, TreeNode> visited = new HashMap<>(); // value keep parent info
        LinkedList<TreeNode[]> que = new LinkedList<>();
        que.add(new TreeNode[]{root, null});

        while(!que.isEmpty()) {
            TreeNode[] cur = que.remove();
            TreeNode curN = cur[0], curP = cur[1];
            if(visited.containsKey(curN)) {
                TreeNode prevP = visited.get(curN);
                if(prevP.left==curN) prevP.left=null;
                else prevP.right=null;
                return;
            }
            visited.put(curN, curP);
            if(curN.left!=null) que.add(new TreeNode[]{curN.left, curN});
            if(curN.right!=null) que.add(new TreeNode[]{curN.right, curN});
        }
    }


    public void deleteEdge(TreeNode root) {
        dfs(root, new HashSet<>());
    }

    TreeNode dfs(TreeNode node, Set<TreeNode> visited) {
        if(node == null) return null;
        if(visited.contains(node)) return null;
        visited.add(node);
        node.left = dfs(node.left, visited);
        node.right = dfs(node.right, visited);
        return node;
    }

    public static void main(String[] args) {
        TreeNode n1 = new TreeNode(1);
        TreeNode n2 = new TreeNode(2);
        TreeNode n3 = new TreeNode(3);
        TreeNode n4 = new TreeNode(4);
        TreeNode n5 = new TreeNode(5);
//        n1.left=n2;n1.right=n3;
//        n2.left=n4;n2.right=n5;
//        n3.left=n5;
//        new RedundantConnectionInTree().deleteEdge(n1);

        n1.left=n2;n1.right=n5;
        n2.left=n4;n2.right=n5;
        new RedundantConnectionInTree().deleteEdge_followup(n1);
    }
}
