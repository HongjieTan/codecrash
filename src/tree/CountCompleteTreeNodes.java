package tree;

import common.TreeNode;

/**
 *  improved using binary search!:  O(logn^2)
 *  normal traversal: O(n)
 */
public class CountCompleteTreeNodes {

    public int countNodes(TreeNode root) {
        if (root==null) return 0;
        int lh = height(root.left), rh=height(root.right);
        int lcnt, rcnt;
        if (lh==rh) {// left is full
            lcnt = (1<<(lh+1))-1; // a coding trick: 1<<x = 2^x
            rcnt = countNodes(root.right);
        } else {// right is full
            lcnt = countNodes(root.left);
            rcnt = (1<<(rh+1))-1;
        }
        return 1+lcnt+rcnt;
    }

    private int height(TreeNode node) {
        return node==null?-1:1+height(node.left);
    }

    public static void main(String[] args) {
//        System.out.println(1<<2);
        TreeNode n1 = new TreeNode(1);
        TreeNode n2= new TreeNode(1);
        TreeNode n3 = new TreeNode(1);
        TreeNode n4 = new TreeNode(1);
        TreeNode n5 = new TreeNode(1);
        TreeNode n6 = new TreeNode(1);
        n1.left=n2;n1.right=n3;
        n2.left=n4;n2.right=n5;
        n3.left=n6;
        System.out.println(new CountCompleteTreeNodes().countNodes(n1));
    }

}


/*
Given a complete binary tree, count the number of nodes.

Note:

Definition of a complete binary tree from Wikipedia:
In a complete binary tree every level, except possibly the last, is completely filled,
and all nodes in the last level are as far left as possible.
It can have between 1 and 2h nodes inclusive at the last level h.

Example:

Input:
    1
   / \
  2   3
 / \  /
4  5 6

Output: 6
*/