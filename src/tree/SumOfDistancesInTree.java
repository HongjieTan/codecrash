package tree;

import java.util.*;

/**
 *
 *  两次遍历，更新count和res。第一次post order 第二次pre order
 *
 *
 *  Notice the difference between SumOfDistancesInTree vs. ShortestDistanceFromAllBuildings:
 *      对于某个点P，其两边的点相互访问：
 *          - SumOfDistancesInTree: 必定经过点P (tree,无环!!..)
 *          - ShortestDistanceFromAllBuildings: 可不经过点P，路径任意...(graph...)
 *
 */
public class SumOfDistancesInTree {

    Map<Integer, List<Integer>> adjMap;
    int[] res;
    int[] count;
    public int[] sumOfDistancesInTree(int N, int[][] edges) {
        adjMap = new HashMap<>();
        for(int[] edge: edges) {
            if(!adjMap.containsKey(edge[0])) adjMap.put(edge[0], new ArrayList<>());
            if(!adjMap.containsKey(edge[1])) adjMap.put(edge[1], new ArrayList<>());
            adjMap.get(edge[0]).add(edge[1]);
            adjMap.get(edge[1]).add(edge[0]);
        }

        res = new int[N];
        count = new int[N];

        // just use 0 as root
        dfs(0, new HashSet<>());
        dfs2(0, new HashSet<>(), -1);
        return res;
    }

    void dfs(int node, Set<Integer> visited) {
        visited.add(node);
        count[node]+=1;
        if (adjMap.containsKey(node)) {
            for(int nb: adjMap.get(node)) {
                if(!visited.contains(nb)) {
                    dfs(nb, visited);
                    count[node] += count[nb];
                    res[node] += res[nb]+count[nb];
                }
            }
        }

    }

     void dfs2(int node, Set<Integer> visited, int parent) {
        visited.add(node);
        if (parent!=-1) res[node] = res[parent] - count[node] + (res.length-count[node]); //  count[node]数量的结点距离减少1，n-count[node]数量的结点距离增加1，
        if (adjMap.containsKey(node)) {
             for(int nb: adjMap.get(node)) {
                 if(!visited.contains(nb)) {
                     dfs2(nb, visited, node);
                 }
             }
        }
    }

    public static void main(String[] args) {
//        int N = 6;
//        int[][] edges = {{0,1},{0,2},{2,3},{2,4},{2,5}};
        int N = 1;
        int[][] edges = {};
        int[] res = new SumOfDistancesInTree().sumOfDistancesInTree(N, edges);
        for (int x:res) System.out.println(x);
    }
}


/*
An undirected, connected tree with N nodes labelled 0...N-1 and N-1 edges are given.

The ith edge connects nodes edges[i][0] and edges[i][1] together.

Return a list ans, where ans[i] is the sum of the distances between node i and all other nodes.

Example 1:

Input: N = 6, edges = [[0,1],[0,2],[2,3],[2,4],[2,5]]
Output: [8,12,6,10,10,10]
Explanation:
Here is a diagram of the given tree:
  0
 / \
1   2
   /|\
  3 4 5
We can see that dist(0,1) + dist(0,2) + dist(0,3) + dist(0,4) + dist(0,5)
equals 1 + 1 + 2 + 2 + 2 = 8.  Hence, answer[0] = 8, and so on.
Note: 1 <= N <= 10000
*/
