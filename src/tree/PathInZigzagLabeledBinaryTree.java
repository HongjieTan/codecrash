//In an infinite binary tree where every node has two children, the nodes are labelled in row order.
//
// In the odd numbered rows (ie., the first, third, fifth,...), the labelling is left to right, while in the even numbered rows (second, fourth, sixth,...), the labelling is right to left.
//
//
//
// Given the label of a node in this tree, return the labels in the path from the root of the tree to the node with that label.
//
//
// Example 1:
//
//
//Input: label = 14
//Output: [1,3,4,14]
//
//
// Example 2:
//
//
//Input: label = 26
//Output: [1,2,6,10,26]
//
//
//
// Constraints:
//
//
// 1 <= label <= 10^6
//
//
//
// hint: Based on the label of the current node, find what the label must be for the parent of that node.
//

package tree;

import java.util.LinkedList;
import java.util.List;

public class PathInZigzagLabeledBinaryTree {
    public List<Integer> pathInZigZagTree(int label) {
        LinkedList<Integer> res = new LinkedList<>();
        res.addFirst(label);
        while(label!=1) {
            int h = (int)Math.floor(Math.log(label)/Math.log(2));
            int l_s = (int) Math.pow(2, h);
            int l_e = l_s*2-1;
            label = (l_e - (label-l_s))/2;
            res.addFirst(label);
        }
        return res;
    }

    public static void main(String[] as) {
        List<Integer> res = new PathInZigzagLabeledBinaryTree().pathInZigZagTree(26);
        for(int x:res) System.out.println(x);
    }
}
