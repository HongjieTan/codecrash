package tree;

import java.util.ArrayList;
import java.util.List;

public class ListOfRoots {

    class Node {
        int val;
        boolean isRed;
        List<Node> children;
        public Node(int val, boolean isRed) {
            this.val=val;
            this.isRed=isRed;
            this.children=new ArrayList<>();
        }
    }

    public List<Node> listOfRoots(Node root){
        List<Node> res = new ArrayList<>();
        dfs(res, root, true);
        return res;
    }

    void dfs(List<Node> res, Node node, boolean isParentDeleted) {
        if (node==null) return;

        if (isParentDeleted && !node.isRed) {
            res.add(node);
        }

        for(Node nb: node.children) {
            dfs(res, nb, node.isRed);
        }
    }

}


/*
More Detail :       ee，
然后返回这几个tree的root。直接一个recursive判断一下，如果这个node是红点的话就ignore 掉再去判断这个node的children，
如果这个node是蓝点的话，要看这个蓝点的parent是不是个红点，是的话，这个蓝点就是散落的tree中其中一个tree的root。

思路：简单BFS。。不应是dfs?
想问一下这题返回的顺序重要吗？

没想到怎么做，有没有大佬写了code能发一下的?感激不尽
(Provider: fill your name)

private void dfs(Node root, Node parent, List<Node> res) {
   if (root == null) {
       return;
   }
   if (parent.color == red && root.color != red) {
       res.add(root);
   }
   for (Node children : root.children) {
       dfs(child, root, res);
   }
}

public void dfsHelp (Node root, List<Node> res) {
		if (root == null) {
			return;
		}
		if (root.isRed) {
			for (Node child:root.childrens) {
				if (!child.isRed) {
					res.add(child);
				}
				dfsHelp(child, res);
			}
		} else {
			Iterator<Node> it = root.childrens.iterator();
			while (it.hasNext()) {
				Node child = it.next();
				if (child.isRed) {
					root.childrens.remove(child);
				}
dfsHelp(child, res);
			}
		}
	}

class TreeNode(object):
   def __init__(self, val, color):
       self.val = val
       self.color = color
       self.children = []

class Solution(object):
   def listOfRoots(self, root):
       if not root: return []

       ans = []

       def dfs(node, parent):
           if not node: return
           if node.color == 'b':
               if parent.color == 'r': ans.append(node.val)
// should check if parent is None
               else: ans.append(-1)//这里加上负一是为什么？

           for child in node.children:
               dfs(child, node)

       dfs(root, None)
       return ans
*/