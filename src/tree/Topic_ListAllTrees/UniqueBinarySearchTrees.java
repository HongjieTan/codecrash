package tree.Topic_AllTrees;

/**
 *  - dp
 *  - recursion with memo
 */
public class UniqueBinarySearchTrees {

    // f(i) = [f(0)*f(i-1)] + [f(1)*f(i-2)] + [f(2)*f(i-3)] + ....
    // f(1) = 1, f(2) = 2, f(3) = 5, ...
    public int numTrees(int n) {
        int[] dp = new int[n+1];
        dp[0] = 1;
        for (int i = 1; i <= n; i++) {
            dp[i] = 0;
            for (int j = 0; j <=i-1 ; j++) {
                dp[i] += dp[j]*dp[i-1-j];
            }
        }
        return dp[n];
    }


     /*
       Not best, it still have duplicated calculation, as: bstNum(1,2,3), bstNum(2,3,4), no need to calculated separately
     */
//    Map<String, Integer> cache = new HashMap<>();
//    public int numTrees_recursion_with_memo(int n) {
//        return numTree(1, n);
//    }
//    private int numTree(int l, int r) {
//        String key = l + "#" + r;
//        if (cache.containsKey(key)) return cache.get(key);
//        if (l>=r) return 1;
//        int count=0;
//        for (int i = l; i <=r ; i++) {
//            count += numTree(l, i-1)*numTree(i+1, r);
//        }
//        cache.put(key, count);
//        return cache.get(key);
//    }
}
