package tree.Topic_AllTrees;

import common.TreeNode;

import java.util.*;

/**
 *  - recursion with memo
 *  - dp
 */
public class UniqueBinarySearchTrees2 {
    Map<String, List<TreeNode>> cache = new HashMap<>();
    public List<TreeNode> generateTrees(int n) {
        if (n==0) return new ArrayList<>();
        return build(1, n);
    }
    private List<TreeNode> build(int l, int r) {
        String key = l+"#"+r;
        if (cache.containsKey(key)) return cache.get(key);
        if (l>r) {
            List<TreeNode> list = new ArrayList<>();
            list.add(null);
            return list;
        }
        if (l==r) return Arrays.asList(new TreeNode(l));

        List<TreeNode> curTrees = new ArrayList<>();
        for (int i = l; i <=r; i++) {
            List<TreeNode> leftSet = build(l, i-1);
            List<TreeNode> rightSet = build(i+1, r);
            for (TreeNode leftChild: leftSet) {
                for (TreeNode rightChild: rightSet) {
                    TreeNode node = new TreeNode(i);
                    node.left = leftChild;
                    node.right = rightChild;
                    curTrees.add(node);
                }
            }
        }
        cache.put(key, curTrees);
        return cache.get(key);
    }
}
