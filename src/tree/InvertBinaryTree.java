package tree;

import common.TreeNode;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

/**
 * Created by thj on 2018/8/7.
 */
public class InvertBinaryTree {
    public TreeNode invertTree_iter3(TreeNode root) {
        TreeNode node = root;
        Queue<TreeNode> q = new LinkedList<>();
        q.add(node);
        while(!q.isEmpty()) {
            node = q.remove();
            if(node==null) continue;

            TreeNode tmp = node.left;
            node.left = node.right;
            node.right = tmp;

            q.add(node.left);
            q.add(node.right);
        }
        return root;
    }

    public TreeNode invertTree_iter2(TreeNode root) {
        TreeNode node = root;
        Stack<TreeNode> st = new Stack<>();
        while(node!=null || !st.isEmpty()) {
            while(node!=null) {
                st.push(node);
                node = node.left;
            }
            node = st.pop();

            TreeNode tmp = node.right;
            node.right = node.left;
            node.left = tmp;

            node = tmp;
        }
        return root;
    }

    public TreeNode invertTree_iter1(TreeNode root) {
        if (root==null) return null;
        Stack<TreeNode> stack = new Stack<>();
        stack.push(root);
        TreeNode node;
        while (!stack.isEmpty()) {
            node = stack.pop();
            if (node!=null) {
                stack.push(node.right);
                stack.push(node.left);

                TreeNode temp = node.left;
                node.left = node.right;
                node.right = temp;
            }

        }
        return root;
    }

    // recursion
    public TreeNode invertTree(TreeNode root) {
        if (root==null) return null;
        TreeNode left = invertTree(root.left);
        TreeNode right = invertTree(root.right);
        root.left = right;
        root.right = left;
        return root;
    }

}
