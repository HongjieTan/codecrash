package tree;

import common.TreeNode;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by thj on 25/05/2018.
 */
public class PathSum2 {

    // iterative version: todo...

    // recursive version
    public List<List<Integer>> pathSumRecursive(TreeNode root, int sum) {
        List<List<Integer>> res = new ArrayList<>();
        List<Integer> path = new ArrayList<>();
        travel(root, path, res, sum);
        return res;
    }


    private void travel(TreeNode root, List<Integer> path, List<List<Integer>> res, int sum) {
        if (root == null) return;

        path.add(root.val);
        if (root.left == null && root.right == null && root.val == sum) {
            res.add(new ArrayList<>(path));
        }

        if (root.left != null) {
            travel(root.left, path, res, sum-root.val);
            path.remove(path.size()-1);
        }
        if (root.right!= null) {
            travel(root.right, path, res, sum-root.val);
            path.remove(path.size()-1);
        }



    }

}
