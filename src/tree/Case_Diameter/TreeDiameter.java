package tree.Case_Diameter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TreeDiameter {

    Map<Integer, List<Integer>> g = new HashMap<>();
    int diameter=0;
    public int treeDiameter(int[][] edges) {
        int n=edges.length;
        for(int i=0;i<=n;i++) g.put(i, new ArrayList<>());
        for(int[] e: edges) {
            g.get(e[0]).add(e[1]);
            g.get(e[1]).add(e[0]);
        }
        dfs(0, -1);
        return diameter;
    }
    int dfs(int node, int from) {
        int max1=0, max2=0;
        for(int child: g.get(node)) {
            if(from==child) continue;
            int h=dfs(child, node);
            if(h>max1) {
                max2=max1;
                max1=h;
            } else if(h>max2) {
                max2=h;
            }
        }
        diameter = Math.max(diameter, max1+max2);
        return max1;
    }


    // first version
//    Map<Integer, List<Integer>> g = new HashMap<>();
//    Map<Integer, Integer> memo = new HashMap<>();
//    int n;
//    public int treeDiameter(int[][] edges) {
//        this.n = edges.length;
//        for(int i=0;i<=n;i++) g.put(i, new ArrayList<>());
//        for(int[] e: edges) {
//            g.get(e[0]).add(e[1]);
//            g.get(e[1]).add(e[0]);
//        }
//        int ret = 0;
//        for(int i=0;i<=n;i++) {
//            ret = Math.max(ret, diameter(i));
//        }
//        return ret;
//    }
//
//    int diameter(int root) {
//        int max1=0, max2=0;
//        for(int cur: g.get(root)) {
//            int h = dfs(cur, root);
//            if(h>max1) {
//                max1=h;
//            } else if(h>max2) {
//                max2=h;
//            }
//        }
//        return max1+max2;
//    }
//
//    int dfs(int cur, int from) {
//        int key = cur+from*(n+1);
//        if(memo.containsKey(key)) return memo.get(key);
//        int maxh=0;
//        for(int child: g.get(cur)) {
//            if(child==from) continue;
//            maxh = Math.max(maxh, dfs(child, cur));
//        }
//        memo.put(key, maxh+1);
//        return maxh+1;
//    }
}
