package tree.Case_Diameter;

import common.TreeNode;

/**
 * Created by thj on 2018/9/19.
 *
 *  good!!!!
 *
 */
public class DiameterOfBinaryTree {

    private int maxPathLen;

    public int diameterOfBinaryTree(TreeNode root) {
        maxPathLen = 0;
        height(root);
        return maxPathLen;

    }

    private int height(TreeNode node) {
        if (node==null) return 0;
        int leftChildHeight = height(node.left);
        int rightChildHeight = height(node.right);

        int maxLenThroughNode = leftChildHeight + rightChildHeight;
        maxPathLen = Math.max(maxPathLen, maxLenThroughNode);

        return Math.max(leftChildHeight, rightChildHeight)+1;
    }

}
