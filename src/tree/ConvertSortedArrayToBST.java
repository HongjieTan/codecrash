package tree;

import common.TreeNode;

import java.util.Stack;

/**
 * Created by thj on 2018/8/7.
 */
public class ConvertSortedArrayToBST {



    // iterative version!! good!!!
    public TreeNode sortedArrayToBST_iterative(int[] nums) {
        if (nums==null || nums.length == 0) return null;

        Stack<TreeNode> stack = new Stack<>();
        Stack<Integer> lStack = new Stack<>();
        Stack<Integer> rStack = new Stack<>();

        TreeNode root = new TreeNode(nums[(nums.length-1)/2]);
        stack.push(root);
        lStack.push(0);
        rStack.push(nums.length-1);

        while (!stack.isEmpty()) {
            TreeNode node = stack.pop();
            int l = lStack.pop();
            int r = rStack.pop();
            int mid = l+(r-l)/2;
            if (mid+1<=r) {
                lStack.push(mid+1);
                rStack.push(r);
                node.right = new TreeNode(nums[mid+1+(r-mid-1)/2]);
                stack.push(node.right);

            }
            if (l<=mid-1) {
                lStack.push(l);
                rStack.push(mid-1);
                node.left = new TreeNode(nums[l+(mid-1-l)/2]);
                stack.push(node.left);
            }
        }

        return root;
    }


    public TreeNode sortedArrayToBST(int[] nums) {
        return buildBST(nums, 0, nums.length-1);
    }

    private TreeNode buildBST(int[] nums, int l, int r) {
        if (l>r) return null;
        int mid = l + (r-l)/2;
        TreeNode root = new TreeNode(nums[mid]);
        root.left = buildBST(nums, l, mid-1);
        root.right = buildBST(nums, mid+1, r);
        return root;
    }



}
