/*
Given a binary tree with the following rules:

root.val == 0
If treeNode.val == x and treeNode.left != null, then treeNode.left.val == 2 * x + 1
If treeNode.val == x and treeNode.right != null, then treeNode.right.val == 2 * x + 2
Now the binary tree is contaminated, which means all treeNode.val have been changed to -1.

You need to first recover the binary tree and then implement the FindElements class:

FindElements(TreeNode* root) Initializes the object with a contamined binary tree, you need to recover it first.
bool find(int target) Return if the target value exists in the recovered binary tree.


Example 1:

Input
["FindElements","find","find"]
[[[-1,null,-1]],[1],[2]]
Output
[null,false,true]
Explanation
FindElements findElements = new FindElements([-1,null,-1]);
findElements.find(1); // return False
findElements.find(2); // return True
Example 2:



Input
["FindElements","find","find","find"]
[[[-1,-1,-1,-1,-1]],[1],[3],[5]]
Output
[null,true,true,false]
Explanation
FindElements findElements = new FindElements([-1,-1,-1,-1,-1]);
findElements.find(1); // return True
findElements.find(3); // return True
findElements.find(5); // return False
Example 3:



Input
["FindElements","find","find","find","find"]
[[[-1,null,-1,-1,null,-1]],[2],[3],[4],[5]]
Output
[null,true,false,false,true]
Explanation
FindElements findElements = new FindElements([-1,null,-1,-1,null,-1]);
findElements.find(2); // return True
findElements.find(3); // return False
findElements.find(4); // return False
findElements.find(5); // return True


Constraints:

TreeNode.val == -1
The height of the binary tree is less than or equal to 20
The total number of nodes is between [1, 10^4]
Total calls of find() is between [1, 10^4]
0 <= target <= 10^6
*/
package tree;

import common.TreeNode;

import java.util.HashSet;
import java.util.Set;

public class FindElementsInContaminatedBinaryTree {

}
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
class FindElements {
    TreeNode root;
    Set<Integer> seen;
    public FindElements(TreeNode root) {
        this.root = root;
        this.seen = new HashSet<>();
        dfs(root, 0);
    }
    void dfs(TreeNode node, int x) {
        node.val = x;
        seen.add(x);
        if(node.left!=null) dfs(node.left, 2*x+1);
        if(node.right!=null) dfs(node.right, 2*x+2);
    }
    public boolean find(int target) {
        return seen.contains(target);
    }
}

/**
 * Your FindElements object will be instantiated and called as such:
 * FindElements obj = new FindElements(root);
 * boolean param_1 = obj.find(target);
 */

/*
by top:
  private Set<Integer> seen = new HashSet<>();

    public FindElements(TreeNode root) {
        dfs(root, 0);
    }
    private void dfs(TreeNode n, int v) {
        if (n == null) return;
        seen.add(v);
        n.val = v;
        dfs(n.left, 2 * v + 1);
        dfs(n.right, 2 * v + 2);
    }

    public boolean find(int target) {
        return seen.contains(target);
    }


    def __init__(self, root: TreeNode):
        self.seen = set()

        def dfs(node: TreeNode, v: int) -> None:
            if node:
                node.val = v
                self.seen.add(v)
                dfs(node.left, 2 * v + 1)
                dfs(node.right, 2 * v + 2)

        dfs(root, 0)

    def find(self, target: int) -> bool:
        return target in self.seen

Analysis:
HashSet cost space O(N), dfs() cost space and time O(N), therefore

FindElements() (dfs()) cost
time & space: O(N).

find() cost
time & space: O(1) excluding the space of HashSet.

Where N is the total number of nodes in the tree.



by second:
It's obvious to use BFS for the initial part.
However, a lot of people use HashSet(set() in python) to pre-store all the values in the initial part,
which may cause MLE when the values are huge.
There is a special way to implement find() that costs O(1) in space and O(logn) in time.

Firstly, let's see what a complete tree will look like in this problem:

It's very easy to find that numbers in each layer range from [2^i-1,2^(i+1)-2]
what if we add 1 to each number? Then it should range from [2^i, 2^(i+1)-1]
See? the binary of all numbers in each layer should be: 100..00 to 111...11

Hence we could discover that maybe we could use the binary number of target+1 to find a path:

image

I'm not proficient in English, so I would prefer to show my code here to explain my idea:

def find(self, target: int) -> bool:
    binary = bin(target+1)[3:]                  # remove the useless first `1`
    index = 0
    root = self.root                                    # use a new pointer `root` to traverse the tree
    while root and index <= len(binary): # traverse the binary number from left to right
        if root.val == target:
            return True
        if  binary[index] == '0':  # if it's 0, we have to go left
            root = root.left
        else:  # if it's 1, we have to go right
            root = root.right
        index += 1
    return False


by 3rd:
bin(9 + 1) = 1 010
Start from the second digit: 0/1/0 means left/right/left when traverse if 9 exists.
recover: T: O(n), S: O(n) (DFS worst case)
find: T: O(logn), S: O(logn) (for the string of path encoding, can be O(1) if use bit mask)

class FindElements(object):

    def __init__(self, root):
        """
        :type root: TreeNode
        """
        self.root = root
        if not root:
            return
        root.val = 0
        self.recover(root)
        return
    def recover(self, root):
        if not root:
            return
        if root.left:
            root.left.val = root.val * 2 + 1
            self.recover(root.left)
        if root.right:
            root.right.val = root.val * 2 + 2
            self.recover(root.right)

    def find(self, target):
        """
        :type target: int
        :rtype: bool
        """
        node = self.root
        encoding = bin(target + 1)[3:]
        counter = 0
        while counter < len(encoding):
            if encoding[counter] == "0":
                if node.left:
                    node = node.left
                    counter += 1
                else:
                    return False
            else:
                if node.right:
                    node = node.right
                    counter += 1
                else:
                    return False
        return True
*/