package tree.Topic_BuildTree;

import common.TreeNode;

// 1028
public class RecoverTreeFromPreorderTraversal {

    int idx, n;
    char[] s;
    public TreeNode recoverFromPreorder(String S) {
        idx=0;
        n=S.length();
        s = S.toCharArray();
        return parse(0);
    }
    TreeNode parse(int dep) {
        int val=0;
        while (idx<n&&s[idx]!='-') {
            val = val*10+s[idx]-'0';
            idx++;
        }
        TreeNode cur = new TreeNode(val);
        if (hasEdge(dep)){
            idx += dep+1;
            cur.left = parse(dep+1);
        }
        if (hasEdge(dep)){
            idx += dep+1;
            cur.right = parse(dep+1);
        }
        return cur;
    }

    boolean hasEdge(int dep) {
        int j = idx;
        while (j<n && s[j]=='-') j++;
        return (j-idx)==(dep+1);
    }

//    public TreeNode recoverFromPreorder(String S) {
//        return build(S, 0);
//    }
//    TreeNode build(String s, int d) {
//        if(s.isEmpty()) return null;
//        List<String> splits = split(s, d);
//        TreeNode root = new TreeNode(Integer.valueOf(splits.get(0)));
//        if (splits.size()>1) root.left = build(splits.get(1), d+1);
//        if (splits.size()>2) root.right = build(splits.get(2), d+1);
//        return root;
//    }
//
//    List<String> split(String s, int d) {
//        List<String> ans = new ArrayList<>();
//        int idx=0, cnt=0,l=-1,r=-1;
//        while(idx<s.length()){
//            if(s.charAt(idx)=='-') cnt++;
//            else {
//                if(cnt==d+1) {
//                    if(l==-1) l=idx;
//                    else r=idx;
//                }
//                cnt=0;
//            }
//            idx++;
//        }
//        ans.add(s.substring(0, l==-1?s.length():l-d-1));
//        if(l!=-1) ans.add(s.substring(l, r==-1?s.length():r-d-1));
//        if(r!=-1) ans.add(s.substring(r));
//        return ans;
//    }


    public static void main(String[] args) {
        RecoverTreeFromPreorderTraversal s = new RecoverTreeFromPreorderTraversal();
        TreeNode r = s.recoverFromPreorder("1-2--3---4-5--6---7");
        System.out.println("asdf");
    }

}
/*
1028. Recover a Tree From Preorder Traversal
We run a preorder depth first search on the root of a binary tree.
At each node in this traversal, we output D dashes (where D is the depth of this node), then we output the value of this node.
(If the depth of a node is D, the depth of its immediate child is D+1.  The depth of the root node is 0.)
If a node has only one child, that child is guaranteed to be the left child.
Given the output S of this traversal, recover the tree and return its root.

Example 1:
Input: "1-2--3--4-5--6--7"
Output: [1,2,5,3,4,6,7]
Example 2:
Input: "1-2--3---4-5--6---7"
Output: [1,2,5,3,null,6,null,4,null,7]
Example 3:
Input: "1-401--349---90--88"
Output: [1,401,null,349,88,90]
Note:

The number of nodes in the original tree is between 1 and 1000.
Each node will have a value between 1 and 10^9.
*/
