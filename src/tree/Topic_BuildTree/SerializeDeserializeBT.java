package tree.Topic_BuildTree;

import common.TreeNode;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Stack;

/**
 * Created by thj on 2018/8/5.
 */
public class SerializeDeserializeBT {


    // Encodes a tree to a single string.
    public String serialize(TreeNode root) {
        StringBuffer sb = new StringBuffer();
        dfs(root, sb);
        return sb.toString();
    }

    private void dfs(TreeNode node, StringBuffer sb) {
        if (node==null) {
            sb.append("null").append(",");
            return;
        }

        sb.append(node.val).append(",");
        dfs(node.left, sb);
        dfs(node.right, sb);
    }

    private void dfs_iterative(TreeNode node, StringBuffer sb) {
        if (node==null) {
            sb.append("null");
            return;
        }

        Stack<TreeNode> stack = new Stack<>();
        while (!stack.isEmpty() || node!=null) {
            while (node!=null) {
                sb.append(node.val).append(",");
                stack.push(node);
                node = node.left;
                if (node==null) sb.append("null").append(",");
            }

            node = stack.pop();
            node = node.right;
            if (node==null) sb.append("null").append(",");
        }
    }

    public String dfs_iterative_v2(TreeNode root) {
        StringBuffer sb = new StringBuffer();
        Stack<TreeNode> stack = new Stack<>();
        stack.push(root);
        while (!stack.isEmpty()) {
            root = stack.pop();
            if (root==null) sb.append("null").append(",");
            else {
                sb.append(root.val).append(",");
                stack.push(root.right);
                stack.push(root.left);
            }
        }
        return sb.toString();
    }

    // Decodes your encoded data to tree.
    public TreeNode deserialize(String data) {
        if (data==null || data.isEmpty()) return null;
        String[] valArr = data.split(",");
        LinkedList<String> valsQue = new LinkedList<>(Arrays.asList(valArr));
        return decode(valsQue);
    }

    public TreeNode decode(LinkedList<String> valsQue) {
        String val = valsQue.poll();
        TreeNode root;
        if (val.equals("null")) {
            root = null;
        } else {
            root = new TreeNode(Integer.valueOf(val));
            root.left = decode(valsQue);
            root.right = decode(valsQue);
        }

        return root;
    }

    // hard.. skip...
    public TreeNode decode_iterative(LinkedList<String> valsQue) {
        return null;
    }


    public static void main(String[] args) {
    TreeNode n1 = new TreeNode(1);
    TreeNode n2 = new TreeNode(2);
    TreeNode n3 = new TreeNode(3);
    TreeNode n4 = new TreeNode(4);
    TreeNode n5 = new TreeNode(5);
    TreeNode n6 = new TreeNode(6);
    TreeNode n7 = new TreeNode(7);

    n1.left=n2;n1.right=n3;
    n2.left=n4;n2.right=n5;
    n5.left=n6;
    n3.left=n7;

    String str = new SerializeDeserializeBT().serialize(n1);
    System.out.println(str);
    TreeNode root = new SerializeDeserializeBT().deserialize(str);
    System.out.println("sdf");

    }

}

//class SerializeDeserializeBTx2 {
//    public String serialize(TreeNode root) {
//        StringBuffer path = new StringBuffer();
//        visit(root, path);
//        return path.toString();
//    }
//
//    private void visit(TreeNode root, StringBuffer path) {
//        if (root==null) {
//            path.append("NULL").append(",");
//        } else {
//            path.append(root.val).append(",");
//            visit(root.left, path);
//            visit(root.right, path);
//        }
//    }
//
//    public TreeNode deserialize(String data) {
//        linkedlist_queue<String> que = new linkedlist_queue<>();
//        String[] arr = data.split(",");
//        for (String val: arr) que.add(val);
//        return build(que);
//    }
//
//    private TreeNode build(linkedlist_queue<String> que) {
//        String val = que.poll();
//        if (val.equals("NULL")) return null;
//        System.out.println(val);
//        TreeNode node = new TreeNode(Integer.valueOf(val));
//        node.left = build(que);
//        node.right = build(que);
//        return node;
//    }
//
//    public  static void main(String[] args) {
//        TreeNode n1 = new TreeNode(1);
//        TreeNode n2 = new TreeNode(2);
//        TreeNode n3 = new TreeNode(3);
//        TreeNode n4 = new TreeNode(4);
//        n1.left=n2;n1.right=n3;
//        n2.right=n4;
//        SerializeDeserializeBTx2 util = new SerializeDeserializeBTx2();
//        String data = util.serialize(n1);
//        System.out.println(data);
//        TreeNode n = util.deserialize(data);
//        System.out.println("asdf");
//    }
//}