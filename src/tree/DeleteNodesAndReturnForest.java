//Given the root of a binary tree, each node in the tree has a distinct value.
//
// After deleting all nodes with a value in to_delete, we are left with a forest (a disjoint union of trees).
//
// Return the roots of the trees in the remaining forest. You may return the result in any order.
//
//
// Example 1:
//
//
//
//
//Input: root = [1,2,3,4,5,6,7], to_delete = [3,5]
//Output: [[1,2,null,4],[6],[7]]
//
//
//
// Constraints:
//
//
// The number of nodes in the given tree is at most 1000.
// Each node has a distinct value between 1 and 1000.
// to_delete.length <= 1000
// to_delete contains distinct values between 1 and 1000.
//
package tree;

import common.TreeNode;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */

public class DeleteNodesAndReturnForest {
    List<TreeNode> res;
    Set<Integer> dset;
    public List<TreeNode> delNodes(TreeNode root, int[] to_delete) {
        res = new ArrayList<>();
        dset = new HashSet<>();
        for(int x: to_delete) dset.add(x);
        dfs(root);
        if (!dset.contains(root.val)) res.add(root);
        return res;
    }
    TreeNode dfs(TreeNode node) {
        if(node==null) return null;
        node.left = dfs(node.left);
        node.right = dfs(node.right);
        if(dset.contains(node.val)) {
            if(node.left!=null) res.add(node.left);
            if(node.right!=null) res.add(node.right);
            return null;
        } else{
            return node;
        }
    }
}
/*
by uwi:
class Solution {
    List<TreeNode> list = new ArrayList<>();

    public List<TreeNode> delNodes(TreeNode root, int[] to_delete) {
        Set<Integer> tod = new HashSet<>();
        for(int t : to_delete)tod.add(t);
        dfs(root, tod);
        if(!tod.contains(root.val)){
            list.add(root);
        }
        return list;
    }

    void dfs(TreeNode root, Set<Integer> tod)
    {
        if(root == null)return;
        dfs(root.left, tod);
        dfs(root.right, tod);
        if(tod.contains(root.val)){
            if(root.left != null && !tod.contains(root.left.val)){
                list.add(root.left);
                root.left = null;
            }
            if(root.right != null && !tod.contains(root.right.val)){
                list.add(root.right);
                root.right = null;
            }
        }else{
            if(root.left != null && tod.contains(root.left.val)){
                root.left = null;
            }
            if(root.right != null && tod.contains(root.right.val)){
                root.right = null;
            }
        }
    }
}


by lee:
Intuition
As I keep saying in my "courses",
solve tree problem with recursion first.


Explanation
If a node is root (has no parent) and isn't deleted,
when will we add it to the result.


Complexity
Time O(N)
Space O(N)


Java:

    Set<Integer> to_delete_set;
    List<TreeNode> res = new ArrayList<>();
    public List<TreeNode> delNodes(TreeNode root, int[] to_delete) {
        res = new ArrayList<>();
        to_delete_set = new HashSet<>();
        for (int i : to_delete)
            to_delete_set.add(i);
        helper(root, true);
        return res;
    }

    private TreeNode helper(TreeNode node, boolean is_root) {
        if (node == null) return null;
        boolean deleted = to_delete_set.contains(node.val);
        if (is_root && !deleted) res.add(node);
        node.left = helper(node.left, deleted);
        node.right =  helper(node.right, deleted);
        return deleted ? null : node;
    }
Python:

    def delNodes(self, root, to_delete):
        to_delete_set = set(to_delete)
        res = []

        def helper(root, is_root):
            if not root: return None
            root_deleted = root.val in to_delete_set
            if is_root and not root_deleted:
                res.append(root)
            root.left = helper(root.left, root_deleted)
            root.right = helper(root.right, root_deleted)
            return None if root_deleted else root
        helper(root, True)
        return res

*/