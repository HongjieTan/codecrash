/*
A tree rooted at node 0 is given as follows:

The number of nodes is nodes;
The value of the i-th node is value[i];
The parent of the i-th node is parent[i].
Remove every subtree whose sum of values of nodes is zero.

After doing so, return the number of nodes remaining in the tree.



Example 1:



Input: nodes = 7, parent = [-1,0,0,1,2,2,2], value = [1,-2,4,0,-2,-1,-1]
Output: 2


Constraints:

1 <= nodes <= 10^4
-10^5 <= value[i] <= 10^5
parent.length == nodes
parent[0] == -1 which indicates that 0 is the root.
*/
package tree.Topic_TwoPass;

import java.util.*;

public class DeleteTreeNodes {

    // two dfs
    Map<Integer, List<Integer>> adj;
    int[] sums, value;
    public int deleteTreeNodes(int nodes, int[] parent, int[] value) {
        int n = nodes;
        adj = new HashMap<>();
        sums = new int[n];
        this.value = value;

        for(int i=0;i<n;i++) {
            adj.putIfAbsent(parent[i], new ArrayList<>());
            adj.get(parent[i]).add(i);
        }

        dfs1(0);
        return dfs2(0);
    }

    int dfs1(int node) {
        int sum = value[node];
        for(int nb: adj.getOrDefault(node, Arrays.asList())) {
            sum += dfs1(nb);
        }
        sums[node] = sum;
        return sum;
    }

    int dfs2(int node) {
        if(sums[node]==0) return 0;

        int cnt = 1;
        for(int nb: adj.getOrDefault(node, Arrays.asList())) {
            cnt += dfs2(nb);
        }
        return cnt;
    }

}


/*
by awice:
class Solution(object):
    def deleteTreeNodes(self, N, parent, value):
        """
        :type nodes: int
        :type parent: List[int]
        :type value: List[int]
        :rtype: int
        """

        children = [[] for _ in xrange(N)]
        for c, p in enumerate(parent):
            if p != -1:
                children[p].append(c)

        bad = [False] * N
        def dfs(node):
            # return subtree sum
            ans = value[node]
            for nei in children[node]:
                ans += dfs(nei)
            if ans == 0:
                bad[node] = True
            return ans
        dfs(0)
        self.ans = N
        def dfs2(node, b=False):
            if bad[node]:
                b = True
            if b is True:
                self.ans -= 1
            for nei in children[node]:
                dfs2(nei, b)
        dfs2(0)
        return self.ans


by wjli:
class Solution {
	vector<vector<int>> adj;
	vector<int> v;
	int n;

	int dfs(int p, int &s) {
		int i, j, k, ss = v[p], ct = 1, sv, c;
		s = 0;

		for (auto pp : adj[p]) {
			sv = 0;
			c = dfs(pp, sv);
			if (sv == 0) continue;
			ct += c;
			ss += sv;
		}

		s = ss;
		if (ss == 0) return 0;
		else return ct;
	}

public:
	int deleteTreeNodes(int nodes, vector<int>& parent, vector<int>& value) {
		v = value;
		n = nodes;
		adj.clear();
		adj.resize(nodes);
		for (int i = 0; i < n; i++) {
			if (parent[i] != -1) adj[parent[i]].push_back(i);
		}

		int s, ans = dfs(0, s);
		return ans;
	}
};


by Hexadecimal:
class Solution:
    def deleteTreeNodes(self, n: int, parent: List[int], value: List[int]) -> int:

        def search(x):
            nonlocal s
            tot = value[x]
            for i in c[x]:
                tot += search(i)
            s[x] = tot
            return tot

        def find(x):
            nonlocal ret
            if s[x] == 0:
                return
            ret += 1
            for i in c[x]:
                find(i)

        c = [[] for i in range(n)]
        s = [0] * n
        for i, p in enumerate(parent):
            if p >= 0:
                c[p].append(i)
        search(0)
        ret = 0
        find(0)
        return ret

*/