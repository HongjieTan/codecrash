// Given the root of a binary tree, consider all root to leaf paths: paths from the root to any leaf.
// (A leaf is a node with no children.)
//
// A node is insufficient if every such root to leaf path intersecting this node has sum strictly less than limit.
//
// Delete all insufficient nodes simultaneously, and return the root of the resulting binary tree.
//
//
//
// Example 1:
//
//
//
//Input: root = [1,2,3,4,-99,-99,7,8,9,-99,-99,12,13,-99,14], limit = 1
//
//Output: [1,2,3,4,null,null,7,8,9,null,14]
//
//
//
// Example 2:
//
//
//
//Input: root = [5,4,8,11,null,17,4,7,1,null,null,5,3], limit = 22
//
//Output: [5,4,8,11,null,17,4,7,null,null,null,5]
//
//
//
// Example 3:
//
//
//
//Input: root = [1,2,-3,-5,null,4,null], limit = -1
//
//Output: [1,null,-3,4]
//
//
//
//
// Note:
//
//
// The given tree will have between 1 and 5000 nodes.
// -10^5 <= node.val <= 10^5
// -10^9 <= limit <= 10^9
//
//
//
//
//
//

package tree.Topic_TwoPass;

import common.TreeNode;

/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
public class InsufficientNodesInRootToLeafPaths {

    // one pass....tricky(by lee)
//    public TreeNode sufficientSubset(TreeNode root, int limit) {
//        if (root == null)
//            return null;
//        if (root.left == null && root.right == null)
//            return root.val < limit ? null : root;
//        root.left = sufficientSubset(root.left, limit - root.val);
//        root.right = sufficientSubset(root.right, limit - root.val);
//        return root.left == root.right ? null : root;
//    }

    // one pass
    class Node{
        TreeNode node;
        int sum;
        public Node(TreeNode node, int sum) {this.node=node;this.sum=sum;}
    }
    public TreeNode sufficientSubset(TreeNode root, int limit) {
        return dfs(root, limit).node;
    }
    Node dfs(TreeNode root, int limit) {
        if(root==null) return new Node(null, 0);

        Node left = dfs(root.left, limit-root.val);
        Node right = dfs(root.right, limit-root.val);
        int maxsum = root.val;
        if (root.left!=null && root.right!=null)  maxsum += Math.max(left.sum, right.sum);
        else if(root.left!=null) maxsum += left.sum;
        else if(root.right!=null) maxsum += right.sum;

        root.left = left.node;
        root.right = right.node;

        if(maxsum<limit) return new Node(null, maxsum);
        return new Node(root, maxsum);
    }


    // twopass:  postorder + preorder
    Map<TreeNode, Integer> maxsum = new HashMap<>();
    public TreeNode sufficientSubset_v1(TreeNode root, int limit) {
        dfs1(root);
        return dfs2(root, limit);
    }

    TreeNode dfs2(TreeNode node, int limit) {
        if(node == null) return null;
        if(maxsum.get(node) < limit) return null;
        node.left = dfs2(node.left, limit-node.val);
        node.right = dfs2(node.right, limit-node.val);
        return node;
    }

    int dfs1(TreeNode node) {
        int sum = 0;
        if(node.left==null && node.right==null) sum = node.val;
        if(node.left!=null && node.right!=null) sum = Math.max(dfs1(node.left), dfs1(node.right))+ node.val;
        if(node.left!=null && node.right==null) sum = dfs1(node.left)+node.val;
        if(node.left==null && node.right!=null) sum = dfs1(node.right)+node.val;
        maxsum.put(node, sum);
        return sum;
    }


    public static void main(String[] a) {
        TreeNode n1 = new TreeNode(1);
        TreeNode n2 = new TreeNode(2);
        TreeNode n3 = new TreeNode(-3);
        TreeNode n4 = new TreeNode(4);
        TreeNode n5 = new TreeNode(-5);
        n1.left=n2;
        n1.right=n3;
        n2.left=n5;
        n3.left=n4;
        System.out.println(new InsufficientNodesInRootToLeafPaths().sufficientSubset(n1, -1));
    }


}


/*

by top:
-> the problems seems have error
-> A leaf is a node with at most one children. (in this problem)
->
With the following input, there are only 2 leaf node{-1, 2}, and only 2 paths from root to leaf. {1} is not a leaf node.
The description of the problem already highlighted the definition of leaf node: (A leaf is a node with no children)
     0
    / \
   1   2
    \
     -1
So there are only 2 paths from root to leaf.
The first path is [0, 1, -1], and second is [0, 2].
If give a limit = 1, there are 2 nodes have to be removed.
{-1} need to be removed.
{1} is also needed to be removed, because there's only one path over the node {1}.

Please obsolete the contest's result. It's totally meanless.




by lee:
Foreword
LeetCode's OJ was wrong and it's fixed.


Intuition
If root is leaf,
we compare the limit and root.val,
and return the result directly.

If root is leaf,
we recursively call the function on root's children with limit = limit - root.val.

Note that if a node become a new leaf,
it means it has no valid path leading to an original leaf,
we need to remove it.


Explanation
if root.left == root.right == null, root is leaf with no child {
    if root.val < limit, we return null;
    else we return root.
}
if root.left != null, root has left child {
    Recursively call sufficientSubset function on left child,
    with limit = limit - root.val
}
if root.right != null, root has right child {
    Recursively call sufficientSubset function on right child,
    with limit = limit - root.val
}
if root.left == root.right == null,
root has no more children, no valid path left,
we return null;
else we return root.

Complexity
Time O(N)
Space O(height) for recursion management.


Java:

    public TreeNode sufficientSubset(TreeNode root, int limit) {
        if (root == null)
            return null;
        if (root.left == null && root.right == null)
            return root.val < limit ? null : root;
        root.left = sufficientSubset(root.left, limit - root.val);
        root.right = sufficientSubset(root.right, limit - root.val);
        return root.left == root.right ? null : root;
    }
C++:

    TreeNode* sufficientSubset(TreeNode* root, int limit) {
        if (root->left == root->right)
            return root->val < limit ? NULL : root;
        if (root->left)
            root->left = sufficientSubset(root->left, limit - root->val);
        if (root->right)
            root->right = sufficientSubset(root->right, limit - root->val);
        return root->left == root->right ? NULL : root;
    }
Python:

    def sufficientSubset(self, root, limit):
        if root.left == root.right:
            return None if root.val < limit else root
        if root.left:
            root.left = self.sufficientSubset(root.left, limit - root.val)
        if root.right:
            root.right = self.sufficientSubset(root.right, limit - root.val)
        return root if root.left or root.right else None




*/