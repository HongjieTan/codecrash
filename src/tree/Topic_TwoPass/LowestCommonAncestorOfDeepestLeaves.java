/*
Given a rooted binary tree, return the lowest common ancestor of its deepest leaves.

Recall that:

The node of a binary tree is a leaf if and only if it has no children
The depth of the root of the tree is 0, and if the depth of a node is d, the depth of each of its children is d+1.
The lowest common ancestor of a set S of nodes is the node A with the largest depth such that every node in S is in the subtree with root A.


Example 1:

Input: root = [1,2,3]
Output: [1,2,3]
Explanation:
The deepest leaves are the nodes with values 2 and 3.
The lowest common ancestor of these leaves is the node with value 1.
The answer returned is a TreeNode object (not an array) with serialization "[1,2,3]".
Example 2:

Input: root = [1,2,3,4]
Output: [4]
Example 3:

Input: root = [1,2,3,4,5]
Output: [2,4,5]


Constraints:

The given tree will have between 1 and 1000 nodes.
Each node of the tree will have a distinct value between 1 and 1000.
*/
package tree.Topic_TwoPass;

import common.TreeNode;

import java.util.HashMap;
import java.util.Map;

//  solution:
//   - one pass: more concise
//   - two pass: more intuitive
public class LowestCommonAncestorOfDeepestLeaves {

    // two pass: postorder to calculate heights and preorder to calculate lca
    Map<TreeNode, Integer> memo;
    public TreeNode lcaDeepestLeaves_twopass(TreeNode root) {
        memo = new HashMap<>();
        height(root);
        return dfs2(root);
    }
    TreeNode dfs2(TreeNode node) {
        if(node==null) return null;
        int lh = memo.getOrDefault(node.left, 0);
        int rh = memo.getOrDefault(node.right, 0);
        if(lh==rh) return node;
        return dfs2(lh>rh?node.left:node.right);
    }
    int height(TreeNode r) {
        if(r==null) return 0;
        memo.put(r, Math.max( height(r.left),  height(r.right))+1);
        return memo.get(r);
    }

    // one pass
    class CNode {
        TreeNode lca;
        int h;
        public CNode(TreeNode lca, int h) {this.lca=lca;this.h=h;}
    }
    public TreeNode lcaDeepestLeaves_onepass(TreeNode root) {
        return helper(root).lca;
    }

    CNode helper(TreeNode node) {
        if(node==null) return new CNode(null, 0);
        CNode left = helper(node.left), right = helper(node.right);
        if (left.h > right.h) return new CNode(left.lca, left.h+1);
        if (left.h < right.h) return new CNode(right.lca, right.h+1);
        return new CNode(node, left.h+1);
    }

}