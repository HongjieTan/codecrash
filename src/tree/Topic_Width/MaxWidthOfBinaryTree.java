package tree.Topic_Width;

import common.TreeNode;

import java.util.HashMap;
import java.util.Map;

public class MaxWidthOfBinaryTree {
    Map<Integer, Integer> left;
    Map<Integer, Integer> right;
    int height;

    public int widthOfBinaryTree(TreeNode root) {
        left = new HashMap<>();
        right = new HashMap<>();
        height = height(root);

        helper(root, 1, 0);
        int ans = 0;
        for(int key: left.keySet()) {
            ans = Math.max(ans, right.get(key)-left.get(key));
        }
        return ans;
    }

    int height(TreeNode node) {
        if (node==null) return 0;
        return 1+Math.max(height(node.left), height(node.right));
    }

    void helper(TreeNode node,  int level, int distance) {
        if(node==null) return;
        if(!left.containsKey(level) || distance<left.get(level)) left.put(level, distance);
        if(!right.containsKey(level) || distance>right.get(level)) right.put(level, distance);

        helper(node.left, level+1, distance-1); // distance ???
        helper(node.right, level+1, distance+1);
    }

    public static void main(String[] args) {
        TreeNode n1 = new TreeNode(1);
        TreeNode n2 = new TreeNode(1);
        TreeNode n3 = new TreeNode(1);
        TreeNode n4 = new TreeNode(1);
        TreeNode n5 = new TreeNode(1);
        n1.left=n3;
        n3.left=n4;n3.right=n5;

        System.out.println(new MaxWidthOfBinaryTree().widthOfBinaryTree(n1));
    }
}

/*
Given a binary tree, write a function to get the maximum width of the given tree.
The width of a tree is the maximum width among all levels. The binary tree has the same structure as a full binary tree, but some nodes are null.

The width of one level is defined as the length between the end-nodes (the leftmost and right most non-null nodes in the level,
where the null nodes between the end-nodes are also counted into the length calculation.

Example 1:

Input:

           1
         /   \
        3     2
       / \     \
      5   3     9

Output: 4
Explanation: The maximum width existing in the third level with the length 4 (5,3,null,9).
Example 2:

Input:

          1
         /
        3
       / \
      5   3

Output: 2
Explanation: The maximum width existing in the third level with the length 2 (5,3).
Example 3:

Input:

          1
         / \
        3   2
       /
      5

Output: 2
Explanation: The maximum width existing in the second level with the length 2 (3,2).
Example 4:

Input:

          1
         / \
        3   2
       /     \
      5       9
     /         \
    6           7
Output: 8
Explanation:The maximum width existing in the fourth level with the length 8 (6,null,null,null,null,null,null,7).


Note: Answer will in the range of 32-bit signed integer.
*/