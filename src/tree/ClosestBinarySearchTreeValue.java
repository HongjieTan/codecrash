package tree;

import common.TreeNode;

public class ClosestBinarySearchTreeValue {
    public int closestValue(TreeNode root, double target) {
        // write your code here
        if (root==null) return -1;
        int floor=Integer.MIN_VALUE, ceil=Integer.MAX_VALUE;
        while (root!=null) {
            if (Double.valueOf(root.val) == target) return root.val;
            else if (Double.valueOf(root.val)>target) { // go left
                ceil = Math.min(ceil, root.val);
                root=root.left;
            } else { // go right
                floor = Math.max(floor, root.val);
                root=root.right;
            }
        }

        if (floor==Integer.MIN_VALUE) return ceil;
        if (ceil==Integer.MAX_VALUE) return floor;
        return ceil-target<target-floor?ceil:floor;
    }

    public static void main(String[] args) {
        TreeNode n1 = new TreeNode(1);
//        TreeNode n2 = new TreeNode(2);
//        TreeNode n3 = new TreeNode(3);
//        TreeNode n4 = new TreeNode(4);
//        n3.left=n2;n3.right=n4;
//        n2.left=n1;
        System.out.println(new ClosestBinarySearchTreeValue().closestValue(n1, 4.142857));
    }
}
