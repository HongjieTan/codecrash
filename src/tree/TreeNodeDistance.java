package tree;

/**
 * Created by thj on 2018/8/1.
 */
public class TreeNodeDistance {

    static class Node
    {
        int data;
        Node left, right;
        Node(int item)    {
            data = item;
            left = right = null;
        }
    }

    public  int findDist(Node root, int a, int b) {
        // Your code here
        Node lca = lowestCommonAncestor(root, a, b);
        return distance(lca, a) + distance(lca, b);
    }


    public Node lowestCommonAncestor(Node root, int a, int b) {
        if (root==null) return null;
        if (root.data == a || root.data==b) return root;
        Node leftSub = lowestCommonAncestor(root.left, a, b);
        Node rightSub = lowestCommonAncestor(root.right, a, b);
        if (leftSub!=null && rightSub!=null) return root;
        return leftSub!=null?leftSub:rightSub;
    }

    public int distance(Node root, int a) {
        if (root==null) return -1;
        if (root.data == a) return 0;

        int left = distance(root.left, a);
        int right = distance(root.right, a);
        if (left==-1 && right==-1) return -1;
        return left!=-1?1+left:1+right;
    }


    // TODO!! bfs iterative
    public int distance_iterative(Node root, int a) {
//        if (root==null) return  -1;
//        linkedlist_queue<Node> que = new linkedlist_queue<>();
//        que.add(root);
//        while (!que.isEmpty()) {
//
//        }

        return -1;
    }

    public static void main(String[] args) {
        Node n1 = new Node(1);
        Node n2 = new Node(2);
        Node n3 = new Node(3);
        Node n4 = new Node(4);
        Node n5 = new Node(5);
        Node n6 = new Node(6);
        Node n7 = new Node(7);
        Node n8 = new Node(8);

        n1.left=n2; n1.right=n3;
        n2.left=n4; n2.right=n5;
        n3.left=n6; n3.right=n7;
        n6.right=n8;

        //       1
        //     /   \
        //    2     3
        //   / \   / \
        //  4   5  6  7
        //          \
        //           8

//        System.out.println(new TreeNodeDistance().distance(n1, 1));
//        System.out.println(new TreeNodeDistance().distance(n1, 2));
//        System.out.println(new TreeNodeDistance().distance(n1, 6));
//        System.out.println(new TreeNodeDistance().distance(n1, 8));
        System.out.println(new TreeNodeDistance().findDist(n1, 4,5));
        System.out.println(new TreeNodeDistance().findDist(n1, 4,6));
        System.out.println(new TreeNodeDistance().findDist(n1, 3,4));
        System.out.println(new TreeNodeDistance().findDist(n1, 2,4));
        System.out.println(new TreeNodeDistance().findDist(n1, 8,5));



    }



}




