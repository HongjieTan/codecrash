package tree;

import common.TreeNode;

import java.util.LinkedList;

/**
 * Created by thj on 2018/10/1.
 *
 *  perfect tree definition:
 *  1. all internal node has two child
 *  2. all leaves same depth
 */
public class PerfectBinaryTreeCheck {

    // one solution: level order traversal -> check if every level has 2^depth nodes
    public boolean isPerfectTree(TreeNode node) {
        if (node==null) return true;

        LinkedList<TreeNode> que = new LinkedList<>();
        que.add(node);

        int depth = 0;
        while (!que.isEmpty()) {
            depth++;
            int levelSize = que.size();

            if (Math.pow(2, depth-1) != levelSize) return false;

            for (int i = 0; i < levelSize; i++) {
                TreeNode curNode = que.poll();
                if (curNode.left!=null) que.add(curNode.left);
                if (curNode.right!=null)que.add(curNode.right);
            }
        }

        return true;
    }


    public static void main(String[] args) {
        TreeNode n1 = new TreeNode(1);
        TreeNode n2 = new TreeNode(2);
        TreeNode n3 = new TreeNode(3);

        n1.left=n2;

        System.out.println(new PerfectBinaryTreeCheck().isPerfectTree(n1));
    }


}
