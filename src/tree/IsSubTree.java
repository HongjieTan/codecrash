package tree;

import common.TreeNode;

import java.util.Stack;

/**
 *  O(m*n)
 */
public class IsSubTree {

    // solution1: compare nodes during traversal
    public boolean isSubtree(TreeNode s, TreeNode t) {
        if (isSame(s, t)) return true;
        return s!=null && (isSubtree(s.left, t) || isSubtree(s.right, t));
    }

    private boolean isSame(TreeNode s, TreeNode t) {
        if (s == null && t==null) return true;
        if (s == null || t==null) return false;
        if (s.val!=t.val) return false;
        return isSame(s.left, t.left) && isSame(s.right, t.right);
     }

     // solution: compare preorder string (treat null as 'null' value!!! this is important!!!)
    private String getNodeValue(TreeNode node) {
        return "#" + node==null?"Null":String.valueOf(node.val);
    }

    public boolean isSubTree_v2(TreeNode s, TreeNode t) {
//        StringBuffer sPath = new StringBuffer();
//        StringBuffer tPath = new StringBuffer();
//        preorder(s, sPath);
//        preorder(t, tPath);
//        return sPath.toString().contains(tPath.toString());
        return preorder_iterative(s).contains(preorder_iterative(t));
    }

    public String preorder_iterative(TreeNode node) {
        StringBuffer sb = new StringBuffer();
        Stack<TreeNode> stack = new Stack<>();
        stack.add(node);
        while (!stack.isEmpty()) {
            TreeNode curNode = stack.pop();
            sb.append(getNodeValue(curNode));
            if (curNode!=null) {
                stack.add(curNode.right);
                stack.add(curNode.left);
            }
        }
        return sb.toString();
    }

    public void preorder(TreeNode node, StringBuffer path) {
        path.append(getNodeValue(node));
        if(node!=null) {
            preorder(node.left, path);
            preorder(node.right, path);
        }
    }




     public static void main(String[] args) {
         TreeNode s3 = new TreeNode(3);
         TreeNode s4 = new TreeNode(4);
         TreeNode s5 = new TreeNode(5);
         TreeNode s1 = new TreeNode(1);
         TreeNode s2 = new TreeNode(2);
         TreeNode s0 = new TreeNode(0);
         TreeNode t4 = new TreeNode(4);
         TreeNode t1 = new TreeNode(1);
         TreeNode t2 = new TreeNode(2);
         s3.left = s4; s3.right = s5;
         s4.left = s1; s4.right = s2;
         s2.left = s0;
         t4.left = t1; t4.right = t2;

         System.out.println(new IsSubTree().isSubtree(s3, t4));

     }
}
