package tree;

import common.TreeNode;

public class BinaryTreeLongestConsecutiveSequence {

    int maxLen = 0;
    public int longestConsecutive(TreeNode root) {
        if (root==null) return 0;
        dfs(root, 1);
        return maxLen;
    }
    private void dfs(TreeNode node, int len) {
        maxLen = Math.max(maxLen, len);
        if (node.left!=null) dfs(node.left, node.left.val==node.val+1?len+1:1);
        if (node.right!=null) dfs(node.right, node.right.val==node.val+1?len+1:1);
    }

//    Input
//        {1,#,3,2,4,#,#,#,5}
//        Output
//    4
//        Expected
//    3
//          1
//           \
//            3
//           / \
//           2   4
//                \
//                  5
    public static void main(String[] args) {
        TreeNode n1 = new TreeNode(1);
        TreeNode n2 = new TreeNode(2);
        TreeNode n3 = new TreeNode(3);
        TreeNode n4 = new TreeNode(4);
        TreeNode n5 = new TreeNode(5);
        n1.left=null;n1.right=n3;
        n3.left=n2;n3.right=n4;
        n4.right=n5;
        System.out.println(new BinaryTreeLongestConsecutiveSequence().longestConsecutive(n1));


    }

}
