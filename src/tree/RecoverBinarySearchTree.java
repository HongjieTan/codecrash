package tree;

import common.TreeNode;

import java.util.Stack;

/**
 * Created by thj on 24/05/2018.
 */
public class RecoverBinarySearchTree {

    public void recoverTreeMorris(TreeNode root) {

        TreeNode first=null, second=null, pre=null;

        TreeNode tempNode = null;
        while (root!=null) {
            if (root.left == null) {

                // visit
                if (pre != null && root.val < pre.val) {
                    if (first == null) {first=pre;second=root;}
                    else {second = root;}
                }
                pre = root;

                root = root.right;
            } else {
                tempNode = root.left;
                while(tempNode.right!=null && tempNode.right!=root) {
                    tempNode = tempNode.right;
                }
                if (tempNode.right!=null) { // the threading already exists
                    tempNode.right = null; // recover it to keep origin tree structure

                    // visit
                    if (pre != null && root.val < pre.val) {
                        if (first == null) {first=pre;second=root;}
                        else {second = root;}
                    }
                    pre = root;

                    root = root.right;
                } else { // do threading
                    tempNode.right = root;
                    root = root.left;

                }
            }
        }

        if (first!=null && second!=null) {
            int tempVal = first.val;
            first.val = second.val;
            second.val = tempVal;
        }
    }



    public void recoverTree(TreeNode root) {

        TreeNode first=null, second=null, pre=null;
        Stack<TreeNode> stack = new Stack<>();

        // iterative: O(n)/O(n)
        while(root!=null || !stack.isEmpty()) {
            while (root!=null) {
                stack.push(root);
                root = root.left;
            }
            root = stack.pop();

            // visit
            if (pre != null && root.val < pre.val) {
                if (first == null) {first=pre;second=root;}
                else {second = root;}
            }
            pre = root;

            root = root.right;
        }

        int temp = first.val;
        first.val = second.val;
        second.val = temp;

    }


}
