package tree.Topic_CasesPostOrder;

import common.TreeNode;

import java.util.*;

/**
 *      brute force: O(n^2)
 *      linear solution: O(n), just notice the traversal order, Post Order!
 */
public class FindDuplicateSubTrees {
    Map<String, TreeNode> map = new HashMap<>();
    public List<TreeNode> findDuplicateSubtrees(TreeNode root) {
        StringBuffer path = new StringBuffer();
        visit(root, path);
        List<TreeNode> res = new ArrayList<>();
        for (String key: map.keySet()) {
            if (map.get(key)!=null) res.add(map.get(key));
        }
        return res;
    }
    private void visit(TreeNode node, StringBuffer path) {
        if (node == null) {
            path.append("NULL").append(",");
        } else {
            StringBuffer leftPath = new StringBuffer();
            visit(node.left, leftPath);
            StringBuffer rightPath = new StringBuffer();
            visit(node.right, rightPath);

            // operation in post order here!!
            path.append(node.val).append(leftPath).append(rightPath);
            if (!map.containsKey(path.toString())) map.put(path.toString(), null);
            else map.put(path.toString(), node);
        }
    }
}
