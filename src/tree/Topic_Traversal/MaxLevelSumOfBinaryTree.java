// Given the root of a binary tree, the level of its root is 1, the level of its children is 2, and so on.
//
// Return the smallest level X such that the sum of all the values of nodes at level X is maximal.
//
//
//
// Example 1:
//
//
//
//
//Input: [1,7,0,7,-8,null,null]
//Output: 2
//Explanation:
//Level 1 sum = 1.
//Level 2 sum = 7 + 0 = 7.
//Level 3 sum = 7 + -8 = -1.
//So we return the level with the maximum sum which is level 2.
//
//
//
//
// Note:
//
//
// The number of nodes in the given tree is between 1 and 10^4.
// -10^5 <= node.val <= 10^5
//
// Related Topics Graph

package tree.Topic_Traversal;


import common.TreeNode;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
public class MaxLevelSumOfBinaryTree {

    // bfs: level-order-traversal
    public int maxLevelSum(TreeNode root) {
        Queue<TreeNode> q = new LinkedList<>();
        int level=1, maxsum=root.val, maxlevel=1;
        q.add(root);
        while(!q.isEmpty()) {
            int levelsize = q.size();
            int sum = 0;
            for(int i=0;i<levelsize;i++) {
                TreeNode cur = q.remove();
                sum += cur.val;
                if(cur.left!=null) q.add(cur.left);
                if(cur.right!=null) q.add(cur.right);
            }
            if(sum > maxsum) {
                maxsum = sum;
                maxlevel = level;
            }
            level++;
        }
        return maxlevel;
    }

    // dfs: skip

    public static void main(String[] as) {
        TreeNode n1 = new TreeNode(1);
        TreeNode n2 = new TreeNode(7);
        TreeNode n3 = new TreeNode(0);
        n1.left=n2;n1.right=n3;
        TreeNode n4 = new TreeNode(7);
        TreeNode n5 = new TreeNode(-8);
        n2.left=n4;n2.right=n5;
        System.out.println(new MaxLevelSumOfBinaryTree().maxLevelSum(n1));
    }
}
