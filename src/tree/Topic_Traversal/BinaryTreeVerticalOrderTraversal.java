package tree.Topic_Traversal;

import common.TreeNode;

import java.util.*;


/**
 *  Level order + HashMap :  O(n)
 */
public class BinaryTreeVerticalOrderTraversal {

    private class PNode {
        TreeNode node;
        int col;
        public PNode(TreeNode node, int col) {
            this.node = node;
            this.col = col;
        }
    }

    public List<List<Integer>> verticalOrder(TreeNode root) {
        Map<Integer, List<TreeNode>> colMap = new HashMap<>();
        int minCol = Integer.MAX_VALUE, maxCol = Integer.MIN_VALUE;

        Queue<PNode> que = new LinkedList<>();
        que.add(new PNode(root, 0));

        while (!que.isEmpty()) {
            PNode pnode = que.remove();
            int col = pnode.col;
            TreeNode node = pnode.node;

            if (!colMap.containsKey(col)) {
                colMap.put(col, new ArrayList<>());
            }
            colMap.get(col).add(node);
            minCol = Math.min(minCol, col);
            maxCol = Math.max(maxCol, col);

            if (node.left!=null) que.add(new PNode(node.left, col-1));
            if (node.right!=null) que.add(new PNode(node.right, col+1));
        }


        List<List<Integer>> res = new ArrayList<>();
        for (int i=minCol; i<=maxCol; i++) {
            List<Integer> vals = new ArrayList<>();
            for (TreeNode node: colMap.get(i)) {
                vals.add(node.val);
            }
            res.add(vals);
        }
        return res;
    }



    // brute force version...
//    Map<TreeNode, Integer> node2col = new HashMap<>();
//    Map<TreeNode, Integer> node2row = new HashMap<>();
//    Map<TreeNode, Integer> node2order = new HashMap<>();
//    int minCol = Integer.MAX_VALUE, maxCol=Integer.MIN_VALUE;
//    int order=0;
//
//    PriorityQueue<TreeNode> heap = new PriorityQueue<>((TreeNode a, TreeNode b)-> {
//        if (node2col.get(a) != node2col.get(b)) return Integer.compare(node2col.get(a), node2col.get(b));
//        else if (node2row.get(a) != node2row.get(b))return Integer.compare(node2row.get(a), node2row.get(b));
//        else return Integer.compare(node2order.get(a), node2order.get(b));
//    });
//
//
//    public List<List<Integer>> verticalOrder(TreeNode root) {
//        // write your code here
//        visit(root, 0, 0);
//        List<List<Integer>> res = new ArrayList<>();
//
//        for (int col=minCol; col<=maxCol; col++) {
//            List<Integer> colRes = new ArrayList<>();
//            while (!heap.isEmpty() && node2col.get(heap.peek()) == col) {
//                colRes.add(heap.poll().val);
//            }
//            res.add(colRes);
//        }
//        return res;
//    }
//
//
//    private void visit(TreeNode node, int row, int col) {
//        if (node==null) return;
//
//        node2col.put(node, col);
//        node2row.put(node, row);
//        node2order.put(node, order++);
//        minCol = Math.min(minCol, col);
//        maxCol = Math.max(maxCol, col);
//        heap.add(node);
//
//
//        visit(node.left, row+1, col-1);
//        visit(node.right, row+1, col+1);
//    }


    public static void main(String[] args){
        TreeNode node3 = new TreeNode(3);
        TreeNode node9 = new TreeNode(9);
        TreeNode node20 = new TreeNode(20);
        TreeNode node15 = new TreeNode(15);
        TreeNode node7 = new TreeNode(7);
        node3.left = node9;
        node3.right = node20;
        node20.left = node15;
        node20.right = node7;
        List<List<Integer>> res = new BinaryTreeVerticalOrderTraversal().verticalOrder(node3);

    }
}
