package tree.Topic_Traversal;

import common.TreeNode;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * Created by thj on 24/05/2018.
 *
 * Iterative solution using stack --- O(n) time and O(n) space;
 * Recursive solution --- O(n) time and O(n) space (considering the spaces of function call stack);
 * Morris traversal (tree threading) --- O(n) time and O(1) space!!!
 */
public class BinaryTreeInorderTraversal {
    // iterative version using stack
    public List<Integer> inorderTraversalIterative(TreeNode root) {
        List<Integer> res = new ArrayList<>();
        Stack<TreeNode> stack = new Stack<>();

        // more understandable code
//        while (root!=null) {
//            stack.push(root);
//            root = root.left;
//        }
//        while(!stack.isEmpty()) {
//            root = stack.pop();
//            res.add(root.val);
//            root = root.right;
//            while (root!=null) {
//                stack.push(root);
//                root = root.left;
//            }
//        }

        // shorter code
        while(root!=null || !stack.isEmpty()) {
            while(root!=null) {
                stack.push(root);
                root = root.left;
            }
            root = stack.pop();
            res.add(root.val);
            root = root.right;
        }

        return res;
    }

    // morris traversal(tree threading) without changing the tree structure!! good!!!!
    public List<Integer> inorderTraversalMorris2(TreeNode root) {
        List<Integer> res = new ArrayList<>();
        TreeNode pre = null;
        while (root!=null) {
            if (root.left == null) {
                res.add(root.val); // visit!
                root = root.right;
            } else {
                pre = root.left;
                while(pre.right!=null && pre.right!=root) {
                    pre = pre.right;
                }
                if (pre.right!=null) { // the threading already exists
                    pre.right = null; // recover it to keep origin tree structure
                    res.add(root.val); // visit!
                    root = root.right;
                } else { // do threading
                    pre.right = root;
                    root = root.left;
                }
            }
        }

        return res;

    }

    // morris traversal(tree threading)
//    public List<Integer> inorderTraversalMorris(TreeNode root) {
//        List<Integer> res = new ArrayList<>();
//
//
//        while (root!=null) {
//            if (root.left == null) {
//                res.add(root.val);
//                root = root.right;
//            } else {
//                TreeNode pre = root.left;
//                while(pre.right!=null) {
//                    pre = pre.right;
//                }
//                pre.right = root;
//                TreeNode tempNode = root.left;
//                root.left = null;
//                root = tempNode;
//            }
//        }
//
//        return res;
//
//    }




    // recursive version:
    public List<Integer> inorderTraversalRecursive(TreeNode root) {
        List<Integer> res = new ArrayList<>();
        visit(root, res);
        return res;
    }

    private void visit(TreeNode root, List<Integer> res) {
        if (root!=null) {
            if (root.left!=null) {
                visit(root.left, res);
            }
            res.add(root.val);
            if (root.right!=null) {
                visit(root.right, res);
            }
        }

    }



}
