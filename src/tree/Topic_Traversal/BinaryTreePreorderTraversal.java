package tree.Topic_Traversal;

import common.TreeNode;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * Created by thj on 24/05/2018.
 */
public class BinaryTreePreorderTraversal {

    // morris method: O(n)/O(1) later

    // recursive version:  O(n)/O(n) skip

    // iterative version: O(n)/O(n)
    public List<Integer> preorderTraversal(TreeNode root) {
        List<Integer> res = new ArrayList<>();
        Stack<TreeNode> stack = new Stack<>();

        if (root!=null) stack.push(root);
        while(!stack.isEmpty()) {
            root = stack.pop();
            res.add(root.val);
            if (root.right != null) {stack.push(root.right);}
            if (root.left != null) {stack.push(root.left);}
        }

        return res;
    }




}
