package tree.Topic_Traversal;

import common.TreeNode;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

/**
 * Created by thj on 2018/7/22.
 *
 *
 *
 *  https://www.lintcode.com/problem/binary-tree-vertical-order-traversal/description
 *
 *
 *  dfs: preorder, inorder, postorder
 *  bfs: level order
 *
 *  Note: for iteration version, consider bfs first!! it is simple.
 */
public class AllTreversal {

    void preorder_recursive(TreeNode node) {
        if (node!=null) {
            System.out.print(node.val); // visit
            preorder_recursive(node.left);
            preorder_recursive(node.right);
        }
    }

    void inorder_recursive(TreeNode node) {
        if (node!=null) {
            inorder_recursive(node.left);
            System.out.print(node.val);
            inorder_recursive(node.right);
        }
    }

    void preOrder_iterative_v1(TreeNode root) {
        Stack<TreeNode> stack = new Stack<>();
        while (root!=null) {
            System.out.print(root.val);// visit
            stack.push(root);
            root = root.left;
        }
        while (!stack.isEmpty()) {
            root = stack.pop();
            root = root.right;
            while (root!=null) {
                System.out.print(root.val);// visit
                stack.push(root);
                root = root.left;
            }
        }
    }

    void preOrder_iterative_v2(TreeNode root) {
        Stack<TreeNode> stack = new Stack<>();
        while (!stack.isEmpty() || root!=null) {
            while (root!=null) {
                System.out.print(root.val); // visit here
                stack.push(root);
                root = root.left;
            }
            root = stack.pop();
            root = root.right;
        }
    }


    // tricky...
    void preOrder_iterative_v3(TreeNode root) {
        Stack<TreeNode> stack = new Stack<>();
        if (root!=null) stack.push(root);
        while (!stack.isEmpty()) {
            root = stack.pop();
            System.out.print(root.val); // visit
            if (root.right!=null) stack.push(root.right);
            if (root.left!=null) stack.push(root.left);
        }
    }

    void inOrder_iterative_v1(TreeNode root) {
        Stack<TreeNode> stack = new Stack<>();
        while (root!=null) {
            stack.push(root);
            root = root.left;
        }
        while (!stack.isEmpty()) {
            root = stack.pop();
            System.out.print(root.val);

            root = root.right;
            while (root!=null) {
                stack.push(root);
                root = root.left;
            }
        }
    }

    void inOrder_iterative_v2(TreeNode root) {
        Stack<TreeNode> stack = new Stack<>();
        while (root!=null || !stack.isEmpty()) {
            while (root!=null) {
                stack.push(root);
                root = root.left;
            }
            root = stack.pop();
            System.out.print(root.val);
            root = root.right;
        }
    }

    // bfs
    void bfs(TreeNode root) {
        LinkedList<TreeNode> que = new LinkedList<>();
        que.add(root);
        while (!que.isEmpty()) {
            TreeNode node = que.poll();
            System.out.print(node.val);
            if (node.left!=null) que.add(node.left);
            if (node.right!=null) que.add(node.right);
        }
    }

    public static void main(String[] args){
        TreeNode n1 = new TreeNode(1);
        TreeNode n2 = new TreeNode(2);
        TreeNode n3 = new TreeNode(3);
        TreeNode n4 = new TreeNode(4);
        TreeNode n5 = new TreeNode(5);
        TreeNode n6 = new TreeNode(6);
        TreeNode n7 = new TreeNode(7);
        n1.left=n2;n1.right=n3;
        n2.left=n4;n2.right=n5;
        n5.left=n6;
        n3.left=n7;
        AllTreversal su = new AllTreversal();
        su.preOrder_iterative_v1(n1);
        System.out.println();
        su.preOrder_iterative_v2(n1);
        System.out.println();
        su.preOrder_iterative_v3(n1);
        System.out.println();
        su.preorder_recursive(n1);
        System.out.println();

        System.out.println();

        su.inorder_recursive(n1);
        System.out.println();
        su.inOrder_iterative_v1(n1);
        System.out.println();
        su.inOrder_iterative_v2(n1);
        System.out.println();

        System.out.println();
        su.bfs(n1);
        System.out.println();

    }




}
