package tree.Topic_Traversal;

import common.TreeNode;

import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

/**
 * Created by thj on 24/05/2018.
 */
public class BinaryTreePostOrderTraversal {

    public List<Integer> postorderTraversal(TreeNode root) {

        List<Integer> res = new LinkedList<>();
        Stack<TreeNode> stack = new Stack<>();

        // opposite to preorder
        if (root!=null) stack.push(root);
        while(!stack.isEmpty()) {
            root = stack.pop();
            res.add(0, root.val);
            if (root.left!=null) {stack.push(root.left); }
            if (root.right!=null) {stack.push(root.right); }
        }

        return res;

    }
}
