package tree.Topic_Traversal;

import common.TreeNode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by thj on 2018/8/7.
 *
 * bfs, level order
 */
public class BinaryTreeZigzagLevelOrderTraversal {
    public List<List<Integer>> zigzagLevelOrder(TreeNode root) {

        if (root==null) return new ArrayList<>();

        List<List<Integer>> ans = new ArrayList<>();
        boolean flip = true;

        LinkedList<TreeNode> que = new LinkedList<>();
        que.add(root);
        while (!que.isEmpty()) {
            int levelSize = que.size();
            Integer[] levelArr = new Integer[levelSize];

            for (int i = 0; i < levelSize; i++) {
                TreeNode node = que.poll();
                levelArr[flip?i:levelSize-1-i] = node.val;

                if (node.left!=null) que.add(node.left);
                if (node.right!=null) que.add(node.right);
            }

            ans.add(Arrays.asList(levelArr));
            flip = !flip;
        }

        return ans;





    }
}
