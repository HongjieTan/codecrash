//Two players play a turn based game on a binary tree. We are given the root of this binary tree, and the number of nodes n in the tree. n is odd, and each node has a distinct value from 1 to n.
//
// Initially, the first player names a value x with 1 <= x <= n, and the second player names a value y with 1 <= y <= n and y != x. The first player colors the node with value x red, and the second player colors the node with value y blue.
//
// Then, the players take turns starting with the first player. In each turn, that player chooses a node of their color (red if player 1, blue if player 2) and colors an uncolored neighbor of the chosen node (either the left child, right child, or parent of the chosen node.)
//
// If (and only if) a player cannot choose such a node in this way, they must pass their turn. If both players pass their turn, the game ends, and the winner is the player that colored more nodes.
//
// You are the second player. If it is possible to choose such a y to ensure you win the game, return true. If it is not possible, return false.
//
//
// Example 1:
//
//
//Input: root = [1,2,3,4,5,6,7,8,9,10,11], n = 11, x = 3
//Output: true
//Explanation: The second player can choose the node with value 2.
//
//
//
// Constraints:
//
//
// root is the root of a binary tree with n nodes and distinct node values from 1 to n.
// n is odd.
// 1 <= x <= n <= 100
//
//

package tree;
import common.TreeNode;

/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
public class BinaryTreeColoringGame {
    public boolean btreeGameWinningMove(TreeNode root, int n, int x) {
        return false;
    }
}

/*
by uwi:
class Solution {
    int max = 0;

    int dfs(TreeNode root, int x, int n)
    {
        if(root == null)return 0;
        int l = dfs(root.left, x, n);
        int r = dfs(root.right, x, n);
        if(root.val == x){
            max = Math.max(max, l);
            max = Math.max(max, r);
            max = Math.max(max, n-l-r-1);
        }
        return l + r + 1;
    }

    public boolean btreeGameWinningMove(TreeNode root, int n, int x) {
        dfs(root, x, n);
        return max > n-max;
    }
}

by lee:
Intuition
The first player colors a node,
there are at most 3 nodes connected to this node.
Its left, its right and its parent.
Take this 3 nodes as the root of 3 subtrees.

The second player just color any one root,
and the whole subtree will be his.
And this is also all he can take,
since he cannot cross the node of the first player.


Explanation
count will recursively count the number of nodes,
in the left and in the right.
n - left - right will be the number of nodes in the "subtree" of parent.
Now we just need to compare max(left, right, parent) and n / 2


Complexity
Time O(N)
Space O(height) for recursion


Java:

    int left, right, val;
    public boolean btreeGameWinningMove(TreeNode root, int n, int x) {
        val = x;
        count(root);
        return Math.max(Math.max(left, right), n - left - right - 1) > n / 2;
    }

    private int count(TreeNode node) {
        if (node == null) return 0;
        int l = count(node.left), r = count(node.right);
        if (node.val == val) {
            left = l;
            right = r;
        }
        return l + r + 1;
    }
C++:

    int left, right, val;
    bool btreeGameWinningMove(TreeNode* root, int n, int x) {
        val = x, n = count(root);
        return max(max(left, right), n - left - right - 1) > n / 2;
    }

    int count(TreeNode* node) {
        if (!node) return 0;
        int l = count(node->left), r = count(node->right);
        if (node->val == val)
            left = l, right = r;
        return l + r + 1;
    }
Python:

    def btreeGameWinningMove(self, root, n, x):
        c = [0, 0]
        def count(node):
            if not node: return 0
            l, r = count(node.left), count(node.right)
            if node.val == x:
                c[0], c[1] = l, r
            return l + r + 1
        return count(root) / 2 < max(max(c), n - sum(c) - 1)

Fun Moment of Follow-up:
Alex and Lee are going to play this turned based game.
Alex draw the whole tree. root and n will be given.
Now Lee says he want to color the first node.

Return true if Lee can ensure his win, otherwise return false
Could you find the set all the nodes, that Lee can ensure he wins the game?



其他表述, from google interview experience:
        在一棵二叉树上玩一个游戏，对手先选择树上一个节点，现在该由你来选择一个点。然后同时开始，从选择的那个点开始，
        向外扩张，只可以去占领其相邻的点(parent and children)，然后从占领的点再去扩张，依次类推，
        但如果相邻的点已经被对方占了，就没法去占领了。谁最终占有的点最多谁获胜。

        input是树root和对手选中的node,返回你是否能胜利

        followup 如果你先选择点，你该选择哪个点能胜利？

        比如下面这个树，如果你的对手选中了8，你选6就能胜利。而如果你选9，对手下一步可以由8扩张到6，对手就赢了。

                1
            2       3
          4   5       6
        7           8   9
                  10      11
                    12  13
                  14      15

public static class TreeOccupyGame {

    boolean canWin(TreeNode root, TreeNode red) {
        int parentPart = countNode(root, red);
        int leftPart = countNode(red.left, null);
        int rightPart = countNode(red.left, null);
        int sum = parentPart+leftPart+rightPart+1;
        int max = Math.max(parentPart, Math.max(leftPart, rightPart));
        return max>(sum-max+1);
    }

    int countNode(TreeNode node, TreeNode red) {
        if (node==red || node==null) return 0;
        return 1+countNode(node.left, red) + countNode(node.right, red);
    }


    TreeNode findNode(TreeNode root, TreeNode red) {
        return null;// easy...
    }
}

*/