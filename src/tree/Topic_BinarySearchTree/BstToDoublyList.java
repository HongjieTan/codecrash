package tree.Topic_BinarySearchTree;

import common.TreeNode;

import java.util.Stack;

/**
 *
 *
 *   inorder, 同时用prev保存前一个node
 *
 *
 *   一个变种是生成循环双项链。。。见 http://www.cnblogs.com/grandyang/p/9615871.html, 只需最后将头尾连起来。。。
 *
 */
public class BstToDoublyList {

    public DoublyListNode bstToDoublyList_iterative(TreeNode root) {
        Stack<TreeNode> stack = new Stack<>();
        DoublyListNode prev=null, head=null;
        while (!stack.isEmpty() || root!=null) {
            while (root!=null) {
                stack.push(root);
                root=root.left;
            }
            root = stack.pop();

            // visit
            DoublyListNode dNode = new DoublyListNode(root.val);
            if (prev==null) {
                head=dNode;
            } else {
                prev.next = dNode;
                dNode.prev = prev;
            }
            prev = dNode;

            root = root.right;
        }
        return head;
    }

    DoublyListNode head=null,prev=null;
    public DoublyListNode bstToDoublyList_recursion(TreeNode root) {
        inorder(root);
        return head;
    }

    void inorder(TreeNode node) {
        if(node==null) return;

        inorder(node.left);

        DoublyListNode curNode = new DoublyListNode(node.val);
        if (prev==null) {
            head=curNode;
        } else {
            prev.next = curNode;
            curNode.prev = prev;
        }
        prev = curNode;

        inorder(node.right);
    }

    class DoublyListNode {
         int val;
         DoublyListNode next, prev;
         DoublyListNode(int val) {
             this.val = val;
             this.next = this.prev = null;
         }
    }

    // first version...code is not concise...
//    DoublyListNode prev;
//    public DoublyListNode bstToDoublyList_v0(TreeNode root) {
//        if(root==null) return null;
//        DoublyListNode cur = new DoublyListNode(root.val);
//
//        DoublyListNode left = bstToDoublyList(root.left);
//
//        if(prev !=null) prev.next=cur;
//        cur.prev = prev;
//        prev = cur;
//
//        cur.next = bstToDoublyList(root.right);
//        if(cur.next!=null) cur.next.prev=cur;
//
//        return left!=null?left:cur;
//    }


    public static void main(String[] args) {
//        TreeNode n1=new TreeNode(1);
//        TreeNode n2=new TreeNode(2);
//        TreeNode n3=new TreeNode(3);
//        TreeNode n4=new TreeNode(4);
//        TreeNode n5=new TreeNode(5);
//        n2.left=n1;n2.right=n3;
//        DoublyListNode res = new tree.Topic_BinarySearchTree.BstToDoublyList().bstToDoublyList(n2);
//        System.out.println("sdaf");
    }

}
// v2. Leetcode 426.Convert Binary Search Tree to Sorted Doubly Linked List
//    private Node pre = null;
//    private Node head = null;
//
//    public Node treeToDoublyList(Node root) {
//        if (root == null) return null;
//
//        inorder(root);
//        head.left = pre;
//        pre.right = head;
//        return head;
//    }
//
//    private void inorder(Node root) {
//        if (root == null) {
//            return;
//        }
//
//        inorder(root.left);
//        if (pre == null) {
//            head = root;
//        }else {
//            pre.right = root;
//            root.left = pre;
//        }
//
//        pre = root;
//
//        inorder(root.right);
//    }