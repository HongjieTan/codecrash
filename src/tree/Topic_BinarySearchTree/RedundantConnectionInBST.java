package tree.Topic_BinarySearchTree;

import common.TreeNode;

/**
  follow-up of LC684 RedundantConnection ,684 RedundantConnectionII

 例子：
         7
      /   \
    5     9
  /  \   /
 3    8
 对于多余边5-8，9-8此处的删除需要有选择，跟之前的题目找到多余边立马不分选择删除有区别

 思路:
 DFS，参数中带着左右边界，返回值为新树的根节点，如果当前节点不在当前树的范围内，返回null删除该边

 */
public class RedundantConnectionInBST {
    public void deleteEdge(TreeNode root) {
        dfs(root, Integer.MIN_VALUE, Integer.MAX_VALUE);
    }

    private TreeNode dfs(TreeNode node, int left, int right) {
        if (node==null) return null;
        if (node.val>=right || node.val<=left) return null;

        node.left = dfs(node.left, left, node.val);
        node.right = dfs(node.right, node.val, right);
        return node;
    }


}
