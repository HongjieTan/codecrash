package tree.Topic_BinarySearchTree;

import common.TreeNode;

import java.util.Stack;

/**
 * Created by thj on 24/05/2018.
 *
 *   inorder, 同时用prev保存前一个node
 */
public class ValidateBinarySearchTree {


    // iterative version !!
    public boolean isValidBST_iterative(TreeNode root) {
        Stack<TreeNode> stack = new Stack<>();
        TreeNode pre = null;
        while (root!=null || !stack.isEmpty()) {
            while (root!=null) {
                stack.push(root);
                root = root.left;
            }

            root = stack.pop();

            if (pre!=null && pre.val >= root.val) return false;
            pre = root;

            root = root.right;
        }
        return true;
    }

    // recursive version
    TreeNode prev = null;
    public boolean isValidBST(TreeNode root) {
        if (root == null) return true;

        if(!isValidBST(root.left)) return false;

        if(prev!=null && root.val <= prev.val) return false;
        prev = root;

        if(!isValidBST(root.right)) return false;

        return true;
    }

}
