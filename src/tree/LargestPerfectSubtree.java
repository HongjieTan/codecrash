package tree;

import common.TreeNode;

/**
 * Created by thj on 2018/10/2.
 *
 *   https://www.jiuzhang.com/qa/7516/
 */
public class LargestPerfectSubtree {

    int max = Integer.MIN_VALUE;

    // O(n)!!!!
    public int solution(TreeNode treeNode) {
        LPS(treeNode);
        return max;
    }

    private int LPS(TreeNode root) {
        if (root == null) return 0;
        if (root.left == null && root.right == null ) return 1;

        int leftLPS = LPS(root.left);
        int rightLPS = LPS(root.right);

        int lps = Math.min(leftLPS, rightLPS)*2 + 1;
        max = Math.max(max, lps);
        return lps;
    }

    // brute force: O(n^2)
//    public int solution_brute_force(TreeNode treeNode) {
//        Stack<TreeNode> stack = new Stack<>();
//        int max = Integer.MIN_VALUE;
//
//        stack.push(treeNode);
//        while (!stack.isEmpty()) {
//            TreeNode node = stack.pop();
//            max = Math.max(max, (int)Math.pow(2, LPS(node))-1);
//            if (node.right!=null) stack.push(node.right);
//            if (node.left!=null) stack.push(node.left);
//        }
//        return max;
//    }
//
//    private int LPS(TreeNode root) {
//        if (root == null) return 0;
//
//        int depth = 0;
//        linkedlist_queue<TreeNode> que = new linkedlist_queue<>();
//        que.add(root);
//        while (!que.isEmpty()) {
//            ++depth;
//            int levelSize = que.size();
//            if (levelSize!=Math.pow(2, depth-1)) return depth-1;
//
//            for (int i = 0; i < levelSize; i++) {
//                TreeNode curNode = que.poll();
//                if (curNode.left!=null) que.add(curNode.left);
//                if (curNode.right!=null) que.add(curNode.right);
//            }
//        }
//
//        return depth;
//    }

    public static void main(String[] args) {
        TreeNode n1 = new TreeNode(1);
        TreeNode n2 = new TreeNode(2);
        TreeNode n3 = new TreeNode(3);
        TreeNode n4 = new TreeNode(4);
        TreeNode n5 = new TreeNode(5);
        TreeNode n6 = new TreeNode(6);
        TreeNode n7 = new TreeNode(7);
        TreeNode n8 = new TreeNode(8);
        TreeNode n9 = new TreeNode(9);
        TreeNode n10 = new TreeNode(10);
        TreeNode n11 = new TreeNode(11);

        n1.left = n2; n1.right = n3;
        n2.right = n4;
        n3.left = n5; n3.right = n6;
        n5.left = n7; n5.right = n8;
        n6.left = n9; n6.right = n10;
        n10.left = n11;

        System.out.println(new LargestPerfectSubtree().solution(n1));

    }
}
