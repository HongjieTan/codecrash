package bitmanipulation;

/**
 * Created by thj on 28/05/2018.
 */
public class SingleNumber {
    public int singleNumber(int[] nums) {
        int res = 0;
        for (int i=0; i<nums.length;i++) {
            res ^= nums[i];
        }
        return res;
    }
}
