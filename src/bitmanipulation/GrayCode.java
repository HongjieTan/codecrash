package bitmanipulation;

/**
 *
 * TODO ???
 *
 *
 Given two hexadecimal numbers find if they can be consecutive in gray code
 For example: 10001000, 10001001
 return 1
 since they are successive in gray code

 Example2: 10001000, 10011001
 return -1
 since they are not successive in gray code.
 */
public class GrayCode {

    public static int judge(byte term1, byte term2) {
        //term1和term2是题目给的两个BYTE
        byte x = (byte)(term1 ^ term2);
        int total = 0;
        while(x != 0){
            x = (byte) (x & (x - 1));
            total++;
        }
        if(total == 1) return 1; else return 0;
    }


}
