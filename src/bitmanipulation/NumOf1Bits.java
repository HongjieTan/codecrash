package bitmanipulation;

/**
 * Created by thj on 2018/8/22.
 *
 */
public class NumOf1Bits {

    // 2. trick way / good!!!   remember it!!!
    public int hammingWeight_v2(int n) {
        int count=0;
        while (n!=0) {
            count++;
            n = n&(n-1); // flip last 1 to 0,  ex. n: 0000110 -> 0000110 & 000101 = 0000100!!!
        }
        return count;
    }

    // 1. direct way
    public int hammingWeight_v1(int n) {
        int mask = 1;
        int count = 0;

        for (int i = 0; i < 32; i++) {
            if ((n & mask) != 0) count++;
            mask<<=1;
        }

        return count;
    }

    // ...
    public int hammingWeight_v0(int n) {
        int count=0;
        while (n!=0) {
            if ((n & 1)==1) count++;
            n>>>=1; // notice!! use >>> instead of >>
        }
        return count;
    }




}
