package bitmanipulation;

import java.util.HashMap;
import java.util.Map;

public class ReverseBits {

    // Follow up: If this function is called many times, how would you optimize it?
    // cache version...
    Map<Byte, Integer> cache = new HashMap<>();
    public int reverseBits_followup(int n) {
        byte[] bytes = new byte[4];
        for (int i = 0; i < 4; i++)
            bytes[i]= (byte) ((n>>>8*i)&0xFF);
        int res=0;
        for (int i = 0; i < 4; i++) {
            res<<=8;
            res+=reverseByte(bytes[i]);
        }
        return res;
    }

    int reverseByte(byte b) {
        if (cache.containsKey(b)) return cache.get(b);
        int value = 0;
        for (int i = 0; i < 8; i++) {
            value<<=1;
            value += ((b >>> i) & 1);
//          value += (b&1); b >>>= 1;  // TODO 为何换成这种写法就不对？？？
        }
        cache.put(b, value);
        return value;
    }

    private int reverseBytev2(byte b) {
        Integer value = cache.get(b); // first look up from cache
        if (value != null)
            return value;
        value = 0;
        // reverse by bit
        for (int i = 0; i < 8; i++) {
            value += ((b >>> i) & 1);
            if (i < 7)
                value <<= 1;
        }
        cache.put(b, value);
        return value;
    }

    public int reverseBits_v1(int n) {
        int res = 0;
        for (int i = 0; i < 32; i++) {
            res<<=1;
            res+=n&1;
            n>>>=1;
        }
        return res;
    }

    public static void main(String[] args) {
        byte b = 0x7f;
        byte rev = (byte) new ReverseBits().reverseByte(b);
        System.out.println(rev);

    }
}

/*
Reverse bits of a given 32 bits unsigned integer.



Example 1:

Input: 00000010100101000001111010011100
Output: 00111001011110000010100101000000
Explanation: The input binary string 00000010100101000001111010011100 represents the unsigned integer 43261596, so return 964176192 which its binary representation is 00111001011110000010100101000000.
Example 2:

Input: 11111111111111111111111111111101
Output: 10111111111111111111111111111111
Explanation: The input binary string 11111111111111111111111111111101 represents the unsigned integer 4294967293, so return 3221225471 which its binary representation is 10101111110010110010011101101001.


Note:

Note that in some languages such as Java, there is no unsigned integer type. In this case, both input and output will be given as signed integer type and should not affect your implementation, as the internal binary representation of the integer is the same whether it is signed or unsigned.
In Java, the compiler represents the signed integers using 2's complement notation. Therefore, in Example 2 above the input represents the signed integer -3 and the output represents the signed integer -1073741825.


Follow up:

If this function is called many times, how would you optimize it?
*/
