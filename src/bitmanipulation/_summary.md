tricks:

- set k-th bit to 1
        x |= 1<<k


- flip k-th bit
        x ^= 1<<k


- count bits in integer
        int bits = Integer.bitCount(f);


- count distinct characters in string
        String s = "xxyz";
        int f = 0;
        for(char c : s.toCharArray()){
            f |= 1<<c;
        }
        int distinctCharCount = Integer.bitCount(f);