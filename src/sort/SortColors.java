package sort;



public class SortColors {

    // one pass! good!
    /*
        quicksort 3-way partition
        +------+---------+------------+--------+
        |  <p  |  =p     |  unseen .  |   > p  |
        +------+---------+------------+--------+
                ↑          ↑           ↑
                lt         i           gt
        lt: 1st elem == pivot
        i:  1st unseen elem
        gt: last unseen elem
    */
    public void sortColors(int[] nums) {
        int l=0, r=nums.length-1, i=0;
        while (i<=r) {
            if (nums[i]==0) swap(nums, i++, l++);
            else if (nums[i]==2) swap(nums, i, r--);
            else i++;
        }
    }

    // two pass
    public void sortColors_2pass(int[] nums) {
        int count[] = new int[3];
        for(int x: nums) count[x]++;
        for (int i = 0; i < nums.length; i++) {
            if (i<count[0]) nums[i]=0;
            else if (i<count[0]+count[1]) nums[i]=1;
            else nums[i]=2;
        }
    }


    private void swap(int[] nums, int i, int j) {
        int temp = nums[i];
        nums[i] = nums[j];
        nums[j] = temp;
    }
}
