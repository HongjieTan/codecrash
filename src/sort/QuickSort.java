package sort;

import java.util.Random;

/**
 * Created by thj on 2018/7/27.
 */
public class QuickSort {


    // avg:O(nlogn), worst:O(n^2),  to avoid worst case, we should shuffle input first!!!
    public void sortArray(int[] nums) {
        shuffle(nums); // notice!
        quickSort(nums, 0, nums.length-1);
    }

    public void quickSort(int[] nums, int l, int r) {
        if (l>=r) return;
        int mid = partition(nums, l, r);
        quickSort(nums, l, mid-1);
        quickSort(nums, mid+1, r);
    }

    // concise code version ! good!
    private int partition(int[] nums, int l, int r) {
        int pivot = l;
        while (l<=r) {
            while (l<=r && nums[l]<=nums[pivot]) l++;
            while (l<=r && nums[r]>=nums[pivot]) r--;
            if (l>r) break;
            swap(nums, l, r);
        }
        swap(nums, pivot, r);
        return r;
    }

    private void swap(int[] nums, int a, int b) {
        int  temp = nums[a];
        nums[a] = nums[b];
        nums[b] = temp;
    }

    private void shuffle(int[] nums) {
        Random random = new Random();
        for (int i = 0; i < nums.length; i++) {
            swap(nums, i, random.nextInt(nums.length));
        }
    }

    // code is not concise...
//    private int partition(int[] nums, int start, int end) {
//        int l=start, r=end;
//        int pivot = nums[l];
//        boolean fromRight = true;
//        while (l<r) {
//            if (fromRight) {
//                if (nums[r]>pivot) {
//                    r--;
//                } else {
//                    swap(nums, l++, r);
//                    fromRight=!fromRight;
//
//                }
//            } else {
//                if (nums[l]<pivot) {
//                    l++;
//                } else {
//                    swap(nums, l, r--);
//                    fromRight=!fromRight;
//                }
//            }
//        }
//
//        return l;
//    }



    public static void main(String[] args) {
        int[] nums = new int[]{1,4,2,4,9,5,1,33,14,8};
        new QuickSort().quickSort(nums, 0, nums.length-1);

        for (int i = 0; i < nums.length; i++) {
            System.out.println(nums[i]);
        }
    }



}
