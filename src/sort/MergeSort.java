package sort;


/**
 *
 *  average： O(nlogn)
 *  worst：O（nlogn）
 *
 */
public class MergeSort {
    /**
     *  Bottom-Up version
     */
    public void mergeSort_v2(int[] A) {
        int[] B = new int[A.length]; // work array
        for (int width = 1; width < A.length; width *= 2) {
            for (int i = 0; i < A.length; i += 2 * width) {
                int l1 = i, r1 = i + width - 1;
                int l2 = i + width, r2 = i + 2 * width - 1;
                if (l2<A.length) {
                    merge(A, l1, r1, l2, Math.min(r2, A.length-1), B);
                }
            }
        }
    }

    void merge(int[] A, int l1, int r1, int l2, int r2, int[] B) {
        int p1 = l1, p2 = l2, pb = l1;
        while (p1<=r1 && p2<=r2) {
            if (A[p1]<A[p2]) B[pb++]=A[p1++];
            else B[pb++]=A[p2++];
        }
        while (p1<=r1) B[pb++]=A[p1++];
        while (p2<=r2) B[pb++]=A[p2++];
        // copy back
        for (int i = l1; i <= r2; i++) A[i]=B[i];
    }

    /**
     *  Top-Down version
     */
    public void mergeSort_v1(int[] A) {
        int[] B = new int[A.length]; // work array
        sort(A, 0, A.length-1, B);
    }
    void sort(int[] A, int l, int r, int[] B) {
        if (l>=r) return;
        int mid = l + (r-l)/2;
        sort(A, l, mid, B);
        sort(A, mid+1, r, B);
        merge(A, l, mid, mid+1, r, B);
    }

//    void merge(int[] A, int l1, int r1, int l2, int r2, int[] B) {
//        int p1 = l1, p2 = l2, pb = l1;
//        while (p1<=r1 && p2<=r2) {
//            if (A[p1]<A[p2]) B[pb++]=A[p1++];
//            else B[pb++]=A[p2++];
//        }
//        while (p1<=r1) B[pb++]=A[p1++];
//        while (p2<=r2) B[pb++]=A[p2++];
//        // copy back
//        for (int i = l1; i <= r2; i++) A[i]=B[i];
//    }


    public static void main(String[] args) {
        int[] arr = new int[]{2,3,5,1,0,7,2};
//        new MergeSort().mergeSort_v1(arr);
        new MergeSort().mergeSort_v2(arr);
        for (int x:arr) System.out.print(x+" ");
    }
}

