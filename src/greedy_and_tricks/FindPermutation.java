package greedy_and_tricks;


/**
 *
 * 思路： 起始设置成最小permutation[1,2,...n]，满足了所有的I, 然后对于连续的D，全部reverse
 *
 * ref: https://leetcode.com/articles/find-permutation/
 *
 */
public class FindPermutation {

    public int[] findPermutation(String s) {
        int n=s.length()+1;
        int[] res = new int[n];
        for(int i=0;i<n;i++) res[i]=i+1;

        int p=0;
        while(p<s.length()){
            if(s.charAt(p)=='D') {
                int r = p;
                while(r<s.length() && s.charAt(r)=='D') r++;
                reverse(res, p, r);
                p=r;
            } else {
                p++;
            }
        }
        return res;
    }

    void reverse(int[] arr, int l, int r) {
        while(l<=r) {
            int temp = arr[l];
            arr[l] = arr[r];
            arr[r] = temp;
            l++;r--;
        }
    }



    public static void main(String[] args) {
        int[] res = new FindPermutation().findPermutation("DIDIDID");
        for (int x: res) System.out.println(x);
    }
}
/*
By now, you are given a secret signature consisting of character 'D' and 'I'.
'D' represents a decreasing relationship between two numbers, 'I' represents an increasing relationship between two numbers.
And our secret signature was constructed by a special integer array, which contains uniquely all the different number from 1 to n
(n is the length of the secret signature plus 1).

For example, the secret signature "DI" can be constructed by array [2,1,3] or [3,1,2],
but won't be constructed by array [3,2,4] or [2,1,3,4], which are both illegal constructing special string that can't represent the "DI" secret signature.

On the other hand, now your job is to find the lexicographically smallest permutation of [1, 2, ... n] could refer to the given secret signature in the input.

The input string will only contain the character 'D' and 'I'.
The length of input string is a positive integer and will not exceed 10,000.

Example 1:
	Input:  str = "DI"
	Output:  [2,1,3]

Example 2:
	Input: str = "I"
	Output:  [1,2]

*/