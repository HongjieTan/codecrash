package greedy_and_tricks;

import java.util.*;

/**
 *  greedy + heap
 */
public class RearrangeStringKDistanceApart {
    public String rearrangeString(String str, int k) {
        Map<Character, Integer> freq = new HashMap<>();
        for (char c: str.toCharArray()) freq.put(c, freq.getOrDefault(c,0)+1);
        PriorityQueue<Character> que = new PriorityQueue<>((a,b)->{
            if (freq.get(a)!=freq.get(b))
                return Integer.compare(freq.get(b),freq.get(a));
            return Integer.compare(a,b);
        });
        que.addAll(freq.keySet());
        StringBuffer sb = new StringBuffer();
        LinkedList<Character> cacheQue = new LinkedList<>();
        for (int i = 0; i < str.length(); i++) {
            if (que.isEmpty()) return "";
            char curChar = que.remove();
            sb.append(curChar);
            freq.put(curChar, freq.get(curChar)-1);
            cacheQue.add(curChar);
            if (cacheQue.size()==k) {
                char c = cacheQue.remove();
                if (freq.get(c)>0) que.add(c);
            }
        }
        return sb.toString();
    }

    public static void main(String[] args) {
        System.out.println("->"+new RearrangeStringKDistanceApart().rearrangeString("aabbcc", 3));
        System.out.println("->"+new RearrangeStringKDistanceApart().rearrangeString("aaabc", 3));
    }
}

/*
Given a non-empty string s and an integer k, rearrange the string such that the same characters are at least distance k from each other.
All input strings are given in lowercase letters. If it is not possible to rearrange the string, return an empty string "".

Example 1:
s = "aabbcc", k = 3
Result: "abcabc"
The same letters are at least distance 3 from each other.

Example 2:
s = "aaabc", k = 3
Answer: ""
It is not possible to rearrange the string.

Example 3:
s = "aaadbbcc", k = 2
Answer: "abacabcd"
Another possible answer is: "abcabcda"
The same letters are at least distance 2 from each other.

 */