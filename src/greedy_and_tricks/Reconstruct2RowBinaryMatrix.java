// Given the following details of a matrix with n columns and 2 rows :
//
//
// The matrix is a binary matrix, which means each element in the matrix can be 0 or 1.
// The sum of elements of the 0-th(upper) row is given as upper.
// The sum of elements of the 1-st(lower) row is given as lower.
// The sum of elements in the i-th column(0-indexed) is colsum[i], where colsum is given as an integer array with length n.
//
//
// Your task is to reconstruct the matrix with upper, lower and colsum.
//
// Return it as a 2-D integer array.
//
// If there are more than one valid solution, any of them will be accepted.
//
// If no valid solution exists, return an empty 2-D array.
//
//
// Example 1:
//
//
//Input: upper = 2, lower = 1, colsum = [1,1,1]
//Output: [[1,1,0],[0,0,1]]
//Explanation: [[1,0,1],[0,1,0]], and [[0,1,1],[1,0,0]] are also correct answers.
//
//
// Example 2:
//
//
//Input: upper = 2, lower = 3, colsum = [2,2,1,1]
//Output: []
//
//
// Example 3:
//
//
//Input: upper = 5, lower = 5, colsum = [2,1,2,0,1,0,1,2,0,1]
//Output: [[1,1,1,0,1,0,0,1,0,0],[1,0,1,0,0,0,1,1,0,1]]
//
//
//
// Constraints:
//
//
// 1 <= colsum.length <= 10^5
// 0 <= upper, lower <= colsum.length
// 0 <= colsum[i] <= 2
//
//

package greedy_and_tricks;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Reconstruct2RowBinaryMatrix {

    // O(n)
    public List<List<Integer>> reconstructMatrix(int upper, int lower, int[] colsum) {
        int n = colsum.length;
        Integer[] row1 = new Integer[n], row2 = new Integer[n];
        Arrays.fill(row1, 0);
        Arrays.fill(row2, 0);

        int sum1=0, sum2=0;
        for (int i = 0; i < n; i++) {
            if(colsum[i]==2) {
                row1[i]=row2[i]=1;
                sum1++;
                sum2++;
            }
        }

        if (sum1>upper || sum2>lower) return new ArrayList<>();


        for (int i = 0; i < n; i++) {
            if(colsum[i]==1) {
                if(sum1<upper) {
                    row1[i]=1;
                    sum1++;
                } else if(sum2<lower) {
                    row2[i]=1;
                    sum2++;
                } else {
                    return new ArrayList<>();
                }
            }
        }
        if(sum1!=upper || sum2!=lower) return new ArrayList<>();
        return Arrays.asList(Arrays.asList(row1),Arrays.asList(row2));
    }


    public static void main(String[] as) {
        int up = 5;
        int lo = 5;
        int[] colsum = new int[]{2,1,2,0,1,0,1,2,0,1};
        Reconstruct2RowBinaryMatrix su = new Reconstruct2RowBinaryMatrix();
        List<List<Integer>> ret = su.reconstructMatrix(up, lo, colsum);
        System.out.println("asdf");

    }


//    // first version, TLE
//    List<List<Integer>> ret = new ArrayList<>();
//    int n;
//    int[] colsum;
//    int upper, lower;
//    Set<String> seen = new HashSet<>();
//
//    public List<List<Integer>> reconstructMatrix(int upper, int lower, int[] colsum) {
//        this.n = colsum.length;
//        this.colsum = colsum;
//        this.upper = upper;
//        this.lower = lower;
//        ret.add(new ArrayList<>());
//        ret.add(new ArrayList<>());
//
//        if(dfs(0, 0, 0)) return ret;
//        return new ArrayList<>();
//    }
//
//    boolean dfs(int idx, int curupper, int curlower) {
//        String key = idx+"#"+curupper+"#"+curlower;
//        if(seen.contains(key)) return false;
//        if(idx==n) {
//            if(curupper==upper && curlower==lower) return true;
//            else return false;
//        }
//        if(curupper>upper || curlower>lower) return false;
//
//        if(colsum[idx]==0) {
//            ret.get(0).add(0);
//            ret.get(1).add(0);
//            if(dfs(idx+1, curupper, curlower)) return true;
//            ret.get(0).remove(idx);
//            ret.get(1).remove(idx);
//        }
//
//        if(colsum[idx]==1){
//            // case1. add uppper
//            ret.get(0).add(1);
//            ret.get(1).add(0);
//            if(dfs(idx+1, curupper+1, curlower)) return true;
//            ret.get(0).remove(idx);
//            ret.get(1).remove(idx);
//
//            // case2. add lower
//            ret.get(0).add(0);
//            ret.get(1).add(1);
//            if(dfs(idx+1, curupper, curlower+1)) return true;
//            ret.get(0).remove(idx);
//            ret.get(1).remove(idx);
//        }
//
//        if(colsum[idx]==2) {
//            ret.get(0).add(1);
//            ret.get(1).add(1);
//            if(dfs(idx+1, curupper+1, curlower+1)) return true;
//            ret.get(0).remove(idx);
//            ret.get(1).remove(idx);
//        }
//
//        seen.add(key);
//        return false;
//    }
}


