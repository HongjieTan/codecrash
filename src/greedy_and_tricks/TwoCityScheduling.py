
'''
- quickselect + greedy: O(n)
- sort + greedy: O(nlogn)
- dp + greedy: O(n^2)
'''
class ArticulationPoints_And_Bridges(object):

    # greedy with quickselect: quick select first half,  O(n)!!!
    def twoCitySchedCost_quickselect(self, costs):
        def quick_select(arr, k):
            l, r = 0, len(arr) - 1
            while l <= r:
                p = patition(arr, l, r)
                if p == k: break
                elif p < k: l = p + 1
                else: r = p - 1

        def patition(arr, l, r):
            pivot = l
            while l <= r:
                while l <= r and arr[l][0] - arr[l][1] <= arr[pivot][0] - arr[pivot][1]: l += 1
                while l <= r and arr[r][0] - arr[r][1] >= arr[pivot][0] - arr[pivot][1]: r -= 1
                if l > r: break
                swap(arr, l, r)
            swap(arr, pivot, r)
            return r

        def swap(arr, a, b):
            tmp = arr[a]
            arr[a] = arr[b]
            arr[b] = tmp

        n = len(costs)//2
        quick_select(costs, n)
        return sum([x[0] for x in costs[:n]]) + sum([x[1] for x in costs[n:]])


    # greedy with sort: O(nlogn)
    def twoCitySchedCost_sort(self, costs):
        costs.sort(key=lambda x: x[0] - x[1])
        return sum([x[0] for x in costs[:len(costs) / 2]]) + sum([x[1] for x in costs[len(costs) // 2:]])


    # dp: O(n^2)
    def twoCitySchedCost_dp(self, costs):
        n = len(costs)//2
        dp = {(0,0):0} # dp[i,j] denotes cost when first i+j people in which i persons go to A and j persons go to B
        for i in range(1, n+1):  dp[i, 0] = dp[i-1, 0] + costs[i-1][0]
        for i in range(1, n+1):  dp[0, i] = dp[0, i-1] + costs[i-1][1]
        for i in range(1, n+1):
            for j in range(1, n+1):
                dp[i,j] = min(dp[i-1,j]+costs[i+j-1][0], dp[i,j-1]+costs[i+j-1][1])
        return dp[n,n]


# costs = [[10, 20], [30, 200], [400, 50], [30, 20]]
costs = [[259,770],[448,54],[926,667],[184,139],[840,118],[577,469]]
print ArticulationPoints_And_Bridges().twoCitySchedCost_quickselect(costs)
print ArticulationPoints_And_Bridges().twoCitySchedCost_sort(costs)
print ArticulationPoints_And_Bridges().twoCitySchedCost_dp(costs)

'''
There are 2N people a company is planning to interview. The cost of flying the i-th person to city A is costs[i][0],
and the cost of flying the i-th person to city B is costs[i][1].

Return the minimum cost to fly every person to a city such that exactly N people arrive in each city.

Example 1:

Input: [[10,20],[30,200],[400,50],[30,20]]
Output: 110
Explanation:
The first person goes to city A for a cost of 10.
The second person goes to city A for a cost of 30.
The third person goes to city B for a cost of 50.
The fourth person goes to city B for a cost of 20.

The total minimum cost is 10 + 30 + 50 + 20 = 110 to have half the people interviewing in each city.


Note:

1 <= costs.length <= 100
It is guaranteed that costs.length is even.
1 <= costs[i][0], costs[i][1] <= 1000


dp[i][j] represents the cost when considering first (i + j) people in which i people assigned to city A and j people assigned to city B.

class ArticulationPoints_And_Bridges {
    public int twoCitySchedCost(int[][] costs) {
        int N = costs.length / 2;
        int[][] dp = new int[N + 1][N + 1];
        for (int i = 1; i <= N; i++) {
            dp[i][0] = dp[i - 1][0] + costs[i - 1][0];
        }
        for (int j = 1; j <= N; j++) {
            dp[0][j] = dp[0][j - 1] + costs[j - 1][1];
        }
        for (int i = 1; i <= N; i++) {
            for (int j = 1; j <= N; j++) {
                dp[i][j] = Math.min(dp[i - 1][j] + costs[i + j - 1][0], dp[i][j - 1] + costs[i + j - 1][1]);
            }
        }
        return dp[N][N];
    }
}




def twoCitySchedCost(self, costs: List[List[int]]) -> int:
        N = len(costs) // 2
        dic = {(0, 0): 0}

        for cost in costs:
            newdic = {}
            for i, j in dic.keys():
                if i + 1 <= N:
                    newdic[i+1, j] = min(newdic.get((i+1, j), float('inf')), dic[i,j] + cost[0])
                if j + 1 <= N:
                    newdic[i, j+1] = min(newdic.get((i, j+1), float('inf')), dic[i,j] + cost[1])
            dic = newdic
        return dic[N, N]
'''
