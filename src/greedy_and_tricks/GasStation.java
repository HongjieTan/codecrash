package greedy_and_tricks;

/**
 * Created by thj on 2018/9/19.
 *
 *  1. brute force: O(n^2)
 *  2. greedy_and_tricks!!!: O(n), need to prove it!!
 *
 *     Two ideas:
 *     1) If A can not reach B, any station(A can reach) between A and B can not reach B!
 *
 *          Prove:  A->...->Ci->...->B,  given:  A can reach Ci, A can not reach B
 *                  Ci can reach B => A can reach B, which is conflict with condition
 *                  So Ci can can not reach B!
 *
 *     2）Total gas is bigger(>=) than Total cost  <=>  There must be a solution.
 *
 *          Prove:  <=, obvious
 *                  =>, Prove by using contradiction!!!(the hard point of this problem)
 *                      Say if total gas - total cost >= 0 there is no solution. that means from any gas station we can't travel around circuit.
 *                      we can randomly pick a station to start, eg gas station i, we know it will stops at somewhere say gas station j (j is the last station that is reachable from i).
 *                      For this interval i to j we know sum from i to j (gas[x] - cost[x]) < 0.
 *                      Next we start from gas station j + 1, it will also stops at somewhere say k, we also have sum from j+1 to k (gas[x] - cost[x]) <0.
 *                      we repeat this process until we already travel a circle.
 *                      We pick out those non-overlapped intervals but also can fully form the circle.
 *                      Based on our analysis, if we sum the value of interval total gas - interval total cost for these segments .
 *                      we got total gas - total cost < 0, which contradicts our assumption.
 *
 */
public class GasStation {

    // greedy_and_tricks!  O(n) solution！
    public int canCompleteCircuit(int[] gas, int[] cost) {
        int startIndex = 0;
        int tank = 0;
        int total = 0;

        for (int i = 0; i < gas.length; i++) {
            int delta = gas[i] - cost[i];
            total+=delta;
            tank+=delta;
            if (tank<0) {
                startIndex=i+1;
                tank = 0;
            }
        }

        return total<0?-1:startIndex;
    }



    public int canCompleteCircuit_brute_force(int[] gas, int[] cost) {

        int bestStart=0, max=Integer.MIN_VALUE;


        for (int i = 0; i < gas.length; i++) {
            int val = gas[i] - cost[i];
            if (val > max) {
                bestStart = i;
                max = val;
            }
        }

        if (canComplete(bestStart, gas, cost)) return bestStart;
        return -1;


//        for (int i = 0; i < gas.length; i++) {
//            if (canComplete(i, gas, cost))
//                return i;
//        }

//        return -1;
    }


    private boolean canComplete(int startIndex, int[] gas, int[] cost) {
        int stations = gas.length;
        int pos = startIndex;

        int tank = gas[pos]-cost[pos];
        if (tank<0) return false;
        pos = (startIndex+1)%stations;

        while (pos!=startIndex) {
            tank = tank + gas[pos] - cost[pos];
            if (tank<0) return false;
            pos = (pos+1)%stations;
        }
        return true;
    }

    public static void main(String[] args) {
//        int[] gas = new int[]{5,1,2,3,4};
//        int[] cost = new int[]{4,4,1,5,1};
        int[] gas = new int[]{1,2,3,4,5};
        int[] cost = new int[]{3,4,5,1,2};
        System.out.println();
        System.out.println(new GasStation().canCompleteCircuit(gas, cost));
    }

}
