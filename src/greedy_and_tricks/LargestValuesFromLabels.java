//We have a set of items: the i-th item has value values[i] and label labels[i].
//
// Then, we choose a subset S of these items, such that:
//
//
// |S| <= num_wanted
// For every label L, the number of items in S with label L is <= use_limit.
//
//
// Return the largest possible sum of the subset S.
//
//
//
//
// Example 1:
//
//
//Input: values = [5,4,3,2,1], labels = [1,1,2,2,3], num_wanted = 3, use_limit = 1
//Output: 9
//Explanation: The subset chosen is the first, third, and fifth item.
//
//
//
// Example 2:
//
//
//Input: values = [5,4,3,2,1], labels = [1,3,3,3,2], num_wanted = 3, use_limit = 2
//Output: 12
//Explanation: The subset chosen is the first, second, and third item.
//
//
//
// Example 3:
//
//
//Input: values = [9,8,8,7,6], labels = [0,0,0,1,1], num_wanted = 3, use_limit = 1
//Output: 16
//Explanation: The subset chosen is the first and fourth item.
//
//
//
// Example 4:
//
//
//Input: values = [9,8,8,7,6], labels = [0,0,0,1,1], num_wanted = 3, use_limit = 2
//Output: 24
//Explanation: The subset chosen is the first, second, and fourth item.
//
//
//
//
// Note:
//
//
// 1 <= values.length == labels.length <= 20000
// 0 <= values[i], labels[i] <= 20000
// 1 <= num_wanted, use_limit <= values.length
//
//
//
// hint:
// Consider the items in order from largest to smallest value, and greedily take the items if they fall under the use_limit.
// We can keep track of how many items of each label are used by using a hash table.
//
package greedy_and_tricks;

import java.util.*;


// hashtable + greedy
public class LargestValuesFromLabels {

    public int largestValsFromLabels(int[] values, int[] labels, int num_wanted, int use_limit) {
        int n = values.length;
        List<int[]> l = new ArrayList<>();
        for(int i=0;i<n;i++) l.add(new int[]{labels[i], values[i]});
        Collections.sort(l, (a, b)->b[1]-a[1]);

        int sum=0;
        Map<Integer, Integer> counter = new HashMap<>();
        for(int[] x: l) {
            if(counter.getOrDefault(x[0], 0)< use_limit) {
                sum += x[1];
                counter.put(x[0], counter.getOrDefault(x[0],0)+1);
                num_wanted--;
                if(num_wanted <=0) break;
            }
        }
        return sum;
    }


    /*
    def largestValsFromLabels(self, values: List[int], labels: List[int], num_wanted: int, use_limit: int) -> int:
        l = [(p,q) for p,q in zip(labels, values)]
        l.sort(key=lambda x:x[1], reverse=True)
        counter = collections.defaultdict(int)
        sum = 0
        for k,v in l:
            if counter[k]<use_limit:
                counter[k]+=1
                sum+=v
                num_wanted-=1
                if num_wanted == 0: break
        return sum

    */


    // 2 nested pq,  nlogk + l*logn
    public int largestValsFromLabels_v0(int[] values, int[] labels, int num_wanted, int use_limit) {
        int n=values.length;
        Map<Integer, TreeSet<int[]>> map = new HashMap<>();
        for(int i=0;i<n;i++) {
            int k=labels[i], v=values[i];
            if(!map.containsKey(k)) map.put(k, new TreeSet<>((a,b)->a[0]!=b[0]?a[0]-b[0]:a[1]-b[1]));
            map.get(k).add(new int[]{v,i});
            if(map.get(k).size()>use_limit)
                map.get(k).pollFirst();
        }

        PriorityQueue<TreeSet<int[]>> pq = new PriorityQueue<>((a, b)->b.last()[0]-a.last()[0]);
        for(TreeSet<int[]> t: map.values()) pq.add(t);
        int sum=0;
        while(num_wanted>0 && !pq.isEmpty()) {
            TreeSet<int[]> cur = pq.remove();
            sum += cur.pollLast()[0];
            num_wanted--;
            if(cur.size()>0) pq.add(cur);
        }
        return sum;
    }


    public static void main(String[] a) {
        int values[] = {3,7,2,7,2}, labels[] = {2,2,2,2,1}, num_wanted = 2, use_limit = 5; // 14
        System.out.println(new LargestValuesFromLabels().largestValsFromLabels(values, labels, num_wanted, use_limit));
    }
}

/*
by uwi:
public int largestValsFromLabels(int[] values, int[] labels, int num_wanted, int use_limit) {
    int n = values.length;
    int[][] ai = new int[n][];
    for(int i = 0;i < n;i++){
        ai[i] = new int[]{values[i], labels[i]};
    }
    Arrays.sort(ai, new Comparator<int[]>() {
        public int compare(int[] a, int[] b) {
            return a[0] - b[0];
        }
    });
    int[] f = new int[50000];
    int sum = 0;
    int used = 0;
    for(int i = n-1;i >= 0;i--){
        if(used < num_wanted && ++f[ai[i][1]] <= use_limit){
            sum += ai[i][0];
            used++;
        }
    }
    return sum;
}
*/