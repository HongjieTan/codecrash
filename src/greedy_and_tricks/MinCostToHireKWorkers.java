package greedy_and_tricks;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.PriorityQueue;


/**
 *
 *  greedy + heap:   O(logN)
 *
 *  ratio[i] = w[i]/q[i]
 *  In Summary:
 *      - Each Candidate Group: 每次新加入一个ratio（按ratio从小到大加）， 把group中quality最大的剔除
 *      - The Result:           min cost(cost = ratio*sumOfQuality) of all candidate groups
 *  The key:  "For Every Ratio find the minimum total quality of K workers"!!
 *
 *
 *  1. 每一个合法的pay group只能取最大的wage/quality当作所有工人的pay ratio。
 *  2. 直接根据ratio排序，然后又需要每个group的pay最小，每个group的总工资计算方法（q1+q2+q3+...+qk）* ratio，所以就是需要quality的sum最小，
 *     那每次我们加入了一个新的ratio就把quality最大worker踢出group，这样每次group的pay可以保证是新ratio下的最小pay。
 *     遍历数组，记录所有ratio中出现的pay最小值即可。
 *
 */
public class MinCostToHireKWorkers {
    class Worker{
        int q;
        int w;
        double rate;
        public Worker(int q, int w) {
            this.q=q;
            this.w=w;
            this.rate=w*1d/q;
        }
    }

    public double mincostToHireWorkers(int[] quality, int[] wage, int K) {
        int n=wage.length;
        List<Worker> workers = new ArrayList<>();
        for(int i=0;i<n;i++)  workers.add(new Worker(quality[i], wage[i]));
        Collections.sort(workers, (a, b)->Double.compare(a.rate, b.rate));

        double ans = Double.MAX_VALUE;
        int sumQ=0;
        PriorityQueue<Worker> group = new PriorityQueue<>((a,b)->b.q-a.q);
        for(Worker worker: workers) {
            sumQ+=worker.q;
            group.add(worker);
            if(group.size()==K) {
                ans = Math.min(ans, worker.rate * sumQ); // current work.rate is the rate of the group...
                sumQ-=group.remove().q; // remove biggest q in the group each time...
            }
        }
        return ans;

    }


//    public double mincostToHireWorkers(int[] quality, int[] wage, int K) {
//        double[] ratio = new double[quality.length];
//        for(int i=0; i<quality.length;i++) ratio[i]=wage[i]*1d/quality[i];
//
//        PriorityQueue<Integer> ratioPQ = new PriorityQueue<>((a,b)->Double.compare(ratio[a], ratio[b]));
//        for (int i = 0; i < quality.length; i++) ratioPQ.add(i);
//
//        double ans = Double.MAX_VALUE;
//        double curMaxRatio;
//        int sumQuality=0;
//        PriorityQueue<Integer> qualityPQ = new PriorityQueue<>((a,b)->Integer.compare(quality[b], quality[a]));
//        while (!ratioPQ.isEmpty()) {
//            int worker = ratioPQ.remove(); // 1. Add worker with smallest ratio each time!
//
//            curMaxRatio = ratio[worker]; // increase each time
//            sumQuality += quality[worker];
//            qualityPQ.add(worker);
//            if (qualityPQ.size()==K) {
//                ans = Math.max(ans, curMaxRatio*sumQuality);
//
//                sumQuality -= quality[qualityPQ.remove()]; // 2. Delete worker with largest quality each time!!
//            }
//        }
//        return ans;
//
//    }

    public static void main(String[] args) {
        int[] quality = new int[]{3,1,10,10,1};
        int[] wage =    new int[]{4,8,2,2,7};
        int K = 3;
        System.out.println(new MinCostToHireKWorkers().mincostToHireWorkers(quality, wage, K));
    }


}
