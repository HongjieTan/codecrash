// There are some chips, and the i-th chip is at position chips[i].
//
// You can perform any of the two following types of moves any number of times (possibly zero) on any chip:
//
//
// Move the i-th chip by 2 units to the left or to the right with a cost of 0.
// Move the i-th chip by 1 unit to the left or to the right with a cost of 1.
//
//
// There can be two or more chips at the same position initially.
//
// Return the minimum cost needed to move all the chips to the same position (any position).
//
//
// Example 1:
//
//
//Input: chips = [1,2,3]
//Output: 1
//Explanation: Second chip will be moved to positon 3 with cost 1. First chip will be moved to position 3 with cost 0. Total cost is 1.
//
//
// Example 2:
//
//
//Input: chips = [2,2,2,3,3]
//Output: 2
//Explanation: Both fourth and fifth chip will be moved to position two with cost 1. Total minimum cost will be 2.
//
//
//
// Constraints:
//
//
// 1 <= chips.length <= 100
// 1 <= chips[i] <= 10^9
//

package greedy_and_tricks;

public class PlayWithChips {

    public int minCostToMoveChips(int[] chips) {
        int odd=0, even=0;
        for(int x: chips) {
            if (x%2==1) odd++;
            else even++;
        }
        return Math.min(odd, even);
    }

    // brute force: O(n^2)
//    public int minCostToMoveChips(int[] chips) {
//        Set<Integer> posset = new HashSet<>();
//        for(int x:chips) posset.add(x);
//        int min = Integer.MAX_VALUE;
//        for(int pos:posset) {
//            int cost = 0;
//            for(int x:chips) {
//                int d = Math.abs(x-pos);
//                cost += d%2;
//            }
//            min = Math.min(min, cost);
//        }
//        return min;
//    }
}


/*
by uwi:
public int minCostToMoveChips(int[] chips) {
    int[] f = new int[2];
    for(int v : chips){
        f[v%2]++;
    }
    return Math.min(f[0], f[1]);
}
*/