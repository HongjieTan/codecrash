package greedy_and_tricks;

import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;


/**
 *
 *  greedy + heap:  also check RearrangeStringKDistanceApart
 */
public class DistantBarcodes {
    public int[] rearrangeBarcodes(int[] barcodes) {
        Map<Integer, Integer> map = new HashMap<>();
        int maxCount = 0, maxCode=barcodes[0];
        for(int x: barcodes) {
            map.put(x, map.getOrDefault(x, 0)+1);
            if(map.get(x)>maxCount) {
                maxCount = map.get(x);
                maxCode = x;
            }
        }

        int[] l = new int[barcodes.length];
        int idx=0;

        Queue<int[]> pq = new PriorityQueue<>((a, b)->b[1]-a[1]);

        for(int key: map.keySet()) pq.add(new int[]{key, map.get(key)});

        while(!pq.isEmpty()) {
            int[] max = pq.remove();
            l[idx++] = max[0];
            if(pq.isEmpty()) break;
            int[] max2 = pq.remove();
            l[idx++] = max2[0];

            if(max[1]>1) {max[1]--; pq.add(max);}
            if(max2[1]>1) {max2[1]--; pq.add(max2);}
        }
        return l;

    }
}
