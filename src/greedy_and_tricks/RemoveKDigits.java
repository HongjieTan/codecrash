package greedy_and_tricks;

import java.util.Stack;

/**
 * Created by thj on 04/06/2018.
 *
 *  compare with next permutation
 *
 */
public class RemoveKDigits {

    // O(n) - use stack!!!
    public String removeKdigits(String num, int k) {
        Stack<Character> stack = new Stack<>();
        for (int i=0;i<num.length();i++) {
            while(!stack.isEmpty() && stack.peek()>num.charAt(i) && k>0) {
                stack.pop();
                k--;
            }
            stack.push(num.charAt(i));
        }

        while (k>0) {stack.pop();k--;}

        StringBuffer sb = new StringBuffer();
        while (!stack.isEmpty()) sb.append(stack.pop());
        num = sb.reverse().toString();
        int i=0;
        while (i<num.length() && num.charAt(i) == '0' ) i++; // remove prefix '0'
        return i==num.length() ? "0":num.substring(i, num.length());

    }



//    // O(kn)
//    public String removeKdigits(String num, int k) {
//        while (k>0) {
//            num = remove1digit(num);
//            k--;
//        }
//
//        int i=0;
//        while (i<num.length() && num.charAt(i) == '0' ) i++;
//        return i==num.length() ? "0":num.substring(i, num.length());
//    }
//
//    // O(n)
//    private String remove1digit(String num) {
//        if (num.length()<=1) return "0";
//        int i=0;
//        while(i<num.length()-1 && num.charAt(i) <= num.charAt(i+1)) i++;
//        return num.substring(0,i)+num.substring(i+1, num.length());
//    }
}
