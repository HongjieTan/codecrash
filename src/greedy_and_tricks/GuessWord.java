package greedy_and_tricks;

import java.util.ArrayList;
import java.util.List;

/**
 *
 *  策略题:
 *
 *  1. 过滤策略： 每次猜完后，过滤wordlist只保留matches相同的word
 *
 *  2. 选词策略：(key is 每次选一个剪枝能力最强的词来猜！)
 *          - 策略0; 每次随机选词...not good
 *
 *          - 策略1（best）(mimax...)
 *                  选择剪枝效果最好的词，该词剪枝效果定义为matches为各种可能（0,1,...,6）中最差的剪枝能力
 *                   1.1 如果选择word[i]来猜, 其可能过滤出的候选词为: possibleGroup[i][matches], matches:0,1,...,6. (possibleGroup越小越好,接下去的搜索空间越小）
 *                       最差情况是:maxPossibleGroup[i]= max{possibleGroup[i][matches]}
 *                   1.2 选词标准是 minimize上述最差情况, 即  minimize{ maxPossibleGroup[i], i=0...n-1 }
 *
 *          - 策略2:
 *                  选择剪枝效果最好的词，该词剪枝效果定义为matches为0时的剪枝能力， 算是策略1 的一种特例
 *                   2.1 每次猜词最坏情况：matches为0时我们减少搜索空间的速度最慢, 故选择matches=0作为标准 （貌似有待商榷？-> OK, 因为任意两个word的matches为0的概率(25/26)^6=79% ,...）
 *                   2.2 每次取和其他单词matches为0的次数最少的词！
 *
 */
public class GuessWord {

    // 策略1
    public void findSecretWord(String[] wordlist, Master master) {
        int n = wordlist.length;
        int[][] M = new int[wordlist.length][wordlist.length];
        for(int i=0; i<n; i++) {
            for(int j=i+1;j<n;j++) {
                int matches = 0;
                for(int k=0;k<6;k++) {
                    if(wordlist[i].charAt(k)==wordlist[j].charAt(k)) matches++;
                }
                M[i][j]=matches;
                M[j][i]=matches;
            }
        }

        List<Integer> possible = new ArrayList<>();
        for(int i=0;i<n;i++) possible.add(i);
        for(int i=0;i<10;i++) {
            // choose a word to guess
            int guess = pickGuess(M, possible);
            int m = master.guess(wordlist[guess]);
            if(m==6) return;

            // filter word list
            List<Integer> temp = new ArrayList<>();
            for(int wordIdx: possible){
                if(wordIdx!=guess && M[wordIdx][guess]==m) temp.add(wordIdx);
            }
            possible = temp;
        }
    }

    private int pickGuess(int[][] M, List<Integer> possible) {
        int guess = -1;
        int minMaxGroup = Integer.MAX_VALUE;
        for(int i: possible) {
            int maxGroup = 0;
            for(int m=0;m<=6;m++) {
                int groupCount=0;
                for(int j: possible) {
                    if(j!=i && M[i][j]==m) groupCount++;
                }
                maxGroup = Math.max(maxGroup, groupCount);
            }

            if(maxGroup<minMaxGroup) {
                minMaxGroup = maxGroup;
                guess = i;
            }
        }
        return guess;
    }


    // 策略2
    public void findSecretWord_2(String[] wordlist, Master master) {
        for(int i=0; i<10; i++) {
            // choose a candidate word (who has minimum 0 matches to other words)， O(N^2)
            String word = wordlist[0];
            int minCount = Integer.MAX_VALUE;
            for(String w1: wordlist) {
                int count=0;
                for(String w2: wordlist) {
                    if(matches(w1, w2)==0) count++;
                }
                if(count<minCount) {
                    word = w1;
                    minCount = count;
                }
            }

            // filter word list
            int x = master.guess(word);
            if(x==6) break; // hit
            List<String> filtered = new ArrayList<>();
            for(String w: wordlist) {
                if(w!=word && matches(w, word)==x) {
                    filtered.add(w);
                }
            }
            wordlist = filtered.toArray(new String[0]);
        }

    }

    private int matches(String w1, String w2) {
        int p=0, count=0;
        while(p<w1.length()) {
            if(w1.charAt(p)==w2.charAt(p)) count++;
            p++;
        }
        return count;
    }
}

// This is the greedy_and_tricks.Master's API interface.
// You should not implement it, or speculate about its implementation
interface Master {
    public int guess(String word);
}
/*
This problem is an interactive problem new to the LeetCode platform.

We are given a word list of unique words, each word is 6 letters long, and one word in this list is chosen as secret.

You may call master.guess(word) to guess a word.  The guessed word should have type string and must be from the original list with 6 lowercase letters.

This function returns an integer type, representing the number of exact matches (value and position) of your guess to the secret word.
Also, if your guess is not in the given wordlist, it will return -1 instead.

For each test case, you have 10 guesses to guess the word. At the end of any number of calls, if you have made 10 or less calls to master.guess and at least one of these guesses was the secret, you pass the testcase.

Besides the example test case below, there will be 5 additional test cases, each with 100 words in the word list.
The letters of each word in those testcases were chosen independently at random from 'a' to 'z', such that every word in the given word lists is unique.

Example 1:
Input: secret = "acckzz", wordlist = ["acckzz","ccbazz","eiowzz","abcczz"]

Explanation:

master.guess("aaaaaa") returns -1, because "aaaaaa" is not in wordlist.
master.guess("acckzz") returns 6, because "acckzz" is secret and has all 6 matches.
master.guess("ccbazz") returns 3, because "ccbazz" has 3 matches.
master.guess("eiowzz") returns 2, because "eiowzz" has 2 matches.
master.guess("abcczz") returns 4, because "abcczz" has 4 matches.

We made 5 calls to master.guess and one of them was the secret, so we pass the test case.
Note:  Any solutions that attempt to circumvent the judge will result in disqualification.
*/

/*
class Solution {
    int[][] H;
    public void findSecretWord(String[] wordlist, Master master) {
        int N = wordlist.length;
        H = new int[N][N];
        for (int i = 0; i < N; ++i)
            for (int j = i; j < N; ++j) {
                int match = 0;
                for (int k = 0; k < 6; ++k)
                    if (wordlist[i].charAt(k) == wordlist[j].charAt(k))
                        match++;
                H[i][j] = H[j][i] = match;
            }

        List<Integer> possible = new ArrayList();
        List<Integer> path = new ArrayList();
        for (int i = 0; i < N; ++i) possible.add(i);

        while (!possible.isEmpty()) {
            int guess = solve(possible, path);
            int matches = master.guess(wordlist[guess]);
            if (matches == wordlist[0].length()) return;
            List<Integer> possible2 = new ArrayList();
            for (Integer j: possible) if (H[guess][j] == matches) possible2.add(j);
            possible = possible2;
            path.add(guess);
        }

    }

    public int solve(List<Integer> possible, List<Integer> path) {
        if (possible.size() <= 2) return possible.get(0);
        List<Integer> ansgrp = possible;
        int ansguess = -1;

        for (int guess = 0; guess < H.length; ++guess) {
            if (!path.contains(guess)) {
                ArrayList<Integer>[] groups = new ArrayList[7];
                for (int i = 0; i < 7; ++i) groups[i] = new ArrayList<Integer>();
                for (Integer j: possible) if (j != guess) {
                    groups[H[guess][j]].add(j);
                }

                ArrayList<Integer> maxgroup = groups[0];
                for (int i = 0; i < 7; ++i)
                    if (groups[i].size() > maxgroup.size())
                        maxgroup = groups[i];

                if (maxgroup.size() < ansgrp.size()) {
                    ansgrp = maxgroup;
                    ansguess = guess;
                }
            }
        }

        return ansguess;
    }
}
*/