package graph_dfs_bfs;

import java.util.HashSet;
import java.util.Set;

/**

  需要跟面试官讨论清楚他需要的最佳匹配是什么：

  case1: 没有tie，优先安排距离最近的（不需要全局最优）
        pq + hashset: 只需要enumerate出每一个B和每一P 的abs distance，放进priorityqueue即可。然后用一个2个set来记录visited。


  case2: 如果出现了“1”作为obstacles
        以P为中心用bfs来计算到每一个B的距离

  case3： 全局人车距离最短 ？
        二分图的最佳匹配问题，使用匈牙利算法/KM算法？
       （但是鉴于面试的时候可能很难写出，所以在此希望大家讨论一下有没有其他稍微简单点的办法，因为和正常的二分图匹配不一样，这个已经告诉你那些节点属于哪一边了。）
  case4： 最大的人车距离最小 ?
        感觉可以用dp来做，但是没有想出很好的状态表示和转移方程，希望大家讨论？
        or 二分查找？
  case3,case4: 参考https://www.1point3acres.com/bbs/thread-447853-1-1.html


 */
public class BikeAndPerson {
    // basic skip

    // followup using backtrack
    int gmin = Integer.MAX_VALUE;
    int m;
    int n;
    int[][] dist;
    Set<Integer> visited;
    public int minMaxDist(int[][] persions, int[][] bikes) {
        m = persions.length;
        n = bikes.length;
        dist = new int[m][n];
        visited = new HashSet<>();
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                dist[i][j] = getDist(persions[i], bikes[j]);
            }
        }
        return gmin;
    }

    void find(int person, int curMax) { // 如果是全局距离和最小，也是差不多做法。。。
        if (person==m){
            gmin = Math.min(gmin, curMax);
            return;
        }

        for (int i = 0; i < n; i++) {
            if (!visited.contains(i)) {
                visited.add(i);
                find(person+1, Math.max(curMax, dist[person][i]));
                visited.remove(i);
            }
        }
    }

    int getDist(int[] person, int[] bike) {
        return Math.abs(person[0]-bike[0]) + Math.abs(person[1]-bike[1]);
    }
}


/*

2D平面上，有m个人（P），n辆自行车(B)，还有空白（O）满足以下条件
1.m < n
2.不存在两个人，到同一辆自行车距离相等, 距离用abs(x1-x2) + abs(y1-y2)定义
3.每个人尽量找离自己最近的自行车，一旦某辆自行车被占，其他人只能找别的自行车。
例：
OPOBOOP
OOOOOOO
OOOOOOO
OOOOOOO
BOOBOOB
红色的人找到第一行的自行车，距离最近。
蓝色的人离第一行自行车最近，但自行车已经被红色人占有，所以他只能找离他第二近的，右下角的自行车。

问：把人和自行车配对，输出vector<pair<int, int>>每个人对应的自行车. {i, j} 是人i对应自行车j

 */

/*
基本版的解：
# 如果没有障碍物，可以直接计算distance
# persons & bikes 用坐标给出
def match0(persons, bikes):

    def distance(person, bike):
        # 如果有障碍物，我们要用bfs求出(p, b)的距离
        return abs(person[0] - bike[0]) + abs(person[1] - bike[1])


    pq = []
    for i, p in enumerate(persons):
        for j, b in enumerate(bikes):
            pq.append((distance(p, b), i, j))

    import heapq
    heapq.heapify(pq)

    visp = set()
    visb = set()
    matches = {}

    while pq:
        d, i, j = heapq.heappop(pq)
        if i in visp or j in visb:
            continue
        visp.add(i)
        visb.add(j)
        matches[i] = j
    return matches
*/

/*
followup1, followup2 回溯的解：
如果现场想不出来其他更好的方法，有一个最笨的算法总比没有好。。。
这个算法可以用来解决followup1 和followup 2

# 用回溯法求最远距离的最小值
def match1_recursion(persons, bikes):

    def find(p, curmax):
        if p == m:
            gmin[0] = min(gmin[0], curmax)
            return
        for i in range(n):
            if i not in visb:
                visb.add(i)
                find(p + 1, max(curmax, dists[p][i]))
                visb.remove(i)
                matches.pop()
        return


    def distance(person, bike):
        # 如果有障碍物，我们要用bfs求出(p, b)的距离
        return abs(person[0] - bike[0]) + abs(person[1] - bike[1])

    m, n = len(persons), len(bikes)
    dists = []
    for i in range(m):
        dists.append([])
        for j in range(n):
            d = distance(persons[i], bikes[j])
            dists[-1].append(d)

    visb = set()
    gmin = [1 << 32]
    find(0, 0)
    return gmin[0]
*/