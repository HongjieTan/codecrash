package graph_dfs_bfs;

import java.util.List;

public class NestedListWeightSum {
    public int depthSum(List<NestedInteger> nestedList) {
        return depthSum(nestedList, 1);
    }

    private int depthSum(List<NestedInteger> nestedList, int depth) {
        int sum = 0;
        for (NestedInteger ni: nestedList) {
            if(ni.isInteger()){
                sum+=depth*ni.getInteger();
            } else {
                sum+=depthSum(ni.getList(), depth+1);
            }
        }
        return sum;
    }
}
/**
 *     Different from the previous question where weight is increasing from root to leaf,
 *     now the weight is defined from bottom up.
 *     i.e., the leaf level integers have weight 1, and the root level integers have the largest weight.
 */
class NestedListWeightSum2 {

    //... use more space to store <num, depth>, after all traversed we have maxDepth, then ...
}

interface NestedInteger {

    // @return true if this graph_dfs_bfs.NestedInteger holds a single integer,
    // rather than a nested list.
    public boolean isInteger();

    // @return the single integer that this graph_dfs_bfs.NestedInteger holds,
    // if it holds a single integer
    // Return null if this graph_dfs_bfs.NestedInteger holds a nested list
    public Integer getInteger();

    // @return the nested list that this graph_dfs_bfs.NestedInteger holds,
    // if it holds a nested list
    // Return null if this graph_dfs_bfs.NestedInteger holds a single integer
    public List<NestedInteger> getList();
}


