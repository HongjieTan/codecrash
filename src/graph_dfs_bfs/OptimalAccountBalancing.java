package graph_dfs_bfs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 *  dfs:
 *      http://fisherlei.blogspot.com/2017/07/leetcode-optimal-account-balancing.html
 *      http://www.cnblogs.com/grandyang/p/6108158.html
 *
 *  dp:  https://www.jiuzhang.com/qa/5734/
 *
 *  dfs->hire, dp->strong hire...
 */
public class OptimalAccountBalancing {

    // dp:  TODO, check https://www.jiuzhang.com/qa/5734/
    public static int balanceGraph_dp(int[][] edges) {
        return -1;
    }

    // dfs+backtrack:...
    public static int balanceGraph(int[][] edges) {
         Map<Integer, Integer> map = new HashMap<>(); // store debt of each person
         for(int[] edge: edges) {
            int u=edge[0],v=edge[1], w=edge[2];
            map.put(u, map.getOrDefault(u,0)-w);
            map.put(v, map.getOrDefault(v,0)+w);
         }
         List<Integer> debts = new ArrayList<>();
         for(int value: map.values()) {
            if(value!=0) debts.add(value);
         }
         int res = helper(debts, 0);
         return res;
    }

    static int helper(List<Integer> debts, int start) {
        if(start >= debts.size()) return 0;
        while(start<debts.size() && debts.get(start)==0) start++;
        int count=Integer.MAX_VALUE;
        for(int i=start+1; i<debts.size(); i++) {
            if(debts.get(start) * debts.get(i) < 0) { // 是否必须？
                debts.set(i, debts.get(i)+debts.get(start));
                //int store = debts.get(start);
                //debts.set(start, 0);
                count = Math.min(count ,helper(debts, start+1)+1);
                //debts.set(start, store);
                debts.set(i, debts.get(i)-debts.get(start));
            }
        }
        return count==Integer.MAX_VALUE?0:count;
    }

    public static void main(String[] args) {
        OptimalAccountBalancing solution = new OptimalAccountBalancing();
        int[][] edges = new int[][]{
                {0,1,10},{1,0,1},{1,2,5},{2,0,5}
        };
        System.out.println(solution.balanceGraph(edges));
//        System.out.println(solution.minTransfers(edges));
    }

}

/*
Description
Given a directed graph where each edge is represented by a tuple, such as [u, v, w]represents an edge with a weight w from u to v.
You need to calculate at least the need to add the number of edges to ensure that each point of the weight are balancing.
That is, the sum of weight of the edge pointing to this point is equal to the sum of weight of the edge of the point that points to the other point.

1.Note that u ≠ v and w > 0.
2.index may not be linear, e.g. we could have the points 0, 1, 2 or we could also have the points 0, 2, 6.

Have you met this question in a real interview?
Example
For example:
Given a graph [[0,1,10],[2,0,5]]
Return 2
Two edges are need to added. There are [1,0,5] and [1,2,5]

Given a graph [[0,1,10],[1,0,1],[1,2,5],[2,0,5]]
Return 1
Only one edge need to added. There is [1,0,4]
*/