package graph_dfs_bfs;


import java.util.*;

class UndirectedGraphNode {
  int label;
  List<UndirectedGraphNode> neighbors;
  UndirectedGraphNode(int x) { label = x; neighbors = new ArrayList<UndirectedGraphNode>(); }
};

public class CloneGraph {
    public UndirectedGraphNode cloneGraph(UndirectedGraphNode node) {
        Map<Integer, UndirectedGraphNode> visited = new HashMap<>();
        return visit(node, visited);
    }

    private UndirectedGraphNode visit(UndirectedGraphNode node, Map<Integer, UndirectedGraphNode> visited) {
        if (node == null) return null;
        if (visited.containsKey(node.label)) return visited.get(node.label);
        UndirectedGraphNode newNode = new UndirectedGraphNode(node.label);
        visited.put(node.label, newNode);
        if (node.neighbors!=null) {
            for (UndirectedGraphNode neighbor: node.neighbors) {
                UndirectedGraphNode neighborCp = visit(neighbor, visited);
                if (neighborCp!=null) {newNode.neighbors.add(neighborCp);}
            }
        }
        return newNode;
    }
}
