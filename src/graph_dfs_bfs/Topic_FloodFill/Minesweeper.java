package graph_dfs_bfs.Topic_FloodFill;

public class Minesweeper {
    // 'M' represents an unrevealed mine,
    // 'E' represents an unrevealed empty square,
    // 'B' represents a revealed blank square that has no adjacent (above, below, left, right, and all 4 diagonals) mines,
    // digit ('1' to '8') represents how many mines are adjacent to this revealed square, and finally 'X' represents a revealed mine.


    int[][] directions = new int[][]{ {0,1}, {0,-1},{-1,0},{1,0},{-1,-1}, {-1,1},{1,-1},{1,1}};
    public char[][] updateBoard(char[][] board, int[] click) {

        int row=click[0], col=click[1];
        System.out.println(row+","+col);
        if (board[row][col]=='M') {
            board[row][col]='X';
        } else {
            int mines = 0;
            for(int[] direction: directions) {
                int nrow=row+direction[0], ncol=col+direction[1];
                if (nrow>=0 && nrow<board.length && ncol>=0 && ncol<board[0].length) {
                    if (board[nrow][ncol]=='M') mines++;
                }
            }
            if (mines>0) board[row][col]=(char)(mines+'0');
            else {
                board[row][col]='B';
                for(int[] direction: directions) {
                    int nrow=row+direction[0], ncol=col+direction[1];
                    if (nrow>=0 && nrow<board.length && ncol>=0 && ncol<board[0].length
                        && board[nrow][ncol]=='E') {
                        updateBoard(board, new int[]{row+direction[0], col+direction[1]});
                    }
                }
            }
        }
        return board;
    }


    public static void main(String[] args) {
        char[][] board = new char[][]
                {
                {'E','E','E','E','E'},
                {'E','E','M','E','E'},
                {'E','E','E','E','E'},
                {'E','E','E','E','E'}};
        new Minesweeper().updateBoard(board, new int[]{3,0});
        System.out.println(board);
    }
}
