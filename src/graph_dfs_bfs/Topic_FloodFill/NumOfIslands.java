package graph_dfs_bfs.Topic_FloodFill;

/**
 * Created by thj on 2018/8/1.
 */
public class NumOfIslands {

    public int numIslands(char[][] grid) {
        if (grid==null || grid.length==0) return 0;
        boolean[][] visited = new boolean[grid.length][grid[0].length];

        int count = 0;

        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[0].length; j++) {
                if (!visited[i][j] && grid[i][j]=='1') {
                    count++;
                    dfs(i,j,grid,visited);
                }
            }
        }

        return count;
    }

    private void dfs(int row, int col, char[][] grid, boolean[][] visited) {
        if (!visited[row][col] && grid[row][col]=='1') {
            visited[row][col] = true;

            if (isValid(row-1, col, grid)) dfs(row-1, col, grid, visited);
            if (isValid(row+1, col, grid)) dfs(row+1, col, grid, visited);
            if (isValid(row, col-1, grid)) dfs(row, col-1, grid, visited);
            if (isValid(row, col+1, grid)) dfs(row, col+1, grid, visited);
        }
    }

    private boolean isValid(int row, int col, char[][] grid) {
        return row>=0&&row<grid.length&&col>=0&&col<grid[0].length;
    }


    public static void main(String[] args) {
        char[][] grid = {
                {'1','1','1','1','0'},
                {'1','1','0','1','0'},
                {'1','1','0','0','0'},
                {'0','0','0','0','0'}};
        System.out.println(new NumOfIslands().numIslands(grid));
    }

}
