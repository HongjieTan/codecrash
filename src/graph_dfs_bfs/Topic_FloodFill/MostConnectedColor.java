package graph_dfs_bfs.Topic_FloodFill;

/**
 * Created by thj on 15/06/2018.
 *
 *
 *  [[1,1,2,2,1],
 *   [1,2,2,3,1],
 *   [1,2,3,3,4]]
 *
 *  most connected color: 5
 *
 *
 */
public class MostConnectedColor {
    public int mostContectedColor(int[][] image) {
        boolean[][] visited = new boolean[image.length][image[0].length];
        int max=0;
        for (int i = 0; i < image.length; i++) {
            for (int j = 0; j < image[0].length; j++) {
                max = Math.max(getConnected(image, i,j, visited, image[i][j]), max);
            }
        }
        return max;
    }

    private int getConnected(int[][] image, int row, int col, boolean[][] visited, int color) {
        if (visited[row][col] || image[row][col]!=color) {
            return 0;
        }
        visited[row][col]=true;
        int top=0, left=0, right=0, down=0;
        if (row>0) {top=getConnected(image, row-1, col, visited, color);}
        if (col>0) {left=getConnected(image, row, col-1, visited, color);}
        if (row<image.length-1) {down=getConnected(image, row+1, col, visited, color);}
        if (col<image[0].length-1) {right=getConnected(image, row, col+1, visited, color);}
        return 1+top+left+right+down;
    }
}
