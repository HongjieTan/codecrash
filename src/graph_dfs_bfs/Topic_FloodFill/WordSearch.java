package graph_dfs_bfs.Topic_FloodFill;

/**
 * Created by thj on 18/06/2018.
 *
 *  dfs+backtrack!!
 *
 */
public class WordSearch {

    public boolean exist(char[][] board, String word) {
        boolean[][] visited = new boolean[board.length][board[0].length];
        for(int row = 0; row<board.length; row++) {
            for(int col=0; col<board[0].length; col++) {
                if(dfs(board, row, col, word, 0, visited)) return true;
            }
        }
        return false;
    }

    // more concise...
    private boolean dfs(char[][] board, int row, int col, String word, int index,
                        boolean[][] visited) {
        if(visited[row][col]) return false;
        if(board[row][col] != word.charAt(index)) return false;
        if(index == word.length()-1) return true;

        visited[row][col] = true;
        int[][] dirs = new int[][]{{0,1},{0,-1},{1,0},{-1,0}};
        for(int[] dir: dirs) {
            int nrow = row+dir[0];
            int ncol = col+dir[1];
            if(nrow<board.length && nrow >=0 && ncol<board[0].length && ncol>=0
                    && dfs(board, nrow, ncol, word, index+1, visited)) return true;
        }
        visited[row][col] = false; // backtrack!!!
        return false;
    }


    private boolean dfs_v1(char[][] board, int row, int col, String word, int wordIdx, boolean[][] visited) {
        if (visited[row][col]) return false;
        if (wordIdx==word.length()-1 && board[row][col] == word.charAt(wordIdx)) return true;
        if (board[row][col] == word.charAt(wordIdx)) {
            visited[row][col] = true;
            if (isValid(row-1, col, board) && dfs_v1(board, row-1, col, word, wordIdx+1, visited)) return true;
            if (isValid(row+1, col, board) && dfs_v1(board, row+1, col, word, wordIdx+1, visited)) return true;
            if (isValid(row, col-1, board) && dfs_v1(board, row, col-1, word, wordIdx+1, visited)) return true;
            if (isValid(row, col+1, board) && dfs_v1(board, row, col+1, word, wordIdx+1, visited)) return true;
            visited[row][col] = false; // backtrack...
        }
        return false;
    }

    private boolean isValid(int row, int col, char[][] board) {
        return row>=0&&row<board.length
                && col>=0&&col<board[0].length;
    }

}
