package graph_dfs_bfs.Topic_FloodFill;

/**
 *  just dfs!!
 *
 */
public class SurroundedRegions {


    // clean version!!
    public void solve(char[][] board) {
        if (board==null || board.length==0) return;

        // dfs from 4 border
        for (int row = 0; row < board.length; row++) {
            dfs(board, row, 0);
            dfs(board, row, board[0].length-1);
        }
        for (int col = 0; col < board[0].length; col++) {
            dfs(board, 0, col);
            dfs(board, board.length-1, col);
        }


        // scan and update
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[0].length; j++) {
                if (board[i][j]=='O') board[i][j] = 'X'; // capture
                if (board[i][j]=='M') board[i][j] = 'O'; // revert back
            }
        }

    }


    private void dfs(char[][] board, int row, int col) {
        if (board[row][col] == 'O') {
            board[row][col] = 'M';
            int[][] dirs = new int[][]{{-1,0},{1,0},{0,-1},{0,1}};
            for (int[] dir: dirs) {
                if (isValid(board, row+dir[0], col+dir[1]))
                    dfs(board, row+dir[0], col+dir[1]);
            }
        }
    }

    private boolean isValid(char[][] board, int row, int col) {
        return row>=0&&row<board.length&&col>=0&&col<board[0].length;
    }



    // my first version...
//    public void solve(char[][] board) {
//        if (board==null || board.length==0) return;
//        boolean[][] visited = new boolean[board.length][board[0].length];
//        for (int i = 0; i < board.length; i++) {
//            for (int j = 0; j < board[0].length; j++) {
//                if (board[i][j] == 'X' || visited[i][j]) continue;
//                List<int[]> path = new ArrayList<>();
//                if (dfs(board, path, visited, i, j)) {
//                    for(int[] pos: path) {
//                        board[pos[0]][pos[1]] = 'X';
//                    }
//                }
//            }
//        }
//    }
//
//    private boolean dfs(char[][] board, List<int[]> path, boolean[][] visited, int row, int col) {
//        if (board[row][col]=='O' && (row == 0 || row==board.length-1 || col==0 || col == board[0].length-1)) return false; // reach border case
//        if (board[row][col]=='X') return true;
//
//        visited[row][col] = true;
//        path.add(new int[]{row, col});
//        int[][] directions = new int[][]{{-1, 0}, {1, 0}, {0, 1}, {0, -1}};
//        boolean isSurrounded = true;
//        for (int[] direction: directions) {
//            int newrow = row + direction[0];
//            int newcol = col + direction[1];
//            if (!visited[newrow][newcol] && !dfs(board, path, visited, row+direction[0], newcol+direction[1])) isSurrounded = false;
//        }
//        return isSurrounded;
//    }


    public static void main(String[] args) {
        char[][] board = {
                {'X','X','X','X'},
                {'X','O','O','X'},
                {'X','X','O','X'},
                {'X','O','X','X'}};

        new SurroundedRegions().solve(board);
    }
}
