package graph_dfs_bfs.Topic_FloodFill;


public class NumberOfClosedIslands {

    // v1, dfs
    int n, m;
    public int closedIsland(int[][] grid) {
        this.n = grid.length;
        this.m = grid[0].length;
        int cnt = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                if (grid[i][j] == 0) {
                    if (dfs(i, j, grid)) cnt++;
                }
            }
        }
        return cnt;
    }

    boolean dfs(int x, int y, int[][] grid) {
        boolean closed = true;
        grid[x][y] = -1;
        int[][] dirs = {{1, 0}, {-1, 0}, {0, -1}, {0, 1}};
        for (int[] dir : dirs) {
            int nx = x + dir[0], ny = y + dir[1];
            if (nx < 0 || nx >= n || ny < 0 || ny >= m) closed = false;
            if (nx >= 0 && nx < n && ny >= 0 && ny < m && grid[nx][ny] == 0) {
                closed &= dfs(nx, ny, grid);
            }
        }
        return closed;
    }


    // v2, union find
    public int closedIsland_v2(int[][] grid) {
        int n=grid.length, m=grid[0].length;
        DJSet ds = new DJSet(n*m+1);
        for(int i=0;i<n;i++) {
            for(int j=0;j<m;j++) {
                if(i+1<n && grid[i][j]==0 && grid[i+1][j]==0) ds.union(i*m+j, (i+1)*m+j);
                if(j+1<m && grid[i][j]==0 && grid[i][j+1]==0) ds.union(i*m+j, i*m+j+1);
                if(i==0 || i==n-1) ds.union(i*m+j, n*m);
                if(j==0 || j==m-1) ds.union(i*m+j, n*m);
            }
        }

        int ret=0;
        for(int i=0;i<n;i++) {
            for(int j=0;j<m;j++) {
                if(ds.parents[i*m+j]==i*m+j && grid[i][j]==0 && !ds.isConnected(i*m+j, n*m)) {
                    ret++;
                }
            }
        }
        return ret;
    }

    class DJSet{
        int[] parents;
        public DJSet(int n) {
            parents = new int[n];
            for(int i=0;i<n;i++) parents[i]=i;
        }
        public int find(int x) {
            return parents[x]==x? x: (parents[x]=find(parents[x]));
        }
        public void union(int x, int y) {
            x = find(x);
            y = find(y);
            if (x!=y) {
                parents[y] = x;
            }
        }
        public boolean isConnected(int x, int y) {
            return find(x)==find(y);
        }
    }
}

/*
by uwi:
class Solution {
    public int closedIsland(int[][] a) {
        int n = a.length, m = a[0].length;
        DJSet ds = new DJSet(n*m+1);
        for(int i = 0;i < n;i++){
            for(int j = 0;j < m;j++){
                if(j+1 < m && a[i][j] + a[i][j+1] == 0){
                    ds.union(i*m+j, i*m+j+1);
                }
                if(i+1 < n && a[i][j] + a[i+1][j] == 0){
                    ds.union(i*m+j, (i+1)*m+j);
                }
                if(i == 0 || i == n-1 || j == 0 || j == m-1){
                    ds.union(i*m+j, n*m);
                }
            }
        }
        int ret = 0;
        for(int i = 0;i < n*m;i++){
            if(ds.upper[i] < 0 && !ds.equiv(i, n*m) && a[i/m][i%m] == 0){
                ret++;
            }
        }
        return ret;
    }

    public class DJSet {
        public int[] upper;

        public DJSet(int n) {
            upper = new int[n];
            Arrays.fill(upper, -1);
        }

        public int root(int x) {
            return upper[x] < 0 ? x : (upper[x] = root(upper[x]));
        }

        public boolean equiv(int x, int y) {
            return root(x) == root(y);
        }

        public boolean union(int x, int y) {
            x = root(x);
            y = root(y);
            if (x != y) {
                if (upper[y] < upper[x]) {
                    int d = x;
                    x = y;
                    y = d;
                }
                upper[x] += upper[y];
                upper[y] = x;
            }
            return x == y;
        }

        public int count() {
            int ct = 0;
            for (int u : upper)
                if (u < 0)
                    ct++;
            return ct;
        }
    }

}

*/