package graph_dfs_bfs.Topic_FloodFill;

import java.util.Stack;

/**
 * Created by thj on 14/06/2018.
 *
 *
 *  dfs
 *
 *  iterative version?
 *
 */
public class FloodFill {

    class Node {
        int row;
        int col;
        public Node(int row, int col) {this.row = row; this.col=col;}
    }

    // iterative version!!
    public int[][] floodFill_v2(int[][] image, int sr, int sc, int newColor) {
        if (newColor!=image[sr][sc]) {
            Stack<Node> stack = new Stack<>();
            stack.push(new Node(sr, sc));
            while(!stack.isEmpty()) {
                Node item = stack.pop();
                int row = item.row, col=item.col;
                if (image[row][col] != newColor) {
                    if (isValid(row-1, col, image) && isSameColor(row-1, col, row, col, image)) {stack.push(new Node(row-1, col)); }
                    if (isValid(row, col-1, image) && isSameColor(row, col-1, row, col, image)) {stack.push(new Node(row, col-1)); }
                    if (isValid(row+1, col, image) && isSameColor(row+1, col, row, col, image)) {stack.push(new Node(row+1, col)); }
                    if (isValid(row, col+1, image) && isSameColor(row, col+1, row, col, image)) {stack.push(new Node(row, col+1)); }
                    image[row][col] = newColor;
                }
            }
        }
        return image;
    }

    private boolean isValid(int row, int col, int[][] image) {
        return row>=0 && row<image.length && col>=0 && col<image[0].length;
    }

    private boolean isSameColor(int r1, int c1, int r2, int c2, int[][] image) {
        return image[r1][c1] == image[r2][c2];
    }

    public int[][] floodFill(int[][] image, int sr, int sc, int newColor) {
        if (newColor!=image[sr][sc]) {
            fill(image, sr, sc, image[sr][sc], newColor);
        }
        return image;
    }

    private void fill(int[][] image, int row, int col, int startColor, int newColor) {
        if (image[row][col] == startColor) {
            image[row][col] = newColor;
            if (isValid(row-1, col, image)) {fill(image, row-1, col, startColor, newColor);}
            if (isValid(row, col-1, image)) {fill(image, row, col-1, startColor, newColor);}
            if (isValid(row+1, col, image)) {fill(image, row+1, col, startColor, newColor);}
            if (isValid(row, col+1, image)) {fill(image, row, col+1, startColor, newColor);}
        }
    }



}
