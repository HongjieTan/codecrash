/*
Description
You want to build a house on an empty land which reaches all buildings in the shortest amount of distance.
You can only move up, down, left and right. You are given a 2D grid of values 0, 1 or 2, where:
·Each **0** marks an empty land which you can pass by freely.
·Each **1** marks a building which you cannot pass through.
·Each **2** marks an obstacle which you cannot pass through.
Example
For example, given three buildings at (0,0), (0,4), (2,2), and an obstacle at (0,2):

1 - 0 - 2 - 0 - 1
|   |   |   |   |
0 - 0 - 0 - 0 - 0
|   |   |   |   |
0 - 0 - 1 - 0 - 0
The point (1,2) is an ideal empty land to build a house, as the total travel distance of 3+3+1=7 is minimal. So return 7.
*/
package graph_dfs_bfs.Topic_DistanceMap_MultipleSourcesToTargets;

import java.util.LinkedList;
import java.util.Queue;

/**
    BFS from all buildings: O(k * m * n)


    The difference between SumOfDistancesInTree vs. ShortestDistanceFromAllBuildings:
        对于某个点P，其两边的点相互访问：
            - SumOfDistancesInTree: 必定经过点P (tree,无环!!..)
            - ShortestDistanceFromAllBuildings: 可不经过点P，路径任意...(graph...)

 */
public class ShortestDistanceFromAllBuildings {

    public int shortestDistance(int[][] grid) {
        int m=grid.length, n=grid[0].length;
        int[][] distance = new int[m][n];
        for(int i=0;i<m;i++) {
            for(int j=0;j<n;j++) {
                distance[i][j]=grid[i][j]==0?0:Integer.MAX_VALUE;
            }
        }

        int visitedFlag=-1;
        for(int i=0;i<m;i++) {
            for(int j=0;j<n;j++) {
                if(grid[i][j]==1) {
                    bfs(grid, i, j, distance, visitedFlag);
                    visitedFlag--;
                }
            }
        }

        int ans=Integer.MAX_VALUE;
        for(int i=0;i<m;i++) {
            for(int j=0;j<n;j++) {
                if (distance[i][j]!=-1) ans = Math.min(ans, distance[i][j]); // 注意跳过坑位...
            }
        }
        return ans;
    }
    final int[][] directions = new int[][]{{-1,0},{1,0},{0,-1},{0,1}};
    void bfs(int[][] grid, int i, int j, int[][] distance, int visitedFlag) {
        LinkedList<int[]> que = new LinkedList<>();
        que.add(new int[]{i,j});
        int level=-1;
        while(!que.isEmpty()){
            int levelSize = que.size();
            ++level;
            for(int k=0;k<levelSize;k++) {
                int[] cur  = que.remove();
                int x=cur[0],y=cur[1];
                if(grid[x][y]!=1 && distance[x][y]!=-1) {
                    distance[x][y]+=level;
                }
                for(int[] dir: directions) {
                    int nx=x+dir[0], ny=y+dir[1];
                    if(nx>=0&&nx<grid.length&&ny>=0&&ny<grid[0].length
                        &&grid[nx][ny]!=1 && grid[nx][ny]!=2 && grid[nx][ny]!=visitedFlag) {
                        que.add(new int[]{nx,ny});
                        grid[nx][ny]=visitedFlag;
                    }
                }
            }
        }

        // 注意可能有不可达所有building的坑位，要标出来。。。
        for (int k = 0; k < grid.length; k++) {
            for (int l = 0; l < grid[0].length; l++) {
                if (grid[k][l]!=1 && grid[k][l]!=2 && grid[k][l]!=visitedFlag) {
                    distance[k][l]=-1;
                }
            }
        }
    }

    public static void main(String[] args) {
        int[][] grid= new int[][]{
                {0,1,2,0,1},
                {1,0,0,0,0},
                {0,0,1,0,0},
//                {1,1,1,1,1,0},
//                {0,0,0,0,0,1},
//                {0,1,1,0,0,1},
//                {1,0,0,1,0,1},
//                {1,0,1,0,0,1},
//                {1,0,0,0,0,1},
//                {0,1,1,1,1,0},
        };
        System.out.println(new ShortestDistanceFromAllBuildings().shortestDistance(grid));

    }
}

