/*
A group of two or more people wants to meet and minimize the total travel distance.
You are given a 2D grid of values 0 or 1, where each 1 marks the home of someone in the group.
The distance is calculated using Manhattan Distance, where distance(p1, p2) = |p2.x - p1.x| + |p2.y - p1.y|.

Example
Given three people living at (0,0), (0,4), and (2,2):

1 - 0 - 0 - 0 - 1
|   |   |   |   |
0 - 0 - 0 - 0 - 0
|   |   |   |   |
0 - 0 - 1 - 0 - 0
The point (0,2) is an ideal meeting point, as the total travel distance of 2 + 2 + 2 = 6 is minimal. So return 6.
*/

package graph_dfs_bfs.Topic_DistanceMap_MultipleSourcesToTargets;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *  idea: 二维的等于一维的相加, 一维的最小点必在median点(用反证法可以证明).
 */
public class BestMeetingPoint {
    public int minTotalDistance(int[][] grid) {
        int m=grid.length, n=grid[0].length;
        List<Integer> xs = new ArrayList<>();
        List<Integer> ys = new ArrayList<>();
        for (int i = 0; i <m ; i++) {
            for(int j=0; j<n; j++) {
                if(grid[i][j]==1) {
                    xs.add(i);
                    ys.add(j);
                }
            }
        }
        Collections.sort(xs);
        Collections.sort(ys);

        int ans = 0;
        int l=0,r=xs.size()-1;
        while(l<=r) {
            ans+=xs.get(r)-xs.get(l);
            ans+=ys.get(r)-ys.get(l);
            l++;r--;
        }
        return ans;
    }

    public static void main(String[] args) {
        int[][] grid = new int[][]{
                {1,0,0,0,1},
                {0,0,0,0,0},
                {0,0,1,0,0},
        };
        System.out.println(new BestMeetingPoint().minTotalDistance(grid));
    }
}


