多起点路径问题：

case1: 如果是需要各个起点的累加路径和最小, 求target点：
        用 distance map
        如： ShortestDistanceFromAllBuildings， BestMeetingPoint


cass2: 如果是需要各个起点的最短路径最小，并已知多个target点的位置, 求最小路径长度：(see graph_dfs_bfs.Topic_SourceToTargetProblems.Case_ShortestPathWithUniformEdge_BFS.Case_MultipleSourcesToTargets)
        用 bfs simultaneously
        如： WallsAndGates， MinimumCostToConnectSticks， AsFarFromLandAsPossible