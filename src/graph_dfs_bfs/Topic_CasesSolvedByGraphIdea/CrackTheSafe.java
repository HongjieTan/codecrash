package graph_dfs_bfs.Topic_CasesSolvedByGraphIdea;

import java.util.HashSet;
import java.util.Set;


/*
    very good problem!!!!

    - 理想情况下所有可能的组合都有另一个组合跟它有n-1位重合，这样最终路径肯定最短！
    - 正确性证明用欧拉环（或hamilton路径），一定存在这样的路径！！

    1. convert to 'find a hamilton path'
        - dfs+backtrack:  backtrack来尝试路径, return earlier improve some...but still O((k^n)!) in worse case...

    2. convert to 'find a euler path'
        - dfs+postorder: use post order to avoid get stuck in the graph prematurely!  O(n*k^n)  cool!!!!!
        - why post order work?  just a trick !


    leetcode hint:
        We can think of this problem as the problem of finding an Euler path (a path visiting every edge exactly once)
        on the following graph: there are $$k^{n-1}$$ nodes with each node having $$k$$ edges.
        It turns out this graph always has an Eulerian circuit (path starting where it ends.)
        We should visit each node in "post-order" so as to not get stuck in the graph prematurely.


    https://leetcode.com/problems/cracking-the-safe/discuss/110264/Easy-DFS

    Proof:
        The solution is very nice... and the greedy approach can be demonstrated as valid. I will try to explain the demonstration...
        We can create a graph.. each vertex is any possible strings contains n - 1 digits.. and each vertex will have k outgoing edges... each points to anther vertex...that vertex can be obtain by appending i to the end of the original vertex... then take the last n - 1 digits...
        for example for case of n = 4, k = 6... "000" is one of the vertices, it will have 6 outgoing edges, pointing to "000", "001", "002", "003", "004", "005" respectively...
        And the original problem of finding shorted string equals to finding an Eulerian cycle of that graph.. and since all the vertices in the directed graph have equal in degree and out degree (both are k)... there definitely exists such Eulerian cycle.
        Proof completed...


 */
public class CrackTheSafe {

    // dfs+postorder...
    public String crackSafe_dfs_post_x2(int n, int k) {
        StringBuilder sb = new StringBuilder();
        StringBuilder start = new StringBuilder();
        for(int i=0;i<n-1;i++) start.append("0");
        Set<String> visited = new HashSet<>();
        dfs_post(sb, start.toString(), k, visited);
        return start.append(sb.reverse()).toString();
    }

    void dfs_post(StringBuilder sb, String node, int k, Set<String>  visited){
        for(int i=0;i<k;i++) {
            if(!visited.contains(node+i)) { // node+i 唯一确定一个有向边， 边不能重复访问...
                visited.add(node+i);
                dfs_post(sb, (node+i).substring(1), k, visited);
                sb.append(i);
            }
        }
    }

    // dfs+backtrack...
    public String crackSafe_dfs_backtrack_x2(int n, int k) {
        StringBuilder sb = new StringBuilder();
        int total = (int)Math.pow(k,n);
        for(int i=0;i<n;i++) sb.append("0");
        Set<String> visited = new HashSet<>();
        visited.add(sb.toString());
        dfs(sb, total, n, k, visited);
        return sb.toString();
    }

    boolean dfs(StringBuilder sb, int total, int n, int k, Set<String>  visited){
        if(visited.size()==total) return true;
        for(int i=0;i<k;i++) {
            if(!visited.contains(sb.substring(sb.length()-n+1)+i)) {
                sb.append(i);
                String nb = sb.substring(sb.length()-n);
                visited.add(nb);
                if (dfs(sb, total, n, k, visited)) return true;
                sb.deleteCharAt(sb.length()-1);
                visited.remove(nb);
            }
        }
        return false;
    }

    // find a hamilton path!!
    public String crackSafe(int n, int k) {
        StringBuffer ans = new StringBuffer();
        for (int i = 0; i < n; i++) {
            ans.append('0');
        }
        Set<String> visited = new HashSet<>();
        int total = (int) Math.pow(k,n);
        visited.add(ans.toString());
        dfs(ans, ans.toString(), visited, k, total);
        return ans.toString();
    }

    private boolean dfs(StringBuffer ans, String node, Set<String> visited, int k, int total) {
        if (visited.size() == total) return true;
        for (char c = '0'; c < k+'0'; c++) {
            String nb = node.substring(1) + c;
            if (!visited.contains(nb)) {
                visited.add(nb);
                ans.append(c);
                if (dfs(ans, nb, visited, k, total)) return true; // return earlier
                visited.remove(nb);  // if not hamilton path, backtrack to try another path!!!
                ans.deleteCharAt(ans.length()-1);
            }
        }
        return false;
    }


    // find euler path !  (n-1)^k nodes, each node has k edges
    public String crackSafe_v2(int n, int k) {
        StringBuffer ans = new StringBuffer();
        StringBuffer start = new StringBuffer();
        for(int i=0; i<n-1; i++) start.append('0');
        Set<String> visited = new HashSet<>();
        dfs(ans, start.toString(), visited, k);
        return ans.append(start).reverse().toString();
    }

    private void dfs(StringBuffer ans, String node, Set<String> visited, int k) {
        for (char c = '0'; c < '0'+ k; c++) {
            if(!visited.contains(node+c)) {
                visited.add(node+c);
                dfs(ans, (node+c).substring(1), visited, k);
                ans.append(c); // notice: use post order!!  why?
            }
        }
    }

    public static void main(String[] args) {
        System.out.println(new CrackTheSafe().crackSafe(3,2));
    }
}
