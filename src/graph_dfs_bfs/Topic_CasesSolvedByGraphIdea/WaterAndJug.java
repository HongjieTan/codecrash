package graph_dfs_bfs.Topic_CasesSolvedByGraphIdea;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;


/*
    - BFS/DFS: 实际上算是TopDown DP

    - bottom up dp 可以解么？later...

 */
public class WaterAndJug {

    // BFS solution...(bfs is suitable for interview...the math solution is hard to come up with...)
    public boolean canMeasureWater(int x, int y, int z) {
        if(x+y<z) return false;
        Set<String> visited = new HashSet<>();
        LinkedList<int[]> que = new LinkedList<>();
        que.add(new int[]{0,0});
        while(!que.isEmpty()) {
            int[] cur = que.poll();
            int a=cur[0], b=cur[1], na,nb;
            visited.add(a+"#"+b);

            if(a+b==z) return true;

            na = x; nb=b;
            if(!visited.contains(na+"#"+nb)) que.add(new int[]{na,nb});
            na = a; nb=y;
            if(!visited.contains(na+"#"+nb)) que.add(new int[]{na,nb});

            na = 0; nb=b;
            if(!visited.contains(na+"#"+nb)) que.add(new int[]{na,nb});
            na = a; nb=0;
            if(!visited.contains(na+"#"+nb)) que.add(new int[]{na,nb});

            na = Math.min(a+b, x); nb=a+b-na;
            if(!visited.contains(na+"#"+nb)) que.add(new int[]{na,nb});
            nb=Math.min(a+b, y); na=a+b-nb;
            if(!visited.contains(na+"#"+nb)) que.add(new int[]{na,nb});
        }
        return false;
    }


//    // Math: according to Bézout's identity
//    public boolean canMeasureWater(int x, int y, int z) {
//        //limit brought by the statement that water is finallly in one or both buckets
//        if(x + y < z) return false;
//        //case x or y is zero
//        if( x == z || y == z || x + y == z ) return true;
//        //get GCD, then we can use the property of Bézout's identity
//        return z%GCD(x, y) == 0;
//    }
//
//    public int GCD(int a, int b){
//        while(b != 0 ){
//            int temp = b;
//            b = a%b;
//            a = temp;
//        }
//        return a;
//    }

    public static void main(String[] args) {
        System.out.println(new WaterAndJug().canMeasureWater(3,5,4));
//        System.out.println(new graph_dfs_bfs.Topic_CasesSolvedByGraphIdea.WaterAndJug().canMeasureWater(2,6,5));
    }
}


/*
You are given two jugs with capacities x and y litres. There is an infinite amount of water supply available.
You need to determine whether it is possible to measure exactly z litres using these two jugs.

If z liters of water is measurable, you must have z liters of water contained within one or both buckets by the end.

Operations allowed:

Fill any of the jugs completely with water.
Empty any of the jugs.
Pour water from one jug into another till the other jug is completely full or the first jug itself is empty.
Example 1: (From the famous "Die Hard" example)

Input: x = 3, y = 5, z = 4
Output: True
Example 2:

Input: x = 2, y = 6, z = 5
Output: False

*/