package graph_dfs_bfs;

import java.util.*;

/**
 * Created by thj on 2018/8/16.
 *
 *
 *  time:  O(m^2 * n^2)
 *
 */
public class CutOffTrees {

    public int cutOffTree(List<List<Integer>> forest) {
        List<int[]> trees = new ArrayList<>();

        for (int row = 0; row < forest.size(); row++) {
            for (int col = 0; col < forest.get(0).size(); col++) {
                int height = forest.get(row).get(col);
                if (height >1) {
                    trees.add(new int[]{row, col, height});
                }
            }
        }
        Collections.sort(trees, (a, b)-> (a[2]-b[2]));


        int sourceRow = 0, sourceCol = 0;
        int sum = 0;
        for (int[] tree: trees) {
            int targetRow = tree[0], targetCol = tree[1];
            int steps = minDist(forest, sourceRow, sourceCol, targetRow, targetCol);

            if (steps == -1) return -1;
            sum+=steps;

            sourceRow = targetRow;
            sourceCol = targetCol;
        }


        return sum;
    }


//    private final static int[][] directions = {{0,1}, {0, -1}, {1, 0}, {-1, 0}};

    // min dist from (sr, sc) to (tr, tc) -> bfs(level order)
    private int minDist(List<List<Integer>> forest, int sourceRow, int sourceCol, int targetRow, int targetCol) {
        int depth = 0;
        LinkedList<int[]> que = new LinkedList<>();

        boolean[][] visited = new boolean[forest.size()][forest.get(0).size()]; // 不走回头路!!!
        que.add(new int[]{sourceRow, sourceCol});
        visited[sourceRow][sourceCol] = true;

        while (!que.isEmpty()) {

            int levelSize = que.size();
            for (int i = 0; i < levelSize; i++) {
                int[] node = que.pop();
                int row = node[0], col = node[1];

                if (row == targetRow && col == targetCol) {
                    return depth;
                }

                // move step at 4 directions
//                for (int[] direction: directions) {
//                    int nextRow = row + direction[0];
//                    int nextCol = col + direction[1];
//
//                    if (isValid(forest, nextRow, nextCol) && forest.get(nextRow).get(nextCol)!=0 && !visited[nextRow][nextCol]) {
//                        que.add(new int[]{nextRow, nextCol});
//                        visited[nextRow][nextCol] = true;
//                    }
//                }

                if (isValid(forest, row-1, col) && forest.get(row-1).get(col)!=0 && !visited[row-1][col]) {que.add(new int[]{row-1, col}); visited[row-1][col] = true;}
                if (isValid(forest, row+1, col) && forest.get(row+1).get(col)!=0 && !visited[row+1][col]) {que.add(new int[]{row+1, col}); visited[row+1][col] = true;}
                if (isValid(forest, row, col-1) && forest.get(row).get(col-1)!=0 && !visited[row][col-1]) {que.add(new int[]{row, col-1}); visited[row][col-1] = true;}
                if (isValid(forest, row, col+1) && forest.get(row).get(col+1)!=0 && !visited[row][col+1]) {que.add(new int[]{row, col+1}); visited[row][col+1] = true;}
            }

            depth++;
        }

        return -1;
    }

    private boolean isValid(List<List<Integer>> forest, int row, int col) {
        return row>=0 && row<forest.size()
                && col>=0 && col<forest.get(0).size();
    }

    public static void main(String[] args) {
        List<List<Integer>> forest = new ArrayList<>();
        forest.add(Arrays.asList(1,2,3));
        forest.add(Arrays.asList(0,0,0));
        forest.add(Arrays.asList(7,6,5));

        System.out.println(new CutOffTrees().cutOffTree(forest));
    }
}
