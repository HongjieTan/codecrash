//        You have N gardens, labelled 1 to N.  In each garden, you want to plant one of 4 types of flowers.
//
//        paths[i] = [x, y] describes the existence of a bidirectional path from garden x to garden y.
//
//        Also, there is no garden that has more than 3 paths coming into or leaving it.
//
//        Your task is to choose a flower type for each garden such that, for any two gardens connected by a path, they have different types of flowers.
//
//        Return any such a choice as an array answer, where answer[i] is the type of flower planted in the (i+1)-th garden.  The flower types are denoted 1, 2, 3, or 4.  It is guaranteed an answer exists.
//
//
//        Example 1:
//
//        Input: N = 3, paths = [[1,2],[2,3],[3,1]]
//        Output: [1,2,3]
//        Example 2:
//
//        Input: N = 4, paths = [[1,2],[3,4]]
//        Output: [1,2,1,2]
//        Example 3:
//
//        Input: N = 4, paths = [[1,2],[2,3],[3,4],[4,1],[1,3],[2,4]]
//        Output: [1,2,3,4]
package graph_dfs_bfs;

import java.util.*;

// build graph + greedy...
public class FlowerPlantingWithNoAdjacent {
    public int[] gardenNoAdj(int N, int[][] paths) {
        Map<Integer, List<Integer>> g = new HashMap<>();
        for(int[] path: paths) {
            if(!g.containsKey(path[0])) g.put(path[0], new ArrayList<>());
            if(!g.containsKey(path[1])) g.put(path[1], new ArrayList<>());
            g.get(path[0]).add(path[1]);
            g.get(path[1]).add(path[0]);
        }

        int[] ans = new int[N];
        for (int i=1; i<=N; i++) {
            Set<Integer> used = new HashSet<>();
            for(int nb: g.getOrDefault(i, new ArrayList<>())) used.add(ans[nb-1]);
            for(int type=1; type<=4; type++)
                if (!used.contains(type))
                    ans[i-1] = type;
        }
        return ans;
    }

    /*
    def gardenNoAdj(self, N, paths):
        g = collections.defaultdict(list)
        for x, y in paths:
            g[x].append(y)
            g[y].append(x)
        ans = [0]*N
        for i in range(N):
            ans[i] = ({1,2,3,4}-{ans[nb-1] for nb in g[i+1]}).pop()
        return ans
     */


    public static void main(String[] as) {
//        int N = 6, paths[][] = {{1,2},{1,4},{2,4},{3,4},{2,6},{6,1}};
//        int[] ans = new FlowerPlantingWithNoAdjacent().gardenNoAdj(N, paths);
//        for(int x: ans) System.out.println(x);
    }
}
