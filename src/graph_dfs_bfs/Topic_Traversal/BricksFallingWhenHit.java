package graph_dfs_bfs.Topic_Traversal;

public class BricksFallingWhenHit {
    // reverse way...O（hits+bricks）
    int count=0;
    int[][] directions = new int[][]{ {-1,0},{1,0},{0,-1}, {0,1}};
    public int[] hitBricks(int[][] grid, int[][] hits) {
        for(int[] hit:hits) {
            int x=hit[0], y=hit[1];
            if (grid[x][y]==1)
                grid[x][y]=-1;
        }

        for(int j=0;j<grid[0].length;j++) dfs(grid, 0, j);
        int[] ans = new int[hits.length];
        for(int i=hits.length-1;i>=0;i--) {
            int x=hits[i][0], y=hits[i][1];
            if(grid[x][y]==0) continue;
            grid[x][y]=1;

            boolean connected = false;
            if(x==0) {
                connected = true;
            } else {
                for(int[] dir:directions){
                    int nx=x+dir[0], ny=y+dir[1];
                    if(nx>=0&&nx<grid.length&&ny>=0&&ny<grid[0].length&&grid[nx][ny]==2) {
                        connected = true; break;
                    }
                }
            }
            if(connected) {
                count=0;
                dfs(grid, x, y);
                ans[i]=count-1;
            }
        }
        return ans;
    }

    void dfs(int[][] grid, int i, int j) {
        if (grid[i][j]!=1) return;
        grid[i][j]=2;
        count++;

        for(int[] dir: directions) {
            int ni=i+dir[0], nj=j+dir[1];
            if(ni>=0&&ni<grid.length&&nj>=0&&nj<grid[0].length) dfs(grid, ni, nj);
        }
    }


    // brute force: TLE... O（hits * bricks）
//    public int[] hitBricks(int[][] grid, int[][] hits) {
//        int[] ans = new int[hits.length];
//        int bricks = countBricks(grid);
//        for(int i=0; i<hits.length; i++) {
//            int[] hit = hits[i];
//            grid[hit[0]][hit[1]]=0;
//            int curBricks = countBricks(grid);
//            ans[i]=curBricks==bricks?0:bricks-curBricks-1;
//            bricks = curBricks;
//        }
//        return ans;
//    }
//    int count;
//    int countBricks(int[][] grid) {
//        boolean[][] visited = new boolean[grid.length][grid[0].length];
//        count=0;
//        for(int j=0;j<grid[0].length;j++) {
//            if(!visited[0][j] && grid[0][j]==1) {
//                dfs(grid, 0, j, visited);
//            }
//        }
//        return count;
//    }
//    void dfs(int[][] grid, int i, int j, boolean[][] visited) {
//        visited[i][j]=true;
//        count++;
//        int[][] directions = new int[][]{ {-1,0},{1,0},{0,-1}, {0,1}};
//        for(int[] dir: directions) {
//            int ni=i+dir[0], nj=j+dir[1];
//            if(ni>=0&&ni<grid.length&&nj>=0&&nj<grid[0].length
//                    && !visited[ni][nj] && grid[ni][nj]!=0) {
//                dfs(grid, ni, nj, visited);
//            }
//        }
//    }

    public static void main(String[] args) {

//[[1,0,0,0],[1,1,1,0]]
//[[1,0]]
        int[][] grid = new int[][] {
                {1,0,0,0},
                {1,1,1,0},
        };
        int[][] hits = new int[][]{
                {1,0},
        };


        int[] ans = new BricksFallingWhenHit().hitBricks(grid, hits);
        for (int x:ans) System.out.println(x);
    }
}

/*
We have a grid of 1s and 0s; the 1s in a cell represent bricks.  A brick will not drop if and only if it is
directly connected to the top of the grid, or at least one of its (4-way) adjacent bricks will not drop.

We will do some erasures sequentially. Each time we want to do the erasure at the location (i, j),
the brick (if it exists) on that location will disappear, and then some other bricks may drop because of that erasure.

Return an array representing the number of bricks that will drop after each erasure in sequence.

Example 1:
Input:
grid = [[1,0,0,0],[1,1,1,0]]
hits = [[1,0]]
Output: [2]
Explanation:
If we erase the brick at (1, 0), the brick at (1, 1) and (1, 2) will drop. So we should return 2.
Example 2:
Input:
grid = [[1,0,0,0],[1,1,0,0]]
hits = [[1,1],[1,0]]
Output: [0,0]
Explanation:
When we erase the brick at (1, 0), the brick at (1, 1) has already disappeared due to the last move.
So each erasure will cause no bricks dropping.  Note that the erased brick (1, 0) will not be counted as a dropped brick.


Note:

The number of rows and columns in the grid will be in the range [1, 200].
The number of erasures will not exceed the area of the grid.
It is guaranteed that each erasure will be different from any other erasure, and located inside the grid.
An erasure may refer to a location with no brick - if it does, no bricks drop.
*/

// 思路：最好方法从后往前补。先把砖块全都打掉，然后用贴天花板的砖块dfs+mark，
// 然后从后往前一个一个往上加，加同时若碰上周围有mark的砖块就主动dfs，dfs出来的就是这次打掉的砖块