package graph_dfs_bfs.Topic_Traversal;

import java.util.HashSet;
import java.util.Set;

// This is the robot's control interface.
// You should not implement it, or speculate about its implementation
interface Robot {
    // Returns true if the cell in front is open and robot moves into the cell.
    // Returns false if the cell in front is blocked and robot stays in the current cell.
    public boolean move();

    // graph_dfs_bfs.Topic_Traversal.Robot will stay in the same cell after calling turnLeft/turnRight.
    // Each turn will be 90 degrees.
    public void turnLeft();
    public void turnRight();

    // Clean the current cell.
    public void clean();
}

public class RobotRoomCleaner {
    public void cleanRoom(Robot robot) {
        Set<String> visited = new HashSet<>();
        dfs(robot, 0, 0, 0, visited);
    }

    int[][] directions = new int[][]{{-1,0}, {0,1}, {1,0}, {0,-1}}; // 顺时针顺序...
    private void dfs(Robot robot, int x, int y, int curDir, Set<String> visited) { // 注意要带上curDir...

        if(visited.contains(x+"#"+y)) return;
        visited.add(x+"#"+y);
        robot.clean();

        for(int i=0;i<4;i++) {
            if(robot.move()) {
                int nx=x+directions[curDir][0], ny=y+directions[curDir][1];
                dfs(robot, nx, ny, curDir, visited);

                // backtrack !
                robot.turnRight();
                robot.turnRight();
                robot.move();
                robot.turnLeft();
                robot.turnLeft();
            }

            robot.turnRight();
            curDir=(curDir+1)%4;
        }

    }
}

/*
https://github.com/awangdev/LintCode/blob/master/Java/Robot%20Room%20Cleaner.java

Given a robot cleaner in a room modeled as a grid.

Each cell in the grid can be empty or blocked.

The robot cleaner with 4 given APIs can move forward, turn left or turn right. Each turn it made is 90 degrees.

When it tries to move into a blocked cell, its bumper sensor detects the obstacle and it stays on the current cell.

Design an algorithm to clean the entire room using only the 4 given APIs shown below.

interface graph_dfs_bfs.Topic_Traversal.Robot {
  // returns true if next cell is open and robot moves into the cell.
  // returns false if next cell is obstacle and robot stays on the current cell.
  boolean move();

  // graph_dfs_bfs.Topic_Traversal.Robot will stay on the same cell after calling turnLeft/turnRight.
  // Each turn will be 90 degrees.
  void turnLeft();
  void turnRight();

  // Clean the current cell.
  void clean();
}
Example:

Input:
room = [
  [1,1,1,1,1,0,1,1],
  [1,1,1,1,1,0,1,1],
  [1,0,1,1,1,1,1,1],
  [0,0,0,1,0,0,0,0],
  [1,1,1,1,1,1,1,1]
],
row = 1,
col = 3

Explanation:
All grids in the room are marked by either 0 or 1.
0 means the cell is blocked, while 1 means the cell is accessible.
The robot initially starts at the position of row=1, col=3.
From the top left corner, its position is one row below and three columns right.
Notes:

The input is only given to initialize the room and the robot's position internally. You must solve this problem "blindfolded".
In other words, you must control the robot using only the mentioned 4 APIs, without knowing the room layout and the initial robot's position.
The robot's initial position will always be in an accessible cell.
The initial direction of the robot will be facing up.
All accessible cells are connected, which means the all cells marked as 1 will be accessible by the robot.
Assume all four edges of the grid are all surrounded by wall.
*/

/*
思路：常规DFS
需要用参数追踪当前机器人朝向
每次backtracking时候别忘了掉头回正
用string记录visit过的位置
参考代码：（disscussion 里面看到的代码量少，比较好写的代码）
class Solution {
    // 0: up, 1: right, 2: down , 3: left
    public void cleanRoom(Robot robot) {
        dfs(new HashSet<>(), 0, 0, 0, robot);
    }
    private void dfs(Set<String> visited, int i, int j, int currDir, Robot r) {
        if(visited.contains(i + "," + j)) return;
        visited.add(i + "," + j);
        r.clean();
        for(int k = 0 ; k < 4; k++) {
            if(r.move()) {
                int x = i, y = j;
                switch(currDir) {
                    case 0: x -= 1; // up
                        break;
                    case 1: y += 1; // right
                        break;
                    case 2: x += 1; // down
                        break;
                    case 3: y -= 1; // left
                        break;
                }
                dfs(visited, x, y, currDir, r);
                r.turnRight();
                r.turnRight();
                r.move();
                r.turnRight();
                r.turnRight();
            }
            r.turnRight();
            currDir += 1;
            currDir %= 4;
        }
    }
}


*/