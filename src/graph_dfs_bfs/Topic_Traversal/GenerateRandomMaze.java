package graph_dfs_bfs.Topic_Traversal;

import java.util.Arrays;
import java.util.Random;

/**
 * https://app.laicode.io/app/problem/218
 */
public class GenerateRandomMaze {
    public int[][] maze(int n) {
        int[][] maze = new int[n][n];
        for (int i = 0; i <n ; i++) Arrays.fill(maze[i],1);
        maze[0][0]=0;
        dfs_v2(maze, 0, 0);
        // dfs_v1(maze, 0, 0);
        return maze;
    }
    void dfs_v2(int[][] maze, int i, int j) {
        maze[i][j]=0;
        int[][] directions = new int[][]{{1,0},{-1,0},{0,1},{0,-1}};
        shuffle(directions);
        for(int[] dir: directions) {
            int ni=i+dir[0], nj=j+dir[1];
            if (isValid(maze,ni,nj,i,j))
                dfs_v2(maze,ni,nj);
        }
    }
    boolean isValid(int[][] maze, int i, int j, int srci, int srcj) {
        if (!(i>=0&&i<maze.length&&j>=0&&j<maze[0].length&&maze[i][j]==1))
            return false;

        int[][] directions = new int[][]{{1,0},{-1,0},{0,1},{0,-1}};
        for(int[] dir: directions) {
            int ni=i+dir[0];
            int nj=j+dir[1];
            if (ni==srci && nj==srcj) continue; // skip src
            if (ni>=0&&ni<maze.length&&nj>=0&&nj<maze[0].length&&maze[ni][nj]==0) {
                return false;
            }
        }
        return true;
    }

    // 每次走两步，避免生成没有墙的空地
//    void dfs_v1(int[][] maze, int i, int j){
//        int[][] directions = new int[][]{{1,0},{-1,0},{0,1},{0,-1}};
//        shuffle(directions);
//        for(int[] dir: directions) {
//            int ni=i+dir[0], nj=j+dir[1];
//            int nni=i+2*dir[0], nnj=j+2*dir[1];
//            if (nni>=0 && nni<maze.length && nnj>=0 && nnj<maze[0].length && maze[nni][nnj]==1) {
//                maze[ni][nj]=0;
//                maze[nni][nnj]=0;
//                dfs_v1(maze, nni, nnj);
//            }
//        }
//    }
    void shuffle(int[][] directions) {
        Random rand = new Random();
        for (int i = 0; i < directions.length; i++) {
            int randIdx = rand.nextInt(directions.length);
            int[] temp = directions[i];
            directions[i] = directions[randIdx];
            directions[randIdx] = temp;
        }
    }

    public static void main(String[] args) {
        int[][] maze = new GenerateRandomMaze().maze(5);

        for (int i = 0; i < maze.length; i++) {
            for (int j = 0; j < maze[0].length; j++) {
                System.out.printf(maze[i][j]+" ");
            }
            System.out.println();
        }
    }
}
/*
Randomly generate a maze of size N * N (where N = 2K + 1) whose corridor and wall’s width are both 1 cell.
For each pair of cells on the corridor, there must exist one and only one path between them.
(Randomly means that the solution is generated randomly, and whenever the program is executed, the solution can be different.).
The wall is denoted by 1 in the matrix and corridor is denoted by 0.

Assumptions

N = 2K + 1 and K >= 0
the top left corner must be corridor
there should be as many corridor cells as possible
for each pair of cells on the corridor, there must exist one and only one path between them
Examples

N = 5, one possible maze generated is

        0  0  0  1  0

        1  1  0  1  0

        0  1  0  0  0

        0  1  1  1  0

        0  0  0  0  0
 */

/*
左上到右下，怎么设计可玩性
maze generation. 输入是int[][] board, int[] start, int[] dest,返回一个int[][] maze. 这题题意比较复杂。简单来说就是让你随机生成一个迷宫，
条件是：
（1）你肯定要生成一些墙，这些墙宽度为1，意思就是board[0][0] - board[0][3]可以是墙，s宽度为1， 长度为4。 但是不能生成board[0][0] - board[1][3]这样的厚墙（2*4)
 (2)  要求这个迷宫有且仅有一条路可以从start到达destination， 另外对于那些不是墙的blank cell，也要有可以从start到达它的路径。 也就是说不能有一些孤岛是不能到达的
 (3)  后来大哥给我简化了一点，如果输入board里面已经有一些墙， 用1表示，但是这个迷宫并不是具有通路的，然后让你根据以上条件，生成迷宫。

https://app.laicode.io/app/problem/218
思路：直接暴力小人DFS瞎走，每次走两步，避免生成没有墙的空地。
https://en.wikipedia.org/wiki/Maze_generation_algorithm
(Provider: Sean)
public class graph_dfs_bfs.Topic_Traversal.GenerateRandomMaze {
    public int[][] maze(int n) {
        // Assumptions: n = 2 * k + 1, where k > = 0.
        int[][] maze = new int[n][n];
        // initialize the matrix as only (0,0) is corridor,
        // other cells are all walls at the beginning.
        // later we are trying to break the walls to form corridors.
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (i == 0 && j == 0) {
                    maze[i][j] = 0;
                } else {
                    maze[i][j] = 1;
                }
            }i
        }
        generate(maze, 0, 0);
        return maze;
    }
    private void generate(int[][] maze, int x, int y) {
        // get a random shuffle of all the possible directions,
        // and follow the shuffled order to do DFS & backtrack.
        Dir[] dirs = Dir.values();
        shuffle(dirs);
        for (Dir dir: dirs) {
            // advance by two steps.
            int nextX = dir.moveX(x, 2);
            int nextY = dir.moveY(y, 2);
            if (isValidWall(maze, nextX, nextY)) {
                // only if the cell is a wall(meaning we have not visited before),
                // we break the walls through to make it corridors.
                maze[dir.moveX(x, 1)][dir.moveY(y, 1)] = 0;
                maze[nextX][nextY] = 0;
                generate(maze, nextX, nextY);
            }
        }
    }
    // Get a random order of the directions.
    private void shuffle(Dir[] dirs) {
        for (int i = 0; i < dirs.length; i++) {
            int index = (int)(Math.random() * (dirs.length - i));
            Dir tmp = dirs[i];
            dirs[i] = dirs[i + index];
            dirs[i + index] = tmp;
        }
    }
    // check if the position (x,y) is within the maze and it is a wall.
    private boolean isValidWall(int[][] maze, int x, int y) {
        return x >= 0 && x < maze.length && y >= 0 && y < maze[0].length && maze[x][y] ==
            1;
    }
    enum Dir {
        NORTH(-1, 0), SOUTH(1, 0), EAST(0, -1), WEST(0, 1);
        int deltaX;
        int deltaY;
        Dir(int deltaX, int deltaY) {
            this.deltaX = deltaX;
            this.deltaY = deltaY;
        }
        // move certain times of deltax.
        public int moveX(int x, int times) {
            return x + times * deltaX;
        }
        // move certain times of deltay.
        public int moveY(int y, int times) {
            return y + times * deltaY;
        }
    }
}
*/