package graph_dfs_bfs.Topic_Traversal;

import java.util.Arrays;
import java.util.LinkedList;

public class TheMaze {

    public boolean hasPath(int[][] maze, int[] start, int[] destination) {
//        boolean[][] visited = new boolean[maze.length][maze[0].length];
//        return dfs(maze, start, destination, visited);
        return bfs(maze, start, destination);
    }

    private boolean bfs(int[][] maze, int[] start, int[] destination) {
        LinkedList<int[]> que = new LinkedList<>();
        boolean[][] visited = new boolean[maze.length][maze[0].length];
        int[][] DIRS = new int[][]{{0,1}, {0,-1}, {1,0}, {-1,0}};

        que.add(start);
        while (!que.isEmpty()) {
            int[] pos = que.poll();
            if (Arrays.equals(pos, destination)) return true;
            visited[pos[0]][pos[1]] = true;
            for (int[] dir: DIRS) {
                int nx = pos[0], ny = pos[1];
                // When the ball stops, it could choose the next direction or reach the destination!
                // So go along one direction!!!
                while (isValid(maze, nx+dir[0], ny+dir[1]) && maze[nx+dir[0]][ny+dir[1]] == 0) {
                    nx+=dir[0]; ny+=dir[1];
                }
                if (!visited[nx][ny]) que.add(new int[]{nx, ny});
            }
        }
        return false;
    }

    private boolean dfs(int[][] maze, int[] pos, int[] dst, boolean[][] visited) {
        if (Arrays.equals(pos, dst)) return true;

        visited[pos[0]][pos[1]] = true;
        int[][] dirs = new int[][]{{0,1}, {0,-1}, {1,0}, {-1,0}};
        for (int[] dir: dirs) {
            int nx = pos[0], ny = pos[1];

            // When the ball stops, it could choose the next direction or reach the destination!
            // So go along one direction!!!
            while (isValid(maze, nx+dir[0], ny+dir[1]) && maze[nx+dir[0]][ny+dir[1]] == 0) {
                nx += dir[0]; ny += dir[1];
            }
            if (!visited[nx][ny] && dfs(maze, new int[]{nx, ny}, dst, visited)) return true;
        }
        return false;
    }

    private boolean isValid(int[][] maze, int x, int y) {
        return x>=0 && x<maze.length && y>=0 && y<maze[0].length;
    }

    public static void main(String[] args) {
        int[][] maze = new int[][] {
                {0,0,1,0,0},
                {0,0,0,0,0},
                {0,0,0,1,0},
                {1,1,0,1,1},
                {0,0,0,0,0}
        };
        int[] start = new int[]{0,4};
        int[] dst = new int[]{4,4};
        System.out.println(new TheMaze().hasPath(maze, start, dst));
    }
}
