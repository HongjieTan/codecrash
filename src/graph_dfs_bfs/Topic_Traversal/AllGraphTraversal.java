package graph_dfs_bfs.Topic_Traversal;

/**
 * Created by thj on 2018/7/31.
 *
 *
 *  one sample see HasRouteBetweenNodes.java
 *
 *  visited is needed...
 *
 */
public class AllGraphTraversal {



    public void dfs_iterative() {
        // use a stack

    }

    public void dfs_recursion() {
        // ...
    }


    public void bfs() {
        // use a queue
    }




    public static void main(String[] args) {




    }
}
