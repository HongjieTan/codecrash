/*
In a 1 million by 1 million grid, the coordinates of each grid square are (x, y) with 0 <= x, y < 10^6.

We start at the source square and want to reach the target square.  Each move, we can walk to a 4-directionally adjacent square in the grid that isn't in the given list of blocked squares.

Return true if and only if it is possible to reach the target square through a sequence of moves.



Example 1:

Input: blocked = [[0,1],[1,0]], source = [0,0], target = [0,2]
Output: false
Explanation:
The target square is inaccessible starting from the source square, because we can't walk outside the grid.
Example 2:

Input: blocked = [], source = [0,0], target = [999999,999999]
Output: true
Explanation:
Because there are no blocked cells, it's possible to reach the target square.


Note:

0 <= blocked.length <= 200
blocked[i].length == 2
0 <= blocked[i][j] < 10^6
source.length == target.length == 2
0 <= source[i][j], target[i][j] < 10^6
source != target
*/
package graph_dfs_bfs.Topic_Traversal;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;


// bfs + early stop
public class EscapeLargeMaze {
    public boolean isEscapePossible(int[][] blocked, int[] source, int[] target) {
        Set<String> blockset = new HashSet<>();
        for(int[] b: blocked) blockset.add(b[0]+"#"+b[1]);
        return canEscape(blockset, source, target) && canEscape(blockset, target, source);
    }
    boolean canEscape(Set<String> blockset,int[] source, int[] target) {
        int MAX_AREA = 19900; // (1+199)*199/2
        Set<String> visited = new HashSet<>();
        int[][] dirs = {{-1,0},{1,0},{0,-1},{0,1}};
        LinkedList<int[]> q = new LinkedList<>();
        q.add(source);
        visited.add(source[0]+"#"+source[1]);
        while(!q.isEmpty()) {
            int[] cur = q.remove();
            for(int[] dir: dirs) {
                int nx=cur[0]+dir[0], ny=cur[1]+dir[1];
                if(nx>=0&&nx<1000000&&ny>=0&&ny<1000000 && !blockset.contains(nx+"#"+ny) && !visited.contains(nx+"#"+ny)) {
                    q.add(new int[]{nx,ny});
                    visited.add(nx+"#"+ny);
                    if(nx==target[0] && ny==target[1]) return true;
                    if(visited.size()>MAX_AREA) return true;
                }
            }
        }
        return false;
    }

    /*
    py:

    def isEscapePossible(self, blocked, source, target):
        if not blocked: return True
        blocked = set(tuple(p) for p in blocked)

        def check(source, target):
            q = [source]
            visited = {tuple(source)}
            while len(q) != 0:
                x, y = q.pop(0)
                for dir in [[0, 1], [0, -1], [1, 0], [-1, 0]]:
                    nx, ny = x + dir[0], y + dir[1]
                    if 0 <= nx < 10 ** 6 and 0 <= ny < 10 ** 6 and (nx, ny) not in visited and (nx, ny) not in blocked:
                        visited.add((nx, ny))
                        if nx == target[0] and ny == target[1]: return True
                        if len(visited) > 19900: return True
                        q.append((nx, ny))
            return False

        return check(source, target) and check(target, source)
     */

    public static void main(String[] a) {
        int blocked[][]  = {}, source[] = {0,0}, target[] = {999999,999999};
        System.out.println(new EscapeLargeMaze().isEscapePossible(blocked, source, target));
    }
}

/*
Complexity
Time complexity depends on the size of blocked
The maximum area blocked are B * (B - 1) / 2.
As a result, time and space complexity are both O(B^2)
In my solution I used a fixed upper bound 20000.


Python, DFS:
    def isEscapePossible(self, blocked, source, target):
        blocked = set(map(tuple, blocked))

        def dfs(x, y, target, seen):
            if not (0 <= x < 10**6 and 0 <= y < 10**6) or (x, y) in blocked or (x, y) in seen: return False
            seen.add((x, y))
            if len(seen) > 20000 or [x, y] == target: return True
            return dfs(x + 1, y, target, seen) or \
                dfs(x - 1, y, target, seen) or \
                dfs(x, y + 1, target, seen) or \
                dfs(x, y - 1, target, seen)
        return dfs(source[0], source[1], target, set()) and dfs(target[0], target[1], source, set())
Python, BFS:
    def isEscapePossible(self, blocked, source, target):
        blocked = {tuple(p) for p in blocked}

        def bfs(source, target):
            bfs, seen = [source], {tuple(source)}
            for x0, y0 in bfs:
                for i, j in [[0, 1], [1, 0], [-1, 0], [0, -1]]:
                    x, y = x0 + i, y0 + j
                    if 0 <= x < 10**6 and 0 <= y < 10**6 and (x, y) not in seen and (x, y) not in blocked:
                        if [x, y] == target: return True
                        bfs.append([x, y])
                        seen.add((x, y))
                if len(bfs) == 20000: return True
            return False
        return bfs(source, target) and bfs(target, source)
*/