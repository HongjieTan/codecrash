package graph_dfs_bfs.Topic_SourceToTargetProblems.Case_ShortestPathWithUniformEdge_BFS;

import java.util.*;

/**
 * Created by thj on 2018/8/5.
 */
public class WordLadder {

    // bfs + level order
    public int ladderLength(String beginWord, String endWord, List<String> wordList) {
        Set<String> dic = new HashSet<>(wordList);
        LinkedList<String> que = new LinkedList<>();
        que.add(beginWord);

        int depth = 0;
        while (!que.isEmpty()) {
            depth++;
            int levelCount = que.size();
            for (int i = 0; i < levelCount; i++) {
                String levelWord = que.poll();
                dic.remove(levelWord);
                if (levelWord.equals(endWord)) return depth;

                List<String> neighbors = getNeighbors_v2(levelWord, dic);
                que.addAll(neighbors);
            }
        }
        return 0;
    }


    // improved!!! the find is constant time complexity!!!!
    private List<String> getNeighbors_v2(String word, Set<String> dic) {
        List<String> res = new ArrayList<>();
        char[] wordChars = word.toCharArray();
        for (int i = 0; i < wordChars.length; i++) {
            char originChar = wordChars[i];
            for (int c='a'; c<='z'; c++) {
                if (c!=originChar) {
                    wordChars[i] = (char)c; // change
                    String newWord = String.valueOf(wordChars); //notice!! do not use wordChars.toString(), not work!!!!
                    if (dic.contains(newWord)) {
                        res.add(newWord);
                    }
                    wordChars[i] = originChar; // revert back
                }
            }
        }
        return res;
    }


    // notice! this is linear complexity! it will cause LTE!!!
//    private List<String> getNeighbors(String word, List<String> wordList, Set<String> visited) {
//        List<String> res = new ArrayList<>();
//        for (int i = 0; i < wordList.size(); i++) {
//            String candidate = wordList.get(i);
//            if (!visited.contains(candidate) && isNeighbor(word, candidate)) {
//                res.add(candidate);
//            }
//        }
//        return res;
//    }
//
//    private boolean isNeighbor(String word1, String word2) {
//        int diffCount=0;
//        for (int i = 0; i < word1.length(); i++) {
//            if (word1.charAt(i) != word2.charAt(i)) diffCount++;
//        }
//        return diffCount==1;
//    }

    public static void main(String[] args) {
        String beginWord = "hit";
        String endWord = "cog";
        List<String> wordList = Arrays.asList(new String[]{"hot","dot","dog","lot","log","cog"});
        System.out.println(new WordLadder().ladderLength(beginWord, endWord, wordList));
    }



}

/*
Given two words (beginWord and endWord), and a dictionary's word list, find the length of shortest transformation sequence from beginWord to endWord, such that:

Only one letter can be changed at a time.
Each transformed word must exist in the word list. Note that beginWord is not a transformed word.
Note:

Return 0 if there is no such transformation sequence.
All words have the same length.
All words contain only lowercase alphabetic characters.
You may assume no duplicates in the word list.
You may assume beginWord and endWord are non-empty and are not the same.
Example 1:

Input:
beginWord = "hit",
endWord = "cog",
wordList = ["hot","dot","dog","lot","log","cog"]

Output: 5

Explanation: As one shortest transformation is "hit" -> "hot" -> "dot" -> "dog" -> "cog",
return its length 5.
Example 2:

Input:
beginWord = "hit"
endWord = "cog"
wordList = ["hot","dot","dog","lot","log"]

Output: 0

Explanation: The endWord "cog" is not in wordList, therefore no possible transformation.
*/