//Given a matrix consists of 0 and 1, find the distance of the nearest 0 for each cell.
//
// The distance between two adjacent cells is 1.
//
//
//
// Example 1:
//
//
//Input:
//[[0,0,0],
// [0,1,0],
// [0,0,0]]
//
//Output:
//[[0,0,0],
// [0,1,0],
// [0,0,0]]
//
//
// Example 2:
//
//
//Input:
//[[0,0,0],
// [0,1,0],
// [1,1,1]]
//
//Output:
//[[0,0,0],
// [0,1,0],
// [1,2,1]]
//
//
//
//
// Note:
//
//
// The number of elements of the given matrix will not exceed 10,000.
// There are at least one 0 in the given matrix.
// The cells are adjacent in only four directions: up, down, left and right.
//
//


package graph_dfs_bfs.Topic_SourceToTargetProblems.Case_ShortestPathWithUniformEdge_BFS.Case_MultipleSourcesToTargets;

import java.util.LinkedList;
import java.util.Queue;

public class _01Matrix {
    public int[][] updateMatrix(int[][] matrix) {
        Queue<int[]> q = new LinkedList<>();
        int m=matrix.length, n=matrix[0].length;
        for(int i=0;i<m;i++) {
            for(int j=0;j<n;j++){
                if(matrix[i][j]==0) q.add(new int[]{i,j});
            }
        }

        boolean[][] visited = new boolean[m][n];
        int[][] dirs = {{1,0},{-1,0},{0,1},{0,-1}};
        int level = 0;
        while(!q.isEmpty()) {
            int size = q.size();
            for(int i=0;i<size;i++) {
                int[] cur = q.remove();
                int x = cur[0], y = cur[1];
                if (visited[x][y]) continue;
                matrix[x][y] = level;
                visited[x][y] = true;
                for(int[] dir:dirs) {
                    int nx = x+dir[0], ny = y+dir[1];
                    if(nx>=0 && nx<m && ny>=0 && ny<n && !visited[nx][ny])q.add(new int[]{nx,ny});
                }
            }
            level++;
        }
        return matrix;
    }

    public static void main(String[] as) {
        int[][] m = {
                {0,1,0},
                {0,1,0},
                {0,1,0},
                {0,1,0},
                {0,1,0}};

//        [[0,1,0],
//        [0,2,0],
//        [0,2,0],
//        [0,2,0],
//        [0,2,0]]


        new _01Matrix().updateMatrix(m);
        System.out.println("asdf");
    }
}
