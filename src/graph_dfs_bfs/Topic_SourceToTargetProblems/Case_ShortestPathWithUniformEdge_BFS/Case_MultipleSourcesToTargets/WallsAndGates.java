/*
-1 - A wall or an obstacle.
0 - A gate.
INF - Infinity means an empty room. We use the value 2^31 - 1 = 2147483647 to represent INF as you may assume that the distance to a gate is less than 2147483647.
Fill each empty room with the distance to its nearest gate. If it is impossible to reach a ROOM, that room should remain filled with INF

Example1

Input:
[[2147483647,-1,0,2147483647],[2147483647,2147483647,2147483647,-1],[2147483647,-1,2147483647,-1],[0,-1,2147483647,2147483647]]
Output:
[[3,-1,0,1],[2,2,1,-1],[1,-1,2,-1],[0,-1,3,4]]

Explanation:
the 2D grid is:
INF  -1  0  INF
INF INF INF  -1
INF  -1 INF  -1
  0  -1 INF INF
the answer is:
  3  -1   0   1
  2   2   1  -1
  1  -1   2  -1
  0  -1   3   4
Example2

Input:
[[0,-1],[2147483647,2147483647]]
Output:
[[0,-1],[1,2]]
*/

package graph_dfs_bfs.Topic_SourceToTargetProblems.Case_ShortestPathWithUniformEdge_BFS.Case_MultipleSourcesToTargets;

import java.util.LinkedList;

/*
   parallel bfs: O(m*n) !!!!!!
        这是一道多源头多终点的BFS问题。可以多个源头同时放进queue， 然后一层一层得去遍历。利用先访问到的便是最短的距离为原则，时间复杂度为n * m.
   normal bfs: O(k*m*n)  k:num of gates
*/
public class WallsAndGates {

    // O（m*n）
    int MAX_VALUE = 2147483647;//2^31 - 1 = 2147483647
    public void wallsAndGates(int[][] rooms) {
        LinkedList<int[]> que = new LinkedList<>();
        for (int i = 0; i < rooms.length; i++) {
            for (int j = 0; j < rooms[0].length; j++) {
                if (rooms[i][j]==0) que.add(new int[]{i,j});
            }
        }
        int level = 0;
        int[][] directions = new int[][]{{-1,0},{1, 0}, {0,-1}, {0,1 }};
        while (!que.isEmpty()) {
            int levelSize = que.size();
            for (int i = 0; i < levelSize; i++) {
                int[] node = que.poll();
                int x = node[0], y=node[1];
                if (rooms[x][y]==MAX_VALUE || rooms[x][y]==0) { // cut search space
                    rooms[x][y]=level;
                    for (int[] dir: directions) {
                        int nx = x+dir[0], ny=y+dir[1];
                        if (nx>=0&&nx<rooms.length && ny>=0 && ny<rooms[0].length && rooms[nx][ny]==MAX_VALUE) que.add(new int[]{nx,ny});
                    }
                }
            }
            level++;
        }
    }


    /*
       bfs from every gate  MLE?? -> O(k*m*n)
     */
//    public void wallsAndGates_naive(int[][] rooms) {
//        for (int i = 0; i < rooms.length; i++) {
//            for (int j = 0; j < rooms[0].length; j++) {
//                if (rooms[i][j]==0) bfs_naive(rooms, i,j);
//            }
//        }
//    }
//
//    private void bfs_naive(int[][] rooms, int row, int col) {
//        LinkedList<int[]> que = new LinkedList<>();
//        boolean[][] visited = new boolean[rooms.length][rooms[0].length];
//        que.add(new int[]{row, col});
//        visited[row][col] = true;
//        int[][] directions = new int[][]{{-1,0},{1, 0}, {0,-1}, {0,1 }};
//        int level=0;
//        while (!que.isEmpty()) {
//            int levelSize = que.size();
//            for (int i = 0; i < levelSize; i++) {
//                int[] node = que.poll();
//                int x = node[0], y = node[1];
//                if (rooms[x][y]>0) {
//                    if(level>=rooms[x][y]) continue;  // cut search space...
//                    rooms[x][y] = level;
//                }
//                visited[x][y] = true;
//
//                for (int[] dir: directions) {
//                    int nx = x+dir[0], ny=y+dir[1];
//                    if (nx>=0&&nx<rooms.length && ny>=0 && ny<rooms[0].length && !visited[nx][ny] && rooms[nx][ny]>0) que.add(new int[]{nx,ny});
//                }
//            }
//            level++;
//        }
//    }

    public static void main(String[] args) {
//        Input
//                [[2147483647,-1,0,2147483647],
//                [2147483647,2147483647,2147483647,-1],
//                 [2147483647,-1,2147483647,-1],
//                 [0,-1,2147483647,2147483647]]
//        Output
//                [[3,-1,2,1],[2,2,1,-1],[1,-1,2,-1],[2,-1,3,4]]
//        Expected
//                [[3,-1,0,1],[2,2,1,-1],[1,-1,2,-1],[0,-1,3,4]]

        int[][] rooms = new int[][]{
                {2147483647,-1,0,2147483647},
                {2147483647,2147483647,2147483647,-1},
                {2147483647,-1,2147483647,-1},
                {0,-1,2147483647,2147483647}};
        new WallsAndGates().wallsAndGates(rooms);
        System.out.println("ASDF");

    }
}

