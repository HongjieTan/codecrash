//Given an N x N grid containing only values 0 and 1, where 0 represents water and 1 represents land,
// find a water cell such that its distance to the nearest land cell is maximized and return the distance.
//
// The distance used in this problem is the Manhattan distance: the distance between two cells (x0, y0) and (x1, y1) is |x0 - x1| + |y0 - y1|.
//
// If no land or water exists in the grid, return -1.
//
//
//
// Example 1:
//
//
//
//
//Input: [
// [1,0,1],
// [0,0,0],
// [1,0,1]]
//Output: 2
//Explanation:
//The cell (1, 1) is as far as possible from all the land with distance 2.
//
//
// Example 2:
//
//
//
//
//Input: [
// [1,0,0],
// [0,0,0],
// [0,0,0]]
//Output: 4
//Explanation:
//The cell (2, 2) is as far as possible from all the land with distance 4.
//
//
//
//
// Note:
//
//
// 1 <= grid.length == grid[0].length <= 100
// grid[i][j] is 0 or 1
//
// Related Topics Breadth-first Search Graph

package graph_dfs_bfs.Topic_SourceToTargetProblems.Case_ShortestPathWithUniformEdge_BFS.Case_MultipleSourcesToTargets;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

/*
    what's the difference? no difference...
    - maxDistance between all 1's and 0's
    - minDistance between all 1's and 0's
 */
public class AsFarFromLandAsPossible {

    // O(m*n), good!!!
    // All lands bfs simultaneously, every cell is processed only once...
    public int maxDistance(int[][] grid) {
        int m=grid.length, n=grid[0].length;
        int[][] distance = new int[m][n];
        for(int i=0;i<m;i++) Arrays.fill(distance[i], -1);
        int[][] dirs = {{0,1}, {0,-1}, {1,0},{-1,0}};

        Queue<int[]> q = new LinkedList<>();
        for(int i=0;i<m;i++) {
            for(int j=0;j<n;j++) {
                if (grid[i][j] == 1) q.add(new int[]{i, j, 0});
            }
        }

        while (!q.isEmpty()) {
            int[] cur = q.remove();
            int x=cur[0], y=cur[1], dist=cur[2];
            for(int[] dir: dirs) {
                int nx = x+dir[0], ny = y+dir[1];
                if(nx>=0 && nx<m && ny>=0 && ny<n && grid[nx][ny]==0 && distance[nx][ny]==-1) {
                    distance[nx][ny] = dist+1;
                    q.add(new int[]{nx, ny, dist+1});
                }
            }
        }

        int res = -1;
        for(int i=0;i<m;i++) {
            for(int j=0;j<n;j++) {
                if(grid[i][j]==0) res = Math.max(res, distance[i][j]);
            }
        }
        return res;
    }


    // O(m*n*lands)
    public int maxDistance_v0(int[][] grid) {
        int m=grid.length, n=grid[0].length;
        int[][] distance = new int[m][n];
        for(int i=0;i<m;i++) Arrays.fill(distance[i], Integer.MAX_VALUE);

        int[][] dirs = {{0,1}, {0,-1}, {1,0},{-1,0}};
        for(int i=0;i<m;i++) {
            for(int j=0;j<n;j++) {
                if(grid[i][j]==1) {
                    Queue<int[]> q = new LinkedList<>();
                    q.add(new int[]{i,j,0});
                    while(!q.isEmpty()) {
                        int[] cur = q.remove();
                        int x=cur[0], y=cur[1], dist=cur[2];
                        for(int[] dir: dirs) {
                            int nx = x+dir[0], ny = y+dir[1];
                            if(nx>=0 && nx<m && ny>=0 && ny<n && grid[nx][ny]==0 && (dist+1)<distance[nx][ny]) {
                                distance[nx][ny] = dist+1;
                                q.add(new int[]{nx, ny, dist+1});
                            }
                        }
                    }
                }
            }
        }

        int res = -1, lands=0;
        for(int i=0;i<m;i++) {
            for(int j=0;j<n;j++) {
                if(grid[i][j]==0)
                    res = Math.max(res, distance[i][j]);
                else
                    lands++;
            }
        }
        return lands>0?res:-1;
    }

    public static void main(String[] as) {
        int[][] grid = {
//            {1,0,1},
//            {0,0,0},
//            {1,0,1},
                {0,0,0},
                {0,0,0},
                {0,0,0},
        };
        System.out.println(new AsFarFromLandAsPossible().maxDistance(grid));
    }
}
