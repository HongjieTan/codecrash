/*
Storekeeper is a game in which the player pushes boxes around in a warehouse trying to get them to target locations.

The game is represented by a grid of size n*m, where each element is a wall, floor, or a box.

Your task is move the box 'B' to the target position 'T' under the following rules:

Player is represented by character 'S' and can move up, down, left, right in the grid if it is a floor (empty cell).
Floor is represented by character '.' that means free cell to walk.
Wall is represented by character '#' that means obstacle  (impossible to walk there).
There is only one box 'B' and one target cell 'T' in the grid.
The box can be moved to an adjacent free cell by standing next to the box and then moving in the direction of the box. This is a push.
The player cannot walk through the box.
Return the minimum number of pushes to move the box to the target. If there is no way to reach the target, return -1.



Example 1:
Input: grid = [["#","#","#","#","#","#"],
               ["#","T","#","#","#","#"],
               ["#",".",".","B",".","#"],
               ["#",".","#","#",".","#"],
               ["#",".",".",".","S","#"],
               ["#","#","#","#","#","#"]]
Output: 3
Explanation: We return only the number of times the box is pushed.

Example 2:

Input: grid = [["#","#","#","#","#","#"],
               ["#","T","#","#","#","#"],
               ["#",".",".","B",".","#"],
               ["#","#","#","#",".","#"],
               ["#",".",".",".","S","#"],
               ["#","#","#","#","#","#"]]
Output: -1
Example 3:

Input: grid = [["#","#","#","#","#","#"],
               ["#","T",".",".","#","#"],
               ["#",".","#","B",".","#"],
               ["#",".",".",".",".","#"],
               ["#",".",".",".","S","#"],
               ["#","#","#","#","#","#"]]
Output: 5
Explanation:  push the box down, left, left, up and up.
Example 4:

Input: grid = [["#","#","#","#","#","#","#"],
               ["#","S","#",".","B","T","#"],
               ["#","#","#","#","#","#","#"]]
Output: -1


Constraints:

1 <= grid.length <= 20
1 <= grid[i].length <= 20
grid contains only characters '.', '#',  'S' , 'T', or 'B'.
There is only one character 'S', 'B' and 'T' in the grid.
*/

package graph_dfs_bfs.Topic_SourceToTargetProblems.Case_ShortestPathWithUniformEdge_BFS;
import java.util.*;

public class MinimumMovesToMoveBoxToTheirTargetLocation {

    // two BFS
    char[][] grid;
    int[][] dirs = {{0,-1},{0,1},{1,0},{-1,0}};
    int n,m;
    public int minPushBox(char[][] grid) {
        this.n = grid.length;
        this.m = grid[0].length;
        this.grid = grid;

        int[] player=null, box=null, target=null;
        for(int i=0;i<n;i++) {
            for(int j=0;j<m;j++) {
                if(grid[i][j]=='S') player = new int[]{i,j};
                if(grid[i][j]=='B') box = new int[]{i,j};
                if(grid[i][j]=='T') target = new int[]{i,j};
            }
        }

        Queue<int[]> q = new LinkedList<>(); // [player_x, player_y, box_x, box_y, moves]
        boolean[][][][] visited = new boolean[n][m][n][m];
        q.add(new int[]{player[0], player[1], box[0],box[1], 0});

        while(!q.isEmpty()) {
            int[] cur = q.remove();
            int[] p = {cur[0], cur[1]};
            int[] b = {cur[2], cur[3]};
            int moves=cur[4];

            if (visited[p[0]][p[1]][b[0]][b[1]]) continue; // cut search space
            visited[p[0]][p[1]][b[0]][b[1]] = true;

            if(Arrays.equals(b,target)) return moves;
            for(int[] d:dirs) {
                int[] nb = {b[0]+d[0], b[1]+d[1]};
                int[] stand_pos = {b[0]-d[0], b[1]-d[1]};
                if (nb[0]>=0 && nb[0]<n && nb[1]>=0 && nb[1]<m && grid[nb[0]][nb[1]]!='#'
                        && !visited[b[0]][b[1]][nb[0]][nb[1]] && canReach(p,stand_pos,b) ) {
                    q.add(new int[]{b[0], b[1], nb[0], nb[1], moves+1});
                }
            }
        }
        return -1;
    }

    boolean canReach(int[] player, int[] target, int[] box) {
        if(target[0]<0 || target[0]>=n || target[1]<0 || target[1]>=m) return false;
        boolean[][] visited = new boolean[n][m];
        Queue<int[]> q = new LinkedList<>();
        q.add(player);
        while(!q.isEmpty()) {
            int[] cur = q.remove();
            if(Arrays.equals(cur, target)) return true;
            if (visited[cur[0]][cur[1]]) continue; // cut search space as early as possible
            visited[cur[0]][cur[1]] = true;
            for(int[] d: dirs) {
                int nx = cur[0]+d[0], ny = cur[1]+d[1];
                if(nx>=0 && nx<n && ny>=0 && ny<m && !visited[nx][ny] && grid[nx][ny]!='#' && !(nx==box[0]&&ny==box[1])) {
                    q.add(new int[]{nx, ny});
                }
            }
        }
        return false;
    }

    public static void main(String[] as) {
        char[][] g1 = {{'#','#','#','#','#','#'},
                        {'#','T','#','#','#','#'},
                        {'#','.','.','B','.','#'},
                        {'#','.','#','#','.','#'},
                        {'#','.','.','.','S','#'},
                        {'#','#','#','#','#','#'}};

        char[][] g2 = {{'#','#','#','#','#','#'},
                {'#','T','#','#','#','#'},
                {'#','.','.','B','.','#'},
                {'#','#','#','#','.','#'},
                {'#','.','.','.','S','#'},
                {'#','#','#','#','#','#'}};


        char[][] g3 = {{'#','#','#','#','#','#'},
                {'#','T','.','.','#','#'},
                {'#','.','#','B','.','#'},
                {'#','.','.','.','.','#'},
                {'#','.','.','.','S','#'},
                {'#','#','#','#','#','#'}};


        char[][] g4 = {{'#','#','#','#','#','#','#'},
                {'#','S','#','.','B','T','#'},
                {'#','#','#','#','#','#','#'}};

        char[][] g5 = {
                {'#','#','#','#','#','#'},
                {'#','T','.','.','#','#'},
                {'#','.','#','B','.','#'},
                {'#','.','.','.','.','#'},
                {'#','.','.','.','S','#'},
                {'#','#','#','#','#','#'}};
        char[][] g6 ={
                {'#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#'},
                {'#','.','.','.','.','.','.','.','.','.','.','.','#','#','#','#'},
                {'#','.','#','#','#','#','.','#','#','#','#','.','#','#','#','.'},
                {'#','.','.','.','.','.','.','#','T','#','.','.','#','#','#','.'},
                {'#','.','.','.','#','.','.','.','.','.','.','.','#','#','#','.'},
                {'#','.','.','.','.','.','B','.','.','.','.','.','#','#','#','.'},
                {'#','.','#','#','#','#','#','#','#','#','#','.','#','#','#','.'},
                {'#','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.'},
                {'#','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.'},
                {'#','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.'},
                {'#','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.'},
                {'#','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.'},
                {'#','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.'},
                {'#','.','.','.','.','.','.','.','S','.','.','.','.','.','.','.'},
                {'#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#'}};


        char[][] g7 = {
                {'#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#'},
                {'#','.','.','.','.','.','.','.','.','.','.','.','#','#','#','#','#','#','#','#'},
                {'#','.','.','.','#','#','.','#','#','#','#','.','#','#','#','.','#','#','T','#'},
                {'#','.','.','.','.','.','.','#','.','#','.','.','#','#','#','.','#','#','.','#'},
                {'#','.','.','.','#','.','.','.','.','.','.','.','#','#','#','.','#','#','.','#'},
                {'#','.','#','.','.','.','.','.','.','.','.','.','#','#','#','.','#','#','.','#'},
                {'#','.','#','.','#','#','#','#','#','#','#','.','#','#','#','.','#','#','.','#'},
                {'#','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','#','.','.','#'},
                {'#','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','#','.','.','#'},
                {'#','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','#'},
                {'#','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','#','.','.','#'},
                {'#','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','#','.','.','#'},
                {'#','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','#','.','.','#'},
                {'#','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','#','.','.','#'},
                {'#','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','#','.','.','#'},
                {'#','.','B','.','.','.','.','.','.','.','.','.','.','.','.','.','#','.','.','#'},
                {'#','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','#','.','.','#'},
                {'#','.','.','.','.','.','.','.','.','.','.','.','.','.','.','.','#','.','.','#'},
                {'#','.','.','.','.','.','.','.','S','.','.','.','.','.','.','.','#','.','.','#'},
                {'#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#'}};
        System.out.println(new MinimumMovesToMoveBoxToTheirTargetLocation().minPushBox(g1));
        System.out.println(new MinimumMovesToMoveBoxToTheirTargetLocation().minPushBox(g2));
        System.out.println(new MinimumMovesToMoveBoxToTheirTargetLocation().minPushBox(g3));
        System.out.println(new MinimumMovesToMoveBoxToTheirTargetLocation().minPushBox(g4));
        System.out.println(new MinimumMovesToMoveBoxToTheirTargetLocation().minPushBox(g5));
        System.out.println(new MinimumMovesToMoveBoxToTheirTargetLocation().minPushBox(g6));
        System.out.println(new MinimumMovesToMoveBoxToTheirTargetLocation().minPushBox(g7));
    }
}
/*
by cuiaoxiang:
typedef pair<int, int> ii;
const int d[4][2] = {{1, 0}, {0, 1}, {-1, 0}, {0, -1}};
const int N = 20;
int flag[N][N][N][N];

struct Node {
    int px, py, bx, by;
};

class Solution {
public:
    int minPushBox(vector<vector<char>>& a) {
        int n = a.size(), m = a[0].size();
        int px, py, bx, by, tx, ty;
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < m; ++j) {
                if (a[i][j] == 'S') {
                    px = i;
                    py = j;
                    a[i][j] = '.';
                } else if (a[i][j] == 'T') {
                    tx = i;
                    ty = j;
                    a[i][j] = '.';
                } else if (a[i][j] == 'B') {
                    bx = i;
                    by = j;
                    a[i][j] = '.';
                }
            }
        }
        memset(flag, 255, sizeof(flag));
        deque<Node> Q;
        Q.push_back({px, py, bx, by});
        flag[px][py][bx][by] = 0;
        while (!Q.empty()) {
            auto [px, py, bx, by] = Q.front();
            if (bx == tx && by == ty) return flag[px][py][bx][by];
            Q.pop_front();
            for (int k = 0; k < 4; ++k) {
                int nx = px + d[k][0];
                int ny = py + d[k][1];
                if (nx < 0 || nx >= n || ny < 0 || ny >= m || a[nx][ny] != '.') continue;
                if (nx == bx && ny == by) continue;
                if (flag[nx][ny][bx][by] >= 0) continue;
                flag[nx][ny][bx][by] = flag[px][py][bx][by];
                Q.push_front({nx, ny, bx, by});
            }
            if (abs(px - bx) + abs(py - by) == 1) {
                int k;
                for (k = 0; k < 4; ++k) {
                    int nx = px + d[k][0];
                    int ny = py + d[k][1];
                    if (nx == bx && ny == by) break;
                }
                int nbx = bx + d[k][0];
                int nby = by + d[k][1];
                if (!(nbx < 0 || nbx >= n || nby < 0 || nby >= m || a[nbx][nby] != '.') && flag[bx][by][nbx][nby] < 0) {
                    flag[bx][by][nbx][nby] = flag[px][py][bx][by] + 1;
                    Q.push_back({bx, by, nbx, nby});
                }
            }
        }
        return -1;
    }
};


by neal_wu:
const int INF = 1e9 + 5;

const int DIRS = 4;
const int DR[] = {-1,  0, +1,  0};
const int DC[] = { 0, +1,  0, -1};

const int RC_MAX = 24;
const char PLAYER = 'S', BOX = 'B', TARGET = 'T';
const char EMPTY = '.', WALL = '#';

struct state {
    int player_r, player_c;
    int box_r, box_c;
    int dist;
};

int R, C;
int PR, PC, BR, BC, TR, TC;
vector<vector<char>> grid;
int _dist[RC_MAX][RC_MAX][RC_MAX][RC_MAX];
queue<state> q;

bool valid(int r, int c) {
    return 0 <= r && r < R && 0 <= c && c < C;
}

int &dist(const state &s) {
    return _dist[s.player_r][s.player_c][s.box_r][s.box_c];
}

void bfs_check(state s, int add) {
    s.dist += add;

    if (s.dist < dist(s)) {
        dist(s) = s.dist;
        q.push(s);
    }
}

int bfs() {
    memset(_dist, 63, sizeof(_dist));
    q = queue<state>();
    bfs_check({PR, PC, BR, BC, 0}, 0);
    int best = INF;

    while (!q.empty()) {
        state top = q.front(); q.pop();

        if (top.dist > dist(top))
            continue;

        if (top.box_r == TR && top.box_c == TC)
            best = min(best, top.dist);

        // cout << top.player_r << ' ' << top.player_c << ' ' << top.box_r << ' ' << top.box_c << ' ' << top.dist << endl;

        for (int dir = 0; dir < DIRS; dir++) {
            int nr = top.player_r + DR[dir];
            int nc = top.player_c + DC[dir];

            if (!valid(nr, nc) || grid[nr][nc] == WALL)
                continue;

            if (nr == top.box_r && nc == top.box_c) {
                int nbr = nr + DR[dir];
                int nbc = nc + DC[dir];

                if (valid(nbr, nbc) && grid[nbr][nbc] != WALL)
                    bfs_check({nr, nc, nbr, nbc, top.dist}, 1);
            } else {
                bfs_check({nr, nc, top.box_r, top.box_c, top.dist}, 0);
            }
        }
    }

    return best < INF ? best : -1;
}

class Solution {
public:
    int minPushBox(vector<vector<char>>& _grid) {
        grid = _grid;
        R = grid.size();
        C = grid.empty() ? 0 : grid[0].size();

        for (int r = 0; r < R; r++)
            for (int c = 0; c < C; c++)
                if (grid[r][c] == PLAYER) {
                    PR = r;
                    PC = c;
                    grid[r][c] = EMPTY;
                } else if (grid[r][c] == BOX) {
                    BR = r;
                    BC = c;
                    grid[r][c] = EMPTY;
                } else if (grid[r][c] == TARGET) {
                    TR = r;
                    TC = c;
                    grid[r][c] = EMPTY;
                }

        // cout << PR << ' ' << PC << ' ' << BR << ' ' << BC << ' ' << ' ' << TR << ' ' << TC << endl;
        return bfs();
    }
};



by top:
Heuristic (an under-estimate of the remaining moves required) is the Manhattan distance between box and target.
A state consist of box and person locations together.

Repeatedly pop the state with the lowest heuristic + previous moves off the heap.
Attempt to move the person in all 4 directions.
If any direction moves the person to the box, check if the box can move to the nex position in the grid.

        rows, cols = len(grid), len(grid[0])
        for r in range(rows):
            for c in range(cols):
                if grid[r][c] == "T":
                    target = (r, c)
                if grid[r][c] == "B":
                    start_box = (r, c)
                if grid[r][c] == "S":
                    start_person = (r, c)

        def heuristic(box):
            return abs(target[0] - box[0]) + abs(target[1] - box[1])

        def out_bounds(location):  # return whether the location is in the grid and not a wall
            r, c = location
            if r < 0 or r >= rows:
                return True
            if c < 0 or c >= cols:
                return True
            return grid[r][c] == "#"

        heap = [[heuristic(start_box), 0, start_person, start_box]]
        visited = set()

        while heap:
            _, moves, person, box = heapq.heappop(heap)
            if box == target:
                return moves
            if (person, box) in visited: # do not visit same state again
                continue
            visited.add((person, box))

            for dr, dc in [[0, 1], [1, 0], [-1, 0], [0, -1]]:
                new_person = (person[0] + dr, person[1] + dc)
                if out_bounds(new_person):
                    continue

                if new_person == box:
                    new_box = (box[0] + dr, box[1] + dc)
                    if out_bounds(new_box):
                        continue
                    heapq.heappush(heap, [heuristic(new_box) + moves + 1, moves + 1, new_person, new_box])
                else:
                    heapq.heappush(heap, [heuristic(box) + moves, moves, new_person, box]) # box remains same

        return -1


by second:
First define a function to check from current state, what are the possible neighbouring states (use BFS to check if we can move the player to required location).
Notice that the state includes both the location of the box and the player.
Second BFS to see if we can reach the target location.

class Solution:
    def minPushBox(self, grid: List[List[str]]) -> int:
        dire = [(1,0),(0,1),(-1,0),(0,-1)]

        def can_get(cur_b,cur_p,tar):
            seen,cur = set([cur_p]),set([cur_p])
            while cur:
                tmp = []
                for loc in cur:
                    for x,y in dire:
                        if 0<= loc[0]+x < len(grid) and 0 <= loc[1] + y < len(grid[0]) and (loc[0]+x,loc[1] +y) != cur_b and grid[loc[0] +x][loc[1] +y] != '#' and (loc[0]+x,loc[1] +y) not in seen:
                            tmp += [(loc[0]+x,loc[1] +y)]
                cur = set(tmp)
                seen |= cur
                if tar in seen:
                    return True
            return False

        for i in range(len(grid)):
            for j in range(len(grid[0])):
                if grid[i][j] == 'B': box = (i,j)
                if grid[i][j] == 'S': player = (i,j)
                if grid[i][j] == 'T': target = (i,j)

        seen,cur,res = set([(box,player)]), set([(box,player)]), 0
        while cur:
            tmp = []
            res += 1
            for b,p in cur:
                for x,y in dire:
                    if 0<= b[0]+x < len(grid) and 0 <= b[1] + y < len(grid[0]) and grid[b[0]+x][b[1]+y] != '#' and can_get(b,p,(b[0]-x,b[1]-y)) and ((b[0]+x,b[1]+y),b) not in seen:
                        tmp += [((b[0]+x,b[1]+y),b)]
            cur = set(tmp)
            seen |= cur
            for x,y in dire:
                if (target,(target[0]+x,target[1]+y)) in seen:
                    return res
        return -1


by 3rd:
For me, this is a typical bfs question to find the minimum step, so the key point is to find out the state.
For this problem, I choose the coordinates of box and storekeeper as the state.
Because the grid length is from 1 to 20, we can use one 32 bit integer to present all the coordinates.
Every step, move storekeeper 1 step, if it meet the box, then the box move 1 step in the same direction.
Because it want to know the minimum step the box move, so when the first time box on the target pocision, it may not be the minimum anwser.
So we compare every one and find the minimum step.

class Solution {
    int[][] moves = new int[][]{{0,-1}, {0,1}, {-1,0}, {1,0}};
    public int minPushBox(char[][] grid) {
        int[] box = null, target = null, storekeeper = null;
        int n = grid.length, m = grid[0].length;
        for (int i = 0; i < n; i++) for (int j = 0; j < m; j++) {
            if (grid[i][j] == 'B') box = new int[]{i, j};
            else if (grid[i][j] == 'T') target = new int[]{i, j};
            else if (grid[i][j] == 'S') storekeeper = new int[]{i, j};
        }
        Queue<Integer> q = new LinkedList<>();
        Map<Integer, Integer> dis = new HashMap<>();
        int start = encode(box[0], box[1], storekeeper[0], storekeeper[1]);
        dis.put(start, 0);
        q.offer(start);
        int ret = Integer.MAX_VALUE;
        while (!q.isEmpty()) {
            int u = q.poll();
            int[] du = decode(u);
            if (dis.get(u) >= ret) continue;
            if (du[0] == target[0] && du[1] == target[1]) {
                ret = Math.min(ret, dis.get(u));
                continue;
            }
            int[] b = new int[]{du[0], du[1]};
            int[] s = new int[]{du[2], du[3]};
			// move the storekeeper for 1 step
            for (int[] move : moves) {
                int nsx = s[0] + move[0];
                int nsy = s[1] + move[1];
                if (nsx < 0 || nsx >= n || nsy < 0 || nsy >= m || grid[nsx][nsy] == '#') continue;
				// if it meet the box, then the box move in the same direction
                if (nsx == b[0] && nsy == b[1]) {
                    int nbx = b[0] + move[0];
                    int nby = b[1] + move[1];
                    if (nbx < 0 || nbx >= n || nby < 0 || nby >= m || grid[nbx][nby] == '#') continue;
                    int v = encode(nbx, nby, nsx, nsy);
                    if (dis.containsKey(v) && dis.get(v) <= dis.get(u) + 1) continue;
                    dis.put(v, dis.get(u) + 1);
                    q.offer(v);
                } else { // if the storekeeper doesn't meet the box, the position of the box do not change
                    int v = encode(b[0], b[1], nsx, nsy);
                    if (dis.containsKey(v) && dis.get(v) <= dis.get(u)) continue;
                    dis.put(v, dis.get(u));
                    q.offer(v);
                }
            }
        }
        return ret == Integer.MAX_VALUE ? -1 : ret;
    }
    int encode(int bx, int by, int sx, int sy) {
        return (bx << 24) | (by << 16) | (sx << 8) | sy;
    }
    int[] decode(int num) {
        int[] ret = new int[4];
        ret[0] = (num >>> 24) & 0xff;
        ret[1] = (num >>> 16) & 0xff;
        ret[2] = (num >>> 8) & 0xff;
        ret[3] = num & 0xff;
        return ret;
    }
}

*/