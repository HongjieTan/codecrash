/*
Given a m x n binary matrix mat. In one step, you can choose one cell and flip it and all the four neighbours of it if they exist (Flip is changing 1 to 0 and 0 to 1). A pair of cells are called neighboors if they share one edge.

Return the minimum number of steps required to convert mat to a zero matrix or -1 if you cannot.

Binary matrix is a matrix with all cells equal to 0 or 1 only.

Zero matrix is a matrix with all cells equal to 0.



Example 1:


Input: mat = [[0,0],[0,1]]
Output: 3
Explanation: One possible solution is to flip (1, 0) then (0, 1) and finally (1, 1) as shown.
Example 2:

Input: mat = [[0]]
Output: 0
Explanation: Given matrix is a zero matrix. We don't need to change it.
Example 3:

Input: mat = [[1,1,1],[1,0,1],[0,0,0]]
Output: 6
Example 4:

Input: mat = [[1,0,0],[1,0,0]]
Output: -1
Explanation: Given matrix can't be a zero matrix


Constraints:

m == mat.length
n == mat[0].length
1 <= m <= 3
1 <= n <= 3
mat[i][j] is 0 or 1.
*/
package graph_dfs_bfs.Topic_SourceToTargetProblems.Case_ShortestPathWithUniformEdge_BFS;


import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

// bfs
public class MinNumberOfFlipsToConvertBinaryMatrixToZeroMatrix {
    // concise version, represent state using bit manipulation
    public int minFlips(int[][] mat) {
        int[][] dirs = {{1,0},{-1,0},{0,1},{0,-1}};
        int n = mat.length, m = mat[0].length;
        int start = 0;
        for(int i=0;i<n;i++) {
            for(int j=0;j<m;j++) {
                start |= mat[i][j]<<i*m+j;
            }
        }

        Queue<Integer> q = new LinkedList<>();
        Set<Integer> seen = new HashSet<>();
        q.add(start);
        int level = 0;
        while(!q.isEmpty()) {
            int size = q.size();
            for(int i=0;i<size;i++) {
                int cur = q.remove();
                if(cur==0) return level;

                for(int r=0;r<n;r++) {
                    for(int c=0;c<m;c++) {
                        int next = cur;
                        next ^= 1<<r*m+c;
                        for(int[] dir: dirs) {
                            int nr = r+dir[0], nc = c+dir[1];
                            if(nr>=0 && nr<n && nc>=0 && nc<m) {
                                next ^= 1<<nr*m+nc;
                            }
                        }
                        if(!seen.contains(next)) {
                            seen.add(next);
                            q.add(next);
                        }
                    }
                }
            }
            level++;
        }
        return -1;
    }




//    int n,m;
//    public int minFlips(int[][] mat) {
//        int[][] dirs = {{1,0},{-1,0},{0,1},{0,-1}};
//        n = mat.length;
//        m = mat[0].length;
//
//        Set<String> seen = new HashSet<>();
//        Queue<int[][]> q = new LinkedList<>();
//        q.add(copy(mat));
//        int level = 0;
//        while(!q.isEmpty()) {
//            int size = q.size();
//            for(int i=0;i<size;i++) {
//                int[][] cur = q.remove();
//                if(check(cur)) return level;
//
//                for(int r=0;r<n;r++) {
//                    for(int c=0;c<m;c++) {
//                        int[][] nm = copy(cur);
//                        nm[r][c] = (nm[r][c]+1)%2;
//                        for(int[] dir: dirs) {
//                            int nr = r+dir[0], nc = c+dir[1];
//                            if(nr>=0 && nr<n && nc>=0 && nc<m) {
//                                nm[nr][nc] = (nm[nr][nc]+1)%2;
//                            }
//                        }
//
//                        String key = key(nm);
//                        if(!seen.contains(key)) {
//                            seen.add(key);
//                            q.add(nm);
//                        }
//                    }
//                }
//            }
//            level++;
//        }
//        return -1;
//    }
//
//    String key(int[][] mat) {
//        StringBuilder sb = new StringBuilder();
//        for(int i=0;i<n;i++) {
//            for(int j=0;j<m;j++) {
//                sb.append(mat[i][j]);
//            }
//        }
//        return sb.toString();
//    }
//
//    int[][] copy(int[][] mat) {
//        int[][] nm = new int[n][m];
//        for(int i=0;i<n;i++) {
//            for(int j=0;j<m;j++) {
//                nm[i][j]=mat[i][j];
//            }
//        }
//        return nm;
//    }
//
//    boolean check(int[][] mat) {
//        for(int i=0;i<n;i++) {
//            for(int j=0;j<m;j++) {
//                if(mat[i][j]==1) return false;
//            }
//        }
//        return true;
//    }
}

/*
by uwi:
class Solution {
    public int minFlips(int[][] mat) {
        int u = 0;
        int n = mat.length, m = mat[0].length;
        for(int i = 0;i < mat.length;i++){
            for(int j = 0;j < mat[0].length;j++){
                u |= mat[i][j]<<i*mat[0].length+j;
            }
        }
        int[] ptns = new int[n*m];
        for(int i = 0;i < n;i++){
            for(int j = 0;j < m;j++){
                for(int k = 0;k < n;k++){
                    for(int l = 0;l < m;l++){
                        if(Math.abs(i-k) + Math.abs(j-l) <= 1){
                            ptns[i*m+j] |= 1<<k*m+l;
                        }
                    }
                }
            }
        }

        int ans = 99;
        for(int i = 0;i < 1<<n*m;i++){
            int v = u;
            for(int j = 0;j < n*m;j++){
                if(i<<~j<0){
                    v ^= ptns[j];
                }
            }
            if(v == 0){
                ans = Math.min(ans, Integer.bitCount(i));
            }
        }
        if(ans == 99)return -1;
        return ans;
    }
}

by top:

private static final int[] d = {0, 0, 1, 0, -1, 0};
public int minFlips(int[][] mat) {
    int start = 0, m = mat.length, n = mat[0].length;
    for (int i = 0; i < m; ++i)
        for (int j = 0; j < n; ++j)
            start |= mat[i][j] << i * n + j; // convert the matrix to an int.
    Queue<Integer> q = new LinkedList<>(Arrays.asList(start));
    Set<Integer> seen = new HashSet<>();
    for (int step = 0; !q.isEmpty(); ++step) {
        for (int sz = q.size(); sz > 0; --sz) {
            int cur = q.poll();
            if (cur == 0)
                return step;
            for (int i = 0; i < m; ++i) {
                for (int j = 0; j < n; ++j) {
                    int next = cur;
                    for (int l = 0; l < 5; ++l) { // flip the cell (i, j) and its neighbors.
                        int r = i + d[l], c = j + d[l + 1];
                        if (r >= 0 && r < m && c >= 0 && c < n)
                            next ^= 1 << r * n + c;
                    }
                    if (seen.add(next)) // seen it before ?
                        q.offer(next); // no, put it into the Queue.
                }
            }
        }
    }
    return -1; // impossible to become to 0.
}
*/
