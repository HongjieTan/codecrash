// In an n*n grid, there is a snake that spans 2 cells and starts moving from the top left corner at (0, 0) and (0, 1).
// The grid has empty cells represented by zeros and blocked cells represented by ones. The snake wants to reach the lower right corner at (n-1, n-2) and (n-1, n-1).
//
// In one move the snake can:
//
//
// Move one cell to the right if there are no blocked cells there. This move keeps the horizontal/vertical position of the snake as it is.
// Move down one cell if there are no blocked cells there. This move keeps the horizontal/vertical position of the snake as it is.
// Rotate clockwise if it's in a horizontal position and the two cells under it are both empty. In that case the snake moves from (r, c) and (r, c+1) to (r, c) and (r+1, c).
//
// Rotate counterclockwise if it's in a vertical position and the two cells to its right are both empty. In that case the snake moves from (r, c) and (r+1, c) to (r, c) and (r, c+1).
//
//
//
// Return the minimum number of moves to reach the target.
//
// If there is no way to reach the target, return -1.
//
//
// Example 1:
//
//
//
//
//Input: grid = [[ 0,0,0,0,0,1],
//               [ 1,1,0,0,1,0],
//               [0,0,0,0,1,1],
//               [0,0,1,0,1,0],
//               [0,1,1,0,0,0],
//               [0,1,1,0,0,0]]
//Output: 11
//Explanation:
//One possible solution is [right, right, rotate clockwise, right, down, down, down, down, rotate counterclockwise, right, down].
//
//
// Example 2:
//
//
//Input: grid = [[0,0,1,1,1,1],
//               [0,0,0,0,1,1],
//               [1,1,0,0,0,1],
//               [1,1,1,0,0,1],
//               [1,1,1,0,0,1],
//               [1,1,1,0,0,0]]
//Output: 9
//
//
//
// Constraints:
//
//
// 2 <= n <= 100
// 0 <= grid[i][j] <= 1
// It is guaranteed that the snake starts at empty cells.

package graph_dfs_bfs.Topic_SourceToTargetProblems.Case_ShortestPathWithUniformEdge_BFS;
import java.util.ArrayDeque;
import java.util.Queue;


// bfs, good!!! 这里bfs的状态是（x,y,d）, 即位置状态(x,y)和方向状态（d）
public class MinMovesToReachTargetWithRotations {
    public int minimumMoves(int[][] grid) {
        int m=grid.length, n=grid[0].length;
        Queue<int[]> q = new ArrayDeque<>();
        q.add(new int[]{0,1,0,0}); // (x,y,d,moves): d: 0-horizontal, 1-vertical
        boolean[][][] visited = new boolean[m][n][2];
        while(!q.isEmpty()) {
            int[] cur = q.remove();
            int x=cur[0],y=cur[1],d=cur[2],moves=cur[3];
            if(x==n-1 && y==n-1 && d==0) return moves;
            if(visited[x][y][d]) continue;
            visited[x][y][d] = true;

            if(d==0) {
                // right
                if(y+1<n && grid[x][y+1]==0) q.add(new int[]{x,y+1,d,moves+1});
                // down
                if(x+1<m && grid[x+1][y-1]==0 && grid[x+1][y]==0) q.add(new int[]{x+1,y,d,moves+1});
                // rotate
                if(x+1<m && grid[x+1][y-1]==0 && grid[x+1][y]==0) q.add(new int[]{x+1,y-1,(d+1)%2,moves+1});
            }

            if(d==1){
                // right
                if(y+1<n && grid[x-1][y+1]==0 && grid[x][y+1]==0) q.add(new int[]{x,y+1,d,moves+1});
                // down
                if(x+1<m && grid[x+1][y]==0) q.add(new int[]{x+1,y,d,moves+1});
                // rotate
                if(y+1<n && grid[x-1][y+1]==0 && grid[x][y+1]==0) q.add(new int[]{x-1,y+1,(d+1)%2,moves+1});
            }
        }

        return -1;
    }

    public static void main(String[] as) {
//        int[][] grid ={
//                {0,0,1,1,1,1},
//                {0,0,0,0,1,1},
//                {1,1,0,0,0,1},
//                {1,1,1,0,0,1},
//                {1,1,1,0,0,1},
//                {1,1,1,0,0,0}};


        int[][] grid = {
                {0,0,0,0,0,1},
                {1,1,0,0,1,0},
                {0,0,0,0,1,1},
                {0,0,1,0,1,0},
                {0,1,1,0,0,0},
                {0,1,1,0,0,0}
        };
        System.out.println(new MinMovesToReachTargetWithRotations().minimumMoves(grid));


    }
}


/*
by uwi:

class Solution {
    public int minimumMoves(int[][] g) {
        int n = g.length, m = g[0].length;
        Queue<int[]> q = new ArrayDeque<>();
        q.add(new int[]{0, 0, 0});
        int[][][] d = new int[2][n][m];
        for(int i = 0;i < 2;i++){
            for(int j = 0;j < n;j++){
                Arrays.fill(d[i][j], 99999999);
            }
        }
        d[0][0][0] = 0;
        while(!q.isEmpty()){
            int[] cur = q.poll();
            int st = cur[0], r = cur[1], c = cur[2];
            int ur = r + (st == 1 ? 1 : 0), uc = c + (st == 1 ? 0 : 1);
            if(r == n-1 && c == n-2 && st == 0){
                return d[st][r][c];
            }
            // r
            if(uc+1 < m && g[r][c+1] == 0 && g[ur][uc+1] == 0 &&
                    d[st][r][c+1] > d[st][r][c] + 1){
                d[st][r][c+1] = d[st][r][c] + 1;
                q.add(new int[]{st, r, c+1});
            }
            // d
            if(ur+1 < n && g[r+1][c] == 0 && g[ur+1][uc] == 0 &&
                    d[st][r+1][c] > d[st][r][c] + 1){
                d[st][r+1][c] = d[st][r][c] + 1;
                q.add(new int[]{st, r+1, c});
            }
            int xr = r + (st == 0 ? 1 : 0), xc = c + (st == 0 ? 0 : 1);
            if(xr < n && xc < m && g[xr][xc] == 0 && g[r+1][c+1] == 0 &&
                    d[st^1][r][c] > d[st][r][c] + 1){
                d[st^1][r][c] = d[st][r][c] + 1;
                q.add(new int[]{st^1, r, c});
            }
        }
        return -1;
    }
}

*/