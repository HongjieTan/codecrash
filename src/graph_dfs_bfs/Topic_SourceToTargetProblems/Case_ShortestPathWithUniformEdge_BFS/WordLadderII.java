package graph_dfs_bfs.Topic_SourceToTargetProblems.Case_ShortestPathWithUniformEdge_BFS;

import java.util.*;

/**
 * Created by thj on 2018/8/5.
 *
 *
 *  Problem: find all shortest paths from source to target!!!
 *
 *  1. just one bfs!!!!!!!! very good!!!!:
 *      store 'path information' in queue instead of word
 *
 *  2. leetcode top solution： it is shit!!!!!!
 *      first use bfs to build the graph, then dfs to construct the paths
 *
 *
 */
public class WordLadderII {

    public List<List<String>> findLadders(String beginWord, String endWord, List<String> wordList) {

        List<List<String>> ans = new ArrayList<>();

        Set<String> dic = new HashSet<>(wordList);
        LinkedList<List<String>> que = new LinkedList<>();

        que.add(Arrays.asList(beginWord));
        boolean levelFound = false;
        while (!que.isEmpty()) {
            int levelSize = que.size();
            for (int i = 0; i < levelSize; i++) {
                List<String> curPath = que.poll();
                String curWord = curPath.get(curPath.size()-1);

                if (curWord.equals(endWord)) { // check point
                    levelFound = true;
                    ans.add(curPath);
                }

                dic.remove(curWord);
                for (String nextWord: getNeighbors(curWord, dic)) {
                    List<String> newPath = new ArrayList<>(curPath);
                    newPath.add(nextWord);
                    que.add(newPath);
                }
            }

            if (levelFound) break;
        }

        return ans;
    }





    // TLE again!!!  This is shit！！！！
    // Because: it is good for AllPaths case, but not good for shortestPaths case!!!!
    //   当前结点+所有缓存的邻居结点的所有路径     -> 一定是当前结点的所有路径
    //   当前结点+所有缓存的邻居结点的所有最短路径  -> 不一定是当前结点的所有最短路径！！！！！ 坑！！
//    private List<List<String>> dfs_v2(Map<String, List<List<String>>> cache, String curWord, String endWord, Set<String> dic, int depth) {
//        if (depth<=0) return new ArrayList<>();
//
//        if (cache.containsKey(curWord)) {
//            return cache.get(curWord);
//        }
//
//        List<List<String>> paths = new ArrayList<>();
//        if (curWord.equals(endWord)) {
//            List<String> path = new ArrayList<>();
//            path.add(endWord);
//            paths.add(path);
//        } else {
//
//            List<String> neighbors = getNeighbors(curWord, dic);
//            for (String nb: neighbors) {
//                dic.remove(nb); // needed to avoid cycle visit...
//                List<List<String>> subPaths = dfs_v2(cache, nb, endWord, dic, depth-1);
//                for (List<String> path: subPaths) {
//                    linkedlist_queue<String> newPath = new linkedlist_queue<>(path);
//                    newPath.add(0, curWord);
//                    if (path.size() <= depth-1) {  // important!!!!!!!
//                        paths.add(newPath);
//                    }
//                }
//                dic.add(nb);
//            }
//
//
//        }
//
//
//        if (!paths.isEmpty()) { // important!!!!!
//            cache.put(curWord, paths);
//        }
//        return paths;
//    }
//
//    // it will cause TLE
//    // 可行，但也不是好方案，会导致很多重复计算，时间开销大
//    private void dfs_v1(List<List<String>> ans, List<String> path, String curWord, String endWord, Set<String> dic, int depth) {
//        if (depth<=0) return;
//
//        if (curWord.equals(endWord)) {
//            ans.add(new ArrayList<>(path));
//            return;
//        }
//
//
//        for (String nb: getNeighbors(curWord, dic)) {
//            path.add(nb);
//            dic.remove(nb);
//
//            dfs_v1(ans, path, nb, endWord, dic, depth-1);
//
//            path.remove(path.size()-1);
//            dic.add(nb);
//        }
//    }
//
//
//    // bfs to construct graph
//    private int bfs(String beginWord, String endWord, List<String> wordList) {
//        Set<String> dic = new HashSet<>(wordList);
//        Queue<String> que = new linkedlist_queue<>();
//        que.add(beginWord);
//
//        int depth=0;
//        while (!que.isEmpty()) {
//            depth++;
//            int levelSize = que.size();
//            for (int i = 0; i < levelSize; i++) {
//                String word = que.poll();
//                dic.remove(word);
//                List<String> neighbors = getNeighbors(word, dic);
//                for (int j = 0; j < neighbors.size(); j++) {
//                    que.add(neighbors.get(j));
//                }
//                if (word.equals(endWord)) return depth;
//            }
//        }
//
//        return -1;
//    }

    private List<String> getNeighbors(String word, Set<String> dic) {
        List<String> nbs = new ArrayList<>();
        char[] chs = word.toCharArray();
        for (int i = 0; i < chs.length; i++) {
            char origin_char = chs[i];
            for (int c = 'a'; c <= 'z'; c++) {
                if (c!=origin_char) {
                    chs[i] = (char)c;
                    String newWord = String.valueOf(chs); // do not use chs.toString() !!
                    if (dic.contains(newWord)) {
                        nbs.add(newWord);
                    }
                    chs[i] = origin_char; // revert
                }
            }
        }
        return nbs;
    }


    public static void main(String[] args){
        String beginWord = "hit";
        String endWord = "cog";
        List<String> wordList = Arrays.asList(new String[]{"hot","dot","dog","lot","log","cog"});
        System.out.println(new WordLadderII().findLadders(beginWord, endWord, wordList));

//        Map<String, List<List<String>>> cache = new HashMap<>();
//        Set<String> dic = new HashSet<>();
////        dic.add("hot");
//        dic.add("lot");
//        dic.add("log");
//        dic.add("dot");
//        dic.add("cog");
//        dic.add("dog");
//
//        List<List<String>> subPaths = new WordLadderII().dfs_v2(cache, "hot", "cog", dic, 4);
//        System.out.println(subPaths);
    }
//    ["hit","hot","dot","dog","cog"],
//            ["hit","hot","lot","log","cog"]


//    hit-hot-dot-dog-cog
//           \ |    | /
//            lot-log





}

/*
Given two words (beginWord and endWord), and a dictionary's word list, find all shortest transformation sequence(s) from beginWord to endWord, such that:

Only one letter can be changed at a time
Each transformed word must exist in the word list. Note that beginWord is not a transformed word.
Note:

Return an empty list if there is no such transformation sequence.
All words have the same length.
All words contain only lowercase alphabetic characters.
You may assume no duplicates in the word list.
You may assume beginWord and endWord are non-empty and are not the same.
Example 1:

Input:
beginWord = "hit",
endWord = "cog",
wordList = ["hot","dot","dog","lot","log","cog"]

Output:
[
  ["hit","hot","dot","dog","cog"],
  ["hit","hot","lot","log","cog"]
]
Example 2:

Input:
beginWord = "hit"
endWord = "cog"
wordList = ["hot","dot","dog","lot","log"]

Output: []

Explanation: The endWord "cog" is not in wordList, therefore no possible transformation.
*/