//In an N by N square grid, each cell is either empty (0) or blocked (1).
//
// A clear path from top-left to bottom-right has length k if and only if it is composed of cells C_1, C_2, ..., C_k such that:
//
//
// Adjacent cells C_i and C_{i+1} are connected 8-directionally (ie., they are different and share an edge or corner)
// C_1 is at location (0, 0) (ie. has value grid[0][0])
// C_k is at location (N-1, N-1) (ie. has value grid[N-1][N-1])
// If C_i is located at (r, c), then grid[r][c] is empty (ie. grid[r][c] == 0).
//
//
// Return the length of the shortest such clear path from top-left to bottom-right. If such a path does not exist, return -1.
//
//
//
// Example 1:
//
//
//Input: [[0,1],[1,0]]
//Output: 2
//
//
//
// Example 2:
//
//
//Input: [[0,0,0],[1,1,0],[1,1,0]]
//Output: 4
//
//
//
//
// Note:
//
//
// 1 <= grid.length == grid[0].length <= 100
// grid[r][c] is 0 or 1
//

package graph_dfs_bfs.Topic_SourceToTargetProblems.Case_ShortestPathWithUniformEdge_BFS;

import java.util.LinkedList;
import java.util.Queue;

public class ShortestPathInBinaryMatrix {
    public int shortestPathBinaryMatrix(int[][] grid) {
        int n=grid.length;
        if(grid[0][0]==1 || grid[n-1][n-1]==1) return -1;
        int[][] dirs = {{0,1},{0,-1},{1,0},{-1,0},{1,1},{1,-1},{-1,1},{-1,-1}};
        Queue<int[]> q= new LinkedList<>();
        q.add(new int[]{0,0,1});
        while(!q.isEmpty()) {
            int[] cur = q.remove();
            if(cur[0]==n-1 && cur[1]==n-1) return cur[2];
            for(int[] dir: dirs) {
                int nx=cur[0]+dir[0], ny=cur[1]+dir[1];
                if(nx>=0&&nx<n&&ny>=0&&ny<n && grid[nx][ny]==0) {
                    grid[nx][ny]=1;
                    q.add(new int[]{nx,ny, cur[2]+1});
                }
            }
        }
        return -1;
    }

    public int shortestPathBinaryMatrix_v0(int[][] grid) {
        int n=grid.length;
        if(grid[0][0]==1 || grid[n-1][n-1]==1) return -1;
        int[][] dirs = {{0,1},{0,-1},{1,0},{-1,0},{1,1},{1,-1},{-1,1},{-1,-1}};

        boolean[][] visited = new boolean[n][n];
        Queue<int[]> q= new LinkedList<>();
        int level = 1;
        q.add(new int[]{0,0});
        while(!q.isEmpty()) {
            int size = q.size();
            for(int i=0;i<size;i++) {
                int[] cur = q.remove();
                for(int[] dir: dirs) {
                    int nx=cur[0]+dir[0], ny=cur[1]+dir[1];
                    if(nx>=0&&nx<n&&ny>=0&&ny<n && !visited[nx][ny] && grid[nx][ny]==0) {
                        if(nx==n-1 && ny==n-1) return level+1;
                        visited[nx][ny]=true;
                        q.add(new int[]{nx,ny});
                    }
                }
            }
            level++;
        }
        return -1;
    }


    /*
    def shortestPathBinaryMatrix(grid):
        n = len(grid)
        if grid[0][0] or grid[n-1][n-1]:
            return -1
        q = [(0, 0, 1)]
        for i, j, d in q:
            if i == n-1 and j == n-1: return d
            for x, y in ((i-1,j-1),(i-1,j),(i-1,j+1),(i,j-1),(i,j+1),(i+1,j-1),(i+1,j),(i+1,j+1)):
                if 0 <= x < n and 0 <= y < n and not grid[x][y]:
                    grid[x][y] = 1
                    q.append((x, y, d+1))
        return -1
    */

}


