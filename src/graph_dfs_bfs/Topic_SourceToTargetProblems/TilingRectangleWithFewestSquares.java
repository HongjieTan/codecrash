// Given a rectangle of size n x m, find the minimum number of integer-sided squares that tile the rectangle.
//
//
// Example 1:
//
//
//
//
// Input: n = 2, m = 3
// Output: 3
// Explanation: 3 squares are necessary to cover the rectangle.
// 2 (squares of 1x1)
// 1 (square of 2x2)
//
// Example 2:
//
//
//
//
//Input: n = 5, m = 8
//Output: 5
//
//
// Example 3:
//
//
//
//
//Input: n = 11, m = 13
//Output: 6
//
//
//
// Constraints:
//
//
// 1 <= n <= 13
// 1 <= m <= 13
//  https://leetcode.com/discuss/interview-question/373237/google-onsite-tiling-a-rectangle-with-the-fewest-squares
package graph_dfs_bfs.Topic_SourceToTargetProblems;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;


// good problem!
// idea: treet it like a search problem
// topics: backtrack + dp
public class TilingRectangleWithFewestSquares {
    Map<Long, Integer> memo = new HashMap<>();
    int ret = Integer.MAX_VALUE;
    int n, m;
    public int tilingRectangle(int n, int m) {
        this.n = n;
        this.m = m;
        int[] state = new int[n + 1];
        dfs(state, 0);
        return ret;
    }

    void dfs(int[] state, int step) {
        if (step >= ret) return;

        long key = genkey(state);
        if (memo.containsKey(key) && step >= memo.get(key)) return;
        memo.put(key, step);

        boolean isFull = true;
        int minh = m+1, pos = 0; // find lowest position
        for (int i = 1; i <= n; i++) {
            if (state[i] < m) isFull = false;
            if (state[i] < minh) { minh = state[i]; pos = i;}
        }

        if (isFull) {
            ret = Math.min(ret, step);
            return;
        }

        // go to next state (add square from (pos, minh))
        int l = 1;
        while(minh+l<=m && pos+l-1<=n && state[pos+l-1]==minh) l++;
        int maxlen = l-1;

        for (int len = maxlen; len>=1; len--) {
            int[] tmp = Arrays.copyOf(state, n+1);

            for (int i = pos; i <=pos+len-1; i++) state[i] = minh + len;
            dfs(state, step+1);

            state = tmp; // backtrack
        }
    }

    long genkey(int[] state) {
        long ret = 0, base = 1;
        for (int x : state) {
            ret += x * base;
            base *= m+1;
        }
        return ret;
    }

    public static void main(String[] a) {
        System.out.println(new TilingRectangleWithFewestSquares().tilingRectangle(2, 3));//3
        System.out.println(new TilingRectangleWithFewestSquares().tilingRectangle(3, 2));//3
        System.out.println(new TilingRectangleWithFewestSquares().tilingRectangle(5, 8));//5
        System.out.println(new TilingRectangleWithFewestSquares().tilingRectangle(8, 5));//5
        System.out.println(new TilingRectangleWithFewestSquares().tilingRectangle(11, 13));//6
        System.out.println(new TilingRectangleWithFewestSquares().tilingRectangle(13, 11));//6
        System.out.println(new TilingRectangleWithFewestSquares().tilingRectangle(11, 7));//6
    }
}


/*
by top post:
The basic idea is to fill the entire block bottom up.
In every step, find the lowest left unfilled square first, and select a square with different possible sizes to fill it.
We maintain a height array (skyline) with length n while dfs. This skyline is the identity of the state.
The final result we ask for is the minimum number of squares for the state [m, m, m, m, m, m, m] (The length of this array is n).
Of course, backtrack without optimization will have a huge time complexity, but it can be pruned or optimized by the following three methods.

When the current cnt (that is, the number of squares) of this skyline has exceeded the value of the current global optimal solution, then return directly.
When the current skyline has been traversed, and the previous cnt is smaller than the current cnt, then return directly.
When we find the empty square in the lowest left corner, we pick larger size for the next square first.
This can make the program converge quickly. (It is a very important optimization)

class Solution {
    Map<Long, Integer> set = new HashMap<>();
    int res = Integer.MAX_VALUE;
    public int tilingRectangle(int n, int m) {
        if (n == m) return 1;
        if (n > m) {
            int temp = n;
            n = m;
            m = temp;
        }
        dfs(n, m, new int[n + 1], 0);
        return res;
    }

    private void dfs(int n, int m, int[] h, int cnt) {
        if (cnt >= res) return;
        boolean isFull = true;
        int pos = -1, minH = Integer.MAX_VALUE;
        for (int i = 1; i <= n; i++) {
            if (h[i] < m) isFull = false;
            if (h[i] < minH) {
                pos = i;
                minH = h[i];
            }
        }
        if (isFull) {
            res = Math.min(cnt, res);
            return;
        }

        long key = 0, base = m + 1;
        for (int i = 1; i <= n; i++) {
            key += h[i] * base;
            base *= m + 1;
        }
        if (set.containsKey(key) && set.get(key) <= cnt) return;
        set.put(key, cnt);

        int end = pos;
        while (end + 1 <= n && h[end + 1] == h[pos] && (end + 1 - pos + 1 + minH) <= m) end++;
        for (int j = end; j >= pos; j--) {
            int curH = j - pos + 1;

            int[] next  = Arrays.copyOf(h, n + 1);
            for (int k = pos; k <= j; k++) {
                next[k] += curH;
            }
            dfs(n, m, next, cnt + 1);
        }

    }
}

by uwi:
class Solution {
    public int tilingRectangle(int n, int m) {
        ans = 99999999;
        boolean[][] g = new boolean[n][m];
        dfs(0, 0, g, 0);
        return ans;
    }

    int ans;

    void dfs(int r, int c, boolean[][] g, int num)
    {
        int n = g.length, m = g[0].length;
        if(num >= ans)return;
        if(r >= n){
            ans = num;
            return;
        }
        if(c >= m){
            dfs(r+1, 0, g, num); return;
        }
        if(g[r][c]){
            dfs(r, c+1, g, num); return;
        }
        out:
        for(int K = Math.min(n-r, m-c);K >= 1;K--){
            for(int i = 0;i < K;i++){
                for(int j = 0;j < K;j++){
                    if(g[r+i][c+j])break out;
                }
            }
            for(int i = 0;i < K;i++){
                for(int j = 0;j < K;j++){
                    g[r+i][c+j] = true;
                }
            }
            dfs(r, c+1, g, num+1);
            for(int i = 0;i < K;i++){
                for(int j = 0;j < K;j++){
                    g[r+i][c+j] = false;
                }
            }
        }
    }

    public void tf(boolean[]... b)
    {
        for(boolean[] r : b) {
            for(boolean x : r)System.out.print(x?'#':'.');
            System.out.println();
        }
        System.out.println();
    }
}

by lee:
Solution here  http://int-e.eu/~bf3/squares/young.cc
Cheat Table http://int-e.eu/~bf3/squares/young.txt

Though it's a well known problem,
is it really problem for interview?

I think if we can only cuts rectagle into rectaglex,
it can be solved by dp,
which makes more sense as an interview problem.

As you can see in the example 1 and exampl 2,
the original rectangle are cuts into rectagles each time.
While example 3 has an annoying small square in the middle.

But it seems that, [11, 13] in the example 3 is the only special case within size of 13.

*/