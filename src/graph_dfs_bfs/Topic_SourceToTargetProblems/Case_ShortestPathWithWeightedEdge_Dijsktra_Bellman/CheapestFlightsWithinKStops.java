package graph_dfs_bfs.Topic_SourceToTargetProblems.Case_ShortestPathWithWeightedEdge_Dijsktra_Bellman;


import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

/**
 *
 *  - BellmanFord(DP): O(n*k)   good!
 *
 *  - other DP: todo later...
 *
 *  - dijkstra's variant (seems just bfs...) / bfs / dfs : O(n^k), need to explore every k-length path...
 */
public class CheapestFlightsWithinKStops {

    final int MAX_VALUE = 1000001;
    public int findCheapestPrice_bellmanford_X2(int n, int[][] flights, int src, int dst, int K) {
        int maxLen = K+1;
        int[][] distance = new int[maxLen+1][n];
//        int[][] prev = new int[K+1][n];
        Arrays.fill(distance[0], MAX_VALUE);
        distance[0][src]=0;
        for(int k=1;k<=maxLen;k++) {
            for (int i=0;i<n;i++) distance[k][i]=distance[k-1][i];
            for(int[] f: flights) {
                int u=f[0], v=f[1], w=f[2];
                if(distance[k-1][u]+w<distance[k][v]) { // Notice here! Not distance[k-1][u]+w<distance[k-1][v] !!
//                    prev[k][v]=u;
                    distance[k][v]=distance[k-1][u]+w;
                }
            }
        }
        return distance[maxLen][dst]==MAX_VALUE?-1:distance[maxLen][dst];
    }

    public int findCheapestPrice_bellmanford(int n, int[][] flights, int src, int dst, int K) {
        int MAX_VALUE = 100001;
        int[] dist = new int[n];
        Arrays.fill(dist, MAX_VALUE);
        dist[src] = 0;
        for (int i = 1; i <=K+1; i++) {
            int[] curDist = new int[n]; // Notice the difference with origin algorithm! Use a new dist array to avoid interference from previous iteration!!
            for (int j=0; j<n; j++) curDist[j] = dist[j];
            for (int[] edge: flights) {
                int u=edge[0], v=edge[1], w=edge[2];
                if (dist[u]+w < curDist[v]) // Notice, use dist[u]（previous iteration） and curDist[v] (current iteration)!
                    curDist[v] = dist[u]+w;
            }
            dist = curDist;
        }
        return dist[dst]==MAX_VALUE?-1:dist[dst];
    }

    /*
        The key difference with the classic Dijkstra algo is, we don't maintain the global optimal distance to each node,
        i.e. ignore below optimization:
            alt ← dist[u] + length(u, v)
            if alt < dist[v]: ...
        Because there could be routes which their length is shorter but pass more stops, and those routes don't necessarily constitute the best route in the end.
        To deal with this, rather than maintain the optimal routes with 0..K stops for each node, the solution simply put all possible routes into the priority queue,
        so that all of them has a chance to be processed. IMO, this is the most brilliant part.
        And the solution simply returns the first qualified route, it's easy to prove this must be the best route.

        Time complexity is just like bfs/dfs, as it search all paths, it's exponential
     */
    public int findCheapestPrice_dijkstra(int n, int[][] flights, int src, int dst, int K) {
        // build edge matrix
        Map<Integer, Map<Integer, Integer>> adjMap = new HashMap<>();
        for (int[] f: flights) {
            if (!adjMap.containsKey(f[0])) adjMap.put(f[0], new HashMap<>());
            adjMap.get(f[0]).put(f[1], f[2]);
        }

        PriorityQueue<int[]> pq = new PriorityQueue<>((a,b)->a[1]-b[1]); //a[0]: node, a[1]:dist from source, a[2]: stops
        pq.add(new int[]{src,0,0});

        while (!pq.isEmpty()) {
            int[] curNodeInfo = pq.remove();
            int curNode = curNodeInfo[0];
            int curDist = curNodeInfo[1];
            int curStop = curNodeInfo[2];
            if (curNode==dst) return curDist;
            if (curStop<=K) {
                Map<Integer, Integer> nbs = adjMap.getOrDefault(curNode, new HashMap<>());
                for (int nb: nbs.keySet()) {
                    pq.add(new int[]{nb, curDist+nbs.get(nb), curStop+1});
                }
            }
        }
        return -1;
    }




    public static void main(String[] args) {
//        3
//                [[0,1,100],[1,2,100],[0,2,500]]
//        0
//        2
//        1

//        3
//                [[0,1,100],[1,2,100],[0,2,500]]
//        0
//        2
//        1
//        Output
//        500
//        Expected
//        200

//        int[][] edges = new int[][] {{4,1,1},{1,2,3},{0,3,2},{0,4,10},{3,1,1},{1,4,3}};
//        int n = 5;
//        int src = 2;
//        int dst = 1;
//        int K = 1;
        int[][] edges = new int[][] {{0,1,100}, {1,2,100}, {0,2,500}};
        int n = 3;
        int src = 0;
        int dst = 2;
        int K = 1;
//        System.out.println(new CheapestFlightsWithinKStops().findCheapestPrice_bellmanford(n,edges,src,dst,K));
        System.out.println(new CheapestFlightsWithinKStops().findCheapestPrice_bellmanford_X2(n,edges,src,dst,K));
    }
}

/*
It is obvious that filights within K stops is cheaper than K-1 stops. So there is a very simple DP idea.
We maintain a table DP[K][N]. We could deduce K from K-1 as code below.

   public int findCheapestPrice(int n, int[][] flights, int src, int dst, int K) {
        int matrix[][] = new int[n][n];
        for (int i=0; i < flights.length; i++) {
            matrix[flights[i][0]][flights[i][1]] = flights[i][2];
        }
        //dp[k][i] stores min value from src to i, when k stops are reached
        int dp[][] = new int[K+1][n];
        for (int i = 0; i<n; i++) {
            if(matrix[src][i] > 0)
                dp[0][i] = matrix[src][i];
            else
                dp[0][i] = Integer.MAX_VALUE;
        }

        int k=1;
        for (; k <=K; k++) {
            boolean changed = false;
            for (int i=0; i <n; i++) {
                dp[k][i] = dp[k-1][i];
                for (int j=0; j<n; j++) {
                    if (j != i) {
                        if ( dp[k-1][j] != Integer.MAX_VALUE && matrix[j][i] != 0) {
                            if (dp[k][i] > dp[k-1][j]+matrix[j][i]) {
                                dp[k][i] = dp[k-1][j]+matrix[j][i];
                                changed = true;
                            }
                        }
                    }
                }
            }
            if (!changed) break;
        }
        k = Integer.min(K, k);
        return  dp[k][dst] == Integer.MAX_VALUE ? -1: dp[k][dst];
    }
*/

/*
    public int findCheapestPrice(int n, int[][] flights, int src, int dst, int K) {
        int[][] distance = new int[k+1][n];
        int[][] prev = new int[k+1][n];
        Arrays.fill(distance[0], Integer.MAX_VALUE);
        distance[src]=0;

        for(int k=1;k<=K;k++) {
            System.arraycopy(distance[k-1], 0, n, distance[k], 0, n);
            for(int[] f: flights) {
                int u=f[0], v=f[1], w=f[2];
                if(distance[k-1][u]+w<distance[k][v]) {
                    prev[v]=u;
                    distance[k][v]=distance[k-1][u]+w;
                }
            }
        }
        return distance[K][dst];
    }
*/

/*
There are n cities connected by m flights. Each fight starts from city u and arrives at v with a price w.

Now given all the cities and flights, together with starting city src and the destination dst,
your task is to find the cheapest price from src to dst with up to k stops. If there is no such route, output -1.

Example 1:
Input:
n = 3, edges = [[0,1,100],[1,2,100],[0,2,500]]
src = 0, dst = 2, k = 1
Output: 200
Explanation:
The graph looks like this:

The cheapest price from city 0 to city 2 with at most 1 stop costs 200, as marked red in the picture.
Example 2:
Input:
n = 3, edges = [[0,1,100],[1,2,100],[0,2,500]]
src = 0, dst = 2, k = 0
Output: 500
Explanation:
The graph looks like this:

The cheapest price from city 0 to city 2 with at most 0 stop costs 500, as marked blue in the picture.
Note:

The number of nodes n will be in range [1, 100], with nodes labeled from 0 to n - 1.
The size of flights will be in range [0, n * (n - 1) / 2].
The format of each flight will be (src, dst, price).
The price of each flight will be in the range [1, 10000].
k is in the range of [0, n - 1].
There will not be any duplicated flights or self cycles.
*/