package graph_dfs_bfs.Topic_SourceToTargetProblems.Case_ShortestPathWithWeightedEdge_Dijsktra_Bellman;

import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;


public class Dijkstra {

    class Node {
        int id;
        int dist; // distance from src
        int prev;
        public Node(int id, int dist, int prev) {this.id=id;this.dist=dist;this.prev=prev;}
    }

    // O(E+VlogV)
    public int findCheapestPrice(int n, int[][] flights, int src, int dst) {
        // build adj matrix
        Map<Integer, Map<Integer, Integer>> adjMap = new HashMap<>();
        for (int[] flight: flights) {
            if (!adjMap.containsKey(flight[0])) adjMap.put(flight[0], new HashMap<>());
            adjMap.get(flight[0]).put(flight[1], flight[2]);
        }

        // init
        PriorityQueue<Node> pq = new PriorityQueue<>((a, b)->Integer.compare(a.dist, b.dist));
        Node[] nodes = new Node[n];
        for (int i = 0; i < n; i++) {
            nodes[i] = new Node(i, i==src?0:Integer.MAX_VALUE, -1);
            // pq.add(nodes[i]); // can skip
        }
        pq.add(nodes[src]);

        // greedily select the closest vertex that has not yet been processed, and performs this relaxation process on all of its outgoing edges
        while (!pq.isEmpty()) {
            Node curNode = pq.remove();
            if (curNode.id==dst) return curNode.dist; // if we only interested in single target
            Map<Integer, Integer> edges = adjMap.getOrDefault(curNode.id, new HashMap<>());
            for (int nb: edges.keySet()) {
                int newDist = curNode.dist+edges.get(nb);
                if (newDist<nodes[nb].dist) {
                    nodes[nb].dist = newDist;
                    nodes[nb].prev = curNode.id;

                    // update its order in pq, seems O(n), use TreeMap to improve it?
                    pq.remove(nodes[nb]); pq.add(nodes[nb]);

                    // 另一种是只用 pq.add(nodes[nb]);， 但在pq.pollFirst()时候过滤下已经加入的node...
                }
            }
        }
        return -1;
    }


    public static void main(String[] args) {
//        int[][] edges = new int[][] {{4,1,1},{1,2,3},{0,3,2},{0,4,10},{3,1,1},{1,4,3}};
        int[][] edges = new int[][] {{0,1,5},{0,3,2},{1,4,2},{1,2,5},{4,2,1},{3,1,2}};
        System.out.println(new Dijkstra().findCheapestPrice(5,edges,0,2 )); //7
        System.out.println(new Dijkstra().findCheapestPrice(5,edges,0,1 )); //4
    }

}

