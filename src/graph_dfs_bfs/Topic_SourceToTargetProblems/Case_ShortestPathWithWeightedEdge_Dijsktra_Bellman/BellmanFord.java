package graph_dfs_bfs.Topic_SourceToTargetProblems.Case_ShortestPathWithWeightedEdge_Dijsktra_Bellman;

import java.util.HashMap;
import java.util.Map;

public class BellmanFord {

    // O(E*V), code is simpler than dijkstra...
    public int findCheapestPrice(int n, int[][] flights, int src, int dst) {
        // build adj matrix  -> NO NEED....
//        Map<Integer, Map<Integer, Integer>> adjMap = new HashMap<>();
//        for (int[] flight: flights) {
//            if (!adjMap.containsKey(flight[0])) adjMap.put(flight[0], new HashMap<>());
//            adjMap.get(flight[0]).put(flight[1], flight[2]);
//        }

        int[] dist = new int[n];
        int[] prev = new int[n];

        // Step1. init
        for (int i = 0; i < n; i++) {
            dist[i] = i==src?0:Integer.MAX_VALUE;
            prev[i] = -1;
        }

        // Step2.relax edges repeatedly
        for (int i = 1; i <= n-1; i++) { // iterate V-1 times
            for(int[] edge: flights) {
                int u = edge[0], v = edge[1],w = edge[2];
                if (dist[u] + w < dist[v]) {
                    dist[v] = dist[u] + w;
                    prev[v] = u;
                }
            }
        }

        // Step 3: check for negative-weight cycles
        for(int[] edge: flights) {
            int u = edge[0], v = edge[1],w = edge[2];
            if (dist[u] + w < dist[v]) return -1;
        }

        return dist[dst];
    }


    public static void main(String[] args) {
        int[][] edges = new int[][] {{0,1,5},{0,3,2},{1,4,2},{1,2,5},{4,2,1},{3,1,2}};
        System.out.println(new Dijkstra().findCheapestPrice(5,edges,0,2 )); //7
        System.out.println(new Dijkstra().findCheapestPrice(5,edges,0,1 )); //4
    }
}