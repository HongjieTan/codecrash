1. Dijkstra: O(E+VlogV)
    - dijkstra is just like  bfs + priority queue + relaxation after each step， 即带松弛操作和PQ的BFS...

2. Bellman-Ford: O(V*E)
    -Slower than Dijkstra's algorithm, but capable of handling graphs in which some of the edge weights are negative numbers.
     And code is conciser...

Summary:
Dijkstra's algorithm uses a priority queue to greedily select the closest vertex that has not yet been processed, and performs this relaxation process on all of its outgoing edges;
by contrast, the Bellman–Ford algorithm simply relaxes all the edges, and does this |V|-1 times, where  |V| is the number of vertices in the graph.


'''
function Dijkstra(Graph, source):
  create vertex set Q                      // Q can be PriorityQue to improve performance

  for each vertex v in Graph:              // Initialization
      dist[v] ← v!=source? INFINITY:0     // Unknown distance from source to v
      prev[v] ← UNDEFINED                 // Previous node in optimal path from source
      Q.add_with_priority(v, dist[v])      // All nodes initially in Q (unvisited nodes)

   while Q is not empty:
      u ← Q.extract_min()                 // Node with the least distance will be selected first
      // if u== target: ...                // If we are only interested in a shortest path between vertices source and target, we can terminate the search here

      for each neighbor v of u:            // where v is still in Q.   (edge: u->v)
          alt ← dist[u] + length(u, v)
          if alt < dist[v]:                // A shorter path to v has been found
              dist[v] ← alt
              prev[v] ← u
              Q.decrease_priority(v, alt)

  return dist[], prev[]
'''

'''
function BellmanFord(list vertices, list edges, vertex source)
   ::distance[],predecessor[]

   // This implementation takes in a graph, represented as
   // lists of vertices and edges, and fills two arrays
   // (distance and predecessor) about the shortest path
   // from the source to each vertex

   // Step 1: initialize graph
   for each vertex v in vertices:
       distance[v] := inf             // Initialize the distance to all vertices to infinity
       predecessor[v] := null         // And having a null predecessor

   distance[source] := 0              // The distance from the source to itself is, of course, zero

   // Step 2: relax edges repeatedly
   for i from 1 to size(vertices)-1:
       for each edge (u, v) with weight w in edges:
           if distance[u] + w < distance[v]:
               distance[v] := distance[u] + w
               predecessor[v] := u

   // Step 3: check for negative-weight cycles
   for each edge (u, v) with weight w in edges:
       if distance[u] + w < distance[v]:
           error "Graph contains a negative-weight cycle"

   return distance[], predecessor[]
'''
