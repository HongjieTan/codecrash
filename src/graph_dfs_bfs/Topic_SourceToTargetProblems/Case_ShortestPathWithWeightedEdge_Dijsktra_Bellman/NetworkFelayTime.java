package graph_dfs_bfs.Topic_SourceToTargetProblems.Case_ShortestPathWithWeightedEdge_Dijsktra_Bellman;

import java.util.*;

public class NetworkFelayTime {
    public int networkDelayTime(int[][] times, int N, int K) {
        int[] cost = new int[N+1];
        Arrays.fill(cost, Integer.MAX_VALUE);

        Map<Integer, List<int[]>> adjMap = new HashMap<>();
        for (int[] time: times) {
            int u=time[0], v=time[1], w=time[2];
            if (!adjMap.containsKey(u)) adjMap.put(u, new ArrayList<>());
            adjMap.get(u).add(new int[]{v,w});
        }

        PriorityQueue<int[]> pq = new PriorityQueue<>((a,b)->a[1]-b[1]);
        pq.add(new int[]{K, 0});
        cost[K]=0;

        while(!pq.isEmpty()) {
            int[] cur = pq.remove();
            // if (visited.contains(cur[0])) continue; // 这里最好加这个过滤逻辑，因为下面用的pq.add(), 而不是update
            for(int[] nb: adjMap.getOrDefault(cur[0], Arrays.asList())) {
                int v=nb[0], w=nb[1];
                if(cur[1]+w < cost[v]) {
                    cost[v] = cur[1]+w;
                    pq.add(new int[]{v, cost[v]});
                }
            }
        }

        int max = -1;
        for(int i=1;i<=N;i++) {
            max = Math.max(max, cost[i]);
        }
        return max==Integer.MAX_VALUE?-1:max;
    }


    public static void main(String[] args){
        //        [[2,1,1],[2,3,1],[3,4,1]]
        // 4
        // 2
        int[][] times = new int[][]{
                {2,1,1},
                {2,3,1},
                {3,4,1},
        };
        System.out.println(new NetworkFelayTime().networkDelayTime(times, 4, 2));
    }
}

/*
There are N network nodes, labelled 1 to N.

Given times, a list of travel times as directed edges times[i] = (u, v, w), where u is the source node, v is the target node, and w is the time it takes for a signal to travel from source to target.

Now, we send a signal from a certain node K. How long will it take for all nodes to receive the signal? If it is impossible, return -1.

Note:

N will be in the range [1, 100].
K will be in the range [1, N].
The length of times will be in the range [1, 6000].
All edges times[i] = (u, v, w) will have 1 <= u, v <= N and 0 <= w <= 100.
*/