package graph_dfs_bfs.Topic_SourceToTargetProblems;

import java.util.*;

/**
 * Created by thj on 2018/8/8.
 *
 *  https://www.lintcode.com/problem/route-between-two-nodes-in-graph/description
 */
public class HasRouteBetweenNodes {

    static class DirectedGraphNode {
        int label;
        ArrayList<DirectedGraphNode> neighbors;
        DirectedGraphNode(int x) {
            label = x;
            neighbors = new ArrayList<>();
        }
    }


    public boolean hasRoute_bfs(ArrayList<DirectedGraphNode> graph, DirectedGraphNode s, DirectedGraphNode t) {
        LinkedList<DirectedGraphNode> que = new LinkedList<>();
        Set<DirectedGraphNode> visited = new HashSet<>();

        que.add(s);

        while (!que.isEmpty()) {
            DirectedGraphNode curNode = que.pop();
            visited.add(curNode);
            if (curNode == t) return true;

            for (DirectedGraphNode nb: curNode.neighbors) {
                if (!visited.contains(nb)) {
                    que.add(nb);
                }
            }
        }

        return false;
    }

    public boolean hasRoute_dfs_iterative(ArrayList<DirectedGraphNode> graph, DirectedGraphNode s, DirectedGraphNode t) {
        Stack<DirectedGraphNode> stack = new Stack<>();
        stack.push(s);
        Set<DirectedGraphNode> visited = new HashSet<>();

        while (!stack.isEmpty()) {
            DirectedGraphNode curNode = stack.pop();
            visited.add(curNode);
            if (curNode == t) return true;

            for (DirectedGraphNode nb: curNode.neighbors) {
                if (!visited.contains(nb)) {
                    stack.push(nb);
                }
            }
        }

        return false;
    }


    public boolean hasRoute_dfs_recursion(ArrayList<DirectedGraphNode> graph, DirectedGraphNode s, DirectedGraphNode t) {
        Set<DirectedGraphNode> visited = new HashSet<>();
        return dfs(s, t, visited);
    }


    public boolean dfs(DirectedGraphNode s, DirectedGraphNode t, Set<DirectedGraphNode> visited) {
        if (visited.contains(s)) return false;
        if (s==t) return true;

        visited.add(s);
        for (DirectedGraphNode nb: s.neighbors) {
            if (dfs(nb, t, visited)) return true;
        }
        return false;
    }


    public static void main(String[] args) {
        DirectedGraphNode node1 = new DirectedGraphNode(1);
        DirectedGraphNode node3 = new DirectedGraphNode(3);
        node3.neighbors.add(node1);
        ArrayList<DirectedGraphNode> graph = new ArrayList<>();
        graph.add(node1);
        graph.add(node3);
//        System.out.println(new HasRouteBetweenNodes().hasRoute(graph, node3, node1));

    }

}
