package graph_dfs_bfs.Topic_SourceToTargetProblems;

import java.util.LinkedList;
import java.util.PriorityQueue;


/**
 *   Solution-1： bfs+pq+memo(dijkstra):
 *          - O(m*n*log(m*n)), cool!!
 *          - 跟普通bfs解法区别就一个，就是'Q'改成'PQ'，这样每次都从最短的路径开始走，能避免重复走的问题！
 *
 *   Solution-2： bfs/dfs with memo: （use a distance cache to avoid part duplication, but still have duplication!）
 *          - O(m*n*max(m,n))   (said by leetcode solution, need confirm...I think O(m*n*m*n)?)
 *          - Notice! 似乎不能用到终点的最小距离做DP（对比LongestIncreasingLineInMatrix）,子问题的解不一定是全局最优!! DP的顺序不对，应该用从起点开始的最小距离做DP（即dijkstra！），见Solution-1！
 *          - Notice! bfs vs dfs has no difference here, as "bfs" also can not guarantee shortest distance first...
 *
 *
 *   Discuss: https://www.1point3acres.com/bbs/thread-443306-1-1.html?_dsign=a2871777
 *   - "我觉得这道题的BFS跟普通的BFS不一样的地方就在于，先放入queue中的不一定是最短的路径，比如例子里的紫色路径很可能先放入queue中，那么放入顺序，cell[1][2]就会先被dfs一次。第二次有了一个更短的路径，又会被dfs一次。这题跟minimal flight cost类似，不用priorityqueue的话，每个节点有可能被多次check。"
 *   - " 有道理。比较了一下code我才发现dijkstra和bfs区别只有一行，就是q的type不一样。"
 *
 *
 */
public class TheMazeII {
    final int[][] DIRECTIONS = new int[][]{{-1,0},{1,0},{0,-1},{0,1}};
    final int MAX_VALUE = 10001;
    public int shortestDistance_X2(int[][] maze, int[] start, int[] destination) {
        int m=maze.length, n=maze[0].length;
        PriorityQueue<int[]> pq = new PriorityQueue<>((a, b)->a[2]-b[2]);
        int[][] distance = new int[m][n];
        for(int i=0;i<m;i++) {
            for(int j=0; j<n;j++) {
                distance[i][j] = start[0]==i&&start[1]==j?0:MAX_VALUE;
            }
        }
        pq.add(new int[]{start[0], start[1], 0});
        while(!pq.isEmpty()){
            int[] cur = pq.remove();
            if(cur[0]==destination[0]&&cur[1]==destination[1]) return cur[2];
            if(cur[2]==MAX_VALUE) return -1;
            for(int[] dir: DIRECTIONS) {
                int nx=cur[0], ny=cur[1], steps=0;
                while(nx>=0&&nx<m&&ny>=0&&ny<n&&maze[nx][ny]==0) {
                    nx+=dir[0]; ny+=dir[1];steps++;
                }
                nx-=dir[0]; ny-=dir[1]; steps--;
                if(steps>0 && cur[2]+steps < distance[nx][ny]) {
                    distance[nx][ny] = cur[2]+steps;
                    pq.add(new int[]{nx, ny, cur[2]+steps});
                }
            }
        }
        return -1;
    }

    // naive bfs/dfs with memo...
    public int shortestDistance_naive(int[][] maze, int[] start, int[] destination) {
        int m=maze.length, n=maze[0].length;
        int[][] distance = new int[m][n];
        for (int i = 0; i < m*n; i++) distance[i/n][i%n]=Integer.MAX_VALUE;
        dfs(maze, start, 0, distance);
        return distance[destination[0]][destination[1]]==Integer.MAX_VALUE?-1:distance[destination[0]][destination[1]];
//        return bfs(maze, start, destination);
    }
    private void dfs(int[][] maze, int[] pos, int len, int[][] distance) {
        if (len>=distance[pos[0]][pos[1]]) return;
        distance[pos[0]][pos[1]] = len;
        for (int[] dir: DIRECTIONS) {
            int nx=pos[0], ny=pos[1], nlen=len;
            while (isValid(maze,nx+dir[0], ny+dir[1]) && maze[nx+dir[0]][ny+dir[1]]==0) {
                nx+=dir[0]; ny+=dir[1]; nlen++;
            }
            dfs(maze, new int[]{nx, ny}, nlen, distance);
        }
    }
    private boolean isValid(int[][] maze, int x, int y) {
        return x>=0 && x<maze.length && y>=0 && y<maze[0].length;
    }

    // naive bfs
    private class Node { int x, y, l;  public Node(int x, int y, int l) {this.x=x;this.y=y;this.l=l;}}
    private int bfs_naive(int[][] maze, int[] start, int[] destination) {
        int m=maze.length, n=maze[0].length;
        LinkedList<Node> que = new LinkedList<>();
        int[][] distance = new int[m][n];
        for (int i = 0; i < m*n; i++) distance[i/n][i%n]=Integer.MAX_VALUE; // cool...
        que.add(new Node(start[0], start[1], 0));
        while (!que.isEmpty()) {
            Node node = que.poll();
            if (distance[node.x][node.y] <= node.l) continue; // skip if already found route shorter..
            distance[node.x][node.y] = node.l;
            for (int[] dir: DIRECTIONS) {
                int nx=node.x, ny=node.y, nl=node.l;
                while (isValid(maze, nx+dir[0], ny+dir[1]) && maze[nx+dir[0]][ny+dir[1]]==0) {
                    nx+=dir[0]; ny+=dir[1]; nl++;
                }
                que.add(new Node(nx, ny, nl));
            }
        }
        return distance[destination[0]][destination[1]]==Integer.MAX_VALUE?-1:distance[destination[0]][destination[1]];
    }

    public static void main(String[] args) {
        int[][] maze = new int[][]{
                {0,0,1,0,0},
                {0,0,0,0,0},
                {0,0,0,1,0},
                {1,1,0,1,1},
                {0,0,0,0,0},
        };
        int[] start = new int[]{0,4};
        int[] destination = new int[]{4,4};
        System.out.println(new TheMazeII().shortestDistance_X2(maze, start, destination));
    }

    /*
    I try to solve it using DP like LongestIncreasingLineInMatrix, but fail to do it!!!!  已得到的子问题的解不一定是全局最优
    static final int MAX_VALUE = 99;
    static final int[][] DIRECTIONS = new int[][]{{0,1},{0,-1},{1,0},{-1,0}};
    public int shortestDistance_v2(int[][] maze, int[] start, int[] destination) {
        int m=maze.length, n=maze[0].length;
        int[][] memo = new int[m][n];
        boolean[][] visited = new boolean[m][n];
        int ans = Integer.MAX_VALUE;
        for(int i=0;i<m;i++) {
            for (int j = 0; j < n; j++) {
                ans = Math.min(ans, dfs(maze, start, destination, visited, memo));
            }
        }
        for(int i=0;i<m;i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(memo[i][j]+"\t");
            }
            System.out.println();
        }
        return ans;
    }
    int dfs(int[][] maze, int[] pos, int[] destination, boolean[][] visited, int[][] memo) {
        if (memo[pos[0]][pos[1]]>0) return memo[pos[0]][pos[1]];
        if (pos[0]==destination[0] && pos[1]==destination[1]) return 0;

        visited[pos[0]][pos[1]]=true;
        int minDist = MAX_VALUE;
        for (int[] dir:DIRECTIONS) {
            // scroll along one direction until stop...
            int nx=pos[0]+dir[0], ny=pos[1]+dir[1], steps = 1;
            while (nx>=0&&nx<maze.length&&ny>=0&&ny<maze[0].length &&maze[nx][ny]==0) {
                nx+=dir[0]; ny+=dir[1]; steps++;
            }
            nx-=dir[0]; ny-=dir[1]; steps--;
            if (!visited[nx][ny]) {
                int dist =  steps+dfs(maze, new int[]{nx,ny}, destination, visited, memo);
                minDist = Math.min(minDist, dist);
            }
        }
        memo[pos[0]][pos[1]]=minDist;
        return minDist;
    }*/

    /*  my first wrong answer....
    private int bfs_v0(int[][] maze, int[] start, int[] destination) {
        linkedlist_queue<int[]> que = new linkedlist_queue<>();
        linkedlist_queue<Integer> depthQue = new linkedlist_queue<>();
        boolean[][] visited = new boolean[maze.length][maze[0].length];

        int min = Integer.MAX_VALUE;
        que.add(start);
        depthQue.add(0);
        while (!que.isEmpty()) {
            int[] pos = que.poll();
            int depth = depthQue.poll();

            if (Arrays.equals(pos, destination)) {
                min = Math.min(min, depth);
                continue;
            }

            visited[pos[0]][pos[1]] = true;
            for (int[] dir: DIRECTIONS) {
                int nx = pos[0], ny = pos[1], ndepth = depth;
                // go alone one direction!!!
                while ( isValid(maze, nx+dir[0], ny+dir[1]) && maze[nx+dir[0]][ny+dir[1]]==0) {
                    nx+=dir[0]; ny+=dir[1]; ndepth++;
                }
                if (!visited[nx][ny]) { // 这里有问题！ 也许有更短的路径，这里会被过滤掉！！！！
                    que.add(new int[]{nx, ny});
                    depthQue.add(ndepth);
                }
            }
        }
        return min==Integer.MAX_VALUE?-1:min;
    }*/
}

/*
Description
There is a ball in a maze with empty spaces and walls. The ball can go through empty spaces by rolling up, down, left or right,
but it won't stop rolling until hitting a wall. When the ball stops, it could choose the next direction.

Given the ball's start position, the destination and the maze, find the shortest distance for the ball to stop at the destination.
The distance is defined by the number of empty spaces traveled by the ball from the start position (excluded) to the destination (included).
If the ball cannot stop at the destination, return -1.

The maze is represented by a binary 2D array. 1 means the wall and 0 means the empty space. You may assume that the borders of the maze are all walls.
The start and destination coordinates are represented by row and column indexes.

1.There is only one ball and one destination in the maze.
2.Both the ball and the destination exist on an empty space, and they will not be at the same position initially.
3.The given maze does not contain border (like the red rectangle in the example pictures), but you could assume the border of the maze are all walls.
4.The maze contains at least 2 empty spaces, and both the width and height of the maze won't exceed 100.

Example
Example 1:
	Input:
	(rowStart, colStart) = (0,4)
	(rowDest, colDest)= (4,4)
	0 0 1 0 0
	0 0 0 0 0
	0 0 0 1 0
	1 1 0 1 1
	0 0 0 0 0

	Output:  12

	Explanation:
	(0,4)->(0,3)->(1,3)->(1,2)->(1,1)->(1,0)->(2,0)->(2,1)->(2,2)->(3,2)->(4,2)->(4,3)->(4,4)

Example 2:
	Input:
	(rowStart, colStart) = (0,4)
	(rowDest, colDest)= (0,0)
	0 0 1 0 0
	0 0 0 0 0
	0 0 0 1 0
	1 1 0 1 1
	0 0 0 0 0

	Output:  6

	Explanation:
	(0,4)->(0,3)->(1,3)->(1,2)->(1,1)->(1,0)->(0,0)
*/



    /*

    */
