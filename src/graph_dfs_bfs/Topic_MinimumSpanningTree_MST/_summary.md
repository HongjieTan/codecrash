MST, minimum spanning tree

- kruskal: （首选，代码简单）
    - 每次选一条w最小的非联通边 （用union-find来快速判断两端点是否联通)
    - O(E*logE) or O(V*logE) (取决于图的表示方式。。。)

- prim:
    - 每次选一个w最小的顶点（每次加完定点后通过松弛操作更新未加入的顶点的w，跟dijkstra很相似）
    - O(E*logV)
