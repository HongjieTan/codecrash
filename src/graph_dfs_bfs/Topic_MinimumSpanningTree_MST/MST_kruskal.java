package graph_dfs_bfs.Topic_MinimumSpanningTree_MST;

import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;

/**
 * Created by thj on 2018/7/29.
 *
 *  prefer kruskal, it has more clean code!!!!!
 *
 *
 * 1. kruskal:  O(ElogE) or O(ElogV), union-find!!!
 *
 * 2. prim: see MST_prim.java
 */
public class MST_kruskal {


    static class Edge {
        int src;
        int dst;
        int weight;
        public Edge(int src, int dst, int weight) {
            this.src = src;
            this.dst = dst;
            this.weight = weight;
        }
    }

    private static void makeSet(int[] parent) {
        for (int i = 0; i < parent.length; i++)  parent[i] = i;
    }

    private static int find(int[] parent, int x){
        if (parent[x]!=x) {
            parent[x] = find(parent, parent[x]);  // path compression(optional)
        }
        return parent[x];
    }

    private static void union(int[] parent, int x, int y) {
        int x_set = find(parent, x);
        int y_set = find(parent, y);
        parent[y_set] = x_set;
    }

    public static void kruskal_prim(int vertices, List<Edge> edgeList) {
        List<Edge> mst = new ArrayList<>();

        int[] parent = new int[vertices];
        makeSet(parent);

        PriorityQueue<Edge> heap = new PriorityQueue<>((a,b)->(Integer.compare(a.weight, b.weight)));
        for(Edge edge: edgeList) heap.add(edge);

        int count=0;
        while (count<vertices-1) {
            if (heap.isEmpty()) throw new IllegalStateException("can not connect all vertices....");
            Edge edge = heap.poll();
            int x_set = find(parent, edge.src);
            int y_set = find(parent, edge.dst);
            if (x_set!=y_set) {
                mst.add(edge);
                count++;
                union(parent, edge.src, edge.dst);
            }

        }

        printGraph(mst);
    }

    private static void addEdge(List<Edge> edgeList, int src, int dst, int weight) {
        edgeList.add(new Edge(src, dst, weight));
    }

    private static void printGraph(List<Edge> edgeList){
        for (int i = 0; i <edgeList.size() ; i++) {
            Edge edge = edgeList.get(i);
            System.out.println("Edge-" + i + " source: " + edge.src +
                    " destination: " + edge.dst +
                    " weight: " + edge.weight);
        }
    }

    public static void main(String[] args) {
        int vertices = 6;
        List<Edge> edgeList = new ArrayList<>();
        addEdge(edgeList, 0, 1, 4);
        addEdge(edgeList, 0, 2, 3);
        addEdge(edgeList, 1, 2, 1);
        addEdge(edgeList, 1, 3, 2);
        addEdge(edgeList, 2, 3, 4);
        addEdge(edgeList, 3, 4, 2);
        addEdge(edgeList, 4, 5, 6);
        kruskal_prim(vertices, edgeList);
    }
}
