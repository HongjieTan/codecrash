package graph_dfs_bfs.Topic_MinimumSpanningTree_MST;

import java.util.PriorityQueue;

/*
一个村庄里有很多房子，有的房子旁边能建水井，有的房子只能通过与其他房子建立管道获得供水。
给出了在不同房子旁边建水井的花费，已及两个房子间建管道的花费，要求结果给出一份竞价，使得总花费尽可能少

by 网友：
- (tan: 正解!!!cool!!)
   感觉不要去考虑连通分量连通图之类的，直接用最小生成树的思路比较好。因为能建水井的房子，相当于有一条管道可以通往水源，管道长度就是建井费用，最后就是要用最小生成树把所有房子+水源都连接起来。
   +1所以事实上就是构建这样一个graph G=<V, E>, V包括了所有的房子+一个虚拟的水源节点，E包括了所有的管道和打井消耗，然后寻找G的MST即可

- 第一题就卡住了，给出了个不算特别好的算法，我又试了别的想法，但是没想出来，面试官问是要个提示还是就之前我给出的完整的算法写，我选择要了提示，面试官根据我第二个思路引导我构成联通图 最后怕时间不够写代码，提醒说用最小生成树-b
- 这题一周内已经看到两三次了。仔细想了一下，觉得比最小生成树更复杂，因为可以仅仅找多个联通分量，每个联通分量内只需要一个水井，最后代价最小就行，并不是一定要连通图。

 */
public class BuildWell {

    // kruskal version... find edge each step
    int[] parents;
    int find(int x) {
        if(x!=parents[x]) parents[x]=find(parents[x]);
        return parents[x];
    }
    public int minCost_kruskal(int n, int[] wellCost, int[][] pipeCost) {
        int ans=0;

        PriorityQueue<int[]> pq = new PriorityQueue<>((a, b)->a[2]-b[2]);
        for(int i=0;i<n;i++) {
            pq.add(new int[]{i,n,wellCost[i]});
            for(int j=i+1;j<n;j++) {
                pq.add(new int[]{i,j,pipeCost[i][j]});
            }
        }

        parents=new int[n+1];
        for(int i=0;i<n+1;i++) parents[i]=i;

        int count=0;
        while(count<n) {
            if (pq.isEmpty()) return -1; // can not connect all vertices
            int[] cur = pq.remove();
            int u=cur[0], v=cur[1], w=cur[2];

            int uroot=find(u);
            int vroot=find(v);

            if(uroot!=vroot) {
                parents[uroot]=vroot;
                count++;
                ans+=w;
            }
        }

        return ans;
    }


    // prim version, find version each step
    public int minCost_prim(int n, int[] wellCost, int[][] pipeCost) {
        int ans = 0;
        boolean[] inMst = new boolean[n+1];

        PriorityQueue<int[]> pq = new PriorityQueue<>((a,b)->a[1]-b[1]);
        // well结点作为开始点。。。
        inMst[n]=true;
        for(int i=0;i<n;i++) pq.add(new int[]{i, wellCost[i]});

        while(!pq.isEmpty()){
            int[] cur = pq.remove();
            if(inMst[cur[0]]) continue;
            inMst[cur[0]] = true;
            ans+=cur[1];

            for(int i=0;i<n;i++) {
                if(!inMst[i]) {
                    pq.add(new int[]{i, pipeCost[cur[0]][i]});
                }
            }
        }

        for(int i=0;i<n+1;i++) if(!inMst[i]) return -1; // can not connect all...
        return ans;
    }


    public static void main(String[] args) {
        int n=3;
        int[] wellCost = new int[]{5,3,9};
        int[][] pipeCost = new int[n][n];
        int i,j,w;

        i=0;j=1;w=10; pipeCost[i][j]=w; pipeCost[j][i]=w;
        i=0;j=2;w=10; pipeCost[i][j]=w; pipeCost[j][i]=w;
        i=1;j=2;w=10; pipeCost[i][j]=w; pipeCost[j][i]=w;

        System.out.println(new BuildWell().minCost_kruskal(n, wellCost, pipeCost));
        System.out.println(new BuildWell().minCost_prim(n, wellCost, pipeCost));
    }




}
