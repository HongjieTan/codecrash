package graph_dfs_bfs.Topic_MinimumSpanningTree_MST;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.PriorityQueue;

/**
 * Created by thj on 2018/7/28.
 *
 *
 *    prefer kruskal, it has more clean code!!!!
 *
 *
 * 1. kruskal: (see MST_kruskal.java)  O(ElogE) or O(ElogV)
 *
 * 2. prim:
 *    Data Structure of Graph   Ordering	                                 Time Complexity    Note
 *      Adjacency Matrix	    Searching	                                   O(V^2)           very basic version
 *      Adjacency List	        Priority Queue with decrease key	           O(EVlogV)
 *      Adjacency List	        Binary Heap	                                   O(ElogV)         complex code, need to implement own heap
 *      Adjacency List	        Priority Queue without decrease key(improved)  O(ElogV)         best!
 */
public class MST_prim {

    public void primMST_x2(int vertices, LinkedList<Edge>[] adjList) {
        int[] cost = new int[vertices];
        int[] prev = new int[vertices];
        boolean[] inMst = new boolean[vertices];

        PriorityQueue<int[]> pq = new PriorityQueue<>((a,b)->a[1]-b[1]);

        Arrays.fill(cost, Integer.MAX_VALUE);
        cost[0]=0;
        prev[0]=-1;
        pq.add(new int[]{0, 0});

        while(!pq.isEmpty()) {
            int[] cur = pq.remove();

            if(inMst[cur[0]]) continue; // if we have pq with decrease key, we don't need to check here...
            inMst[cur[0]]=true;

            LinkedList<Edge> edges = adjList[cur[0]];
            for(Edge edge: edges) {
                int nb = edge.dst;
                if(nb==cur[0]) continue;
                if(!inMst[nb] && edge.weight < cost[nb]) { // 这里貌似可以简化，不用cost，直接往pq里扔也可以
                    cost[nb] = edge.weight;
                    prev[nb] = cur[0];
                    // pq.decreaseKey(nb, cost[nb]); // if we have pq with decrease key
                    pq.add(new int[]{nb, cost[nb]});
                }
            }
        }

        // result is stored in prev, cost
    }


    static class Edge {
        int src;
        int dst;
        int weight;
        public Edge (int src, int dst, int weight) {
            this.src = src;
            this.dst = dst;
            this.weight = weight;
        }
    }

    static class ResultNode {
        int parent;
        int weight;
    }

    static class HeapNode {
        int cost;
        int vertex;
        public HeapNode(int cost, int vertex) {this.cost=cost; this.vertex=vertex;}
    }

    /**
     *  prim: adjacent list + priority queue  O(ElogV)
     *
     *  https://algorithms.tutorialhorizon.com/prims-minimum-spanning-tree-mst-using-adjacency-list-and-priority-queue-without-decrease-key-in-oelogv/
     */
    private static void addEdge(LinkedList<Edge>[] adjList, int src, int dst, int weight) {
        Edge edge = new Edge(src, dst, weight);
        adjList[src].add(edge);

        edge = new Edge(dst, src, weight);
        adjList[dst].addFirst(edge); //for undirected graph_dfs_bfs
    }

    public void primMST(int vertices, LinkedList<Edge>[] adjList) {
        boolean[] inMst = new boolean[vertices];
        int[] cost = new int[vertices];
        ResultNode[] resultSet = new ResultNode[vertices];

        for (int i = 0; i < vertices; i++) {
            inMst[i] = false;
            cost[i] = Integer.MAX_VALUE;
            resultSet[i] = new ResultNode();
        }

        PriorityQueue<HeapNode> heap = new PriorityQueue<>((a,b)-> (a.cost-b.cost));

        // start from vertex 0
        cost[0] = 0;
        resultSet[0].parent = -1; resultSet[0].weight = 0;
        HeapNode node0 = new HeapNode(0, cost[0]);
        heap.add(node0);

        while (!heap.isEmpty()) {
            // get the min
            HeapNode min = heap.poll();

            if (inMst[min.vertex]) continue; // skip!

            int vertex = min.vertex;
            inMst[vertex] = true;

            LinkedList<Edge> adjs = adjList[vertex];
            for (int i = 0; i < adjs.size(); i++) {
                Edge edge = adjs.get(i);
                if (!inMst[edge.dst] && edge.weight < cost[edge.dst]) { // 不用cost，直接往pq里扔, 这里也可以; 如果用pqWithDecreaseKey，这里还是需要的;
                    // update cost
                    cost[edge.dst] = edge.weight;

                    // add to heap
                    heap.add(new HeapNode(edge.weight, edge.dst));

                    // update result
                    resultSet[edge.dst].parent = vertex; resultSet[edge.dst].weight = edge.weight;
                }
            }
        }

        printMST(resultSet);
    }



    /**
     *
     *  prim: adjacent matrix + searching  O(v^2), very basic version
     *
     */
//    private int getMinVertex(boolean[] inMst, int[] cost) {
//        int min = Integer.MAX_VALUE;
//        int minIndex = -1;
//        for (int i=0; i<cost.length; i++) {
//            if (!inMst[i] && cost[i]<min) {
//                min = cost[i];
//                minIndex = i;
//            }
//        }
//        return minIndex;
//
//    }
//
//    public void primMST_v1(int vertices, int[][] adjMatrix) {
//        boolean[] inMst = new boolean[vertices];
//        ResultNode[] res = new ResultNode[vertices];
//        int[] cost = new int[vertices];
//
//        // init
//        for (int i = 0; i < vertices; i++) {
//            cost[i] = Integer.MAX_VALUE;
//            res[i] = new ResultNode();
//        }
//
//        // start form vertex 0
//        cost[0] = 0;
//        res[0].parent=-1; res[0].weight=0;
//
//        for (int i = 0; i < vertices; i++) {
//
//            // get the vertex: 1.not in mst  2.with minimum cost
//            int v = getMinVertex(inMst, cost);
//
//            // put in mst
//            inMst[v] = true;
//
//            // decrease cost of adjacent vertices if necessary
//            for (int j = 0; j < vertices; j++) {
//                if (adjMatrix[v][j] > 0) {
//                    if (!inMst[j] && adjMatrix[v][j]<cost[j]) {
//                        // update cost
//                        cost[j] = adjMatrix[v][j];
//
//                        // update res set
//                        res[j].parent = v;
//                        res[j].weight = cost[j];
//
//                    }
//                }
//            }
//        }
//
//
//        printMST(res);
//
//    }
//
//    private static void addEdge(int source, int destination, int weight, int[][] adjMatrix) {
//        //add edge
//        adjMatrix[source][destination]=weight;
//
//        //add back edge for undirected graph_dfs_bfs
//        adjMatrix[destination][source]=weight;
//    }

    public void printMST(ResultNode[] resultSet){
        int total_min_weight = 0;
        System.out.println("Minimum Spanning Tree: ");
        for (int i = 1; i <resultSet.length ; i++) {
            System.out.println("Edge: " + i + " <- " + resultSet[i].parent +
                    " key: " + resultSet[i].weight);
            total_min_weight += resultSet[i].weight;
        }
        System.out.println("Total minimum key: " + total_min_weight);
    }

    /**
     *
     *
     * test case
     *
     *
     * @param args
     */

    public static void main(String[] args) {
        int vertices = 6;
//        int[][] adjMatrix = new int[vertices][vertices];
//        addEdge(0, 1, 4, adjMatrix);
//        addEdge(0, 2, 3, adjMatrix);
//        addEdge(1, 2, 1, adjMatrix);
//        addEdge(1, 3, 2, adjMatrix);
//        addEdge(2, 3, 4, adjMatrix);
//        addEdge(3, 4, 2, adjMatrix);
//        addEdge(4, 5, 6, adjMatrix);
//        new MST_prim().primMST(vertices, adjMatrix);
        LinkedList<Edge>[] adjList = new LinkedList[vertices];
        for (int i = 0; i < vertices; i++) {
            adjList[i] = new LinkedList<>();
        }
        addEdge(adjList, 0, 1, 4);
        addEdge(adjList, 0, 2, 3);
        addEdge(adjList, 1, 2, 1);
        addEdge(adjList, 1, 3, 2);
        addEdge(adjList, 2, 3, 4);
        addEdge(adjList, 3, 4, 2);
        addEdge(adjList, 4, 5, 6);
//        new MST_prim().primMST(vertices, adjList);
        new MST_prim().primMST_x2(vertices, adjList);
    }




}
