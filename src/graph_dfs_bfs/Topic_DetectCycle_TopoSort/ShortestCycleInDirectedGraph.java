package graph_dfs_bfs.Topic_DetectCycle_TopoSort;

import java.util.*;

/*
https://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=495655
https://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=507901
第三轮：
在一个有向图中给定一个节点，返回从该节点出发以该节点结尾的最短环的长度，若不存在环返回-1. From 1point 3acres bbs
followup 1: 返回最短环的路径
followup 2: 如果有很多台机器可以使用，如果并行化？
 */

/**
 * tan: 题意应该是从一个指定点出发到该点作为结尾的最短的环
 *     - BFS: 从该店出发BFS即可，遇到出发点就是最短的环
 *     - followup1：存parent...
 *     - followup2: (by lz: 言之有理即可，没有问特别细), 队列的操作多线程来做？
 */
public class ShortestCycleInDirectedGraph {

    class Node{
        int id;
        Node parent;
        public Node(int id, Node parent) {this.id=id;this.parent=parent;}
    }

    int shortestCycle(Map<Integer, List<Integer>> adjMap, int start) {
        LinkedList<Node> q = new LinkedList<>();
        q.add(new Node(0, null));
        int level=0;
        Set<Integer> visited = new HashSet<>();
        Node last = null;
        while (!q.isEmpty()) {
            int levelSize = q.size();
            for (int i = 0; i < levelSize; i++) {
                Node cur = q.remove();
                if (visited.contains(cur.id) && cur.id==start) { // found!
                    // print path
                    Node node = cur;
                    while (node!=null) {
                        System.out.printf(node.id+"<-");
                        node = node.parent;
                    }
                    System.out.println();
                    return level;
                }
                visited.add(cur.id);
                if (adjMap.containsKey(cur.id)) {
                    for(int nb: adjMap.get(cur.id)) {
                        if (!visited.contains(nb) || nb==start) q.add(new Node(nb, cur));
                    }
                }
            }
            level++;
        }

        return -1;
    }


    public static void main(String[] args) {
        Map<Integer, List<Integer>> adjMap = new HashMap<>();
        adjMap.put(0, Arrays.asList(1));
        adjMap.put(1, Arrays.asList(2,3));
        adjMap.put(2, Arrays.asList(0));
        adjMap.put(3, Arrays.asList(4));
        adjMap.put(4, Arrays.asList(0));
        int start=0;
        System.out.println(new ShortestCycleInDirectedGraph().shortestCycle(adjMap, start));
    }
}
