package graph_dfs_bfs.Topic_DetectCycle_TopoSort;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;


/**
 *
 *  - kahn's variant (use outDegree instead!)  O(N+E)   good!
 *  - dfs using 3-colors or   O(N+E)    good!
 *  - dfs using using localVisited
 *
 */
public class FindEventualSafeStates {

    // kahn, but remove 0 outDegree nodes
    public List<Integer> eventualSafeNodes_v2(int[][] graph) {
        int[] outDegree = new int[graph.length];
        boolean[] safe = new boolean[graph.length];

        List<Integer>[] rgraph = new List[graph.length]; // rgraph[i] stores i's incoming edges, use rgraph to improve searching sources here...
        for (int i = 0; i < graph.length; i++) rgraph[i] = new ArrayList<>();

        for (int i = 0; i < graph.length; i++) {
            outDegree[i] += graph[i].length;
            for (int nb: graph[i]) {
                rgraph[nb].add(i);
            }
        }

        LinkedList<Integer> que = new LinkedList<>();
        for (int i = 0; i < graph.length; i++) {
            if (outDegree[i]==0) que.add(i);
        }
        while (!que.isEmpty()) {
            int curNode = que.poll();
            safe[curNode] = true;
            for (int src: rgraph[curNode]) { // use rgraph to improve searching sources
                outDegree[src]--;
                if (outDegree[src]==0) que.add(src);
            }
        }

        List<Integer> ans = new ArrayList<>();
        for (int i = 0; i < graph.length; i++) {
            if (safe[i]) ans.add(i);
        }
        return ans;
    }

    // dfs with 3-colors!
    int WHILE=0, GRAY=1, BLACK=2;
    public List<Integer> eventualSafeNodes_dfs_colors(int[][] graph) {
        int[] colors = new int[graph.length];
        for (int i = 0; i < graph.length; i++) {
            dfs(graph, i, colors);
        }

        List<Integer> ans = new ArrayList<>();
        for (int i = 0; i < graph.length; i++) {
            if (colors[i]==BLACK) ans.add(i);
        }
        return ans;
    }

    boolean dfs(int[][] graph, int node, int[] colors) {
        if (colors[node] == GRAY) return true;
        if (colors[node] == BLACK) return false;

        colors[node] = GRAY;

        for(int nb: graph[node]) {
            if (dfs(graph, nb, colors)) return true;
        }

        colors[node] = BLACK;
        return false;
    }


    // dfs with localVisited..
    public List<Integer> eventualSafeNodes_dfs_localVisited(int[][] graph) {
        boolean[] visited = new boolean[graph.length];
        boolean[] localVisited = new boolean[graph.length];
        boolean[] hasCycle = new boolean[graph.length];

        for (int i = 0; i < graph.length; i++) {
            dfs(graph, new ArrayList<>(), i, localVisited, visited, hasCycle);
        }

        List<Integer> ans = new ArrayList<>();
        for (int i = 0; i < graph.length; i++) {
            if (!hasCycle[i]) ans.add(i);
        }
        return ans;
    }

    void dfs(int[][] graph, List<Integer> path, int node, boolean[] localVisited, boolean[] visited, boolean[] hasCycle) {
        if (localVisited[node] || hasCycle[node]) { // notice...
            for (int i: path) hasCycle[i] = true;
        }
        if (visited[node]) return;

        localVisited[node] = true;
        visited[node] = true;
        path.add(node);

        for (int nb: graph[node]) {
            dfs(graph, path, nb, localVisited, visited, hasCycle);
        }

        localVisited[node] = false;
        path.remove(path.size()-1);
    }



    public static void main(String[] args) {
        int[][] graph = new int[][]{{0},{2,3,4},{3,4},{0,4},{}};
        List<Integer> res = new FindEventualSafeStates().eventualSafeNodes_v2(graph);
//        System.out.println("result:");
        for(int i: res) System.out.println(i);
    }

}
