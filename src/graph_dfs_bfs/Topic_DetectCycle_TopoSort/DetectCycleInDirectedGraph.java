package graph_dfs_bfs.Topic_DetectCycle_TopoSort;

import java.util.LinkedList;

/**
 * Created by thj on 2018/7/28.
 *
 *  Note: detect clcye in directed graph can not use union-find, consider the case:
 *          n1->n2, n2->n3, n1->n3
 *
 *  1. dfs(or bfs):
 *
 *      dfs_v1: using a recursion stack (localVisited)
 *              https://algorithms.tutorialhorizon.com/graph-detect-cycle-in-a-directed-graph/
 *              https://www.geeksforgeeks.org/detect-cycle-in-a-graph/
 *
 *      dfs_v2: using colors (white-gray-black dfs, use 3 states)
 *              https://www.geeksforgeeks.org/detect-cycle-direct-graph-using-colors/
 *              https://algorithms.tutorialhorizon.com/graph-detect-cycle-in-a-directed-graph-using-colors/
 *
 *  2. Kahn algorithm (与bfs结构相似...):
 *      use indgrees..
 *      
 *
 *
 */
public class DetectCycleInDirectedGraph {
    // kahn (bfs)
    public static boolean hasCycleInDG_kahn(int vertices, LinkedList<Integer>[] adjList) {
        int[] inDegrees = new int[vertices];
        for (int i = 0; i < vertices; i++) {
            for (int nb: adjList[i]) {
                inDegrees[nb]++;
            }
        }
        int count=0;
        LinkedList<Integer> que = new LinkedList<>();
        for (int i = 0; i < inDegrees.length; i++) {
            if (inDegrees[i]==0) que.add(i);
        }
        while (!que.isEmpty()){
            int curNode = que.poll();
            count++;
            for(int nb: adjList[curNode]) { // 类似于松弛操作。。。
                inDegrees[nb]--;
                if (inDegrees[nb]==0) que.add(nb);
            }
        }
        return count!=vertices;
    }


    // dfs with white-gray-black colors
    static int WHITE = 0; // Vertex is not processed yet.
    static int GRAY = 1; // Vertex is being processed
    static int BLACK = 2; // Vertex and all its descendants are processed.
    public static boolean hasCycleInDG_dfs_with_colors(int vertices, LinkedList<Integer>[] adjList) {
        int[] colors = new int[vertices];
        for (int i = 0; i < vertices; i++) {
            if(dfs_colors(adjList, i, colors))
                return true;
        }
        return false;
    }

    static boolean dfs_colors(LinkedList<Integer>[] adjList, int node, int[] colors) {
        if (colors[node] == BLACK) return false;
        if (colors[node] == GRAY) return true;
        colors[node] = GRAY;
        for (int nb: adjList[node]) {
            if (dfs_colors(adjList, nb, colors))
                return true;
        }
        colors[node] = BLACK;
        return false;
    }


    // dfs with localVisited(recursion stack)
    public static boolean hasCycleInDG_dfs_with_localvisited(int vertices, LinkedList<Integer>[] adjList) {
        boolean[] visited = new boolean[vertices];
        boolean[] localVisited = new boolean[vertices]; // Notice! only directed graph need this, undirected graph no need

        for (int i = 0; i < vertices; i++) {
            if (dfs(adjList, i, localVisited, visited))
                return true;
        }
        return false;
    }

    static boolean dfs(LinkedList<Integer>[] adjList, int node, boolean[] localVisited, boolean[] visited) {
        if (localVisited[node]) return true;
        if (visited[node]) return false;
        // notice the order of above 2 lines, it matters in case n1->n1

        visited[node] = true;
        localVisited[node] = true;

        for(int nb: adjList[node]) {
            if (dfs(adjList, nb, localVisited, visited))
                return true;
        }

        localVisited[node] = false; // revert this...
        return false;
    }


    public static void main(String[] args) {
        int v=4;
        LinkedList<Integer>[] adjList = new LinkedList[v];
        for(int i=0;i<v;i++) adjList[i] = new LinkedList<>();
        adjList[1].add(0);
        adjList[1].add(2);
        adjList[2].add(0);
        adjList[0].add(1);
//        adjList[1].add(2);
//        adjList[3].add(3);
        System.out.println(hasCycleInDG_kahn(v, adjList));
        System.out.println(hasCycleInDG_dfs_with_colors(v, adjList));
        System.out.println(hasCycleInDG_dfs_with_localvisited(v, adjList));

    }

}
