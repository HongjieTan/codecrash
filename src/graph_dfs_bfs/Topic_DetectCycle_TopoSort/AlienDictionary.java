package graph_dfs_bfs.Topic_DetectCycle_TopoSort;

import java.util.*;

public class AlienDictionary {

    public String alienOrder(String[] words) {
        Map<Character, List<Character>> adjMap = new HashMap<>();
        for (int i = 0; i < words.length - 1; i++) buildEdge(adjMap, words[i], words[i+1]);

        Map<Character, Integer> inDegreeMap = new HashMap<>();
        for (String word: words) {
            for (char c: word.toCharArray()) {
                inDegreeMap.put(c, 0);
            }
        }
        for (char start: adjMap.keySet()) {
            for (char end: adjMap.get(start)) {
                inDegreeMap.put(end, inDegreeMap.get(end)+1);
            }
        }

        // return the smallest order in lexicographical order, so to use PriorityQueue here!!
        PriorityQueue<Character> zeroInDegreeNodes = new PriorityQueue<>();
        for (char node: inDegreeMap.keySet()) {
            if (inDegreeMap.get(node)==0) zeroInDegreeNodes.add(node);
        }
        StringBuffer sb = new StringBuffer();
        while (!zeroInDegreeNodes.isEmpty()) {
            char curNode = zeroInDegreeNodes.poll();
            sb.append(curNode);
            if (adjMap.containsKey(curNode)) {
                for (char end: adjMap.get(curNode)) {
                    inDegreeMap.put(end, inDegreeMap.get(end)-1);
                    if (inDegreeMap.get(end)==0) zeroInDegreeNodes.add(end);
                }
            }

        }

        return sb.length()==inDegreeMap.size()?sb.toString():"";
    }

    private void buildEdge(Map<Character, List<Character>> adjMap, String word1, String word2) {
        int idx=0;
        while (idx<word1.length() && idx<word2.length()) {
            if(word1.charAt(idx)!=word2.charAt(idx)) {
                if (!adjMap.containsKey(word1.charAt(idx))) adjMap.put(word1.charAt(idx), new ArrayList<>());
                adjMap.get(word1.charAt(idx)).add(word2.charAt(idx));
                return;
            }
            idx++;
        }
    }


//    public String alienOrder_x2(String[] words) {
//        Map<Character, List<Character>> adjMap = new HashMap<>();
//        Map<Character, Integer> inDegree = new HashMap<>();
//        for(String w: words) {
//            for(char c: w.toCharArray()) {
//                inDegree.put(c, 0);
//            }
//        }
//
//        // build graph...
//        for(int i=0; i<words.length-1; i++) {
//               String w1=words[i], w2=words[i+1];
//               int p=0;
//               while(p<w1.length() && p<w2.length()) {
//                    if(w1.charAt(p)!=w2.charAt(p)) {
//                        if(!adjMap.containsKey(w1.charAt(p))) adjMap.put(w1.charAt(p), new ArrayList<>());
//                        adjMap.get(w1.charAt(p)).add(w2.charAt(p));
//                        inDegree.put(w2.charAt(p), inDegree.get(w2.charAt(p))+1);
//                        break;
//                    }
//                    p++;
//               }
//        }
//
//        PriorityQueue<Character> que = new PriorityQueue<>();
//        for(char c: inDegree.keySet()) {
//            if(inDegree.get(c)==0) que.add(c);
//        }
//
//        StringBuffer sb = new StringBuffer();
//        while(!que.isEmpty()) {
//            char cur = que.remove();
//            sb.append(cur);
//            for(char nb: adjMap.getOrDefault(cur, new ArrayList<>())) {
//                inDegree.put(nb, inDegree.get(nb)-1);
//                if(inDegree.get(nb)==0) que.add(nb);
//            }
//        }
//        return sb.length()!=inDegree.size()?"":sb.toString();
//    }
//
//    public String alienOrder_v2(String[] words) {
//        boolean[][] edge = new boolean[26][26];
//        for (int i = 0; i < words.length-1; i++) buildEdge(edge, words[i], words[i+1]);
//
//        Set<Character> charSet = new HashSet<>();
//        for (String word: words) for (char c: word.toCharArray()) charSet.add(c);
//
//        int[] inDegrees = new int[26];
//        Arrays.fill(inDegrees, 0);
//        for (int i = 0; i < 26; i++) {
//            for (int j = 0; j < 26; j++) {
//                if (edge[i][j]) inDegrees[j]++;
//            }
//        }
//        PriorityQueue<Integer> startsQue = new PriorityQueue<>();
//        for (int i = 0; i < 26; i++) {
//            if (inDegrees[i]==0 && charSet.contains((char)('a'+i))) startsQue.add(i);
//        }
//
//        StringBuffer sb = new StringBuffer();
//        while (!startsQue.isEmpty()) {
//            int node = startsQue.poll();
//            sb.append((char)('a'+node));
//            for (int j = 0; j < 26; j++) {
//                if (edge[node][j]) {
//                    inDegrees[j]--;
//                    if (inDegrees[j]==0 && charSet.contains((char)('a'+j))) startsQue.add(j);
//                }
//            }
//        }
//        return sb.length()==charSet.size()?sb.toString():"";
//    }
//
//    private void buildEdge(boolean[][] edge, String word1, String word2) {
//        int idx=0;
//        while (idx<word1.length() && idx<word2.length()) {
//            if(word1.charAt(idx)!=word2.charAt(idx)) {
//                edge[word1.charAt(idx)-'a'][word2.charAt(idx)-'a'] = true;
//                return;
//            }
//            idx++;
//        }
//    }

    public static void main(String[] args) {
        String[] words = new String[]{"ze","yf","xd","wd","vd","ua","tt","sz","rd", "qd","pz","op","nw","mt","ln","ko","jm","il", "ho","gk","fa","ed","dg","ct","bb","ba"};
//        System.out.println(new AlienDictionary().alienOrder(words));
//        System.out.println(new AlienDictionary().alienOrder_v2(words));
//        System.out.println(new AlienDictionary().alienOrder_x2(words));

    }

}


/*
Description
There is a new alien language which uses the latin alphabet. However, the order among letters are unknown to you.
You receive a list of non-empty words from the dictionary, where words are sorted lexicographically by the rules of this new language.
Derive the order of letters in this language.

You may assume all letters are in lowercase.
You may assume that if a is a prefix of b, then a must appear before b in the given dictionary.
If the order is invalid, return an empty string.
There may be multiple valid order of letters, return the smallest in lexicographical order

Example
Given the following words in dictionary,

[
  "wrt",
  "wrf",
  "er",
  "ett",
  "rftt"
]
The correct order is: "wertf"

Given the following words in dictionary,

[
  "z",
  "x"
]
The correct order is: "zx".
*/