package graph_dfs_bfs.Topic_DetectCycle_TopoSort;

import java.util.LinkedList;

/**
 * Created by thj on 2018/7/26.
 */
public class CourseScheduleII {


    // kahn(bfs)
    public int[] findOrder(int numCourses, int[][] prerequisites) {

        boolean[][] edge = new boolean[numCourses][numCourses];
        int[] indegrees = new int[numCourses];
        for (int i = 0; i < prerequisites.length; i++) {
            int start = prerequisites[i][1];
            int end = prerequisites[i][0];
            edge[start][end] = true;
            indegrees[end]++;
        }


        int[] res = new int[numCourses];
        int index=0;

        LinkedList<Integer> que = new LinkedList<>();
        for (int i = 0; i < indegrees.length; i++) {
            if (indegrees[i]==0) que.add(i);
        }

        while (!que.isEmpty()) {
            int node = que.poll();
            res[index++] = node;

            for (int j=0; j<edge[node].length; j++) {
                if (edge[node][j]) {
                    indegrees[j]--;
                    if (indegrees[j]==0) que.add(j);
                }
            }
        }


        return index==numCourses?res:new int[]{};
    }

    // dfs version?
//    public int[] findOrder_v2(int numCourses, int[][] prerequisites) {
//
//    }
}
