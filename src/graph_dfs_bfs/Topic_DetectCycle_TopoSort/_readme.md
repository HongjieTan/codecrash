Detect Cycle In Directed Graph:
1. dfs (with 3 colors or dfs with localVisited)
2. kahn algorithm


Detect Cycle In UnDirected Graph:
1. union-find
2. dfs/bfs


Topological Sorting for Directed Graph:
1. kahn
2. dfs (use a recursion stack)



=====================================================================

#### 以下伪代码有点乱，还是看实际代码比较清楚。。。
#### Kahn's algorithm
'''
L ← Empty list that will contain the sorted elements
S ← Set of all nodes with no incoming edge
while S is non-empty do
    remove a node n from S
    add n to tail of L
    for each node m with an edge e from n to m do
        remove edge e from the graph
        if m has no other incoming edges then
            insert m into S
if graph has edges then
    return error   (graph has at least one cycle)
else
    return L   (a topologically sorted order)
'''

#### DFS( white-gray-black dfs, need 3 states to detect cycle!!)  (or use localVisited...)
'''
L ← Empty list that will contain the sorted nodes
while there are unmarked nodes do
    select an unmarked node n
    visit(n)

function visit(node n)
    if n has a permanent mark then return
    if n has a temporary mark then stop   (not a DAG)
    mark n temporarily
    for each node m with an edge from n to m do
        visit(m)
    mark n permanently
    add n to head of L
'''