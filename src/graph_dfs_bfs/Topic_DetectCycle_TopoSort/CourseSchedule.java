package graph_dfs_bfs.Topic_DetectCycle_TopoSort;

import java.util.*;

/**
 * Created by thj on 2018/7/27.
 *
 * topological sort!!
 *
 */
public class CourseSchedule {

    public boolean canFinish_x2(int numCourses, int[][] prerequisites) {
        int n=numCourses;
        Map<Integer, List<Integer>> adjMap = new HashMap<>();
        int[] indegree = new int[n];
        for(int[] p: prerequisites) {
            int src = p[1], dst = p[0];
            if(!adjMap.containsKey(src)) adjMap.put(src, new ArrayList<>());
            adjMap.get(src).add(dst);
            indegree[dst]++;
        }

        LinkedList<Integer> q = new LinkedList<>();
        for(int i=0;i<n;i++) {
            if(indegree[i]==0) q.add(i);
        }

        int count=0;
        while(!q.isEmpty()) {
            int cur = q.remove();
            count++;
            for(int nb: adjMap.getOrDefault(cur, Arrays.asList())) {
                indegree[nb]--;
                if(indegree[nb]==0) q.add(nb);
            }
        }
        return count==n;
    }

    // kahn (bfs )
//    public boolean canFinish(int numCourses, int[][] prerequisites) {
//
//        boolean[][] edge = new boolean[numCourses][numCourses];
//        int[] inDegrees = new int[numCourses];
//        for (int i = 0; i < prerequisites.length; i++) {
//            edge[prerequisites[i][1]][prerequisites[i][0]] = true;
//            inDegrees[prerequisites[i][0]]++;
//        }
//
//        LinkedList<Integer> zeroIndegreeNodes = new LinkedList<>();
//        for (int i = 0; i < inDegrees.length; i++) {
//            if (inDegrees[i]==0) zeroIndegreeNodes.add(i);
//        }
//
//        int count=0;
//        while (!zeroIndegreeNodes.isEmpty()) {
//            int node = zeroIndegreeNodes.poll();
//            count++;
//
//            for (int j = 0; j < edge[node].length; j++) {
//                if (edge[node][j]) {
//                    inDegrees[j]--;
//                    if (inDegrees[j]==0) {
//                        zeroIndegreeNodes.add(j);
//                    }
//                }
//            }
//        }
//
//        return numCourses==count;
//    }

    // dfs version?
}
