package graph_dfs_bfs.Topic_DetectCycle_TopoSort;

import java.util.ArrayList;
import java.util.List;


/**
 *
 *  backtrack + toposort(kahn)
 *
 */
public class ListAllTopologicalSorts {

    public List<List<Integer>> allTopoSorts(int V, List<Integer>[] adjList) {
        List<List<Integer>> ans = new ArrayList<>();
        boolean[] visited = new boolean[V];
        int[] inDegree = new int[V];
        for (int i = 0; i < V; i++)
            for(int nb: adjList[i])
                inDegree[nb]++;
        helper(ans, new ArrayList<>(), adjList, V, visited, inDegree);
        return ans;
    }

    void helper(List<List<Integer>> ans, List<Integer> path, List<Integer>[] adjList, int V, boolean[] visited, int[] inDegree) {
        if (path.size() == V) {
            ans.add(new ArrayList<>(path));
            return;
        }
        for (int i = 0; i < V; i++) {
            if (!visited[i] && inDegree[i]==0) {
                visited[i] = true;
                for(int nb: adjList[i]) inDegree[nb]--;
                path.add(i);

                helper(ans, path, adjList, V, visited, inDegree);

                // backtrack!!
                visited[i] = false;
                for(int nb: adjList[i]) inDegree[nb]++;
                path.remove(path.size()-1);
            }
        }
    }




    public static void main(String[] args) {
        int V=6;
        List<Integer>[] adjList = new List[V];
        for (int i = 0; i < V; i++) adjList[i]=new ArrayList<>();
        adjList[2].add(3);
        adjList[3].add(1);
        adjList[4].add(0);
        adjList[4].add(1);
        adjList[5].add(0);
        adjList[5].add(2);


        List<List<Integer>> ans = new ListAllTopologicalSorts().allTopoSorts(V, adjList);
        for (List<Integer> path: ans) {
            for(int v: path) System.out.printf(v+"");
            System.out.println();
        }

    }
}
