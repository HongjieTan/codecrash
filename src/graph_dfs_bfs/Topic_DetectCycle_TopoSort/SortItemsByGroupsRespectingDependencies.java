// There are n items each belonging to zero or one of m groups where group[i] is the group that the i-th item belongs
// to and it's equal to -1 if the i-th item belongs to no group. The items and the groups are zero indexed.
// A group can have no item belonging to it.
//
// Return a sorted list of the items such that:
//
//
// The items that belong to the same group are next to each other in the sorted list.
// There are some relations between these items where beforeItems[i] is a list containing all the items that should come before the i-th item in the sorted array (to the left of the i-th item).
//
//
// Return any solution if there is more than one solution and return an empty list if there is no solution.
//
//
// Example 1:
//
//
//
//
//Input: n = 8, m = 2, group = [-1,-1,1,0,0,1,0,-1], beforeItems = [[],[6],[5],[6],[3,6],[],[],[]]
//Output: [6,3,4,1,5,2,0,7]
//
//
// Example 2:
//
//
//Input: n = 8, m = 2, group = [-1,-1,1,0,0,1,0,-1], beforeItems = [[],[6],[5],[6],[3],[],[4],[]]
//Output: []
//Explanation: This is the same as example 1 except that 4 needs to be before 6 in the sorted list.
//
//
//
// Constraints:
//
//
// 1 <= m <= n <= 3*10^4
// group.length == beforeItems.length == n
// -1 <= group[i] <= m-1
// 0 <= beforeItems[i].length <= n-1
// 0 <= beforeItems[i][j] <= n-1
// i != beforeItems[i][j]
// beforeItems[i] does not contain duplicates elements.
//
//

package graph_dfs_bfs.Topic_DetectCycle_TopoSort;

import java.util.*;

public class SortItemsByGroupsRespectingDependencies {
    public int[] sortItems(int n, int m, int[] group, List<List<Integer>> beforeItems) {
        for (int i=0;i<n;i++){
            if (group[i]==-1)
                group[i] = m++;
        }
        Map<Integer, Set<Integer>> group_g = new HashMap<>();
        Map<Integer, Set<Integer>> group_items = new HashMap<>();

        for (int i = 0; i < m; i++) group_g.put(i, new HashSet<>());
        for (int i = 0; i < n; i++) {
            for (int src : beforeItems.get(i)) {
                if (group[src] != group[i]) group_g.get(group[src]).add(group[i]);
            }
        }
        for (int i = 0; i < m; i++) group_items.put(i, new HashSet<>());
        for (int i = 0; i < n; i++) group_items.get(group[i]).add(i);

        List<Integer> group_sorted = topoSort(group_g);
        if (group_sorted == null) return new int[0];

        int[] ret = new int[n];
        int p = 0;
        for (int k : group_sorted) {
            Map<Integer, Set<Integer>> item_g = new HashMap<>();
            for (int dst : group_items.get(k)) item_g.put(dst, new HashSet<>());
            for (int dst : group_items.get(k)) {
                for (int src : beforeItems.get(dst)) {
                    if (item_g.containsKey(src)) item_g.get(src).add(dst);
                }
            }

            List<Integer> items_sorted = topoSort(item_g);
            if (items_sorted == null) return new int[0];
            for (int item : items_sorted) ret[p++] = item;
        }
        return ret;
    }

    List<Integer> topoSort(Map<Integer, Set<Integer>> g) {
        Map<Integer, Integer> indegree = new HashMap<>();
        Queue<Integer> q = new LinkedList<>();

        for (int src : g.keySet()) indegree.put(src,0);
        for (int src : g.keySet()) {
            for (int dst : g.get(src)) {
                indegree.put(dst, indegree.get(dst) + 1);
            }
        }
        for (int item : indegree.keySet()) {
            if (indegree.get(item) == 0) q.add(item);
        }

        List<Integer> ret = new ArrayList<>();
        while (!q.isEmpty()) {
            int cur = q.remove();
            ret.add(cur);
            for (int dst : g.get(cur)) {
                indegree.put(dst, indegree.get(dst) - 1);
                if (indegree.get(dst) == 0) q.add(dst);
            }
        }
        return ret.size() == g.size() ? ret:null;
    }

    public static void main(String[] as) {
        int n=5,m=5;
        int[] group = {2,0,-1,3,0};
        List<List<Integer>> beforeItems = Arrays.asList(
                Arrays.asList(2,1,3),
                Arrays.asList(2,4),
                Arrays.asList(),
                Arrays.asList(),
                Arrays.asList()
        );
        int[] res = new SortItemsByGroupsRespectingDependencies().sortItems(n,m,group, beforeItems);
        for (int x:res) System.out.println(x);

    }
}


/*
by uwi:
class Solution {
    public int[] sortItems(int n, int m, int[] group, List<List<Integer>> beforeItems) {
        for(int i = 0;i < n;i++){
            if(group[i] == -1){
                group[i] = m++;
            }
        }
        int u = 0;
        for(List<Integer> row : beforeItems)u += row.size();
        int[] from = new int[u];
        int[] to = new int[u];
        int p = 0;
        for(int i = 0;i < beforeItems.size();i++){
            for(int x : beforeItems.get(i)){
                if(group[x] != group[i]){
                    from[p] = group[x];
                    to[p] = group[i];
                    p++;
                }
            }
        }
        int[][] cg = packD(m, from, to, p);
        int[] cord = sortTopologically(cg);
        if(cord == null)return new int[0];

        int[] from2 = new int[u];
        int[] to2 = new int[u];
        p = 0;
        for(int i = 0;i < beforeItems.size();i++){
            for(int x : beforeItems.get(i)){
                if(group[x] == group[i]){
                    from2[p] = x;
                    to2[p] = i;
                    p++;
                }
            }
        }
        int[][] g = packD(n, from2, to2, p);
        int[] ord = sortTopologically(g);
        if(ord == null)return new int[0];
        int[] iord = new int[n];
        for(int i = 0;i < n;i++)iord[ord[i]] = i;
        final int[] io = iord;

        List<List<Integer>> all = new ArrayList<>();
        for(int i = 0;i < m;i++)all.add(new ArrayList<Integer>());
        for(int i = 0;i < n;i++)all.get(group[i]).add(i);

        int[] ret = new int[n];
        p = 0;
        for(int i = 0;i < m;i++){
            Collections.sort(all.get(cord[i]), new Comparator<Integer>() {
                public int compare(Integer a, Integer b) {
                    return io[a] - io[b];
                }
            });
            for(int t : all.get(cord[i])){
                ret[p++] = t;
            }
        }
        return ret;
    }

    public int[] sortTopologically(int[][] g)
    {
        int n = g.length;
        int[] ec = new int[n];
        for(int i = 0;i < n;i++){
            for(int to : g[i])ec[to]++;
        }
        int[] ret = new int[n];
        int q = 0;

        // sources
        for(int i = 0;i < n;i++){
            if(ec[i] == 0)ret[q++] = i;
        }

        for(int p = 0;p < q;p++){
            for(int to : g[ret[p]]){
                if(--ec[to] == 0)ret[q++] = to;
            }
        }
        // loop
        for(int i = 0;i < n;i++){
            if(ec[i] > 0)return null;
        }
        return ret;
    }


    public int[][] packD(int n, int[] from, int[] to){ return packD(n, from, to, from.length);}
    public int[][] packD(int n, int[] from, int[] to, int sup)
    {
        int[][] g = new int[n][];
        int[] p = new int[n];
        for(int i = 0;i < sup;i++)p[from[i]]++;
        for(int i = 0;i < n;i++)g[i] = new int[p[i]];
        for(int i = 0;i < sup;i++){
            g[from[i]][--p[from[i]]] = to[i];
        }
        return g;
    }

}


top post:
We do a two-level topological sort according to beforeItems.

Group level. Decide which group should go first.
Inner-group level. Decide which item should go first.
Isolated items (-1) can be put into separate groups to make the solution simpler.

class Graph {
    unordered_map<int, vector<int>> adj;
    unordered_map<int, int> indegree;
public:
    Graph() {}

    Graph(int n) {
        adj = unordered_map<int, vector<int>>{};
        indegree = unordered_map<int, int>{};
        for (int i = 0; i < n; ++i) {
            indegree[i] = 0;
            adj[i] = vector<int>{};
        }
    }

    Graph(vector<int> &vec) {
        adj = unordered_map<int, vector<int>>{};
        indegree = unordered_map<int, int>{};
        for (const int &i : vec) {
            indegree[i] = 0;
            adj[i] = vector<int>{};
        }
    }

    void addEdge(int from, int to) {
        adj[from].push_back(to);
        indegree[to]++;
    }

    vector<int> sort() {
        queue<int> q;
        vector<int> ans;
        for (const auto &p : indegree) {
            if (p.second == 0) q.push(p.first);
        }

        while (!q.empty()) {
            int h = q.front();
            q.pop();
            ans.push_back(h);
            for (const auto &i : adj[h]) {
                indegree[i]--;
                if (indegree[i] == 0) q.push(i);
            }
        }

        return ans;
    }
};

class Solution {
public:
    vector<int> sortItems(int n, int m, vector<int>& group, vector<vector<int>>& beforeItems) {
        vector<vector<int>> groupItems(m, vector<int>{});
        for (int i = 0; i < n; ++i) {
            if (group[i] >= 0) groupItems[group[i]].push_back(i);
            else {
                // Isolated items are put into separate groups.
                group[i] = groupItems.size();
                groupItems.push_back(vector<int>{i});
            }
        }

        Graph groupGraph = Graph(groupItems.size());
        vector<Graph> groupItemGraphs(groupItems.size());
        for (int i = 0; i < groupItems.size(); ++i) {
            groupItemGraphs[i] = Graph(groupItems[i]);
        }
        for (int i = 0; i < n; ++i) {
            for (const int &item : beforeItems[i]) {
                int gi = group[i];
                if (gi == group[item]) {
                    // BeforeItem is in the same group, add edge in the graph of that group.
                    groupItemGraphs[gi].addEdge(item, i);
                } else {
                    // BeforeItem is in a different group, add edge in the graph of groups.
                    groupGraph.addEdge(group[item], gi);
                }
            }
        }

        vector<int> groupOrder = groupGraph.sort();
        vector<int> ans;
        if (groupOrder.size() < groupItems.size()) return ans;
        for (const int &i : groupOrder) {
            vector<int> order = groupItemGraphs[i].sort();
            if (order.size() < groupItems[i].size()) return vector<int>{};
            else for(const int &j : order) ans.push_back(j);
        }
        return ans;
    }
};

top java post:
public int[] sortItems(int n, int m, int[] group, List<List<Integer>> beforeItems) {

    // Item dependency graph creation.
    Map<Integer,Set<Integer>> itemGraph = new HashMap<>();
    Map<Integer,Integer> itemInDegree = new HashMap<>();

    for (int i=0;i<n;i++) {
        itemGraph.putIfAbsent(i,new HashSet<>());
    }

    // Group dependency graph creation
    Map<Integer,Set<Integer>> groupGraph = new HashMap<>();
    Map<Integer,Integer> groupInDegree = new HashMap<>();

    for (int g : group) {
        groupGraph.putIfAbsent(g,new HashSet<>());
    }

    for (int i=0;i<beforeItems.size();i++) {
        List<Integer> list = beforeItems.get(i);
        if(list.size()!=0) {
            for (int val : list) {
                itemGraph.get(val).add(i);
                itemInDegree.put(i,itemInDegree.getOrDefault(i,0)+1);
                // If an item(i1) is dependent on another(i2) then its group(g1) should also be dependent on (g2)
                int g1 = group[val];
                int g2 = group[i];
                if (g1 != g2 && groupGraph.get(g1).add(g2)) {
                    groupInDegree.put(g2,groupInDegree.getOrDefault(g2,0)+1);
                }
            }
        }
    }

    List<Integer> itemOrdering = topologicalSort(itemGraph, itemInDegree, n);
    List<Integer> groupOrdering = topologicalSort(groupGraph, groupInDegree, groupGraph.size());

    // In case we find a cycle.
    if(itemOrdering.size()==0 || groupOrdering.size()==0) {
        return new int[0];
    }

    Map<Integer,List<Integer>> map = new HashMap<>();

    // Put items in respective buckets.
    for (int item : itemOrdering) {
        int g = group[item];
        map.putIfAbsent(g,new ArrayList<>());
        map.get(g).add(item);
    }

    int[] result = new int[n];
    int i=0;

    // Get result, by looping over group ordering.
    for (int g : groupOrdering) {
        List<Integer> list = map.get(g);
        for (int item : list) {
            result[i] = item;
            i++;
        }
    }

    return result;

}

// Kahn’s algorithm
private List<Integer> topologicalSort(Map<Integer,Set<Integer>> graph,
                                      Map<Integer,Integer> inDegree,int count) {

    List<Integer> result = new ArrayList<>();
    Queue<Integer> queue = new LinkedList<>();
    for (int key : graph.keySet()) {
        if(inDegree.getOrDefault(key,0)==0) {
            queue.add(key);
        }
    }

    while (!queue.isEmpty()) {
        int pop = queue.poll();
        count--;
        result.add(pop);
        for (int next : graph.get(pop)) {
            int val = inDegree.get(next);
            inDegree.put(next,val-1);
            if(inDegree.get(next) ==0) {
                queue.add(next);
            }
        }
    }
    return count==0 ? result : new ArrayList<>();
}
*/