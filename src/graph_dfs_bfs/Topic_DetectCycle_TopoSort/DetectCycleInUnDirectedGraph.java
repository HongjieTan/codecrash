package graph_dfs_bfs.Topic_DetectCycle_TopoSort;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by thj on 2018/7/28.
 *
 * 1. Disjoint-Set data structure and Union-Find algorithm: best choice!
 *      https://algorithms.tutorialhorizon.com/disjoint-set-data-structure-union-find-algorithm/
 *      https://algorithms.tutorialhorizon.com/graph-find-cycle-in-undirected-graph-using-disjoint-set-union-find/
 *
 * 2. DFS/BFS:
 *      https://algorithms.tutorialhorizon.com/graph-detect-cycle-in-undirected-graph-using-dfs/
 *
 */
public class DetectCycleInUnDirectedGraph {

    // union find
    static class Edge {
        int src;
        int dst;
        public Edge(int src, int dst) {this.src=src; this.dst=dst;}
    }

    private static int find(int[] parent, int v) {
        if(parent[v]==v) return v;
        return find(parent, parent[v]);
    }

    private static void union(int[] parent, int x, int y) {
        int x_set_parent = find(parent, x);
        int y_set_parent = find(parent, y);
        //make x as parent of y
        parent[y_set_parent] = x_set_parent;
    }

    public static boolean hasCycle_DisjointSet(int vertices, List<Edge> edgeList) {
        int[] parent = new int[vertices];

        // make sets
        for (int i = 0; i < vertices; i++) {
            parent[i]=i;
        }

        // iterate through all the edges and keep making the sets
        for (int i = 0; i < edgeList.size(); i++) {
            Edge edge = edgeList.get(i);
            int x_set = find(parent, edge.src);
            int y_set = find(parent, edge.dst);
            if (x_set==y_set) {
                return true; // cycle detected
            } else {
                union(parent, x_set, y_set);
            }
        }

        return false;

    }


    // dfs version x2
    public static boolean hasCycle_dfs(int vertices, LinkedList<Integer>[] adjList) {
        boolean[] visited = new boolean[vertices];
        for (int i = 0; i < vertices; i++) {
            if (!visited[i] && dfs(adjList, i, visited, -1)) // when start node is unvisited...
                return true;
        }
        return false;
    }

    static boolean dfs(LinkedList<Integer>[] adjList, int node, boolean[] visited, int parent) {
        if (visited[node]) return true;
        visited[node] = true;
        for (int nb: adjList[node]) {
            if (nb!=parent && dfs(adjList, nb, visited, node)) // skip the direct parent!! notice!
                return true;
        }
        return false;
    }


    // dfs version
//    public static boolean hasCycle_dfs(int vertices, LinkedList<Integer>[] adjList) {
//        boolean[] visited = new boolean[vertices];
//        for (int i = 0; i < vertices; i++) {
//            if (hasCycleUtil_dfs(i, adjList, visited, -1)) return true;
//        }
//        return false;
//    }
//
//    private static boolean hasCycleUtil_dfs(int currVertex, LinkedList<Integer>[] adjList, boolean[] visited, int parent) {
//
//        if (visited[currVertex]) return false;
//        visited[currVertex] = true;
//
//        for (int i=0; i<adjList[currVertex].size(); i++) {
//            int adj = adjList[currVertex].get(i);
//            if (adj!=parent ) { // skip the direct parent!! notice!
//                if (visited[adj]) return true;
//                if (hasCycleUtil_dfs(adj, adjList, visited, currVertex)) return true;
//            }
//        }
//        return false;
//    }

    private static void addEdge(int v1, int v2, LinkedList<Integer>[] edges) {
        edges[v1].add(v2);
        edges[v2].add(v1);
    }

    public static void main(String[] args) {

        int vertices = 4;
        LinkedList<Integer>[] edges = new LinkedList[vertices];
        for (int i = 0; i < vertices; i++) {
            edges[i] = new LinkedList<>();
        }
        addEdge(0, 1, edges);
        addEdge(1, 2, edges);
        addEdge(3, 0, edges);
        addEdge(2, 0, edges);
        System.out.println(hasCycle_dfs(vertices, edges));

//        int vertices = 4;
//        List<Edge> edgeList = new ArrayList<>();
//        edgeList.add(new Edge(0,1));
//        edgeList.add(new Edge(1,2));
//        edgeList.add(new Edge(2,0));
//        edgeList.add(new Edge(2,3));
//        System.out.println(hasCycle_DisjointSet(vertices, edgeList));


    }


}


/*
// A Java Program to detect cycle in a graph
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

class Graph {

    private final int V;
    private final List<List<Integer>> adj;

    public Graph(int V)
    {
        this.V = V;
        adj = new ArrayList<>(V);

        for (int i = 0; i < V; i++)
            adj.add(new LinkedList<>());
    }

    // This function is a variation of DFSUytil() in
    // https://www.geeksforgeeks.org/archives/18212
    private boolean isCyclicUtil(int i, boolean[] visited,
                                      boolean[] recStack)
    {

        // Mark the current node as visited and
        // part of recursion stack
        if (recStack[i])
            return true;

        if (visited[i])
            return false;

        visited[i] = true;

        recStack[i] = true;
        List<Integer> children = adj.get(i);

        for (Integer c: children)
            if (isCyclicUtil(c, visited, recStack))
                return true;

        recStack[i] = false;

        return false;
    }

    private void addEdge(int source, int dest) {
        adj.get(source).add(dest);
    }

    // Returns true if the graph contains a
    // cycle, else false.
    // This function is a variation of DFS() in
    // https://www.geeksforgeeks.org/archives/18212
    private boolean isCyclic()
    {

        // Mark all the vertices as not visited and
        // not part of recursion stack
        boolean[] visited = new boolean[V];
        boolean[] recStack = new boolean[V];


        // Call the recursive helper function to
        // detect cycle in different DFS trees
        for (int i = 0; i < V; i++)
            if (isCyclicUtil(i, visited, recStack))
                return true;

        return false;
    }

    // Driver code
    public static void main(String[] args)
    {
        Graph graph = new Graph(4);
        graph.addEdge(0, 1);
        graph.addEdge(0, 2);
        graph.addEdge(1, 2);
        graph.addEdge(2, 0);
        graph.addEdge(2, 3);
        graph.addEdge(3, 3);

        if(graph.isCyclic())
            System.out.println("Graph contains cycle");
        else
            System.out.println("Graph doesn't "
                                    + "contain cycle");
    }
}

// This code is contributed by Sagar Shah.
*/