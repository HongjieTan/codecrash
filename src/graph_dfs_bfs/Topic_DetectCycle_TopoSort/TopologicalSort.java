package graph_dfs_bfs.Topic_DetectCycle_TopoSort;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

/**
 *
 *  1. Kahn
 *
 *  2. dfs: need a recursion stack or 3-colors
 *
 *
 */
public class TopologicalSort {

    // kahn
    public List<Integer> topologicalSort_kahn(int V, LinkedList<Integer>[] adjList) {
        int[] inDegrees = new int[V];
        for (int i = 0; i < V; i++) {
            for(int nb: adjList[i]) inDegrees[nb]++;
        }

        List<Integer> ans = new ArrayList<>();
        LinkedList<Integer> que = new LinkedList<>();
        for (int i = 0; i < V; i++) {
            if (inDegrees[i]==0) que.add(i);
        }
        while (!que.isEmpty()) {
            int curNode = que.poll();
            ans.add(curNode);
            for (int nb: adjList[curNode]) {
                inDegrees[nb]--;
                if (inDegrees[nb]==0) que.add(nb);
            }
        }

        if (ans.size()!=V)
           //  throw new Exception("there is cycle...");
            System.out.println("there is cycle...");

        return ans;
    }

    // dfs version
//    public List<Integer> topologicalSort_dfs(int V, LinkedList<Integer>[] adjList) {
//        boolean[] visited = new boolean[V];
//        Stack<Integer> stack = new Stack<>();
//
//        for (int i = 0; i < V; i++) {
//            dfs(adjList, i, visited, stack);  // 感觉不对。。。应该是从一个零入度点开始dfs???
//        }
//
//        List<Integer> ans = new ArrayList<>();
//        while (!stack.isEmpty()) ans.add(stack.pop());
//        return ans;
//    }
//
//    void dfs(LinkedList<Integer>[] adjList, int node, boolean[] visited, Stack<Integer> stack) {
//        if (visited[node]) return;
//        visited[node]=true;
//
//        for (int nb: adjList[node]) {
//            dfs(adjList, nb, visited, stack);
//        }
//        stack.push(node); //A vertex is pushed to stack when all of its adjacent vertices (and their adjacent vertices and so on) are already in stack.
//    }




    public static void main(String[] args) {
        int V=6;
        LinkedList<Integer>[] adjList = new LinkedList[V];
        for (int i = 0; i < V; i++) adjList[i] = new LinkedList<>();
        adjList[2].add(3);
        adjList[3].add(1);
        adjList[4].add(0);
        adjList[4].add(1);
        adjList[5].add(0);
        adjList[5].add(2);

//        List<Integer> ans = new TopologicalSort().topologicalSort_dfs(V, adjList);
        List<Integer> ans = new TopologicalSort().topologicalSort_kahn(V, adjList);
        for (int x: ans )  System.out.println(x);
    }
}
