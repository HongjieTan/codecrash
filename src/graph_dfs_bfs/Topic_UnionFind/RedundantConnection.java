package graph_dfs_bfs.Topic_UnionFind;


// union-find with path compression and union by rank: O(N)
public class RedundantConnection {

    int[] parents;
    int[] ranks;

    private void makeset(int size) {
        parents = new int[size+1];
        ranks = new int[size+1];
        for (int i = 0; i < parents.length; i++) {
            parents[i] = i;
            ranks[i] = 1;
        }
    }

    private int find(int i) {
        if (i!=parents[i]) {
            parents[i] = find(parents[i]);
        }
        return parents[i];
    }

    private boolean union(int x, int y) {
        int rootx = find(x);
        int rooty = find(y);
        if (rootx==rooty) return false;
        if (ranks[rootx] > ranks[rooty]) {
            parents[rooty] = rootx;
        } else if (ranks[rootx] < ranks[rooty]) {
            parents[rootx] = rooty;
        } else {
            parents[rooty] = rootx;
            ranks[rootx]++;
        }
        return true;
    }

    public int[] findRedundantConnection(int[][] edges) {
        makeset(edges.length);
        for (int[] edge: edges) {
            if (!union(edge[0], edge[1])) return edge;
        }
        return null;
    }
}
