package graph_dfs_bfs.Topic_UnionFind;

//        Given a list accounts, each element accounts[i] is a list of strings, where the first element accounts[i][0] is a name, and the rest of the elements are emails representing emails of the account.
//
//        Now, we would like to merge these accounts. Two accounts definitely belong to the same person if there is some email that is common to both accounts.
//          Note that even if two accounts have the same name, they may belong to different people as people could have the same name. A person can have any number of accounts initially, but all of their accounts definitely have the same name.
//
//        After merging the accounts, return the accounts in the following format: the first element of each account is the name, and the rest of the elements are emails in sorted order. The accounts themselves can be returned in any order.
//
//        Example 1:
//        Input:
//        accounts = [["John", "johnsmith@mail.com", "john00@mail.com"], ["John", "johnnybravo@mail.com"], ["John", "johnsmith@mail.com", "john_newyork@mail.com"], ["Mary", "mary@mail.com"]]
//        Output: [["John", 'john00@mail.com', 'john_newyork@mail.com', 'johnsmith@mail.com'],  ["John", "johnnybravo@mail.com"], ["Mary", "mary@mail.com"]]
//        Explanation:
//        The first and third John's are the same person as they have the common email "johnsmith@mail.com".
//        The second John and Mary are different people as none of their email addresses are used by other accounts.
//        We could return these lists in any order, for example the answer [['Mary', 'mary@mail.com'], ['John', 'johnnybravo@mail.com'],
//        ['John', 'john00@mail.com', 'john_newyork@mail.com', 'johnsmith@mail.com']] would still be accepted.
//        Note:
//
//        The length of accounts will be in the range [1, 1000].
//        The length of accounts[i] will be in the range [1, 10].
//        The length of accounts[i][j] will be in the range [1, 30].

import java.util.*;


/**
 *  1. brute: O(n^2)
 *  2. union-find: O(n)
 */
public class AccountsMerge {


    // more concise version
    String find(Map<String, String> parents, String x) {
        if (!x.equals(parents.get(x)))
            parents.put(x, find(parents, parents.get(x)));
        return parents.get(x);
    }

    void union(Map<String, String> parents, String x, String y){
        String xroot = find(parents, x);
        String yroot = find(parents, y);
        if (!xroot.equals(yroot)) parents.put(xroot, yroot);
    }

    public List<List<String>> accountsMerge_v2(List<List<String>> accounts) {
        Map<String, String> parents = new HashMap<>();
        Map<String, String> email2name = new HashMap<>();
        for (List<String> acc: accounts) {
            for (int i = 1; i < acc.size(); i++) {
                parents.put(acc.get(i), acc.get(i));
                email2name.put(acc.get(i), acc.get(0));
            }
        }

//        for (List<String> acc: accounts) {
//            for (int i = 2; i < acc.size(); i++) {
//                union(parents, acc.get(i), acc.get(1));
//            }
//        }
//        for (List<String> acc: accounts) {
//            for (int i = 1; i < acc.size(); i++) {
//                union(parents, parents.get(acc.get(i)), acc.get(i));
//            }
//        }

        // 上述两次for可以改进成一次！！
        // 每隔accounts组内unoin一遍。。。
        for (List<String> acc: accounts) {
            for (int i = 2; i < acc.size(); i++) {
                union(parents, parents.get(acc.get(i)), acc.get(1));
            }
        }

        // 收集一遍...
        Map<String, TreeSet<String>> group2emails = new HashMap<>();
        for (List<String> acc: accounts) {
            for (int i = 1; i < acc.size(); i++) {
                String root = find(parents, acc.get(i));
                if (!group2emails.containsKey(root)) group2emails.put(root, new TreeSet<>());
                group2emails.get(root).add(acc.get(i));
            }
        }
        List<List<String>> res = new ArrayList<>();
        for (String root: group2emails.keySet()) {
            List<String> newaccount = new ArrayList<>();
            newaccount.add(email2name.get(root));
            newaccount.addAll(group2emails.get(root));
            res.add(newaccount);
        }
        return res;
    }



    int find(int[] parents, int x) {
        if (parents[x] != x) {
            parents[x] = find(parents, parents[x]);
        }
        return parents[x];
    }

    void union(int[] parents, int x, int y) {
        int xroot = find(parents, x);
        int yroot = find(parents, y);
        if (xroot != yroot) parents[yroot] = xroot;
    }

    public List<List<String>> accountsMerge(List<List<String>> accounts) {
        int[] parents = new int[accounts.size()];
        for (int i = 0; i < accounts.size(); i++) {
            parents[i] = i;
        }

        Map<String, List<Integer>> email2accounts = new HashMap<>();
        for (int i = 0; i < accounts.size(); i++) {
            for (int j = 1; j < accounts.get(i).size(); j++) {
                String email = accounts.get(i).get(j);
                if (!email2accounts.containsKey(email)) email2accounts.put(email, new ArrayList<>());
                email2accounts.get(email).add(i);
            }
        }

        for (String email: email2accounts.keySet()) {
            List<Integer> accountsToUnion = email2accounts.get(email);
            if (accountsToUnion.size()>1) {
                for (int i = 1; i < accountsToUnion.size(); i++) {
                    union(parents, accountsToUnion.get(0), accountsToUnion.get(i));
                }
            }
        }

        Map<Integer, Set<String>> root2mails = new HashMap<>();
        for (int i = 0; i < accounts.size(); i++) {
            int root = find(parents, i);
            if (!root2mails.containsKey(root)) root2mails.put(root, new HashSet<>());
            root2mails.get(root).addAll(accounts.get(i).subList(1, accounts.get(i).size()));
        }

        List<List<String>> res = new ArrayList<>();
        for (Integer root: root2mails.keySet()) {
            List<String> newaccount = new ArrayList<>();
            String name = accounts.get(root).get(0);
            List<String> mails = new ArrayList<>(root2mails.get(root));
            Collections.sort(mails); // O(klogk)
            newaccount.add(name);
            newaccount.addAll(mails);
            res.add(newaccount);
        }
        return res;
    }


    public static void main(String[] args) {
        List<List<String>> accounts = new ArrayList<>();
        accounts.add(Arrays.asList("David","David0@m.co","David1@m.co"));
        accounts.add(Arrays.asList("David","David3@m.co","David4@m.co"));
        accounts.add(Arrays.asList("David","David4@m.co","David5@m.co"));
        accounts.add(Arrays.asList("David","David2@m.co","David3@m.co"));
        accounts.add(Arrays.asList("David","David1@m.co","David2@m.co"));
//        accounts.add(Arrays.asList("John", "0.com", "4.com", "3.com"));
//        accounts.add(Arrays.asList("John", "5.com", "5.com", "0.com"));
//        accounts.add(Arrays.asList("John", "1.com", "4.com", "0.com"));
//        accounts.add(Arrays.asList("John", "0.com", "1.com", "3.com"));
//        accounts.add(Arrays.asList("John", "4.com", "1.com", "3.com"));
//        accounts.add(Arrays.asList("John", "johnsmith@mail.com", "john00@mail.com"));
//        accounts.add(Arrays.asList("John", "johnnybravo@mail.com"));
//        accounts.add(Arrays.asList("John", "johnsmith@mail.com", "john_newyork@mail.com"));
//        accounts.add(Arrays.asList("Mary", "mary@mail.com"));
        List<List<String>> res = new AccountsMerge().accountsMerge_v2(accounts);
        System.out.println("ASdf");
//
//        [
// ["David","David0@m.co","David1@m.co"],
// ["David","David3@m.co","David4@m.co"],
// ["David","David4@m.co","David5@m.co"],
// ["David","David2@m.co","David3@m.co"],
// ["David","David1@m.co","David2@m.co"]]
//        Output [["David","David0@m.co","David1@m.co","David3@m.co","David4@m.co"],["David","David1@m.co","David2@m.co","David3@m.co","David4@m.co","David5@m.co"]]
//        Expected [["David","David0@m.co","David1@m.co","David2@m.co","David3@m.co","David4@m.co","David5@m.co"]]

//        Input
//                [ ["David","David0@m.co","David4@m.co","David3@m.co"],
//                  ["David","David5@m.co","David5@m.co","David0@m.co"],
//                  ["David","David1@m.co","David4@m.co","David0@m.co"],
//                  ["David","David0@m.co","David1@m.co","David3@m.co"],
//                  ["David","David4@m.co","David1@m.co","David3@m.co"]]
//        Output
//                [["David","David3@m.co"],
//                  ["David","David0@m.co","David1@m.co","David4@m.co","David5@m.co"]]
//        Expected
//                [["David","David0@m.co","David1@m.co","David3@m.co","David4@m.co","David5@m.co"]]
    }
}


//    The key task here is to connect those emails, and this is a perfect use case for union find.
//        to group these emails, each group need to have a representative, or parent.
//        At the beginning, set each email as its own representative.
//        Emails in each account naturally belong to a same group, and should be joined by assigning to the same parent (let's use the parent of first email in that list);
//        Simple Example:
//
//        a b c // now b, c have parent a
//        d e f // now e, f have parent d
//        g a d // now abc, def all merged to group g
//
//        parents populated after parsing 1st account: a b c
//        a->a
//        b->a
//        c->a
//
//        parents populated after parsing 2nd account: d e f
//        d->d
//        e->d
//        f->d
//
//        parents populated after parsing 3rd account: g a d
//        g->g
//        a->g
//        d->g
//
//        Java
//
//class Solution {
//    public List<List<String>> accountsMerge(List<List<String>> acts) {
//        Map<String, String> owner = new HashMap<>();
//        Map<String, String> parents = new HashMap<>();
//        Map<String, TreeSet<String>> unions = new HashMap<>();
//        for (List<String> a : acts) {
//            for (int i = 1; i < a.size(); i++) {
//                parents.put(a.get(i), a.get(i));
//                owner.put(a.get(i), a.get(0));
//            }
//        }
//        for (List<String> a : acts) {
//            String p = find(a.get(1), parents);
//            for (int i = 2; i < a.size(); i++)
//                parents.put(find(a.get(i), parents), p);
//        }
//        for(List<String> a : acts) {
//            String p = find(a.get(1), parents);
//            if (!unions.containsKey(p)) unions.put(p, new TreeSet<>());
//            for (int i = 1; i < a.size(); i++)
//                unions.get(p).add(a.get(i));
//        }
//        List<List<String>> res = new ArrayList<>();
//        for (String p : unions.keySet()) {
//            List<String> emails = new ArrayList(unions.get(p));
//            emails.add(0, owner.get(p));
//            res.add(emails);
//        }
//        return res;
//    }
//    private String find(String s, Map<String, String> p) {
//        return p.get(s) == s ? s : find(p.get(s), p);
//    }
//}
