package graph_dfs_bfs.Topic_UnionFind;/*
734. I:
Given two sentences words1, words2 (each represented as an array of strings), and a list of similar word pairs pairs, determine if two sentences are similar.

For example, "great acting skills" and "fine drama talent" are similar, if the similar word pairs are pairs = [["great", "fine"], ["acting","drama"], ["skills","talent"]].

Note that the similarity relation is not transitive. For example, if "great" and "fine" are similar, and "fine" and "good" are similar, "great" and "good" are not necessarily similar.

However, similarity is symmetric. For example, "great" and "fine" being similar is the same as "fine" and "great" being similar.

Also, a word is always similar with itself. For example, the sentences words1 = ["great"], words2 = ["great"], pairs = [] are similar, even though there are no specified similar word pairs.

Finally, sentences can only be similar if they have the same number of words. So a sentence like words1 = ["great"] can never be similar to words2 = ["doubleplus","good"].

Note:

The length of words1 and words2 will not exceed 1000.
The length of pairs will not exceed 2000.
The length of each pairs[i] will be 2.
The length of each words[i] and pairs[i][j] will be in the range [1, 20].

737. II
Given two sentences words1, words2 (each represented as an array of strings), and a list of similar word pairs pairs, determine if two sentences are similar.

For example, words1 = ["great", "acting", "skills"] and words2 = ["fine", "drama", "talent"] are similar, if the similar word pairs are pairs = [["great", "good"], ["fine", "good"], ["acting","drama"], ["skills","talent"]].

Note that the similarity relation is transitive. For example, if "great" and "good" are similar, and "fine" and "good" are similar, then "great" and "fine" are similar.

Similarity is also symmetric. For example, "great" and "fine" being similar is the same as "fine" and "great" being similar.

Also, a word is always similar with itself. For example, the sentences words1 = ["great"], words2 = ["great"], pairs = [] are similar, even though there are no specified similar word pairs.

Finally, sentences can only be similar if they have the same number of words. So a sentence like words1 = ["great"] can never be similar to words2 = ["doubleplus","good"].
 Note:
The length of words1 and words2 will not exceed 1000.
The length of pairs will not exceed 2000.
The length of each pairs[i] will be 2.
The length of each words[i] and pairs[i][j] will be in the range [1, 20].
*/


import java.util.*;

/*
    I:  not transitive  -> just hash
    II: transitive  -> union find

 */
public class SentenceSimilarityII {


    public boolean areSentencesSimilarII(String[] words1, String[] words2, String[][] pairs) {
        if(words1.length != words2.length) return false;
        DJSet djset = new DJSet();
        for(String[] p:pairs) {
            djset.union(p[0], p[1]);
        }

        for(int i=0;i<words1.length;i++) {
            if(words1[i].equals(words2[i])) continue;
            String r1 = djset.find(words1[i]);
            String r2 = djset.find(words2[i]);
            if(r1.equals(r2)) continue;
            return false;
        }
        return true;
    }
    class DJSet {
        Map<String, String> parents;
        public DJSet() { parents = new HashMap<>(); }
        String find(String w) {
            if(!parents.containsKey(w)) parents.put(w,w);
            if(!parents.get(w).equals(w)) parents.put(w, find(parents.get(w)));
            return parents.get(w);
        }
        void union(String a, String b) {
            String ra = find(a), rb=find(b);
            if(!ra.equals(rb)) parents.put(ra, rb);
        }
    }



    public boolean areSentencesSimilarI(String[] a, String[] b, String[][] pairs) {
        if(a.length != b.length) return false;
        Map<String, Set<String>> map = new HashMap<>();
        for(String[] p:pairs) {
            if(!map.containsKey(p[0])) map.put(p[0], new HashSet<>());
            if(!map.containsKey(p[1])) map.put(p[1], new HashSet<>());
            map.get(p[0]).add(p[1]);
            map.get(p[1]).add(p[0]);
        }

        for(int i=0;i<a.length;i++) {
            if(a[i].equals(b[i])) continue;
            if(map.containsKey(a[i]) && map.get(a[i]).contains(b[i])) continue;
            return false;
        }
        return true;
    }
}