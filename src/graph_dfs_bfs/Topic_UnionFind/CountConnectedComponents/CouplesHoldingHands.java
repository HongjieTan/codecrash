//
//N couples sit in 2N seats arranged in a row and want to hold hands. We want to know the minimum number of swaps so that every couple is sitting side by side. A swap consists of choosing any two people, then they stand up and switch seats.
//
//The people and seats are represented by an integer from 0 to 2N-1, the couples are numbered in order, the first couple being (0, 1), the second couple being (2, 3), and so on with the last couple being (2N-2, 2N-1).
//
//The couples' initial seating is given by row[i] being the value of the person who is initially sitting in the i-th seat.
//
// Example 1:
//Input: row = [0, 2, 1, 3]
//Output: 1
//Explanation: We only need to swap the second (row[1]) and third (row[2]) person.
//
//
// Example 2:
//Input: row = [3, 2, 0, 1]
//Output: 0
//Explanation: All couples are already seated side by side.
//
//
//
//Note:
//
// len(row) is even and in the range of [4, 60].
// row is guaranteed to be a permutation of 0...len(row)-1.
//
package graph_dfs_bfs.Topic_UnionFind.CountConnectedComponents;

/**
 *
 *  TODO: to prove both solutions...
 *
 *  - cyclic swap
 *  - union find !!!!good!!!!!
 *
 */
public class CouplesHoldingHands {
    /*
        union find or dfs/bfs (the problem become calculate how many connected components ...)

        https://leetcode.com/problems/couples-holding-hands/discuss/117520/Java-union-find-easy-to-understand-5-ms

        Think about each couple as a vertex in the graph. So if there are N couples, there are N vertices.
        Now if in position 2i and 2i +1 there are person from couple u and couple v sitting there,
        that means that the permutations are going to involve u and v. So we add an edge to connect u and v.

        If the graph has k connected components, we need N - k swaps to reduce it back to N disjoint vertices.
        - How to prove it  -> each connected component (size xi, i=1...k) need xi-1 swaps,  sum(xi-1)=sum(xi)-sum(1)=N-k done!

        The theory: https://en.wikipedia.org/wiki/Cyclic_permutation
     */
    int[] parents;
    int find(int x) {
        if (parents[x]!=x) return find(parents[x]);
        return parents[x];
    }
    void union(int x, int y) {
        int rootx = find(x);
        int rooty = find(y);
        if (rootx!=rooty) {
            parents[rootx] = rooty;
        }
    }
    public int minSwapsCouples_union_find(int[] row) {
        int n = row.length/2;
        parents = new int[n];
        for (int i = 0; i < n; i++) {
            parents[i] = i;
        }
        int groups = n;
        for (int i = 0; i < row.length; i+=2) {
            int u = row[i]/2;
            int v = row[i+1]/2;

            if (u!=v && find(u)!=find(v)) {
                union(u, v);
                groups--;
            }
        }
        return n-groups;
    }


    // cyclic swap
    // https://leetcode.com/problems/couples-holding-hands/discuss/113362/JavaC%2B%2B-O(N)-solution-using-cyclic-swapping
    public int minSwapsCouples_cyclic_swap(int[] row) {
        int res = 0;
        int[] partner = new int[row.length];
        int[] position = new int[row.length];
        for (int i = 0; i < row.length; i++) {
            partner[i] = i%2==0?i+1:i-1;
            position[row[i]] = i;
        }

        int p = 0;
        while (p<row.length) {
            int expected_position = partner[position[partner[row[p]]]]; // TODO why??
            if (p!=expected_position) {
                swap(row, p, expected_position);
                swap(position, row[p], row[expected_position]);
                res++;
            } else {
                p++;
            }
        }
        return res;
    }

    void swap(int[] row, int a, int b) {
        int temp = row[a];
        row[a] = row[b];
        row[b] = temp;
    }

}
