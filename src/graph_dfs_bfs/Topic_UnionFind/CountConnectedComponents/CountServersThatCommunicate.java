//You are given a map of a server center, represented as a m * n integer matrix grid,
// where 1 means that on that cell there is a server and 0 means that it is no server.
// Two servers are said to communicate if they are on the same row or on the same column.
//
//Return the number of servers that communicate with any other server.
//
//
// Example 1:
//
//
//
//
//Input: grid = [[1,0],[0,1]]
//Output: 0
//Explanation: No servers can communicate with others.
//
// Example 2:
//
//
//
//
//Input: grid = [[1,0],[1,1]]
//Output: 3
//Explanation: All three servers can communicate with at least one other server.
//
//
// Example 3:
//
//
//
//
//Input: grid =
//[[1,1,0,0],
// [0,0,1,0],
// [0,0,1,0],
// [0,0,0,1]]
//Output: 4
//Explanation: 
// The two servers in the first row can communicate with each other.
// The two servers in the third column can communicate with each other.
// The server at right bottom corner can't communicate with any other server.
//
//
//
// Constraints:
//
//
// m == grid.length
// n == grid[i].length
// 1 <= m <= 250
// 1 <= n <= 250
// grid[i][j] == 0 or 1
//


package graph_dfs_bfs.Topic_UnionFind.CountConnectedComponents;

import java.util.HashMap;
import java.util.Map;

public class CountServersThatCommunicate {

    // union-find
    class DJSet {
        int[] parent;
        public DJSet(int n){
            parent = new int[n];
            for(int i=0;i<n;i++) parent[i]=i;
        }
        int find(int x) {
            return x==parent[x] ? x: (parent[x] = find(parent[x]));
        }
        void union(int x, int y) {
            x = find(x);
            y = find(y);
            if(x!=y) parent[x] = y;
        }
    }

    public int countServers(int[][] grid) {
        int n=grid.length, m=grid[0].length;
        DJSet djset = new DJSet(n*m + n + m);
        for(int i=0;i<n;i++) {
            for(int j=0;j<m;j++) {
                if(grid[i][j]==1) {
                    djset.union(i*m+j, n*m+i);
                    djset.union(i*m+j, n*m+n+j);
                }
            }
        }

        Map<Integer, Integer> freq = new HashMap<>();
        for(int i=0;i<n*m;i++) {
            int root = djset.find(i);
            freq.put(root, freq.getOrDefault(root,0)+1);
        }
        int ret = 0;
        for(int val: freq.values()) ret+=val>1?val:0;
        return ret;
    }


    // another concise version, but the union-find version generalize well!
    public int countServers_v2(int[][] grid) {
        int n = grid.length, m = grid[0].length;
        int[] row = new int[n];
        int[] col = new int[m];

        for(int i=0;i<n;i++) {
            for(int j=0;j<m;j++) {
                if(grid[i][j]==1) {
                    row[i]++;
                    col[j]++;
                }
            }
        }

        int ret = 0;
        for(int i=0;i<n;i++) {
            for(int j=0;j<m;j++) {
                if(grid[i][j]==1 && row[i]+col[j]>2) {
                    ret++;
                }
            }
        }
        return ret;
    }

    public static void main(String[] as) {
        int[][] g = {
//                {0,0,0,0},
//                {1,1,1,1},
//                {0,0,0,1},
//                {0,0,1,1},
//                {0,0,0,1}

                 {1,1,0,0},
                 {0,0,1,0},
                 {0,0,1,0},
                 {0,0,0,1}
        };
        System.out.println(new CountServersThatCommunicate().countServers(g));
    }

}
















/*
by top:
Observation

For each server, the row or the column must have another server except the current one.
We can simply keep a count of servers in each row and column and use this information to get the result while traversing the grid.

Solution

class Solution {
public:
    int countServers(vector<vector<int>>& grid)
    {
        vector<int> rows(grid.size(),0),columns(grid[0].size(),0);	//Stores count of servers in rows and colums
        for(int i=0;i<grid.size();i++)						//Fill the count vectors
            for(int j=0;j<grid[i].size();j++)
                if(grid[i][j])
                    rows[i]++,columns[j]++;
        int result=0;
        for(int i=0;i<grid.size();i++)			//Traverse the grid to get result count
            for(int j=0;j<grid[i].size();j++)
                if(grid[i][j]&&(columns[j]>1||rows[i]>1))	//Check if there are any other server except the current one in it's corresponding row or column.
                    result++;
        return result;
    }
};
Complexity
Here m is the number of rows and n is the number of columns.
Space: O(m+n).This can be reduced to min(m,n).
Time: O(m*n).


by lee:
Count the number of servers in each row to X
Count the number of servers in each col to Y
If X[i] + Y[j] > 2, the server at A[i][j] (if exists) communicate.

Python:

    def countServers(self, A):
        X, Y = map(sum, A), map(sum, zip(*A))
        return sum(X[i] + Y[j] > 2 for i in xrange(len(A)) for j in xrange(len(A[0])) if A[i][j])



by uwi
class Solution {
    public int countServers(int[][] grid) {
        int n = grid.length;
        int m = grid[0].length;
        DJSet ds = new DJSet(n*m);
        for(int i = 0;i < n;i++){
            for(int j = 0;j < m;j++){
                if(grid[i][j] == 1){
                    for(int k = i+1;k < n;k++){
                        if(grid[k][j] == 1){
                            ds.union(i*m+j, k*m+j);
                            break;
                        }
                    }
                    for(int k = j+1;k < m;k++){
                        if(grid[i][k] == 1){
                            ds.union(i*m+j, i*m+k);
                            break;
                        }
                    }
                }
            }
        }
        int ct = 0;
        for(int i = 0;i < n;i++){
            for(int j = 0;j < m;j++){
                if(grid[i][j] == 1 && ds.upper[i*m+j] < -1){
                    int q = -ds.upper[i*m+j];
                    ct += q;
                }
            }
        }
        return ct;
    }

    public class DJSet {
        public int[] upper;

        public DJSet(int n) {
            upper = new int[n];
            Arrays.fill(upper, -1);
        }

        public int root(int x) {
            return upper[x] < 0 ? x : (upper[x] = root(upper[x]));
        }

        public boolean equiv(int x, int y) {
            return root(x) == root(y);
        }

        public boolean union(int x, int y) {
            x = root(x);
            y = root(y);
            if (x != y) {
                if (upper[y] < upper[x]) {
                    int d = x;
                    x = y;
                    y = d;
                }
                upper[x] += upper[y];
                upper[y] = x;
            }
            return x == y;
        }

        public int count() {
            int ct = 0;
            for (int u : upper)
                if (u < 0)
                    ct++;
            return ct;
        }
    }

}
*/