package graph_dfs_bfs.Topic_UnionFind;

/**
 * Created by thj on 2018/9/9.
 *
 *  本质就是判环！
 *
 */
public class GraphValidTree {



    private void makeset(int[] parents) {
        for (int i = 0; i < parents.length; i++) {
            parents[i] = i;
        }
    }

    private int find(int[] parents, int x) {
        if (parents[x] != x) {
            return find(parents, parents[x]);
        }
        return parents[x];
    }

    private void union(int[] parents, int x, int y) {
        int xset = find(parents, x);
        int yset = find(parents, y);
        if (xset!=yset) {
            parents[xset] = yset;
        }
    }

    public boolean validTree(int n, int[][] edges) {
        // write your code here

        if (edges.length != n-1) return false;

        int[] parents = new int[n];

        makeset(parents);

        for(int[] edge: edges) {
            int x = edge[0];
            int y = edge[1];

            int xset = find(parents, x);
            int yset = find(parents, y);

            if (xset == yset) {
                return false; // cycle detected
            } else {
                union(parents, x, y);
            }
        }

        return true;
    }

}
