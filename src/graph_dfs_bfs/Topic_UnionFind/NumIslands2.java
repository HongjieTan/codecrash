package graph_dfs_bfs.Topic_UnionFind;

import common.Point;

import java.util.*;

/**
 * Created by thj on 2018/9/9.
 *
 *  1. brute force: dfs after each operation, O(m*n*ops)
 *  2. union-find(with pass compression!!!):  O(ops) !
 */
public class NumIslands2 {

    /**
     *  union-find !!!!
     */
    private int find(int[] parents, int x) {
        while (parents[x]!=x) {
            parents[x] = parents[parents[x]]; // path compression !!!
            x = parents[x];
        }
        return x;
    }

    public List<Integer> numIslands2(int n, int m, Point[] operators) {
        if (operators==null) return new ArrayList<>();

        List<Integer> res = new ArrayList<>();
        int[] parents = new int[n*m];
        Arrays.fill(parents, -1);

        int[][] directions = new int[][]{{-1,0}, {1,0}, {0, -1}, {0, 1}};
        int count=0;
        for (int i = 0; i < operators.length; i++) {
            Point p = operators[i];
            int key = p.x*m+p.y; // notice: key=x*cols+y (not x*rows+y !!!!!)

            if (parents[key]==-1) {
                count++;
                parents[key] = key;

                for (int[] direction: directions) {
                    int nx = p.x+direction[0];
                    int ny = p.y+direction[1];
                    int nkey = nx*m+ny;

                    if (nx>=0 && nx<n && ny>=0 && ny<m && parents[nkey]!=-1) {
                        int root = find(parents, key); // find with path compression! O(1)
                        int nroot = find(parents, nkey);
                        if (root!=nroot) {
                            // union!
                            // (always set parent to the root, same effect as union by rank, can prevent tree hight grow as O(n))
                            parents[nroot] = root;
                            count--;
                        }
                    }
                }
            }

            res.add(count);

        }

        return res;
    }


    // x2 version...
//    private int find(int[] parents, int x) {
//        while (parents[x]!=x) {
//            parents[x] = parents[parents[x]]; // path compression !!!
//            x = parents[x];
//        }
//        return x;
//    }
//
//    public List<Integer> numIslands2_x2(int n, int m, Point[] operators) {
//        if (operators==null) return new ArrayList<>();
//
//        List<Integer> res = new ArrayList<>();
//        int[] parents = new int[n*m];
//        Arrays.fill(parents, -1);
//
//        int[][] directions = new int[][]{{-1,0}, {1,0}, {0, -1}, {0, 1}};
//        int count=0;
//        for (int i = 0; i < operators.length; i++) {
//            Point p = operators[i];
//            int key = p.x*m+p.y; // notice: key=x*cols+y (not x*rows+y !!!!!)
//
//            if (parents[key]==-1) {
//                count++;
//                parents[key] = key;
//
//                for (int[] direction: directions) {
//                    int nx = p.x+direction[0];
//                    int ny = p.y+direction[1];
//                    int nkey = nx*m+ny;
//
//                    if (nx>=0 && nx<n && ny>=0 && ny<m && parents[nkey]!=-1) {
//                        int root = find(parents, key); // find with path compression! O(1)
//                        int nroot = find(parents, nkey);
//                        if (root!=nroot) {
//                            // union!
//                            // (always set parent to the root, same effect as union by rank, can prevent tree hight grow as O(n))
//                            parents[nroot] = root;
//                            count--;
//                        }
//                    }
//                }
//            }
//
//            res.add(count);
//
//        }
//
//        return res;
//    }





    /**
     *     1. brute force
     */
    public List<Integer> numIslands2_naive(int n, int m, Point[] operators) {
        //...
        List<Integer> res = new ArrayList<>();
        int[][] grid = new int[n][m];
        if (operators!=null) {
            for (Point p: operators) {
                grid[p.x][p.y] = 1;
                res.add(numOfIslands(n, m, grid));
            }
        }

        return res;

    }

    public int numOfIslands(int n, int m, int[][] grid) {
        boolean[][] visited = new boolean[n][m];
        int count=0;

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                if (!visited[i][j] && grid[i][j]==1) {
                    count++;
                    dfs(i, j, grid, visited);
                }
            }
        }
        return count;
    }

    private void dfs(int row, int col, int[][] grid, boolean[][] visited) {
        if (grid[row][col]==1 && !visited[row][col] ) {
            visited[row][col] = true;

            if (row-1>=0 && !visited[row-1][col]) dfs(row-1, col, grid, visited);
            if (row+1<grid.length && !visited[row+1][col]) dfs(row+1, col, grid, visited);
            if (col-1>=0 && !visited[row][col-1]) dfs(row, col-1, grid, visited);
            if (col+1<grid[0].length && !visited[row][col+1]) dfs(row, col+1, grid, visited);
        }
    }


    public static void main(String[] args) {
//        8
//        14
//        [0,9],[5,4],[0,12],[6,9],[6,5],[0,4]


        int n=8, m=14;
        Point[] ops = new Point[]{new Point(0,9),new Point(5,4),new Point(0,12),new Point(6,9),new Point(6,5),new Point(0,4)};
        List<Integer> res = new NumIslands2().numIslands2(n, m, ops);
        for (Integer count: res) System.out.println(count);

//        int[][] grid=new int[][]{
//            {1,0},
//            {0,0}
//        };
//        System.out.println(new graph_dfs_bfs.Topic_UnionFind.NumIslands2().numOfIslands(2,2,grid));

    }

}
