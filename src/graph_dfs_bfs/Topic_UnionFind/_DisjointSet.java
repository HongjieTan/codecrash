package graph_dfs_bfs.Topic_UnionFind;

public class _DisjointSet {
    int[] parent;
    public _DisjointSet(int n){
        parent = new int[n];
        for(int i=0;i<n;i++) parent[i]=i;
    }
    int find(int x) {
        return x==parent[x] ? x: (parent[x] = find(parent[x]));
    }
    void union(int x, int y) {
        x = find(x);
        y = find(y);
        if(x!=y) parent[x] = y;
    }
}
