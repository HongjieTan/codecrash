package graph_dfs_bfs.Topic_UnionFind;

/**
 * Created by thj on 2018/9/6.
 *
 *  1. naive union-find:
 *       find: O(n)
 *       union: O(n)
 *
 *  2. union-find with path-compression and union-by-rank
 *      N queries of union takes (amortized) O(a(N)) time,and O(a(N)) is approximately O(1), (a(x):Inverse-Ackermann function, ...)
 *      find: O(1) (amortized)
 *      union: O(1) (amortized)
 *
 */
public class UnionFindBasic {

    /**
     *  basic version
     *
     */
    void makeSet(int[] parent) {
        for (int i = 0; i < parent.length; i++) {
            parent[i] = i;
        }
    }

    int find(int[] parent, int i) {
        if (parent[i]==i)
            return i;
        return find(parent, parent[i]);
    }

    void union(int[] parent, int x, int y) {
        int xset = find(parent, x);
        int yset = find(parent, y);
        parent[xset] = yset;
    }


    /**
     *  using union by rank and path compression ！
     *
     */
    class Subset {
        int parent;
        int rank;
    }

    void makeset(Subset[] parents) {
        for (int i = 0; i < parents.length; i++) {
            parents[i].parent = i;
            parents[i].rank = 0;
        }
    }

    // find with path compression
    int find(Subset[] parents, int i) {
        if (parents[i].parent != i)
            parents[i].parent = find(parents, parents[i].parent);
        return parents[i].parent;
    }

    // union by rank
    void union(Subset[] parents, int x, int y) {
        int xset = find(parents, x);
        int yset = find(parents, y);

        // Attach smaller rank tree under root of high rank tree
        // (Union by Rank)
        if (parents[xset].rank < parents[yset].rank) {
            parents[xset].parent = yset;
        } else if (parents[xset].rank > parents[yset].rank) {
            parents[yset].parent = xset;
        } else {
            // If ranks are same, then make one as root and increment its rank by one
            parents[yset].parent = xset;
            parents[yset].rank ++ ;
        }
    }

}
