package graph_dfs_bfs.Topic_UnionFind;

import java.util.*;

/**
 * Created by thj on 2018/9/4.
 *
 *
 *  另一种问法是汇率转换。。。 'USD' -> 'RMB' = 6.8, 'RMB' -> 'JPY' = 10，。。。
 *
 *  1. dfs(or bfs): O(e * q),
 *
 *  2. union-find:  O(e + q),  union-find with pass compression and union by rank!
 *          - 建立unin-find-set: O(e)
 *          - 每次query： O(1）
 *
 *          注意这里其实是有向图，使用union-find时候要注意边的顺序的选择！！使得最终并查集无环。
 *
 *
 *     一般要用union-find的这种方法，因为一般查询的需求是比较多的，需要查询高效率！！！！
 *     （如果只有单次查询。。用dfs和union-find一样。。。）
 *
 *
 */
public class EvaluateDivision {

    Map<String, String> parents;
    Map<String, Double> ratio;
    String find(String x) {
        if(x!=parents.get(x)) { // 注意这里的操作顺序，比较容易出错。。。
            String root = find(parents.get(x));
            double x2p = ratio.get(x);
            double p2r = ratio.get(parents.get(x));
            ratio.put(x, x2p*p2r);
            parents.put(x, root);
        }
        return parents.get(x);
    }
    public double[] calcEquation_X3(String[][] equations, double[] values, String[][] queries) {
        parents = new HashMap<>();
        ratio = new HashMap<>();

        for(int i=0;i<equations.length;i++) {
            String src = equations[i][0], dst=equations[i][1];
            double v = values[i];
            // 因为每人只能有一个parent，所以这里分情况处理!!
            if(!parents.containsKey(src) && !parents.containsKey(dst)) {
                parents.put(dst, dst);
                ratio.put(dst, 1.0);
                parents.put(src, dst);
                ratio.put(src, v);
            } else if(!parents.containsKey(src)) {
                parents.put(src, dst);
                ratio.put(src, v);
            } else if(!parents.containsKey(dst)) {
                parents.put(dst, src);
                ratio.put(dst, 1.0/v);
            } else {
                parents.put(src, dst);
                ratio.put(src, v);
            }
        }

        double[] ans = new double[queries.length];
        int idx=0;
        for(String[] q: queries) {
            String x = q[0], y=q[1];
            if(!parents.containsKey(x) || !parents.containsKey(y)) {
                ans[idx++] = -1.0;
            } else {
                String xroot = find(x);
                String yroot = find(y);
                if(!xroot.equals(yroot)) {
                    ans[idx++] = -1.0;
                } else {
                    double x2r = ratio.get(x);
                    double y2r = ratio.get(y);
                    ans[idx++] = x2r/y2r;
                }
            }

        }
        return ans;
    }


    // how to handle the case:  a->b:2.0   a->c:3.0  query:c->b?
//    Map<String, String> parents;
//    Map<String, Double> ratio;  // ratio to its parent...
//    String find(String x) {
//        if(x!=parents.get(x)) {
//            String root = find(parents.get(x));
//            ratio.put(x, ratio.get(x)* ratio.get(parents.get(x)));
//            parents.put(x, root);
//        }
//        return parents.get(x);
//    }
//
//    public double[] calcEquation_x2(String[][] equations, double[] values, String[][] queries) {
//        parents = new HashMap<>();
//        ratio = new HashMap<>();
//        for(int i=0;i<equations.length;i++) {
//            String[] eq=equations[i];
//            if (!parents.containsKey(eq[0]) && !parents.containsKey(eq[1])) {
//                parents.put(eq[0], eq[1]);
//                ratio.put(eq[0], values[i]);
//                parents.put(eq[1], eq[1]);
//                ratio.put(eq[1], 1.0);
//            } else if (!parents.containsKey(eq[0])) {
//                parents.put(eq[0], eq[1]);
//                ratio.put(eq[0], values[i]);
//            } else if (!parents.containsKey(eq[1])) {
//                parents.put(eq[1], eq[0]);
//                ratio.put(eq[1], 1.0/values[i]);
//            } else {
//                String xroot=find(eq[0]), yroot=find(eq[1]);
//                if (xroot!=yroot) {
//                    parents.put(xroot, yroot);
//                    ratio.put(xroot, ratio.get(eq[1])/ ratio.get(eq[0]) *values[i]); // notice!
//                }
//            }
//        }
//
//        double[] ans = new double[queries.length];
//        int idx=0;
//        for(String[] q: queries) {
//            if (!parents.containsKey(q[0]) || !parents.containsKey(q[1])) {
//                ans[idx++]=-1.0;
//                continue;
//            }
//            String xroot = find(q[0]), yroot=find(q[1]);
//            if(xroot!=yroot) {
//                ans[idx++]=-1.0;
//            } else {
//                ans[idx++]= ratio.get(q[0])/ ratio.get(q[1]);
//            }
//        }
//        return ans;
//    }


    /**
     * union find version
     */
    class Node {
        String key;
        double ratio; // save to ratio from curNode to root
        public Node(String key, double ratio) {this.key = key; this.ratio = ratio;}
    }

    public double[] calcEquation_uf(String[][] equations, double[] values, String[][] queries) {

        // step1. make union-find set

        //    A/B = 2.0  ->  parents[A] = {B, 2.0}
        //    B/C = 3.0  ->  parents[B] = {C, 3.0}
        Map<String, Node> parents = new HashMap<>();

        for (int i = 0; i < equations.length; i++) {

            String a = equations[i][0];
            String b = equations[i][1];
            double val = values[i];

            if (!parents.containsKey(a) && !parents.containsKey(b)) { // a, b not in forests
                parents.put(a, new Node(b, val));
                parents.put(b, new Node(b, 1.0));
            } else if (!parents.containsKey(a)) { // only a not in forests
                parents.put(a, new Node(b, val));
            } else if (!parents.containsKey(b)){ // only b not in forests
                parents.put(b, new Node(a, 1/val));
            } else { // both in forests -> union
                Node rootA = find(parents, a);
                Node rootB = find(parents, b);
                parents.put(rootA.key, new Node(rootB.key, val/rootA.ratio*rootB.ratio)); // notice!!!
            }
        }

        // step2. queries
        double[] res = new double[queries.length];
        for (int i = 0; i < queries.length; i++) {
            String a = queries[i][0];
            String b = queries[i][1];
            if (!parents.containsKey(a) || !parents.containsKey(b)) {
                res[i] = -1;
                continue;
            }
            Node rootA = find(parents, a);  // a/rootA = rootA.ratio  (Notice: node.ratio contains ratio from a to root!!!)
            Node rootB = find(parents, b);  // b/rootB = rootB.ratio
            if (rootA.key != rootB.key) {
                res[i] = -1;
            } else {
                res[i] = rootA.ratio/rootB.ratio; // a/b = (a/rootA) / (b/rootB)
            }
        }

        return res;
    }

    // find with Path Compression!!!!
    Node find(Map<String, Node> parents, String x) {
        if (parents.get(x).key != x) {
            Node root = find(parents, parents.get(x).key);
            parents.get(x).key = root.key;
            parents.get(x).ratio *= root.ratio; // notice!!
        }
        return parents.get(x);
    }



    /**
     *  dfs version
     */
    class Edge {
        String node;
        Double cost;
        Edge(String node, Double cost) {this.node=node; this.cost=cost;}
    }

    // dfs solution: O(e * q)
    public double[] calcEquation(String[][] equations, double[] values, String[][] queries) {

        Map<String, List<Edge>> adjMap = new HashMap<>();
        for (int i = 0; i < equations.length; i++) {
            String start = equations[i][0];
            String end = equations[i][1];
            double cost = values[i];
            if (!adjMap.containsKey(start)) {
                adjMap.put(start, new ArrayList<>());
            }
            if (!adjMap.containsKey(end)) {
                adjMap.put(end, new ArrayList<>());
            }

            adjMap.get(start).add(new Edge(end, cost));
            adjMap.get(end).add(new Edge(start, 1/cost));
        }

        Set<String> visited = new HashSet<>();

        double[] res = new double[queries.length];

        for (int i = 0; i < queries.length; i++) {
            visited.clear();
            String source = queries[i][0];
            String target = queries[i][1];
            res[i] = dfs(1, source, target, adjMap, visited);
        }

        return res;
    }

    private double dfs(double cost, String soure, String target,
                                  Map<String, List<Edge>> adjs,
                                  Set<String> visited) {
        if (!adjs.containsKey(soure)) return -1;
        if (soure.equals(target)) return cost;

        visited.add(soure);

        if (adjs.get(soure)!=null) {
            for (Edge nb: adjs.get(soure)) {
                if (!visited.contains(nb.node)) {
                    double newCost = dfs(cost*nb.cost, nb.node,target, adjs, visited);
                    if (newCost!=-1) {
                        return newCost;
                    }
                }
            }
        }

        return -1;
    }

    private double bfs(String source, String target, Map<String, List<Edge>> adjMap) {
        // skip ...
        return -1;
    }


    public static void main(String[] args) {
//        String[][] equations = new String[][]{
//                {"a", "b"},
//                {"b", "c"},
////                {"a", "c"},
//        };
        String[][] equations = new String[][]{
                {"a","b"},
                {"e","f"},
                {"b","e"},
        };

        double[] vals = new double[]{2.0,3.0,4.0};
        String[][] queries = new String[][]{
                {"a", "f"},
//                {"b", "c"},
//                {"a", "e"}
        };

//        double[] res = new EvaluateDivision().calcEquation_uf(equations, vals, queries);
        double[] res = new EvaluateDivision().calcEquation_X3(equations, vals, queries);
        for(double x:res)
            System.out.println(x);

    }

}
