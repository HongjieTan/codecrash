package graph_dfs_bfs.Topic_UnionFind;

/**
 *  two aspects to break tree structure: (aspect 1,2 can overlap...)
 *  1. two parents
 *  2. cycle exists
 *
 *  steps:
 *   找出导致two parents的两条边 -> 如果存在，删一条边后用union-find来找环，如找到环返回另一条边，如没找到返回删的边
 *                             -> 如果不存在，直接用union-find（跟684一样做）
 *
 *
 *
 *             The key: 有没有一种场景，直接unionfind找到环后，直接删除任何一边，不能达到要求？ 如下：把1->2删了还是有环！所以两条边不能随机删，要try and check!!
 *
 *                             1
 *                            /  \
 *                           v    v
 *                       >  2      3
 *                      /  /
 *                     /  v
 *                      4
 */
public class RedundantConnection2 {

    public int[] findRedundantDirectedConnection_x2(int[][] edges) {
        int n = edges.length;
        int[] parents = new int[n+1];

        int[] cand1=null, cand2=null;
        for(int[] edge: edges) {
            int src = edge[0], dst=edge[1];
            if(parents[dst]==0) {
                parents[dst]=src;
            } else {
                cand1 = new int[]{src, dst};
                cand2 = new int[]{parents[dst], dst};
            }
        }


        for(int i=1;i<=n;i++) parents[i]=i;

        for(int[] edge: edges) {
            if(cand1!=null && cand1[0]==edge[0] && cand1[1]==edge[1]) continue; // delete cand1
            if(find(parents, edge[0])==find(parents, edge[1])) {
                // circle detect
                return cand1==null?edge:cand2;
            } else {
                union(parents,edge[0], edge[1]);
            }
        }
        return cand1;
    }

    private int find(int[] parents, int x) {
        if (parents[x]!=x) return find(parents, parents[x]);
        return parents[x];
    }
    private void union(int[] parents, int x, int y) {
        int xroot=find(parents, x);
        int yroot = find(parents, y);
        if(xroot!=yroot) {
            parents[xroot]=yroot;
        }
    }


//    public int[] findRedundantDirectedConnection(int[][] edges) {
//        int[] candidate1 = new int[]{-1, -1};
//        int[] candidate2 = new int[]{-1, -1};
//        int[] parents = new int[edges.length+1];
//
//        // find possible candidates to cause two-parents issue
//        for (int[] edge: edges) {
//            int src = edge[0], dst = edge[1];
//            if (parents[dst] == 0) {
//                parents[dst] = src;
//            } else {
//                candidate1 = new int[]{src, dst}; // copy it
//                candidate2 = new int[]{parents[dst], dst};
//                edge[0] = -1; // delete candidate1 and try to check
//            }
//        }
//
//        // find cycle issue using union-find
//        for (int i = 1; i <= edges.length; i++) {
//            parents[i] = i;
//        }
//        for (int[] edge: edges) {
//            if (edge[0] == -1) continue; // skip deleted edge
//            int src = edge[0], dst = edge[1];
//            int srcroot = find(parents, src), dstroot = find(parents, dst);
//            if (srcroot == dstroot) { // cycle detected
//                if (candidate1[0] == -1) { // 没有two-parents问题的话，就是这条边的锅
//                    return edge;
//                } else {// 有two-parents问题，并且已去掉can1，还产生了环，那就是can2的锅
//                    return candidate2;
//                }
//            }
//
//            // union
//            parents[dstroot] = srcroot;
//
//        }
//
//        return  candidate1;
//    }
//
//    private int find(int[] parents, int x) {
//        while(parents[x]!=x) {
//            parents[x] = parents[parents[x]]; // path compression (optional)
//            x = parents[x];
//        }
//        return x;
//    }

    public static void main(String[] args) {
        int[][] edges = new int[][]{{2,1},{3,1},{4,2},{1,4}};
//        new RedundantConnection2().findRedundantDirectedConnection(edges);
    }
}
