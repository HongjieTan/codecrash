// You are given a string s, and an array of pairs of indices in the string pairs where pairs[i] = [a, b] indicates 2 indices(0-indexed) of the string.
//
// You can swap the characters at any pair of indices in the given pairs any number of times.
//
// Return the lexicographically smallest string that s can be changed to after using the swaps.
//
//
// Example 1:
//
//
//Input: s = "dcab", pairs = [[0,3],[1,2]]
//Output: "bacd"
//Explaination:
//Swap s[0] and s[3], s = "bcad"
//Swap s[1] and s[2], s = "bacd"
//
//
// Example 2:
//
//
//Input: s = "dcab", pairs = [[0,3],[1,2],[0,2]]
//Output: "abcd"
//Explaination:
//Swap s[0] and s[3], s = "bcad"
//Swap s[0] and s[2], s = "acbd"
//Swap s[1] and s[2], s = "abcd"
//
// Example 3:
//
//
//Input: s = "cba", pairs = [[0,1],[1,2]]
//Output: "abc"
//Explaination:
//Swap s[0] and s[1], s = "bca"
//Swap s[1] and s[2], s = "bac"
//Swap s[0] and s[1], s = "abc"
//
//
//
// Constraints:
//
//
// 1 <= s.length <= 10^5
// 0 <= pairs.length <= 10^5
// 0 <= pairs[i][0], pairs[i][1] < s.length
// s only contains lower case English letters.
//

package graph_dfs_bfs.Topic_UnionFind;

import java.util.*;


// union find...
public class SmallestStringWithSwaps {
    int[] parents;
    void init(int n) {
        parents = new int[n];
        for(int i=0;i<n;i++) parents[i]=i;
    }
    int find(int x) {
        if(parents[x]!=x) parents[x] = find(parents[x]);
        return parents[x];
    }
    void union(int x, int y) {
        int rx = find(x), ry = find(y);
        if(rx!=ry) {
            parents[rx]=ry;
        }
    }

    public String smallestStringWithSwaps(String s, List<List<Integer>> pairs) {
        int n = s.length();
        init(n);
        for(List<Integer> p: pairs) union(p.get(0), p.get(1));

        Map<Integer, List<Integer>> groups = new HashMap<>();
        for(int i=0;i<n;i++) {
            int root = find(i);
            if(!groups.containsKey(root)) groups.put(root, new ArrayList<>());
            groups.get(root).add(i);
        }

        char[] ret = new char[n];
        for(List<Integer> idxs: groups.values()) {
            int k = idxs.size();
            char[] tmp = new char[k];
            for(int i=0;i<k;i++) tmp[i] = s.charAt(idxs.get(i));
            Arrays.sort(tmp);
            for(int i=0;i<k;i++) ret[idxs.get(i)] = tmp[i];
        }

        return String.valueOf(ret);
    }

    public static void main(String[] as) {
//        String s = "dcab";
//        List<List<Integer>> pairs = Arrays.asList(Arrays.asList(0,3),Arrays.asList(1,2));
//        String s = "dcab";
//        List<List<Integer>> pairs = Arrays.asList(Arrays.asList(0,3),Arrays.asList(1,2),Arrays.asList(0,2));
        String s = "cba";
        List<List<Integer>> pairs = Arrays.asList(Arrays.asList(0,1),Arrays.asList(1,2));
        System.out.println(new SmallestStringWithSwaps().smallestStringWithSwaps(s, pairs));
    }
}


/*
by uwi:
class Solution {
    public String smallestStringWithSwaps(String ss, List<List<Integer>> pairs) {
        int n = ss.length();
        DJSet ds = new DJSet(n);
        for(List<Integer> p : pairs){
            ds.union(p.get(0), p.get(1));
        }
        char[] s = ss.toCharArray();
        List<List<Integer>> all = new ArrayList<>();
        for(int i = 0;i < n;i++){
            if(ds.upper[i] < 0){
                all.add(new ArrayList<Integer>());
            }else{
                all.add(null);
            }
        }
        for(int i = 0;i < n;i++){
            all.get(ds.root(i)).add(i);
        }
        char[] t = new char[n];
        for(List<Integer> q : all){
            if(q == null)continue;
            int p = 0;
            for(int u : q){
                t[p++] = s[u];
            }
            Arrays.sort(t, 0, p);
            for(int i = 0;i < p;i++){
                s[q.get(i)] = t[i];
            }
        }
        return new String(s);
    }

    public class DJSet {
        public int[] upper;

        public DJSet(int n) {
            upper = new int[n];
            Arrays.fill(upper, -1);
        }

        public int root(int x) {
            return upper[x] < 0 ? x : (upper[x] = root(upper[x]));
        }

        public boolean equiv(int x, int y) {
            return root(x) == root(y);
        }

        public boolean union(int x, int y) {
            x = root(x);
            y = root(y);
            if (x != y) {
                if (upper[y] < upper[x]) {
                    int d = x;
                    x = y;
                    y = d;
                }
                upper[x] += upper[y];
                upper[y] = x;
            }
            return x == y;
        }

        public int count() {
            int ct = 0;
            for (int u : upper)
                if (u < 0)
                    ct++;
            return ct;
        }
    }

}

*/
