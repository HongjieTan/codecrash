package graph_dfs_bfs.Topic_ListAllPaths;

import java.util.*;

/**
 * Created by thj on 2018/8/5.
 *
 *   given directed acyclic graph:  acyclic -> no need visited
 *
 *
 *   Time:
 *    1. basic:   exponential level
 *
 *    2. with memorization!!!(top down dp):   O(n^2)
 *
 *        to use memorization. Each node will be only visited once since the sub result from
 *        this node has already been recorded.
 *
 *
 *
 */
public class AllPaths {


    // 9 ms
    public List<List<Integer>> allPathsSourceTarget_dp(int[][] graph) {
        Map<Integer, List<List<Integer>>> cache = new HashMap<>();
        int n = graph.length, source = 0, target=n-1;
        dfs_dp(graph, source, target, cache);
        return cache.get(0);
    }

    private List<List<Integer>> dfs_dp(int[][] graph, int curNode, int target,  Map<Integer, List<List<Integer>>> cache) {

        if (cache.containsKey(curNode)) { // avoid duplicate calculation
            return cache.get(curNode);
        }

        List<List<Integer>> paths = new ArrayList<>();
        if (curNode == target) {
            paths.add(Arrays.asList(target));
        } else {
            for (int nb: graph[curNode]) {
                List<List<Integer>> subPaths = dfs_dp(graph, nb, target, cache);
                for (List<Integer> path: subPaths) {
                    LinkedList<Integer> newPath = new LinkedList<>(path);
                    newPath.add(0, curNode);
                    paths.add(newPath);
                }
            }
        }
        cache.put(curNode, paths);
        return paths;
    }



    // 15 ms
    public List<List<Integer>> allPathsSourceTarget_iterative(int[][] graph) {
        List<List<Integer>> ans = new ArrayList<>();
        List<Integer> path = new ArrayList<>();
        int n = graph.length, source = 0, target=n-1;

        Stack<Integer> stack = new Stack<>();
        Stack<List<Integer>> pathStack = new Stack<>(); // can be improved later...

        stack.add(source);
        path.add(source);
        pathStack.add(path);

        while (!stack.isEmpty()) {
            int curNode = stack.pop();
            List<Integer> curPath = pathStack.pop();
            if (curNode == target) {
                ans.add(curPath);
            }

            for (int nb: graph[curNode]) {
                stack.push(nb);

                List<Integer> nextPath = new ArrayList<>();
                nextPath.addAll(curPath);
                nextPath.add(nb);
                pathStack.push(nextPath);
            }
        }

        return ans;
    }

    public List<List<Integer>> allPathsSourceTarget_recursion(int[][] graph) {
        List<List<Integer>> ans = new ArrayList<>();
        List<Integer> path = new ArrayList<>();
        int n = graph.length, source = 0, target=n-1;

        path.add(source);
        dfs(ans, path, graph, source, target);
        return ans;
    }

    private void dfs(List<List<Integer>> ans, List<Integer> path, int[][] gragh, int source, int target) {
        if (source==target) {
            ans.add(new ArrayList<>(path));
            return;
        }
        for (int neighbor: gragh[source]) {
            path.add(neighbor);
            dfs(ans, path, gragh, neighbor, target);
            path.remove(path.size()-1); // backtrack!!
        }
    }





    public static void main(String[] args) {
        int[][] graph = new int[][]{
                {1,2},
                {3},
                {3},
                {}};

//        int[][] graph = new int[][] {
//                {1},
//                {2},
//                {}
//        };

        System.out.println(new AllPaths().allPathsSourceTarget_dp(graph));

    }
}
