package graph_dfs_bfs.Topic_ListAllPaths;

import java.util.*;

/**
 * Created by thj on 2018/8/16.
 *
 *
 *   similar with AllPaths.java!!!
 *
 *
 *  2. dfs with cache (Top Down DP):  good!!!!!
 *  1. dfs:  will cause LTE
 *
 *
 */
public class WordBreakII {

    // use a cache to avoid recalculation!!!
    public List<String> wordBreak_dfs_cache(String s, List<String> wordDict) {
        return dfs_with_cache(s, new HashSet<>(wordDict), new HashMap<>());
    }

    private List<String> dfs_with_cache(String s, Set<String> dic, Map<String, List<String>> cache) {

        if (s.isEmpty()) return Arrays.asList("");

        if (cache.containsKey(s)) return cache.get(s);

        List<String> res = new ArrayList<>();
        for (int end = 1; end <= s.length(); end++) {
            String prefix = s.substring(0,end);
            if (dic.contains(prefix)) {
                List<String> subRes = dfs_with_cache(s.substring(end, s.length()), dic, cache);

                for (String suffix: subRes) {
                    res.add(prefix + (suffix.isEmpty()?"":(" "+ suffix)));
                }

            }
        }

        cache.put(s, res);
        return res;
    }

    // will cause LTE
    public List<String> wordBreak_dfs(String s, List<String> wordDict) {
        Set<String> dic = new HashSet<>(wordDict);
        List<String> res = new ArrayList<>();
        helper(res, new StringBuffer(), s, dic);
        return res;
    }

    private void helper(List<String> res, StringBuffer sb, String s, Set<String> dic) {

        if (s.isEmpty()) {
            res.add(sb.toString().trim());
            return;
        }

        for (int i = 1; i <= s.length(); i++) {
            String prefix = s.substring(0, i);
            if (dic.contains(prefix)) {
                sb.append(prefix).append(" ");

                helper(res, sb, s.substring(i, s.length()), dic);

                sb.delete(sb.length()-prefix.length()-1, sb.length()); // backtrack
            }
        }
    }

    public static void main(String[] args) {
        String s = "pineapplepenapple";
//        String s = "apple";
        List<String> wordDict = new ArrayList<>(Arrays.asList("apple", "pen", "applepen", "pine", "pineapple"));
        System.out.println(new WordBreakII().wordBreak_dfs_cache(s, wordDict));
    }


}
