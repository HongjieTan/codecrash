package graph_dfs_bfs.Topic_StronglyConnectedComponents;

import java.util.*;


// https://www.hackerearth.com/zh/practice/algorithms/graphs/articulation-points-and-bridges/tutorial/
class ArticulationPoints_And_Bridges {

    public static void main(String args[] ) throws Exception {
        /* Sample code to perform I/O:
         * Use either of these methods for input

        //BufferedReader
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String name = br.readLine();                // Reading input from STDIN
        System.out.println("Hi, " + name + ".");    // Writing output to STDOUT

        //Scanner
        Scanner s = new Scanner(System.in);
        String name = s.nextLine();                 // Reading input from STDIN
        System.out.println("Hi, " + name + ".");    // Writing output to STDOUT

        */

        // Write your code here
        Scanner s = new Scanner(System.in);
        // String name = s.nextLine();                 // Reading input from STDIN
        // System.out.println("Hi, " + name + ".");    // Writing output to STDOUT

        String[] arr = s.nextLine().split(" ");
        int n = Integer.valueOf(arr[0]), m = Integer.valueOf(arr[1]);
        int[][] edges = new int[m][2];
        for(int i=0;i<m;i++) {
            arr = s.nextLine().split(" ");
            edges[i][0] = Integer.valueOf(arr[0]);
            edges[i][1] = Integer.valueOf(arr[1]);
        }

        entry(n,m,edges);

        Collections.sort(points);
        Collections.sort(bridges, (a,b)->{
            if(a.get(0)==b.get(0)) return a.get(1)-b.get(1);
            return a.get(0)-b.get(0);
        });

        System.out.println(points.size());
        for(int p: points) System.out.println(p);

        System.out.println(bridges.size());
        for(List<Integer> e: bridges) System.out.println(e.get(0)+" "+e.get(1));
    }

    static int[] low;
    static int[] dist;
    static Map<Integer, List<Integer>> adj;
    static int time = 0;
    static List<Integer> points;
    static List<List<Integer>> bridges;

    static void entry(int n, int m, int[][] edges) {
        adj = new HashMap<>();
        for(int i=0;i<n;i++) adj.put(i, new ArrayList<>());
        for(int[] e:edges) {
            adj.get(e[0]).add(e[1]);
            adj.get(e[1]).add(e[0]);
        }

        low = new int[n];
        dist = new int[n];
        Arrays.fill(dist, -1);

        points = new ArrayList<>();
        bridges = new ArrayList<>();
        for(int i=0;i<n;i++) {
            if(dist[i]==-1) {
                dfs(i, -1);
            }
        }

    }

    static void dfs(int u, int parent) {
        low[u] = dist[u] = time++;

        int child = 0;
        for(int v: adj.get(u)) {
            if(v == parent) continue;
            if(dist[v]==-1) {
                child++;
                dfs(v, u);
                low[u] = Math.min(low[u], low[v]);

                // detect Bridges!
                if(low[v]>dist[u]) bridges.add(Arrays.asList(u,v));

                // detect Articulation Points!
                if(parent==-1 && child>1) points.add(u);
                if(parent!=-1 && low[v]>=dist[u]) points.add(u);


            } else {
                low[u] = Math.min(low[u], dist[v]);
            }
        }
    }



}



