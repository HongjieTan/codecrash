//AWS wants to increase the reliability of their network by upgrading crucial data center routers. Each data center has a single router that is connected to every other data center through a direct link or some other data center.
//
//To increase the fault tolerance of the network, AWS wants to identify routers which would result in a disconnected network if they went down and replace then with upgraded versions.
//
//Write an algorithm to identify all such routers.
//
//Input:
//
//The input to the function/method consists of three arguments:
//
//numRouters, an integer representing the number of routers in the data center.
//numLinks, an integer representing the number of links between the pair of routers.
//Links, the list of pair of integers - A, B representing a link between the routers A and B. The network will be connected.
//Output:
//
//Return a list of integers representing the routers with need to be connected to the network all the time.
//
//Example:
//
//Input:
//
//numRouters = 7
//numLinks = 7
//Links = [[0,1], [0, 2], [1, 3], [2, 3], [2, 5], [5, 6], [3,4]]
//
//Output:
//
//[2, 3, 5]
//
//Explanation:
//
//On disconnecting router 2, a packet may be routed either to the routers- 0, 1, 3, 4 or the routers - 5, 6, but not to all.
//
//On disconnecting router 3, a packet may be routed either to the routers - 0,1,2,5,6 or to the router - 4, but not to all.
//
//On disconnecting router 5, a packet may be routed either to the routers - 0,1,2,3,4 or to the router - 6, but not to all.

package graph_dfs_bfs.Topic_StronglyConnectedComponents;

import java.util.*;



public class CriticalRouter {

    Map<Integer, List<Integer>> adj;
    int[] low, dist;
    int time=0;
    List<Integer> ret;
    public List<Integer> getCriticalRouter(int n, int m, int[][] links) {
        low = new int[n];
        dist = new int[n];
        Arrays.fill(dist, -1);
        adj = new HashMap<>();
        for(int i=0;i<n;i++) adj.put(i, new ArrayList<>());
        for(int[] link: links) {
            adj.get(link[0]).add(link[1]);
            adj.get(link[1]).add(link[0]);
        }
        ret = new ArrayList<>();
        dfs(0, -1);
        return ret;
    }

    void dfs(int u, int parent) {
        dist[u]=low[u]=time++;

        int child = 0;
        for(int v: adj.get(u)) {
            if(dist[v]==-1) {
                child++;
                dfs(v, u);
                low[u] = Math.min(low[u], low[v]);
                if(parent==-1 && child>1) ret.add(u);
                if(parent!=-1 && low[v]>=dist[u]) ret.add(u);
            } else {
                low[u] = Math.min(low[u], dist[v]);
            }
        }
    }

    public static void main(String[] as) {
        int n = 7;
        int m = 7;
        int[][] links = {{0,1}, {0, 2}, {1, 3}, {2, 3}, {2, 5}, {5, 6}, {3,4}};
        System.out.println(new CriticalRouter().getCriticalRouter(n,m,links));
    }


}
