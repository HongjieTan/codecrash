//There are n servers numbered from 0 to n-1 connected by undirected server-to-server connections forming a network where connections[i] = [a, b] represents a connection between servers a and b.
// Any server can reach any other server directly or indirectly through the network.
//
// A critical connection is a connection that, if removed, will make some server unable to reach some other server.
//
// Return all critical connections in the network in any order.
//
//
// Example 1:
//
//
//
//
//Input: n = 4, connections = [[0,1],[1,2],[2,0],[1,3]]
//Output: [[1,3]]
//Explanation: [[3,1]] is also accepted.
//
//
//
// Constraints:
//
//
// 1 <= n <= 10^5
// n-1 <= connections.length <= 10^5
// connections[i][0] != connections[i][1]
// There are no repeated connections.
//

package graph_dfs_bfs.Topic_StronglyConnectedComponents;

import java.util.*;

// Tarjan algorithm...
public class CriticalConnections {
    int[] disc;
    int[] low;
    boolean[] seen;
    int time =0;
    List<List<Integer>> res;
    Map<Integer, List<Integer>> graph;
    public List<List<Integer>> criticalConnections(int n, List<List<Integer>> connections) {
        disc = new int[n];
        low = new int[n];
        seen = new boolean[n];
        graph = new HashMap<>();
        for(int i=0;i<n;i++) graph.put(i,new ArrayList<>());
        for (List<Integer> conn: connections) {
            graph.get(conn.get(0)).add(conn.get(1));
            graph.get(conn.get(1)).add(conn.get(0));
        }
        res = new ArrayList<>();

        for (int i=0;i<n;i++) {
            if (!seen[i]) {
                dfs(i, -1);
            }
        }
        return res;
    }

    void dfs(int u, int parent) {
        disc[u] = low[u] = time++;
        seen[u] = true;

        for(int v: graph.get(u)) {
            if(v==parent) continue;
            if(seen[v]) {
                low[u] = Math.min(low[u], disc[v]); // notice: disc[v]
            } else {
                dfs(v, u);
                low[u] = Math.min(low[u], low[v]);
                if(low[v]>disc[u]) { // notice: not low[v]>low[u]
                    // u - v is critical, there is no path for v to reach back to u or previous vertices of u
                    res.add(Arrays.asList(u, v));
                }
            }
        }
    }

    public static void main(String[] a) {
        List<List<Integer>> connections = new ArrayList<>();
        connections.add(Arrays.asList(0,1));
        connections.add(Arrays.asList(1,2));
        connections.add(Arrays.asList(2,0));
        connections.add(Arrays.asList(1,3));

        int n=4;
        List<List<Integer>> res = new CriticalConnections().criticalConnections(n, connections);
        System.out.println(res);
    }
}


/*
class Solution(object):
    def criticalConnections(self, n, connections):

        graph = [[] for _ in xrange(n)]
        for u, v in connections:
            graph[u].append(v)
            graph[v].append(u)

        seen = [False] * n
        time = [-1] * n
        low = [-1] * n
        t = [0]

        self.ans = []
        def dfs(node, parent = None):
            seen[node] = True
            time[node] = low[node] = t[0]
            t[0] += 1

            for nei in graph[node]:
                if nei == parent: continue
                if seen[nei]:
                    low[node] = min(low[node], time[nei])
                else:
                    dfs(nei, node)
                    low[node] = min(low[nei], low[node])
                    if low[nei] > time[node]:
                        self.ans.append([node, nei])

        for node, s in enumerate(seen):
            if not s:
                dfs(node)
        return self.ans
*/

/*
java version:
Basically, it uses dfs to travel through the graph to find if current vertex u, can travel back to u or previous vertex
low[u] records the lowest vertex u can reach
disc[u] records the time when u was discovered

public List<List<Integer>> criticalConnections(int n, List<List<Integer>> connections) {
	int[] disc = new int[n], low = new int[n];
	// use adjacency list instead of matrix will save some memory, adjmatrix will cause MLE
	List<Integer>[] graph = new ArrayList[n];
	List<List<Integer>> res = new ArrayList<>();
	Arrays.fill(disc, -1); // use disc to track if visited (disc[i] == -1)
	for (int i = 0; i < n; i++) {
		graph[i] = new ArrayList<>();
	}
	// build graph
	for (int i = 0; i < connections.size(); i++) {
		int from = connections.get(i).get(0), to = connections.get(i).get(1);
		graph[from].add(to);
		graph[to].add(from);
	}

	for (int i = 0; i < n; i++) {
		if (disc[i] == -1) {
			dfs(i, low, disc, graph, res, 0);
		}
	}
	return res;
}

int time = 0; // time when discover each vertex

private void dfs(int u, int[] low, int[] disc, List<Integer>[] graph, List<List<Integer>> res, int pre) {
	disc[u] = low[u] = ++time; // discover u
	for (int j = 0; j < graph[u].size(); j++) {
		int v = graph[u].get(j);
		if (v == pre) {
			continue; // if parent vertex, ignore
		}
		if (disc[v] == -1) { // if not discovered
			dfs(v, low, disc, graph, res, u);
			low[u] = Math.min(low[u], low[v]);
			if (low[v] > disc[u]) {
				// u - v is critical, there is no path for v to reach back to u or previous vertices of u
				res.add(Arrays.asList(u, v));
			}
		} else { // if v discovered and is not parent of u, update low[u], cannot use low[v] because u is not subtree of v
			low[u] = Math.min(low[u], disc[v]);
		}
	}
}
*/