package _Topic_LeastSteps;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

/**
 *  least moves -> bfs(level order)!!!
 *
 */
public class SlidingPuzzle {
    public int slidingPuzzle(int[][] board) {
        LinkedList<int[][]> que = new LinkedList<>();
        Set<String> visited = new HashSet<>();
        int[][] directions = new int[][]{{-1,0},{1,0},{0,-1},{0, 1}};
        que.add(board);
        visited.add(getKey(board));
        int depth = -1;
        while (!que.isEmpty()) {
            depth++;
            int levelSize = que.size();
            for (int i = 0; i < levelSize; i++) {
                int[][] curBoard = que.poll();
                if(getKey(curBoard).equals("123450")) return depth;
                visited.add(getKey(curBoard));

                for (int j = 0; j < 6; j++) {
                    int row = j/3, col=j%3;
                    if (curBoard[row][col]==0) {
                        for (int[] dir: directions) {
                            int nrow = row + dir[0], ncol = col + dir[1];
                            if (nrow>=0 && nrow<2 && ncol>=0 && ncol<3) {
                                swap(curBoard, row, col, nrow, ncol);
                                if (!visited.contains(getKey(curBoard))) que.add(clone(curBoard));
                                swap(curBoard, row, col, nrow, ncol);
                            }
                        }
                        break;
                    }
                }
            }
        }
        return -1;
    }

    private int[][] clone(int[][] board) {
        int[][] newboard = new int[board.length][board[0].length];
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[0].length; j++) {
                newboard[i][j] = board[i][j];
            }
        }
        return newboard;
    }

    private void swap(int[][] board, int x, int y, int nx, int ny) {
        int temp = board[x][y];
        board[x][y] = board[nx][ny];
        board[nx][ny] = temp;
    }

    private String getKey(int[][] board) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[0].length; j++) {
                sb.append(board[i][j]);
            }
        }
        return sb.toString();
    }


    public static void main(String[] args) {
        int[][] board = new int[][]{{1,2,3}, {4,0,5}};
        System.out.println(new SlidingPuzzle().slidingPuzzle(board));
    }
}
