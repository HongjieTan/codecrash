package _Topic_LeastSteps;

import java.util.Arrays;

/**
 *   greedy: https://leetcode.com/problems/task-scheduler/discuss/104496/concise-Java-Solution-O(N)-time-O(26)-space
 */
public class TaskScheduler {
    /*
        maxFreq: instances of max_freq task with   countOfMaxFreqTasks: count of max_freq tasks
        1. can not fill all slots: (maxFreq-1)*(n+1) + countOfMaxFreqTasks
        2. can fill all slots： tasks.length

        -> ans: Max of case 1,2
     */
    public int leastInterval(char[] tasks, int n) {
        int[] freq = new int[26];
        for(char c: tasks) freq[c-'A']++;
        Arrays.sort(freq);
        int maxFreq = freq[freq.length-1], maxFreqTasks = 0, i=freq.length-1;
        while (i>=0 && freq[i]==maxFreq) {maxFreqTasks++; i--;}
        return Math.max(tasks.length, (maxFreq-1)*(n+1)+maxFreqTasks);
    }


    // use priority que to simulate the process TODO
    public int leastInterval_v2(char[] tasks, int n) {
//        Map<Character, Integer> count = new HashMap<>();
//        for (char c: tasks) count.put(c, count.getOrDefault(c, 0)+1);
//        PriorityQueue<Pair<Character, Integer>> pq = new PriorityQueue<>((a,b)->Integer.compare(b.getValue(), a.getValue()));
//        for (char task: tasks) pq.add(new Pair<>(task, count.get(task)));
//
//        int res = 0;
//        while (!pq.isEmpty()) {
//            char task = pq.peek().getKey();
//            int taskCount = pq.peek().getValue();
//            pq.poll();
//            res += taskCount*(n+1)-n;
//            int fillCount = taskCount*n-n;
//            while (!pq.isEmpty() && fillCount>0) {
//                char ntask = pq.peek().getKey();
//                int ncount = pq.peek().getValue();
//                pq.poll();
//                if (fillCount-ncount>=0) fillCount-=ncount;
//                else pq.add(new Pair<>(ntask, ncount-fillCount));
//            }
//        }
//
//        return res;
        return -1;
    }

    public static void main(String[] args) {
        char[] tasks = new char[]{'A','A','A','B','B','B'};
        System.out.println(new TaskScheduler().leastInterval(tasks, 2));
    }
}
