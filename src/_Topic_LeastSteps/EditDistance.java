package _Topic_LeastSteps;

import java.util.*;

public class EditDistance {

    /*
        DP
        dp[i,j]:   min distance of w1[0,...,i-1] w2[0,...,j-1]

        i==0 && j==0,  dp[i,j]=0
        i=0 && j>0:  dp[i,j]=j
        i>0 && j==0: dp[i,j]=i
        i>0 && j>0:
            w1[i-1]==w2[j-1]: dp[i-1, j-1]
            w1[i-1]!=w2[j-1]: min(dp[i-1, j-1]+1, dp[i,j-1]+1, dp[i-1,j]+1)

     */
    public int minDistance(String word1, String word2) {
        int[][] dp = new int[word1.length()+1][word2.length()+1];
        for (int i = 0; i < word1.length() + 1; i++) {
            for (int j = 0; j < word2.length() + 1; j++) {
                if (i==0 && j==0) dp[i][j] = 0;
                else if(j==0) dp[i][j] = i;
                else if(i==0) dp[i][j] = j;
                else {
                    if (word1.charAt(i-1) == word2.charAt(j-1)) {
                        dp[i][j] = dp[i-1][j-1];
                    }else{
                        dp[i][j] = Math.min(dp[i-1][j-1]+1, Math.min( dp[i][j-1]+1, dp[i-1][j]+1));
                    }
                }
            }
        }
        return dp[word1.length()][word2.length()];
    }


    //  TLE! time is exponential...
    public int minDistance_naive(String word1, String word2) {
        Set<String> visited = new HashSet<>();

        LinkedList<String> que = new LinkedList<>();
        que.add(word1);
        visited.add(word1);
        int level = -1;
        while (!que.isEmpty()) {
            int levelSize = que.size();
            level++;
            for (int i = 0; i < levelSize; i++) {
                String curWord = que.poll();
                visited.add(curWord);
                System.out.println(curWord);
                if (curWord == word2) return level;
                for(String nb: getNbs(curWord)) {
                    if (!visited.contains(nb)) que.add(nb);
                }
            }
        }
        return -1;
    }

    private List<String> getNbs(String word) {
        List<String> nbs = new ArrayList<>();
        // insert
        for (int i = 0; i < word.length()+1; i++) {
            for (char c='a'; c<='z'; c++) {
                StringBuffer sb = new StringBuffer();
                sb.append(word.substring(0,i));
                sb.append(c);
                sb.append(word.substring(i));
                nbs.add(sb.toString());
            }
        }

        // update
        for (int i = 0; i < word.length(); i++) {
            for (char c='a'; c<='z'; c++) {
                if (word.charAt(i) == c) continue;
                StringBuffer sb = new StringBuffer();
                sb.append(word.substring(0,i));
                sb.append(c);
                sb.append(word.substring(i+1));
                nbs.add(sb.toString());
            }
        }

        // delete
        for (int i = 0; i < word.length(); i++) {
            StringBuffer sb = new StringBuffer();
            sb.append(word.substring(0,i));
            sb.append(word.substring(i+1));
            nbs.add(sb.toString());
        }
        return nbs;
    }


    public static void main(String[] args) {
//        "horse"
//        "ros"
        System.out.println(new EditDistance().minDistance("a", "a"));
    }



}
