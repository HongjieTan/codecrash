package _Topic_LeastSteps;

import java.util.*;

public class BusRoutes {

    public int numBusesToDestination_x2(int[][] routes, int S, int T) {
        if(S==T) return 0;
        int n = routes.length;
        List<Integer>[] adjList = new ArrayList[n];

        List<Integer> srcList= new ArrayList<>();
        Set<Integer> dstSet= new HashSet<>();


        for(int i=0;i<n;i++) {
            Set<Integer> set = new HashSet<>();
            for(int stop: routes[i]) {
                set.add(stop);
                if(stop==S) srcList.add(i);
                if(stop==T) dstSet.add(i);
            }

            List<Integer> nbs = new ArrayList<>();
            for(int j=0;j<n;j++) {
                if(i==j) continue;
                for(int stop: routes[j]) {
                    if(set.contains(stop)) {
                        nbs.add(j);
                        break;
                    }
                }
            }
            adjList[i] = nbs;
        }


        // bfs
        LinkedList<Integer> que = new LinkedList<>();
        int level=0;
        for(int stop: srcList) que.add(stop);
        Set<Integer> visited = new HashSet<>();
        while(!que.isEmpty()) {
            int size = que.size();

            level++;

            for(int i=0;i<size; i++) {
                int bus = que.remove();
                visited.add(bus);
                if(dstSet.contains(bus)) return level;

                for(int nb: adjList[bus]) {
                    if(!visited.contains(nb)) {
                        que.add(nb);
                    }
                }
            }

        }
        return -1;
    }

//    // the trick： treat each bus as node!
//
//    // n：bus num   ki: stops of bus i （k: max stops of each bus...）
//    public int numBusesToDestination(int[][] routes, int S, int T) {
//        if(S==T) return 0;
//        int busNum = routes.length;
//
//        // build bus2stops map: O(n * k)
//        Map<Integer, Set> bus2stops = new HashMap<>();
//        for (int i = 0; i < busNum; i++) {
//            Set<Integer> stops = new HashSet<>();
//            for (int j = 0; j < routes[i].length; j++) {
//                stops.add(routes[i][j]);
//            }
//            bus2stops.put(i, stops);
//        }
//
//        // build graph: O(n^2 * k)
//        List<List<Integer>> graph = new ArrayList<>();
//        for (int i = 0; i < busNum; i++) graph.add(new ArrayList<>());
//        for (int i = 0; i < busNum; i++) {
//            for (int j = i+1; j < busNum; j++) {
//                if (hasIntersection(bus2stops, i, j)) {
//                    graph.get(i).add(j);
//                    graph.get(j).add(i);
//                }
//            }
//        }
//
//        // bfs: O(n^2)
//        Set<Integer> dstBuses = new HashSet<>();
//        Set<Integer> srcBuses = new HashSet<>();
//        for (int i = 0; i < busNum; i++) {
//            if (bus2stops.get(i).contains(S)) srcBuses.add(i);
//            if (bus2stops.get(i).contains(T)) dstBuses.add(i);
//        }
//
//        boolean[] visited = new boolean[busNum];
//        LinkedList<int[]> que = new LinkedList<>();
//        for(int bus: srcBuses) que.add(new int[]{bus, 1}); // start bfs at SAME TIME!! who find the target first is the shortest path
//
//        while (!que.isEmpty()) {
//            int[] curBus = que.poll();
//            int curBusNo = curBus[0];
//            int level = curBus[1];
//            if (dstBuses.contains(curBusNo)) return level;
//            visited[curBusNo] = true;
//
//            for(int nb: graph.get(curBusNo)) {
//                if(!visited[nb]) que.add(new int[]{nb, level+1});
//            }
//        }
//
//        return -1;
//    }
//
//
//    // O(len(per bus stops))
//    private boolean hasIntersection(Map<Integer, Set> bus2stops, int i, int j) {
//        Set<Integer> stops1 = bus2stops.get(i);
//        Set<Integer> stops2 = bus2stops.get(j);
//        for(int stop: stops1) {
//            if(stops2.contains(stop)) return true;
//        }
//        return false;
//    }
//
//
//    public int numBusesToDestination_other(int[][] routes, int S, int T) {
//        HashSet<Integer> visited = new HashSet<>();
//        Queue<Integer> q = new LinkedList<>();
//        HashMap<Integer, ArrayList<Integer>> map = new HashMap<>();
//        int ret = 0;
//
//        if (S==T) return 0;
//
//        for(int i = 0; i < routes.length; i++){
//            for(int j = 0; j < routes[i].length; j++){
//                ArrayList<Integer> buses = map.getOrDefault(routes[i][j], new ArrayList<>());
//                buses.add(i);
//                map.put(routes[i][j], buses);
//            }
//        }
//
//        q.offer(S);
//        while (!q.isEmpty()) {
//            int len = q.size();
//            ret++;
//            for (int i = 0; i < len; i++) {
//                int cur = q.poll();
//                ArrayList<Integer> buses = map.get(cur);
//                for (int bus: buses) {
//                    if (visited.contains(bus)) continue;
//                    visited.add(bus);
//                    for (int j = 0; j < routes[bus].length; j++) {
//                        if (routes[bus][j] == T) return ret;
//                        q.offer(routes[bus][j]);
//                    }
//                }
//            }
//        }
//        return -1;
//    }


    public static void main(String[] args) {

        int[][] routes = new int[][]{
                {1,2,7},
                {3,6,7},
        };
        System.out.println(new BusRoutes().numBusesToDestination_x2(routes, 1, 6));
    }
}
