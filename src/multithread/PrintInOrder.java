//Suppose we have a class:
//
//
//public class Foo {
//  public void first() { print("first"); }
//  public void second() { print("second"); }
//  public void third() { print("third"); }
//}
//
//
// The same instance of Foo will be passed to three different threads. Thread A will call first(), thread B will call second(),
// and thread C will call third(). Design a mechanism and modify the program to ensure that second() is executed after first(),
// and third() is executed after second().
//
//
//
// Example 1:
//
//
//Input: [1,2,3]
//Output: "firstsecondthird"
//Explanation: There are three threads being fired asynchronously. The input [1,2,3] means thread A calls first(), thread B calls second(), and thread C calls third(). "firstsecondthird" is the correct output.
//
//
// Example 2:
//
//
//Input: [1,3,2]
//Output: "firstsecondthird"
//Explanation: The input [1,3,2] means thread A calls first(), thread B calls third(), and thread C calls second(). "firstsecondthird" is the correct output.
//
//
//
// Note:
//
// We do not know how the threads will be scheduled in the operating system, even though the numbers in the input seems to imply the ordering. The input format you see is mainly to ensure our tests' comprehensiveness.
//

package multithread;

import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class PrintInOrder {
}

/*
 */
class Foo {


    public Foo() {
        try {
            wait();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    public void first(Runnable printFirst) throws InterruptedException {
        // printFirst.run() outputs "first". Do not change or remove this line.
        printFirst.run();
    }

    public void second(Runnable printSecond) throws InterruptedException {
        // printSecond.run() outputs "second". Do not change or remove this line.
        printSecond.run();
    }

    public void third(Runnable printThird) throws InterruptedException {
        // printThird.run() outputs "third". Do not change or remove this line.
        printThird.run();
    }
}

/*
  Semaphore version

  Notice:
    we need "non-ownership-release" here, so ReentrantLock not work here!

  refs doc in Semaphore.java:
    When used in this way (binary semaphore way...),
    the binary semaphore has the property (unlike many {@link java.util.concurrent.locks.Lock}
    implementations), that the &quot;lock&quot; can be released by a
    thread other than the owner (as semaphores have no notion of ownership).
 */
class Foo_v1 {
    Semaphore permit2;
    Semaphore permit3;
    public Foo_v1() {
        permit2 = new Semaphore(0);
        permit3 = new Semaphore(0);
    }

    public void first(Runnable printFirst) throws InterruptedException {
        // printFirst.run() outputs "first". Do not change or remove this line.
        printFirst.run();
        permit2.release(1);

    }

    public void second(Runnable printSecond) throws InterruptedException {
        permit2.acquire();
        // printSecond.run() outputs "second". Do not change or remove this line.
        printSecond.run();
        permit3.release(1);
    }

    public void third(Runnable printThird) throws InterruptedException {
        permit3.acquire();
        // printThird.run() outputs "third". Do not change or remove this line.
        printThird.run();
    }
}

/*
by top post:
"Semaphore is a bowl of marbles" - Professor Stark

Semaphore is a bowl of marbles (or locks in this case).
If you need a marble, and there are none, you wait. You wait until there is one marble and then you take it.
If you release(), you will add one marble to the bowl (from thin air).
If you release(100), you will add 100 marbles to the bowl (from thin air).
The thread calling third() will wait until the end of second() when it releases a '3' marble.
The second() will wait until the end of first() when it releases a '2' marble.
Since first() never acquires anything, it will never wait. There is a forced wait ordering.
With semaphores, you can start out with 1 marble or 0 marbles or 100 marbles. A thread can take marbles (up until it's empty) or put many marbles (out of thin air) at a time.
Upvote and check out my other concurrency solutions.

import java.util.concurrent.*;
class Foo {
    Semaphore run2, run3;

    public Foo() {
        run2 = new Semaphore(0);
        run3 = new Semaphore(0);
    }

    public void first(Runnable printFirst) throws InterruptedException {
        printFirst.run();
        run2.release();
    }

    public void second(Runnable printSecond) throws InterruptedException {
        run2.acquire();
        printSecond.run();
        run3.release();
    }

    public void third(Runnable printThird) throws InterruptedException {
        run3.acquire();
        printThird.run();
    }
}



by second post, python version:
Raise two barriers. First one waits for all three threads to reach it. Second one waits for two threads to reach it.

First thread can print before reaching the first barrier. Second thread can print before reaching the second barrier.

from threading import Barrier

class Foo:
    def __init__(self):
        self.first_barrier = Barrier(3)
        self.second_barrier = Barrier(2)

    def first(self, printFirst):
        printFirst()
        self.first_barrier.wait()

    def second(self, printSecond):
        self.first_barrier.wait()
        printSecond()
        self.second_barrier.wait()

    def third(self, printThird):
        self.first_barrier.wait()
        self.second_barrier.wait()
        printThird()
Start with two locked locks. First thread unlocks the first lock that the second thread is waiting on. Second thread unlocks the second lock that the third thread is waiting on.

from threading import Lock

class Foo:
    def __init__(self):
        self.locks = (Lock(),Lock())
        self.locks[0].acquire()
        self.locks[1].acquire()

    def first(self, printFirst):
        printFirst()
        self.locks[0].release()

    def second(self, printSecond):
        with self.locks[0]:
            printSecond()
            self.locks[1].release()


    def third(self, printThird):
        with self.locks[1]:
            printThird()
Set events from first and second threads when they are done. Have the second thread wait for first one to set its event. Have the third thread wait on the second thread to raise its event.

from threading import Event

class Foo:
    def __init__(self):
        self.done = (Event(),Event())

    def first(self, printFirst):
        printFirst()
        self.done[0].set()

    def second(self, printSecond):
        self.done[0].wait()
        printSecond()
        self.done[1].set()

    def third(self, printThird):
        self.done[0].wait()
        self.done[1].wait()
        printThird()

Start with two closed gates represented by 0-value semaphores. Second and third thread are waiting behind these gates. When the first thread prints, it opens the gate for the second thread. When the second thread prints, it opens the gate for the third thread.

from threading import Semaphore

class Foo:
    def __init__(self):
        self.gates = (Semaphore(0),Semaphore(0))

    def first(self, printFirst):
        printFirst()
        self.gates[0].release()

    def second(self, printSecond):
        with self.gates[0]:
            printSecond()
            self.gates[1].release()

    def third(self, printThird):
        with self.gates[1]:
            printThird()

Have all three threads attempt to acquire an RLock via Condition. The first thread can always acquire a lock, while the other two have to wait for the order to be set to the right value. First thread sets the order after printing which signals for the second thread to run. Second thread does the same for the third.

from threading import Condition

class Foo:
    def __init__(self):
        self.exec_condition = Condition()
        self.order = 0
        self.first_finish = lambda: self.order == 1
        self.second_finish = lambda: self.order == 2

    def first(self, printFirst):
        with self.exec_condition:
            printFirst()
            self.order = 1
            self.exec_condition.notify(2)

    def second(self, printSecond):
        with self.exec_condition:
            self.exec_condition.wait_for(self.first_finish)
            printSecond()
            self.order = 2
            self.exec_condition.notify()

    def third(self, printThird):
        with self.exec_condition:
            self.exec_condition.wait_for(self.second_finish)
            printThird()

*/