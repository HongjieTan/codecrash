## "Reentrance":
 - In java, lock and synchronized are re-entrance aware: If a lock is held by a thread, and the thread tries to re-acquire the same lock, it is allowed.

## How to use mutex lock in java:
 - choices: 1. ReentrantLock(or synchronized), 2. binary Semaphore
 - difference:
    - If all you need is reentrant mutual exclusion, then a ReentrantLock is enough, there is no reason to use a binary semaphore over a ReentrantLock.
    - If for any reason you need non-ownership-release semantics then obviously semaphore is your only choice. (eg. PrintInOrder.java)

## lock, synchronized, synchronized vs lock
 - check doc in Lock.java, it's quite clear.

## wait notify notifyall
 - how is it used ?
 - what's the difference with lock/asynchronized ?



## countdownlatch
## volatile
## CyclicBarrier
## LinkedBlockingQueue
## countdownlatch
## semaphore
