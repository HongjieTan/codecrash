package heap;

import common.ListNode;

import java.util.List;
import java.util.PriorityQueue;

/**
 * Created by thj on 07/06/2018.
 *
 *
 *  heap version: O(nlogk)
 *  other: ...
 *
 *
 */
public class MergeKSortedList {
    public ListNode mergeKLists(List<ListNode> lists) {
        PriorityQueue<ListNode> heap = new PriorityQueue<>((a,b)->a.val-b.val);
        for (ListNode listNode: lists) {
            if (listNode!=null) {
                heap.add(listNode);
            }
        }
        ListNode start=null, node=null;
        while (heap.size()>0) {
            node = heap.poll();
            if (start==null) {start = node;}
            if (node.next!=null) {heap.add(node.next);}
            node.next = heap.size()==0?null:heap.peek();
        }

        return start;
    }
}
