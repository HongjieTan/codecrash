package heap;

import java.util.PriorityQueue;

/**
 * Created by thj on 31/05/2018.
 *
 *
 *  1. insertion sort:  add:O(n)
 *  2. two heap:        add:O(logn)
 *
 */
public class MedianFinder {
    /** initialize your data structure here. */

    private PriorityQueue<Integer> maxHeap;
    private PriorityQueue<Integer> minHeap;


    public MedianFinder() {
        minHeap = new PriorityQueue<>();
        maxHeap = new PriorityQueue<>((a,b) -> b-a);
    }

    public void addNum(int num) {

        if (minHeap.isEmpty() || num > minHeap.peek()) {
            minHeap.add(num);
        } else {
            maxHeap.add(num);
        }

        // balance
        if (maxHeap.size() - minHeap.size() > 0) {
            minHeap.add(maxHeap.poll());
        }
        if (minHeap.size() - maxHeap.size() > 1 ) {
            maxHeap.add(minHeap.poll());
        }
    }

    public double findMedian() {
        if (maxHeap.size() == minHeap.size()) {
            return (maxHeap.peek() + minHeap.peek()) * 0.5d;
        } else {
            return minHeap.peek();
        }

    }
}
