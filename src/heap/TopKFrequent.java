package heap;

import java.util.*;

/**
 * Created by thj on 30/05/2018.
 *
 *
 * 1. minheap: O(nlogk)/O(n)
 *
 * 2. bucketsort: O(n)/O(n)  best!!!!
 *     I call it InPlaceHash: map array index to frequency!!!
 *     Because freq is in range 0,1,2,...,n !!!!!!! All frequency problem can use this way!!!
 *
 * 3. quicksort: ...
 *
 *
 */
public class TopKFrequent {
    public List<Integer> topKFrequent(int[] nums, int k) {

        List<Integer> ans = new ArrayList<>();

        Map<Integer, Integer> countMap = new HashMap<>();
        for (int num: nums) {
            countMap.put(num, countMap.getOrDefault(num, 0)+1);
        }

        List<Integer>[] freq = new List[nums.length+1];

        for (Map.Entry<Integer, Integer> entry: countMap.entrySet()) {
            if (freq[entry.getValue()] == null) {
                freq[entry.getValue()] = new ArrayList<>();
            }
            freq[entry.getValue()].add(entry.getKey());
        }

        for (int i=freq.length-1; i>=1; i--) {
            List<Integer> keys = freq[i];
            if (keys!=null) {
                for (int key: keys) {
                    ans.add(key);
                    k--;
                    if (k==0) break;
                }
            }
            if (k==0) break;

        }

        return ans;


    }
}
