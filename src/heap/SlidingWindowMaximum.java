package heap;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by thj on 02/06/2018.
 *
 * 1. brute force: O(n*k)             (find max in window using linear search)
 *
 * 2. heap:        O(nlogk)           (find max in window using linear search)
 *      注意：此处java提供的heap(priorityqueue),删除（sliding window外的）结点比较困难，最好使用HashHeap，但java中没有原生支持
 *
 * 3. monotonic queue(based on dequeue): O(n)     (find max in window by maintaining a monotonic deque)
 *      注意：The algorithm is amortized(!!) O(n) as each element is put and polled once！！
 *
 *
 *
 * Conclusion: Sliding window minimum/maximum = monotonic queue
 *
 *
 * Monoqueue !!!：( all nodes in queue are both ordered in input sequence and value)
 *   It has three basic options:
 *    - push: push an element into the queue; O (1) (amortized!!!!!)
 *    - pop: pop an element out of the queue; O(1) (pop = remove, it can't report this element)
 *    - max: report the max element in queue;O(1)
 *   It takes only O(n) time to process a N-size sliding window minimum/maximum problem.
 *   Note: different from a priority queue (which takes O(nlogk) to solve this problem), it doesn't pop the max element: It pops the first element (in original order)
 */


// more detail check https://leetcode.com/problems/sliding-window-maximum/discuss/65885/This-is-a-typical-monotonic-queue-problem
class MonoQueue {

    class Node{
        int val;
        int deleteCount; // how many elements were deleted between it and the one before it.
        Node(int val, int deleteCount){this.val = val; this.deleteCount = deleteCount;}
    }

    private Deque<Node> deque;

    MonoQueue(){
        deque = new ArrayDeque<>();
    }

    public void push(int num){
        int count = 0;
        while (!deque.isEmpty() && num > deque.peekLast().val) {
            count += deque.peekLast().deleteCount+1;
            deque.pollLast();
        }
        deque.addLast(new Node(num, count));
    }

    public void pop() {
        if (deque.peekFirst().deleteCount>0) {
            deque.peekFirst().deleteCount--;
        } else {
            deque.pollFirst();
        }
    }

    public int max(){
        return deque.peekFirst().val;
    }

}

class MonoQueue_v1 {

    private Deque<Integer> monoQue;
    private Queue<Integer> numQue;

    MonoQueue_v1(){
        monoQue = new ArrayDeque<>();
        numQue = new LinkedList<>();
    }

    public void push(int num){
        numQue.add(num);
        while (!monoQue.isEmpty() && num > monoQue.peekLast()) {monoQue.pollLast();}
        monoQue.addLast(num);
    }

    public void pop() {
        if (numQue.peek() == monoQue.peekFirst()) {
            monoQue.pollFirst();
        }
        numQue.poll();
    }

    public int max(){
        return monoQue.peekFirst();
    }

}


public class SlidingWindowMaximum {
    public int[] maxSlidingWindow(int[] nums, int k) {
        if (nums.length == 0) return new int[0];
        int[] ans = new int[nums.length-k+1];
        MonoQueue monoqueue = new MonoQueue();

        for (int i=0;i<k-1;i++) {
            monoqueue.push(nums[i]);
        }

        for (int i=k-1;i<nums.length;i++) {
            monoqueue.push(nums[i]);
            ans[i-k+1] = monoqueue.max();
            monoqueue.pop();
        }
        return ans;
    }
}
