package heap;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.PriorityQueue;

public class CarFleet {

    class Car {
        int position;
        double time;
        public Car (int position, double time) {
            this.position = position;
            this.time = time;
        }
    }

    public int carFleet_x2(int target, int[] position, int[] speed) {
        if (position.length==0) return 0;
        List<Car> cars = new ArrayList<>();
        for(int i=0;i<position.length;i++) {
            cars.add(new Car(position[i], (target-position[i])*1d/speed[i]));
        }

        Collections.sort(cars, (a, b)->a.position-b.position);
        int fleets = 1;
        for(int i=cars.size()-1; i>0;i--) {
            if(cars.get(i).time<cars.get(i-1).time){
                fleets++;
            } else {
                cars.get(i-1).time = cars.get(i).time;
            }
        }
        return fleets;
    }

//    public int carFleet(int target, int[] position, int[] speed) {
//        PriorityQueue<Integer> pq = new PriorityQueue<>((a, b)->position[b]-position[a]);
//        for(int i=0;i<position.length;i++) pq.add(i);
//
//        int count=0;
//        double prevTime=-1;
//        while(!pq.isEmpty()) {
//            int cur = pq.remove();
//            double time = (target-position[cur])*1d/speed[cur];
//            if (prevTime<time) { // can not catch up prev car
//                count++;
//                prevTime=time;
//            }
//        }
//        return count;
//    }

}
