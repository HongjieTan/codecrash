package _Topic_SpecialDataStructure;

import java.util.*;

/**

 - brute force: O(n^2)

 - sweep line algorithm:
        time complexity depends on the data structure,
        O(nlogn) if we use heap with fast remove

 */
public class Skyline {

    /*
        Sweep Line Algorithm:
        '''
        events = {{x:L, height: H, type: entering},
                  {x:R, height: H, type: leaving}}
        events.SortByX() // edge case: when x is same, if entering,process higher first; if leaving. process lower first;

        ds = new DS()
        prevMax = 0
        for e in events:
            if e.type == entering:
                ds.add(e.height)
            if e.type == leaving:
                ds.remove(e.height)

            if ds.max() != prevMax:  // target: maxHeight changing point!
                ans += ds.max()
                prevMax = ds.max()
        '''
        DS Candidate:
        - Heap: max O(1), add O(logn), removeByKey  O(n) using PriorityQue or O(logn) using customized heap
        - TreeMap/TreeSet: max O(logn),  add(logn), remove O(logn)
     */
    public List<int[]> getSkyline(int[][] buildings) {
        List<int[]> ans = new ArrayList<>();
        List<int[]> events = new ArrayList<>();
        for (int[] b: buildings) {
            events.add(new int[]{b[0], -b[2]});
            events.add(new int[]{b[1], b[2]});
        }

        // notice edge case: when x is same, if entering,process higher first; if leaving, process lower first;
        Collections.sort(events, (a, b)-> a[0]==b[0]?a[1]-b[1]:a[0]-b[0]);

        TreeMap<Integer, Integer> heights = new TreeMap<>(); // height->count
        heights.put(0, 1);
        int prevMax = 0;
        for (int[] e: events) {
            int x = e[0], h = e[1];
            if (h < 0) { // entering
                heights.put(-h, heights.getOrDefault(-h,0)+1);
            } else { // leaving
                heights.put(h, heights.get(h)-1);
                if (heights.get(h)==0) heights.remove(h);
            }

            int curMax = heights.lastKey();
            if (prevMax!=curMax) { // target point: maxHeight changes !!!!!
                ans.add(new int[]{x, curMax});
                prevMax = curMax;
            }
        }
        return ans;
    }

    public static void main(String[] args) {
        int[][] buildings = new int[][] {
                {2,9,10},
                {3,7,15},
                {5,12,12},
                {15,20,10},
                {19,24,8}
        };

        List<int[]> res = new Skyline().getSkyline(buildings);

        System.out.println("adsf");


    }
}
