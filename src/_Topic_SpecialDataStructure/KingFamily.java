package _Topic_SpecialDataStructure;

import java.util.*;

/**
 *  discussion: https://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=491451
 *
 *
 *  - brute force:  tree + hashmap,  birth:O(1), death:O(1), getOrder: O(n)
 *  - improved:     (tree + DoubleLinkedList) + hashmap,   birth:O(logn), death:O(1), getOrder: O(1)
 */
public class KingFamily {
    class DLNode {
        String name;
        List<DLNode> children;
        DLNode prev, next;
        boolean isDead;
        public DLNode(String name){
            this.children = new ArrayList<>();
            this.name = name;
        }
    }

    Map<String, DLNode> map;
    DLNode head, tail;

    public KingFamily(String rootName){
        head = new DLNode(rootName);
        map = new HashMap<>();
        map.put(rootName, head);
    }

    // update DLinkedList, 需要快速找到其preorder的前一个node(即parent的right most descendant),时间为O(logn), 但是删除时不能删掉DLinkedList中的死了的结点，不然parent的right most descendant死了就定位不到了(否则需要O(n)遍历来找...)
    void birth(String parent, String name) {
        DLNode pNode =  map.get(parent);
        DLNode cNode = new DLNode(name);
        map.put(name, cNode);

        DLNode tail = pNode;
        while(pNode.children.size()>0) tail=pNode.children.get(pNode.children.size()-1); // move to right most descendant

        DLNode temp = tail.next;
        tail.next = cNode;
        cNode.prev = tail;
        cNode.next = temp;
        if(temp!=null) temp.prev = cNode;

        pNode.children.add(cNode);
    }

    void death(String name) { // 注意这里不实际删除DoubleLinkedList里的结点...
        map.get(name).isDead = true;
        map.remove(name);
    }

    List<String> getOrder() {
        List<String> ans = new ArrayList<>();
        DLNode cur = head;
        while(cur!=null) {
            if(!cur.isDead) ans.add(cur.name);
            cur = cur.next;
        }
        return ans;
    }
}
/*
void birth(String parent, String name) 父亲名字和孩子名字，生个娃
void death(String name) 此人要死
List<String> getOrder() 返回当前的继承顺序，string array/list

讨论得知，每个人的名字是唯一的，继承顺序符合如下规律:
假设王有大皇子二皇子三皇子，大皇子有长子次子三子，那么继承顺序是王->大皇子->大皇子长子->大皇子次子->大皇子三子->二皇子->三皇子
死掉的人不能出现在继承顺序里，但是如果上面例子中大皇子死了，只需把大皇子移除，原始继承顺序保持不变：王->大皇子长子->大皇子次子->大皇子三子->二皇子->三皇子

三个function会被反复调用，实现function细节。

* 我用了 tree 来表示，没有用面经常用的 map。后面 follow up 做优化，我提到可以 pre-compute 继承的 order，用改良版 linkedlist 保存，每次调用 birth 和 death function 的时候可以O(1) update
  这一轮的 feedback 是最好的，我的感想是看面经可以知己知彼百战不殆，但是如果被面经束缚就不好了。解题有自己的思路和风格也可以在简单的面经轮脱颖而出

* 用hashmap + dlinkedlist ?  How?

* one discussion: (tan: improved version, O(1)update, O(1)query !!)
 我用了 tree 来表示，没有用面经常用的 map。后面 follow up 做优化，我提到可以 pre-compute 继承的 order，用改良版 linkedlist 保存，每次调用 birth 和 death function 的时候可以O(1) update。
 这一轮的 feedback 是最好的，我的感想是看面经可以知己知彼百战不殆，但是如果被面经束缚就不好了。解题有自己的思路和风格也可以在简单的面经轮脱颖而出

* one discussion: （tan: O(1)update, O(n)query）
// key: parent, value: children
Map<String, List<String>> tree = new HashMap<>();
Set<String> isDead = new HashSet<>();
String root = "king";
{
	tree.put("king", new ArrayList<>());
}

public void birth(String parent, String name) {
	if(!tree.containsKey(parent)) {
	// throw exception
} else {
	tree.get(parent).add(name);
	tree.put(name, new ArrayList<>());
}
}

public void death(String name) {
	isDead.add(name);
}
Zelong Qiu: 这个方程只是lazy delete会不会导致dead越存越大？面试官会不会问如果需要实际删除发方法
根据看到的面经，还没有人被遇到过这个问题，如果有人遇到需要实际删除的问题，欢迎提供思路和代码

Provider: Yang Qi
对于动态删，提供一个思路，用一个map记录子节点到父节点的映射，如果当前节点挂掉，把自己的子节点插入的父节点自身对应的位置之后

public List<String> getOrder(){
	List<String> res = new ArrayList<>();
	dfs(root, res);
	return res;
}

private void dfs(String curr, List<String> res) {
	if(!isDead.contains(curr)) {
	res.add(curr);
}
	for(String child : tree.get(curr)) {
		dfs(child, res);
}
}

 */