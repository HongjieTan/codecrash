/*
Implement a SnapshotArray that supports the following interface:

SnapshotArray(int length) initializes an array-like data structure with the given length.  Initially, each element equals 0.
void set(index, val) sets the element at the given index to be equal to val.
int snap() takes a snapshot of the array and returns the snap_id: the total number of times we called snap() minus 1.
int get(index, snap_id) returns the value at the given index, at the time we took the snapshot with the given snap_id


Example 1:

Input: ["SnapshotArray","set","snap","set","get"]
[[3],[0,5],[],[0,6],[0,0]]
Output: [null,null,0,null,5]
Explanation:
SnapshotArray snapshotArr = new SnapshotArray(3); // set the length to be 3
snapshotArr.set(0,5);  // Set array[0] = 5
snapshotArr.snap();  // Take a snapshot, return snap_id = 0
snapshotArr.set(0,6);
snapshotArr.get(0,0);  // Get the value of array[0] with snap_id = 0, return 5


Constraints:

1 <= length <= 50000
At most 50000 calls will be made to set, snap, and get.
0 <= index < length
0 <= snap_id < (the total number of times we call snap())
0 <= val <= 10^9

*/

package _Topic_SpecialDataStructure;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 * Your SnapshotArray object will be instantiated and called as such:
 * SnapshotArray obj = new SnapshotArray(length);
 * obj.set(index,val);
 * int param_2 = obj.snap();
 * int param_3 = obj.get(index,snap_id);
 */
public class SnapshotArray {
    int time = 0;
    TreeMap<Integer, Integer>[] arr;

    public SnapshotArray(int length) {
        arr = new TreeMap[length];
        for(int i=0;i<length;i++) {
            arr[i] = new TreeMap<>();
            arr[i].put(0,0);
        }
    }

    public void set(int index, int val) {
        arr[index].put(time, val);
    }

    public int snap() {
        return time++;
    }

    public int get(int index, int snap_id) {
        int key = arr[index].floorKey(snap_id);
        return arr[index].get(key);
    }
}


/*
by uwi:
class SnapshotArray {

    List<List<Integer>> times;
    List<List<Integer>> values;
    int time;

    public SnapshotArray(int n) {
        time = 0;
        times = new ArrayList<List<Integer>>();
        values = new ArrayList<List<Integer>>();
        for(int i = 0;i < n;i++){
            times.add(new ArrayList<Integer>());
            times.get(i).add(0);
            values.add(new ArrayList<Integer>());
            values.get(i).add(0);
        }
    }

    public void set(int index, int val) {
        if(times.get(index).get(times.get(index).size()-1).equals(time)){
            values.get(index).set(values.get(index).size()-1, val);
        }else{
            times.get(index).add(time);
            values.get(index).add(val);
        }
    }

    public int snap() {
        return time++;
    }

    public int get(int index, int snap_id) {
        int ind = Collections.binarySearch(times.get(index), snap_id);
        if(ind < 0)ind = -ind-2;
        return values.get(index).get(ind);
    }
}

by lee:
Intuition
Instead of copy the whole array,
we can only record the changes of set.


Explanation
The idea is, the whole array can be large,
and we may take the snap tons of times.
(Like you may always ctrl + S twice)

Instead of record the history of the whole array,
we will record the history of each cell.
And this is the minimum space that we need to record all information.

For each A[i], we will record its history.
With a snap_id and a its value.

When we want to get the value in history, just binary search the time point.


Complexity
Time O(logS)
Space O(S)
where S is the number of set called.


Python:

class SnapshotArray(object):

    def __init__(self, n):
        self.A = [[[-1, 0]] for _ in xrange(n)]
        self.snap_id = 0
        self.n = n

    def set(self, index, val):
        self.A[index].append([self.snap_id, val])

    def snap(self):
        self.snap_id += 1
        return self.snap_id - 1

    def get(self, index, snap_id):
        i = bisect.bisect(self.A[index], [snap_id + 1]) - 1
        return self.A[index][i][1]

*/

// other similar problems:

// LC 981
class TimeBasedKeyValueStore {
    Map<String, TreeMap<Integer, String>> map;

    public TimeBasedKeyValueStore() {
        map = new HashMap<>();
    }

    public void set(String key, String value, int timestamp) {
        if (!map.containsKey(key)) map.put(key, new TreeMap<>());
        map.get(key).put(timestamp, value);
    }

    public String get(String key, int timestamp) {
        if (!map.containsKey(key)) return "";
        Integer found = map.get(key).floorKey(timestamp);
        return found==null?"":map.get(key).get(found);
    }
}

/*
Google Snap （高频） (类似LC 981)
第二轮是面经高频题，Google GoogleSnapshot
就是一个array，然后这个array有不同的版本数，然后有不同的函数，比如set(int index, int val)，get(int index, int version), snapshot()。
比如说我先set(1,1)，set(0,0)，set(2,2)，这样我有一个[0,1,2]，然后snapshot()之后系统里会记住version 1的array是[0,1,2]，
然后set(1,0)，array就变成[0,0,2]，这个时候如果get(1,1)得到的是1，但get(1,2)得到的是0。
思路：
1. 模拟过程，用map存下snapshot即可,主要要和面试官交流得到各种异常的处理方式
包括数组长度是否固定，idx是否一定合法，version不存在是返回最近的还是抛出异常等等
2. hash map嵌套treemap，牺牲了一些时间，但是如果一些idx在某些version一直没用过可以节省空间

code：
Provider: Xing
*/
class GoogleSnapshot {
    int version = 1;
    int length;
    Map<Integer, TreeMap<Integer, Integer>> map = new HashMap<>();
    public GoogleSnapshot(int length) {
        this.length = length;
        for(int i = 0 ; i < length ; i++) {
            map.put(i, new TreeMap<>());
        }
    }
    public void set(int idx, int val) {
        // sanity check and exception process
        map.get(idx).put(version, val);
    }
    public int get(int idx, int version) {
        // sanity check and exception process
        Integer key = map.get(idx).floorKey(version);
        if(key == null) return Integer.MIN_VALUE;
        return map.get(idx).get(key);
    }
    public void snapshot() {
        version++;
    }
}