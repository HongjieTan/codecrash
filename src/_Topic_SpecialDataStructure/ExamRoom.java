package _Topic_SpecialDataStructure;

import java.util.*;


/**
   The key is how to choose a suitable DS (ds.add(), ds.max(), ds.remove(item))
   DS options:
        PQ(good!):       add O(logN), max O(1), remove O(N)
        TreeMap(good!!): add O(logN), max O(logN), remove O(logN)
        Array:           add O(1), max O(N) remove O(1)

 */

// use TreeMap, seat: O(logn), leave O(logn)
class ExamRoom_TreeMap {

    TreeSet<Interval> inservalSet;

    // use two hashmap just to find interval(to remove) fast, instead of linear find...
    Map<Integer, Interval> startMap;
    Map<Integer, Interval> endMap;
    int n;

    class Interval {
        int start, end, dist;
        public Interval(int start, int end) {
            this.start = start;
            this.end = end;
            if (start==-1) this.dist=end;
            else if (end==n) this.dist=n-1-start;
            else this.dist=(start+end)/2-start;
        }
    }

    public ExamRoom_TreeMap(int N) {
        n=N;
        inservalSet =new TreeSet<>((a, b)->{
            if (a.dist!=b.dist) return b.dist-a.dist;
            return a.start-b.start;
        });
        startMap = new HashMap<>();
        endMap = new HashMap<>();

        inservalSet.add(new Interval(-1,n));
    }

    public int seat() {  // O(lognN)
        int seat;
        Interval curMax = inservalSet.pollFirst();
        if (curMax.start==-1) {
            seat = 0;
            Interval itv = new Interval(seat, curMax.end);
            inservalSet.add(itv);
            startMap.put(itv.start, itv);
            endMap.put(itv.end, itv);
        } else if (curMax.end==n) {
            seat = n-1;
            Interval itv = new Interval(curMax.start, seat);
            inservalSet.add(itv);
            startMap.put(itv.start, itv);
            endMap.put(itv.end, itv);
        } else {
            seat = (curMax.start+curMax.end)/2;
            Interval itv1 = new Interval(curMax.start, seat);
            Interval itv2 = new Interval(seat, curMax.end);
            inservalSet.add(itv1);
            inservalSet.add(itv2);
            startMap.put(itv1.start, itv1);
            endMap.put(itv1.end, itv1);
            startMap.put(itv2.start, itv2);
            endMap.put(itv2.end, itv2);
        }
        return seat;
    }

    public void leave(int p) { // O(lognN)
        int start=-1, end=n;

        if (startMap.containsKey(p)) {
            end = startMap.get(p).end;
            inservalSet.remove(startMap.get(p));
            startMap.remove(p);
        }
        if (endMap.containsKey(p)) {
            start = endMap.get(p).start;
            inservalSet.remove(endMap.get(p));
            endMap.remove(p);
        }

        Interval nItv = new Interval(start, end);
        inservalSet.add(nItv);
        startMap.put(nItv.start, nItv);
        endMap.put(nItv.end, nItv);
    }

}


// use PQ,  seat: O(logn), leave O(n)
class ExamRoom_PQ {
    PriorityQueue<Interval> pq;
    int n;

    class Interval {
        int start, end, dist;
        public Interval(int start, int end) {
            this.start = start;
            this.end = end;
            if (start==-1) this.dist=end;
            else if (end==n) this.dist=n-1-start;
            else this.dist=(start+end)/2-start;
        }
    }

    public ExamRoom_PQ(int N) {
        n=N;
        pq = new PriorityQueue<>((a,b)-> {
           if (a.dist != b.dist) return Integer.compare(b.dist, a.dist);
           return Integer.compare(a.start, b.start);
        });
        pq.add(new Interval(-1,n));
    }

    public int seat() { // O(logn)
        int seat=0;
        Interval curMax = pq.remove();
        if(curMax.start==-1) {
            seat = 0;
            pq.add(new Interval(seat, curMax.end));
        } else if(curMax.end==n) {
            seat = n-1;
            pq.add(new Interval(curMax.start, seat));
        } else {
            seat = curMax.start + (curMax.end-curMax.start)/2;
            pq.add(new Interval(curMax.start, seat));
            pq.add(new Interval(seat, curMax.end));
        }
        return seat;
    }

    public void leave(int p) { // O(n)
        int start=-1, end=n;
        Iterator<Interval> it = pq.iterator();
        while(it.hasNext()) {
            Interval cur = it.next();
            if(cur.start==p) {
                end=cur.end;
                it.remove(); // notice the usage of Iterator.remove()
            }
            if(cur.end==p) {
                start=cur.start;
                it.remove();
            }
        }
        pq.add(new Interval(start, end));
    }

    public static void main(String[] args) {
//        _Topic_SpecialDataStructure.ExamRoom_PQ room = new _Topic_SpecialDataStructure.ExamRoom_PQ(10);
        ExamRoom_TreeMap room = new ExamRoom_TreeMap(10);
        System.out.println(room.seat());
        System.out.println(room.seat());
        System.out.println(room.seat());
        System.out.println(room.seat());
//        System.out.println(room.seat());
//        System.out.println(room.seat());
//        System.out.println(room.seat());
//        System.out.println(room.seat());
//        System.out.println(room.seat());
//        System.out.println(room.seat());
//        room.leave(0);
        room.leave(4);
//        System.out.println(room.seat());
        System.out.println(room.seat());

    }
}




/**
 * Your ExamRoom object will be instantiated and called as such:
 * ExamRoom obj = new ExamRoom(N);
 * int param_1 = obj.seat();
 * obj.leave(p);
 */
/*
In an exam room, there are N seats in a single row, numbered 0, 1, 2, ..., N-1.

When a student enters the room, they must sit in the seat that maximizes the distance to the closest person.
If there are multiple such seats, they sit in the seat with the lowest number.
(Also, if no one is in the room, then the student sits at seat number 0.)

Return a class ExamRoom(int N) that exposes two functions: ExamRoom.seat()
returning an int representing what seat the student sat in, and ExamRoom.leave(int p)
representing that the student in seat number p now leaves the room.
It is guaranteed that any calls to ExamRoom.leave(p) have a student sitting in seat p.



Example 1:

Input: ["ExamRoom","seat","seat","seat","seat","leave","seat"], [[10],[],[],[],[],[4],[]]
Output: [null,0,9,4,2,null,5]
Explanation:
ExamRoom(10) -> null
seat() -> 0, no one is in the room, then the student sits at seat number 0.
seat() -> 9, the student sits at the last seat number 9.
seat() -> 4, the student sits at the last seat number 4.
seat() -> 2, the student sits at the last seat number 2.
leave(4) -> null
seat() -> 5, the student sits at the last seat number 5.
​​​​​​​

Note:

1 <= N <= 10^9
ExamRoom.seat() and ExamRoom.leave() will be called at most 10^4 times across all test cases.
Calls to ExamRoom.leave(p) are guaranteed to have a student currently sitting in seat number p.
*/