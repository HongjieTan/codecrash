package _Topic_SpecialDataStructure;

/**
 *
 *  refs: https://leetcode.com/discuss/interview-question/178662/Design-a-Hit-Counter/
 *
 *  1. brute force:
 *      - store all hits in queue, O(1) hit,  O(n) getHits
 *      - space improved: remove old hits in head of queue
 *
 *  2. Most optimized solution: (best in single machine!!!! good!!)
 *      - 思路：类似于heap求第k大元素一样，5 minutes = 300 seconds, 始终维护一个300大小数组记录hits，300秒之前大会被后面的顶掉。。。
 *      - time: hit O(1) , getHits O(1), space O(1)
 *      - 对于不按时间顺序的情况也适用！！！
 *
 *  3. How to handle concurrent requests:
 *      - use lock...
 *      - Placing a lock can be costly at some times and when there are too many concurrent requests -> Distribute the counter
 *
 *  4. Distribute the counter:
 *      - Hash the userID to assign them to different hosts.
 *        Add load balancer on top to make sure requests gets allocated evenly.
 *        On each individual machine, take the approach of single machine version(see solution-2) to gather the counts.
 *        Upon reading, sum up the count across all machines.
 *        For a read-heavy application, put a cache on top so that multiple read requests that take place in the same second does not incur unnecessary cross-node communications.
 *
 */
public class HitCounter {

    int[] counters;
    int[] times;

    public HitCounter(){
        counters = new int[300]; // 5 mins = 300 seconds
        times = new int[300];
    }

    public void hit(int timestamp){
        int idx = timestamp%300;
        if (times[idx]!=timestamp) {
            counters[idx]=1;
            times[idx]=timestamp;
        } else {
            ++counters[idx];
        }
    }

    public int getHits(int timestamp){
        int sum=0;
        for (int i = 0; i < 300; i++) {
            if (timestamp - times[i] < 300) { // 注意这里别忘了check...
                sum+=counters[i];
            }
        }
        return sum;
    }

}

/*
Design a hit counter which counts the number of hits received in the past 5 minutes.

Each function accepts a timestamp parameter (in seconds granularity) and
you may assume that calls are being made to the system in chronological order (ie, the timestamp is monotonically increasing).
You may assume that the earliest timestamp starts at 1.

It is possible that several hits arrive roughly at the same time.

Example:
_Topic_SpecialDataStructure.HitCounter counter = new _Topic_SpecialDataStructure.HitCounter();

// hit at timestamp 1.
counter.hit(1);

// hit at timestamp 2.
counter.hit(2);

// hit at timestamp 3.
counter.hit(3);

// get hits at timestamp 4, should return 3.
counter.getHits(4);

// hit at timestamp 300.
counter.hit(300);

// get hits at timestamp 300, should return 4.
counter.getHits(300);

// get hits at timestamp 301, should return 3.
counter.getHits(301);

Follow-up1: Due to latency, several hits arrive roughly at the same time and the order of timestamps is not guaranteed chronological.

Follow-up2: What if the number of hits per second could be very large? Does the design scale?

Credits:
Special thanks to @elmirap for adding this problem and creating all test cases.s
*/