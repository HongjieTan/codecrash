package _Topic_SpecialDataStructure;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


public class HashMapWithExpiration<K, V> {

    Map<K, V> valueMap;
    Map<K, Long> timeMap;

    public HashMapWithExpiration(){
        valueMap = new HashMap<>();
        timeMap = new HashMap<>();
    }

    public void put(K key, V value, long duration) {
        valueMap.put(key, value);
        timeMap.put(key, System.currentTimeMillis()+duration);
    }

    public V get(K key) {
        if (!timeMap.containsKey(key) || System.currentTimeMillis()> timeMap.get(key)) { // lazy delete here...
            valueMap.remove(key);
            timeMap.remove(key);
            return null;
        }
        return valueMap.get(key);
    }
}

//    Follow up: 采用更主动的策略删除过期的Key
//        思路；创建后台线程定期清理过期的Key。
class HashMapWithExpirationV2<K, V> {
    Map<K,V> valueMap;
    Map<K,Long> timeMap;
    Thread cleanThread;

    public HashMapWithExpirationV2() {
        valueMap = new ConcurrentHashMap<>();
        timeMap = new ConcurrentHashMap<>();
        cleanThread = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    try {
                        Thread.sleep(10000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    for (K key: valueMap.keySet()) valueMap.get(key);
                }
            }
        });
        cleanThread.start();  // map对象被回收时，该线程是否会被回收？需要确认下。。。
    }

    public void put(K key, V value, long duration) {
        valueMap.put(key, value);
        timeMap.put(key, System.currentTimeMillis()+duration);
    }

    public V get(K key) { // 用ConcurrentHashMap保证线程安全，或者用synchronized关键字
        if (timeMap.containsKey(key)) return null;
        if (timeMap.get(key)<System.currentTimeMillis()) {
            valueMap.remove(key);
            timeMap.remove(key);
            return null;
        }
        return valueMap.get(key);
    }


}


/*
Create a count with expiring entries:
Example
12:00:00 - put(10, 25, 5000)
12:00:04 - get(10) -> 25
12:00:06 - get(10) -> null

思路：两个hash count，一个记录key，value pair，一个记录key的过期时间，get的时候检查key是否过期，如果过期了，删除key返回null
Put方法有三个参数，除了key，value还有个duration


Follow up: 采用更主动的策略删除过期的Key
思路；创建后台线程定期清理过期的Key。
用两个map，一个装<key, value>一个装<key, expiredTime>
在get中采用lazy deletion，get的时候检查key是否过期，如果过期的话两个map中都删除key，返回null。put的时候每次都更新key的expiredTime。
后台线程每过一段时间遍历所有key，调用get方法删除过期key。此处为了避免多线程冲突，Map用ConcurrentHashMap实现。
参考代码  （用后台线程主动删除）
Provider: Xing
class MyMap<K, V> {
	Map<K, V> count;
	Map<K, Long> time;
	private static final int DEFAULT_CAPACITY = 16;
	private static final float DEFAULT_LOAD_FACTOR = 0.75f;
	private Thread clearThread = new Thread(new Runnable() {
		@Override
		public void run() {
			while(true) {
				try {
					Thread.sleep(5000);
				}catch(Exception e) {
					e.printStackTrace();
				}
				for(K key : count.keySet()) get(key);
			}
		}

	});
	public MyMap() {
		this(DEFAULT_CAPACITY, DEFAULT_LOAD_FACTOR);
	}
	public MyMap(int capacity) {
		this(capacity, DEFAULT_LOAD_FACTOR);
	}
	public MyMap(int capacity, float loadFactor) {
		count = new ConcurrentHashMap<>(capacity, loadFactor);
		time = new ConcurrentHashMap<>(capacity, loadFactor);
		clearThread.start();
	}
	public synchronized V get(K key) {  //  synchronized  和 ConcurrentHashMap 二选一
		long now = System.currentTimeMillis();
		Long expired = time.get(key);
		if(expired == null) return null;
		if(Double.compare(now, expired) > 0) {
			count.remove(key);
			time.remove(key);
			return null;
		} else {
			return count.get(key);
		}
	}
	public V put(K key, V value, long duration) {
		long now = System.currentTimeMillis();
		long expired = now + duration;
		time.put(key, expired);
		return count.put(key, value);
	}
}


*/