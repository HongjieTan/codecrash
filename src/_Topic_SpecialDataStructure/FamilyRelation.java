package _Topic_SpecialDataStructure;

/*
lowest common ancestor using BFS
 */
public class FamilyRelation {
    class Node {
        int val;
        Node father;
        Node mother;
    }
    public boolean isRelated(Node p1, Node p2){
        // Skip.. find lowest common ancestor using BFS
        return false;
    }
}

/*
家庭树判断亲属关系

Desc v1 :
第二轮：自己定义数据结构，找出两个人是否是亲戚，follow up是如何测试。

Desc v2:
第二轮：
输入一些 parent-child pairs, 然后给一个pair, 判断他们是否genetically related. 比如(p1, c1)表示p1生了c1, c1有p1的gene. 输入(p1, c1), (p2, c1), (p1, c2), (p2, c2).
 画图可知(c1, c2)是related, (p1, p2)不是related. 面试官给了一个无向图, 结点就是p1, p2, c1, c2, 根据输入把边加上, 然后我就一直按无向图来做了, 觉得从一点dfs遍历能看到另一个点就行了.
 其实这应该是有向图. 如果是无向图, p1通过c1可以到达p2, 但其实不应该. 有向图就不会有问题. 后来发现这其实很像树, 这题就是要找common ancestor, 如果有就是related.
 把一个点的所有ancestor放到一个set里, 另一个点的放到另一个set里, 有交集就代表related. 这题最大的失误就是想复杂了, 想成general graph了, 其实根据遗传的知识来想要简单一些, 更像是树.
 代码写出来了, 面试官说有bug, 但我觉得不是. 由于下一个面试官已经在外面等着了, 就没继续解释了. 事后想想应该说明白, 也就一两分钟的事儿, 面过试的应该都能理解. 最好还是不要有bug吧?

Family Tree
在family tree中找两个人是否是亲戚。follow up如果是亲戚的话，找lowest common ancestor。
第三题大概这样
class Node {
int val,
Node father;
Node mother;
}
public boolean isRelated(Node p1, Node p2);
follow up:
public Node findLowestCommonAncester(Node p1, Node p2);
其实这题有很多followup:
假设现在人分布于各个国家，比如，中国，澳大利亚，美国，那么有什么优化可以做
这是我当年的onsite问题，估计凉在这里
Example :

楼主做法 ： 开始我考虑是parent指向child，然后稍微一比划不对，没让我多想面试官提示我反过来，让child指向parent，这样就是求两个node到leaf的路径中有没有交集了，dfs做出来。
帖子： https://www.1point3acres.com/bbs/thread-201712-1-1.html
路人建议 ：以这个人为root节点，以父母为left和right，向上建树就是二叉树了
然后query的两个人就是两个root，对这两个二叉树进行BFS，找到最浅的公共节点n，n在tree1z和tree2的深度相加即可
这个follow up的一个关键点是找到lowest common ancestor就停了，所以用BFS比较好，不要把某一个人的ancestors都找出来了再去跟另一个人的ancestor找match.
*/