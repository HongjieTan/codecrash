8.10: ms 电面
1. volatile
2. java gc
3. gc调优实践？
4. single producer , multiple consumers, limited sized queue
5. https://www.lintcode.com/problem/number-of-corner-rectangles/description

========================
作者：lihan2011
链接：https://www.nowcoder.com/discuss/54773
来源：牛客网

有幸过了微软的笔试，参加了周五微软紫竹院那边的面试。

不得不说微软的面试体验是我面到现在最好，面试官会认真听你说的话，与你互动，分析你的解法的问题。当你解决一个问题，他会提出一个更加深入的问题。问题难度都不是很大，大约leetcode 中等左右，主要考察你分析解决问题的能力。

一共三轮，有没有第三轮看你前两轮的表现（好像是这样。。）

第一轮 一个小哥，问了两个问题
第一题 长度为n，宽度为2的墙上贴瓷砖，瓷砖宽度2，长度1，问几种贴法。经典斐波那契。 (tan:ok)
第二题 上课，每门课有个区间表示开始和结束，上课需要一件教室，现在有一个课程列表，问最少需要多少间教室能满足这个列表。 贪心算法 (tan:ok, meetings rooms ii)

第二轮 一个年纪稍大的小哥
问题，给定一个有向图，问两点是否可达。广度优先搜索。  (tan: routeBetweenNodes/bfs/, ok)
follow 判断是否可达并返回最短路径沿途经过的点  (tan: 类似于wordladderII/bfs, ok)
follow 找出所有路径和沿途经过的点，并分析时间空间复杂度 (tan: All Paths/dfs+cache/ , ok)

第三轮 一个年纪再大一点的小哥
base64 编解码 

微软很效率 当天出结果。
祝大家面试顺利 ：）



2018(4-6月) 码农类General 硕士 全职@Microsoft - 猎头 - 技术电面 Onsite  | Fail | 在职跳槽
电面：1. 在环状链表中删除一个节点
2. insertRule(first, second)， first必须在second之前出现，如果conflict，return false， no conflict的话把新的rule加进去并返回true

昂赛：
1. 力扣而久期
2. 未排序数组去重，follow up：如果数据很多无法放进内存怎么办
3. 排序后但被移位的数组中找最小的元素，用二分法， 里口原题不记得题号啦
4. 里口司玲散. From 1point 3acres bbs
5. 判断链表是否有环， 问为什么俩指针的速度是2倍关系而不是三倍或者四倍.... 1point3acres

题目并不是很难，面我的都是manager，每个人一个小时但是基本上都要聊天聊20分钟到半小时。。。
由于准备的不是很充分，两轮答得不好所以给挂了。。。


2018(7-9月) 码农类General 硕士 全职@Microsoft - 内推 - 技术电面  | Other | 在职跳槽
刚刚面完的MS phone interview， 上来先自我介绍，然后问了BQ。
接下来就是coding，最常考的Tic Tac，然后还问了一下复杂度。
然后就聊了一下组的情况。全程一共只有20 分钟。


2018(7-9月) 码农类General 硕士 全职@Microsoft - 猎头 - 技术电面 Onsite  | Other | 在职跳槽
先是OTS，三道题目的那种。
1. 找bug。用dfs找两个人的社交网络是不是有交集。
2. sort color
3. validate binary tree, 要求O(n)

昂赛：
1. 蠡口 ⑩. 围观我们@1point 3 acres
2. 从一个binary stream(0/1)里面拿bits组成binary string。第i轮可以从stream中依次拿i个bits。每次拿1个bit，就把之前bits组成的bianry string往最高位shift一位，再加入当前bit。当拿了j个bits (j <=i)的时候，如果当前binary string的值是可以被3整除，就打印。. 留学申请论坛-一亩三分地
例子：
    i = 1: 只能拿一个bit。假如拿到的是1，没有任何打印，因为不被3整除。假如拿到的是0，打印在第1位可以被3整除。
    i = 2: 00 -> "at 1, divisible by 3" "at 2, divisible by 3"
             01 -> "at 1, divisible by 3"
             10 -> nothing
             11 -> "at 2, divisible by 3"
    ...
    i = 5: 11011 -> "at 2, divisible by 3", "at 3, divisible by 3", "at 5, divisible by 3"

3. merge k sorted linked list， 没有额外空间
4. 字符替换。比较简单。。好像是用来考验有没有自信心的


2018(7-9月) 码农类General 硕士 全职@Microsoft - 网上海投 - 技术电面  | Other | 在职跳槽
感觉大概率是挂，最后是thanks for your time...
凡是听到这个的还没见过没挂的。。。
乐扣起另一  701


2018(7-9月) 码农类General 本科 全职@Microsoft - 网上海投 - 技术电面  | Other | 在职跳槽
刚刚跟巨硬的SDE面了第一轮。一开始HR说是跟hiring manager面，我还以为大多是behavior呢
后来约好了meeting发现是个SDE

今天面试，
30分钟的面试变成了50分钟。（她还挺有耐心的）一开始她介绍流程， 说我先介绍一下我自己，然后问个简单的算法题。我自然就想到了LC的easy tag

历时15分钟介绍完自己回答完她针对我experience的提问后，我们开始考题。

原题是利口 四时易 找出array里面缺少的最小的整数。没做过，也没任何公司的tag。tag题都做不完呢。

首先我给了brut force solution，先sort 然后再一个一个查. from: 1point3acres
然后她说sort 太慢O(nlogn), 可不可以不sort。然后就说那就搞个同样大小的boolean array来keep某个integer是否visited了
然后此时已经30分钟了，我心想你饶了我吧，然后她又说那我们可不可以不用这个extra space. from: 1point3acres
在各种思考各种提示下想出如果一个arr > 0, 那么arr[arr] = arr, 最后再过一遍array看哪个element是负数就好了。.留学论坛-一亩-三分地
她居然让我写， 我就写，写了俩for loop, 然后她问你觉得你的两个for loop 遇到 [2,3,4]怎么办，然后尴尬，然后我要改。
她终于放过我了说哎呀已经超了20分钟啦，就算了吧。
-google 1point3acres
肯定挂了，没得说了。 来源一亩.三分地论坛.
大好好刷题。加油


!!!!:
http://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=424307&extra=page%3D1%26filter%3Dsortid%26sortid%3D311%26searchoption%5B3089%5D%5Bvalue%5D%5B2%5D%3D2%26searchoption%5B3089%5D%5Btype%5D%3Dcheckbox%26searchoption%5B3046%5D%5Bvalue%5D%3D3%26searchoption%5B3046%5D%5Btype%5D%3Dradio%26sortid%3D311



作者：BNU_15_张雨
链接：https://www.nowcoder.com/discuss/82916
来源：牛客网

5.       微软(C+E 大陆预科生，完成3面)

微软的3面都在skype上完成。


一面11:00开始，面试官迟到了大概20分钟。

自我介绍，聊了一些ACM竞赛经历；

一些课程设计是做什么的、怎么实现的；

C++的多态怎么理解；

算法题：给定一些正整数(1,3,4,5,7)，输入一个n，输出最少用多少个给定的数能加出n来。

一开始给了一个贪心，后来发现不对；

后来找到了一个特判，修正之后对(1,3,4,5,7)适用了；

要求对任意输入适用，于是写了一个dp，最后dp写出来了。

一面结束；


二面13:00开始。

自我介绍+课程设计；

了不了解design pattern；

算法题：手动实现STL的equal_range()

Emmmm这里我用了两个二分做的，实际上操作的时候有一个typo没发现，是边界情况没控制好，所以调试的时候调了很久，最后有小bug，只能口胡说一说思路，讲一讲写出来的代码然后过去了。


算法题：给定有序的整数序列A和B，求序列A和B merge以后的中位数。

这个题我想了一个(logn)2的做法，思路是：对任意一个数x，我可以用二分确定x在序列A中排第几，B中排第几，所以可以用logn的代价来判断x是否为中位数。那么我对通过二分去枚举x即可。

面试官最后说有个logn的算法，是和数学公式有关的。。。没有再深入研究

(这里斗胆质疑一下面试官算复杂度的水平。。。竟然认为(logn)2的解法和n的解法是一样的。。。)


二面结束以后接到HR电话，说一会儿15:00有三面。

三面同样是自我介绍开始(面试官的头像明显比前两个帅多了)；

三面只做了一个算法题：

给定一个整数序列，A和B两个人轮流取数，每次只能取最左的数或最右的数，两个人都想使自己的和尽可能大，问给定序列以后A和B能取到的和各是多少。


这个题乍一看是一个博弈，但是仔细想了想发现博弈的一些常见算法都没法用。考虑到A取完以后相当于得到了一个新序列并让B来取，我一开始想到的思路是用递归做。

递归写完之后面试官认可了正确性，但是要求优化。我在递归的基础上加了记忆化，把理论复杂度降到了n2。但是面试官还坚持要继续优化。

最后根据递归写了一个DP，最后把DP写出来了，DP的循环方式和正解有点不一样，但是正确性还是得到了验证。

三面就结束了，面试官的评价是：最后做到这个地步，已经可以了。

当时觉得希望还蛮大。


微软的三场面试是最累的，直观感受其实还是蛮适合ACMer的，不需要工程经验，都是以做题为主。当时觉得面得还不错，但是总归是人外有人，也可能上海的竞争比较激烈，从HR的反馈来看应该是进了备胎池，一直没有发拒信，打电话问也还说有机会，但是至今也没等到offer。