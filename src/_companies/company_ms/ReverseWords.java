package _companies.company_ms;

/**
 * Created by thj on 2018/7/22.
 */
public class ReverseWords {
    public char[] reverseWords(char[] str) {
        // write your code here
        reverseWord(str, 0, str.length-1);

        int start, end, i=0;
        while(i<str.length) {
            while (i < str.length && str[i]==' ') i++; // do not forget the "i < str.length" condition
            start=i;
            while (i < str.length && str[i]!=' ') i++;
            end=i-1;
            reverseWord(str, start, end);
        }

        return str;
    }

    private void reverseWord(char[] str, int start, int end) {
        while (start < end) {
            char tmp = str[start];
            str[start] = str[end];
            str[end] = tmp;
            start++; end--;
        }
    }

    public static void main(String[] args) {
        String str = "the sky is blue";
        System.out.println(new ReverseWords().reverseWords(str.toCharArray()));
    }
}
