package _companies;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * 1. brute force: O(N^2)
 * 2. heap: O(N* N* LogN)
 * 3. linked list O(N^2)
 * 4. hashtable+heap ! O(nlogk)
 *
 */


public class Airbnb_Solution {



    // hashtable + heap!!!
    static String[] paginate_v2(int num, String[] results) {

        return null;
    }


    // origin answer
    static String[] paginate(int num, String[] results) {
        /*
         * Write your code here.
         *
         *  no time to implement a better version....
         *  for better performance, maybe to use a linkedlist_queue, remove inserted node each time to reduce scan size
         *
         */
        List<String> ans = new ArrayList<>();
        Set<String> hostIdSet = new HashSet<>();
        Set<Integer> inserted = new HashSet<>();

        int i=0;
        while (i<results.length) {
            String hostId = results[i].split(",")[0];
            if (ans.size()%num==0 && !hostIdSet.isEmpty()) { // page end
                hostIdSet.clear();
                i=0; // start from begin
            }

            if (inserted.contains(i) || hostIdSet.contains(hostId) ) {
                i++;
                continue;
            }

            hostIdSet.add(hostId);
            inserted.add(i);
            ans.add(results[i]);
            i++;
        }

        // add remaining
        for (i=0;i<results.length;i++) {
            if (!inserted.contains(i)) {
                ans.add(results[i]);
            }
        }

        // add seperator
        List<String> ansWithSeperator = new ArrayList<>();
        int count=0;
        for (String s: ans) {
            ansWithSeperator.add(s);
            count++;
            if (count%num==0) {ansWithSeperator.add("");}
        }

        // conver to arr
        String[] ansArr = new String[ansWithSeperator.size()];
        for (i=0;i<ansWithSeperator.size();i++) {
            ansArr[i] = ansWithSeperator.get(i);
        }

        return ansArr;
    }

}
