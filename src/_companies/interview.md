MS:
1. Num of corner rectangles..
2. java basics: keyword volatile, gc, ...

hulu:
- binary search insert point...
- LRU cache

Autonomic:
round1:
1. minimum sub perfect tree
2. system design:
一边是车辆实时生成各种数据(<vehicle_id, time_point, key, value>): <v1, t1, speed, 30>， <v2, t1, speed, 20>, <v1, t2, motometer, 12>, ...
一边是终端查询, query={vehicle: v1, start_time: t1, end_time: t2, key: speed} -> res={average_speed: speed}，
设计这样的系统。
 - 怎么做范围查询，以及t1数据后到达的情况，怎么处理（现场回答有点问题，）
    presum? segment tree?
 - kafka key value store ？

round2:
1. design uber !!!!!!
2. some bq:
    what's ideal job. (当场说的废话太多，应该言简意赅。。)
    what's the biggest challenge in your project xxx.
3. What situations to use karfka(MQ) ???

AirBnb:
Array2DIterator...

LinkedIn:
- lt: 179(largest number), 226(invert binary tree), 50(pow(x,n))
- java: ConcurrentHashMap!!, Sprint IoC AOP 实现， JVM, Locker, 多线程， 。。。

Ant1:
- 分布式事务的处理，二阶段提交 vs 更高性能的设计（乐观锁设计）？
- 乐观锁，悲观锁
- spring aop实现原理
- jvm 双亲委派模型
- java 多线程相关， 哪几种锁，。。。
- redis 缓存穿透，缓存雪崩？？
- java 单例的实现（注意线程安全） ？
- consistent hashing!!??

Ant2:
- IOC, BeanFactory。。。
- Mysql 隔离等级 ？

ebay:
phone round:
- java: asynchronized vs lock, volitale
- coding: find kth element in two sorted arrays
onsite：
-分片存储后数据的查询？ Distributed storage: (like hadoop...)

amazon:
phone round: check _20181123_amazon_interview
onsite: check _20181203_amazon_onsite

google:
phone round:20190318
- lc1011!! (lc410类似)

onsite round: 20190408:
1. 第一问:f(x)是一个单调递增，从非负整数到非负整数的映射，给定一个y值，求对应的x值。
    followup：如果找不到对应的x值，找距离最近的x值
    followup：比如f(x)里面有溢出的情况，怎么定义api，同时找x的逻辑里怎么处理这种case
   第二问:经典人车问题基本版
2. 纯bq
3. 有M个boys，N个girls， M<=N, 给定一个API， Set queryMatches(Set girls),输入是给定girls集合，返回是对应的boyfriends集合，如何用最少的api调用，找出所有男女朋友关系
4. 第一问: 给定一堆路径及其读权限：
        dir                  read access
        /temp/abc/aaa        true
        /temp/abc/bbb        false
        /temp/xyz/yyy        true
        /temp/xyz/zzz        true
        /temp/efg            false
   打印出有读权限的路径如下：
        /temp/abc/aaa,  /temp/xyz/
   第二问: 给定grid, 里面有'X','Y', 找出'X'到'Y'的最短曼哈顿距离。（面经题）  （如果是求最大: check LC1162.AsFarFromLandAsPossible.java）
5. 第一问: 两个字符串表示的数相加
   第二问: (System Design) 一个数据库迁移到另一个数据库，迁移过程中，要求持续相应用户请求和保持数据一致性。
            tan: 参考下
            - <<8 Steps to Safely Migrate a Database Without Downtime>> (http://www.aviransplace.com/2015/12/15/safe-database-migration-pattern-without-downtime/)
            - <<design data-intensive applications>> charper5#Leaders and Followers#Setting Up New Followers, P155
总结:
- 系统设计方面需要完善
- 第3轮跪，花了较长时间想到解法，coding也差点来不及，对新问题的分析上，怎么总结改进下？ 算法功力也需要提升。。。
- coding速度尝试下python，可以更快


20191224:
ms phone round:
- lc10 Regular Expression Matching
- invert binary tree: iterative version!