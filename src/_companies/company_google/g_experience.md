later if time is enough:
hugarian, TSP, design tcp receiver, design game board,
lc425,
https://www.douban.com/note/621727035/

#####
_Topic_ConsecutiveValueSeq: x1
_Topic_Counting: x1
_Topic_Iterator: x1
_Topic_LeastSteps: x1
_Topic_RangeQuery: x1
_Topic_SpecialDataStructure: x1
dp:
    Topic_DPUsingLength: x1!
    Topic_GameTheory: x1!
    Topic_Knapsack: x1
    Topic_MultiDirectionDP: x1
    Topic_MultiPartDP: x1
    Topic_RangeDP: x1
    Topic_RegExpMatching: x1
    Topic_SubSeq: x1
    Topic_SubString_SubArray_ConsecutiveLine: x1
    Topic_UniquePaths: x1
    Topic_TopDownDP_DfsWithMemo: x1
    others:  wordbreak x1/copybooks x1/
graph:  x1!
    Topic_UnionFind: x1/important!!
    Topic_DistanceMap: x1
    Topic_MST: x1!
    Topic_DetectCycle_TopoSort： x1
    Topic_SourceToTargetProblems: x1/ dij/bellman/bfs/dfs/...
    Topic_ListAllPaths: -x1
    Topic_FloodFill: -x1
    Topic_CasesSolvedByGraphIdea: -x1/
    Topic_AssignmentProblem: TODO if have time...?
    Topic_CasesSolvedByGraphIdea: -x1/
    others: -x1/
bs: ok
    _Topic_CompareByFunc: -x1/important!!！
    _Topic_SpecialCase(BinarySearchInUnSortedArray):  -x1/
    _Topic_RandomPickWithWeight  -x1/
    _Topic_FindSomething    -x1
hashtable: ok
    Topic_BiJection -x1
    Topic_PointsInCoordinateSystem  -x1
    Topic_CheckBeforeSelf   -x1!
    Topic_BucketSort   -x1/H-index...
    others: x1
stack: ok
    _Topic_EvaluateExpression_ExpressionParsing   -x1
    _Topic_NextGreater  -x1
    others  -x1
string: ok
    _Topic_AlignWords  -x1
    _Topic_StringMatch_KMP  -later..
    _Topic_SubseqMatch  x1!!
    others -x1
array:
    _Topic_PrefixSum_PrefixMax  -x1!
    _Tpoic_IntervalProcessing   -x1!
    _Topic_CyclicSwapping  x1
    _Topic_2DArray  -x1
    others -x1
tree:
    Topic_BuildTree -x1
    others: skip..not hard...
2p: -x1/just use the template...
string._DataStructure_Trie_PrefixTree: -x1
greedy: -x1...
backtrack: skip..easy...
heap:
math:

##### very high freq
https://docs.google.com/document/d/1qxA2wps0IhVRWULulQ55W4SGPMu2AE5MkBB37h8Dr58/edit
https://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=446944
https://www.1point3acres.com/bbs/thread-448608-1-1.html

- 1.自行车和人匹配问题 (高频 21+次)
    -x1/case1，有先安排距离最近（局部最优），pq+set/case2，case1+有障碍，bfs/ TODO case3 全局距离最小？二分图最佳匹配？/ case4 max距离最小 ？
    - followup 笨办法是直接用回溯尝试。。。。

- 2. LC685/LC685/二叉树删除边/BST删除边 (高频 13次)
    -x2/good!!! /分别见：RedundantConnection/RedundantConnection2/RedundantConnectionInTree/RedundantConnectionInBST
- 3. LC62/63Unique Paths with followups
    -x3/ good!
- 4. LC843. Guess Word 频率：11
    - x4/ good! 策略是每次选一个剪枝能力最强的词来猜...
- 5. LC890 word pattern match 高频8次
    -x2/双射，用两个hashmap
- 6. LC489 位置地形扫地机器人 高频 7次
    -x2/
- 7. LC855 考试找位子，尽量分散坐，人会离开 高频 6次
    -x2/good!/pq or treeset
- 8. Key有过期时间的hashmap 高频 6次
    -x2
- 9. 多个不重复的长方形内随机取点【类似LC497?】 高频 6次
    -x2/good!
- 10. LC853 car fleet问题 高频 6次
    -x2/sort or pq
- 11. LC857 雇工人 高频 6次
    -x3/good!/greedy...
- 12. LC750 Corner Rectangle个数 高频 5次
    -x3/利用排列组合优化。。
- 13. LC815 Bus Route 高频 5次
    -X2/ bfs
- 14. LC659 Split Array into Consecutive Subsequences 高频 6次
    -x3/stuck...good!!!/greedy+hashtable
- 15. 王位继承 高频 10+次
    -x2 / good! see KingFamily.java
- 16. LC951 Tree Isomorphism Problem 树的同构问题 高频 5次 https://www.geeksforgeeks.org/tree-isomorphism-problem/
    -x2/
- 17. N叉树，要求删一些node，返回list of roots 高频 8次
    -x1/
- 18. 可乐饮料机 高频 5次
    -x2/stuck.....good!!!/dfs with memo or dp/ ColaMachine.java
- 19. 生成随机迷宫，高频5次
    -x2/dfs, see GenerateRandomMaze.java
- *. 红蓝小人占领二叉树 （频率 5）
    -x2/ see TreeOccupyGame.java
- *. 最方便都寓所
    -x1/good!!! 见ApartmentInStreetBlock.java
- *. 建水井
    -x1/good!!! 见BuildWell.java
- *. 血缘关系
    -x1/ see FamilyRelation.java
- *. 可删除字母，需要多少次构造
    -x1/ good!!! see RepeatedSubSequenceMatch.java
- *. 无序数组二分搜索：找不到的数  https://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=489043
    -x1/ stuck.../ good!! see BinarySearchInUnSortedArray.java/ 带着范围递归验证！！
- *. 巧克力甜度
    -x1/ see Chocolate_WithFollowUps.java
- *. 给2个string，相同的index各切一刀，要求组成palindrome。follow up：位置可以不同
    - TODO 先check下lc214 ？
    - https://www.1point3acres.com/bbs/thread-477290-1-1.html
- *. 中间60%的k window之和
    - x1/ good!! see SlidingWindowSumOfPortion.java

- 高频4次
  1 下围棋，判断棋盘上点是否被包围 follow up test case 各种形状
     -x1/ 思路：dfs，碰到空子返回false 没被围死
  2 n层map，每层m个node，node和edge都有值，问第一层到最后的minimum cost （类似LC120?）
     -x2/dp...
  3 拿纸牌游戏， 纸牌上面有值，比如说 100， 1， -1， 2， 200， 1. 然后两个人轮流拿，直到拿完。 但是每次只能拿从左边数起的前三个，
    但是如果你要拿第三个，就必须前两个都拿了，你要拿第二个，就必须第一个也拿了，大家都最优策略，问最后第一个人能拿多少分。
    思路：dp存当前人比另一个人能多拿的数，从后往前拿，每次看三个[谁能给个解法链接]
    https://www.geeksforgeeks.org/optimal-strategy-for-a-game-dp-31/类似题目
    这个是一个蛮经典的dp问题，lintcode上也有类似的题目 https://www.lintcode.com/problem/coins-in-a-line-ii/description
    (comment: 博弈的问题应该用 Minmax algorithm. 画出树型结构可以发现用memorization 或者 DP)
     -x2/ game theory dp!!
  4 image以byte[][]储存 如果想中心镜像翻转怎么弄 (LC190)
     -x1/ 类似reverse words  / followup: 如何在O(m*n*8)基础上再优化时间 -> hashtable存反转映射
  5 已知screen的高和宽，给你最小和最大的fontSize，要求给定一个string，将string用尽可能大的fontSize显示在screen里。
    已知两个API getHeight(int fontSize), getWidth(char c, int fontSize)
    ，可以得到每个character在不同fontSize下的高和宽。和面试官交流后，确认string可以拆分成几行显示在screen中
    -x1/ FitScreen.java / binary search...
  6 LC803 打砖块
    思路：最好方法从后往前补。先把砖块全都打掉，然后用贴天花板的砖块dfs+mark，然后从后往前一个一个往上加，加同时若碰上周围有mark的砖块就主动dfs，dfs出来的就是这次打掉的砖块
    -x1/ reverse order + dfs
  7 LC253 给一堆interval，问最多要定多少间会议室
    -x2/ ...
  9 给一堆intervals和一个时间点，问这个时间点是不是空闲。follow up多call优化时间 ( 思路：做一遍merge intervals再来一遍binary search)
    -x1 / 多call优化：merge intervals后，没有重合的intervals，可用二分搜索！
  10 iterator of iterator LC281 zigzag iterator
    -x1
  11 LC68 text justification 把word list转化成等长的行对齐 思路：two pointer [left, right] greedy的把words往上放，中间spaces个数是right-left，注意首行和末尾行的判断。
    - x2/ idea is straight forward, notice the corner cases...
- 高频3次
    1。LC676 magic dictionary各种变种 所求word再dict单词差一个字母  思路：两种解决思路。可以把words按length存起来然后每有词想search时候遍历查找相符。第二种是存的时候就开始删，记得要记录删的地方的index，最后和所求删的index是否相等
        -x2 / 用pattern
    2。LC849 选座位 跟exam room 思路：注意边界条件和怎么判断距离
        -x2
    3。LC418 sentence screen fitting
        -x2
    5。LC844 Backspace string compare 思路：简单stack秒杀 注意follow up是O(1) space → two pointers
        -x1/ followup: O(1) space, in reverse order...
    6。 LC394 s = "3[a]2[bc]", return "aaabcbc".
        -x2/ stack...
    7。LIS... LC334 给一个array，arr[i] < arr[j] < arr[k] given 0 ≤ i < j < k ≤ n-1 else return false. 思路：从左到右过array，用两个变量存第一小和第二小的数（初始最大值），更新尽量小的数，若同时遇到第三小的数，则为true
        -x3/ check LIS
    8。LC774 加油站最短距离  思路：用binary search寻找最短的距离，边search边找当前mid是否符合mid条件，注意判断边界条件和mid==target怎么走/ 我准备用binary search做，老哥不让，说我就要你用dp做，可是dp的解法我没看啊 磨了很久才做出来
        -x1/ bs和dp两种方法都必须能写！/ binarysearch!! similar with CopyBooks.../dp!!! good!! /see MinimizeMaxDistanceToGasStation.java
    9。LC337 二叉树House Robber  思路：dfs+dp存抢当前node和不抢当前node的最大值
        -x2/...
    10。LC340 longest substring with at most K distinct characters  思路：基本滑动窗口问题，遇到就是赚到
        -x3/
    11。log start log finish 没太看懂题
        -x1/ easy...MyLogger.java
    12。LC426 BST撸直变成双向链表 首尾相接  思路：简单DFS。想清楚思路！！！开始dummy当prev留住head，最后prev是tail。其中prev可当做class变量
        -x1/ inorder,用prev保存前一个node...
    13。LC215 Kth largest element in an array     思路：可以先用优先队列装个怂，再用quick select
        -x3/ quick sort partition func...
    14。LC312 扎气球游戏  思路：二维dp问题。dp[left][right]代表能在当前段内能扎出来的最高分。memorize是当dp非零则是没计算过。
        -x2/ dp in order of length...
    15。LC769 LC768 问一个array在怎样trunk sorted之后只经过拼接就能得到升序array 思路：[0~n]的array做法为maintain一个max变量存当前max，当max==当前index则count++. 无限制array时候做法为构造两个新的array存maxOfLeft和minOfRight。当一个数左看都比自己小，右看都比自己大的时候，则可以trunk。（这个更加generalize
        -x2/...
    16。LC505 the maze II 求total steps 思路：用BFS+PQ+memorization做，注意撞墙别忘了往回退一步
        -x2/ good!! bfs+pq+memo(dijkstra...)
    17。LC96 Unique Binary Search Trees  思路：递归+memorization
        -x2/ dp or recursion+memo
    18。LC834 Sum Of distances in tree  思路：两次遍历，更新count和res。第一次post order 第二次pre order
        -x2/ good!

- other
    *. LC939 && LC963 给一堆坐标点，求坐标点形成的横平竖直矩形最小面积  思路：任取两点当对角线做矩形，存在set里。若已有set存在则因为另外一个对角线存在，更新面积
        -x2/  I：两个对角点确定一个矩形  II: 两个中点相等并且长度相等的对角线确定一个矩形
    *. LC230: Kth smallest in BST，followup 若要改树怎么整 [改树是什么意思？有很多树call这个函数多次？]如果中间有人要insert node to BST的话，如何实现同样功能  思路：建一个TreeNodeWithCount，这样以后改的时候也是log n复杂度。改树的同时改TreeNodeWithCount
        -x1/ inorder, followup: 建一个TreeNodeWithCount
    4. LC302:  面试官迟到 15 分钟。面试时间实际为 30 分钟。给定一个 picture (二维)。里面有一些有色的像素，保证所有像素是相连的（上下左右相连），且只有一个联通块。返回一个最小矩阵，这个矩阵能包含所有的有色 pixel。
        -x1/ good! 用二分查找做的，上下左右分别二分找边界
    5. 给定一个棋盘，里面有一些棋子。你能移走这个棋子，当且仅当这个棋子的同行同列有其它棋子。要求最多能移走多少棋子。类似LC 947
        思路：可以把所有棋子放到list里，每row，col存在的棋子再分别放到set of set of nodes里。用dfs思路第一轮删除任意一点，然后往后推第二次在上一次基础上清除任意满足要求的那个点，直到最后无点可清除时回溯看总共清除了多少。可mem
        更新思路：用union find看能有多少组划分出来（如果同行或同列分成一组），然后最多能移走的棋子数=总棋子-组数（number of islands）
        follow up ：是应该用什么顺序拿，才能保证能拿最多 (这个follow up应该怎么解呢)尽量先把一个component里的都去掉？
        优先拿掉不导致component数量增加的棋子。
        -x1/ good! count connected components, union-find(best choice...) or dfs
    6. LC410 SplitArrayLargestSum or CopyBooks
        -x2/ binarysearch! or dp...
    *. 国人。 一个只有正整数的list， 其中插入+， * 或者（），求得到式子最大的值。 e.g. [1，2，1，2 ]->  (1+2)*(1+2)=9.  dp解， follow up， 如果有负数该怎么办， 如果想要拿到最大的式子该怎么办。
        思路：类似burst balloon dp[i][j] = max of for (k : i ~ j  max(dp[i][k - 1] * dp[k][j], dp[i][k - 1] + dp[k][j]))
        -x1/ good! similar with burst balloon / see MaxExpression.java
    *  LC739 array of temperatures, tell me how many days have to wait till next warmer weather 思路：用stack从后往前存，每次看天气时候pop出比栈顶温度低的日子，再peek出比当前暖和的一天index
        -x1/
    *  LC736 非常难 parse lisp expression 注意边界条件和判断条件   思路：分情况 let mult add讨论，用一个parse function处理运算符以后的事宜
        - TODO later
    *  Google Snapshot (LC981)
        -x1/ see TimeBasedKeyValueStore.java...

- onsite cases：
    * 2019.5. TODO https://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=521347&extra=page%3D1%26filter%3Dtypeid%26typeid%3D1019%26typeid%3D1019
        有一个 List<String>, 每个 String 是一个电影名字(全小写), "i love you", "you are smart", "you awesome", "smart baby" 这样
        如果规则是, String A 的最后一词和 B的第一词match, 则可以合并, 求全合并后最长(总 String最长)的合并结果, 返回这个结果. 用上面的例子就是 "i love you are smart baby".

        我用的 DFS 把所有可能的起点都试一遍, 然后把所有合并的词都存下来, 最后从总集合找最长的. 三姐说了一句"‍‌‍‍‌‌‌‍‌‍‍‍‍‍‌‌‌‌‌so it's brute force solution?" 然后不给任何提示让我很头大.

    * Match 4. TODO https://www.1point3acres.com/bbs/thread-490357-1-1.html
        -
    * April 3: https://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=507901
        - 李扣 疑似伞，把链表重新排序
            - TODO
        - 李扣 亦久久，二叉树从右边看的视角
            - ok
        - 李扣 把武器 改编版，hire woker, 思想差不多
            - TODO
        - 二叉树中存在一条多余的边，在不影响树的高度下删除多余的边
            - ok
        - 在一个有向图中给定一个节点，返回从该节点出发以该节点结尾的最短环的长度，若不存在环返回-1.  followup 1: 返回最短环的路径 followup 2: 如果有很多台机器可以使用，如果并行化？
            - x1
        - 设计search auto complete, 给定一些单词和对应的分数，返回给定prefix下分数最高的词。 用的trie，讨论了优缺点和一些优化，然后只让写了建树的代码
            - ok
        - 李扣 夭夭寺 改编版，把二叉树变成链表  一开用前序遍历写出了bug，自己发现后改好了一步步跑test case，最后面试官表示同意。如果跪应该是在这一轮，这道题花时间太久了。
            - TODO

    * April 2: (good!!!) https://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=507962
        - string conversion: given two strings A, B of same length. Define an operation that replaces all occurances of a character from A to another character, for example, replacing a in adac with d gives dddc.
          First question: can you convert string A to string B after a finite number of operations.
          Second question: find the minimal number of operations to convert A to B.
          - x2/ see graph_dfs_bfs.Topic_UnionFind.CountConnectedComponents.StringConversion.java !!!
        - 其他：TreeOccupyGame/ Flipping cards (game theory)/ RepeatedSubSeqMatch   ok...
    * Match 31 https://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=507223
        - 5轮coding第一轮：一条横着的走廊 （比如： y = 0和y = 10的两条线）走廊上有随机的半径不同的柱子（x,y,r），问存不存在能通过这条走廊的路径
            -x1/ union-find 找联通组 / 柱子之间建立边要n^2次。。。
        - 第二轮：meeting room变种
            -xn
        - 第三轮：原题，蠡口巴思思 LC844
            -x1
        - 第四轮：原题 蠡口气气死 LC774 我准备用binary search做，老哥不让，说我就要你用dp做，可是dp的解法我没看啊 磨了很久才做出来
            -x1/ good!
        - 第五轮：蠡口三药物，LC315 但是是反过来的 是bigger number before self
            -x1/ good!

    * March 27 https://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=504803
        - q1 一个停车场，用array表示[1,0,1,0,0,.....]. 1表示有车，0表示没车。写一个function，返回一个空停车位，使得它距离最近的车最远。返回一个array，里面的元素排序表示返回的空停车位的顺序。follow up如果可以parallel怎么做。
            - lc855 exam room /...
        - q2 一个2D matrix，从左上方格开始，到右上方格，求有多少种走法。每次往右移一格，可以平行，上，下移动。follow up1：给另外一个方块list，要求返回所有穿过list中方块的路经数量。follow up2：给定一行方块，返回所有经过这一行方块任意一个/多个的路经数量。
            - unique_paths_with_followups / https://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=490305&extra=page%3D1%26filter%3Dsortid%26sortid%3D335%26sortid%3D335
        - q3 一个猜单词的游戏。client猜一个字符，比如p，server返回_ pp_ _。client继续猜，比如a，server返回app_ _。client直到猜到整个单词为止apple。设计一个client使得猜的次数最小。test这个client。这一问我回答的很一般，因为刚开始想简单了，想当然了。
            - lc843/ 这里题意不清，skip...
        - q4 一列street block，每个street block上都有POI，比如学校，商店etc 也可能没有。给定一个list of requirements, 比如[grocery，school]，找到距离所有requirement最近的apartment位置。跟第一题有点像。follow up是如果只有一些street block有apartment怎么办
            -x1/见ApartmentInStreetBlock.java

    * March 18
        - There are two colors in the matrix, find the shortest manhattan distance of two colors
            - skip/ problem not clear....
        - There are an array of lights. I have a list of intervals, eg [0,2) which means switch the light #0, and light #1. I want to know the final state of these lights
            - x1/  interval processing...treemap...

    * March 7 https://www.1point3acres.com/bbs/thread-505957-1-1.html
        - 第一轮建水井，一个村庄里有很多房子，有的房子旁边能建水井，有的房子只能通过与其他房子建立管道获得供水。给出了在不同房子旁边建水井的花费，已及两个房子间建管道的花费，要求结果给出一份竞价，使得总花费尽可能少 第一题就卡住了，给出了个不算特别好的算法，我又试了别的想法，但是没想出来，面试官问是要个提示还是就之前我给出的完整的算法写，我选择要了提示，面试官根据我第二个思路引导我构成联通图 最后怕时间不够写代码，提醒说用最小生成树
            -x1/ good!!! see BuildWell.java
        - LC362
            -x1/ good! see HitCounter.java
        - 第三轮给了两个string A和B，求B重复的最少次数，使得A能在B中获得匹配，其中B重复后得到的字符串可以进行删除操作，删去不需要的字符，之后再进行匹配面试官直接给出要求用贪心解做完要求给出一些 test case
            - x1/ good!!!! see RepeatedSubSequenceMatch.java !!!!
        - 经典人车匹配
            - 只问到了局部最优版本，"没有用到匈牙利，我就是遍历了每个人和每个车之间的距离，最小堆一个一个往外拿，用visited记录，最后让自己跑一个test case，问了复杂度，变量的作用"

    * Feb 26
        -  {a, b}c{d,e}f 需要return 所有可能的组合，acdf，acef，bcdf，bcef。followup 怎么处理 nested case:    a{b{c, d}e{f}}
            - x1 / see StringCombinations.java / coding is hard!!!!
            - https://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=498286

    * Feb 11
        - lc 947 remove stones...
            -x2/...
        - string deduplication
            -x1/ see StringDeduplication.java
        - bingo game/ refs https://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=452049
            -x0/ 貌似就是set去重 / 是否有其他考查点？水库抽样？
        - SkipIterator
            -x1/ good! see SkipIterator.java...

    * Feb 8
        (Provider: anonymous， 匹兹堡office)
        1. 最方便的寓所
            -x1/见ApartmentInStreetBlock.java
        2. 给一个路径列表，找出每个路径在列表里的parent路径。例子：exports = [“/a/b/c”, “/abc/foo”, “/a”, “/abc”, “/a/b”, “/foo/abc”]，return [[“/a/b”, “/a”], [“/abc”], [], [], [“/a”], []]
            - 网友思路：建立trie，在node里有map of children和boolean isValid。然后遍历exports，到最后一个node把isValid设为true。再遍历exports，如果碰到isValid为true的parent node就把当前string加入结果。时间复杂度O(m*n)，m为路径个数，n为每个路径里“/”的个数。
            - tan：skip...题意不明，如果这样为何不直接扫一遍得结果。。。
        3. 删除二叉树中多余的一个edge。假设input是root node，而且删完edge后还是root。follow up: 如何测试？
            - x1/ 见RedundantConnectionInTree /思路：从root开始遍历，建立set保存visited node，如果碰到有node的儿子已经在set里就删除这个edge。（lc 685的超级简化版）
        4. 面经题，给定宽为w高为h的screen，和一句话string，还有字体的上限和下限，问能使这句话fit进screen的最大字体。有两个写好的api分别是getWidth(Character c, int font), getHeight(int font)。
            - x1/ 见FitScreen / 思路：二分法找median font，然后parse string看能不能fit进screen。

    * Feb 7
        - lc 484 find permutation
            -x1/!!
        - lc 662 / 二叉树找最多siblings的层
            -x1/!!
        - 棋盘上铺多米洛骨牌 lc 790 follow up poj 2446
            -x1/dp...
        - 判断表达式是否合法
            -x0.5 / 感觉就是考想出来所有corner cases
        - valid sudoku 第二题：有一个填满了的数独board，已知有且只有一个数字填错了，且这个数字还是在1-9之间，找出这个填错的数字的坐标。
            -x1 / ...

    * Feb 9 (待?)
        1. 给一堆职员及其老板的关系，要求实现两个query： A. 输入职员姓名按顺序返回其老板chain B. 输入老板姓名，按顺序返回职员chain
            - x0/ 思路：看起来像常规的建图和搜索
        2. 给一个长长的String，String里面有一些char是separator，给一个isSeparator函数可以调用判断是不是separator。separator将该String分成若干部分，判断每部分是不是一个valid password。定义一个valid的password就是长度在6-10的string，包含两个以上的数字和字母。求所有valid password的数目。
            - x0/ 思路：Two Pointers
        3. google doc 有个comment 功能，每个comment是个tuple（int A，int B）。A表示是指向正文中哪一行的comment, B表示当前comment实际显示在正文对应的哪一行。要求实现用户在前一行插入一个comment后如果space不够了，后面的comment自动下移。
            - ?
        4. 国王住在King's Landing, 他手下有很多的信使，他要发布一项消息给所有的kingdoms，派信使去送信。问通知完所有的kingdoms所需要花费的最长时间。
            - 类似LC743 / ok...
        5. 给两个文件，一个文件A很长，另一个文件B记录的是bad words。要求把文件A里面所有match的文件B里面的bad words替换成掉。
            - ?
    * Feb 1 https://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=499453
        - BinarySearchInUnSortedArray.java/巧克力甜度/...
    * May 3  https://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=419382
        - MergeTwoSortedIntrervalArrays/ BinarySearchInUnSortedArray / 没啥难题。。。skip。。。


    * Jan 31
        - LC 23 Merge k Sorted Lists. -ok
        - LC 253 Meeting Rooms II  -ok
        - 家庭树判断亲属关系  -x1/ lowest common ancestor using BFS / see _Topic_SpecialDataStructure.FamilyRelation.java
        - Top k的value，受label限制  -x1/ see TopKWithLabel.java
        - 第四轮：
          中国小哥哥。
          4, 5, 6, 8
          5, 5, 6, 9
          6, 7, 9, 10
          红色表示目的地. 问题是要找出能到所有目的地的点. 可以从其它任何点出发, 上下左右四个方向,, 只能往小于等于当前值的点挪(8可以去6, 但6不可以去8, 5可以去5). 图中绿色点就是答案. 刚开始对题目有一些问题, 后来小哥哥给了提示, 说是目的地不多. 后来就决定反着来了, 看从目的地开始能反着到哪些点, 存成set. 再用每一个点试, 如果在所有的set里就表示从这个点出发能到所有的目的地. 假设k个目的地, m行n列, 复杂度是O(kmn). 然后问如何并行. 对每个目的地都可以单独做, 我以为是O(mn), 但其实复杂度最高的地方是最后的test部分. 把所有的set放到一台机器上复杂度还是O(kmn). 类似merge sort, set也应该两两merge. 最后能优化到O(logkmn). 谢谢小哥哥了.
        - 第五轮：
          模拟抓娃娃机。
          color      count
          Green:   3        3个绿娃娃
          Blue:      5
          Red:       2
          随机抓, 哪个颜色的数量多, 那个颜色被抓的概率也相应的高. 抓完要把相应颜色的数量减去1.
          类似LC 528 Random Pick with Weight, 但不同点在于每次要减去1. 开始用数组, 二分查找O(logn), 更新O(n). 面试官没让写O(logn)的, 写个O(n)的就行. 写完了说想想能不能把更新的部分也优化一下. 我知道要用segment tree, 但想不起来了, 还是不太熟练. 主要是一直在想用用数组实现的方式, 卡在坐标变换上了. 面试官提示说还是用tree node吧, 好想一些. 请参考: https://leetcode.com/problems/ra ... n-with-segment-tree . 我觉得应该也把左右子树的sum放在node里. 面试官就是看看我怎么想的, 说是知道不太好写.


    * Dec 19 2018 (later)
        电面：
            - max distance between any nodes in binary tree.
            - given a string and a int k, return maximum length of subarray that contains less than k. Follow-up question: how to optimize the space complexity since my sliding window solution's space complexity is O(n)
        一面：
            给定一个 query string 和每个子串以及相应的 weight，
            以下内容需要积分高于 188 您已经可以浏览
            需要切分 query string，每个切分如果都在 weight 内，sum 所有的 weight 值，求一个 query string 的最大的 weight 值，如果某个切分其中一个子串不在 weight 内则返回 -1。
            例如：query: abcdefg, weights: {“a”: 1, “abc”: 10, “bcd”: 11, “cde”: 30, “e”: 3, “fg”: 5} 应该返回 20。切分是 a、bce、e、fg。. 1point3acres
            回答：用 DP 求解，一个 array 存储从 query[:i + 1] 的子串的最大 weight，两个指针 i, j 用于遍历 query string，根据 dp[i - 1] 是否为 -1 判断是否应该继续，如果不为 -1 且 query[i:j] 在 weights 内则 dp[:j] 的值应该是 dp[i - 1] + weight[query[i:j]]。遍历完后结果在 dp[-1] 内。
            优化：weight 是 hash 的，最坏情况下复杂度是多少？能不能优化。提示说可以用 Trie 树。
            实现了个 Trie 树
        二面
            经理面，问了 behavior question：
            过去的项目经验
            最自豪的项目
            是否遇到过和其他团队成员沟通过程中的意见分歧，如何解决
        三面
            给定一个 words of dictionary， 看一个 word 是不是能通过在任意位置增加一个 char 也能是在 dictionary 内，这样递归下去能组成一个 chain，返回最长的 chain 的长度。

        四面
            英语面试：设计个 tcp reciever 端的逻辑，包括来了一个包应该怎么处理，怎么处理上层的 read 调用。包是无序的，但是不会丢失，而且 size 不固定。

        五面. 1point3acres
            设计一个跳棋，包括棋盘的布局设计，bondary check，一个子如何走下一步。

#### High frequent leetcode
- （2）134. Gas Station  x1
- （3）162. Find Peak Element x2
- （2）205. Isomorphic Strings x1
- （3）249. Group Shifted Strings  x1
- （3）253. Meeting Rooms II x2
- （2）340. Longest Substring with At Most K Distinct Characters x1
- （4）359. Logger Rate Limiter x1
- （2）308. Range Sum Query 2D - Mutable  x4
- （4 high!）399. Evaluate Division：会写Union-Find解法 x3
- （5）418. Sentence Screen Fitting x2
- （2）486. Predict the Winner x3!/game dp
- （2）496. Next Greater Element I：follow up是如果data是streaming data, 要怎么改代码和设计输出。 x1/stack...
- （2）490. The Maze x2
- （2）505. The Maze II x3
- （3）685. Redundant Connection II x2
- （3）753. Cracking the Safe x2 /good!!!
- （4）815. Bus Routes x3  -x3/bfs
- 10. Regular Expression Matching x2
- 36. Valid Sudoku  x2
- 37. Sudoku Solver x2  /check improved solution later...
- 41. First Missing Positive  x2
- 42. Trapping Rain Water x1
- 43. Multiply Strings x0
- 44. Wildcard Matching     x2/dp...
- 56. Merge Intervals x2
- 146. LRU Cache x2!
- 91. Decode Ways x2!!/dp
- 200. Number of Islands x2
- 264. Ugly Number II   x2
- 289. Game of Life     x2
- 734. Sentence Similarity      x1
- 737. Sentence Similarity II   x1
- 750. Number Of Corner Rectangles      x2
- 684. Redundant Connection x1
- 773. Sliding Puzzle       x1
- 295. Find Median from Data Stream  x1/heap...
- 153. Find Minimum in Rotated Sorted Array     x1
- 494. Target Sum       x2 !
- 96. Unique Binary Search Trees    x1
- 95. Unique Binary Search Trees II     x1
- 72. Edit Distance     x2/good! dp!
- 59. Spiral Matrix II  x1 /nothing special
- 115. Distinct Subsequences    x1 !!
- 128. Longest Consecutive Sequence     x1 !!
- 298. Binary Tree Longest Consecutive Sequence     x1...
- 334. Increasing Triplet Subsequence       x1/trick...
- 392. Is Subsequence       x2!!/very good question
- 659. Split Array into Consecutive Subsequences        x1/trick...
- 674. Longest Continuous Increasing Subsequence        x1
- 727. Minimum Window Subsequence       x1/hard...ag.../good!!
- 150. Evaluate Reverse Polish Notation     x1/easy..
- 152. Maximum Product Subarray     x1!!/stuck.../good!!
- 165. Compare Version Numbers      x2
- 805. Split Array With Same Average     x2!!!/good!!
- 222. Count Complete Tree Nodes    x2
- 270. Closest Binary Search Tree Value     x1
- 269. Alien Dictionary     x2/toposort...
- 621. Task Scheduler   x2/stuck.../greedy...trick
- 560. Subarray Sum Equals K    x2/good!
- 527. Word Abbreviation    x1/hashmap,tricky...good
- 288|Unique Word Abbreviation |17.3%|Medium|     -x1/
- 529. Minesweeper      x1/dfs
- 297. Serialize and Deserialize Binary Tree    x1
- 652. Find Duplicate Subtrees      x1/ post order...
- 346. Moving Average from Data Stream  x1/queue
- 286. Walls and Gates  x2/multiple source bfs !!/距离场问题！
- 394. Decode String    x2/stack!
- 721. Accounts Merge      x1/a little stuck../ very classical union-find!!!
- 552. Student Attendance Record II     x2/ stuck...!!!  hard.../ dp TODO: skip at phone round...
- 731. My Calendar II       x2/!!!
- 739. daily temperature       x1/stack...
- 438. Find All Anagrams in a String     x1/ substring search(sliding window) /...
- 540. Single Element in a Sorted Array     x1/binary search
- 765. Couples Holding Hands    - x1/ TODO need to prove it
- 218. Skyline Problem  -x2/good!!/
- 361. Bomb Enemy   -x2/dp...
- 312. Burst Balloons   -x3/dp...
- 769. Max Chunks To Make Sorted    -x2/
- 787. Cheapest Flights Within K Stops   -x2/!!!
- 802. Find Eventual Safe States    -x1/!!!
- 248. Strobogrammatic Number III   -x2/!!!
- 465. Optimal Account Balancing    -x2/!!
- 337. House Robber III -x2/
- 562|Longest Line of Consecutive One in Matrix |40.9%|Medium|  -x1/stuck!/直观做法，沿着4个方向扫描或者四个方向dp
- 329|Longest Increasing Path in a Matrix|36.9%|Hard|   -x2/dp! good!!!
- 317. Shortest Distance from All Buildings -x1/ stuck.../ just bfs from all 1s
- 362. Design Hit Counter   - x1/ good!!
- 503|Next Greater Element II|47.3%|Medium|     -x1/ stack
- 315|Count of Smaller Numbers After Self|34.8%|Hard|   -x1/! BST..
- 274. H-Index  -x2/ stuck.../bucket sort...good!
- 370|Range Addition |56.5%|Medium|   -x2 /presum...
- 743. Network Delay Time -x1/dijkstra
- 210. Course Schedule II   -x1
- 722. Remove Comments  - /phone/   -x1/ straightforward, just be careful of edge cases...
- 305. Number of Islands II     - union-find...
- 499. The Maze III


##### Real Phone Round:
- xxx. generate random number with possibility  -x1/ good!! presum+binarysearch
    https://www.geeksforgeeks.org/random-number-generator-in-arbitrary-probability-distribution-fashion/
- 38. Count and Say  -x1
- 658. Find K Closest Elements -x1
- xxx. 把一个数组按第k个index的数进行排列，变成小于arr[k],等于arr[k],大于arr[k]的三部分。 (sort colors) -x1
- xxx. 问一个64位整数的1是奇数还是偶数，followup怎么优化复杂度到O(1)解.  (NumOf1Bits)   -x1
- 358. Rearrange String k Distance Apart    -x1/!
- 939. Minimum Area Rectangle   -x1/stuck.../just hash, notice coding...
- 410. Split Array Largest Sum  （or LintCode 437. Copy Books）   -x1/dp! or binarysearch
        在给定的一个任务列表task costs里, 给定天数k, 需要连续地切分任务, 求一个使所有分段的task costs之和的最大值最小的值. 不能有空的分段. 小弟不才, 只写出了一个深度优先搜索的算法, 但是应该使用DP做. 后来大佬告诉我可以用二分做. 原题https://www.lintcode.com/problem/copy-books/description基本一致
        https://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=483704
- 135. Candy    -x2/ dp in 2 directions...
  一月份的狗狗电面，题目是lc原题发糖果135，lz之前没刷过这道题，看着感觉不难，后来一看发现是hard题。。。lz一开始没有理解好题意，试了下greedy，跑了几个testcase都对，结果小哥让我证明greedy为嘛是对的，lz一脸懵逼。。。后来好不容易整出来思路，但时间只剩10分钟不到了，最后代码没有敲完，过了一周多挂了
  https://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=480570

- 问1: 给一个grid，以及人车的坐标，求距离，就是很简单的 一行代码。简单的有点不敢相信，写好了也没支声，小哥哥先说了这么做可以但让我简化下，一下子又闷逼了，就一行的代码怎么简化。于是和小哥哥扯淡，估计他也听不下去了。就直接和我说那个grid不用作为函数的输入。 这问题其实grid有没有没差距。问2 属于一个follow up 的问题，有多个人，多个车，每个人和车的距离不一样，但同时出发，拿最近的车（假设每个人知道其他人距离所有车子的距离），问 “你” 能拿到哪辆车。
        https://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=483343

- 给一个字符串，比如 welcomegoogle
  再给一些骰子，编号为1 ~ n, n是字符串的长度，
  每个骰子有6面，每面有一个字符，比如g, 或者 o，或者是空的“”,
  求一个骰子的序列，以及 朝上的那一面的index(从1到6)，让这个序列和朝上的字符 正好组成了那个字符串, 输出一个满足条件的就行
  followup 是怎么优化
  https://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=481617
- 2019(1-3月)
    https://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=476395&extra=&page=1
    第一个电话
    [1,2,3,4,4,5]
    判断一个list能不能分成groups，一个group必须含有两个或两个以上的元素是重复的
    follow up：
    判断能不能分成groups，每个group必须是五个连续的元素

    第二个电话：
    A,B,C,D,E
    F,G,H,I,J
    K,L,M,N,O
    P,Q,R,S,T
    U,V,W,X,Y
    Z
    输入一个string：WEST   输出 RRDDDD*UUUURR*DDDL*R*
    去那个list里面找这个string的字母，连续输出方向。 第一个字母W，从A开始向右两次，向下四次。
    第二个字母E从W出发，向上四次，向右两次

    补充内容 (2019-1-31 11:23):
    第一个问题[1,1,3,3,5,5,4,4] 可以分成[1,1,3,3][5,5,4,4],这个就是合法的返回true，所有的元素都要在groups里，每个group 必须要有两个或两个以上重复元素。主要是检测原list有没有足够的重复元素的

- 986. Interval List Intersections


# Google
Title | Acceptance | Difficulty | Frequency
---- | --- | --- | ---
562|Longest Line of Consecutive One in Matrix |40.9%|Medium|    - locked
534|Design TinyURL|0.0%|Medium|  - sys
388|Longest Absolute File Path|37.1%|Medium|    - ?
683|K Empty Slots |36.6%|Hard|  - locked
681|Next Closest Time |42.9%|Medium|    -locked
340|Longest Substring with At Most K Distinct Characters |39.2%|Hard|   -locked
482|License Key Formatting|41.8%|Medium|    -x1
308|Range Sum Query 2D - Mutable |24.1%|Hard|   -x1
346|Moving Average from Data Stream |59.5%|Easy|    -x1
298|Binary Tree Longest Consecutive Sequence |41.4%|Medium|
418|Sentence Screen Fitting |27.8%|Medium|  -x1
281|Zigzag Iterator |51.4%|Medium|
425|Word Squares |43.5%|Hard|
361|Bomb Enemy |39.4%|Medium|
394|Decode String|41.7%|Medium|
393|UTF-8 Validation|34.7%|Medium|  -?
568|Maximum Vacation Days |39.1%|Hard|
163|Missing Ranges |23.8%|Medium|
66|Plus One|39.1%|Easy|
686|Repeated String Match|32.9%|Easy|
351|Android Unlock Patterns |44.3%|Medium|
317|Shortest Distance from All Buildings |34.4%|Hard|
271|Encode and Decode Strings |26.2%|Medium|
289|Game of Life|37.1%|Medium|
288|Unique Word Abbreviation |17.3%|Medium|
305|Number of Islands II |39.3%|Hard|
200|Number of Islands|35.6%|Medium|
280|Wiggle Sort |57.9%|Medium|
616|Add Bold Tag in String |38.7%|Medium|
259|3Sum Smaller |41.7%|Medium|
218|The Skyline Problem|28.2%|Hard|
399|Evaluate Division|41.5%|Medium|
159|Longest Substring with At Most Two Distinct Characters |41.9%|Hard|
246|Strobogrammatic Number |40.0%|Easy|
329|Longest Increasing Path in a Matrix|36.9%|Hard|
417|Pacific Atlantic Water Flow|34.1%|Medium|
406|Queue Reconstruction by Height|56.2%|Medium|
279|Perfect Squares|37.5%|Medium|
345|Reverse Vowels of a String|38.7%|Easy|
56|Merge Intervals|31.0%|Medium|
687|Longest Univalue Path|33.3%|Easy|
284|Peeking Iterator|35.5%|Medium|
247|Strobogrammatic Number II |40.5%|Medium|
524|Longest Word in Dictionary through Deleting|43.3%|Medium|
471|Encode String with Shortest Length |42.8%|Hard|
228|Summary Ranges|30.8%|Medium|
391|Perfect Rectangle|27.1%|Hard|
407|Trapping Rain Water II|37.6%|Hard|
286|Walls and Gates |44.8%|Medium|
362|Design Hit Counter |54.4%|Medium|
297|Serialize and Deserialize Binary Tree|34.0%|Hard|
294|Flip Game II |46.6%|Medium|
146|LRU Cache|18.7%|Hard|
380|Insert Delete GetRandom O(1)|39.4%|Medium|
276|Paint Fence |34.7%|Easy|
401|Binary Watch|44.9%|Easy|
295|Find Median from Data Stream|28.0%|Hard|
249|Group Shifted Strings |42.7%|Medium|
463|Island Perimeter|57.5%|Easy|
42|Trapping Rain Water|37.2%|Hard|
320|Generalized Abbreviation |45.5%|Medium|
505|The Maze II |38.2%|Medium|
341|Flatten Nested List Iterator|42.2%|Medium|
266|Palindrome Permutation |57.5%|Easy|
315|Count of Smaller Numbers After Self|34.8%|Hard|
360|Sort Transformed Array |44.5%|Medium|
411|Minimum Unique Word Abbreviation |33.3%|Hard|
269|Alien Dictionary |25.2%|Hard|
239|Sliding Window Maximum|33.6%|Hard|
389|Find the Difference|51.0%|Easy|
465|Optimal Account Balancing |37.8%|Hard|
274|H-Index|33.5%|Medium|
253|Meeting Rooms II |39.3%|Medium|
128|Longest Consecutive Sequence|37.5%|Hard|
20|Valid Parentheses|33.7%|Easy|
158|Read N Characters Given Read4 II - Call multiple times |24.5%|Hard|
293|Flip Game |56.5%|Easy|
139|Word Break|30.8%|Medium|
415|Add Strings|41.5%|Easy|
359|Logger Rate Limiter |60.1%|Easy|
230|Kth Smallest Element in a BST|44.5%|Medium|     -x1
162|Find Peak Element|38.1%|Medium|
270|Closest Binary Search Tree Value |40.1%|Easy|
23|Merge k Sorted Lists|27.7%|Hard|
498|Diagonal Traverse|46.1%|Medium|
400|Nth Digit|30.1%|Easy|
490|The Maze |43.2%|Medium|
318|Maximum Product of Word Lengths|44.9%|Medium|
10|Regular Expression Matching|24.3%|Hard|
212|Word Search II|24.0%|Hard|
240|Search a 2D Matrix II|38.8%|Medium|
373|Find K Pairs with Smallest Sums|30.9%|Medium|
4|Median of Two Sorted Arrays|22.3%|Hard|
369|Plus One Linked List |54.8%|Medium|
422|Valid Word Square |36.4%|Easy|
251|Flatten 2D Vector |40.9%|Medium|
332|Reconstruct Itinerary|29.4%|Medium|
173|Binary Search Tree Iterator|42.4%|Medium|
155|Min Stack|29.7%|Easy|
358|Rearrange String k Distance Apart |31.7%|Hard|
54|Spiral Matrix|26.6%|Medium|
587|Erect the Fence|32.6%|Hard|
679|24 Game|38.9%|Hard|
140|Word Break II|23.9%|Hard|
324|Wiggle Sort II|26.2%|Medium|
17|Letter Combinations of a Phone Number|35.4%|Medium|
484|Find Permutation |55.1%|Medium|
166|Fraction to Recurring Decimal|17.8%|Medium|
22|Generate Parentheses|46.3%|Medium|
409|Longest Palindrome|45.5%|Easy|
224|Basic Calculator|27.8%|Hard|
31|Next Permutation|28.8%|Medium|
676|Implement Magic Dictionary|50.0%|Medium|
327|Count of Range Sum|30.1%|Hard|
421|Maximum XOR of Two Numbers in an Array|46.9%|Medium|
57|Insert Interval|28.3%|Hard|
503|Next Greater Element II|47.3%|Medium|
336|Palindrome Pairs|26.6%|Hard|
684|Redundant Connection|39.2%|Medium|
133|Clone Graph|25.1%|Medium|
44|Wildcard Matching|20.6%|Hard|
50|Pow(x, n)|26.0%|Medium|
543|Diameter of Binary Tree|44.7%|Easy|
460|LFU Cache|24.4%|Hard|
231|Power of Two|40.4%|Easy|
208|Implement Trie (Prefix Tree)|29.5%|Medium|
363|Max Sum of Rectangle No Larger Than K|33.4%|Hard|
354|Russian Doll Envelopes|32.3%|Hard|
348|Design Tic-Tac-Toe |45.8%|Medium|
652|Find Duplicate Subtrees|35.8%|Medium|
444|Sequence Reconstruction |19.6%|Medium|
309|Best Time to Buy and Sell Stock with Cooldown|41.3%|Medium|
501|Find Mode in Binary Search Tree|37.7%|Easy|
382|Linked List Random Node|47.1%|Medium|
402|Remove K Digits|26.0%|Medium|
370|Range Addition |56.5%|Medium|   -x1
272|Closest Binary Search Tree Value II |39.4%|Hard|
475|Heaters|29.6%|Easy|
214|Shortest Palindrome|24.7%|Hard|
551|Student Attendance Record I|44.1%|Easy|
377|Combination Sum IV|42.4%|Medium|
282|Expression Add Operators|30.2%|Hard|
302|Smallest Rectangle Enclosing Black Pixels |45.9%|Hard|
379|Design Phone Directory |34.1%|Medium|
353|Design Snake Game |26.8%|Medium|
375|Guess Number Higher or Lower II|35.9%|Medium|
459|Repeated Substring Pattern|38.2%|Easy|
356|Line Reflection |30.1%|Medium|
261|Graph Valid Tree |38.1%|Medium|
486|Predict the Winner|45.1%|Medium|
542|01 Matrix|33.0%|Medium|
397|Integer Replacement|30.2%|Medium|
321|Create Maximum Number|24.8%|Hard|
408|Valid Word Abbreviation |28.2%|Easy|
368|Largest Divisible Subset|33.7%|Medium|
378|Kth Smallest Element in a Sorted Matrix|45.2%|Medium|
530|Minimum Absolute Difference in BST|47.1%|Easy|
313|Super Ugly Number|38.0%|Medium|
331|Verify Preorder Serialization of a Binary Tree|36.6%|Medium|
323|Number of Connected Components in an Undirected Graph |48.3%|Medium|
451|Sort Characters By Frequency|51.1%|Medium|
374|Guess Number Higher or Lower|35.9%|Easy|
448|Find All Numbers Disappeared in an Array|51.2%|Easy|
310|Minimum Height Trees|28.9%|Medium|
326|Power of Three|40.4%|Easy|
257|Binary Tree Paths|39.7%|Easy|
520|Detect Capital|51.9%|Easy|
729|My Calendar I|36.1%|Medium|
469|Convex Polygon |33.4%|Medium|
474|Ones and Zeroes|39.1%|Medium|
480|Sliding Window Median|31.1%|Hard|
481|Magical String|45.6%|Medium|
483|Smallest Good Base|33.4%|Hard|
485|Max Consecutive Ones|54.0%|Easy|
312|Burst Balloons|43.2%|Hard|
314|Binary Tree Vertical Order Traversal |37.3%|Medium|
357|Count Numbers with Unique Digits|46.1%|Medium|
487|Max Consecutive Ones II |45.6%|Medium|
493|Reverse Pairs|20.6%|Hard|
494|Target Sum|43.6%|Medium|
506|Relative Ranks|46.6%|Easy|
514|Freedom Trail|39.1%|Hard|
447|Number of Boomerangs|45.7%|Easy|
521|Longest Uncommon Subsequence I|55.9%|Easy|
522|Longest Uncommon Subsequence II|31.8%|Medium|
526|Beautiful Arrangement|54.2%|Medium|
527|Word Abbreviation |41.9%|Hard|
316|Remove Duplicate Letters|29.9%|Hard|    x1
531|Lonely Pixel I |55.6%|Medium|
533|Lonely Pixel II |44.6%|Medium|
535|Encode and Decode TinyURL|74.2%|Medium|
541|Reverse String II|43.7%|Easy|
330|Patching Array|32.3%|Hard|
544|Output Contest Matches |70.9%|Medium|
545|Boundary of Binary Tree |31.9%|Medium|
549|Binary Tree Longest Consecutive Sequence II |41.9%|Medium|
552|Student Attendance Record II|31.3%|Hard|
560|Subarray Sum Equals K|40.4%|Medium|
569|Median Employee Salary |34.2%|Hard|
581|Shortest Unsorted Continuous Subarray|29.3%|Easy|
583|Delete Operation for Two Strings|44.2%|Medium|
604|Design Compressed String Iterator |32.5%|Easy|
638|Shopping Offers|43.4%|Medium|
643|Maximum Average Subarray I|37.8%|Easy|
644|Maximum Average Subarray II |23.0%|Hard|
651|4 Keys Keyboard |48.8%|Medium|
656|Coin Path |25.1%|Hard|
657|Judge Route Circle|68.6%|Easy|
658|Find K Closest Elements|35.3%|Medium|
659|Split Array into Consecutive Subsequences|35.7%|Medium|
665|Non-decreasing Array|21.1%|Easy|
667|Beautiful Arrangement II|51.5%|Medium|
668|Kth Smallest Number in Multiplication Table|37.7%|Hard|
685|Redundant Connection II|28.4%|Hard|
689|Maximum Sum of 3 Non-Overlapping Subarrays|41.3%|Hard|
719|Find K-th Smallest Pair Distance|25.3%|Hard|
726|Number of Atoms|45.7%|Hard|
727|Minimum Window Subsequence |29.1%|Hard|
731|My Calendar II|31.6%|Medium


System Design:
- 可以问个system design吗？，已经有uuid来标识每个job, 现在有一帮子pull job 的worker。想设计一个deduper来让worker识别不要去处理已经处理过的job。