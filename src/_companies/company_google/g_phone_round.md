####
https://leetcode.com/discuss/interview-question/233131/Google-Phone-Interview-Question
Hello all,

I recently went though the google phone screen.

Question:
Given valid Java source code as input find the word "google" in the code comments.

My thoughts:
The question is simple enough to understand. The tough part is to handle all the edge cases and getting it done in 40 mins time. I did not pass the interview, even though I made a compelling case by coming up with a lot of edges cases to handle. Only working code can get one though, especially if the problem is seemingly simple, like the one above.

My advice:
Concentrate on speed and practice without an IDE.

Hope this helps.




2018 年 1 月(http://www.1point3acres.com/bbs/thread-316049-1-1.html) 华人小哥面试，积分不够看不了，自己去看吧。
2018 年 1 月(http://www.1point3acres.com/bbs/thread-312999-1-1.html)
   前天面的面完很难过，感叹自己还是经验不足，一口气看完新一季黑镜才平复了心情-google 1point3acres
 1 个小时电面
 应该是一个国人大哥
 只有一道题。
 给 x,y 轴上一堆点，点的坐标都是正整数，问这些点是否是关于 x = ? 的直线对称。
 大哥问了半个多小时简历，所以只有一道题了。。. 一亩-三分-地，独家发布
 . Waral 博客有更多文章,
 . 1point 3acres 论坛
 昨天下午 hr 说结果出了，问我电话还是邮件，我说电话吧，老收据信邮件都收腻了，结果一直都没有
 给我打电话。。估计自己已经对 hr 姐姐不重要了。。. 一亩-三分-地，独家发布
 现在回忆起那段经历还是比较难过的。. 牛人云集,一亩三分地
 问题出在面试经验不足，没太仔细准备简历，也没有想到狗狗家电面也会问简历。。问了半个小时的
 简历，简历上有一个去年春季做的项目(当时还是自己 carry 全组的，叹气)，里面的公式一下子记
 不清了，在大哥追问下答不上来，只好认怂说记不清了。。当时就觉得 gg 了
 后来整个人都有点凌乱，给了题目，大脑一片空白，没有什么更好的思路，就直接用 hashMap 实现
 了，敲完代码，里面有两个 typo，小哥在指出第一个 bug 的时候，高亮了那行代码说了三遍“What
 does this mean?”傻逼如我还给他解释了一下思路，后来才反应过来原来是在说 bug。。赶紧改
 了。第二个 typo 是结束的时候被小哥点出来的。。隔着屏幕都能感觉到小哥的不满。。哎，以后还
 是要多冷静一些，写代码的时候仔细一些，谨防 typo
 总的来说，狗家全职电面题目一般不会太难(毕竟有昂赛)，面的题目的类型一般都跟组有直接关
 系。面试全程你都不知道面试官是谁，都是匿名，狗狗家组又多，所以一般不太会有原题。个人感觉
 全职的电面的题目大多还是考察是否熟悉经典题型，熟悉经典数据结。


2018年1月 (http://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=314883&ctid =84995)
狗家电面新鲜 Intern 电话面试:
. 留学申请论坛-一亩三分地
第一轮:2 道题。第二题可以看作是第一题的 extension。
第一道:找到最长的字符串的长度，要求这个子字符串的头字符和尾字符相同。
如 abdcbe, 则最长的是 bdcb,头 b 和尾 b 相同，返回 4。要求时间复杂度 O(n)。. 1point3acres 第二道:找到最长的连续子序列，要求这个连续子序列的和为 0。

先用 sum_array 转换一下，就变成了第一道题。
要考虑好 edge case。这个很重要!
第二轮:1 道题。OOD。
设计一个扫地机器人。
这个机器人可以前进，向左转，向右转。
方法有 move, turnRight, turnLeft, clean()。 设计相应的数据结构使得它能扫完整个房间。房间大小未知，机器人的初始位置未知。

2018年1月 (http://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=314207&ctid =84995)
需要积分，自己查看。
2018年1月 (http://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=313878&ctid=84 995)
     个多小时前刚刚结束的狗电话。楼主 16Fall 转的计算机，申请的是 18 年的 SDE NewGrad。
  一道完整的题，一点点的展开，很狗，很狗。
 1. 让我写一个"人"类，含有名字跟年龄。用 private+setter/getter，楼主琢磨考 encapsulation
 2. 让我写一个“码工”类，楼主琢磨考 inheritance
 3. 让我区分“码工”类里面，各种 variables 跟 methods，是 public/protected/private/default 和
 why
 4. 让我写一个"给予"跟“接收”的方法
 5. 把第四步的方法，多线程化，考 multi-threading + synchronization
 6. 哪里会锁死和如何解锁 (时间到了，就没细问)
 作为转专业狗，我很认真的看了里扣高频跟新题，过去几天硬着头皮背了一些算法(插一句话:我真 的很佩服那些能够理解并且写出复杂算法的人，楼主只能靠死记硬背，虽然知道这样不好)，不想题
 目竟然是这样。
 提醒一点后来人吧:别家问 encapsulation 和 inheritance，是问这个东西什么意思，或者给你一段
 程序，问这个结果是什么(Mathlab 最最最喜欢考读程序，亲身经历)。狗家一声不知，直接说你写
 一个类看看，也不说要用 encapsulation，是个小坑。当然，也有可能是我道行不够，还有更多的坑
 我没有发现，后来人慎重，慎重。

2018年1月 (http://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=312757&ctid =84995)
   1 月 3 号的面经，准备面试的时候看了些地里面经，发一下回馈地里。不知道是不是新年第一天上班
 比较忙，都有些迟到. more info on 1point3acres
 一面:做软硬件接口部分的美国小哥?
 面试的问题比较另类。对于我这种转专业的人来说不是很友好，问了三道概念题，对于每一题还要求
 写出自己的 summary:
 .1point3acres 网
 先是给了一个二进制转成 hex，再是问 volatile keyword 的作用，我完全没听说过，小哥人很好，
 提问他之后告诉我了，让我在 doc 上总结回答一下。又问了 meaning of endianness. 其实还问了
 RTOS，我说没接触过，他说没关系。。就跳过了。。
 . 1point 3acres 论坛
 {hide=88}
 coding 部分，先是 reverse string in place，算很简单的了。
 然后又加了一题，设计一个 soft timer from a hardware timer to support N tasks.
 我一开始又不懂他的意思，又问了好久，他的意思是有 N 个 task，每个 task 都有各自每隔一个
 period 执行一次，设计一个 data structure 根据 hardware timer 的时间去按照各自的 period 执行
 这 N 个 task。
 二面:感觉年纪偏大的美国人?
 [size=14.6667px]问了一个图像相关的 coding，给你一张图，返回 biggest connected component 的
 面积。
 [size=14.6667px]写了 DFS，又要求写 BFS，又问每种的时间和空间复杂度，还给我举了 dfs 的最差
 情况。
 [size=14.6667px]复杂度确实自己之前没考虑过那么复杂的情况
 [size=14.6667px]follow up:当图片很大的时候会有什么问题，在他的提示下 dfs 的 stack 会很大。.
 from: 1point3acres
 [size=14.6667px]还有什么方法让算法更快一些，我用的是二维 vector，我说转为一维的 vector，也不
 知道对不对。
 [size=14.6667px]
 [size=14.6667px]第二题，给一个函数和一系列的输入输出，string convert(int number, string system) functio
  // 123, “012X456789” -> “12X”
 // 255, “0123456789ABCDEF” -> “FF”
 // 7, “01” -> “111”
 // 7, “AB” -> “BBB”

 我一上来就懵了，完全不会，求了 hint，他又给我举了几个例子才明白。
2018年1月 (http://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=312689&ctid =84995)
刚刚面完谷歌加面，最后挂电话的时候面试官一句 good luck,我就感觉凉了。
 写 code 的时候又出了两个 bug，在他指导下才勉强写完。
 {/hide}
 总的感觉，还是自己的水平不够，要多向地里的大神学习，努力提高自己的姿势水平。
    一面: 来源一亩.三分地论坛.
 扫地机器人，地里面有有不说了。
 -google 1point3acres
 二面:
 上来问了问 如何提升网络速度， 我说的就是分布式啥的。
 然后转换字符串， python 的命名规则转 java 的。
 follow up:就是把一个代码的 python 形式的命名转换成 java 的
 加面:. 牛人云集,一亩三分地
 第一个题目是给你一个 array,假设叫 nums. 那么要输出一个 array,假设叫 nums2，nums2[i][j]就是
 nums2[i][j] = nums[0..i][0...j]。
 第二个题目是给一个 queue，queue 中有 node，node 中是时间戳和字符串。 其中 queue 中的
 node 是按 时间戳增序排列。
 输出一个 queue，要求， 对于当前的 node，如果这个 node 的字符串在最近的 10s 中被加入到了
 output queue 中，就不能加到 output 中，否则可以。
 我采用的就是 hashMap<String,Integer>, 记录每一个 string 被加入 output 的最近时间，然后对于
 新来的 node,来决定加入 output 还是删掉
 follow-up 是 queue 特别特别大，要我优化空间，我就是用了一个 TreeMap<Integer, String> 和
 set<String>。 这两个数据结构用来存储最近 10s 中被加入到 output 的 stirng， 然后对于新来的
 node，判断能不能加入到 output 中
 设计题是 假如你有一个 id machine，要分配很多很多的 unique id 给 requester machine，怎么
 办。
 我说分区，不同区域用一个 id machine 管理。 然后每个 id machine 管理的 id set 不相同。

 然后他问如果 id machine 1 有 id {1,2,3}，然后分配给了 两个 requester machine {0,1}，然后这个
 时候 id machine 需要重启，重启后 另一个 requester machine 请求 id，问应该怎么办。
 我说的是弄个备份，一旦 id machine 坏了，从备份里面提取数据，然后再将剩下的 id 分配给新的请
 求。.留学论坛-一亩-三分地
 我不知道为什么加面的题目这么简单，但是我感觉和面试官沟通的不好，可能自己的口语还是太差
 了。。。
 我看网上说 good luck 就是没戏了， 可能真的需要 move on 了。。。
2018 年 2 月(http://www.1point3acres.com/bbs/thread-319519-1-1.html) Timeline:
9.18 内推 -> 9.19 收到系统邮件 填表 -> 1.16 收 OA -> 1.21 交 OA -> 1.23 HR reach out 填表 -> 2.7 phone interview
从九月就开始等，能收到 OA 感觉已经很幸运了，一开始安排的是加州的 YOUTUBE 组，后来 HR 帮忙换
成了西雅图的 office，赞 HR 超级效率，人都特别 nice
电面:好像是华人小哥，一上来就开始 google doc 做题
题目:给两个 numberStream，已知 bool hasNext(), int next()两 method，要求写一个 method 求两个中最 大的那个 numberStream
边写边各种讨论，可能是我写太慢了，时间刚好写完整的一题，问了 time complexity 和 space 就结束 了......


2018 年 1-3 月(http://www.1point3acres.com/bbs/thread-315446-1-1.html)
   全职 software engineer 电面 也不知道哪里的人 不是印度人不是中国人 不像是美国人 口音有一点
 聊了十五分钟 做题，45 分钟到点走人，题目如下:
 给一个 string 比如"abslsaasssa" 如何调整顺序 输出的是没有间隔相同的 string, 比如"sasasasasbl"
 如何做?
 一开始想用 two pointer swap，结果做了一会发现 corner case 太多了 于是决定 greedy_and_tricks，建立
 dict，然后开始先放最多频率的 char，紧接着放 second highest 的 char,彼此隔开 直到全部放完
 做完了觉得还可以 跟内推聊了一下觉得还可以 但是晚上突然想到了 corner case，应该用
 ceil(len(str)/len(most frequent letter)) 然后一位一位的放 最后把空格去掉输出 str，
 一周之后今天收到 HR 电话 拒了 冷冻期一年 没什么好说的 和 apple onsite rejection 一天收到的 继
 续找


2018 年 2 月(http://www.1point3acres.com/bbs/thread-333774-1-1.html) 听声音是国人小姐姐，人品不错遇到了很简单的一题和原题. 围观我们@1point 3 acres
1. Given a doubly linked list, remove the first node with a value input.
2. Currency exchange, 就是穿了马甲的蠡蔻散韭韭
Timeline 是 2.22 面试，2.27 晚上 hr 打电话通知准备 schedule onsite。
2018 年 2 月(http://www.1point3acres.com/bbs/thread-320243-1-1.html)
       假设有 10 张牌分别是 1，2，3，4，5，6，7，8，9，10 。可以无限次取牌。你是 dealer，给你一
 个数字，根据下面规则求 busted 的概率。
 1. if the dealer's total is less than 17, the dealer draws another card。
 2. if the dealer's total is at least 17, but not more than 21, the dealer stands.
 3. if the dealer's total is over 21 the dealer has busted(lost).
 -google 1point3acres
 tips:
 1. prob(22) = 1, prob(23) = 1 busted probability of any number greater than 21 is 1
 2. prob(17) = 0, prob(21) = 0, busted probability of any number greater than 17 and not
 greater than 21 is 0
 3. 用 dp 解决。
 。。。。。。。弄懂题目就花了好长时间，也是经验少，没做过这种题。在一步一步的提示下最后还
   是没有写完时间到了。我已经猜到我 busted 的概率了
2018年2月 (http://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=331761&ct id=483)
。各位好运
   刚刚问完，听口音应该不是国人和老印，new york office。简单聊了下简历，然后做题，问的题很简
 单。. 一亩-三分-地，独家发布
 1，有两个 String， 只能判断它们是不是只有一组 swap, e.g. conSerVe, conVerSe, 只要交换两个位
 置的 S 和 V 就行了, 这个只要 pass 一遍就行，然后问了复杂度 O(n).
 2, followup,如果不只一组swap,有多组swap怎么办，我说用hashmap，还是走一遍。
 两个程序写完已经 35 分钟了，然后尬聊了一会就结束了。
 两个程序都做了 bugfree, 但是没想到会问这么简单的题，lz 以为至少问个拓扑排序啥的，所以现在好

 慌啊。为啥看面经大家都问了两三题，还比我的难。听说店面问一题是 fail,两题 pass,不造真假，但是
 小哥好像对我的两个程序还挺满意的。先来攒一波人品再说吧。。。
 来源一亩.三分地论坛.
 补充内容 (2018-2-28 10:47):
 回来 update 一下，果然第二天就收到了 onsite，真是水过。。。
2018年2月 (http://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=320578&ext ra=page%3D1%26filter%3Dsortid%26sortid%3D311%26searchoption%5B3089%5D %5Bvalue%5D%5B2%5D%3D2%26searchoption%5B3089%5D%5Btype%5D%3Dch eckbox%26searchoption%5B3046%5D%5Bvalue%5D%3D1%26searchoption%5B30 46%5D%5Btype%5D%3Dradio%26sortid%3D311)
2018 年 2 月 (http://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=318130&ctid=84995 )
      easy 题因子分解，可惜我没刷过，搞了一个老复杂的方法，估计挂了，数学题还是要提前做做，否则
 临场不容易写出最优解
  先说重要的:美国大哥感觉人很好的 上来无废话直接给题 就一个题 Mastermind Game
 Design 问题 第一问类似利口二舅舅 就是返回位置颜色都正确 和只有颜色正确但是位置不正
 确的两个数. From 1point 3acres bbs
 之后做完问了 followup 问如何猜能够尽量少的次数(问了不需要最少)猜出正确答
 案 感觉这不是 AI Design 么有点懵 最后勉勉强强写了先猜出正确组成完了 backtracking 的解
 法
 1 小时前刚刚面完 之前因为有了 return offer 导致一直比较颓废 这次完全是因为女朋友已经有
 了 Google Offer 被她 refer 了完了有了这次面试机会 决定要努力再次好好面狗家这个 dream
 company 真的感觉最近几天学习了好多东西 希望能继续努力 不管结果如何这次想好好拼一
 次 刚刚面完遵从女朋友建议来地里发帖 攒人品攒人品
2018 年 1-3 月电面总结(http://www.1point3acres.com/bbs/thread-366061-1-1.html) 最近总结了地里近期 google 全职电面题目，在地里翻了差不多五页。
给大家分享一下。. (自己下载 pdf 吧)

2018 年 1-3 月 电面+onsite(http://www.1point3acres.com/bbs/thread-347534-1-1.html)
  google 是 17 年 10 月和 hr 沟通的，拖到了 18 年 1 月面试，lc google 题目刷了 2 遍，不熟的又看
 了1遍
 电面:
 lc google tag 原题，感觉像国人，就不上题目了。
 onsite:. 围观我们@1point 3 acres
 round 1: lc463，就一道题，不过矩阵边界处视为 0，dfs 秒了。
 round 2: 好奇怪，胡乱问问题，交流有障碍。什么是 ip 地址，举个例子，如何把一个整数解析成 ip
 地址。给你一堆文档，存在一个文件夹里，每个文档里有很多 ip 地址，假如你有各种工具可以用，你
 如何尽量抽取出里面的 email。
 round 3: 设计停车场。
 round 4: 面经高频题，生成矩形内的随机点，follow up 也一摸一样。
 round 5: 给一个树形的图，让你找叶节点到叶节点的最大长度，输入是一些边的数组。
 第二轮，完全 get 不到他的点，加上听不懂他说话，自己感觉偏向 negative;第三轮沟通的不是很
 好，写完代码后告诉我忘了其它功能，都擦掉重写后没时间做 follow up 了，只是解释了一下怎么
 搞。算法难度偏低，全部秒杀，onsite 未遇到国人与老印。
 fb 面的比 google 好些，希望能来个 offer 吧，要不就要回国了。
 . From 1point 3acres bbs
 补充内容 (2018-3-10 14:58):
 round 2 是找文档里的 email 地址。. 留学申请论坛-一亩三分地
  补充内容 (2018-3-29 01:39):
 加面两轮 onsite
 补充内容 (2018-5-10 08:05): 来源一亩.三分地论坛.
 拿到了 offer，去了 fb
2018年3月 (http://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=365908&ctid=457 03)
   题目简单
 check toeplitz matrix
 . 留学申请论坛-一亩三分地

 但这里的 follow up 问得很有意思 我当时没想出来 现在想出了答案 分享给大家
follow up 2: 这种情况下 有什么更有效的办法么? 在提示下找到
每次比较前后两行 其实只要比较括号里的数字 所以可以用 memcmp 直接比 follow up3:
 follow up 1: 如果 matrix 很大 不能 fit memory 怎么办
 .本文原创自 1point3acres 论坛
 答: 每次读两行比较 这样 memory 只需要比两行大就行
   1 【2 3 4 5】 【2 3 4 5】 6
 34567
   如果 memory 只能存一行 怎么办?
 .1point3acres 网
 答: 每行分块 比如 先比较 第一行的【2，3】和第二行的【2，3】 再比较【4，5】
 . 1point 3acres 论坛
 follow up4: 分块对内存的效率不高 那假设允许程序有百分之一的错误率(就是不符合 toeplitz 性质
 的没有被检测出来)
 这个面试的时候没回答出来 但我结束以后想了想 其实就是用每一行第 1 到 n-1 个元素 算一个类似
 MD5的hash然后计算下一行计算第0到n-2个元素的MD5 如果MD5相同那就认为是相同符合
 条件的
2018年3月 (http://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=334666&ct id=45703)
   CSS colors can be defined in the hexadecimal notation "#rgb", a shorthand for "#rrggbb".
 For example, "#03f" is equivalent to "#0033ff". Suppose the similarity between two colors
 "#abcdef" and "#ghijkl" is defined as (-(ab-gh)^2 - (cd-ij)^2 - (ef-kl)^2), write a function that
 accepts a color in the "#abcdef" format and returns a "#rgb" color that is most similar to the
 input. For example, given "#09f166", "#1e6" should be returned.

2018年3月 (http://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=363194&ct id=483
 先补一个听不出口音的电面:. 1point 3acres 论坛
 symmetric string
 给一个 dic:(a, b) (c, d).留学论坛-一亩-三分地
 (a, b)意思是 a、b 同义，可以互相转化。. Waral 博客有更多文章,
 那么 ac = bd, ac = ad, ac = ac. 1point3acres
 问两个 string 是否相同。
 一道典型的 union find 题。但是我当时菜啊不知道呀，我就说我用 hashmap，如果有(a， b)，那
 么存(a，1)(b, 1)，用一个代号表示。
 然后她举了个例子(a, b)(a, c)，结果用我的方法完全可以做，她感觉有点 confuse，让我继续
 写，
 然后等我写完了我自己举出了(a, b) (c, d) (a, c)的例子证明我的方法有局限性，然后和他讲用 union
 find。就没有时间干别的了，她直接说结束了。
 请大家一定好好掌握 union find，挺常见了。楼主确实菜。
 在大概三天之后收到了 onsite 通知，还是挺惊喜的。因为我觉得我没有给完美解，但向他展示了我的
 完整思路和解决问题的思考，所以还是过了。
后面还有 onsite 面筋。
2018 年 3 月 (http://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=346149&extra=page
%3D2%26filter%3Dsortid%26sortid%3D311%26searchoption%5B3089%5D%5Bvalue%5D%5B 2%5D%3D2%26searchoption%5B3089%5D%5Btype%5D%3Dcheckbox%26searchoption%5B30 46%5D%5Bvalue%5D%3D1%26searchoption%5B3046%5D%5Btype%5D%3Dradio%26sortid% 3D311)
2018 年 3 月 (http://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=377810&extra=page %3D1%26filter%3Dsortid%26sortid%3D311%26searchoption%5B3089%5D%5Bvalue%5D%5B
      买股票那题变种，最多可以买两次，但一次最多 hold 一只股。一开始理解错了题意，最后五分钟写
 了答案，水过。。

2%5D%3D2%26searchoption%5B3089%5D%5Btype%5D%3Dcheckbox%26searchoption%5B30 46%5D%5Bvalue%5D%3D1%26searchoption%5B3046%5D%5Btype%5D%3Dradio%26sortid% 3D311)
    今天下午刚面的。 .1point3acres 网
 让设计和实现 TTL cache.
 API:. visit 1point3acres for more.
 set(key, val, expireTimeInMilliSec)
 get(key)
 .本文原创自 1point3acres 论坛
 follow up:. 一亩-三分-地，独家发布
 设计和实现删除过期的数据。
2018 年 3 月 (http://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=347041&extra=page
%3D2%26filter%3Dsortid%26sortid%3D311%26searchoption%5B3089%5D%5Bvalue%5D%5B 2%5D%3D2%26searchoption%5B3089%5D%5Btype%5D%3Dcheckbox%26searchoption%5B30 46%5D%5Bvalue%5D%3D1%26searchoption%5B3046%5D%5Btype%5D%3Dradio%26sortid% 3D311)
      第一道. more info on 1point3acres
 给一个 rand()的方法，可以 return [0,1)的 double 随机数. 围观我们@1point 3 acres
 让你写一个 int myRand(int x, int y)，return[x, y](y inclusive)之间的随机整数
 第二道
 利扣散久久的变体
 给一系列同义词对 synonyms
 [["a", "b"], ["b", "c"], ["d", "e"]] --> a == b && b == c && d == e. visit 1point3acres for more.
 然后再给一系列 queries，判断是不是全都是同义词, return true or false
 ["a"] ["c"] --> return true (a == b == c)
 ["b", "c"] ["a", "d"] --> return false (b == a && c != d) 来
2018 年 3 月 (http://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=359813&extra=page %3D1%26filter%3Dsortid%26sortid%3D311%26searchoption%5B3089%5D%5Bvalue%5D%5B 2%5D%3D2%26searchoption%5B3089%5D%5Btype%5D%3Dcheckbox%26searchoption%5B30 46%5D%5Bvalue%5D%3D1%26searchoption%5B3046%5D%5Btype%5D%3Dradio%26sortid% 3D311)

 一道题给两个人的 interval list. (interval 由开始和结束组成， 每个人自己的 interval 可能有重合)
 找 a 的 interval 里不和 b 的 interval 重叠的部分。不知道 lc 上有没有这道题。
2018 年 3 月(http://www.1point3acres.com/bbs/thread-319270-1-1.html)
  面试时间上上周四， 1/25/18， 坐标美西，上来寒暄两句直接上题，LC 原题“polulte right
 pointers to each node"，可惜面试时楼主没刷到这题。简单说来就是给一颗二叉树，给每一个
 node 加一个 pointer,指向右边的 node，若没有右边 node 则指向 null。贴一下 LC 上的例子:
 For example,
Given the following perfect binary tree,
 1 /\ 2 3 /\/\ 4567.本文原创自1point3acres论坛
  After calling your function, the tree should look like:
1 -> NULL / \ 2 -> 3 -> NULL / \ / \ 4->5->6->7 -> NULL
  楼主先用大概 25 分钟的时间和面试官探讨并用类似 BFS 的解法写了个 workong solution。然后面试
 官再确认完毕之后问了一下时间空间复杂度的问题，然后开始问 follow up，即用 constant space 来
 解决这道题。楼主花了 15 分钟左右在递归里绕，最后也没绕出来。还剩 10 分钟面试官开始提示可以
 取几个变量 prev，curr 之类的来帮助解题，可最终时间不够题主未能答出 follow up。-google
 1point3acres
 总结一下就是，LC 原题没抓住确实有些可惜。楼主近期恶补了一下二叉树方面的各种题，希望下次能
 够更顺利些。
2018 年 3 月 (http://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=365992&extra=page
%3D2%26filter%3Dsortid%26sortid%3D311%26searchoption%5B3089%5D%5Bvalue%5D%5B 2%5D%3D2%26searchoption%5B3089%5D%5Btype%5D%3Dcheckbox%26searchoption%5B30 46%5D%5Bvalue%5D%3D1%26searchoption%5B3046%5D%5Btype%5D%3Dradio%26sortid% 3D311)
以下内容需要积分高于 100 才可浏览
      刚刚结束的狗家电面，面之前在板上看了不少面经，自己也发一下回馈地里
 两道题都没遇到过，但是还是比较简单
 -google 1point3acres
 之前还准备了一下 BQ，简历啥的，结果啥都问没问，上来就直接做题。做完了题让我问了点问题
   1. 给字符串 s,返回重复次数超过两次的连续字符的起始位置(重复 1 次两次都算合法，大于两次算不 合法)
e.g. hellloooo 返回[[2,4][5,8]].本文原创自 1point3acres 论坛

 . 1point3acres
2. followup
给一个 method boolean isDictWord(),判断 hellloooo 被 format 之后在不在 dict 之中
e.g.hellloooo 变形为 helo, hello, heloo, helloo
只要有一个在 dict 中，便返回 true
. 牛人云集,一亩三分地
第一题就是 O(N),第二题 dfs 都比较简单. from: 1point3acres
 面试的时候没有发现第一题和第二题的联系，自己想出用 map 来村重复字符的信息，然后 dfs 面试官给提示说可以用第一题的结果，然后说我得 map 和上面存好的结果一样，所以第二题第一问 返回的 result 基础上写的
第二题 code 写的有问题，不太对。 写完和面试官说了问题，提出要改一下。面试官说，没有问题， 他觉得是对的。。。我也就没再争辩 这是个啥情况。。。难道是挂了?.本文原创自 1point3acres 论坛
.留学论坛-一亩-三分地
面完了说 recuriter 1 到 2 周会联系
2018 年 3 月 (http://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=378203&extra=page
%3D1%26filter%3Dsortid%26sortid%3D311%26searchoption%5B3089%5D%5Bvalue%5D%5B 2%5D%3D2%26searchoption%5B3089%5D%5Btype%5D%3Dcheckbox%26searchoption%5B30 46%5D%5Bvalue%5D%3D1%26searchoption%5B3046%5D%5Btype%5D%3Dradio%26sortid% 3D311)
2018 年 3 月 (http://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=337015&extra=page
%3D2%26filter%3Dsortid%26sortid%3D311%26searchoption%5B3089%5D%5Bvalue%5D%5B 2%5D%3D2%26searchoption%5B3089%5D%5Btype%5D%3Dcheckbox%26searchoption%5B30 46%5D%5Bvalue%5D%3D1%26searchoption%5B3046%5D%5Btype%5D%3Dradio%26sortid% 3D311)
      计算器肆
 字数字数字数
      朋友内推，2/28 电面。一个国人大哥，没有问其他，直接上题。给一个二叉树，随机改变每个 node
 的位置，新的树结构不变，要保证每个 node 跟以前的该位置 node 不一样，不能直接更换 node 的
 value。 A C.本文原创自1point3acres论坛

 /\ /\
 B C ==> D B
 \\
 DA
 题刷的还是不够，临场反应也慢。讲了半天我的思路，时间过了大半了，他还是没太明白我的思路，
 大哥人很好，又多给了些时间写 code，不过也没写好。应该是跪了，机会就这么浪费了。
 题没刷多少，不知道地里有没有人见过这个，大家可以讨论下。
2018年4月 (http://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=396498&ct id=483)
   先说 timeline, 去年 11 月熟人内推(写了一堆 support 的东西)，直接简历拒。听说今年招很多人，
 1 月又在地里找人帮推，很快收到确认，但由于很多事情一直没有点击申请，2 月初申请的吧，很快
  收到了 OA(大概是 2 月中旬)，拖到了 2 月底做，一周左右收到约电面，3.9 电面，两小时内接到
 电话说过了。等约 onsite 等了一周，收到邮件时说最早只能约 3.26 了，于是约了 3.27。
 电面:一道超简单的 warmup 和 LC 散灵翎
 onsite:
 一轮:给一个 String 数组，比如["Apple", "Ice", "Ivory", "Equal"]和 一个 char 数组，比如['a',
 'i','e','o'’]. 得到结果是以 char 中数组里的 char 开头的每个 String 长度最短的 String 数组，比如这
 里得到的是["Apple","Ice","Equal"]。因为 Ice 长度比 Ivory 小，所以只放 Ice 进来，因为 String 数组
 里没有以 o 开头的，所以无视 o.
 就很简单了，hashmap。
 followup 讨论了一个也是 String 的问题，不知道该怎么描述，用 Trie 做，讨论了很久，没时间写
 了。. 留学申请论坛-一亩三分地
 二轮:一个 n*n 的 grid,有很多 Bike 和很多 Person。输入是 grid 和一个 person 的坐标。求离这个
 person 最近的 bike.(离这个 person 最近的 bike 不一定是所求答案，因为这个 bike 有可能离其他
 person 更近，所有 person 同时出发 take bike,所以该 bike 有可能被别的 person 先 take 了)。解法
 似乎是 BFS 放 bike 进去，更新得到离该 bike 最近的 person。然后遍历这个结果，得到 person 的
 position 和输入 person position 相等的情况时，得到答案?
 我当时在这道题沟通了很久。因为开始跟我说输入只有一个 grid，我心想，只有一个 grid 我求什么
 啊，是把所有 person 放进去求每个 person 得到的 bike 组成的 list 吗。过一会儿跟他确认又被告知
 输入是 grid 和 person。总之沟通耽误了很多时间，直到我写的时候，面试官仍然在思索这题，我直
 接暴力解的，没写完。可能是这轮导致了加面。

 三轮:LC 无屋药，吴舞珥。后者算是 followup，只有讨论，没有写码。两种方法都说了，觉得这轮
 面的不错。(可能没面到 45 分，因为据说 12 点 45 开始面，结果面试官 1 点才来，下一轮的面试官
 直接来敲门了)
 四轮:LC 屋撕酒。讨论了一道 LC 吴思伞，只不过换成多叉树。如果我没理解错。
 面完整整两周毫无消息，于是等不住了给 recruiter 发了邮件，recruiter 邮件什么都没说，只约了周
 五电话(焦虑了三天，觉得肯定挂了，绝望的要死)。然后刚接完电话，说加面两轮。时间地点形式
 随便选。
 加面原因，应该是第二轮吧我觉得，沟通了太久没写完，并且也没给最优解。嗯。希望好运吧。
 面试官人都很好，好吧遇到的题也都很简单，哎但还是被加面了，不过没挂我已经很感恩了。
 总结一下就是 onsite 一共四道题，三个讨论。题数似乎不重要，沟通好代码写好就可以吧。
2018 年 4 月(http://www.1point3acres.com/bbs/thread-415717-1-1.html)
  电面:
给一个树,删掉一些 node 形成一些新的树
面试前没看面经,后来才意识到高频题,不过现场也想出来最优解了,他一共问了三个 followup 也没啥问 题
第二天收到 onsite 邀请
onsite 一共 4 轮
1 系统设计
2 array+search
3 array+search+math 4 二维数组+search
1 轮 设计 loggingsystem,log 有一些参数:thread id, start time, stop time, 要求按照 start time 来排 序.先设计 API 大概结构,挨个讨论了一下用什么数据结构,然后实现
2 轮 find peak in an array 可能有重复, [12 3 4 4 5 2 1] 找到 peak 是 5, binary search 各种 corner case 考虑清楚
3 轮 给一个数组,找到一个 x 使得所有小于 x 的数就等于本身 大于它的数等于 x,所有数字相加最接近
  一个 target
比如 [1 3 5 7 9] target=24, answer is8. 因为当 x=8,数组里面只有 9>8,所以 1+3+5+7+8=24 最接 近 target
4 轮 二维数组 有人 有车 一个人一辆车 看怎么分配.脑子一团浆糊,感觉当时答题像个傻子一样
 来源一亩.三分地论坛.

 隔了一天,onsite interview passed,进入下一步 team match
2018 年 4 月 (http://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=417522&ctid=84995 )
  先说 Timeline: 3/1 HR 联系- 3/15 电面- 4/6 Onsite - 4/16 HC Approval & Team Match - 4/27
 Offer. 一亩-三分-地，独家发布
 电面:. from: 1point3acres
 1. 设计 Google 搜索的测试 case, 一个表达式求值的语句，能想出怎么样的边界情况，记得原来是有
 过这个题的。大概就是数字类型是整数啊，小数啊，有没有可能是字符串啊之类的。
 2. Reverse polish notation evaluation, 逆波兰表示法求值，其实就是很常规的 stack 题，也保证了
 没有特殊情况，正常写。
 3. Check 一个字符串能不能由另外一个字符串中的字符组成，其实就是弱化版的 anagram，开个数
 组或者 hashmap 记一下数量就完事了。之后讨论了一些特殊情况，比如字符串太长不能放进
 memory 之类的。. From 1point 3acres bbs
 总体而言比较基础，可能没有太多参考价值。
后面还有 onsite 的面筋，自己可以查看
2018 年 4 月 (http://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=395888&ctid=84995 )
这个小哥哥面试失败了，写得挺好的，可以吸取经验。
2018 年 4 月 (http://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=399008&ctid=84995 )
这个更佳偏重于 onsite 的面筋
2018 年 4 月 (http://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=408665&extra=page
%3D2%26filter%3Dsortid%26sortid%3D311%26searchoption%5B3089%5D%5Bvalue%5D%5B 2%5D%3D2%26searchoption%5B3089%5D%5Btype%5D%3Dcheckbox%26searchoption%5B30 46%5D%5Bvalue%5D%3D1%26searchoption%5B3046%5D%5Btype%5D%3Dradio%26sortid% 3D311)
        第一轮国人姐姐 问的我 lc307 解释了半天 binary index tree 然后问 Update sum 的时间复杂度.
 1point3acres

 我解释了半天我的树里结构是什么 估计姐姐对这个树也不是很熟
 第二轮印度姐姐 问了我两个题
 第一个 给了一个 BST 问我给 preorder 的 traversal 结果能复原这个树吗 然后又问我只给个 inorder
 的 traversal 结果能 reconstruct 出来吗 我说能 然后就写代码 跑 testcase 有个 bug 改了掉了
 第二个 anagrams 也是 lc 原题.留学
2018 年 4 月 (http://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=409678&extra=page
%3D1%26filter%3Dsortid%26sortid%3D311%26searchoption%5B3089%5D%5Bvalue%5D%5B 2%5D%3D2%26searchoption%5B3089%5D%5Btype%5D%3Dcheckbox%26searchoption%5B30 46%5D%5Bvalue%5D%3D1%26searchoption%5B3046%5D%5Btype%5D%3Dradio%26sortid% 3D311)
==================================
      上个月电面的题 45 分钟 三哥电面 上来就给题目 可能是楼主太菜 可能是被黑了 总之是这一年见过
 的最难的电面/面试题
  给定一个无向带权图 G 和给定节点 N
 求从 N 出发经过图中所有点且最终回到 N 的最短路径, 节点可以多次访问
 . Waral 博客有更多文章,
 TSP 变种, 不会做
 然后面试官降低难度给定 Source 和 Target
 求从 Source 出发经过图中所有点且最到达 Target 的最短路径
 写了一个 DFS 暴力搜索 . 1point 3acres 论坛
 一周之后不出意外的凉了 ==================================
2018 年 5 月 (http://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=427169&extra=page
%3D2%26filter%3Dsortid%26sortid%3D311%26searchoption%5B3089%5D%5Bvalue%5D%5B 2%5D%3D2%26searchoption%5B3089%5D%5Btype%5D%3Dcheckbox%26searchoption%5B30
  同学说 POJ 3311 有点像 http://poj.org/problem?id=3311
  可以参考一下

46%5D%5Bvalue%5D%3D1%26searchoption%5B3046%5D%5Btype%5D%3Dradio%26sortid% 3D311)
面完问了复杂度 第二天通知过了.本
2018 年 5 月 (http://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=425206&extra=page
%3D2%26filter%3Dsortid%26sortid%3D311%26searchoption%5B3089%5D%5Bvalue%5D%5B 2%5D%3D2%26searchoption%5B3089%5D%5Btype%5D%3Dcheckbox%26searchoption%5B30 46%5D%5Bvalue%5D%3D1%26searchoption%5B3046%5D%5Btype%5D%3Dradio%26sortid% 3D311)
1. 上来直接写题，问的是
   G 的面试官的确不错， 能看出来他们家的 engineer 的 quality 的确高， 无论从技术方面还是面试的
 引导
 .本文原创自 1point3acres 论坛
 题目就是 lc 里面判断两句子是不是同义的那题。
         cache 的 put and get。put function 有三个变量 key, value and expiration duration, get function 有一个变量，在 key 有效时间内才会返回 put 的值。
 面试官讲得很快，一开始不太了解题目，幸好自己琢磨了一下 function signature 后，跟他给的例子
 联系起来才明白题目要求。 由此可见好的 function signature 和变量命名的重要性。
 2. 问了一个 follow up question:
 implement a clean up function,
 然后问了时间复杂度。
 3. 面试完没多久就接到 recruiter 通知 onsite
 P.S.其实我最想分享的是自己如何在面试前放松心情的。以前我等面试电话的时候总是在提前在房间
 里等电话，但是这样面试的时候我会非常紧张，因为紧张的心情在面试前就累积起来了。现在我面试 之前都会在花园里走走放松心情，因为大自然的鸟叫声，风吹和阳光下的花都是我喜欢的，然后我会
 一边想 behavior question 和做心理学上 powerful pose，这样我最近面试都不再紧张了。

2018 年 5 月 (http://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=426434&extra=page
%3D2%26filter%3Dsortid%26sortid%3D311%26searchoption%5B3089%5D%5Bvalue%5D%5B 2%5D%3D2%26searchoption%5B3089%5D%5Btype%5D%3Dcheckbox%26searchoption%5B30 46%5D%5Bvalue%5D%3D1%26searchoption%5B3046%5D%5Btype%5D%3Dradio%26sortid% 3D311)
      LZ 一直以来在地理看面筋，得到很多帮助，明天 onsite，发个狗哥简单电面求 rp。
 惯例网上 3 月海投狗哥拿了一个 OA，题目是求最近时间和种花变形，交了后拿到电面。电面是 5 月
 中从 TX 打来的轻口音烙印小哥(?)聊了一下简历开始做题
 题目很简单 力扣贰灵武，写了个 HashMap 解，手打几个 testcase 手动演示结果。小哥表示满意。.
 more info on 1point3acres
 follow up 是如何要做很多次匹配怎么办?LZ 想用 Trie Tree 然后语言描述了一下时间到了。
 答疑时间，问了下小哥组用的是什么东西，他说 BigTable 但是都是 internal 的无可奉告。额 口..口
2018 年 5 月 (http://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=420611&extra=page
%3D2%26filter%3Dsortid%26sortid%3D311%26searchoption%5B3089%5D%5Bvalue%5D%5B 2%5D%3D2%26searchoption%5B3089%5D%5Btype%5D%3Dcheckbox%26searchoption%5B30 46%5D%5Bvalue%5D%3D1%26searchoption%5B3046%5D%5Btype%5D%3Dradio%26sortid% 3D311)
1 踹树， 给了一个 List<striing> ， 实现叉入
2 大隐所有 word
3 随即输出，该率相等.本文原创自 1point3acres 论坛 ---------------------------------------------------------------- 所有问题基本都是很快给出想法，第二问用 DFS 做的，给他解释了下，跑了一个小测试
           第三问快没时间了，没想到问了这么多。LC 上有一个 linkedlist 随即抽样的问题，和那个思路一样写
 的，返回一个 index，但是就是说我写的不对，说我写的是另一个问题，说没时间了， 也不听解释。
 -----------------------------------
 . 一亩-三分-地，独家发布
 面试官迟到 20 分钟，我联系 HR 才给我打电话，全程想主动和他沟通，没反应，很多次问他， 都是

 没反应， 感觉完全心不在焉。 DFS 也和他解释了半天。如果是我自己答的不好，还请大神指点下，
 轻拍。准备了好久，很失落。. 1point3acres
2018 年 5 月 (http://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=417915&extra=page
%3D2%26filter%3Dsortid%26sortid%3D311%26searchoption%5B3089%5D%5Bvalue%5D%5B 2%5D%3D2%26searchoption%5B3089%5D%5Btype%5D%3Dcheckbox%26searchoption%5B30 46%5D%5Bvalue%5D%3D1%26searchoption%5B3046%5D%5Btype%5D%3Dradio%26sortid% 3D311)
~
2018 年 5 月谷歌 面经 按照知识点 总结 2018 年度 2-4 月 (http://www.1point3acres.com/bbs/thread-417430-1-1.html) 得你自己上去看
2018 年 4-6 月 (http://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=430085&ctid=84995 )需要积分，自己看一下
2018 年 5 月 (http://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=417561&extra=page
%3D2%26filter%3Dsortid%26sortid%3D311%26searchoption%5B3089%5D%5Bvalue%5D%5B 2%5D%3D2%26searchoption%5B3089%5D%5Btype%5D%3Dcheckbox%26searchoption%5B30 46%5D%5Bvalue%5D%3D1%26searchoption%5B3046%5D%5Btype%5D%3Dradio%26sortid% 3D311)
以下内容需要积分高于 110 才可浏览
      第一次地里发面经求大米~~~谢谢
  狗家店面小哥风趣幽默，一上来问我紧张吗?我说紧张啊 然后他哈哈大笑说我的任务就是让你不紧张
  第一个问题问了简历上的上课时做的 c++ project，根本没准备说的一塌糊涂
 第二个问题问给你一个树，每一节点可以有 as many as children ，返回这里面所有 姓为史密斯 的人
 名， nary 树不会写墨迹了半天，后来面试官宽容地改让我写二叉树。用的前序 recursive, 还写了树
 的结构，main 测试等等。
         前两周面的狗，电面题目类似系统设计，属于挺简单的，大概就是给你一个 xml 文档，根据这个文档
 设计数据结构，然后给你几个简单的 xml parser 的接口，让你 parse 成这些你设计的数据结构

 比如，给出的文档是 <db>
<store id="123"> <address>xxxx</address> <orders>
<order id="123"> <items>
<item>xxx1</item>
<item>xxx2</item> </order>
</items>. more info on 1point3acres
 <order id="456"> <items>
<item>xxx3</item>-google 1point3acres
<item>xxx4</item> </order>
</orders> </store>
<store id="123">
<address>xxxx</address> <orders>
<order id="123"> <items>
<item>xxx1</item>
<item>xxx2</item> </order>
<order id="456"> <items>
<item>xxx3</item> <item>xxx4</item>
</items>
</items>
</items>
 </order> </orders>
</store> </db>
. From 1point 3acres bbs
然后让你根据这个文档设计数据结构，需要正确得表达这个文档里的内容。我一开始想的是做成常规

 的 XMLNode，然后他说不要做成这种 generic 的，只需要适合这个文档的结构。 其实就是把每个元素做成一个类，然后 list 或者 set 什么的问清楚就好了。
follow up 是，给一个 parser，有三个方法
class parser {
startElement(String elem, Map<String, String> attri); 每当一个 tag 开始时，会 invoke 这个方 法，elem 就是这个 tag 的名字，attri 就是一个属性的 map，就是之前文档里的 id=“xxx” endElement(String elem); 当一个 tag 结束的时候，会 invoke 这个方法，然后告诉你是哪个 tag 结束了
textContent(String text); 当遇到一个 text node 的时候，会给你这个 text node 的内容，比如会
 返回之前的 address 里面的内容
}-google 1point3acres
然后让你实现这三个方法，要求是构造出你直接设计的数据结构。程序外部有一个 file reader 之类的 东西，碰到相应的元素会自动去 call 这些方法。
其实就是注意好 track 当前在哪个元素里就行。然后问了几个问题，比如文档格式不对怎么办，一个 tag 的 endElement 在 startElement 被 call 了怎么办之类的
做完以后又给个 follow up
就是说这个 parser 比较低级，textContent()可能会返回元素和元素之间的空档，比如 items 到子元 素 item 之前会有一段空白字符，其实就是换行符号，这些空白也会 invoke textContent(),问怎么处 理这些空白才能不影响你的数据结构
 小哥上来就开始让做题，也没怎么自我介绍。我一边在 google docs 上写 code 他一边帮我空格对
 齐。。写完正好有七八分钟问问题，然后就结束了。前几天被地里大家的图的面经吓尿了，面试的时
 候问到这种题感觉就放松很多。祝大家好运吧
2018 年 5 月 (http://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=425305&extra=page
%3D2%26filter%3Dsortid%26sortid%3D311%26searchoption%5B3089%5D%5Bvalue%5D%5B 2%5D%3D2%26searchoption%5B3089%5D%5Btype%5D%3Dcheckbox%26searchoption%5B30 46%5D%5Bvalue%5D%3D1%26searchoption%5B3046%5D%5Btype%5D%3Dradio%26sortid% 3D311)
      前几天刚面的，第一个是加州的小姐姐 chrome book 的，人特别热情。题不难但可能真的没准备好
 写的代码和思路都不对。

 以下内容需要积分高于 100 才可浏览
  给 两个 binary tree， 里面有 +,a,b,c,...,d 求 他们是不是相等。 math equvialent a+b+c=b+c+a. 牛 人云集,一亩三分地
.留学论坛-一亩-三分地
follow up: 如果+，-，*，/ 都可以的话怎么办 树表达式一致就可以，可以随意。
第二面是专业方向， 关于 congestion/flow control， 一开始没意识到以为是算法，结果意识到的 时候太迟了。好尴尬!
一共有 4 个 follow up， 但小哥说还有更多。 感觉比较多的是看设计，测试，多线程，网络基础知 识。是个讨论为主的问题
 . 1point3acres
4/17 早上收到 OA，拖了一周，做完 OA 第二天收到第二轮， 因为期末所以拖了两周才面。
感觉第二面是根据填的方向来的，因为选了网络方向结果没想到面试就遇到了。感觉还是没准备充 足，大家加油!
2018 年 5 月 (http://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=426547&extra=page
%3D2%26filter%3Dsortid%26sortid%3D311%26searchoption%5B3089%5D%5Bvalue%5D%5B 2%5D%3D2%26searchoption%5B3089%5D%5Btype%5D%3Dcheckbox%26searchoption%5B30 46%5D%5Bvalue%5D%3D1%26searchoption%5B3046%5D%5Btype%5D%3Dradio%26sortid% 3D311)
}
要求:
1. 2.
下面是一个例子，每个横线就是一个 annotation
      面试官听声音是个白人小哥，啥也不问，也没啥自我介绍，直接上题:annotation 的定义
 class annotation {
 int start;
 int end;
   I want you to assign each annotation a “level”, such that:
 Annotations should always occupy the “lowest” level they can
 Annotations should never overlap in the same level

 this is a sentence
 ___ _____ annotation level 0. 一亩-三分-地，独家发布
 ____ annotation level 1
 __ annotation level 2
 面的时候不是特别明白是什么意思，要了几个例子，a1[1, 2], a2[2, 3], a3[0, 1], a4[2,3]
 -google 1point3acres
 [size=13.3333px]assign a1, level = 0; . From 1point 3acres bbs
 [size=13.3333px]assign a2, level = 0; 因为 a2 和 a1 有 overlap，所以不能是同一个
 level，又因为要 lowest level，就是 0
 [size=13.3333px]assign a3, level = 0; 同 a2
 [size=13.3333px]assign a4, level = 0; a4 和 a3 没有 overlap，所以还是 level0. 一亩-
 三分-地，独家发布
 [size=13.3333px]当时做的时候很模糊，有几个疑问:
 [size=13.3333px]1. 是不是只 care 当前输入的 annotation，给他一个 level 就行 - yes
 [size=13.3333px]2. 需不需要存之前 annotation 的 level - no
 [size=13.3333px]我按照我的理解写了一下，每次进来一个 annotation，和当前最低层比，如
 果有 overlap，直接 set level 0;如果没有 overlap，那就加到当前层里，返回当前层。感觉
 这个逻辑是不是太白痴了......
[size=13.3333px]
   [size=13.3333px]第一次狗家电面，虐的体无完肤
2018 年 5 月 (http://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=426234&extra=page
%3D2%26filter%3Dsortid%26sortid%3D311%26searchoption%5B3089%5D%5Bvalue%5D%5B 2%5D%3D2%26searchoption%5B3089%5D%5Btype%5D%3Dcheckbox%26searchoption%5B30 46%5D%5Bvalue%5D%3D1%26searchoption%5B3046%5D%5Btype%5D%3Dradio%26sortid% 3D311)
      刚刚做完第一个电面:
 一开始问了问我的学习经验什么的
 介绍了他自己是生物学博士 blablabla
 问题很简单
 你上了个电车上面有个很长的长凳，让你找个离所有人最远的位置:
 input: [1,0,0,0,1]. From 1point 3acres bbs
 output: 2

 input: [1,0,0,0,0,1, 0 ,1]
 output: 2
 很简单的问题 一听到脑子里一片混乱，给了个 O(n^2)时间 O(1) 空间的解法 很蠢，后来聊 testcases
 最后 15 分钟说了个 O(n)时间 O(1)空间的解法，不过没时间写了。
 . more info on 1point3acres
 攒攒人品希望给过吧，这是我见过的最简单的题了。
2018年5月 (http://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=426300&ctid=320 )
虽然是在职的面试 但还是面的是 entry level 的. 围观我们@1point 3 acres
有一道题目很有意思 感觉自己可能没作对
面试官上来就问 given a list of rectangles, write a method to choose a random point uniformly in the list of rectangles (maybe overlapped)
感觉怎么没按套路出牌? 别人都是先问一个长方形 再问多个不重叠;我怎么直接被扔了个重叠 的。。。
虽然最后写出来 但感觉做的不是很好;不知道有没有好的解法
2018 年 5 月 (http://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=417422&ctid=84995 )
刚电话的，发帖攒人品求 offer T.T. From 1point 3acres bbs
    就俩问题，
 数组求平均数，没啥好说的
 followup 数组很大怎么办 --> 存到不同 host，每个 host 求平均数后再得出答案呗. visit 1point3acres for
 more.
 数组求中位数
 先说了 heap，他说有没有复杂度小的，我说排序，然后他诱导我说 partition，在人肉写了一遍
 partition 的代码. Waral 博客有更多文章,
 followup 数组很大怎么办 --> 存到不同 host，每次得到每个 host 的中位数后能排除差不多一半的

 答案咯，嘴炮一下即可
2018 年 5 月 (http://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=426559&extra=page
%3D1%26filter%3Dsortid%26sortid%3D311%26searchoption%5B3089%5D%5Bvalue%5D%5B 2%5D%3D2%26searchoption%5B3089%5D%5Btype%5D%3Dcheckbox%26searchoption%5B30 46%5D%5Bvalue%5D%3D1%26searchoption%5B3046%5D%5Btype%5D%3Dradio%26sortid% 3D311)
第一次店面面得不好，被要求做第二次店面。这次题容易一点，po 出来给大家看看吧。 1. Input is a string and a List<Edit> edits, output a string.
class Edit { int index; string before; string after;
}
-google 1point3acres
Example:
input: "A foo".
edits: [{0, "A", "B"}, {2, "foo", "bar"}] output: "B bar"
input: "A foo".
edits: [{0, "A", "B"}, {2, "f", "bar"}] output: "B oobar"
followup 如何 test。
2. 给了一堆 range 和一个点，output 所有包含这个点的 range。如果 List<Range>是固定不变的， 怎么优化
. visit 1point3acres for more.
补充内容 (2018-5-23 12:49):
第一题第二个例子打错了，output 应该是“B baroo"

2018 年 5 月 (http://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=417947&extra=page %3D1%26filter%3Dsortid%26sortid%3D311%26searchoption%5B3089%5D%5Bvalue%5D%5B 2%5D%3D2%26searchoption%5B3089%5D%5Btype%5D%3Dcheckbox%26searchoption%5B30 46%5D%5Bvalue%5D%3D1%26searchoption%5B3046%5D%5Btype%5D%3Dradio%26sortid% 3D311)
      內推後三天接到 HR 安排一週後電面
 一開始面試官先是遲到個五分鐘
 然後 google hangout 聯繫不上，只能在 google doc 上打字溝通
 後來好不容易互相加了 Google+ 才聯繫上，大概就拖延了 15 分鐘了(哎呀我寶貴的面試時間)
 稍微自我介紹，聊了一下工作經歷，就開始作題
 .本文原创自 1point3acres 论坛
 熱身題:
 刪除 double linkedlist 的 node，很簡單，跟面試官討論了做法之後寫出答案
 題二:
 方格上 0 是人類，1 是殭屍，2 是牆，問走法
 先說了暴力解，面試官詢問有沒有更好的做法，我提出了 bfs ，寫出了偽代碼，之後跟面試官討論一
 下就開始實作
 寫出來後面試官也沒說什麼，問了我知不知道 dfs 做法，兩種做法有沒有差別
 .留学论坛-一亩-三分地
 時間不夠就結束了面試 来源一亩.三分地论坛.
 .本文原创自 1point3acres 论坛
 兩週後收到 HR 告訴我掛了
 也許是我自我感覺良好吧，準備了很久了，面試過程也沒有發現什麼不妥，自我感覺跟面試官討論愉
 快，
 現在才知道什麼相談勝歡都是假的，
 自覺履歷的工作經驗也跟 position 上符合，.本文原创自 1point3acres 论坛
 但還就是掛了，HR 也沒說具體原因，就說面試官的 feedback 不好，要求加面也被拒。
 情緒低落，但也只能 move on ，還有幾家公司的面試，. 1point 3acres 论坛
 Apple 也還沒回我信(也許又是石沈大海)，. 一亩-三分-地，独家发布
 希望一點面經可以幫到後面的人。. 1
2018 年 5 月 (http://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=425422&extra=page %3D1%26filter%3Dsortid%26sortid%3D311%26searchoption%5B3089%5D%5Bvalue%5D%5B 2%5D%3D2%26searchoption%5B3089%5D%5Btype%5D%3Dcheckbox%26searchoption%5B30

46%5D%5Bvalue%5D%3D1%26searchoption%5B3046%5D%5Btype%5D%3Dradio%26sortid% 3D311)
   刚刚结束的狗家第二轮店面，
   银行 A
数字
0010-0039
   银行 B
数字
0040-0069
   银行 C
数字
0070-0075
 他给写了个 wrapper object
 如何实现给一个 00012 返回银行 A。用了二分法搜索那个数。可是如何把银行和数字联系起来没有弄
 出来 TT。。。 move on 了 怪自己不扎实，一紧张啥也不会。
 大家会怎么做这道题?
2018 年 5 月 (http://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=426736&extra=page
%3D1%26filter%3Dsortid%26sortid%3D311%26searchoption%5B3089%5D%5Bvalue%5D%5B 2%5D%3D2%26searchoption%5B3089%5D%5Btype%5D%3Dcheckbox%26searchoption%5B30 46%5D%5Bvalue%5D%3D1%26searchoption%5B3046%5D%5Btype%5D%3Dradio%26sortid% 3D311)
一道题，但是有几个 follow up
       给一行 string，比如说 I have an apple pie
 再给一个 number，比如说 5
 如何把这个 string 给分开，这样每行最多有 5 个 character?string 不可以从中间截断，但是可以从
 空格的地方截断. 一亩-三分-地，独家发布
 举个例子
 divide_string("I have an apple pie", 5)
 返回这样子，不用管 leading zero 和 tailing zero(不过后面 follow up 会问到，有没有什么 edge
 case，可以提一下 leading space 和 tailing space，不会让 implement)
 "I have". 牛人云集,一亩三分地
 "an"
 "apple"
 "pie"
 followup，如果 string 可以从中截断，该怎么办?以及怎么 implement 比较好
 我回答:那就从中间截断吧，截断的地方要加一个“-”，表示这个 string 被切开了，比如
 divide_string("I have an apple pie", 4). 牛人云集,一亩三分地

 返回
 "I h-" "ave"
 "an"
 "app-"
 "le"
 "pie"
 继续 follow up，比如说有一种字体，可能每个字母的宽度都不一样，有一个 API 可以查看每个字母
 的宽度, divide string 的时候，怎么才能尽可能少次数的调用这个函数
 答:用 hash table 存所有 ASCII 字符的宽度，然后需要用的时候直接查找 ASCII table
 可能题目中的某些细节记不清楚了，面试的时候 study room 的屏幕上正在放 avenger3 的预告片，
 不小心看了一眼就走神了。。。。害得小哥重复了好几遍题。。。。TOT
2018 年 5 月 (http://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=426847&extra=page
%3D2%26filter%3Dsortid%26sortid%3D311%26searchoption%5B3089%5D%5Bvalue%5D%5B 2%5D%3D2%26searchoption%5B3089%5D%5Btype%5D%3Dcheckbox%26searchoption%5B30 46%5D%5Bvalue%5D%3D1%26searchoption%5B3046%5D%5Btype%5D%3Dradio%26sortid% 3D311)
2018 年 6 月 (http://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=427983&extra=page
%3D2%26filter%3Dsortid%26sortid%3D311%26searchoption%5B3089%5D%5Bvalue%5D%5B 2%5D%3D2%26searchoption%5B3089%5D%5Btype%5D%3Dcheckbox%26searchoption%5B30 46%5D%5Bvalue%5D%3D1%26searchoption%5B3046%5D%5Btype%5D%3Dradio%26sortid% 3D311)
标准白姐姐的声音，上来直接上题，
      給兩个 string，比較一樣與否(return bool)，包含#代表 backspace
 ab#c ==ac
 ab##==""
       给一个 tree，每个 node 最多两个子 node, 删除其中的一些 edge 使其成为 binary tree.
 例子:
 A . Waral 博客有更多文章,
 /\
 B ->C

 (A 到 B 和 A 到 C 有箭头). Waral 博客有更多文章,
 删除 B->C。
 一开始白姐姐信号差换了个电话浪费 3 分钟，然后楼主辣鸡耳机听不清气的直接把耳机拔了夹着电话
 写，心态爆炸导致思维停滞状态好久，一直要白姐姐重复说的话她都快烦死了，基本跪了。
 第一次店面经验不足，心态太重要了。刷再多题心态爆炸连平时 10%的功力都达不到，大家面试请一
 定放平心态啊!!
2018 年 6 月 (http://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=427997&extra=page %3D2%26filter%3Dsortid%26sortid%3D311%26searchoption%5B3089%5D%5Bvalue%5D%5B 2%5D%3D2%26searchoption%5B3089%5D%5Btype%5D%3Dcheckbox%26searchoption%5B30 46%5D%5Bvalue%5D%3D1%26searchoption%5B3046%5D%5Btype%5D%3Dradio%26sortid% 3D311)
2018 年 6 月 (http://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=428871&extra=page
%3D1%26filter%3Dsortid%26sortid%3D311%26searchoption%5B3089%5D%5Bvalue%5D%5B 2%5D%3D2%26searchoption%5B3089%5D%5Btype%5D%3Dcheckbox%26searchoption%5B30 46%5D%5Bvalue%5D%3D1%26searchoption%5B3046%5D%5Btype%5D%3Dradio%26sortid% 3D311)
      [hide = 120]题目是给一个 tree 里面的内容是 HTML，每个 Node 要么是标签要么是标签里的 text，
 给你一个 tree 和一个 string 要 return 包含 string 的所有 node[/hide]
 上来听完题很懵。。. 围观我们@1point 3 acres
 一直在和国人小姐姐说思路，小姐姐人也很好不断引导我，但最后写到 retrun 时间到了。。。
 可能自己还是没有准备充分。。。
      今天店面，遇到一个听不出口音的超 nice 的面试官，和一道最近的高频题。
 . From 1point 3acres bbs
 问:.留学论坛-一亩-三分地
 给定一个屏幕，大小为 W*H， 一个字符串 s，一个字号范围 1 ~ 12， 两个 method 能分别返回给
 定 character 在给定字号下的 w 和 h，求能将 s 放进屏幕的最大字号。
 答案可以参考另一个同学的帖子: http://www.1point3acres.com/bbs/thread-423513-1-
  1.html.1point3acres 网
 2018 年 6 月 (http://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=428592&extra=page

%3D2%26filter%3Dsortid%26sortid%3D311%26searchoption%5B3089%5D%5Bvalue%5D%5B 2%5D%3D2%26searchoption%5B3089%5D%5Btype%5D%3Dcheckbox%26searchoption%5B30 46%5D%5Bvalue%5D%3D1%26searchoption%5B3046%5D%5Btype%5D%3Dradio%26sortid% 3D311)
2018 年 6 月 (http://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=430463&ctid=84995 )
     聊了好几分钟 research + BQ.
 . Waral 博客有更多文章,
 题目是 LC 150
  电面
 二叉树多了一条边 找到这条边 删掉
 onsite
 1. 蠡口 易武巴， 伞斯舞
 2. 蠡口 散酒久 变了一下 本质一样
 3. 蠡口 而伞， 链表换成数组
 4. 有 n 个凳子排成一排，每次来人选座位，每次选择要求离最近的人的距离最远。之前面经见过
 5. 应该是个新题? n 个工人，每个工人有自己的工作时间 和 时薪，多个工人同时工作的时候， 所有
 人的时薪要按时薪最高的人来算. more info on 1point3acres
 如何从中选出 k 个工人，支付的工资最少。
2018 年 6 月 (http://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=430780&extra=page
%3D1%26filter%3Dsortid%26sortid%3D311%26searchoption%5B3046%5D%5Bvalue%5D%3D 1%26searchoption%5B3046%5D%5Btype%5D%3Dradio%26sortid%3D311)
2018 年 6 月 (http://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=429710&extra=page
%3D1%26filter%3Dsortid%26sortid%3D311%26searchoption%5B3046%5D%5Bvalue%5D%3D 1%26searchoption%5B3046%5D%5Btype%5D%3Dradio%26sortid%3D311) 积分不够，自己看哦。
    google 技术电面跪经。.1point3acres 网
 题目是给一个图，然后有几架无人机飞行。 如果有三架，那么就要把图分成三个区域。input 是一个
 图和无人机个数，输出一个划分好区域的图

 .
2018 年 6 月 (http://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=430366&extra=page
%3D1%26filter%3Dsortid%26sortid%3D311%26searchoption%5B3046%5D%5Bvalue%5D%3D 1%26searchoption%5B3046%5D%5Btype%5D%3Dradio%26sortid%3D311) 还是积分不够!
2018 年 6 月 (http://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=430790&extra=page
%3D1%26filter%3Dsortid%26sortid%3D311%26searchoption%5B3089%5D%5Bvalue%5D%5B 2%5D%3D2%26searchoption%5B3089%5D%5Btype%5D%3Dcheckbox%26searchoption%5B30 46%5D%5Bvalue%5D%3D1%26searchoption%5B3046%5D%5Btype%5D%3Dradio%26sortid% 3D311)
一道有趣的题 给你一个 list
("aaa","abd","acd","aad","ede") 这个 List 里的每一个字符串分别代表一个城市 另外给一个 hashtable 分别是每一个 城市相邻的城市 比如
            aaa: acd
 abd: acd, ede. visit 1point3acres for more.
 acd: aaa, abd
 aad: ede
 ede: abd,aad. 1point3acres
 . 1point 3acres 论坛
 现在给一个新的 list 里面是一段航线 但这个航线里的城市的名字 可能是错误的 比如
 ("aaa","abq","aaa")
 现在问你 最少修改几个字母 让这个航线符合 hashtable 定义的 graph
2018年6月 (http://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=430019&ext ra=page%3D1%26filter%3Dsortid%26sortid%3D311%26searchoption%5B3089%5D %5Bvalue%5D%5B2%5D%3D2%26searchoption%5B3089%5D%5Btype%5D%3Dch eckbox%26searchoption%5B3046%5D%5Bvalue%5D%3D1%26searchoption%5B30 46%5D%5Btype%5D%3Dradio%26sortid%3D311)
积分不够!

2018年6月 (http://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=430038&ext ra=page%3D1%26filter%3Dsortid%26sortid%3D311%26searchoption%5B3089%5D %5Bvalue%5D%5B2%5D%3D2%26searchoption%5B3089%5D%5Btype%5D%3Dch eckbox%26searchoption%5B3046%5D%5Bvalue%5D%3D1%26searchoption%5B30 46%5D%5Btype%5D%3Dradio%26sortid%3D311)
      面的 SETI，工作一年跳槽，算 newgrad 吧 1.给两个 interger 的 set,找出他们的 common set
 2 find all combinations of a string
给一个 string，找出所有 char 的 combination(不止是 substring)
 . 留学申请论坛-一亩三分地
  lz 第一次店面狗家，时间掌握的不好，第一题做完只剩大概 20 分钟了(还留 5 分钟问问题)，第二
 题上来有点蒙，用 recursion 其实就能做。卡了一会要了个 hint， 等明白过来就剩 5 分钟了。 bb 了
 一下没写完。follow up 都没见到就进入问问题环节了。
 两题其实都不难，自己没准备好。共勉
2018年6月 (http://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=430114&ext ra=page%3D1%26filter%3Dsortid%26sortid%3D311%26searchoption%5B3089%5D %5Bvalue%5D%5B2%5D%3D2%26searchoption%5B3089%5D%5Btype%5D%3Dch eckbox%26searchoption%5B3046%5D%5Bvalue%5D%3D1%26searchoption%5B30 46%5D%5Btype%5D%3Dradio%26sortid%3D311)
      刚刚面的狗家，遇到的是个中国小哥，对方英语拙计又偏偏对我的简历感兴趣，结果简历就问了快十
 分钟，结果偏偏 lz 的简历里的项目和工作内容都特别细节。在多次解释，对方依旧表示没有听懂后，
 心态瞬间爆炸。.1point3acres 网
 只做了一道题:
 Input: Give 2 files
 file1:
USD, GBP, 0.69

 Meter, Yard, 1.09
YEN, EUR, 0.0077
GBP, YEN, 167.75 Horsepower, Watt, 745.7
 -google 1point3acres
 file1 表示的意思是 USD = 0.69 * GBP 以此类推
 file2: USD,EUR
 Yard,Meter
 要求输出
output:
USD,EUR, 0.89 = 0.69 * 167.75 * 0.0077
Yard,Meter, 0.91
Input: Give 2 files
file1:USD, GBP, 0.69Meter, Yard, 1.09YEN, EUR, 0.0077GBP, YEN, 167.75Horsepower, Watt, 745.7
USD = 0.69 * GBP 来源一亩.三分地论坛.
file2:USD,EURYard,Meter
output:USD,EUR, 0.89 = 0.69 * 167.75 * 0.0077Yard,Meter, 0.91 假设不会出现错误的信息，比如:USD， GBP， 0.69GBP， USD， 除了 1/0.69 以外的数字 其实是非常常见的题型，有多种解法，可能当时心态炸了，默默写完，也没顾着和小哥交流，所以就 跪了。但愿以后好运吧!求各位大佬赏大米，听说随手赠送大米的程序员，运气都不会太差 :)
 2018 年 6 月 intern (http://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=429893&ext ra=page%3D1%26filter%3Dsortid%26sortid%3D311%26searchoption%5B3089%5D %5Bvalue%5D%5B2%5D%3D2%26searchoption%5B3089%5D%5Btype%5D%3Dch eckbox%26searchoption%5B3046%5D%5Bvalue%5D%3D1%26searchoption%5B30 46%5D%5Btype%5D%3Dradio%26sortid%3D311)
      LZ 上个月面了狗家 2018 秋季实习，电面两轮+加面一轮，已挂。
 电面第一轮:国人小哥
 题目是用 string 表示的 positive int 相加。follow up 是假如有负数怎么办。只说了想法，没有写
 code。
 .留学论坛-一亩-三分地

 电面第二轮:美国小哥
 第一题是 double linked list 里 remove 一个 node，很多 corner case，要考虑清楚。
 第二题是乐扣 而零午。幸好是见过的题目，要不然时间可能来不及。
 这两面感觉都面的不错，跟面试官交流的也还行，然鹅面完 3 天通知被加面了，不知道是不是因为面
 的题目太简单了。
 加面似乎也是国人大哥。题目是: 给一个 int[]，要求你按照这个 array 里的权重 random 生成从 0 到
 array.length - 1 的数字。比如给的数列是[30, 40, 50]，那么生成的随机数就是 0，1，2，并且分别
 有 30/120, 40/120, 50/120 的概率被生成。当时比较懵逼，楞是没想到好方法。后来在面试官的提
 醒下才想到用 rand()来帮助实现。比如用 rand(120)生成一个随机数，看这个数在哪个区间，决定生
 成 0，1 还是 2。
 加面之后 5 天接到电话挂了，估计是加面没面好。祝大家好运!
2018 年 6 月 intern (http://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=429460&ext ra=page%3D1%26filter%3Dsortid%26sortid%3D311%26searchoption%5B3089%5D %5Bvalue%5D%5B2%5D%3D2%26searchoption%5B3089%5D%5Btype%5D%3Dch eckbox%26searchoption%5B3046%5D%5Bvalue%5D%3D1%26searchoption%5B30 46%5D%5Btype%5D%3Dradio%26sortid%3D311)
狗家实习电面以及 10 月到 1 月地里 google 所有面经总结分享
(http://www.1point3acres.com/bbs/thread-317620-1-1.html)
      电面第一轮 doublelinkedlist remove。
 第二轮:第一题 fix binary tree，第二题:给一个 graph 的 node，找到最短的 circle。

2018年6月 (http://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=429386&ct id=45703)
   楼主马上要面 Google，最近刷了地里的一些面经，整理了几道 Google 近期的面试题，至于解法我
 没有找到特别好的，请各位大神不吝赐教哈!希望可以帮助到要面 Google 的同学们~
 第一题:矩阵从左上角到右下角有多少种走法. 围观我们@1point 3 acres
 给定一个矩形的长宽，用多少种方法可以从左上角走到右上角 (每一步，只能向正右、右上 或 右下
 走). from: 1point3acres
 Follow up 1:如果给矩形里的三个点，要求解决上述问题的同时，经过这三个点
 Follow up 2:如何判断这三个点一定是合理的，即存在路径
 Follow up 3:如果给你一个 H，要求你的路径必须向下越过 H 这个界，怎么做
 Follow up 4:要经过某些特定 row 怎么走?要先经过一个 row 再经过另一个 row 怎么走?
 感觉可以第一问用 two pass DP 解决。Follow up 1 和 2 可以纵向切割矩阵，一个矩阵一个矩阵做
 DP。.留学论坛-一亩-三分地
 但是 Followup 3&4 我就不太会了，该咋做呢~
 第二题:01 矩阵走路问题
 0 和 1 的 grid，1 是墙，0 是路，从左上角走到右下角，最少多少步。
 Follow-up: 现在说能把 grid 中的一个 1 变成 0，问新的最小步数是多少步。.1point3acres 网
 第一问应该就是常规的 DP。第二问不知道该怎么做~
 第三题:User CPU Peak 问题
 给了一堆 log，log 里有用户 id，resource id 以 resource 在某个起始时间和终止时间的使用量，比
 如用户 abc 在 1 到 5 秒钟使用了 CPU 的数量是 2，用户 abc 在 2 到 3 秒使用的 CPU 数量是 4，也就
 是一个用户对某个 resource 的使用在某个时间是可以叠加的，给定一个 resource id，根据用户对这
 个 resource 的 peak 使用量，找到 top k 的用户。上面的例子中 abc 的 CPU 的 peak 使用量是
 2+4=6. 围观我们@1point 3 acres
 Follow up:如果数据量很大怎么办。.本文原创自 1point3acres 论坛
 感觉可以把占用 resource id 的 user id 对应 log 聚合在一起，但是怎么计算这个 peak 就想不通了，
 感觉跟 Meeting room 有点像?但不知道该怎么实现。

 第四题:拿卡片游戏
 每张卡片都有一个值，给定一堆卡片从一头拿，每次可以拿一到三张，两人轮流拿，求最高得分。考
 虑卡片值为负的情况。. 1point 3acres 论坛
 . 1point 3acres 论坛
 这个用 DP?DFS?
 第五题:是男人就跳问题
 给一个二维坐标平面和一个起始点，从这点开始垂直下跳，下面有若干水平挡板，位置长度会在
 input 里给，板的形式是(x, y, distance)，当跳到挡板上时，可以选择走到挡板的左端或者右端，然
 后继续垂直下跳，直到落地位置，求从开始到落地需要走过的路程最短是多少。
 懵逼中，有大神能讲讲做法吗~
 . 留学申请论坛-一亩三分地
  第六题:找到一条线将坐标系中的点分成两等份
 题目是在一个坐标系里给很多离散的点，找到一条线将所有点分成两个数量相等的集合，不用考虑基
 偶性或者多个点在一条线上的特殊情况。. from: 1point3acres
 . Waral 博客有更多文章,
 这是啥。。楼主数**渣。。
 第七题:随机生成一个 Candy Crush
 黑人问号脸?有大神能详细描述下题目吗?
 2018年6月
狗家面经整理 2018 年 2-3 月
 (http://www.1point3acres.com/bbs/thread-430802-1-1.html)
g