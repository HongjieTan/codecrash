package _companies.company_amazon;

import java.util.*;

/**
 * Created by thj on 2018/7/31.
 */
public class _2018_MostCommonWord {

    public String mostCommonWord(String paragraph, String[] banned) {

        Set<String> bannedSet = new HashSet<>(Arrays.asList(banned));

        Map<String, Integer> countMap = new HashMap<>();
        String[] words = paragraph.toLowerCase().split("[!?',;. ]+"); // 重点在于此，如何用regex split字符串！！
        for(String word: words) {
            if (!bannedSet.contains(word)) {
                countMap.put(word, countMap.getOrDefault(word, 0)+1);
            }
        }

        int max = Integer.MIN_VALUE;
        String res = "";
        for (String key: countMap.keySet()) {
            if (countMap.get(key) > max) {
                max = countMap.get(key);
                res = key;
            }
        }
        return res;
    }



    public static void main(String[] args) {
        String p="Bob hit a ball, the hit BALL flew far after it was hit.";
        String[] banned = {"hit"};
        System.out.println(new _2018_MostCommonWord().mostCommonWord(p, banned));


//        String[] arr = "a     b c ";
//        System.out.println(arr);
    }
}