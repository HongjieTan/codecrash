package _companies.company_amazon;

/**
 * Created by thj on 2018/8/18.
 */
public class RectangleOverlap {


    // If the rectangles do not overlap, then rec1 must either be higher, lower, to the left, or to the right of rec2.
    public boolean isRectangleOverlap_v2(int[] rec1, int[] rec2) {
        return !(rec1[2] <= rec2[0]   // rec1 is to left of rec2
                || rec1[0] >= rec2[2] // rec1 is to right of rec2
                || rec1[1] >= rec2[3] // rec1 is to top of rec2
                || rec1[3] <= rec2[1] // rec1 is to bottom of rec2
                );
    }

    public boolean isRectangleOverlap_v1(int[] rec1, int[] rec2) {

        int ax1=rec1[0], ay1=rec1[1], ax2=rec1[2], ay2=rec1[3];
        int bx1=rec2[0], by1=rec2[1], bx2=rec2[2], by2=rec2[3];

        boolean isXOverlap = isRangeOverlap(ax1, ax2, bx1, bx2);
        boolean isYOverlap = isRangeOverlap(ay1, ay2, by1, by2);

        return isXOverlap&&isYOverlap;

    }

    private boolean isRangeOverlap(int a1, int a2, int b1, int b2) {
        int minA = Math.min(a1, a2);
        int maxA = Math.max(a1, a2);
        int minB = Math.min(b1, b2);
        int maxB = Math.max(b1, b2);
        return !(minA>=maxB || minB>=maxA);
    }

    public static void main(String[] args) {

        int[] rec1 = new int[]{0,0,1,1};
        int[] rec2 = new int[]{1,0,2,1};


        System.out.println(new RectangleOverlap().isRectangleOverlap_v2(rec1, rec2));

    }




}
