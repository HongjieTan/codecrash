package _companies.company_amazon;

import common.Point;

import java.util.*;

/**
 * Created by thj on 2018/7/27.
 */
public class _2018_KClosestPoints {


    /*
        另一种版本: check images/2-1&2-2
     */
    public List<Point> kClosestLocations(List<Point> allLocs, int k) {

        Comparator<Point> comparator = new Comparator<Point>() {
            @Override
            public int compare(Point o1, Point o2) {
                int d1 = o1.x * o1.x + o1.y*o1.y;
                int d2 = o2.x * o2.x + o2.y*o2.y;
                if (d1 == d2) return Integer.compare(o2.x, o1.x);
                else return Integer.compare(d2, d1); // !! 大顶推！顶元素是最大
            }
        };

        PriorityQueue<Point> heap = new PriorityQueue<>(comparator);

        for(Point p: allLocs) {
            heap.add(p);
            k--;

            if (k<0) {
                heap.poll();
                k++;
            }
        }

        List<Point> res = new ArrayList<>();
        while (!heap.isEmpty()) res.add(heap.poll());

        return res;
    }

    public static void main(String args[]) {
        List<Point> locs = Arrays.asList(new Point(1,2), new Point(3, 4), new Point(1, -1));

        List<Point> res = new _2018_KClosestPoints().kClosestLocations(locs, 2);
        System.out.println("asdf");

    }



    /**
     * @param points: a list of points
     * @param origin: a point
     * @param k: An integer
     * @return: the k closest points
     */
    public Point[] kClosest(Point[] points, Point origin, int k) {
        // write your code here
        Point[] res = new Point[k];
        int index = k-1;


        PriorityQueue<Point> heap = new PriorityQueue<>((o1, o2) -> {
            double d1 = distance(o1, origin);
            double d2 = distance(o2, origin);
            if (d1 != d2) {
                return Double.compare(d2, d1);
            } else {
                return Integer.compare(o2.x, o1.x);
            }
        });

        for (int i = 0; i < points.length; i++) {
            heap.add(points[i]);
            k--;
            if (k<0) {
                heap.poll();
            }
        }

        while (!heap.isEmpty()) {
            res[index--] = heap.poll();
        }

        return res;
    }


    private double distance(Point a, Point b) {
        return Math.sqrt(Math.pow(a.x-b.x, 2) + Math.pow(a.y-b.y, 2));
    }
}
