package _companies.company_amazon;

import common.ListNode;

/**
 * Created by thj on 2018/7/30.
 *
 *
 *  TODO
 *
 *  reverse right half linkedlist
 *  example: 2->1->3->4->5->6->7->8 变成 2->1->3->4->8->7->6->5 ；
 *  如果总是为奇数，中间的也要变 5->7->8->6->3->4->2 变成 5->7->8->2->4->3->6 很简单就不多说了
 */
public class ReverseRightHalfLinkedList {

    public static ListNode reverseRightHalf(ListNode head) {
        if (head == null) return null;

        ListNode cur, prev, part1Tail, part2Head, part2Tail;

        int n = 0;
        cur = head;
        while (cur!=null) {
            n++;
            cur=cur.next;
        }


        // first half
        prev = null;
        cur = head;
        for (int i = 0; i < n/2; i++) {
            prev = cur;
            cur = cur.next;
        }
        part1Tail = prev;


        // second half - reverse
        for (int i=n/2; i<n; i++) {
            if (i>n/2) {
                ListNode nextTemp = cur.next;
                cur.next = prev;
                prev = cur;
                cur = nextTemp;
            } else {
                prev = cur;
                cur = cur.next;
            }
        }
        part2Head = prev;
        part2Tail = part1Tail.next;

        // connect
        part1Tail.next = part2Head;
        part2Tail.next = null;

        return head;

    }


    public static void main(String[] args) {
////        2->1->3->4->5->6->7->8
//        ListNode n1 = new ListNode(2);
//        ListNode n2 = new ListNode(1);
//        ListNode n3 = new ListNode(3);
//        ListNode n4 = new ListNode(4);
//        ListNode n5 = new ListNode(5);
//        ListNode n6 = new ListNode(6);
//        ListNode n7 = new ListNode(7);
//        ListNode n8 = new ListNode(8);
//        n1.next=n2;n2.next=n3;n3.next=n4;n4.next=n5;n5.next=n6;n6.next=n7;n7.next=n8;


        // 5->7->8->6->3->4->2
        ListNode n1 = new ListNode(5);
        ListNode n2 = new ListNode(7);
        ListNode n3 = new ListNode(8);
        ListNode n4 = new ListNode(6);
        ListNode n5 = new ListNode(3);
        ListNode n6 = new ListNode(4);
        ListNode n7 = new ListNode(2);
        n1.next=n2;n2.next=n3;n3.next=n4;n4.next=n5;n5.next=n6;n6.next=n7;

        reverseRightHalf(n1);

        System.out.println();
    }



}
