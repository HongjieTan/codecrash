package _companies.company_amazon;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigInteger;
import java.util.Scanner;

/**
 * Created by thj on 2018/7/9.
 *
 *  amazon hackerrank
 *  https://www.hackerrank.com/challenges/stockmax/problem
 *
 *
 *  each day you can:
 *    1. buy one share
 *    2. sell any numbers of share
 *    3. no op
 */
public class StockMax {

    // Complete the stockmax function below.
    static BigInteger stockmax(int[] prices) {

        int dp[] = new int[prices.length]; // dp[i]: max value on right of prices[i] (not include) i,i+1,i+2,...,n
        dp[prices.length-1] = prices[prices.length-1];

        for (int i=prices.length-2; i>=0;i--) {
            dp[i] = Math.max(prices[i], dp[i+1]);
        }

        BigInteger profit = BigInteger.ZERO;
        for (int i=0; i<prices.length; i++) {
            if (dp[i]>prices[i]) {
                profit = profit.add(BigInteger.valueOf(dp[i]-prices[i]));
            }
        }

        return profit;
    }


    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
//        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.out));
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("input11.txt"));

        int t = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int tItr = 0; tItr < t; tItr++) {
            int n = scanner.nextInt();
            System.out.println("->"+n);
            scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

            int[] prices = new int[n];

            String[] pricesItems = scanner.nextLine().split(" ");
            scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

            for (int i = 0; i < n; i++) {
                int pricesItem = Integer.parseInt(pricesItems[i]);
                prices[i] = pricesItem;
            }

            BigInteger result = stockmax(prices);
            System.out.println(result);

            bufferedWriter.write(String.valueOf(result));
            bufferedWriter.newLine();
        }

        bufferedWriter.close();

        scanner.close();
    }

//    public static void main(String[] args) {
////        System.out.println(stockmax(new int[]{5, 3, 2}));
////        System.out.println(stockmax(new int[]{1, 2, 100}));
//        System.out.println(stockmax(new int[]{30887, 92778, 36916, 47794, 38336, 85387, 60493, 16650, 41422, 2363, 90028, 68691, 20060, 97764, 13927, 80541, 83427, 89173, 55737, 5212, 95369, 2568, 56430, 65783, 21531, 22863, 65124, 74068, 3136, 13930, 79803, 34023, 23059, 33070, 98168, 61394, 18457, 75012, 78043, 76230, 77374, 84422, 44920, 13785, 98538, 75199, 94325, 98316, 64371, 66414, 3527, 76092, 68981, 59957, 41874, 6863, 99171, 6997, 97282, 2306, 20926, 77085, 36328, 60337, 26506, 50847, 21730}));
//    }

}
