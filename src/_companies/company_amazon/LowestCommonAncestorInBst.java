package _companies.company_amazon;

import common.TreeNode;

/**
 * Created by thj on 2018/7/25.
 */
public class LowestCommonAncestorInBst {

    public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
        if (root==null) return null;
        if (root.val>p.val && root.val>q.val) return lowestCommonAncestor(root.left, p, q);
        else if (root.val<p.val && root.val<q.val) return lowestCommonAncestor(root.right, p, q);
        else return root;
    }
}
