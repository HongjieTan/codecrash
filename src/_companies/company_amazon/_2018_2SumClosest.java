package _companies.company_amazon;

import java.util.Arrays;

/**
 Given an array nums of n integers, find two integers in nums such that the sum is closest to a given number, target.
 Return the difference between the sum of the two integers and the target.
 Example: Given array nums = [-1, 2, 1, -4], and target = 4.
 The minimum difference is 1. (4 - (2 + 1) = 1).
 Do it in O(nlogn) time complexity.


 solution:  two pointers problem...

 */
public class _2018_2SumClosest {

    public static int twoSumCloset(int[] nums, int target) {
        Arrays.sort(nums);

        int l=0, r=nums.length-1, diff=Integer.MAX_VALUE;

        while (l<r) {
            int sum = nums[l] + nums[r];

            if (sum > target) {
                r--;
            } else {
                l++;
            }


            diff = Math.min(diff, Math.abs(sum-target));
        }

        return diff;
    }


    public static void main(String[] args) {
        int[] nums = {-1, 2, 1, -4};
        System.out.println(twoSumCloset(nums, 4));
    }
}
