package _companies.company_amazon;

import common.TreeNode;

/**
 * Created by thj on 2018/7/24.
 *
 * !!!
 */
public class LowestCommonAncestor {

    /*
     * @param root: The root of the binary search tree.
     * @param A: A TreeNode in a Binary.
     * @param B: A TreeNode in a Binary.
     * @return: Return the least common ancestor(LCA) of the two nodes.
     *
     *
     *
     *   understanding of lowestCommonAncestor:
     *      if p,q are ensured to be both inside root:       then return the LCA
     *      if p,q are not ensured to be both inside root:   then return the node of p or q
     *
     *   On
     */
    public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
        if (root == null ) return null;
        if (root == p || root == q) return root;
        TreeNode leftLCA = lowestCommonAncestor(root.left, p, q);
        TreeNode rightLCA = lowestCommonAncestor(root.right, p, q);
        if (leftLCA != null && rightLCA != null) return root;
        return leftLCA!=null?leftLCA:rightLCA;
    }



    // iterative version


    public static void main(String[] args) {
        TreeNode root = new TreeNode(3);
        root.left = new TreeNode(5);
        root.right = new TreeNode(1);
        System.out.println(new LowestCommonAncestor().lowestCommonAncestor(root, root.left, root.right).val);
    }

}
