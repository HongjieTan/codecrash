package _companies.company_amazon;

import javafx.util.Pair;

import java.util.*;

/**
 *  shortest distance from source to target : use BFS
 */
public class _2018_ShortestDistance {

    /*
       另外一个变种， 见images/1-1&1-2
     */
    public static int  getShortestDistance_v2(int rows, int cols, int[][] lot) {

        LinkedList<Pair<Integer, Integer>> que = new LinkedList<>();

//        Set<String> visited = new HashSet<>();
        boolean[][] visited = new boolean[rows][cols];
        que.add(new Pair<>(0, 0));

        int depth=0;
        while (!que.isEmpty()) {
            depth++;

            int levelSize = que.size();
            for (int i = 0; i < levelSize; i++) {
                Pair curPair = que.pop();
                int curRow = (int) curPair.getKey();
                int curCol = (int) curPair.getValue();

                visited[curRow][curCol] = true;
                if (lot[curRow][curCol]==9) return depth-1;

                if (!visited[curRow+1][curCol] && isPosValid(curRow+1, curCol, lot) && lot[curRow+1][curCol]!=0) que.add(new Pair<>(curRow+1, curCol));
                if (!visited[curRow-1][curCol] && isPosValid(curRow-1, curCol, lot) && lot[curRow-1][curCol]!=0) que.add(new Pair<>(curRow-1, curCol));
                if (!visited[curRow][curCol+1] && isPosValid(curRow, curCol+1, lot) && lot[curRow][curCol+1]!=0) que.add(new Pair<>(curRow, curCol+1));
                if (!visited[curRow][curCol-1] && isPosValid(curRow, curCol-1, lot) && lot[curRow][curCol-1]!=0) que.add(new Pair<>(curRow, curCol-1));

            }
        }

        return -1;
    }

    private static boolean isPosValid(int row, int col, int[][] lot) {
        return row>=0 && row<lot.length && col>=0 && col<lot[0].length && lot[row][col] != 0;
    }




    /*
     题目的意思是要去送货，假设是在一个二维的地图上，图上的点也是用List<List<Integer>>表示。
     每个坐标上的点有三种，0，1，2. 0表示可以到达， 1表示没有路可以去，2表示是要送的货的终点。
     每次都是从{0，0}这个点出发。最后要求求出最短的路径。应该是跟利口那道bfs的题一样的。

     题目要求的是从起点到终点的最短距离

     0, 0, 0, 0, 0
     0, 0, 1, 1, 0
     1, 0, 0, 1, 0
     0, 1, 0, 1, 2
     0, 1, 0, 0, 0
     */
    public static int getShortestDistance(int[][] matrix) {
        LinkedList<Pair<Integer, Integer>> que = new LinkedList<>();
        que.add(new Pair<>(0,0));

        int level = 0;

        boolean[][] visited = new boolean[matrix.length][matrix[0].length];

        while (!que.isEmpty()) {
            List<Pair<Integer, Integer>> levelList = new ArrayList<>();
            while (!que.isEmpty()) levelList.add(que.poll());
            for (int i = 0; i < levelList.size(); i++) {
                int row = levelList.get(i).getKey();
                int col = levelList.get(i).getValue();

                visited[row][col] = true;

                if (matrix[row][col]==2) return level;

                if (isValid(row-1, col, matrix) && !visited[row-1][col] && matrix[row-1][col]!=1) que.add(new Pair<>(row-1, col));
                if (isValid(row+1, col, matrix) && !visited[row+1][col] && matrix[row+1][col]!=1) que.add(new Pair<>(row+1, col));
                if (isValid(row, col-1, matrix) && !visited[row][col-1] && matrix[row][col-1]!=1) que.add(new Pair<>(row, col-1));
                if (isValid(row, col+1, matrix) && !visited[row][col+1] && matrix[row][col+1]!=1) que.add(new Pair<>(row, col+1));
            }

            level++;
        }

        return -1;
    }

    private static boolean isValid(int row, int col, int[][] matrix) {
        return row>=0 && row<matrix.length && col>=0 && col<matrix[0].length;
    }


    public static void main(String[] args) {
//        int[][] matrix = new int[][]{
//                {0, 0, 0, 0, 0},
//                {0, 0, 1, 1, 0},
//                {1, 0, 0, 1, 0},
//                {0, 1, 0, 1, 2},
//                {0, 1, 0, 0, 0}
//        };
//
//        System.out.println(getShortestDistance(matrix));


        int[][] lot = new int[][]{
                {1, 0, 0},
                {1, 0, 0},
                {1, 9, 1}
        };

        System.out.println(getShortestDistance_v2(3, 3, lot));
    }


}
