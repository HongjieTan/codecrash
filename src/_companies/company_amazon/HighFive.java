package _companies.company_amazon;

import java.util.*;

/**
 * Created by thj on 2018/7/26.
 *
 *
 *
 Given a list of test results (each with a test date, Student ID, and the student’s Score),
 return the Final Score for each student. A student’s Final Score is calculated as the average
 of his/her 5 highest test scores. You can assume each student has at least 5 test scores.

 You may use the JDK or the standard template library. The solution will be evaluated on correctness,
 runtime complexity (big-O), and adherence to coding best practices. A complete answer will include the following:

 Document your assumptions
 Explain your approach and how you intend to solve the problem
 Provide code comments where applicable
 Explain the big-O run time complexity of your solution. Justify your answer.
 Identify any additional data structures you used and justify why you used them.
 Only provide your best answer to each part of the question.

 */

class TestResult {
    int studentId;
    String testDate;
    int testScore;
    public TestResult (int studentId, int testScore) {
        this.studentId = studentId;
        this.testScore = testScore;
    }
}

public class HighFive {

    static Map<Integer, Double> calculateFinalScores(List<TestResult> results) {
        if (results==null || results.size()==0) return null;

        Map<Integer, PriorityQueue<Integer>> scoresQueueMap = new HashMap<>();
        for (TestResult result: results) {
            PriorityQueue<Integer> scores = scoresQueueMap.get(result.studentId);
            if (scores == null) {
                scores = new PriorityQueue<>();
                scoresQueueMap.put(result.studentId, scores);
            }

            scores.add(result.testScore);
            if (scores.size()>5) {
                scores.poll();
            }

        }

        Map<Integer, Double> res = new HashMap<>();
        for(int studentId: scoresQueueMap.keySet()) {
            double finalScore = 0;
            PriorityQueue<Integer> scores = scoresQueueMap.get(studentId);
            while (!scores.isEmpty()){
                finalScore += scores.poll();
            }
            res.put(studentId, finalScore/5);
        }

        return res;
    }

    public static void main(String[] args) {
        TestResult r1 = new TestResult(1, 95);
        TestResult r2 = new TestResult(1, 95);
        TestResult r3 = new TestResult(1, 91);
        TestResult r4 = new TestResult(1, 91);
        TestResult r5 = new TestResult(1, 93);
        TestResult r6 = new TestResult(1, 105);

        TestResult r7 = new TestResult(2, 6);
        TestResult r8 = new TestResult(2, 6);
        TestResult r9 = new TestResult(2, 7);
        TestResult r10 = new TestResult(2, 6);
        TestResult r11 = new TestResult(2, 6);
        TestResult[] arr = {r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11};
        Map<Integer, Double> res = calculateFinalScores(Arrays.asList(arr));

        System.out.println(res.get(1) + " " +res.get(2));
    }
}
