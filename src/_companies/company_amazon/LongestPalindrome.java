package _companies.company_amazon;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by thj on 2018/7/26.
 */
public class LongestPalindrome {

    public int longestPalindrome(String s) {
        // write your code here

        if (s==null || s.isEmpty()) return 0;
        Map<Character, Integer> map = new HashMap<>();

        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (!map.containsKey(c)) {
                map.put(c, 0);
            }
            map.put(c, map.get(c)+1);
        }


        int odds = 0;
        for (char c: map.keySet()) {
            if (map.get(c)%2==1) {
                odds+=1;
            }
        }

        return odds==0?s.length():s.length()-odds+1;

    }

}
