OA部分：
- min distance from source to target, check images/1.png







leetcode(amazon分类)
http://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=438680#lastpost


2018(7-9月)  http://www.1point3acres.com/bbs/thread-435513-1-1.html
1. 类似于 leetcode819. Most Common Word不同的是输出，要输出一个list包含所有common words，只出现一次的word不算。不用排序。
2. log file 输入是List<String>，输出要把只有word的行放前面,按字母顺序排序，有数字的行放后面,按数字大小排序。
这道题我以前没做过，就用了最笨的办法。分析了每一行，把数字的行加入一个list，没有数字的加入另一个list。
然后对两个分别排序，用Collections.sort.要自定义Comparator，把每一行的第一个词（id）去掉,再排序即可。因为这一题没做过，有点慌，没有优化代码。
只知道给出的testcase过了。


2018(7-9月) onsite
1. 利口留其舞 + BQ  (lt675..hard..todo)
2. 李寇易思玲 + BQ   (lt140 word break ii)
3. manager 纯BQ + 该岗位工作内容 + 简历工作经历
组里一个junior带着吃饭，纯聊
4. 里抠而亦柳 + BQ
5. 大组下另一个小组manager，system _OODesign 亚麻locker，主要设计其中的一个核心servic。当系统很多层面出问题时，比如locker坏了，快递员晕了，下单错了等等。
   自己提了message queue，push pull等概念。
   感觉面试官更侧重问robust，以及是否work，并没有问scability，我自己提了scability，面试官说先不考虑那个

之前是OA直接昂赛，用了一个月准备。第四轮那个很久前做过，磕磕绊绊算是现场做出来了，其他的提都比较顺利。
. 1point3acres

补充内容 (2018-8-1 23:47):
楼主的小小建议：代码风格 + 现场交流 + 主动说话 + 注意倾听

2018(7-9月) onsite
1. BQ+系统设计        BQ: 介绍自己，为什么亚麻？等等
    SD: 设计一个服务来获取亚麻产品图片。
    @hg93011917 我是按GFS的框架去答的。大概就是图片按产品号hash分到不同的slave servers。然后master server保存一个{产品号hash:slave server地址}的列表。然后request进来先问master找地址，然后再问slave找图片。然后就是各种cache什么的。
2. BQ+数据结构+算法        BQ: 最自豪项目 等等
    DS&Alg:     - 设计两个接口：一个用来获取魔方的状态，一个转动魔方。问了用什么数据结构来表示魔方。
     - 利口斯巴
3 产品经理轮，全BQ        BQ: 给个挑战你经理的例子，给个你需要在项目中学习新知识的例子 等等
4 BQ+数据结构        BQ: 忘了。大概也是最自豪项目类的。        DS: 实现一个哈希表。
5 BQ+算法 (疑似抬杠人)        BQ：忘了。        DS: 利口三姨。跟进：递增改递减。


2018(7-9月) onsite
面的是湾区的组，但是放到了虾图面。一共4轮，三轮烙印，凉凉送给自己。
1. bq  + 题 一个arr存bit，一个数组的list，每个数组两个数字，求arr里对应的两个index的这段range的或值。要求算法复杂度O(n)。.留学论坛-一亩-三分地
2. bq + 系统设计：停车场
3. bq + 蠡口 姚伞玲
4. bq + json parser
每个题 大概都只给了15min。。。. 牛人云集,一亩三分地
卤煮蠡口 没练的很熟。。时间感觉很紧。。

2018（4-6月）
1） behavior question每轮都被问到，问题有和manager， 同事意见不一致怎么办，deadline来了完不成project怎么办，最好准备一些例子
2） system design，Google的三架马车和Amazon dynamo paper看完基本就能应付，无非是如何支持scalability&availability。知识是一个方面，另外要和面试官多交流
3)  coding题可参考leetcode word search I and II，. visit 1point3acres for more.
4) 本人有工作经验，有一个面试官对我以前的工作很感兴趣，几乎问了个遍，所以自己简历上列出的项目一定要准备好。这轮本来是coding，结果变成deep dive。面试结束后又在会议室外面聊了一会。

2018
1. 白鸽，BQ + 设计news feed
2. 三姐+国人小姐姐，BQ+ typehead(要求实现prefix search， 输出同一个prefix的所有单词，用trie写的。follow up是如何设计)
3. 白鸽+老墨大叔，BQ + 除法不用除号
4. 白鸽bar rasier，BQ + 刷题平台第叁题

2018(7-9月)
4轮，每轮25分钟左右的BQ，然后coding或者design，再然后10分钟左右的问问题.
第一轮： BQ + 数岛
第二轮： BQ + design Instagram (manager 要求多讲讲scale问题).
第三轮： BQ + 给一个list of list， 看看所有能不能都连起来，如果list之间有相同的element就能连起来
             一开始讲了错的solution，后来改用类似union find来解决，最后代码没写完，就给过了例子，然后就进入问问题.
第四轮： BQ + top 3-sequence，给一个log file【time， custom，page】，假设最终只会有一个答案

2018(7-9月)
7月13号面的，一共五轮：
1）中国小姐姐，bq + 一道题如下：
输入是固定数量的tasks，task本身具有不同的type；工人分为：固定工种和特殊工种。其中固定工种对应不同的task type，一种工种只能干同一个type的task，价格便宜；特殊工种的工人可以干所有type task，价格昂贵；所有工人固定只能工作8个小时。
问题：求如何安排这些tasks，做到 1）所用的不同种类的工人最少；2）所耗费的费用最低。
另：不要求最优解
2）两个manger 一个白人 + 韩国人面，纯bq
3）印度小伙儿，bq + 题目：. visit 1point3acres for more.
给定一个 m x n matrix，每一个row sorted，然后要求分别从没一row娶一个数字，然后从这n个数字里求出 diff = 最大值 - 最小值
问题：找出所有diff里面最小的值
4）印度大叔，bq + 系统设计题：
先问熟悉的设计模式有哪些，然后问如何限制数据库的链接数目；设计一个online reading的system. more info on 1point3acres
5）土耳其大叔，Sr. Manager bq + 系统设计题：
设计一个twtter news feed

2018.7-9
第一轮： 两个小哥， bq + word ladder
第二轮： hm bq + system design random number generator
第三轮： 烙印 bq + number of island
第四轮： 烙印 bq + zigzag + 字符位移

2018.7-9
一共四轮
第一轮 系统设计一个打车app
第二轮 best time sell stock
第三轮 最大不重复子字符串
第四轮 number of island

2015:
LZ前一阵子，面试亚马逊。自己把地里面的Amazon所有的题目总结了一下，一共大概121页。楼主面试前看了一遍，结果面试时几乎全是原题，已获Amazon offer。下面是我的onsite 题目。以及自己的总结。
楼主快没有米了，走过、路过的赏点米

一共五轮，每轮45分钟，没两轮之间没有break，但是你可以花上3minute左右喝点水和去卫生间
第一轮：（烙印）
hiring manager面试的，也是我以后mobile team的manager。没有coding。对着我的简历，一个项目一个项目的去问。 每个技术细节问的很深，比如细到这个请求网络的服务是用什么Framework类来实现了，这个Framework类比着别的Framework类有什么优点？另外，他对我的一个多媒体播放的一个项目特别感兴趣。问了好到技术细节，以及其中的一个我们设计的很关键的算法。我给他这whiteboard上面画图详细的描述我们当初设计的这个算法。结果这个面试官一眼找出了我们设计的一个漏洞。我告诉他这个问题我至今还没有解决，他也没有继续追问下去。

第二轮：一个年轻的印度三哥。
这货上来也没有寒暄，也没有微笑，直接说题目。我当时也挺紧张的，感觉这货要挂我。
他的英语我也没有听的很懂。我反复给说他pardon 和Do you mean by ….? 无数次之后才弄明白他的题目意思。 他的题目的意思是：输入a list of sections, list
, 在每一个section中有很多product，每一个product有两个属性：user，relevancy ，你可以通过调用系统API得到这两个值。 每一个section有score所有的product的score之和，每一个product的score是user * relevancy 。 最后给这个list of section排序，使得score大的section在前面。 这个题目其实很简单，但是他前面描述这个问题花了很多时间。
第二题：设计一个 in memory cache system，支持
1. capacity（这个system有一定的容量）
2. TTL（time to live）
3. LRU (least recently unused)
参考leetcode https://leetcode.com/problems/lru-cache/
follow ups:让你扩展到一个distributed in memory cache system怎么设计

第三轮：老美
继续问简历上面的一个项目，然后给了一个leetcode的原题https://leetcode.com/problems/surrounded-regions/。
由于问我简历，问的时间太久了，结果算法题目的时间就很短了，算法最后没有在whiteboard写完，但是思路给他们解释清楚了。

第四轮：老美
第一题：给我展示了他们组做的一个APP: Myhabit,（可以在AppStore上面下载看看），面试官给我指着Myhabit上面的一些功能，让我去自己设计。在白板上写出自己的思路。
第二题目： linkedlist 每个节点多一个random pointer。leetcode原题。我用hashMap做出来的。

第五轮：（老美）
第一题： 设计一个大楼的电梯（Amazon经典面试题目，面试前面我专门总结了一下^_^）
第二题：继续问简历上面的项目。

总体感觉，对自己以前做过的简历上面的项目一定要熟悉。两道算法都是leetcode中等题目。回答问题时最好要以customer为中心。



*******************************************************
system design 面筋总结：
-  系统设计：停车场 parkinglot
-  设计news feed
-  tinyurl(or Pastebin)
-  设计一个大楼的电梯（Amazon经典面试题目，面试前面我专门总结了一下^_^）

-  系统设计一个打车app
-  online reading的system
-  system _OODesign 亚麻locker，主要设计其中的一个核心servic。当系统很多层面出问题时，比如locker坏了，快递员晕了，下单错了等等。
      自己提了message queue，push pull等概念。
      感觉面试官更侧重问robust，以及是否work，并没有问scability，我自己提了scability，面试官说先不考虑那个
-  design a video streaming system
-  design Instagram (manager 要求多讲讲scale问题).



******************************************************
prepare：
- http://massivetechinterview.blogspot.com/2015/06/itint5.html 看下经验？

system design:
- tinyurl(or Pastebin): ok
    - https://blog.kamranali.in/system-design/url-shortner (https://www.youtube.com/watch?v=fMZMm_0ZhK4)
    - https://github.com/donnemartin/system-design-primer/blob/master/solutions/system_design/pastebin/README.md
- twitter news feed  ok
    - https://github.com/donnemartin/system-design-primer/tree/master/solutions/system_design/twitter
    - notice the fan-out ...
- uber: roughly ok
    - see youtube
- vedio？
- instgram？
- scaling aws ?
    - https://github.com/donnemartin/system-design-primer/blob/master/solutions/system_design/scaling_aws/README.md
- consistent hashing: ok
    - http://www.acodersjourney.com/2017/10/system-design-interview-consistent-hashing/

oo design:
- parkinglot: ok
   - https://www.geeksforgeeks.org/design-parking-lot-using-object-oriented-principles/
   - https://www.youtube.com/watch?v=DSGsa0pu8-k&list=PLA8lYuzFlBqAy6dkZHj5VxUAaqr4vwrka&index=4
   - https://github.com/donnemartin/system-design-primer/blob/master/solutions/object_oriented_design/parking_lot/parking_lot.ipynb
- elevator: ok
   - https://github.com/joeblau/sample-elevator-control-system
   - http://massivetechinterview.blogspot.com/2015/07/thought-works-object-oriented-design.html   ！！！
   - https://medium.com/system-designing-interviews/design-a-elevator-system-fc5832ca0b8b
   - https://www.1point3acres.com/bbs/thread-147010-1-1.html
        电梯那题是很经典的oo design哇，就是设计一个电梯系统，我写了building，floor， button， elevator几个class。然后用了一个queue装这个elevator接下来要去几层这样。你可以设计的很复杂，也可以很简单。我写了最简单的就是，queue里面是先来后到的。
        我记得还有一种比较复杂（但是智能）的设计是用状态机。= =然后我觉得太复杂惹就没有管它直接写了最简单的~
   - https://www.careercup.com/question?id=1808669

- chess game: ok
    - http://massivetechinterview.blogspot.com/2015/07/design-chess-game-using-oo-principles.html
    - https://codereview.stackexchange.com/questions/71790/design-a-chess-game-using-object-oriented-principles
    - https://www.javacodegeeks.com/2014/10/understanding-strategy-pattern-by-designing-game-of-chess.html

- online reading：basic ok
- vending machine: ok
- deck card: basic ok


