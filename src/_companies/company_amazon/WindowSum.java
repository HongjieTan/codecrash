package _companies.company_amazon;

/**
 * Created by thj on 2018/7/26.
 *
 *
 *
 Given array: [1,2,3,4,5,6,7]
 Given window: 3
 Output: [6,9,12,15,18]
 */
public class WindowSum {

    public int[] sum(int[] nums, int window) {

        if (nums==null) return null;
        if (window > nums.length) return nums;

        int[] res = new int[nums.length-window+1];

        int curSum=0;
        for (int i = 0; i < window; i++) {
            curSum += nums[i];
        }

        for (int i = 0; i < res.length; i++) {
            if (i>0) {
                curSum-=nums[i-1];
                curSum+=nums[i+window-1];
            }
            res[i] = curSum;
        }

        return res;
    }

    public static void main(String[] args) {
        int[] res = new WindowSum().sum(new int[]{1,2,3,4,5,6,7}, 3);
        System.out.println(res);
    }

}
