package _companies.company_amazon;

import java.util.*;

/**
 *
 给一个string, 一个数字k，要求找到这个string中所有的substring，满足每一个substring都是k的长度并且是k个不同的字母
 */
public class _2018_KDistinctSubString {

    public static List<String> getDistinctSubString(String str, int k) {

        List<String> ans = new ArrayList<>();
        Map<Character, Integer> countMap = new HashMap<>();

        int l=0, r=k-1;
        for (int i=l; i<=r ; i++) {
            countMap.put(str.charAt(i), countMap.getOrDefault(str.charAt(i), 0) + 1);
        }
        if (countMap.size()==k) ans.add(str.substring(l, r+1)); // target


        // slide window
        l++;r++;
        while (r<str.length()) {
            char toReduceKey = str.charAt(l-1);
            char toAddKey = str.charAt(r);

            // remove old
            if (countMap.get(toReduceKey) == 1) {
                countMap.remove(toReduceKey);
            } else {
                countMap.put(toReduceKey, countMap.get(toReduceKey)-1);
            }

            // add new
            countMap.put(toAddKey, countMap.getOrDefault(toAddKey, 0)+1);

            // check target
            if (countMap.size()==k)  ans.add(str.substring(l, r+1));

            // slide on
            l++;r++;
        }

        return ans;
    }

    public static void main(String[] args) {
        String str = "abadeabceecferfgcvddsf";
        int k = 5;
        List<String> ans = getDistinctSubString(str, k);

        for (int i = 0; i < ans.size(); i++) {
            System.out.println(ans.get(i));

        }

    }
}
