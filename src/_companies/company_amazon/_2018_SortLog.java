package _companies.company_amazon;

import java.util.*;

/**
 给你一个log file，List<String>，每个元素代表log file一行，在每个元素里面，有substring，他们用空格分开，比如"a1ws efg7 i90s",
 String里面只有字母和数字，第一个substring是id，依据id后面的substring给log file排序，字母在top。
 比如input是
 "fhie 1df8 sfds"
 "fdsf 2def sees"
 "efe2 br9o fjsd"
 “asd1 awer jik9"

 output是：
 “asd1 awer jik9"
 "efe2 br9o fjsd"
 "fhie 1df8 sfds"
 "fdsf 2def sees"
 */
public class _2018_SortLog {

    public static List<String> sortLog(List<String> logs) {

        Comparator<String> comparator = new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                String sub1 = o1.split(" ")[1];
                String sub2 = o2.split(" ")[1];

                int p=0, q=0;
                while (p<sub1.length()) {
                    char c1 = sub1.charAt(p);
                    char c2 = sub2.charAt(q);

                    if (Character.isLetter(c1) && !Character.isLetter(c2)) {
                        return -1;
                    } else if (Character.isLetter(c2) && !Character.isLetter(c1)) {
                        return 1;
                    } else {
                        int res = Integer.compare(c1, c2);
                        if (res!=0) {
                            return res;
                        }
                    }

                    p++;q++;
                }


                return 0;
            }
        };


        Collections.sort(logs, comparator);
//        PriorityQueue<String> heap = new PriorityQueue<>(comparator);
//
//        for (int i = 0; i < logs.size(); i++) {
//            heap.add(logs.get(i));
//        }
//
//        List<String> ans = new ArrayList<>();
//        while (!heap.isEmpty()) {
//            ans.add(heap.poll());
//        }

        return logs;
    }

    public static void main(String[] args) {
        List<String> logs = new ArrayList<>();
        logs.add("fhie 1df8 sfds");
        logs.add("fdsf 2def sees");
        logs.add("efe2 br9o fjsd");
        logs.add("asd1 awer jik9");

        System.out.println(sortLog(logs));

    }


}
