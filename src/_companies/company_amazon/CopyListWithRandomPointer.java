package _companies.company_amazon;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by thj on 2018/7/26.
 */

class RandomListNode {
    int label;
    RandomListNode next, random;
    RandomListNode(int x) { this.label = x; }
}

public class CopyListWithRandomPointer {

    public RandomListNode copyRandomList(RandomListNode head) {

        if (head == null) return null;
        Map<RandomListNode, RandomListNode> map = new HashMap<>();

        RandomListNode node = head;
        while (node!=null) {
            map.put(node, new RandomListNode(node.label));
            node=node.next;
        }

        node = head;
        while (node!=null) {
            map.get(node).next = map.get(node.next);
            map.get(node).random = map.get(node.random);
            node=node.next;
        }

        return map.get(head);
    }


    public static void main(String[] args) {

        RandomListNode n1 = new RandomListNode(1);
        RandomListNode n2 = new RandomListNode(2);
        RandomListNode n3 = new RandomListNode(3);
        n1.next=n2; n1.random=n3;
        n2.next=n3; n2.random=n1;
        n3.next=null; n3.random=null;

        long t1 = System.currentTimeMillis();
        new CopyListWithRandomPointer().copyRandomList(n1);
        long t2 = System.currentTimeMillis();
        System.out.println(t2-t1);

    }
}
