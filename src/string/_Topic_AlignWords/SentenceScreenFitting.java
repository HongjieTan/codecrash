package string._Topic_AlignWords;

/**
 * 直接的方法是每次扫描一行，尝试能放几个，这样时间复杂度会高一点．
 * 另外一种方法是把所有的字符串都加起来，然后每次看如果位移一整行的距离是否正好落在这个字符串的空格位置，如果不是的话就退后，直到遇到一个空格．
 */
public class SentenceScreenFitting {

    // O(rows * wordLen)
    public int wordsTyping_x2_best(String[] sentence, int rows, int cols) {
        StringBuilder sb = new StringBuilder();
        for(String word:sentence) sb.append(word).append(" ");
        int index=0;
        int row=0;
        while (row<rows) {
            while (sb.charAt((index+cols)%sb.length())!=' ') index--;
            index += cols+1;
            row++;
        }
        return index/sb.length();
    }

    // brute force: O(rows * cols / wordLen)
    public int wordsTyping_bruteforce(String[] sentence, int rows, int cols) {
        int wordIndex=0;
        int row=0, col=-1;
        while(row<rows) {
            String word = sentence[wordIndex%sentence.length];
            if (col+1+word.length()<=cols) { // fit into line
                wordIndex++;
                col+=1+word.length();
            } else { // exceed line
                col=-1;
                row++;
            }
        }
        return wordIndex/sentence.length;
    }



















    // improved!!!! ：O(rows * k) , k: max word len
    public int wordsTyping(String[] sentence, int rows, int cols) {
        StringBuffer str = new StringBuffer();
        for (String s: sentence) {
            str.append(s).append(" ");
        }

        int pos = 0;
        for (int i = 0; i < rows; i++) {
            pos+=cols;
            if (str.charAt(pos%str.length()) == ' ') { // notice, use %str.length()
                pos++;
            } else {
                while (pos>0 && str.charAt(pos%str.length())!=' ') pos--;
                pos++;
            }
        }

        return pos/str.length();
    }

    public static void main(String[] args) {
        String[] ss = new String[]{"sopqqjh", "cwkguzri", "wvlivwhcsb", "kbn", "mvzzw", "vntzxee", "nnmao", "wbhrz", "hxkhpbwzj", "hlzuz","vwlhrxm", "cvlmlbws", "eeholhrcm", "pemaukjus", "bmzugjefuv", "lrhraibype", "pheybigs", "myh", "ncegczhtql", "mrbtwdnxnw", "axsbcjo", "rf"};
        int rows=966,cols=915;
//        String[] ss = new String[]{"I", "a", "b"};
//        int rows=4, cols=1;
        System.out.println(new SentenceScreenFitting().wordsTyping_bruteforce(ss, rows, cols));
        System.out.println(new SentenceScreenFitting().wordsTyping(ss, rows, cols));
        System.out.println(new SentenceScreenFitting().wordsTyping_x2_best(ss, rows, cols));
    }

}
/*
Given a rows x cols screen and a sentence represented by a list of non-empty words,
find how many times the given sentence can be fitted on the screen.

A word cannot be split into two lines.
The order of words in the sentence must remain unchanged.
Two consecutive words in a line must be separated by a single space.
Total words in the sentence won't exceed 100.
Length of each word is greater than 0 and won't exceed 10.
1 ≤ rows, cols ≤ 20,000.
Example
Given rows = 2, cols = 8, sentence = ["hello", "world"], retrun 1.

Explanation:
hello---
world---

The character '-' signifies an empty space on the screen.
Given rows = 3, cols = 6, sentence = ["a", "bcd", "e"], return 2.

Explanation:
a-bcd-
e-a---
bcd-e-

The character '-' signifies an empty space on the screen.
Given rows = 4, cols = 5, sentence = ["I", "had", "apple", "pie"], return 1.

Explanation:
I-had
apple
pie-I
had--

The character '-' signifies an empty space on the screen.
*/