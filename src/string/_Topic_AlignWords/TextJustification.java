package string._Topic_AlignWords;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by thj on 07/06/2018.
 *
 *    the solution is straight forward, just notice the edge case...
 *
 */
public class TextJustification {


    public List<String> fullJustify_x2(String[] words, int maxWidth) {
        List<String> ans = new ArrayList<>();
        int wordIdx=0;
        int curLen=-1,curWords=0; // for each line
        StringBuffer sb = new StringBuffer();
        while(wordIdx<words.length) {
            if(curLen+1+words[wordIdx].length()<=maxWidth) { // word fit into line
                curLen+=1+words[wordIdx].length();
                curWords+=1;
                wordIdx++;
            } else { // word exceed line
                int spaces = maxWidth-curLen+curWords-1;
                int spaceEach = curWords>1?spaces/(curWords-1):spaces;
                int spaceRemain = curWords>1?(spaces - spaceEach*(curWords-1)):0;
                for(int i=wordIdx-curWords; i<wordIdx; i++) {
                    sb.append(words[i]);
                    if (i==wordIdx-1 && curWords>1) continue;
                    for(int times=0;times<spaceEach;times++) sb.append(" ");
                    if(spaceRemain-->0) sb.append(" ");
                }
                ans.add(sb.toString());
                curLen=-1;curWords=0;sb=new StringBuffer();
            }
        }

        // process last line
        int spaceRemain = maxWidth-curLen;
        for(int i=wordIdx-curWords;i<wordIdx;i++) {
            sb.append(words[i]);
            if(i!=words.length-1) sb.append(" ");
        }
        for (int len=0;len<spaceRemain;len++) sb.append(" ");
        ans.add(sb.toString());
        return ans;
    }

    public static void main(String[] args) {
//        String[] words = new String[]{"This", "is", "an", "example", "of", "text", "justification."};
//        String[] words = new String[]{"What","must","be","acknowledgment","shall","be"};
        String[] words = new String[]{"Science","is","what","we","understand","well","enough","to","explain",
                "to","a","computer.","Art","is","everything","else","we","do"};
        List<String> res = new TextJustification().fullJustify_x2(words, 20);
        for (String w: res) System.out.println(w);
    }

    public List<String> fullJustify(String[] words, int maxWidth) {
        List<String> ans = new ArrayList<>();

        int count=0;
        List<String> tempList = new ArrayList<>();
        StringBuffer tempStr = new StringBuffer();
        int i=0;
        while (i<words.length) {
            if (count + words[i].length() > maxWidth ) {// line end
                int spaceNeeded = maxWidth - (count-tempList.size());
                int spaceEach = tempList.size()>1?spaceNeeded/(tempList.size()-1):spaceNeeded;
                int spaceRemain = tempList.size()>1?spaceNeeded%(tempList.size()-1):0;
                int wordIdx=0;
                while(tempStr.length()<maxWidth) {
                    tempStr.append(tempList.get(wordIdx));
                    if (tempStr.length()>=maxWidth) break;
                    if (wordIdx < tempList.size()-1 || tempList.size()==1) {
                        for(int k=0;k<spaceEach;k++) {tempStr.append(" ");}
                        if (spaceRemain>0) {tempStr.append(" ");spaceRemain--;}
                    }
                    wordIdx++;
                }
                ans.add(tempStr.toString());
                // clear temp info
                tempList.clear();
                tempStr = new StringBuffer();
                count=0;
            } else { // continue search
                tempList.add(words[i]);
                count+=words[i].length();
                count+=1; // at least 1 space
                i++;
            }
        }

        // add remains in templist
        int wordIdx=0;
        while (tempStr.length()<maxWidth) {
            if (wordIdx<tempList.size()) {tempStr.append(tempList.get(wordIdx++));}
            if (tempStr.length()>=maxWidth) break;
            tempStr.append(" ");
        }
        ans.add(tempStr.toString());
        return ans;
    }
}


/*

Given an array of words and a width maxWidth, format the text such that each line has exactly maxWidth characters and is fully (left and right) justified.

You should pack your words in a greedy approach; that is, pack as many words as you can in each line.
Pad extra spaces ' ' when necessary so that each line has exactly maxWidth characters.

Extra spaces between words should be distributed as evenly as possible.
If the number of spaces on a line do not divide evenly between words,the empty slots on the left will be assigned more spaces than the slots on the right.

For the last line of text, it should be left justified and no extra space is inserted between words.

Note:
A word is defined as a character sequence consisting of non-space characters only.
Each word's length is guaranteed to be greater than 0 and not exceed maxWidth.
The input array words contains at least one word.
Example 1:
Input:
words = ["This", "is", "an", "example", "of", "text", "justification."]
maxWidth = 16
Output:
[
   "This    is    an",
   "example  of text",
   "justification.  "
]
Example 2:
Input:
words = ["What","must","be","acknowledgment","shall","be"]
maxWidth = 16
Output:
[
  "What   must   be",
  "acknowledgment  ",
  "shall be        "
]
Explanation: Note that the last line is "shall be    " instead of "shall     be",
             because the last line must be left-justified instead of fully-justified.
             Note that the second line is also left-justified becase it contains only one word.
Example 3:
Input:
words = ["Science","is","what","we","understand","well","enough","to","explain",
         "to","a","computer.","Art","is","everything","else","we","do"]
maxWidth = 20
Output:
[
  "Science  is  what we",
  "understand      well",
  "enough to explain to",
  "a  computer.  Art is",
  "everything  else  we",
  "do                  "
]
*/

/*
other people's solution for ref...
public class Solution {
    public List<String> fullJustify(String[] words, int L) {
        List<String> lines = new ArrayList<String>();

        int index = 0;
        while (index < words.length) {
            int count = words[index].length();
            int last = index + 1;
            while (last < words.length) {
                if (words[last].length() + count + 1 > L) break;
                count += words[last].length() + 1;
                last++;
            }

            StringBuilder builder = new StringBuilder();
            int diff = last - index - 1;
            // if last line or number of words in the line is 1, left-justified
            if (last == words.length || diff == 0) {
                for (int i = index; i < last; i++) {
                    builder.append(words[i] + " ");
                }
                builder.deleteCharAt(builder.length() - 1);
                for (int i = builder.length(); i < L; i++) {
                    builder.append(" ");
                }
            } else {
                // middle justified
                int spaces = (L - count) / diff;
                int r = (L - count) % diff;
                for (int i = index; i < last; i++) {
                    builder.append(words[i]);
                    if (i < last - 1) {
                        for (int j = 0; j <= (spaces + ((i - index) < r ? 1 : 0)); j++) {
                            builder.append(" ");
                        }
                    }
                }
            }
            lines.add(builder.toString());
            index = last;
        }


        return lines;
    }
}
*/