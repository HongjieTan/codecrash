package string;

public class ValidPalindrome2 {
    public boolean isPalindrome(String s) {
        return isPalindrome(s,true);
    }

    private boolean isPalindrome(String s, boolean canRemove) {
        if (s.length()==0 || s.length()==1) return true;
        if (s.charAt(0) == s.charAt(s.length()-1)) return isPalindrome(s.substring(1, s.length()-1), canRemove);
        if (!canRemove) return false;
        return isPalindrome(s.substring(1, s.length()), false) || isPalindrome(s.substring(0, s.length()-1), false);
    }


//    public boolean isPalindrome(String s) {
//        boolean removed = false;
//
//        int l=0,r=s.length()-1;
//        while (l<=r) {
//            if (s.charAt(l) != s.charAt(r)) {
//                if (!removed){
////                    r--;
//
//                    if (l+1<=r && s.charAt(l+1)==s.charAt(r)) l++;
//                    else r--;
//
//                    removed = true;
//                } else {
//                    return false;
//                }
//            } else {
//                l++;r--;
//            }
//        }
//        return true;
//    }

    public static void main(String[] args) {
        System.out.println(new ValidPalindrome2()
                .isPalindrome("abc"));
    }

}
