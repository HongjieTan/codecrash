//Given a list of folders, remove all sub-folders in those folders and return in any order the folders after removing.
//
// If a folder[i] is located within another folder[j], it is called a sub-folder of it.
//
// The format of a path is one or more concatenated strings of the form: / followed by one or more lowercase English letters. For example, /leetcode and /leetcode/problems are valid paths while an empty string and / are not.
//
//
// Example 1:
//
//
//Input: folder = ["/a","/a/b","/c/d","/c/d/e","/c/f"]
//Output: ["/a","/c/d","/c/f"]
//Explanation: Folders "/a/b/" is a subfolder of "/a" and "/c/d/e" is inside of folder "/c/d" in our filesystem.
//
//
// Example 2:
//
//
//Input: folder = ["/a","/a/b/c","/a/b/d"]
//Output: ["/a"]
//Explanation: Folders "/a/b/c" and "/a/b/d/" will be removed because they are subfolders of "/a".
//
//
// Example 3:
//
//
//Input: folder = ["/a/b/c","/a/b/ca","/a/b/d"]
//Output: ["/a/b/c","/a/b/ca","/a/b/d"]
//
//
//
// Constraints:
//
//
// 1 <= folder.length <= 4 * 10^4
// 2 <= folder[i].length <= 100
// folder[i] contains only lowercase letters and '/'
// folder[i] always starts with character '/'
// Each folder name is unique.
//
//

package string._Topic_FIleSystem;


import java.util.*;

/*
  - sort: O(nlogn)
  - string._DataStructure_Trie_PrefixTree: O(n)

 */
public class RemoveSubFolders {

    // sort, O(nlogn)
    public List<String> removeSubfolders(String[] folder) {
        Arrays.sort(folder);
        List<String> ret = new ArrayList<>();
        String parent= "#"; // dummy head
        for(String path: folder) {
            if(!path.startsWith(parent)) {
                parent = path+"/";
                ret.add(path);
            }
        }
        return ret;
    }


    // string._DataStructure_Trie_PrefixTree, O(n)
    class Node {
        String word = null;
        Map<String, Node> children = new HashMap<>();
    }

    public List<String> removeSubfolders_trie(String[] folder) {
        Node root = new Node();

        for(String path: folder) {
            Node cur = root;
            String[] parts = path.substring(1).split("/");
            for(String part: parts) {
                if(!cur.children.containsKey(part))cur.children.put(part, new Node());
                cur = cur.children.get(part);
            }
            cur.word = path;
        }

        Set<String> set = new HashSet<>();
        for(String path: folder) {
            Node cur = root;
            String[] parts = path.substring(1).split("/");
            for(String part: parts) {
                cur = cur.children.get(part);
                if(cur.word!=null) {
                    set.add(cur.word);
                    break;
                }
            }
        }

        return new ArrayList<>(set);
    }

    public static void main(String[] as) {
        for (String s: new RemoveSubFolders().removeSubfolders(new String[]{"/a","/a/b","/c/d","/c/d/e","/c/f"})) System.out.println(s);
        System.out.println();
        for (String s: new RemoveSubFolders().removeSubfolders(new String[]{"/a","/a/b/c","/a/b/d"})) System.out.println(s);
        System.out.println();
        for (String s: new RemoveSubFolders().removeSubfolders(new String[]{"/a/b/c","/a/b/ca","/a/b/d"})) System.out.println(s);
    }}
