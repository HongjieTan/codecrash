//
// Solve a given equation and return the value of x in the form of string "x=#value".
// The equation contains only '+', '-' operation, the variable x and its coefficient.
//
//
//
//If there is no solution for the equation, return "No solution".
//
//
//If there are infinite solutions for the equation, return "Infinite solutions".
//
//
//If there is exactly one solution for the equation, we ensure that the value of x is an integer.
//
//
// Example 1:
//
//Input: "x+5-3+x=6+x-2"
//Output: "x=2"
//
//
//
// Example 2:
//
//Input: "x=x"
//Output: "Infinite solutions"
//
//
//
// Example 3:
//
//Input: "2x=x"
//Output: "x=0"
//
//
//
// Example 4:
//
//Input: "2x+3x-6x=x+2"
//Output: "x=-1"
//
//
//
// Example 5:
//
//Input: "x=x+2"
//Output: "No solution"
//
//

package string;


import java.util.ArrayList;
import java.util.List;

// straight forward...(maybe we'd better create functions to make code concise...)
public class SolveTheEquation {
    // concise version
    public String solveEquation(String equation) {
        String[] exps = equation.split("=");
        int lv=0, rv=0;
        for(String part: parse(exps[0])) {
            if(part.charAt(part.length()-1) == 'x')
                lv += Integer.valueOf(part.substring(0, part.length()-1));
            else
                rv -= Integer.valueOf(part);
        }

        for(String part: parse(exps[1])) {
            if(part.charAt(part.length()-1) == 'x')
                lv -= Integer.valueOf(part.substring(0, part.length()-1));
            else
                rv += Integer.valueOf(part);
        }

        if(lv==0)
            return rv==0?"Infinite solutions":"No solution";
        else
            return "x="+rv/lv;
    }

    List<String> parse(String exp) {
        List<String> res = new ArrayList<>();
        char[] s = exp.toCharArray();
        StringBuilder sb = new StringBuilder();
        for(char ch: s) {
            if(sb.length()!=0 && (ch=='+' || ch=='-')) {
                String str = sb.toString();
                if(str.equals("x")) str="+1x";
                if(str.equals("+x")) str="+1x";
                if(str.equals("-x")) str="-1x";
                res.add(str);
                sb = new StringBuilder();
            }
            sb.append(ch);
        }
        String str = sb.toString();
        if(str.equals("x")) str="+1x";
        if(str.equals("+x")) str="+1x";
        if(str.equals("-x")) str="-1x";
        res.add(str);
        return res;
    }



    public String solveEquation_v0(String equation) {
        String[] exps = equation.split("=");
        char[] a = exps[0].toCharArray();
        char[] b = exps[1].toCharArray();
        int lv=0, rv=0, flag = 1;

        StringBuilder sb = new StringBuilder();
        for(int i=0;i<a.length;i++) {
            char ch = a[i];
            if(ch=='+' || ch=='-' || i==a.length-1) {
                if (i==a.length-1) sb.append(ch);

                String s = sb.toString();
                if (!s.isEmpty()) {
                    if(s.charAt(s.length()-1)=='x') {
                        if (s.length()==1)
                            lv += flag;
                        else
                            lv += flag*Integer.valueOf(s.substring(0, s.length()-1));
                    }
                    else {
                        rv -= flag*Integer.valueOf(s);
                    }
                }

                sb = new StringBuilder();
                flag = ch=='+'?1:-1;
            }
            else {
                sb.append(ch);
            }
        }

        flag=1;
        for(int i=0;i<b.length;i++) {
            char ch = b[i];
            if(ch=='+' || ch=='-' || i ==b.length-1) {
                if (i==b.length-1) sb.append(ch);

                String s = sb.toString();
                if (!s.isEmpty()) {
                    if(s.charAt(s.length()-1)=='x') {
                        if (s.length()==1)
                            lv -= flag;
                        else
                            lv -= flag*Integer.valueOf(s.substring(0, s.length()-1));
                    }
                    else {
                        rv += flag*Integer.valueOf(s);
                    }
                }

                sb = new StringBuilder();
                flag = ch=='+'?1:-1;
            }
            else {
                sb.append(ch);
            }
        }

        if(lv==0)
            return rv==0?"Infinite solutions":"No solution";
        else
            return "x="+rv/lv;
    }

    public static void main(String[] as) {
//        System.out.println(Integer.valueOf("+12"));
//        System.out.println(new SolveTheEquation().solveEquation("-x=-1"));
        System.out.println(new SolveTheEquation().solveEquation("-x=1"));
        System.out.println(new SolveTheEquation().solveEquation("x+5-3+x=6+x-2"));
        System.out.println(new SolveTheEquation().solveEquation("x=x"));
        System.out.println(new SolveTheEquation().solveEquation("2x=x"));
        System.out.println(new SolveTheEquation().solveEquation("2x+3x-6x=x+2"));
        System.out.println(new SolveTheEquation().solveEquation("x=x+2"));
    }
}

/*
solution1:

public class Solution {
    public String coeff(String x) {
        if (x.length() > 1 && x.charAt(x.length() - 2) >= '0' && x.charAt(x.length() - 2) <= '9')
            return x.replace("x", "");
        return x.replace("x", "1");
    }
    public String solveEquation(String equation) {
        String[] lr = equation.split("=");
        int lhs = 0, rhs = 0;
        for (String x: breakIt(lr[0])) {
            if (x.indexOf("x") >= 0) {
                lhs += Integer.parseInt(coeff(x));
            } else
                rhs -= Integer.parseInt(x);
        }
        for (String x: breakIt(lr[1])) {
            if (x.indexOf("x") >= 0)
                lhs -= Integer.parseInt(coeff(x));
            else
                rhs += Integer.parseInt(x);
        }
        if (lhs == 0) {
            if (rhs == 0)
                return "Infinite solutions";
            else
                return "No solution";
        }
        return "x=" + rhs / lhs;
    }
    public List < String > breakIt(String s) {
        List < String > res = new ArrayList < > ();
        String r = "";
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '+' || s.charAt(i) == '-') {
                if (r.length() > 0)
                    res.add(r);
                r = "" + s.charAt(i);
            } else
                r += s.charAt(i);
        }
        res.add(r);
        return res;
    }
}


solution2:

public class Solution {
    public String coeff(String x) {
        if (x.length() > 1 && x.charAt(x.length() - 2) >= '0' && x.charAt(x.length() - 2) <= '9')
            return x.replace("x", "");
        return x.replace("x", "1");
    }
    public String solveEquation(String equation) {
        String[] lr = equation.split("=");
        int lhs = 0, rhs = 0;
        for (String x: lr[0].split("(?=\\+)|(?=-)")) {
            if (x.indexOf("x") >= 0) {

                lhs += Integer.parseInt(coeff(x));
            } else
                rhs -= Integer.parseInt(x);
        }
        for (String x: lr[1].split("(?=\\+)|(?=-)")) {
            if (x.indexOf("x") >= 0)
                lhs -= Integer.parseInt(coeff(x));
            else
                rhs += Integer.parseInt(x);
        }
        if (lhs == 0) {
            if (rhs == 0)
                return "Infinite solutions";
            else
                return "No solution";
        } else
            return "x=" + rhs / lhs;
    }
}


*/
