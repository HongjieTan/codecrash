package string._DataStructure_Trie_PrefixTree;
import java.util.ArrayList;
import java.util.List;


class Trie {

    class TrieNode {
        String word = null;
        TrieNode[] children = new TrieNode[26];
    }

    TrieNode root;

    public Trie() {root = new TrieNode();}

    public void insert(String word) {
        TrieNode cur = root;
        for(char c: word.toCharArray()) {
            if(cur.children[c-'a'] == null)
                cur.children[c-'a'] = new TrieNode();
            cur = cur.children[c-'a'];
        }
        cur.word = word;
    }

    public List<String> searchByPrefix(String prefix) {
        TrieNode cur = root;
        for(char c: prefix.toCharArray()) cur = cur.children[c-'a'];

        List<String> ret = new ArrayList<>();
        dfs(ret, cur);
        return ret;
    }

    private void dfs(List<String> ret, TrieNode cur) {
        if(cur.word!=null) ret.add(cur.word);
        for(TrieNode child: cur.children) {
            if(child!=null) dfs(ret, child);
        }
    }

//    public boolean find(String word) {} // todo
}