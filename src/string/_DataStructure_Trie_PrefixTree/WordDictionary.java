package string._DataStructure_Trie_PrefixTree;

/**
 * Created by thj on 30/05/2018.
 */
public class WordDictionary {

    private class Node {
        Node[] children = new Node[26];
        String val;
    }

    private Node root;

    /** Initialize your data structure here. */
    public WordDictionary() {
        root = new Node();
    }

    /** Adds a word into the data structure. */
    public void addWord(String word) {
        Node node = this.root;
        for (char c: word.toCharArray()) {
            if (node.children[c-'a'] == null) {
                node.children[c-'a'] = new Node();
            }
            node = node.children[c-'a'];
        }
        node.val = word;
    }

    /** Returns if the word is in the data structure. A word could contain the dot character '.' to represent any one letter. */
    public boolean search(String word) {
        return subSearch(word.toCharArray(), 0, this.root);
    }

    private boolean subSearch(char[] chars, int start, Node root) {
        if (root == null) return false;
        if (start == chars.length) return root.val!=null; // reach path end
        if (chars[start] != '.' ) {
            return subSearch(chars, start+1, root.children[chars[start]-'a']);
        } else { // if '.'
            for (Node child: root.children) {
                if (subSearch(chars, start+1, child)) {return true;}
            }
        }
        return false;
    }
}


