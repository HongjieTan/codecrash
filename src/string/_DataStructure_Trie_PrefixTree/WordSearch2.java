package string._DataStructure_Trie_PrefixTree;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by thj on 16/06/2018.
 *
 * dfs+backtrack+string._DataStructure_Trie_PrefixTree !!!!
 *
 *
 */
public class WordSearch2 {

    public List<String> findWords_x2(char[][] board, String[] words) {
        Set<String> res = new HashSet<>();

        TrieNode trie = createTrieTree(words);
        boolean[][] visited = new boolean[board.length][board[0].length];
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[0].length; j++) {
                dfs_x2(res, board, i, j, trie, visited);
            }
        }
        return res.stream().collect(Collectors.toList());
    }

    private void dfs_x2(Set<String> res, char[][] board, int row, int col, TrieNode node, boolean[][] visited) {
        if (visited[row][col]) return;
        TrieNode child = node.children[board[row][col]-'a'];
        if (child==null) return; // path not match...

        if (child.word!=null) res.add(child.word);
        visited[row][col] = true;
        int[][] dirs = new int[][]{{-1,0},{1,0},{0,-1},{0,1}};
        for (int[] dir: dirs) {
            int nextrow = row+dir[0];
            int nextcol = col+dir[1];
            if (nextrow>=0 && nextrow<board.length
                && nextcol>=0 && nextcol<board[0].length) {
                dfs_x2(res, board, nextrow, nextcol, child, visited);
            }
        }
        visited[row][col] = false; // backtrack!!!
    }

    private TrieNode createTrieTree(String[] words) {
        TrieNode root = new TrieNode();
        for (String word : words) {
            insertWord(root, word);
        }
        return root;
    }

    private void insertWord(TrieNode node, String word) {
        for(char c: word.toCharArray()) {
            if (node.children[c-'a'] == null) {
                node.children[c-'a'] = new TrieNode();
            }
            node = node.children[c-'a'];
        }
        node.word = word;
    }

























    public List<String> findWords(char[][] board, String[] words) {
        // write your code here
        TrieNode trie = buildTrie(words);
        List<String> ans = new ArrayList<>();
        boolean[][] visited = new boolean[board.length][board[0].length];

        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[0].length; j++) {
                dfs(trie, board, i, j, ans, visited);
            }
        }
        return ans;
    }


    private void dfs(TrieNode trie, char[][] board, int row, int col, List<String> ans, boolean[][] visited) {

        if (visited[row][col] || trie==null) return;

        char c = board[row][col];
        TrieNode child = trie.children[c-'a'];
        if (child!=null) {  // path match!
            if (child.word!=null) { // hit a word
                ans.add(child.word);
                child.word = null; // remove duplicate instead of using set...
            }

            // continue dfs
            visited[row][col] = true;
            if (isValid(row-1, col, board)) {dfs(child, board, row-1, col, ans, visited);}
            if (isValid(row+1, col, board)) {dfs(child, board, row+1, col, ans, visited);}
            if (isValid(row, col-1, board)) {dfs(child, board, row, col-1, ans, visited);}
            if (isValid(row, col+1, board)) {dfs(child, board, row, col+1, ans, visited);}
            visited[row][col] = false; // backtrack!! revert status!!...
        }

    }

    private boolean isValid(int row, int col, char[][] board) {
        int rows = board.length, cols = board[0].length;
        return row>=0&&row<rows&&col>=0&&col<cols;
    }


    // need to keep in mind!!!
    private TrieNode buildTrie(String[] words) {
        TrieNode root = new TrieNode();
        for (String word: words) {
            TrieNode node = root;
            for (char c: word.toCharArray()) {
                int i = c-'a';
                if (node.children[i] == null) {node.children[i] = new TrieNode();}
                node = node.children[i];
            }
            node.word = word;
        }
        return root;
    }

    class TrieNode {
        String word;
        TrieNode[] children = new TrieNode[26];
    }


    public static void main(String[] args) {
        String[] words = {"aaab", "aaa"};
        char[][] board = new char[][]{
                {'a', 'b'},
                {'a', 'a'}};
        List<String> ans = new WordSearch2().findWords_x2(board, words);
    }

}
