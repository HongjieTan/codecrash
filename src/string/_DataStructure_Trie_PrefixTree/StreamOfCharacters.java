package string._DataStructure_Trie_PrefixTree;

public class StreamOfCharacters {
}

/*
  - string._DataStructure_Trie_PrefixTree: init-O(n), query-O(W), better...as it save time checking all suffixes
  - set:  init-O(n), query-O(W)
 */
class StreamChecker {
    class TrieNode {
        boolean end;
        TrieNode[] children = new TrieNode[26];
    }

    TrieNode root;
    StringBuilder sb;
    int maxLen;

    public StreamChecker(String[] words) {
        root = new TrieNode();
        for(String w: words) {
            insert(w);
            maxLen = Math.max(maxLen, w.length());
        }
        sb = new StringBuilder();
    }

    void insert(String word) {
        TrieNode node = root;
        for(int i=word.length()-1;i>=0;i--) {
            char c = word.charAt(i);
            if(node.children[c-'a']==null) node.children[c-'a']=new TrieNode();
            node = node.children[c-'a'];
        }
        node.end = true;
    }

    public boolean query(char letter) {
        sb.append(letter);
        TrieNode node = root;
        for (int i = sb.length()-1; i >=0 && sb.length()-i<=maxLen ; i--) {
            char c = sb.charAt(i);
            node = node.children[c-'a'];
            if(node==null) break;
            if(node.end) return true;
        }
        return false;
    }

}

/**
 * Your StreamChecker object will be instantiated and called as such:
 * StreamChecker obj = new StreamChecker(words);
 * boolean param_1 = obj.query(letter);
 */

/*
Implement the StreamChecker class as follows:

StreamChecker(words): Constructor, init the data structure with the given words.
query(letter): returns true if and only if for some k >= 1, the last k characters queried (in order from oldest to newest, including this letter just queried) spell one of the words in the given list.


Example:

StreamChecker streamChecker = new StreamChecker(["cd","f","kl"]); // init the dictionary.
streamChecker.query('a');          // return false
streamChecker.query('b');          // return false
streamChecker.query('c');          // return false
streamChecker.query('d');          // return true, because 'cd' is in the wordlist
streamChecker.query('e');          // return false
streamChecker.query('f');          // return true, because 'f' is in the wordlist
streamChecker.query('g');          // return false
streamChecker.query('h');          // return false
streamChecker.query('i');          // return false
streamChecker.query('j');          // return false
streamChecker.query('k');          // return false
streamChecker.query('l');          // return true, because 'kl' is in the wordlist


Note:

1 <= words.length <= 2000
1 <= words[i].length <= 2000
Words will only consist of lowercase English letters.
Queries will only consist of lowercase English letters.
The number of queries is at most 40000.
*/

/*
We say
W = max(words.length),the maximum length of all words.
N = words.size, the number of words
Q, the number of calls of function query


Solution 1: Check all words (TLE)
If we save the whole input character stream and compare with words one by one,
The time complexity for each query will be O(NW),
depending on the size of words.


Solution 2: Check Query Suffixes (Maybe AC, Maybe TLE)
While the words.size can be really big,
the number of the suffixes of query stream is bounded.

For example, if the query stream is "abcd",
the suffix can be "abcd", "bcd", "cd", "d".

We can save all hashed words to a set.
For each query, we check query stream's all suffixes.
The maximum length of words is W, we need to check W suffixes.

The time complexity for each query will be O(W) if we take the set search as O(1).
The overall time is O(WQ).


Solution 3: Trie (Accepted)
Only a part of suffixes can be the prefix of a word,
waiting for characters coming to form a complete word.
Instead of checking all W suffixes in each query,
we can just save those possible waiting prefixes in a waiting list.

Explanation:
Initialization:

Construct a string._DataStructure_Trie_PrefixTree
declare a global waiting list.
Query:

for each node in the waiting list,
check if there is child node for the new character.
If so, add it to the new waiting list.
return true if any node in the waitinglist is the end of a word.
Time Complexity:
waiting.size <= W, where W is the maximum length of words.
So that O(query) = O(waiting.size) = O(W)
We will make Q queries, the overall time complexity is O(QW)

Note that it has same complexity in the worst case as solution 2 (like "aaaaaaaa" for words and query),
In general cases, it saves time checking all suffixes, and also the set search in a big set.


Space Complexity:

waiting.size <= W, where W is the maximum length of words.
waiting list will take O(W)

Assume we have initially N words, at most N leaves in the string._DataStructure_Trie_PrefixTree.
The size of string._DataStructure_Trie_PrefixTree is O(NW).


Python:

class StreamChecker(object):

    def __init__(self, words):
        T = lambda: collections.defaultdict(T)
        self.string._DataStructure_Trie_PrefixTree = T()
        for w in words: reduce(dict.__getitem__, w, self.string._DataStructure_Trie_PrefixTree)['#'] = True
        self.waiting = []

    def query(self, letter):
        self.waiting = [node[letter] for node in self.waiting + [self.string._DataStructure_Trie_PrefixTree] if letter in node]
        return any("#" in node for node in self.waiting)
Solution 4: Construct Trie with Reversed Words
Time: 600 ~ 700ms
Time complexity: O(WQ)

    def __init__(self, words):
        T = lambda: collections.defaultdict(T)
        self.string._DataStructure_Trie_PrefixTree = T()
        for w in words: reduce(dict.__getitem__, w[::-1], self.string._DataStructure_Trie_PrefixTree)['#'] = True
        self.S = ""
        self.W = max(map(len, words))

    def query(self, letter):
        self.S = (letter + self.S)[:self.W]
        cur = self.string._DataStructure_Trie_PrefixTree
        for c in self.S:
            if c in cur:
                cur = cur[c]
                if cur['#'] == True:
                    return True
            else:
                break
        return False
*/

/*****

******/