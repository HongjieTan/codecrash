package string._Topic_StringMatch;

public class RobinKarp {
    int find(String txt, String pat) {
        int n = txt.length(), m = pat.length();
        int d = 256;          // d is the number of characters in the input alphabet
        int MOD = 1000000000; // use modular arithmetic to make sure that the hash values can be stored in an integer variable

        int h=1;  // h = pow(d, m-1) % MOD
        for(int i=0;i<m-1;i++) h = (h*d)%MOD;

        int hp=0, hs=0;
        for(int i=0;i<m;i++) {
            hp = (hp*d + pat.charAt(i))%MOD;
            hs = (hs*d + txt.charAt(i))%MOD;
        }

        for(int i=0;i+m-1<n;i++) {
            if(hs==hp) {
                int j=0;
                for(;j<m;j++) {
                    if(txt.charAt(i+j) != pat.charAt(j)) break;
                }
                if(j==m) return i;
            } else {
                if(i+m<n) hs = ((hs - txt.charAt(i)*h%MOD )*d + txt.charAt(i+m))%MOD; // O(1)， txt.charAt(i)*h%MOD 这一步要MOD？yes
                if(hs<0) hs = hs + MOD; // We might get negative value of hs, converting it to positive
            }
        }
        return -1;
    }

    public static void main(String[] as) {
        RobinKarp su = new RobinKarp();
        String txt = "cbbabba", pat = "abba";
        System.out.println(su.find(txt, pat));
        System.out.println(txt.indexOf(pat));
    }
}