package string._Topic_StringMatch;
/*
今天下午面的Google 全职电面 面试官感觉像个国人大哥 挺nice的 题也不难 但是一开始想复杂了

给两个长度一样的string A，B  把他们对齐切一刀 这样就得到了A1 A2 B1 B2 四个substring
写一个func check 能不能找到这一刀  A1和B2 拼起来 或者A2和B1拼起来能组成parlindrome
e.g.  A = "abcbbbb" B = "xxxbcba"  那么在第三个char之后切一刀 A1 = "abc" B2 = "bcba" 拼起来 "abcbcba" 那就返回true
如果找不到这一刀返回false

followup 是如果两个string不对齐 切的话 比如 A可以在第一个char之后切 B可以在最后一个char之前切 这样他们能组成 "aa" 也是parlindrome
问题是怎么切这俩string 能让组成的parlindrome最长-baidu 1point3acres

感觉用正常的two pointers就能做了 一开始没申清题花了点时间 然后有个case没考虑 面试官也提醒了下 之后打code的时候把速度放慢了点
顺带着解释 虽然也没啥好解释的 就尽量interact 所以后面没啥时间 followup时候就讲了idea怎么实现 没具体写code了 希望能给个旅游的机会吧



- Manacher's Algorithm 马拉车算法 - 可以O(n）时间求最长回文子串

- by 网友：
    follow up是有O(n)的解的，关键点是不一样了选A还是B切，然后切在什么地方。. check 1point3acres for more.
    切肯定是以不一样了的那个char开始的在长的回文除，而找longest prefix parlindrome可以先flip string，
    然后用KMP找longest identical suffix，因为回文flip以后和原本的回文还是一样的。参考里扣214

    good!!!
*/

public class StringCutToBuildPalindrome {
//    string shortestPalindrome(string s)
//    {
//        int n = s.size();
//        string rev(s);
//        reverse(rev.begin(), rev.end());
//        string s_new = s + "#" + rev;
//        int n_new = s_new.size();
//        vector<int> f(n_new, 0);
//        for (int i = 1; i < n_new; i++) {
//            int t = f[i - 1];
//            while (t > 0 && s_new[i] != s_new[t])
//                t = f[t - 1];
//            if (s_new[i] == s_new[t])
//                ++t;
//            f[i] = t;
//        }
//        return rev.substr(0, n - f[n_new - 1]) + s;
//    }
    int[] build_KMP_table(String pattern) {
        int[] f = new int[pattern.length()];
        f[0] = 0;  // Base case
        for (int i = 1; i < pattern.length(); i++) {
            // Start by assuming we're extending the previous LSP
            int j = f[i - 1];
            while (j > 0 && pattern.charAt(i) != pattern.charAt(j))
                j = f[j - 1]; // why??
            if (pattern.charAt(i) == pattern.charAt(j))
                j++;
            f[i] = j;
        }
        return f;
    }


    int[] build_table(String str) {
        int[] T = new int[str.length()];
        T[0]=0;
        for(int i=1;i<T.length;i++) {
            int j=T[i-1];
            while (j>0 && str.charAt(i)!=str.charAt(j)) j=T[j-1];
            if (str.charAt(i)==str.charAt(j)) j++;
            T[i]=j;
        }
        return T;
    }

    public static void main(String[] args) {
//        int[] T = new StringCutToBuildPalindrome().build_KMP_table("ABACABABC");
        int[] T = new StringCutToBuildPalindrome().build_table("ABACABABC");
        for (int x: T) System.out.printf(x+" ");
    }

//    i	    0	1	2	3	4	5	6	7	8
//    W[i]	A	B	A	C	A	B	A	B	C
//    T[i]	0	0	1	0	1	2	3	2	0

}
