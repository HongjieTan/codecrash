package string;

public class ValidPalindrome {
    public boolean isPalindrome(String s) {
        int l=0, r=s.length()-1;
        while (l<=r) {
            if(!isAlphaOrNum(s.charAt(l))) l++;
            else if(!isAlphaOrNum(s.charAt(r))) r--;
            else if (Character.toLowerCase(s.charAt(l)) != Character.toLowerCase(s.charAt(r))) return false;
            else {
                l++;r--;
            }
        }
        return true;
    }

    private boolean isAlphaOrNum(char c) {
        return Character.isAlphabetic(c) || Character.isDigit(c);
    }
}
