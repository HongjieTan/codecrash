package string._Topic_SubseqMatch;

import java.util.*;

public class RepeatedSubSequenceMatch {

    // greedy - hashing query,  matching: O(A), preprocess O(26*B)
    public int minRepeats_hashquery_v3(String A, String B) {
        int[][] right = new int[26][B.length()]; // build this using DP idea !!
        for(int i=0;i<26;i++) {
            for(int j=B.length()-1;j>=0;j--) {
                char c = B.charAt(j);
                if (c=='a'+i) right[i][j]=j;
                else if (j==B.length()-1) right[i][j]=-1;
                else right[i][j]=right[i][j+1];
            }
        }

        int idx=0;
        int repeat=1;
        for (char c: A.toCharArray()) {
            int pos= idx<B.length()? right[c-'a'][idx]:-1;
            if (pos==-1) {
                idx=0;
                repeat++;
                pos = right[c-'a'][idx];
            }
            if (pos==-1) return -1; // 找一圈后没找到，则为无效
            idx=pos+1;
        }
        return repeat;
    }


    // greedy - binarysearch,   matching: O(A * logB)), preprocess O(B),
    public int minRepeats_binarysearch_v2(String A, String B) {
        Map<Character, List<Integer>> map = new HashMap<>(); // or use treeset
        for(int i=0;i<B.length();i++) {
            char c = B.charAt(i);
            if(!map.containsKey(c)) map.put(c, new ArrayList<>());
            map.get(c).add(i);
        }

        int idx=0;
        int repeats=1;
        for(char c: A.toCharArray()) {
            if(!map.containsKey(c)) return -1;
            List<Integer> poses = map.get(c);
            int pos = Collections.binarySearch(poses, idx);
            if (pos<0) pos=-(pos+1);
            if (pos==poses.size()) {
                repeats++;
                idx=0;
                pos = Collections.binarySearch(poses, idx);
            }

            idx=pos+1;
        }
        return repeats;
    }


    // greedy -- treeset,  matching: O(A * logB) preprocess: O(B*logB)
    public int minRepeats_binarysearch_v1(String A, String B) {
        Map<Character, TreeSet<Integer>> map = new HashMap<>();
        for(int i=0;i<B.length();i++) {
            char c = B.charAt(i);
            if(!map.containsKey(c)) map.put(c, new TreeSet<>());
            map.get(c).add(i);
        }

        int idx=0;
        int repeats=1;
        for(char c: A.toCharArray()) {
            if(!map.containsKey(c)) return -1;
            TreeSet<Integer> poses = map.get(c);
            Integer pos = poses.ceiling(idx);
            if (pos == null) {
                repeats++;
                idx=0;
                pos = poses.ceiling(idx);
            }

            idx=pos+1;
        }
        return repeats;
    }

    // greedy - brute force  O(m*n)
    public int minRepeats_naive(String A, String B) {
        int idx=0;
        for(char c: A.toCharArray()) {
            int steps=0;
            while (c!=B.charAt(idx%B.length())) {
                if (++steps>B.length()) return -1; // 走B.length()步都找不到匹配，说明不存在合法匹配...
                idx++;
            }
            idx++;
        }
        return idx/B.length() + (idx%B.length()>0?1:0);
    }


    public static void main(String[] agrs) {
        System.out.println(new RepeatedSubSequenceMatch().minRepeats_naive("adabac", "abppplee"));
        System.out.println(new RepeatedSubSequenceMatch().minRepeats_binarysearch_v1("adabac", "abppplee"));
        System.out.println(new RepeatedSubSequenceMatch().minRepeats_binarysearch_v2("adabac", "abppplee"));
        System.out.println(new RepeatedSubSequenceMatch().minRepeats_hashquery_v3("adabac", "abppplee"));

        System.out.println(new RepeatedSubSequenceMatch().minRepeats_naive("adabac", "abcd"));
        System.out.println(new RepeatedSubSequenceMatch().minRepeats_binarysearch_v1("adabac", "abcd"));
        System.out.println(new RepeatedSubSequenceMatch().minRepeats_binarysearch_v2("adabac", "abcd"));
        System.out.println(new RepeatedSubSequenceMatch().minRepeats_hashquery_v3("adabac", "abcd"));
    }
}
/*

Desc v1:
第三轮给了两个string A和B，求B重复的最少次数，使得A能在B中获得匹配，
其中B重复后得到的字符串可以进行删除操作，删去不需要的字符，
之后再进行匹配面试官直接给出要求用贪心解做完要求给出一些 test case

https://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=485447

- 第三轮给出的test case不是面试官想要的，我给出的都是能运行出不同结果的例子，面试官最后说希望给出包括特殊字符（外语字符）、超出内存一类的会抛出异常的情况。.

Desc v2:
source和target，求问最少需要repeat source几次才可以得到target，repeat完的string可以删除任意character。先是暴力解然后优化的。

原作者 sakura328 先用window做的，但是没有记忆。后面优化是先遍历记录source里每个字母的所有位置在treeset，遍历target的时候记录当前字母在source的位置优化的，
如果先用list记录用二分法，整体时间复杂度应该会更快一点，我主要是来不及就偷懒用treeset了。
https://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=463515


*/

/*
public class RepeatingWordToTarget {
    public int findRepeatingTimes(String target, String resource) {
        int indexInTarget = 0;
        int indexInResource = 0;
        int lent = target.length();
        int lenr = resource.length();

        while(indexInTarget < lent) {
            char c = target.charAt(indexInTarget);
            if(c == resource.charAt(indexInResource%lenr)) {
                indexInResource++;
                indexInTarget++;
            } else {
                indexInResource++;
            }
        }
        if(indexInResource%lenr == 0) return indexInResource/lenr;
        else return indexInResource/lenr + 1;
    }

    public static void main(String[] args) {
        RepeatingWordToTarget rw = new RepeatingWordToTarget();
        String target = "aabaaaaabaaabbbbaaaa";
        String resource = "bbaabbbb";
        System.out.println(rw.findRepeatingTimes(target, resource));
    }

}

indexInResource 也需要一个上界，不然会死循环

就是说如果扫了一遍，没有这个字符就直接break，不再重复扫下去
*/