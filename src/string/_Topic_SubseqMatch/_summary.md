refs: https://techdevguide.withgoogle.com/paths/foundational/find-longest-word-in-dictionary-that-subsequence-of-given-string/#code-challenge


Check whether string W is subsequence of string S:
    1. greedy:
       - O(S),
       - just scan S[j:] to match w[i], j is last matching index in S

    2. improved greedy: good!!
       - O（W*logS),   preprocess O(S)
       - maintain sorted list for each character in S:
            ex.S(abppplee)
                b -> [1]
                p -> [2, 3, 4]
                l -> [5]
                e -> [6, 7]

             Then to match w[i] in S[j:], we can use binary search by O(logS)

    3. improved greedy v2:
        - O(W),     preprocess O(S)
        - hashing:
            ex.S(abppplee)
               a -> [0,-1,-1,-1,-1,-1,-1,-1]
               p -> [2, 2, 3, 4,-1,-1,-1,-1]
               l -> [5, 5, 5, 5, 5, 5,-1,-1]
               e -> [6, 6, 6, 6, 6, 6, 6, 7]
            Then to match w[i] in S[j:], we can find directly using j and w[i] by O(1)