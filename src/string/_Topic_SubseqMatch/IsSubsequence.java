package string._Topic_SubseqMatch;

import java.util.*;

/**
 *
 *
 *  t is potentially a very long (length ~= 500,000) string, and s is a short string (<=100).
 *
 *  cool!!!!!
 *  Followup1:      lots of s coming from data stream    use hashmap and binary search:    preprocess:O(lenT)  query: O(lenS * log(lenT))
 *  Followup2(792): lots of s given at once              scan T once while using hashmap to track waiting words... : O(lenT * numS),
 *
 *
 *   1. greedy check! straight forward solution: O(len_s * len_t)
 *   2. dp: not need!! the brute force is faster than using dp stupidly here!
 *
 *
 */
public class IsSubsequence {

    // Followup2: Lots of s given at once.. check lt792

    // Followup1: lots of s coming from data stream
    // preprocess: O(lenT), query: O(lenS * log(lenT))
    public boolean isSubsequence(String s, String t) {
        if (s==null || s.length()==0) return true;
        Map<Character, List<Integer>> map = new HashMap<>();
        for (int i = 0; i < t.length(); i++) {
            char c = t.charAt(i);
            if (!map.containsKey(c)) map.put(c, new ArrayList<>());
            map.get(c).add(i);
        }

        int tIndex=0;
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (!map.containsKey(c)) return false;
            int index = binarySearch(map.get(c), tIndex);
            if (index==-1) return false;
            tIndex = index+1;
        }
        return true;
    }

    // find the first element who is >= target  -> use binary search! the arr is already sorted O(logk)
    private int binarySearch(List<Integer> arr, int target) {
        int l=0,r=arr.size()-1;
        while (l<=r) {
            int mid = l+(r-l)/2;
            if(mid==0 && arr.get(mid)>=target) return arr.get(mid);
            if(mid>0 && arr.get(mid-1)<target && arr.get(mid)>=target) return arr.get(mid);
            if(arr.get(mid)>=target) {
                r = mid-1;
            } else {
                l = mid+1;
            }
        }
        return -1;
    }


    // straight forward solution !  greedy check: query: O(lenS*lenT)
    public boolean isSubsequence_v0(String s, String t) {
//        if (s==null || s.length()==0) return true;
//        int tIndex=0;
//        for (int i = 0; i < s.length(); i++) {
//            int index = t.indexOf(s.charAt(i), tIndex);
//            if (index==-1) return false;
//            tIndex = index+1;
//        }
//        return true;

        int sIndex=0, tIndex=0;
        while (tIndex<t.length()) {
            if (s.charAt(sIndex) == t.charAt(tIndex)) {
                sIndex++;
                if (sIndex==s.length()) return true;
            }
            tIndex++;
        }
        return false;
    }

    /*
        not need!!! the brute force is faster than using dp stupidly here!!!

        t is potentially a very long (length ~= 500,000) string, and s is a short string (<=100).

        O(len(s)*len(t))
        dp[i,j]  s[0...i-1] is subseq t[0...j-1]

        i==0: true
        i>0&&j==0:  false
        i>0&&j>0:
            s[i-1]==t[j-1]:  dp[i-1,j-1] || dp[i][j-1]
            s[i-1]!=t[j-1]:  dp[i][j-1]
     */
//    public boolean isSubsequence(String s, String t) {
//        boolean[][] dp = new boolean[s.length()+1][t.length()+1];
//        for (int i = 0; i < s.length()+1; i++) {
//            for (int j = 0; j < t.length()+1; j++) {
//                if (i==0) dp[i][j]=true;
//                else if (j==0) dp[i][j]=false;
//                else {
//                    if (s.charAt(i-1)==t.charAt(j-1)) dp[i][j]=dp[i-1][j-1]||dp[i][j-1];
//                    else dp[i][j]=dp[i][j-1];
//                }
//            }
//        }
//        return dp[s.length()][t.length()];
//    }

    public static void main(String[] args) {
        System.out.println(new IsSubsequence().isSubsequence("acb", "ahbgdc"));
    }
}


/*
Given a string s and a string t, check if s is subsequence of t.

You may assume that there is only lower case English letters in both s and t.
t is potentially a very long (length ~= 500,000) string, and s is a short string (<=100).

A subsequence of a string is a new string which is formed from the original string by deleting some (can be none)
of the characters without disturbing the relative positions of the remaining characters.
(ie, "ace" is a subsequence of "abcde" while "aec" is not).

Example 1:
s = "abc", t = "ahbgdc"

Return true.

Example 2:
s = "axc", t = "ahbgdc"

Return false.

Follow up:
If there are lots of incoming S, say S1, S2, ... , Sk where k >= 1B, and you want to check one by one to see if T has its subsequence.
In this scenario, how would you change your code?

Credits:
Special thanks to @pbrother for adding this problem and creating all test cases.
*/