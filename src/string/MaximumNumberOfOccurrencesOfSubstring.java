/*
Given a string s, return the maximum number of ocurrences of any substring under the following rules:

The number of unique characters in the substring must be less than or equal to maxLetters.
The substring size must be between minSize and maxSize inclusive.


Example 1:

Input: s = "aababcaab", maxLetters = 2, minSize = 3, maxSize = 4
Output: 2
Explanation: Substring "aab" has 2 ocurrences in the original string.
It satisfies the conditions, 2 unique letters and size 3 (between minSize and maxSize).
Example 2:

Input: s = "aaaa", maxLetters = 1, minSize = 3, maxSize = 3
Output: 2
Explanation: Substring "aaa" occur 2 times in the string. It can overlap.
Example 3:

Input: s = "aabcabcab", maxLetters = 2, minSize = 2, maxSize = 3
Output: 3
Example 4:

Input: s = "abcde", maxLetters = 2, minSize = 3, maxSize = 3
Output: 0


Constraints:

1 <= s.length <= 10^5
1 <= maxLetters <= 26
1 <= minSize <= maxSize <= min(26, s.length)
s only contains lowercase English letters.
*/

package string;


import java.util.HashMap;
import java.util.Map;


// #String #SlidingWindow #BitManipulations
public class MaximumNumberOfOccurrencesOfSubstring {
    public int maxFreq(String s, int maxLetters, int minSize, int maxSize) {// seems maxSize is not needed...
        int ret = 0, n = s.length();
        char[] chs = s.toCharArray();
        int[] cfreq = new int[128];
        Map<String, Integer> sfreq = new HashMap<>();

        int l=0, r=0, distincts=0;
        while (r<n) {
            // move r
            if (cfreq[chs[r]]==0) distincts++;
            cfreq[chs[r]]++;
            r++;

            // move l
            if (r-l == minSize) {
                if (distincts<=maxLetters) {
                    String ss = s.substring(l,r);
                    sfreq.put(ss, sfreq.getOrDefault(ss,0)+1); // we can use rolling hash to speed up here
                    ret = Math.max(ret, sfreq.get(ss));
                }

                cfreq[chs[l]]--;
                if (cfreq[chs[l]]==0) distincts--;
                l++;
            }
        }
        return ret;
    }

//    public int maxFreq_v1(String s, int maxLetters, int minSize, int maxSize) {
//        int n = s.length();
//        int ret = 0;
//        char[] chs = s.toCharArray();
//        for(int w=minSize;w<=maxSize;w++) {
//            Map<String, Integer> freq = new HashMap<>();
//            Map<Character, Integer> cfreq = new HashMap<>();
//            int distincts = 0;
//
//            int l=0, r=0;
//            while(r<n) {
//                // move r
//                if(!cfreq.containsKey(chs[r])) {
//                    distincts++;
//                    cfreq.put(chs[r], 0);
//                }
//                cfreq.put(chs[r], cfreq.get(chs[r])+1);
//                r++;
//
//                if(r<w) continue;
//                if(distincts<=maxLetters) {
//                    String sub =s.substring(l,r);
//                    freq.put(sub, freq.getOrDefault(sub,0)+1);
//                    ret = Math.max(ret, freq.get(sub));
//                }
//
//                // move l
//                if(cfreq.get(chs[l])==1) {
//                    distincts--;
//                    cfreq.remove(chs[l]);
//                } else {
//                    cfreq.put(chs[l], cfreq.get(chs[l])-1);
//                }
//                l++;
//            }
//        }
//        return ret;
//    }
}

/*

by uwi:

public int maxFreq(String s, int maxLetters, int minSize, int maxSize) {
    int n = s.length();
    Map<String, Integer> map = new HashMap<>();
    for(int i = 0;i < n;i++){
        for(int j = minSize;j <= maxSize && i+j <= n;j++){
            String ss = s.substring(i, i+j);
            int f = 0;
            for(char c : ss.toCharArray()){
                f |= 1<<c;
            }
            if(Integer.bitCount(f) <= maxLetters){
                if(!map.containsKey(ss)){
                    map.put(ss, 1);
                }else{
                    map.put(ss, map.get(ss) + 1);
                }
            }else{
                break;
            }
        }
    }
    int ret = 0;
    for(int v : map.values()){
        ret = Math.max(ret, v);
    }
    return ret;
}
*/