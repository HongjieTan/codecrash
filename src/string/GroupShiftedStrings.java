package string;

import java.util.*;

/**
 *
 * seems easy...
 */
public class GroupShiftedStrings {
    public List<List<String>> groupStrings(String[] strings) {

        Map<String, List<String>> map = new HashMap<>();
        for(String str: strings) {
            String key = normalization(str);
            if (!map.containsKey(key)) {
                map.put(key, new ArrayList<>());
            }
            map.get(key).add(str);
        }

        List<List<String>> res = new ArrayList<>();
        for(String key: map.keySet()) {
           Collections.sort(map.get(key));
        }
        res.addAll(map.values());
        return res;

    }

    private String normalization(String str) {
        int k = str.charAt(0)-'a';
        StringBuffer sb = new StringBuffer();
        for (char c: str.toCharArray())
            sb.append(c-k);
        return sb.toString();
    }
}
