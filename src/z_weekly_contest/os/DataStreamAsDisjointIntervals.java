//Given a data stream input of non-negative integers a1, a2, ..., an, ..., summarize the numbers seen so far as a list of disjoint intervals.
//
//For example, suppose the integers from the data stream are 1, 3, 7, 2, 6, ..., then the summary will be:
//
//[1, 1]
//[1, 1], [3, 3]
//[1, 1], [3, 3], [7, 7]
//[1, 3], [7, 7]
//[1, 3], [6, 7]
//
//
//Follow up:
//
//What if there are lots of merges and the number of disjoint intervals are small compared to the data stream's size?
package z_weekly_contest.os;


import java.util.*;


class SummaryRanges {

    /** Initialize your data structure here. */
    TreeMap<Integer, Integer> tree;

    public SummaryRanges() {
        tree = new TreeMap<>();
    }

    public void addNum(int val) {
        Map.Entry<Integer, Integer> entry1 = tree.floorEntry(val);
        Map.Entry<Integer, Integer> entry2 = tree.ceilingEntry(val);
        int s1=entry1!=null?entry1.getKey():Integer.MIN_VALUE;
        int e1=entry1!=null?entry1.getValue():Integer.MIN_VALUE;
        int s2=entry2!=null?entry2.getKey():Integer.MIN_VALUE;
        int e2=entry2!=null?entry2.getValue():Integer.MIN_VALUE;

        if(s1<=val && val<=e1) return;    // case1: fit in existing range
        if(s2==val+1 && e1==val-1) {      // case1: s1-e1, val, s2-e2     ->   s1-e2
            tree.put(s1, e2);
            tree.remove(s2);
        } else if(e1==val-1) {            // case2: s1-e1,val,..., s2-e2  ->   s1-val,..., s2-e2
            tree.put(s1, val);
        } else if(s2==val+1) {            // case3: s1-e1,..., val,s2-e2  ->   s1-e1,..., val-e2
            tree.put(val, e2);
            tree.remove(s2);
        } else {                          // case4: s1-e1,..., val,...s2-e2  ->   s1-e1,...,val-val, ...s2-e2
            tree.put(val, val);
        }

    }

    public int[][] getIntervals() {
        int[][] itvs = new int[tree.size()][2];
        int i=0;
        for(Map.Entry<Integer, Integer> entry: tree.entrySet()) {
            itvs[i][0] = entry.getKey();
            itvs[i][1] = entry.getValue();
            i++;
        }
        return itvs;
    }

}

/**
 * Your SummaryRanges object will be instantiated and called as such:
 * SummaryRanges obj = new SummaryRanges();
 * obj.addNum(val);
 * int[][] param_2 = obj.getIntervals();
 */


public class DataStreamAsDisjointIntervals {
//    ["SummaryRanges","addNum","getIntervals","addNum","getIntervals","addNum","getIntervals","addNum","getIntervals","addNum","getIntervals"]
//            [[],[1],[],[3],[],[7],[],[2],[],[6],[]]

    public static void main(String[] as) {
        SummaryRanges obj = new SummaryRanges();
        obj.addNum(6);
        print(obj.getIntervals());
        obj.addNum(6);
        print(obj.getIntervals());
        obj.addNum(0);
        print(obj.getIntervals());
        obj.addNum(4);
        print(obj.getIntervals());
        obj.addNum(8);
        print(obj.getIntervals());
        obj.addNum(7);
        print(obj.getIntervals());
        obj.addNum(6);
        print(obj.getIntervals());
        obj.addNum(4);
        print(obj.getIntervals());
        obj.addNum(7);
        print(obj.getIntervals());
        obj.addNum(5);
        print(obj.getIntervals());

    }
    static void print(int[][] arr) {
        for(int[] x: arr) System.out.println(x[0]+","+x[1]);
        System.out.println();
    }
}

