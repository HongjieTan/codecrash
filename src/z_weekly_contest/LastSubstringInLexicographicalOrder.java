// Given a string s, return the last substring of s in lexicographical order.
//
//
//
// Example 1:
//
//
//Input: "abab"
//Output: "bab"
//Explanation: The substrings are ["a", "ab", "aba", "abab", "b", "ba", "bab"]. The lexicographically maximum substring is "bab".
//
//
// Example 2:
//
//
//Input: "leetcode"
//Output: "tcode"
//
//
//
//
// Note:
//
//
// 1 <= s.length <= 4 * 10^5
// s contains only lowercase English letters.
//
// Related Topics String Suffix Array

package z_weekly_contest;

public class LastSubstringInLexicographicalOrder {

    /*

    // todo
    public String lastSubstring(String s) {

    }
    */

    // bruteforce: O(n^2), TLE
//    public String lastSubstring(String s) {
//        String res = s;
//        int n = s.length();
//        for (int i = 0; i < n; i++) {
//            String sub = s.substring(i);
//            if (res.compareTo(sub)<0) {
//                res = sub;
//            }
//        }
//        return res;
//    }

    public static void main(String[] as) {
//        System.out.println(new LastSubstringInLexicographicalOrder().lastSubstring("abab"));
//        System.out.println(new LastSubstringInLexicographicalOrder().lastSubstring("leetcode"));
    }
}
