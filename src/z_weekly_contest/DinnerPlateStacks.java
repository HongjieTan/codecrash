//You have an infinite number of stacks arranged in a row and numbered (left to right) from 0, each of the stacks has the same maximum capacity.
//
// Implement the DinnerPlates class:
//
//
// DinnerPlates(int capacity) Initializes the object with the maximum capacity of the stacks.
// void push(int val) pushes the given positive integer val into the leftmost stack with size less than capacity.
// int pop() returns the value at the top of the rightmost non-empty stack and removes it from that stack, and returns -1 if all stacks are empty.
// int popAtStack(int index) returns the value at the top of the stack with the given index and removes it from that stack, and returns -1 if the stack with that given index is empty.
//
//
// Example:
//
//
//Input:
//["DinnerPlates","push","push","push","push","push","popAtStack","push","push","popAtStack","popAtStack","pop","pop","pop","pop","pop"]
//[[2],[1],[2],[3],[4],[5],[0],[20],[21],[0],[2],[],[],[],[],[]]
//Output:
//[null,null,null,null,null,null,2,null,null,20,21,5,4,3,1,-1]
//
//Explanation:
//DinnerPlates D = DinnerPlates(2);  // Initialize with capacity = 2
//D.push(1);
//D.push(2);
//D.push(3);
//D.push(4);
//D.push(5);         // The stacks are now:  2  4
//                                          1  3  5
//                                           ﹈ ﹈ ﹈
//D.popAtStack(0);   // Returns 2.  The stacks are now:     4
//                                                      1  3  5
//                                                       ﹈ ﹈ ﹈
//D.push(20);        // The stacks are now: 20  4
//                                          1  3  5
//                                           ﹈ ﹈ ﹈
//D.push(21);        // The stacks are now: 20  4 21
//                                          1  3  5
//                                           ﹈ ﹈ ﹈
//D.popAtStack(0);   // Returns 20.  The stacks are now:     4 21
//                                                       1  3  5
//                                                        ﹈ ﹈ ﹈
//D.popAtStack(2);   // Returns 21.  The stacks are now:     4
//                                                       1  3  5
//                                                        ﹈ ﹈ ﹈
//D.pop()            // Returns 5.  The stacks are now:      4
//                                                       1  3
//                                                        ﹈ ﹈
//D.pop()            // Returns 4.  The stacks are now:   1  3
//                                                        ﹈ ﹈
//D.pop()            // Returns 3.  The stacks are now:   1
//                                                        ﹈
//D.pop()            // Returns 1.  There are no stacks.
//D.pop()            // Returns -1.  There are still no stacks.
//
//
//
// Constraints:
//
//
// 1 <= capacity <= 20000
// 1 <= val <= 20000
// 0 <= index <= 100000
// At most 200000 calls will be made to push, pop, and popAtStack.
//
//
package z_weekly_contest;
import java.util.*;

/**
 * Your DinnerPlates object will be instantiated and called as such:
 * DinnerPlates obj = new DinnerPlates(capacity);
 * obj.push(val);
 * int param_2 = obj.pop();
 * int param_3 = obj.popAtStack(index);
 */

public class DinnerPlateStacks {
    public static void main(String[] as) {
        DinnerPlates D = new DinnerPlates(1);
        D.push(1);
        D.push(2);
        System.out.println(D.popAtStack(1));
        System.out.println(D.pop());
        D.push(1);
        D.push(2);
        System.out.println(D.pop());
        System.out.println(D.pop());

//        D.push(5);
//        System.out.println(D.popAtStack(0));
//        D.push(20);
//        D.push(21);
//        System.out.println(D.popAtStack(0));
//        System.out.println(D.popAtStack(2));
//        System.out.println(D.pop());
//        System.out.println(D.pop());
//        System.out.println(D.pop());
//        System.out.println(D.pop());
//        System.out.println(D.pop());
    }
}

/*
    1. brute force:
        push O(n)
        pop amortized O(1)
        popAtIndex O(1)
    2. maintain heap for push, rightmost for pop:
        push O(logn)
        pop amortized O(1)
        popAtIndex O(1ogn)
    3. improve 2, heap maintains pair <stack_index, remaining_slots_count>
        push O(log(stacks))
        pop amortized O(1)
        popAtIndex O(1og(stacks))
*/

// TLE....todo !!!
class DinnerPlates {
    LinkedList<Stack<Integer>> stacks;
    int capacity;
    TreeMap<Integer, Integer> heap;
    public DinnerPlates(int capacity) {
        this.capacity = capacity;
        this.stacks = new LinkedList<>();
        this.heap = new TreeMap<>();
    }

    public void push(int val) {
        if(!heap.isEmpty()) {
            if(heap.firstKey()<stacks.size()-1) {
                int key = heap.firstKey();
                stacks.get(key).push(val);
                if(heap.get(key)==1) heap.remove(key);
                else heap.put(key, heap.get(key)-1);
                return;
            } else {
                heap.clear();
            }
        }

        if(stacks.isEmpty() || stacks.getLast().size() == capacity) {
            Stack<Integer> st = new Stack<>();
            st.push(val);
            stacks.add(st);
        } else {
            stacks.getLast().push(val);
        }
    }

    public int pop() {
        while (!stacks.isEmpty() && stacks.getLast().size()==0)
            stacks.removeLast();

        if(stacks.isEmpty())
            return -1;
        else
            return stacks.getLast().pop();
    }

    public int popAtStack(int index) {
        if(index >= stacks.size()) return -1;
        Stack<Integer> st = stacks.get(index);
        if(st.isEmpty()) {
            return -1;
        } else {
            if (index<stacks.size()-1) {
                heap.put(index, heap.getOrDefault(index, 0)+1);
            }
            return st.pop();
        }
    }
}


// TLE...
class DinnerPlates_v2 {
    LinkedList<Stack<Integer>> stacks;
    int capacity;
    PriorityQueue<Integer> heap;
    public DinnerPlates_v2(int capacity) {
        this.capacity = capacity;
        this.stacks = new LinkedList<>();
        this.heap = new PriorityQueue<>();
    }

    public void push(int val) {
        if(!heap.isEmpty()) {
            if(heap.peek()<stacks.size()-1) {
                stacks.get(heap.poll()).push(val);
                return;
            } else {
                heap.clear();
            }
        }

        if(stacks.isEmpty() || stacks.getLast().size() == capacity) {
            Stack<Integer> st = new Stack<>();
            st.push(val);
            stacks.add(st);
        } else {
            stacks.getLast().push(val);
        }
    }

    public int pop() {
        while (!stacks.isEmpty() && stacks.getLast().size()==0)
            stacks.removeLast();

        if(stacks.isEmpty())
            return -1;
        else
            return stacks.getLast().pop();
    }

    public int popAtStack(int index) {
        if(index >= stacks.size()) return -1;
        Stack<Integer> st = stacks.get(index);
        if(st.isEmpty()) {
            return -1;
        } else {
            if (index<stacks.size()-1) heap.add(index);
            return st.pop();
        }
    }
}

/*
by top post:
Use a heap to record all the popAtStack- index. Insert at these locations first.
If the heap is empty, (that means all the stacks except the last one are full) then insert at the end of the stacks.

class DinnerPlates:

    def __init__(self, capacity: int):
        self.queue = []
        self.c = capacity
        self.emp = [] #non-full stacks, if the same index appears twice, that means it has two empty positions in this stack.

    def push(self, val: int) -> None:
        print(self.queue,self.emp)
        if self.emp:
            l = heapq.heappop(self.emp)
            if l < len(self.queue): #in some cases, the pop is called too many times, and the queue is shorter than the index
                self.queue[l] += [val]
                return
            else: self.emp = []
        if len(self.queue)>0 and len(self.queue[-1]) < self.c:
            self.queue[-1] += [val]
            return
        self.queue += [[val]]
        #print(self.queue)

    def pop(self) -> int:
        while len(self.queue) > 0 and not self.queue[-1]:
            self.queue.pop()
        if self.queue:
            res = self.queue[-1][-1]
            self.queue[-1].pop()
            return res
        return -1

    def popAtStack(self, index: int) -> int:
        if index < len(self.queue) and len(self.queue[index]) > 0:
            res = self.queue[index][-1]
            self.queue[index].pop()
            heapq.heappush(self.emp,index)
            return res
        return -1
*/

/*
by uwi:
class DinnerPlates {
    int cap;
    SegmentTreeRMQ st;
    SegmentTreeRMQ rst;
    List<Deque<Integer>> qs;
    int n = 100005;

    public DinnerPlates(int capacity) {
        cap = capacity;
        st = new SegmentTreeRMQ(n + 1);
        rst = new SegmentTreeRMQ(n + 1);
        qs = new ArrayList<>();
        for(int i = 0;i < n;i++){
            qs.add(new ArrayDeque<Integer>());
        }
    }

    public void push(int val) {
        int which = st.firstle(0, cap - 1);
        if(which != -1 && which < n){
            qs.get(which).add(val);
            st.update(which, qs.get(which).size());
            rst.update(which, -qs.get(which).size());
        }
    }

    public int pop() {
        int which = rst.lastle(n-1, -1);
        if(which == -1)return -1;

        int v = qs.get(which).pollLast();
        st.update(which, qs.get(which).size());
        rst.update(which, -qs.get(which).size());
        return v;
    }

    public int popAtStack(int index) {
        if(qs.get(index).isEmpty())return -1;
        int v = qs.get(index).pollLast();
        st.update(index, qs.get(index).size());
        rst.update(index, -qs.get(index).size());
        return v;
    }

    public class SegmentTreeRMQ {
        public int M, H, N;
        public int[] st;

        public SegmentTreeRMQ(int n)
        {
            N = n;
            M = Integer.highestOneBit(Math.max(N-1, 1))<<2;
            H = M>>>1;
            st = new int[M];
            Arrays.fill(st, 0, M, 0);
        }

        public SegmentTreeRMQ(int[] a)
        {
            N = a.length;
            M = Integer.highestOneBit(Math.max(N-1, 1))<<2;
            H = M>>>1;
            st = new int[M];
            for(int i = 0;i < N;i++){
                st[H+i] = a[i];
            }
            Arrays.fill(st, H+N, M, Integer.MAX_VALUE);
            for(int i = H-1;i >= 1;i--)propagate(i);
        }

        public void update(int pos, int x)
        {
            st[H+pos] = x;
            for(int i = (H+pos)>>>1;i >= 1;i >>>= 1)propagate(i);
        }

        private void propagate(int i)
        {
            st[i] = Math.min(st[2*i], st[2*i+1]);
        }

        public int minx(int l, int r){
            int min = Integer.MAX_VALUE;
            if(l >= r)return min;
            while(l != 0){
                int f = l&-l;
                if(l+f > r)break;
                int v = st[(H+l)/f];
                if(v < min)min = v;
                l += f;
            }

            while(l < r){
                int f = r&-r;
                int v = st[(H+r)/f-1];
                if(v < min)min = v;
                r -= f;
            }
            return min;
        }

        public int min(int l, int r){ return l >= r ? 0 : min(l, r, 0, H, 1);}

        private int min(int l, int r, int cl, int cr, int cur)
        {
            if(l <= cl && cr <= r){
                return st[cur];
            }else{
                int mid = cl+cr>>>1;
                int ret = Integer.MAX_VALUE;
                if(cl < r && l < mid){
                    ret = Math.min(ret, min(l, r, cl, mid, 2*cur));
                }
                if(mid < r && l < cr){
                    ret = Math.min(ret, min(l, r, mid, cr, 2*cur+1));
                }
                return ret;
            }
        }

        public int firstle(int l, int v) {
            int cur = H+l;
            while(true){
                if(st[cur] <= v){
                    if(cur < H){
                        cur = 2*cur;
                    }else{
                        return cur-H;
                    }
                }else{
                    cur++;
                    if((cur&cur-1) == 0)return -1;
                    if((cur&1)==0)cur>>>=1;
                }
            }
        }

        public int lastle(int l, int v) {
            int cur = H+l;
            while(true){
                if(st[cur] <= v){
                    if(cur < H){
                        cur = 2*cur+1;
                    }else{
                        return cur-H;
                    }
                }else{
                    if((cur&cur-1) == 0)return -1;
                    cur--;
                    if((cur&1)==1)cur>>>=1;
                }
            }
        }
    }

}
*/

/*
by lee:
Solution 1
Use a list stacks to save all stacks.
Use a heap queue q to find the leftmost available stack.

push, O(logN) time
pop, amortized O(1) time
popAtStack, O(logN) time

Python

    def __init__(self, capacity):
        self.c = capacity
        self.q = []
        self.stacks = []

    def push(self, val):
        while self.q and self.q[0] < len(self.stacks) and len(self.stacks[self.q[0]]) == self.c:
            heapq.heappop(self.q)
        if not self.q:
            heapq.heappush(self.q, len(self.stacks))
            self.stacks.append([])
        self.stacks[self.q[0]].append(val)

    def pop(self):
        while self.stacks and not self.stacks[-1]:
            self.stacks.pop()
        return self.popAtStack(len(self.stacks) - 1)

    def popAtStack(self, index):
        if 0 <= index < len(self.stacks) and self.stacks[index]:
            heapq.heappush(self.q, index)
            return self.stacks[index].pop()
        return -1
Solution 2
Use a map m to keep the mapping between index and stack
Use a set available to keep indices of all no full stacks.

C++:

    int c;
    map<int, vector<int>> m;
    set<int> available;

    DinnerPlates(int capacity) {
        c = capacity;
    }

    void push(int val) {
        if (!available.size())
            available.insert(m.size());
        m[*available.begin()].push_back(val);
        if (m[*available.begin()].size() == c)
            available.erase(available.begin());
    }

    int pop() {
        if (m.size() == 0)
            return -1;
        return popAtStack(m.rbegin()->first);
    }

    int popAtStack(int index) {
        if (m[index].size() == 0)
            return -1;
        int val = m[index].back();
        m[index].pop_back();
        available.insert(index);
        if (m[index].size() == 0)
            m.erase(index);
        return val;
    }
*/