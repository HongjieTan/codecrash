// You have a set of tiles, where each tile has one letter tiles[i] printed on it.
// Return the number of possible non-empty sequences of letters you can make.
//
//
//
// Example 1:
//
//
//Input: "AAB"
//Output: 8
//Explanation: The possible sequences are "A", "B", "AA", "AB", "BA", "AAB", "ABA", "BAA".
//
//
//
// Example 2:
//
//
//Input: "AAABBC"
//Output: 188
//
//
//
//
//
// Note:
//
//
// 1 <= tiles.length <= 7
// tiles consists of uppercase English letters.
//
package z_weekly_contest;

import java.util.HashSet;
import java.util.Set;

public class LetterTilePossibilities {
    Set<String> res = new HashSet<>();
    public int numTilePossibilities(String tiles) {
        char[] s = tiles.toCharArray();
        helper(s, 0, new StringBuilder());
        return res.size();
    }

    void helper(char[] s, int start, StringBuilder path) {
        if(start>=s.length) return;
        for(int i=start; i<s.length; i++) {
            swap(s, i, start);

            path.append(s[start]);
            res.add(path.toString());

            helper(s, start+1, path);

            path.deleteCharAt(path.length()-1);
            swap(s, i, start);
        }
    }

    void swap(char[] s, int a, int b) {
        char tmp = s[a];
        s[a] = s[b];
        s[b] = tmp;
    }

    public static void main(String[] a) {
        System.out.println(new LetterTilePossibilities().numTilePossibilities("AAABBC"));
    }
}
