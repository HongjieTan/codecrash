/*
Given a matrix with r rows and c columns, find the maximum score of a path starting at [0, 0] and ending at [r-1, c-1].
The score of a path is the minimum value in that path. For example, the score of the path 8 → 4 → 5 → 9 is 4.

Don't include the first or final entry. You can only move either down or right at any point in time.

Example 1:

Input:
[[5, 1],
 [4, 5]]

Output: 4
Explanation:
Possible paths:
5 → 1 → 5 => min value is 1
5 → 4 → 5 => min value is 4
Return the max value among minimum values => max(4, 1) = 4.

Example 2:
Input:
[[1, 2, 3]
 [4, 5, 1]]
Output: 4
Explanation:
Possible paths:
1-> 2 -> 3 -> 1
1-> 2 -> 5 -> 1
1-> 4 -> 5 -> 1
So min of all the paths = [2, 2, 4]. Note that we don't include the first and final entry.
Return the max of that, so 4.
Related problems:

https://leetcode.com/problems/minimum-path-sum/
https://leetcode.com/problems/unique-paths-ii/
https://leetcode.com/problems/path-with-maximum-minimum-value (premium) is a different problem. In this problem we can only move in 2 directions.
*/
package z_weekly_contest._new;

public class PathWithMaxMinValueI {

    int maxScore_v2(int[][] grid) {
        int n=grid.length, m=grid[0].length;
        int[][] dp = new int[n][m]; // path score at (i,j)
        for(int i=0;i<n;i++) {
            for(int j=0;j<m;j++) {
                if(i==0 && j==0) dp[i][j]=Integer.MAX_VALUE;
                else {
                    int max = Integer.MIN_VALUE;
                    if (i>0) max = Math.max(max, Math.min(dp[i-1][j], grid[i][j]));
                    if (j>0) max = Math.max(max, Math.min(dp[i][j-1], grid[i][j]));
                    dp[i][j] = max;
                }
            }
        }
        return Math.max(dp[n-1][m-2], dp[n-2][m-1]);
    }

   int maxScore(int[][] grid) {
        int n = grid.length, m = grid[1].length;
        int[][] dp = new int[n][m];
        for(int i=0;i<n;i++) {
            for(int j=0;j<m;j++) {
                if (i==0 && j==0) dp[i][j]=Integer.MAX_VALUE;
                else if (i==0) dp[i][j] = Math.min(grid[i][j], dp[i][j-1]);
                else if (j==0) dp[i][j] = Math.min(grid[i][j], dp[i-1][j]);
                else {
                    int score1 = Math.min(grid[i][j], dp[i][j-1]);
                    int score2 = Math.min(grid[i][j], dp[i-1][j]);
                    dp[i][j] = Math.max(score1, score2);
                }
            }
        }
        return Math.max(dp[n-1][m-2], dp[n-2][m-1]); // ignore start/end position
    }

    public static void main(String[] as) {
        int[][] grid = {
                 {1, 2, 3},
                 {4, 5, 1},
//                 {5, 1},
//                 {4, 5},
                };
        System.out.println(new PathWithMaxMinValueI().maxScore(grid));
        System.out.println(new PathWithMaxMinValueI().maxScore_v2(grid));
    }
}


/*
// DP (2D)
private static int maxScore2D(int[][] grid) {
  // Assume there is at least one element
  int r = grid.length, c = grid[0].length;
  int[][] dp = new int[r][c];
  // Init
  dp[0][0] = Integer.MAX_VALUE; // first entry is not considered
  for (int i = 1; i < r; ++i) dp[i][0] = Math.min(dp[i - 1][0], grid[i][0]);
  for (int j = 1; j < c; ++j) dp[0][j] = Math.min(dp[0][j - 1], grid[0][j]);
  // DP
  for (int i = 1; i < r; ++i) { // row by row
    for (int j = 1; j < c; ++j) {
      if (i == r - 1 && j == c - 1) {
        dp[i][j] = Math.max(dp[i - 1][j], dp[i][j - 1]); // last entry is not considered
      } else {
        int score1 = Math.min(dp[i][j - 1], grid[i][j]); // left
        int score2 = Math.min(dp[i - 1][j], grid[i][j]); // up
        dp[i][j] = Math.max(score1, score2);
      }
    }
  }
  return dp[r - 1][c - 1];
}
*/


