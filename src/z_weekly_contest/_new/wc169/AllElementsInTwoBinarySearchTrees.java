/*
Given two binary search trees root1 and root2.

Return a list containing all the integers from both trees sorted in ascending order.



Example 1:


Input: root1 = [2,1,4], root2 = [1,0,3]
Output: [0,1,1,2,3,4]
Example 2:

Input: root1 = [0,-10,10], root2 = [5,1,7,0,2]
Output: [-10,0,0,1,2,5,7,10]
Example 3:

Input: root1 = [], root2 = [5,1,7,0,2]
Output: [0,1,2,5,7]
Example 4:

Input: root1 = [0,-10,10], root2 = []
Output: [-10,0,10]
Example 5:


Input: root1 = [1,null,8], root2 = [8,1]
Output: [1,1,8,8]


Constraints:

Each tree has at most 5000 nodes.
Each node's value is between [-10^5, 10^5].


*/
package z_weekly_contest._new.wc169;

import common.TreeNode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AllElementsInTwoBinarySearchTrees {

    // todo one pass ? iterative ?



//    List<Integer> ret = new ArrayList<>();
//    public List<Integer> getAllElements(TreeNode root1, TreeNode root2) {
//        dfs(root1);
//        dfs(root2);
//        Collections.sort(ret);
//        return ret;
//    }
//    void dfs(TreeNode root) {
//        if(root==null) return;
//        ret.add(root.val);
//        dfs(root.left);
//        dfs(root.right);
//    }
}


/*
We are doing in-order traversal independently for two trees. When it's time to 'visit' a node on the top of the stack, we are visitng the smallest of two.

void pushLeft(stack<TreeNode*> &s, TreeNode* n) {
    if (n == nullptr) return;
    s.push(n);
    pushLeft(s, n->left);
}
vector<int> getAllElements(TreeNode* root1, TreeNode* root2) {
    vector<int> res;
    stack<TreeNode*> s1, s2;
    pushLeft(s1, root1);
    pushLeft(s2, root2);
    while (!s1.empty() || !s2.empty()) {
        stack<TreeNode*> &s = s1.empty() ? s2 : s2.empty() ? s1 :
            s1.top()->val < s2.top()->val ? s1 : s2;
        auto n = s.top(); s.pop();
        res.push_back(n->val);
        pushLeft(s, n->right);
    }
    return res;
}
*/