/*
We are given S, a length n string of characters from the set {'D', 'I'}.
(These letters stand for "decreasing" and "increasing".)

A valid permutation is a permutation P[0], P[1], ..., P[n] of integers {0, 1, ..., n}, such that for all i:

If S[i] == 'D', then P[i] > P[i+1], and;
If S[i] == 'I', then P[i] < P[i+1].
How many valid permutations are there?  Since the answer may be large, return your answer modulo 10^9 + 7.



Example 1:

Input: "DID"
Output: 5
Explanation:
The 5 valid permutations of (0, 1, 2, 3) are:
(1, 0, 3, 2)
(2, 0, 3, 1)
(2, 1, 3, 0)
(3, 0, 2, 1)
(3, 1, 2, 0)


Note:

1 <= S.length <= 200
S consists only of characters from the set {'D', 'I'}.
*/
package z_weekly_contest._new.wc101;



// !!!
public class ValidPermutationsForDISequence {
    public int numPermsDISequence_dp(String S) {
        int n = S.length();



        return -1;
    }


    // backtrack with pruning, O(n!)
    int ret =0;
    boolean[] used;
    int n;
    char[] chs;
    public int numPermsDISequence_backtrack(String S) {
        n=S.length();
        used = new boolean[n+1];
        chs = S.toCharArray();
        find(0, 0);
        return ret;
    }

    void find(int idx, int prev) {
        if(idx>=n+1) {
            ret++;
            return;
        }

        int l=0,r=n;
        if(idx>0) {
            if(chs[idx-1]=='I') l = prev+1;
            if(chs[idx-1]=='D') r = prev-1;
        }
        for(int i=l;i<=r;i++) {
            if(used[i]) continue;

            used[i]=true;
            find(idx+1, i);
            used[i]=false;
        }
    }
}

/*
by uwi:
class Solution {
    public int numPermsDISequence(String S) {
        int n = S.length();
        int[] a = new int[n+1];
        int p = 0;
        int len = 0;
        for(int i = 0;i < n;i++){
            if(S.charAt(i) == 'D'){
                len++;
            }else{
                a[p++] = len+1;
                len = 0;
            }
        }
        a[p++] = len+1;

        int mod = 1000000007;
        long[][] C = new long[400 + 1][400 + 1];
        for (int i = 0; i <= 400; i++) {
            C[i][0] = 1;
            for (int j = 1; j <= i; j++) {
                C[i][j] = C[i - 1][j - 1] + C[i - 1][j];
                if (C[i][j] >= mod)
                    C[i][j] -= mod;
            }
        }

        a = Arrays.copyOf(a, p);
        long[] dp = new long[p+1];
        dp[0] = 1;
        int all = 0;
        for(int i = 1;i <= p;i++){
            all += a[i-1];
            int s = 0;
            for(int j = i-1, sgn = 1;j >= 0;j--, sgn = -sgn){
                s += a[j];
                dp[i] += dp[j] * C[all][s] * sgn;
                dp[i] %= mod;
            }
            if(dp[i] < 0)dp[i] += mod;
        }
        return (int)dp[p];
    }
}
*/
