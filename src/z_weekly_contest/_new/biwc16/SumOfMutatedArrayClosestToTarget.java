/*
Given an integer array arr and a target value target, return the integer value such that when we change all the integers larger than value in the given array to be equal to value, the sum of the array gets as close as possible (in absolute difference) to target.

In case of a tie, return the minimum such integer.

Notice that the answer is not neccesarilly a number from arr.



Example 1:

Input: arr = [4,9,3], target = 10
Output: 3
Explanation: When using 3 arr converts to [3, 3, 3] which sums 9 and that's the optimal answer.
Example 2:

Input: arr = [2,3,5], target = 10
Output: 5
Example 3:

Input: arr = [60864,25176,27249,21296,20204], target = 56803
Output: 11361


Constraints:

1 <= arr.length <= 10^4
1 <= arr[i], target <= 10^5
*/
package z_weekly_contest._new.biwc16;

// #binarysearch #sort
public class SumOfMutatedArrayClosestToTarget {

//    public int findBestValue_v1(int[] arr, int target) {
//        int max = arr[0];
//        for(int x: arr) max = Math.max(max, x);
//        int l=1, r=max, d=-1, v=-1;
//        while(l<=r) {
//            int mid = l+(r-l)/2;
//            int diff = Math.abs(sum(arr, mid)-target);
//            if(s<target) l=mid+1;
//            else if(s>target) r=mid-1;
//            else return mid;
//        }
//    }

    public int findBestValue_v1(int[] arr, int target) {
        int max = arr[0];
        for(int x: arr) max = Math.max(max, x);
        int l=1, r=max;
        while(l<=r) {
            int mid = l+(r-l)/2;
            int s = sum(arr, mid);
            if(s<target) l=mid+1;
            else if(s>target) r=mid-1;
            else return mid;
        }
        int d1 = Math.abs(sum(arr, r)-target);
        int d2 = Math.abs(sum(arr, l)-target);
        return d1<=d2?r:l;
    }

    int sum(int[] arr, int v) {
        int ret = 0;
        for(int x: arr) ret += x<v?x:v;
        return ret;
    }
}

/*
class Solution {
public:
    int findBestValue(vector<int>& x, int t) {
        sort(x.begin(), x.end());
        int n = x.size();
        x.push_back(INT_MAX);
        int at = 0, best = INT_MAX, bi, sum = 0;
        for (int i = 0; i <= t + 5; i++) {
            while (x[at] <= i) {
                sum += x[at];
                at++;
            }
            int tmp = sum + (n - at) * i;
            tmp = abs(tmp - t);
            if (tmp < best) {
                best = tmp;
                bi = i;
            }
        }
        return bi;
    }
};


concise binary search:
class Solution {
public:
    int findBestValue(vector<int>& arr, int target) {
        int st = 0, ed = target;
        int ans = -1, v = -1;
        while(st <= ed){
            int mid = (st+ed)/2;
            int temp = 0;
            for(const auto i : arr)
                temp += min(mid, i);
            temp -= target;
            if(ans < 0 || ans > abs(temp)){
                ans = abs(temp);
                v = mid;
            }
            if(temp < 0)
                st = mid + 1;
            else
                ed = mid - 1;
        }
        return v;
    }
};


by lee:
Explanation
Binary search O(NlogMax(A)).
In order to ruduce the difficulty, it constrains A[i] < 10 ^ 5.

In this solution,
we sort the input and compared A[i] with target one by one.

Complexity
Time O(NlogN)
Space O(1)

Python:
    def findBestValue(self, A, target):
        A.sort(reverse=1)
        maxA, n = A[0], len(A)
        while A and target >= A[-1] * len(A):
            target -= A.pop()
        return int((target + len(A) / 2.0 - 0.1) / len(A)) if A else maxA
*/