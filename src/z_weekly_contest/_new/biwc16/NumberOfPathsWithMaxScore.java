/*
You are given a square board of characters. You can move on the board starting at the bottom right square marked with the character 'S'.

You need to reach the top left square marked with the character 'E'. The rest of the squares are labeled either with a numeric character 1, 2, ..., 9 or with an obstacle 'X'. In one move you can go up, left or up-left (diagonally) only if there is no obstacle there.

Return a list of two integers: the first integer is the maximum sum of numeric characters you can collect, and the second is the number of such paths that you can take to get that maximum sum, taken modulo 10^9 + 7.

In case there is no path, return [0, 0].



Example 1:

Input: board = ["E23","2X2","12S"]
Output: [7,1]
Example 2:

Input: board = ["E12","1X1","21S"]
Output: [4,2]
Example 3:

Input: board = ["E11","XXX","11S"]
Output: [0,0]


Constraints:

2 <= board.length == board[i].length <= 100

*/
package z_weekly_contest._new.biwc16;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class NumberOfPathsWithMaxScore {

//    public int[] pathsWithMaxScore(List<String> board) {
//        int n = board.size(), m = board.get(0).length();
//        char[][] grid = new char[n][m];
//        for(int i=0;i<n;i++) grid[i]=board.get(i).toCharArray();
//
//        int[][] dp = new int[n][m];
//        for(int i=n-1;i>=0;i--) {
//            for(int j=m-1;j>=0;j--) {
//                if(grid[i][j]=='S') dp[i][j]=0;
//                else if(grid[i][j]=='X') dp[i][j]=-1;
//                else {
//                    dp[i][j] = -1;
//                    if(i<n-1) dp[i][j] = Math.max(dp[i][j], dp[i+1][j]);
//                    if(j<m-1) dp[i][j] = Math.max(dp[i][j], dp[i][j+1]);
//                    if(i<n-1 && j<m-1) dp[i][j] = Math.max(dp[i][j], dp[i+1][j+1]);
//                    if(dp[i][j]>=0 && grid[i][j]!='E') {
//                        dp[i][j] += grid[i][j]-'0';
//                    }
//                }
//            }
//        }
//        return -1;
//    }

//    public int[] pathsWithMaxScore(List<String> board) {
//        int n = board.size(), m = board.get(0).length();
//        char[][] grid = new char[n][m];
//        for(int i=0;i<n;i++) grid[i]=board.get(i).toCharArray();
//
//        int[][] dp = new int[n][m];
//        for(int i=n-1;i>=0;i--) {
//            for(int j=m-1;j>=0;j--) {
//                if(grid[i][j]=='S') dp[i][j]=0;
//                else if(grid[i][j]=='X') dp[i][j]=-1;
//                else {
//                    dp[i][j] = -1;
//                    if(i<n-1) dp[i][j] = Math.max(dp[i][j], dp[i+1][j]);
//                    if(j<m-1) dp[i][j] = Math.max(dp[i][j], dp[i][j+1]);
//                    if(i<n-1 && j<m-1) dp[i][j] = Math.max(dp[i][j], dp[i+1][j+1]);
//                    if(dp[i][j]>=0 && grid[i][j]!='E') {
//                        dp[i][j] += grid[i][j]-'0';
//                    }
//                }
//            }
//        }
//
//        if(dp[0][0]==-1) return new int[]{0,0};
//        int cnt=0, maxscore = dp[0][0];
//        // System.out.println("max score " + maxscore);
//
//        Queue<int[]> q = new LinkedList<>();
//        q.add(new int[]{0,0,maxscore});
//        while(!q.isEmpty()) {
//            int[] cur = q.remove();
//            int x=cur[0], y=cur[1], score=cur[2];
//            if(x==n-1 && y==m-1) {cnt++; continue;}
//
//            int prescore = grid[x][y]=='E'?score:(score-(grid[x][y]-'0'));
//
//            // System.out.println("look for " + prescore);
//            if(x+1<n && prescore==dp[x+1][y]) q.add(new int[]{x+1, y, prescore});
//            if(y+1<m && prescore==dp[x][y+1]) q.add(new int[]{x, y+1, prescore});
//            if(x+1<n && y+1<m && prescore==dp[x+1][y+1]) q.add(new int[]{x+1, y+1, prescore});
//        }
//
//        return new int[]{maxscore, cnt};
//    }
}


/*

hint1: Use dynamic programming to find the path with the max score.
hint2: Use another dynamic programming array to count the number of paths with max score.


by 2th:

#define MOD 1000000007

int dp[105][105][3];

void update(int a, int b, int c, int d) {
    if (c > dp[a][b][0] || dp[a][b][2] == 0) {
        dp[a][b][0] = c;
        dp[a][b][1] = d;
        dp[a][b][2] = 1;
    } else if (c == dp[a][b][0]) {
        dp[a][b][1] += d;
        if (dp[a][b][1] >= MOD)
            dp[a][b][1] -= MOD;
    }
}

class Solution {
public:
    vector<int> pathsWithMaxScore(vector<string>& x) {
        memset(dp, 0, sizeof(dp));
        int n = x.size();
        dp[n - 1][n - 1][1] = 1;
        dp[n - 1][n - 1][2] = 1;
        for (int i = n - 1; i >= 0; i--)
            for (int j = n - 1; j >= 0; j--)
                if (x[i][j] != 'X' && dp[i][j][2]) {
                    if (x[i][j] >= '1' && x[i][j] <= '9')
                        dp[i][j][0] += x[i][j] - '0';
                    if (i)
                        update(i - 1, j, dp[i][j][0], dp[i][j][1]);
                    if (j)
                        update(i, j - 1, dp[i][j][0], dp[i][j][1]);
                    if (i && j)
                        update(i - 1, j - 1, dp[i][j][0], dp[i][j][1]);
                }
        return vector<int>({dp[0][0][0], dp[0][0][1]});
    }
};



by lee:
def pathsWithMaxScore(self, A):
    n, mod = len(A), 10**9 + 7
    dp = [[[-10**5, 0] for j in xrange(n + 1)] for i in xrange(n + 1)]
    dp[n - 1][n - 1] = [0, 1]
    for x in range(n)[::-1]:
        for y in range(n)[::-1]:
            if A[x][y] in 'XS': continue
            for i, j in [[0, 1], [1, 0], [1, 1]]:
                if dp[x][y][0] < dp[x + i][y + j][0]:
                    dp[x][y] = [dp[x + i][y + j][0], 0]
                if dp[x][y][0] == dp[x + i][y + j][0]:
                    dp[x][y][1] += dp[x + i][y + j][1]
            dp[x][y][0] += int(A[x][y]) if x or y else 0
    return [dp[0][0][0] if dp[0][0][1] else 0, dp[0][0][1] % mod]

*/