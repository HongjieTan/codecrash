/*
Given a binary tree, return the sum of values of its deepest leaves.


Example 1:



Input: root = [1,2,3,4,5,null,6,7,null,null,null,null,8]
Output: 15


Constraints:

The number of nodes in the tree is between 1 and 10^4.
The value of nodes is between 1 and 100.
*/
package z_weekly_contest._new.biwc16;

import common.TreeNode;

public class DeepestLeavesSum {
    int ret = 0;
    int h = 0;
    public int deepestLeavesSum(TreeNode root) {
        h = height(root);
        dfs(root, 1);
        return ret;
    }

    void dfs(TreeNode root, int d) {
        if(root==null) return;
        if(d==h) {
            ret+=root.val;
            return;
        }
        dfs(root.left, d+1);
        dfs(root.right, d+1);
    }

    int height(TreeNode t) {
        if(t==null) return 0;
        return Math.max(height(t.left), height(t.right))+1;
    }
}

/*
one pass:
class Solution {
public:
    int depth = -1, val = 0;
    int deepestLeavesSum(TreeNode* root) {
        dfs(0, root);
        return val;
    }

    void dfs(int d, TreeNode* node){
        if(node == nullptr)
            return;

        if(depth < d){
            depth = d;
            val = node->val;
        }
        else if(depth == d)
            val += node->val;

        dfs(d+1, node->left);
        dfs(d+1, node->right);
    }
};

*/