/*
https://leetcode.jp/problemdetail.php?id=1102
Given a matrix of integers A with R rows and C columns, find the maximum score of a path starting at [0,0] and ending at [R-1,C-1].
The score of a path is the minimum value in that path.  For example, the value of the path 8 →  4 →  5 →  9 is 4.
A path moves some number of times from one visited cell to any neighbouring unvisited cell in one of the 4 cardinal directions (north, east, west, south).

Example 1:
Input: [[5,4,5],[1,2,6],[7,4,6]]
Output: 4
Explanation:
The path with the maximum score is highlighted in yellow.

Example 2:
Input: [[2,2,1,2,2,2],[1,2,2,2,1,2]]
Output: 2

Example 3:
Input: [[3,4,6,3,4],[0,2,1,1,7],[8,8,3,2,7],[3,2,4,9,8],[4,1,2,0,0],[4,6,5,4,3]]
Output: 3

Note:
1 <= R, C <= 100
0 <= A[i][j] <= 10^9
*/
package z_weekly_contest._new;
import java.util.PriorityQueue;

/*

类比 minimum spanning tree：
- 1. kruskal: union find
- 2. prim:    priority queue

#priorityqueue #union_find
 */
public class PathWithMaxMinValueII {
    // union find
    public int maximumMinimumPath_v2(int[][] A) {
        return -1;
    }

    // priorityqueue, O(m*n*log(m*n))
    public int maximumMinimumPath_v1(int[][] A) {
        int ret = Integer.MAX_VALUE;
        int[][] dirs = {{1,0},{-1,0},{0,1},{0,-1}};
        int n=A.length, m=A[0].length;
        boolean[][] visited = new boolean[n][m];
        PriorityQueue<int[]> pq = new PriorityQueue<>((a,b)->A[b[0]][b[1]]-A[a[0]][a[1]]);
        pq.add(new int[]{0,0});
        while (!pq.isEmpty()) {
            int[] cur = pq.remove();
            int x = cur[0], y = cur[1];
            visited[x][y] = true;
            ret = Math.min(ret, A[x][y]);
            if (x==n-1 && y==m-1) return ret;

            for (int[] dir: dirs) {
                int nx=x+dir[0], ny=y+dir[1];
                if (nx>=0 && nx<n && ny>=0 && ny<m && !visited[nx][ny]) {
                    pq.add(new int[]{nx, ny});
                }
            }

        }
        return -1;
    }



    // brute force version, O(2^(m+n))
//    int n,m;
//    int[][] A;
//    int[][] maxscore;
//    int[][] dirs = {{1,0},{-1,0},{0,1},{0,-1}};
//    public int maximumMinimumPath(int[][] A) {
//        n = A.length;
//        m = A[0].length;
//        this.A = A;
//        maxscore = new int[n][m];
//        for (int i=0;i<n;i++) Arrays.fill(maxscore[i], Integer.MIN_VALUE);
//        dfs(0,0,Integer.MAX_VALUE);
//        return maxscore[n-1][m-1];
//    }
//
//    void dfs(int i, int j, int score) {
//        score = Math.min(score, A[i][j]);
//        if (score<=maxscore[i][j]) return;
//        maxscore[i][j]=score;
//        if (i==n-1 && j==m-1) return;
//        for (int[] dir: dirs) {
//            int ni = i+dir[0], nj=j+dir[1];
//            if (ni>=0 && ni<n && nj>=0 && nj<m) {
//                dfs(ni, nj, score);
//            }
//        }
//    }

    public static void main(String[] a) {
        int[][] A1 = {{5,4,5},{1,2,6},{7,4,6}};
        int[][] A2 = {{2,2,1,2,2,2},{1,2,2,2,1,2}};
        int[][] A3 = {{3,4,6,3,4},{0,2,1,1,7},{8,8,3,2,7},{3,2,4,9,8},{4,1,2,0,0},{4,6,5,4,3}};
//        System.out.println(new PathWithMaxMinValueII().maximumMinimumPath(A1));
//        System.out.println(new PathWithMaxMinValueII().maximumMinimumPath(A2));
//        System.out.println(new PathWithMaxMinValueII().maximumMinimumPath(A3));

    }


}
