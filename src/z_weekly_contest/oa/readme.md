https://leetcode.com/discuss/interview-question/344650/Amazon-Online-Assessment-Questions

Top N Competitors/Buzzwords ⭐⭐ [Experienced]  x1
Zombie in Matrix ⭐⭐ [Experienced]   x1 
Critical Routers ⭐⭐ [New Grad]  x1
Product Suggestions ⭐⭐ [New Grad | Experienced] x1
Number of Clusters ⭐⭐ [Experienced] x1
Reorder Data in Log Files ⭐⭐⭐ [Experienced] x1
Optimal Utilization ⭐⭐⭐ [Experienced]   x1
Min Cost to Connect Ropes / Min Time to Merge Files ⭐⭐⭐[Experienced]    x1
Treasure Island / Min Distance to Remove the Obstacle (BFS) ⭐⭐⭐ [Experienced]   x1
Treasure Island II  x1
Find Pair With Given Sum/Movies on Flight ⭐⭐ [Experienced, Intern]  x1
Copy List with Random Pointer ⭐⭐ [New Grad]     x1
Merge Two Sorted Lists ⭐⭐ [New Grad | Intern]   x1
Subtree of Another Tree ⭐⭐ [New Grad]   x1
Search a 2D Matrix II ⭐⭐ [New Grad]     x1
Critical Connections ⭐ [New Grad]       x1
Favorite Genres ⭐⭐ [New Grad]       x1
Two Sum - Unique Pairs ⭐⭐ [New Grad]    x1       
Spiral Matrix ⭐ [New Grad]  x1
Count substrings with exactly K distinct chars ⭐ [Intern]   x1
Max Of Min Altitudes ⭐⭐ [Intern]    x1
Longest Palindromic Substring ⭐⭐ [Intern]   x1
Substrings of size K with K distinct chars ⭐⭐ [Intern]  x1
Most Common Word ⭐⭐ [Intern]    x1
Distance Between Nodes in BST   x0.5
K Closest Points to Origin ⭐ [Intern]   x1/ topk, heap or quicksort...
Min Cost to Connect All Nodes (a.k.a. Min Cost to Add New Roads)    x1/mst
Min Cost to Repair Edges (MST)          x1/mst
Prison Cells After N Days               x0.5
Partition Labels                        x1
Subtree with Maximum Average            x1
Point of Lattice ⭐ [Experienced]       x1 
Merge Intervals ⭐       x1                       
Roll Dice ⭐             x1
Longest string without 3 consecutive characters     x1
Longest string made up of only vowels       x1


lc:
200
994