/*
Question:
You are on a flight and wanna watch two movies during this flight.
You are given List<Integer> movieDurations which includes all the movie durations.
You are also given the duration of the flight which is d in minutes.
Now, you need to pick two movies and the total duration of the two movies is less than or equal to (d - 30min).

Find the pair of movies with the longest total duration and return they indexes. If multiple found, return the pair with the longest movie.

Example 1:

Input: movieDurations = [90, 85, 75, 60, 120, 150, 125], d = 250
Output: [0, 6]
Explanation: movieDurations[0] + movieDurations[6] = 90 + 125 = 215 is the maximum number within 220 (250min - 30min)
*/

package z_weekly_contest.oa;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;



// sort + two pointers !
public class MoviesOnFlight {
    class Node{int val; int index;Node(int val, int index){this.val=val;this.index=index;}}

    List<Integer> findPair(int[] nums, int target) {

        target -= 30;
        int n = nums.length;
        Node[] a = new Node[n];
        for (int i=0;i<n;i++) a[i] = new Node(nums[i], i); // keep index information

        Arrays.sort(a, (x,y)->x.val-y.val);

        List<Integer> ret = new ArrayList<>();
        int l=0, r=n-1;
        int maxsum = 0;
        while(l<r) {
            int sum = a[l].val + a[r].val;
            if (sum>target) {
                r--;
            } else {
                if(sum>maxsum) {
                    maxsum = sum;
                    ret = Arrays.asList(a[l].index, a[r].index);
                }
                l++;
            }
        }

        return ret;
    }

    public static void main(String[] as) {
        int[] movieDurations = {90, 85, 75, 60, 120, 150, 125};
        int d = 250;
        System.out.println(new MoviesOnFlight().findPair(movieDurations, d));

    }
}
