//Given a 2D grid, each cell is either a zombie 1 or a human 0. Zombies can turn adjacent (up/down/left/right) human beings into zombies every hour. Find out how many hours does it take to infect all humans?
//
//Example:
//
//Input:
//[[0, 1, 1, 0, 1],
//[0, 1, 0, 1, 0],
//[0, 0, 0, 0, 1],
//[0, 1, 0, 0, 0]]
//
//Output: 2
//
//Explanation:
//At the end of the 1st hour, the status of the grid:
//[[1, 1, 1, 1, 1],
//[1, 1, 1, 1, 1],
//[0, 1, 0, 1, 1],
//[1, 1, 1, 0, 1]]
//
//At the end of the 2nd hour, the status of the grid:
//[[1, 1, 1, 1, 1],
//[1, 1, 1, 1, 1],
//[1, 1, 1, 1, 1],
//[1, 1, 1, 1, 1]]

package z_weekly_contest.oa;


import java.util.ArrayDeque;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

// similar: https://leetcode.com/problems/rotting-oranges/
public class ZombieInMatrix {
    int minHours(int[][] grid) {
        int[][] dirs = {{0,1},{0,-1},{1,0},{-1,0}};
        int n = grid.length, m = grid[0].length;
        Queue<int[]> q = new LinkedList<>();

        for(int i=0;i<n;i++) {
            for(int j=0;j<m;j++) {
                if(grid[i][j]==1) {
                    q.add(new int[]{i,j,0});
                }
            }
        }

        int ret = 0;
        while(!q.isEmpty()) {
            int[] cur = q.remove();
            int x=cur[0], y=cur[1], t=cur[2];
            ret = t;
            for(int[] dir: dirs) {
                int nx=x+dir[0], ny=y+dir[1];
                if(nx>=0 && nx<n && ny>=0 && ny<m && grid[nx][ny]==0) {
                    grid[nx][ny]=1;
                    q.add(new int[]{nx, ny, t+1});
                }
            }
        }

        return ret;

    }

    public static void main(String[] as) {
        int[][] g = {
                {0, 1, 1, 0, 1},
                {0, 1, 0, 1, 0},
                {0, 0, 0, 0, 1},
                {0, 1, 0, 0, 0}};
        System.out.println(new ZombieInMatrix().minHours(g));
    }


    //
private static final int ZOMBIE = 1;
private static final int[][] DIRS = {{0, 1}, {1, 0}, {0, -1}, {-1, 0}};
//
    public int minHours(List<List<Integer>> grid) {
        int people = 0;
        Queue<Point> zombies = new ArrayDeque<>();
        for (int r = 0; r < grid.size(); r++) {
            for (int c = 0; c < grid.get(0).size(); c++) {
                if (grid.get(r).get(c) == ZOMBIE) {
                    zombies.add(new Point(r, c));
                } else {
                    people++;
                }
            }
        }

        if (people == 0) return 0;

        for (int hours = 1; !zombies.isEmpty(); hours++) {
            for (int sz = zombies.size(); sz > 0; sz--) {
                Point zombie = zombies.poll();

                for (int[] dir : DIRS) {
                    int r = zombie.r + dir[0];
                    int c = zombie.c + dir[1];

                    if (isHuman(grid, r, c)) {
                        people--;
                        if (people == 0) return hours;
                        grid.get(r).set(c, ZOMBIE);
                        zombies.add(new Point(r, c));
                    }
                }
            }
        }
        return -1;
    }

    private boolean isHuman(List<List<Integer>> grid, int r, int c) {
        return r >= 0 && r < grid.size() &&c >= 0 && c < grid.get(0).size() && grid.get(r).get(c) != ZOMBIE;
    }

    private static class Point {
        int r, c;
        Point(int r, int c) {
            this.r = r;
            this.c = c;
        }
    }


}

//Java solution
//Time complexity: O(r * c).
//Space cmplexity: O(r * c).
