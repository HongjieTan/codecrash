/*
Given an int array nums and an int target, find how many unique pairs in the array such that their sum is equal to target.
Return the number of pairs.

Example 1:

Input: nums = [1, 1, 2, 45, 46, 46], target = 47
Output: 2
Explanation:
1 + 46 = 47
2 + 45 = 47


Example 2:

Input: nums = [1, 1], target = 2
Output: 1
Explanation:
1 + 1 = 2


Example 3:

Input: nums = [1, 5, 1, 5], target = 6
Output: 1
Explanation:
[1, 5] and [5, 1] are considered the same.
*/
package z_weekly_contest.oa;

import java.util.HashSet;
import java.util.Set;

public class TwoSum_UniquePaths {
    int numOfUniquePaths(int[] nums, int target) {
        Set<Integer> seen_num = new HashSet<>();
        Set<String> seen_pair = new HashSet<>();
        for(int x: nums) {
            if(seen_num.contains(target-x)) {
                seen_pair.add(keyOf(x, target-x));
            } else {
                seen_num.add(x);
            }
        }
        return seen_pair.size();
    }

    String keyOf(int x, int y) {
        return Math.min(x,y)+"#"+Math.max(x,y);
    }

    int numOfUniquePaths2(int[] nums, int target){
        Set<Integer> set = new HashSet<Integer>();
        Set<Integer> seen = new HashSet<Integer>();
        int count = 0;
        for(int num : nums){
            if(set.contains(target-num) && !seen.contains(num)){
                count++;
                seen.add(target-num);
                seen.add(num);
            }
            else if(!set.contains(num)){
                set.add(num);
            }
        }
        return count;
    }

    public static void main(String[] as) {
        System.out.println(new TwoSum_UniquePaths().numOfUniquePaths(new int[]{1, 46, 46, 1}, 47));
        System.out.println(new TwoSum_UniquePaths().numOfUniquePaths2(new int[]{1, 46, 46, 1}, 47));

//        System.out.println(new TwoSum_UniquePaths().numOfUniquePaths(new int[]{1, 1, 2, 45, 46, 46}, 47));
//        System.out.println(new TwoSum_UniquePaths().numOfUniquePaths(new int[]{1, 1}, 2));
//        System.out.println(new TwoSum_UniquePaths().numOfUniquePaths(new int[]{1, 5, 1, 5}, 6));
    }
}
