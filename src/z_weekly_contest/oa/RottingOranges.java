// In a given grid, each cell can have one of three values:
//
//
// the value 0 representing an empty cell;
// the value 1 representing a fresh orange;
// the value 2 representing a rotten orange.
//
//
// Every minute, any fresh orange that is adjacent (4-directionally) to a rotten orange becomes rotten.
//
// Return the minimum number of minutes that must elapse until no cell has a fresh orange. If this is impossible, return -1 instead.
//
//
//
//
// Example 1:
//
//
//
//
//Input: [[2,1,1],[1,1,0],[0,1,1]]
//Output: 4
//
//
//
// Example 2:
//
//
//Input: [[2,1,1],[0,1,1],[1,0,1]]
//Output: -1
//Explanation:  The orange in the bottom left corner (row 2, column 0) is never rotten, because rotting only happens 4-directionally.
//
//
//
// Example 3:
//
//
//Input: [[0,2]]
//Output: 0
//Explanation:  Since there are already no fresh oranges at minute 0, the answer is just 0.
//
//
//
//
// Note:
//
//
// 1 <= grid.length <= 10
// 1 <= grid[0].length <= 10
// grid[i][j] is only 0, 1, or 2.
//
//
//
//

package z_weekly_contest.oa;

import java.util.LinkedList;
import java.util.Queue;




// bfs
public class RottingOranges {
    public int orangesRotting(int[][] grid) {
        int[][] dirs = {{0,1},{0,-1},{1,0},{-1,0}};
        int n = grid.length, m = grid[0].length;
        Queue<int[]> q = new LinkedList<>();

        for(int i=0;i<n;i++) {
            for(int j=0;j<m;j++) {
                if(grid[i][j]==2) {
                    q.add(new int[]{i,j,0});
                }
            }
        }

        int ret = 0;
        while(!q.isEmpty()) {
            int[] cur = q.remove();
            int x=cur[0], y=cur[1], t=cur[2];
            ret = Math.max(ret, t);
            for(int[] dir: dirs) {
                int nx=x+dir[0], ny=y+dir[1];
                if(nx>=0 && nx<n && ny>=0 && ny<m && grid[nx][ny]==1) {
                    grid[nx][ny]=2;
                    q.add(new int[]{nx, ny, t+1});
                }
            }
        }

        for(int i=0;i<n;i++) {
            for(int j=0;j<m;j++) {
                if(grid[i][j]==1) return -1;
            }
        }

        return ret;
    }

    public static void main(String[] as) {
        int[][] g = {
                {2,1,1},
                {1,1,0},
                {0,1,1}};
        System.out.println(new RottingOranges().orangesRotting(g));
    }
}
