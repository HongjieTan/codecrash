//Description:
//Implement a function to return product suggestions using products from a product repository after each character is typed by the customer in the search bar.
//If there are more than THREE acceptable products, return the product name that is first in the alphabetical order.
//Only return product suggestions after the customer has entered two characters.
//Product suggestions must start with the characters already typed.
//Both the repository and the customer query should be compared in a CASE-INSENSITIVE way.
//
//Input:
//The input to the method/function consist of three arguments:
//
//numProducts, an integer representing the number of various products in Amazon's product repository;
//repository, a list of unique strings representing the various products in Amazon's product repository;
//customerQuery, a string representing the full search query of the customer.
//Output:
//Return a list of a list of strings, where each list represents the product suggestions made by the system as the customer types each character of the customerQuery.
// Assume the customer types characters in order without deleting/removing any characters.
//
//Example:
//Input:
//numProducts = 5
//repository = ["mobile", "mouse", "moneypot", "monitor", "mousepad"]
//customerQuery = "mouse"
//
//Output:
//[["mobile", "moneypot", "monitor"],
//["mouse", "mousepad"],
//["mouse", "mousepad"],
//["mouse", "mousepad"]]
//
//Explanation:
//The chain of words that will generate in the search box will be mo, mou, mous and mouse, and each
//line from output shows the suggestions of "mo", "mou", "mous" and "mouse", respectively in each line.
//For the suggestions that are generated for "mo", the matches that will be generated are:
//["mobile", "mouse", "moneypot", "monitor", "mousepad"]. Alphabetically, they will be reordered to
//["mobile", "moneypot", "monitor", "mouse", "mousepad"]. Thus, the suggestions are ["mobile", "moneypot", "monitor"]
//
// related : https://leetcode.com/problems/design-search-autocomplete-system/
package z_weekly_contest.oa;


import java.util.ArrayList;
import java.util.List;


/*
 some optimizations:(todo）
 - limit children size to 3 when building the string._DataStructure_Trie_PrefixTree tree
 - remember string._DataStructure_Trie_PrefixTree node position instead of search from root each time
 - remember string._DataStructure_Trie_PrefixTree node search result to avoid recalculation (dp idea)
*/
public class ProductSuggestion {

    class Node {
        String word = null;
        Node[] children = new Node[26];
    }
    class Trie {

        Node root;
        public Trie() {root = new Node();}

        public void insert(String word) {
            Node cur = root;
            for(char c: word.toCharArray()) {
                if(cur.children[c-'a'] == null) cur.children[c-'a'] = new Node();
                cur = cur.children[c-'a'];
            }
            cur.word = word;
        }

        public List<String> findByPrefix(String prefix) {
            Node cur = root;
            for(char c: prefix.toCharArray()) cur = cur.children[c-'a'];
            List<String> ret = new ArrayList<>();
            dfs(ret, cur);
            return ret;
        }

        private void dfs(List<String> ret, Node cur) {
            if(cur.word!=null) ret.add(cur.word);
            for(Node child: cur.children) {
                if(child!=null) dfs(ret, child);
            }
        }
    }

    List<List<String>> suggestions(int n, String[] repository, String query) {
        Trie trie = new Trie();
        for (String s: repository) trie.insert(s);

        List<List<String>> ret = new ArrayList<>();
        StringBuilder prefix = new StringBuilder();
        for(int i=0; i<query.length(); i++) {
            prefix.append(query.charAt(i));
            if (i==0) continue;

            List<String> words = trie.findByPrefix(prefix.toString());
            if (words.size()>3) words=words.subList(0,3);
            ret.add(words);
        }
        return ret;
    }


    public static void main(String[] as) {
        int numProducts = 5;
        String[] repository = {"mobile", "mouse", "moneypot", "monitor", "mousepad"};
        String customerQuery = "mouse";
        System.out.println(new ProductSuggestion().suggestions(numProducts, repository, customerQuery));
    }
}


/*
private TrieNode root;
public List<String> productSuggestions(String[] repo, String query) {
    root = new TrieNode();

    for (String str : repo) {
        add(str);
    }
    int len = query.length();
    // mouse
    for (int i = len - 3; i <= len; i++) {
        search(query.substring(0, i));
    }

    return null;
}

private void add(String s) {
    TrieNode cur = root;
    for (char c : s.toCharArray()) {
        TrieNode next = cur.children.get(c);
        if (next == null) {
            next = new TrieNode();
            cur.children.put(c, next);
        }
        next.queue.offer(s);
        if (next.queue.size() > 3) {
            next.queue.poll();
        }
        cur = next;

    }
}

private List<String> search(String input) {
    List<String> res = new ArrayList<>();
    TrieNode p = root;
    for (char c : input.toCharArray()) {
        TrieNode child = p.children.get(c);
        if (child == null) { // if not found, return an empty
            return new ArrayList<>();
        }
        p = child;
    }
    PriorityQueue<String> pq = new PriorityQueue<>(p.queue);
    int cnt = 0;
    while (!pq.isEmpty() && cnt < 3) {
        res.add(0, pq.poll());
    }
    System.out.println(res.toString());
    return res;

}

public class TrieNode {
    Map<Character, TrieNode> children;
    Queue<String> queue;
    public TrieNode() {
        this.children = new HashMap<Character, TrieNode>();
        this.queue = new PriorityQueue<>((a, b) -> b.toLowerCase().compareTo(a.toLowerCase()));
    }
}

*/