package z_weekly_contest;

import java.util.*;

public class Solution {
    public int maxCandies(int[] status, int[] candies, int[][] keys, int[][] containedBoxes, int[] initialBoxes) {
        int ret = 0;
        int n = status.length;
        boolean[] used = new boolean[n];
        Set<Integer> boxset = new HashSet<>();
        Set<Integer> keyset = new HashSet<>();
        Queue<Integer> q = new LinkedList<>();

        for(int x: initialBoxes) {
            if(status[x]==1) {
                q.add(x);
            } else {
                boxset.add(x);
            }
        }
        while(!q.isEmpty()) {
            int cur = q.remove();
            if(used[cur]) continue;
            used[cur] = true;

            ret+=candies[cur];                  // get candies

            for(int x: containedBoxes[cur]) {   // get boxes
                if(status[x]==1) {
                    q.add(x);
                } else if (keyset.contains(x)) {
                    status[x] = 1;
                    q.add(x);
                } else {
                    boxset.add(x);
                }
            }


            for(int x: keys[cur]) {             // get keys
                if(boxset.contains(x)) {
                    status[x]=1;
                    q.add(x);
                } else {
                    keyset.add(x);
                }
            }
        }
        return ret;
    }


    public static void main(String[] a) {
        int[] status =          {1,0,0,0};
        int[] candies=          {1,2,3,4};
        int[][] keys =          {{1,2},{3},{},{}};
        int[][] containedBoxes= {{2},{3},{1},{}};
        int[] initialBoxes =    {0};
        System.out.println(new Solution().maxCandies(status, candies, keys, containedBoxes, initialBoxes));
    }
}
