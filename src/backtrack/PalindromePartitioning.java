/*
Given a string s, partition s such that every substring of the partition is a palindrome.

Return all possible palindrome partitioning of s.

Example:

Input: "aab"
Output:
[
  ["aa","b"],
  ["a","a","b"]
]
*/
package backtrack;

import java.util.*;

/*
    - backtrack(better!) or dfs+memo(same time complexity as backtrack)
    - use dp for quick palindrome check
 */
public class PalindromePartitioning {

    // backtrack + dp(preprocess for quick palindrome check)
    List<List<String>> ret;
    boolean[][] dp;
    int n;
    String s;
    public List<List<String>> partition_v2(String s) {
        this.s = s;
        n = s.length();
        dp = new boolean[n][n];
        ret = new ArrayList<>();
        for(int len=1;len<=n;len++){
            for(int i=0,j=i+len-1; j<n; i++,j++) {
                dp[i][j] = s.charAt(i)==s.charAt(j) && (len<=2 || dp[i+1][j-1]);
//                if(len==1) dp[i][j]=true;
//                else if(len==2) dp[i][j]=s.charAt(i)==s.charAt(j);
//                else dp[i][j]=(s.charAt(i)==s.charAt(j)) && dp[i+1][j-1];
            }
        }
        helper(new ArrayList<>(), 0);
        return ret;
    }
    void helper(List<String> path, int pos) {
        if(pos>=n) {
            ret.add(new ArrayList<>(path));
            return;
        }
        for(int i=pos;i<n;i++) {
            if(dp[pos][i]) {
                path.add(s.substring(pos, i+1));
                helper(path, i+1);
                path.remove(path.size()-1);
            }
        }
    }

    // dfs with memo + dp(preprocess for quick palindrome check)
//    Map<Integer, List<List<String>>> memo;
//    boolean[][] dp;
//    String s;
//    int n;
//    public List<List<String>> partition_v1(String s) {
//        this.s = s;
//        n = s.length();
//        memo = new HashMap<>();
//        dp = new boolean[n][n];
//        for(int len=1;len<=n;len++){
//            for(int i=0,j=i+len-1; j<n; i++,j++) {
//                if(len==1) dp[i][j]=true;
//                else if(len==2) dp[i][j]=s.charAt(i)==s.charAt(j);
//                else dp[i][j]=(s.charAt(i)==s.charAt(j)) && dp[i+1][j-1];
//            }
//        }
//        return helper(0);
//    }
//
//    List<List<String>> helper(int pos) {
//        if(memo.containsKey(pos)) return memo.get(pos);
//        List<List<String>> ret = new ArrayList<>();
//        if (pos>=n) {
//            List<String> row = new ArrayList<>();
//            ret.add(row);
//            return ret;
//        }
//        for(int i=pos;i<n;i++) {
//            if(dp[pos][i]) {
//                List<List<String>> rows = helper(i+1);
//                for(List<String> row: rows) {
//                    List<String> nrow = new ArrayList<>();
//                    nrow.add(s.substring(pos, i+1));
//                    nrow.addAll(row);
//                    ret.add(nrow);
//                }
//            }
//        }
//        memo.put(pos, ret);
//        return ret;
//    }

    public static void main(String[] a){
//        System.out.println(new PalindromePartitioning().partition("abbab"));
    }
}