/*
Given an equation, represented by words on left side and the result on right side.

You need to check if the equation is solvable under the following rules:

Each character is decoded as one map (0 - 9).
Every pair of different characters they must map to different digits.
Each words[i] and result are decoded as one number without leading zeros.
Sum of numbers on left side (words) will equal to the number on right side (result).
Return True if the equation is solvable otherwise return False.



Example 1:

Input: words = ["SEND","MORE"], result = "MONEY"
Output: true
Explanation: Map 'S'-> 9, 'E'->5, 'N'->6, 'D'->7, 'M'->1, 'O'->0, 'R'->8, 'Y'->'2'
Such that: "SEND" + "MORE" = "MONEY" ,  9567 + 1085 = 10652
Example 2:

Input: words = ["SIX","SEVEN","SEVEN"], result = "TWENTY"
Output: true
Explanation: Map 'S'-> 6, 'I'->5, 'X'->0, 'E'->8, 'V'->7, 'N'->2, 'T'->1, 'W'->'3', 'Y'->4
Such that: "SIX" + "SEVEN" + "SEVEN" = "TWENTY" ,  650 + 68782 + 68782 = 138214
Example 3:

Input: words = ["THIS","IS","TOO"], result = "FUNNY"
Output: true
Example 4:

Input: words = ["LEET","CODE"], result = "POINT"
Output: false


Constraints:

2 <= words.length <= 5
1 <= words[i].length, results.length <= 7
words[i], result contains only upper case English letters.
Number of different characters used on the expression is at most 10.
*/
package backtrack;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

// #backtracking
public class VerbalArithmeticPuzzle {
    // To avoid TLE during contest, we need to avoid calling toCharArray() multiple times. So we convert to char[] here.
    char[][] words;
    char[] result;
    List<Character> chl = new ArrayList<>();
    boolean[] used = new boolean[10];
    int[] map = new int[128];
    public boolean isSolvable(String[] _words, String _result) {
        words = new char[_words.length][];
        for (int i=0;i<_words.length;i++) words[i]=_words[i].toCharArray();
        result = _result.toCharArray();

        Set<Character> chs = new HashSet<>();
        for(char[] w: words) {
            for(char c: w) {
                chs.add(c);
            }
        }
        for(char c: result) {
            chs.add(c);
        }
        for(char c: chs) chl.add(c);
        return dfs(0); // As letters is lessOrEqual than digits, we backtrack on letters instead of 10 digits!!
    }

    boolean dfs(int idx) {
        if(idx==chl.size()) return ok(map);

        char c = chl.get(idx);
        for(int d=0;d<=9;d++) {
            if(used[d]) continue;
            map[c] = d;
            used[d] = true;
            if(dfs(idx+1)) return true; // early pruning
            used[d] = false;
        }
        return false;
    }


    boolean ok(int[] digit) {
        int right = 0;
        for(char c: result) {
            right *= 10;
            right += digit[c];
        }
        int left = 0 ;
        for(char[] w: words) {
            int num = 0;
            for(char c: w) {
                num *= 10;
                num += digit[c];
            }
            left += num;
        }
        return left == right;
    }
}

/*
class Solution {

	public boolean isSolvable(String[] words, String result) {
		int i = 0;
		int[] map = new int[26];
		for (int j = 0; j < 26; j++) {
			map[j] = -1;
		}
		for (String word : words) {
			for (char c : word.toCharArray()) {
				if (map[c - 'A'] == -1) {
					map[c - 'A'] = i++;
				}
			}
		}
		for (char c : result.toCharArray()) {
			if (map[c - 'A'] == -1) {
				map[c - 'A'] = i++;
			}
		}
		return isSolvable(words, result, new int[i], 0, map, new boolean[10]);
	}

	private boolean isSolvable(String[] words, String result, int[] value, int index, int[] map, boolean[] set) {
		if (index == value.length) {
			int[] sum = new int[words.length];
			int c = 0, S = 0;
			for (int j = 0; j < sum.length; j++) {
				for (int i = 0; i < words[j].length(); i++) {
					sum[j] = sum[j] * 10 + value[map[words[j].charAt(i) - 'A']];
				}
				S += sum[j];
			}
			for (int i = 0; i < result.length(); i++) {
				c = c * 10 + value[map[result.charAt(i) - 'A']];
			}
			return S == c;
		} else {
			for (int i = 0; i <= 9; i++) {
				if (!set[i]) {
					set[i] = true;
					value[index] = i;
					if (isSolvable(words, result, value, index + 1, map, set)) {
						return true;
					}
					set[i] = false;
				}
			}
			return false;
		}
	}
}



*/