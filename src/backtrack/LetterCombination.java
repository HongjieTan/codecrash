package backtrack;

import java.util.*;

public class LetterCombination {


    // more concise
    public List<String> letterCombinations(String digits) {
        if (digits==null || digits.isEmpty()) return new ArrayList<>();
        String[] map = new String[]{"", "", "abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz"};
        List<String> res = new ArrayList<>();
        backtrack(res, new StringBuffer(), digits, 0, map);
        return res;
    }


    private void backtrack(List<String> res, StringBuffer path, String digits, int index, String[] map) {
        if (path.length() == digits.length()) {
            res.add(path.toString());
        } else {
            for(Character c: map[digits.charAt(index) - '0'].toCharArray()) {
                path.append(c);
                backtrack(res, path, digits, index+1, map);
                path.deleteCharAt(path.length()-1);
            }
        }
    }



    public List<String> letterCombinations_v1(String digits) {
        Map<Character, List<Character>> digit2Chars = new HashMap<>();
        digit2Chars.put('2', Arrays.asList('a', 'b', 'c'));
        digit2Chars.put('3', Arrays.asList('d', 'e', 'f'));
        digit2Chars.put('4', Arrays.asList('g', 'h', 'i'));
        digit2Chars.put('5', Arrays.asList('j', 'k', 'l'));
        digit2Chars.put('6', Arrays.asList('m', 'n', 'o'));
        digit2Chars.put('7', Arrays.asList('p', 'q', 'r', 's'));
        digit2Chars.put('8', Arrays.asList('t', 'u', 'v'));
        digit2Chars.put('9', Arrays.asList('w', 'x', 'y', 'z'));
        List<String> res = new ArrayList<>();
        backtrack_v1(res, new StringBuffer(), digits, 0, digit2Chars);
        return res;
    }


    private void backtrack_v1(List<String> res, StringBuffer path, String digits, int i, Map<Character, List<Character>> digit2Chars) {
        if (path.length() == digits.length()) {
            res.add(path.toString());
            return;
        }
        for(Character c: digit2Chars.get(digits.charAt(i))) {
            path.append(c);
            backtrack_v1(res, path, digits,i+1, digit2Chars);
            path.deleteCharAt(path.length()-1);
        }
    }

    public static void main(String[] args) {
        List<String> res = new LetterCombination().letterCombinations("23");
        System.out.println("asdf");
    }
}
