package backtrack;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by thj on 2018/10/31.
 */
public class CombinationSum2 {

    public List<List<Integer>> combinationSum2(int[] candidates, int target) {
        List<List<Integer>> res = new ArrayList<>();
        Arrays.sort(candidates);
        backtack(res, new ArrayList<>(), candidates, target, 0);
        return res;
    }

    private void backtack(List<List<Integer>> res, List<Integer> path, int[] candidates, int target, int startIndex) {
        if (target == 0) {
            res.add(new ArrayList<>(path));
        } else if (target > 0) {
            for (int i = startIndex; i < candidates.length; i++) {
                if (i>startIndex && candidates[i] == candidates[i-1]) continue; // skip duplicates!!!  i>startIndex!!!(not i>0!!!)
                path.add(candidates[i]);
                backtack(res, path, candidates, target-candidates[i], i+1);
                path.remove(path.size()-1);
            }
        }
    }
}
