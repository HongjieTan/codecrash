package backtrack;

import java.util.ArrayList;
import java.util.List;

// dfs+backtrack...
public class CartesianProduct {
    public List<List<Integer>> getSet(int[][] setList) {
        List<List<Integer>> ans = new ArrayList<>();
        dfs(ans, new ArrayList<>(), 0,setList);
        return ans;
    }

    private void dfs(List<List<Integer>> ans, List<Integer> path, int curSetIndex, int[][] setList) {
        if (path.size()==setList.length) {
            ans.add(new ArrayList<>(path));
            return;
        }

        for(int i=0; i<setList[curSetIndex].length; i++) {
            path.add(setList[curSetIndex][i]);
            dfs(ans, path, curSetIndex+1, setList);
            path.remove(path.size()-1); // backtrack...
        }
    }

    public static void main(String[] args) {
        int[][] setList = new int[][] {
                new int[]{1,2,3},
                new int[]{4,5},
                new int[]{6}
        };
        List<List<Integer>> res = new CartesianProduct().getSet(setList);
        for(List<Integer> set: res) {
            for (int x: set) System.out.printf(x+",");
            System.out.println();
        }
    }
}

/*
We use a two-dimensional array setList[][] to represent a collection array, and each element in setList[i] is an integer and is not the same. Find the cartesian product of setList[0],setList[1],...,setList[setList.length - 1].
In general, the Cartesian product of the collection A and the set B is A×B = {(x,y)|x∈A∧y∈B}。


就[1,2,3][4,5][6]. output[1,4,6][1,5,6][2,4,6]...这样
*/


/*
public class Solution {

    int n, m;

    public void dfs(List<List<Integer>> ans,int[][] setList, int pos, List<Integer> tmp) {
        if(pos == setList.length) {
            ans.add(new ArrayList<Integer>(tmp));
            return;
        }
        for(int i = 0; i < setList[pos].length; i++) {
            tmp.add(setList[pos][i]);
            dfs(ans, setList, pos + 1, tmp);
            tmp.remove(tmp.size() - 1);
        }

    }

    public List<List<Integer>> getSet(int[][] setList) {
        // Write your code here
        List<List<Integer>> ans = new ArrayList<List<Integer>>();
        List<Integer> tmp = new ArrayList<Integer>();

        dfs(ans, setList, 0, tmp);
        return ans;
    }
}
*/