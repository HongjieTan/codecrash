/*
Design an Iterator class, which has:

A constructor that takes a string characters of sorted distinct lowercase English letters and a number combinationLength as arguments.
A function next() that returns the next combination of length combinationLength in lexicographical order.
A function hasNext() that returns True if and only if there exists a next combination.


Example:

CombinationIterator iterator = new CombinationIterator("abc", 2); // creates the iterator.

iterator.next(); // returns "ab"
iterator.hasNext(); // returns true
iterator.next(); // returns "ac"
iterator.hasNext(); // returns true
iterator.next(); // returns "bc"
iterator.hasNext(); // returns false


Constraints:

1 <= combinationLength <= characters.length <= 15
There will be at most 10^4 function calls per test.
It's guaranteed that all calls of the function next are valid.
*/

package backtrack;

import java.util.ArrayList;
import java.util.List;

public class IteratorOfCombination {
}
class CombinationIterator {

    List<String> combinations;
    int idx;
    int combinationLength;
    char[] chs;

    public CombinationIterator(String characters, int combinationLength) {
        this.combinationLength = combinationLength;
        this.chs = characters.toCharArray();
        this.combinations = new ArrayList<>();
        this.idx = 0;
        dfs(new StringBuilder(), 0);
    }

    void dfs(StringBuilder path, int index) {
        if(path.length()>=combinationLength) {
            combinations.add(path.toString());
            return;
        }

        for(int i=index;i<chs.length;i++) {
            path.append(chs[i]);
            dfs(path, index+1);
            path.deleteCharAt(path.length()-1);
        }
    }

    public String next() {
        return combinations.get(idx++);
    }

    public boolean hasNext() {
        return idx<combinations.size();
    }
}



/*
by uwi:
class CombinationIterator {
    char[] u;
    int K;
    int ind;
    int[][] a;

    public CombinationIterator(String characters, int combinationLength) {
        u = characters.toCharArray();
        K = combinationLength;

        int n = u.length;
        a = new int[1<<n][];
        int p = 0;
        for(int i = 0;i < 1<<n;i++){
            if(Integer.bitCount(i) == K){
                int[] row = new int[K];
                int t = i;
                for(int j = 0;j < K;j++){
                    row[j] = Integer.numberOfTrailingZeros(t);
                    t &= t-1;
                }
                a[p++] = row;
            }
        }
        a = Arrays.copyOf(a, p);
        Arrays.sort(a, new Comparator<int[]>() {
            public int compare(int[] a, int[] b) {
                for(int i = 0;i < a.length;i++){
                    if(a[i] != b[i]){
                        return a[i] - b[i];
                    }
                }
                return 0;
            }
        });

        ind = -1;

    }

    public String next() {
        ind++;
        char[] ret = new char[K];
        int p = 0;
        for(int o : a[ind]){
            ret[p++] = u[o];
        }
        return new String(ret);
    }

    public boolean hasNext() {
        return ind+1 < a.length;
    }
}

*/