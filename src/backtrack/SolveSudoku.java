package backtrack;

import java.util.HashSet;
import java.util.Set;


/**
 *
 *  1. hashtable + backtrack + dfs: time O(9^emptySlots)
 *  2. improved version:  TODO
 *      - from prime minister of singapore: https://leetcode.com/problems/sudoku-solver/discuss/15796/Singapore-prime-minister-Lee-Hsien-Loong's-Sudoku-Solver-code-runs-in-1ms
 *
 */
public class SolveSudoku {


    // dfs(some kind of dfs...) + backtrack!
    public void solveSudoku(char[][] board) {
        Set<String> seen = new HashSet<>();

        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[0].length; j++) {
                char c = board[i][j];
                if (c!='.') {
                    String valueInRow = c+"#row#"+i;
                    String valueInCol = c+"#col#"+j;
                    String valueInBlock = c+"#brow#"+i/3+"#bcol#"+j/3;
                    seen.add(valueInRow);
                    seen.add(valueInCol);
                    seen.add(valueInBlock);
                }
            }
        }

        fill(board, 0, seen);
    }


    private boolean fill(char[][] board, int index, Set<String> seen) {
        if (index == 81) return true;
        int row = index/9;
        int col = index%9;

        if (board[row][col]!='.') {
            return fill(board, index+1, seen);
        } else {
            for (char c='1';c<='9';c++) {
                String valueInRow = c+"#row#"+row;
                String valueInCol = c+"#col#"+col;
                String valueInBlock = c+"#brow#"+row/3+"#bcol#"+col/3;
                if (!seen.contains(valueInRow) && !seen.contains(valueInCol) && !seen.contains(valueInBlock)) {
                    board[row][col] = c;
                    seen.add(valueInRow);
                    seen.add(valueInCol);
                    seen.add(valueInBlock);
                    if (fill(board, index+1, seen)) return true;
                    board[row][col] = '.'; // backtrack!!
                    seen.remove(valueInRow);
                    seen.remove(valueInCol);
                    seen.remove(valueInBlock);
                }
            }
            return false;
        }
    }


}
