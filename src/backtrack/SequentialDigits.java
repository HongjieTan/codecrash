/*
An integer has sequential digits if and only if each digit in the number is one more than the previous digit.

Return a sorted list of all the integers in the range [low, high] inclusive that have sequential digits.



Example 1:

Input: low = 100, high = 300
Output: [123,234]
Example 2:

Input: low = 1000, high = 13000
Output: [1234,2345,3456,4567,5678,6789,12345]


Constraints:

10 <= low <= high <= 10^9
*/
package backtrack;


import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;


// #backtrack #bfs
public class SequentialDigits {


    // bfs is easy, no need backtrack...
    public List<Integer> sequentialDigits(int low, int high) {
        List<Integer> ret = new ArrayList<>();
        Queue<Integer> q = new LinkedList<>();
        for(int i=1;i<=9;i++) q.add(i);
        while(!q.isEmpty()) {
            int cur = q.remove();
            if(cur>high) continue;
            if(cur>=low) ret.add(cur);
            if(cur%10<=8) q.add(cur*10+cur%10+1);
        }
        return ret;
     }

    public static void main(String[] as) {
        System.out.println(new SequentialDigits().sequentialDigits(1000,13000));
    }
}
