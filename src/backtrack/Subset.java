package backtrack;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by thj on 28/05/2018.
 */
public class Subset {

    // backtrack: O(2^n)/O(2^n) 3 ms
    public List<List<Integer>> subsets(int[] nums) {
        List<List<Integer>> res = new ArrayList<>();
        generate(nums, 0, new ArrayList<>(), res);
        return res;
    }

    private void generate(int[] nums, int start, List<Integer> path, List<List<Integer>> res) {
        res.add(new ArrayList<>(path));
        for (int i=start; i<nums.length; i++) {
            path.add(nums[i]);
            generate(nums, i+1, path, res);
            path.remove(path.size()-1);
        }
    }


    // iterative: O(2^n)
    public List<List<Integer>> subsets_v2(int[] nums) {
        List<List<Integer>> res = new ArrayList<>();
        res.add(new ArrayList<>());

        for (int i=0; i<nums.length; i++) {
            int curResSize = res.size();
            for (int j=0; j<curResSize; j++) {
                List<Integer> curSub = res.get(j);
                res.add(new ArrayList<>(curSub));
                curSub.add(nums[i]);
            }
        }
        return res;
    }


    // bit manipulation // TODO: 28/05/2018
//    public List<List<Integer>> subsets_v3(int[] nums) {
//    }


}
