package backtrack;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by thj on 2018/10/30.
 */
public class CombinationSum {
    public List<List<Integer>> combinationSum(int[] candidates, int target) {
        List<List<Integer>> res = new ArrayList<>();
        backtrack(res, new ArrayList<>(), candidates, 0, target);
        return res;
    }

    private void backtrack(List<List<Integer>> res, List<Integer> path, int[] candidates, int startIdx, int target) {
        if (target==0) {
            res.add(new ArrayList<>(path));
        } else if (target>0) {
            for (int i = startIdx; i < candidates.length; i++) {
                path.add(candidates[i]);
                backtrack(res, path, candidates, i, target-candidates[i]);
                path.remove(path.size()-1);
            }
        }
    }


    public static void main(String[] args) {
//        System.out.println("asd");
//        [[2,2,3],[7]]
        List<List<Integer>> res = new CombinationSum().combinationSum(new int[]{2,3,6, 7}, 7);
        System.out.println("asdf");

    }




}
