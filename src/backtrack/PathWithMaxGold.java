//In a gold mine grid of size m * n, each cell in this mine has an integer representing the amount of gold in that cell, 0 if it is empty.
//
// Return the maximum amount of gold you can collect under the conditions:
//
//
// Every time you are located in a cell you will collect all the gold in that cell.
// From your position you can walk one step to the left, right, up or down.
// You can't visit the same cell more than once.
// Never visit a cell with 0 gold.
// You can start and stop collecting gold from any position in the grid that has some gold.
//
//
//
// Example 1:
//
//
//Input: grid = [[0,6,0],[5,8,7],[0,9,0]]
//Output: 24
//Explanation:
//[[0,6,0],
// [5,8,7],
// [0,9,0]]
//Path to get the maximum gold, 9 -> 8 -> 7.
//
//
// Example 2:
//
//
//Input: grid = [[1,0,7],[2,0,6],[3,4,5],[0,3,0],[9,0,20]]
//Output: 28
//Explanation:
//[[1,0,7],
// [2,0,6],
// [3,4,5],
// [0,3,0],
// [9,0,20]]
//Path to get the maximum gold, 1 -> 2 -> 3 -> 4 -> 5 -> 6 -> 7.
//
//
//
// Constraints:
//
//
// 1 <= grid.length, grid[i].length <= 15
// 0 <= grid[i][j] <= 100
// There are at most 25 cells containing gold.
package backtrack;


// dfs + backtrack
public class PathWithMaxGold {
    // backtrack all paths （use memo to speed up? not possible...the visited status is different..）
    int m,n;
    int[][] g;
    int[][] dirs = {{1,0},{-1,0},{0,1},{0,-1}};
    public int getMaximumGold(int[][] grid) {
        int res = 0;
        m=grid.length;
        n=grid[0].length;
        g=grid;
        for(int i=0;i<m;i++) {
            for(int j=0;j<n;j++) {
                if(grid[i][j]>0)
                    res = Math.max(res, dfs(i, j));
            }
        }
        return res;
    }

    int dfs(int x, int y) {
        int tmp = g[x][y];
        g[x][y] = 0;
        int max = 0;
        for(int[] dir: dirs) {
            int nx=x+dir[0], ny=y+dir[1];
            if(nx>=0 && nx<m && ny>=0 && ny<n && g[nx][ny]!=0 ) {
                max = Math.max(max, dfs(nx,ny));
            }
        }
        g[x][y] = tmp; //backtrack status
        return g[x][y]+tmp;
    }

}


/*
by top:
Observation
Since the count of cells is at maximum 15x15 and we know at maximum there can be 25 cells with gold,
we can perform a simple DFS starting with each cell with gold and find the maximum possible gold considering each step as a new start.
We can then return the maximum gold that can we aquired of all starting points.

Solution

static vector<int> directions={0,1,0,-1,0};
class Solution {
public:
    int dfs(vector<vector<int>> &grid,int i,int j)
    {
        int x,y,temp=grid[i][j],result=0;
        grid[i][j]=0;
        for(int d=0;d<4;d++)                    //Try all 4 possible directions
        {
            x=i+directions[d],y=j+directions[d+1];
            if(x>=0&&y>=0&&x<grid.size()&&y<grid[x].size()&&grid[x][y]!=0)
                result=max(result,grid[x][y]+dfs(grid,x,y));  //Set grid[i][j] to 0 to mark as visited before next dfs and reset the values after the dfs ends
        }
        grid[i][j]=temp;
        return result;
    }
    int getMaximumGold(vector<vector<int>>& grid)
    {
        int result=0;
        for(int i=0;i<grid.size();i++)
            for(int j=0;j<grid[i].size();j++)
                if(grid[i][j]>0)
                    result=max(grid[i][j]+dfs(grid,i,j),result);   //Set grid[i][j] to 0 to mark this cell as visited and reset after the dfs ends.
        return result;
    }
};
Complexity
n being the number of elements in matrix.
Space: O(n) for the algorithm. O(1) for this question. Since the depth of recursion can go upto max 25 cells
Time: O(4^n) for the algorithm , O(n) for this question. Since the question states that there are at most 25 cells with gold. (Although I would argue that complexity is O(3^n) as each cell after the first would just have 3 choices at max.


by uwi:
class Solution {
    public int getMaximumGold(int[][] g) {
        int n = g.length, m = g[0].length;
        int[][] poss = new int[25][];
        int[][] ids = new int[n][m];
        int p = 0;
        for(int i = 0;i < n;i++){
            Arrays.fill(ids[i], -1);
            for(int j = 0;j < m;j++){
                if(g[i][j] > 0){
                    ids[i][j] = p;
                    poss[p++] = new int[]{i, j};
                }
            }
        }
        for(int i = 0;i < p;i++){
            dfs(i, 0, 0, poss, ids, g);
        }
        return ans;
    }

    int[] dr = { 1, 0, -1, 0 };
    int[] dc = { 0, 1, 0, -1 };

    void dfs(int cur, int ved, int has, int[][] poss, int[][] ids, int[][] g)
    {
        ved |= 1<<cur;
        int r = poss[cur][0];
        int c = poss[cur][1];
        has += g[r][c];
        ans = Math.max(ans, has);
        int n = g.length, m = g[0].length;
        for(int k = 0;k < 4;k++){
            int nr = r + dr[k], nc = c + dc[k];
            if(nr >= 0 && nr < n && nc >= 0 && nc < m){
                int ne = ids[nr][nc];
                if(ne == -1)continue;
                if(ved<<~ne<0)continue;
                dfs(ne, ved, has, poss, ids, g);
            }
        }
    }

    int ans = 0;
}
*/