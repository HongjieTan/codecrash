package backtrack;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by thj on 2018/10/31.
 */
public class CombinationSum3 {

    public List<List<Integer>> combinationSum3(int k, int n) {
        List<List<Integer>> res = new ArrayList<>();
        backtrack(res, new ArrayList<>(), k, n, 1);
        return res;
    }


    private void backtrack(List<List<Integer>> res, List<Integer> path, int k, int n, int startIndex) {
        if (k==0 && n==0) {
            res.add(new ArrayList<>(path));
        } else if (k>0 && n>0) {
            for (int i = startIndex; i <= 9; i++) {
                path.add(i);
                backtrack(res, path, k-1, n-i, i+1);
                path.remove(path.size()-1);
            }
        }
    }
}
