// Write an efficient algorithm that searches for a value in an m x n matrix.
// This matrix has the following properties:
//
//
// Integers in each row are sorted in ascending from left to right.
// Integers in each column are sorted in ascending from top to bottom.
//
//
// Example:
//
// Consider the following matrix:
//
//
//[
//  [1,   4,  7, 11, 15],
//  [2,   5,  8, 12, 19],
//  [3,   6,  9, 16, 22],
//  [10, 13, 14, 17, 24],
//  [18, 21, 23, 26, 30]
//]
//
//
// Given target = 5, return true.
//
// Given target = 20, return false.

package twopointers._Topic_MathRelated;

/**
 * Created by thj on 2018/7/25.
 */
public class Search2DMatrix2 {
    // O(m+n)
    public boolean searchMatrix(int[][] matrix, int target) {
        if (matrix.length==0) return false;
        int m=matrix.length, n=matrix[0].length;
        int x=0, y=n-1;
        while(x<m && y>=0) {
            if(matrix[x][y]==target) {
                return true;
            } else if(matrix[x][y]>target){
                y--;
            } else {
                x++;
            }
        }
        return false;
    }

    public static void main(String[] a) {
        int[][] m = {
                {1,   4,  7, 11, 15},
                {2,   5,  8, 12, 19},
                {3,   6,  9, 16, 22},
                {10, 13, 14, 17, 24},
                {18, 21, 23, 26, 30}
        };
        System.out.println(new Search2DMatrix2().searchMatrix(m, 5));
        System.out.println(new Search2DMatrix2().searchMatrix(m, 20));
    }

    // not good! but run faster than the o(m+n) version !!
//    public int searchMatrix(int[][] matrix, int target) {
//        // write your code here
//
//        if (matrix.length==0) return 0;
//        if (matrix[0].length==0) return 0;
//
//        int m=matrix.length, n=matrix[0].length;
//        return countMatrix(matrix, 0, m-1, 0, n-1, target);
//
//    }
//
//    private int countMatrix(int[][] matrix, int rowL, int rowR, int colL, int colR, int target) {
//
//        if (rowL>rowR || colL>colR) return 0;
//
//        int rowPos=rowL;
//        int l=rowL, r=rowR;
//        while (l<=r) {
//            int mid=l+(r-l)/2;
//
//            if (matrix[mid][colL]==target) return 1+countMatrix(matrix, rowL, mid-1, colL+1, colR, target);
//            if (mid==rowL && matrix[mid][colL]>target) return 0;
//            if (mid==rowR && matrix[mid][colL]<target) {rowPos = mid;break;}
//            if (matrix[mid][colL]<target && matrix[mid+1][colL]>target) {rowPos=mid; break;}
//
//            if (matrix[mid][colL]>target) r=mid-1;
//            else l=mid+1;
//        }
//
//        l=colL; r=colR;
//        int colPos=colL;
//        while (l<=r) {
//            int mid=l+(r-l)/2;
//
//            if (matrix[rowPos][mid]==target)
//                return 1+countMatrix(matrix, rowL, rowPos-1, mid+1, colR, target) + countMatrix(matrix, rowPos+1, rowR, colL, mid-1, target);
//            if (mid==colL && matrix[rowPos][mid]>target) {colPos=colL; break;}
//            if (mid==colR && matrix[rowPos][mid]<target) {colPos=colR; break;}
//            if (matrix[rowPos][mid]<target && matrix[rowPos][mid+1]>target) {colPos=mid;break;}
//
//            if (matrix[rowPos][mid]>target) r=mid-1;
//            else l=mid+1;
//        }
//
//        return countMatrix(matrix, rowL, rowPos-1, colPos, colR, target)
//                + countMatrix(matrix, rowPos+1, rowR, colL, colPos, target);
//    }
}
