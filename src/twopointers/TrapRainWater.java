package twopointers;

/**
 * Created by thj on 29/05/2018.
 *
 *
 * brute force: O(n^2) must understand!!
 * dp : O(n) /O(n)  good!!
 * stack: O(n) /O(n)
 * 2 pointers: O(n) /O(1) good!!
 *

 */


public class TrapRainWater {

    // dp version
//    public int trap(int[] height) {
//
//        if(height ==null || height.length == 0) return 0;
//
//        int[] leftMax = new int[height.length];
//        int[] rightMax = new int[height.length];
//
//        leftMax[0] = height[0];
//        for(int i=1;i<height.length;i++) {
//            leftMax[i] = Math.max(leftMax[i-1], height[i]);
//        }
//        rightMax[height.length-1] = height[height.length-1];
//        for (int i=height.length-2; i>=0; i--) {
//            rightMax[i] = Math.max(rightMax[i+1], height[i]);
//        }
//
//
//        int ans = 0;
//        for (int i=0; i<height.length; i++) {
//            ans += Math.min(leftMax[i], rightMax[i]) - height[i];
//        }
//
//        return ans;
//    }

    // 2p version
    public int trap(int[] height) {
        if (height==null || height.length ==0) return 0;
        int curLeftMax = height[0], curRightMax = height[height.length-1];
        int l=0, r=height.length-1;
        int ans = 0;
        while(l<r) {
            if (height[l] < height[r]) {
                // Math.min(leftMax[i], rightMax[i]) must be curLeftMax!!!
                ans += curLeftMax-height[l];
                curLeftMax = Math.max(curLeftMax, height[++l]);
            } else {
                // Math.min(leftMax[i], rightMax[i]) must be curRightMax!!!
                ans += curRightMax-height[r];
                curRightMax = Math.max(curRightMax, height[--r]);
            }
        }
        return ans;

    }


}
