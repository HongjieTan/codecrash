package twopointers._Topic_SubStringSearch_SlidingWindow;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 *  brute force: O(n*m)
 *  two pointers: O(n)
 *
 *
 */
public class FindAllAnagrams {


    // use the template !!
    public List<Integer> findAnagrams_x2(String s, String p) {
        List<Integer> ans = new ArrayList<>();
        Map<Character, Integer> map = new HashMap<>();
        for (char c: p.toCharArray()) map.put(c, map.getOrDefault(c, 0)+1);

        int l=0,r=0, counter=0;
        while (r<s.length()) {
            // move right p
            char rc = s.charAt(r);
            if (map.containsKey(rc)) {
               map.put(rc, map.get(rc)-1);
               if (map.get(rc)==0) counter++;
            }
            r++;

            // move left p
            while (counter == map.size()) {
                if (r-l==p.length()) ans.add(l);
                char lc= s.charAt(l);
                if (map.containsKey(lc)) {
                    map.put(lc, map.get(lc)+1);
                    if (map.get(lc)==1) counter--;
                }
                l++;
            }
        }
        return ans;
    }


    // 2p: O(n)
    public List<Integer> findAnagrams_x1(String s, String p) {
        List<Integer> ans = new ArrayList<>();
        Map<Character, Integer> need = new HashMap<>();
        for (char c: p.toCharArray()) need.put(c, need.getOrDefault(c, 0)+1);
        int l=0, r=0;
        while (r<s.length()) {
            // move right pointer
            if (need.containsKey(s.charAt(r)) && need.get(s.charAt(r))>0 ) {
                need.put(s.charAt(r), need.get(s.charAt(r))-1);
                r++;
                if (r-l==p.length()) ans.add(l);
            }
            // move left pointer
            else {
                if (need.containsKey(s.charAt(l))) need.put(s.charAt(l), need.get(s.charAt(l))+1);
                l++;
                if (l>=r) r=l;
            }
        }
        return ans;
    }

    // brute force: O(n*m)
//    public List<Integer> findAnagrams_bruteforce(String s, String p) {
//        List<Integer> ans = new ArrayList<>();
//        Map<Character, Integer> pMap = new HashMap<>();
//        for (char c: p.toCharArray()) pMap.put(c, pMap.getOrDefault(c,0)+1);
//        int idx=0;
//        while (idx+p.length()-1<s.length()) {
//            int start = idx;
//            Map<Character, Integer> tempMap = new HashMap<>(pMap);
//            while (idx<s.length() && tempMap.containsKey(s.charAt(idx)) && tempMap.get(s.charAt(idx))>0) {
//                tempMap.put(s.charAt(idx), tempMap.get(s.charAt(idx))-1);
//                idx++;
//            }
//            if (idx-start==p.length()) {
//                ans.add(start);
//            }
//            idx=start+1;
//        }
//        return ans;
//    }


    public static void main(String[] args) {
        System.out.println(new FindAllAnagrams().findAnagrams_x2("cbaebabacd","abc"));
    }
}

/*
Given a string s and a non-empty string p, find all the start indices of p's anagrams in s.

Strings consists of lowercase English letters only and the length of both strings s and p will not be larger than 20,100.

The order of output does not matter.

Example 1:

Input:
s: "cbaebabacd" p: "abc"

Output:
[0, 6]

Explanation:
The substring with start index = 0 is "cba", which is an anagram of "abc".
The substring with start index = 6 is "bac", which is an anagram of "abc".
Example 2:

Input:
s: "abab" p: "ab"

Output:
[0, 1, 2]

Explanation:
The substring with start index = 0 is "ab", which is an anagram of "ab".
The substring with start index = 1 is "ba", which is an anagram of "ab".
The substring with start index = 2 is "ab", which is an anagram of "ab".
*/