package twopointers._Topic_SubStringSearch_SlidingWindow;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by thj on 05/06/2018.
 *
 * hashtable + two pointers : O(n)， 更准确的说是 O(2n)
 *
 */
public class MinWindowSubstring {

    // use the template!!!
    public String minWindow_x2(String s, String t) {
        Map<Character, Integer> need = new HashMap<>();
        for (char c: t.toCharArray()) need.put(c, need.getOrDefault(c, 0)+1);
        int minLen = Integer.MAX_VALUE;
        String ans = "";
        int l=0,r=0, needCount = need.size();
        while (r<s.length()) {
            // move right pointer to find valid state
            char rc = s.charAt(r);
            if (need.containsKey(rc)) {
                need.put(rc, need.get(rc)-1); // need can be negative
                if (need.get(rc)==0) needCount--;
            }
            r++;

            // move left pointer to make it invalid
            while (needCount == 0) {
                if (r-l<minLen) {  minLen=r-l+1; ans=s.substring(l, r);} // update min
                char lc = s.charAt(l);
                if (need.containsKey(lc)) {
                    need.put(lc, need.get(lc)+1);
                    if (need.get(lc)==1) needCount++;
                }
                l++;
            }
        }
        return ans;
    }

    public static void main(String[] args) {
        System.out.println(new MinWindowSubstring().minWindow_x2( "ADOBECODEBANC", "ABC"));
    }

//    public String minWindow(String s, String t) {
//        Map<Character, Integer> needMap = new HashMap<>(); //!!! notice the idea of needMap, key: char, value: needed count
//        for (int i=0;i<t.length();i++) {
//            needMap.put(t.charAt(i), needMap.getOrDefault(t.charAt(i),0)+1);
//        }
//        int l=0, counter=0, minHead=0, minLen = Integer.MAX_VALUE;
//        for (int r=0; r<s.length(); r++) {
//            if (needMap.containsKey(s.charAt(r))) {
//                if (needMap.get(s.charAt(r))>0) {counter++;} // update counter
//                needMap.put(s.charAt(r), needMap.get(s.charAt(r))-1);
//            }
//
//            while (counter == t.length()) { // if valid(current range contain all t chars), move left pointer to rightmost position !!!!
//                if (r-l+1<minLen) {minLen = r-l+1; minHead = l;} // update min
//                if (needMap.containsKey(s.charAt(l))) {
//                    if (needMap.get(s.charAt(l)) == 0) {counter--;}
//                    needMap.put(s.charAt(l), needMap.get(s.charAt(l))+1);
//                }
//                l++;
//            }
//        }
//        return minLen == Integer.MAX_VALUE?"":s.substring(minHead, minHead+minLen);
//    }
}

/*
    Given a string S and a string T, find the minimum window in S which will contain all the characters in T in complexity O(n).

        Example:

        Input: S = "ADOBECODEBANC", T = "ABC"
        Output: "BANC"
        Note:

        If there is no such window in S that covers all characters in T, return the empty string "".
        If there is such window, you are guaranteed that there will always be only one unique minimum window in S.*/