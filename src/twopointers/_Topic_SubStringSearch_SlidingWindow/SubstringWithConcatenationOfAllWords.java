package twopointers._Topic_SubStringSearch_SlidingWindow;

import java.util.*;

/**
 *  Similar with FindAllAnagrams, just replace Character with Word ! (note: each word in words has same length).
 *  What's more, we need to process for each offset here.
 *
 *  - 2p and matching using map: good, O(n) !!!!!
 *  - 2p and matching using string._DataStructure_Trie_PrefixTree:  why string._DataStructure_Trie_PrefixTree?? complex and no need here!...
 *
 */
public class SubstringWithConcatenationOfAllWords {


    //. O( wordLen * n/wordLen) = O(n) !!
    public List<Integer> findSubstring_v2(String s, String[] words) {
        if (s==null || words==null || s=="" || words.length==0) return new ArrayList<>();

        List<Integer> ans = new ArrayList<>();
        Map<String, Integer> map = new HashMap<>();
        for (String word: words) map.put(word, map.getOrDefault(word, 0)+1);
        int wordLen = words[0].length();
        for (int offset = 0; offset < wordLen; offset++) { // iterate each offset, O(wordLen)
            Map<String, Integer> curMap = new HashMap<>(map);
            int l=offset, r=offset+wordLen-1, counter=0;
            while (r<s.length()) {  // O(n/wordLen)
                // move right pointer
                String rword = s.substring(r-wordLen+1, r+1);
                if (curMap.containsKey(rword)) {
                    curMap.put(rword, curMap.get(rword)-1);
                    if (curMap.get(rword) == 0) counter++;
                }
                r+=wordLen;

                // move left pointer
                while (counter==curMap.size()) {
                    if (r-l+1-wordLen== wordLen*words.length) ans.add(l); // target
                    String lword = s.substring(l, l+wordLen);
                    if (curMap.containsKey(lword)) {
                        curMap.put(lword, curMap.get(lword)+1);
                        if (curMap.get(lword) == 1) counter--;
                    }
                    l+=wordLen;
                }
            }
        }
        return ans;
    }

    public static void main(String[] args) {
        String s = "barfoothefoobarman";
        String[] words = new String[]{"foo","bar"};
//        String s = "aaaaaaaa";
//        String[] words = new String[]{"aa","aa","aa"};
//        "barfoothefoobarman"
//                ["foo","bar"]
        List<Integer> ans = new SubstringWithConcatenationOfAllWords().findSubstring_v2(s, words);
        for (int x:ans) System.out.println(x);
    }

}

/*
You are given a string, s, and a list of words, words, that are all of the same length.
Find all starting indices of substring(s) in s that is a concatenation of each word in words exactly once and without any intervening characters.

Example 1:

Input:
  s = "barfoothefoobarman",
  words = ["foo","bar"]
Output: [0,9]
Explanation: Substrings starting at index 0 and 9 are "barfoor" and "foobar" respectively.
The output order does not matter, returning [9,0] is fine too.
Example 2:

Input:
  s = "wordgoodgoodgoodbestword",
  words = ["word","good","best","word"]
Output: []
*/