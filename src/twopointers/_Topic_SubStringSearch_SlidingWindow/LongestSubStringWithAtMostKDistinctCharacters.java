package twopointers._Topic_SubStringSearch_SlidingWindow;

import java.util.HashMap;
import java.util.Map;

/*
Given a string S, find the length of the longest substring T that contains at most k distinct characters.

Example
Example 1:

Input: S = "eceba" and k = 3
Output: 4
Explanation: T = "eceb"
Example 2:

Input: S = "WORLD" and k = 4
Output: 4
Explanation: T = "WORL" or "ORLD"
Challenge
O(n) time
*/
public class LongestSubStringWithAtMostKDistinctCharacters {

      // 注意！！ 这种写法对于LongestXXX没问题，但对于ShortestXXX就会出错，所以还是采用标准模板用法更合适！！
//    public int lengthOfLongestSubstringKDistinct_x3(String s, int k) {
//        int maxLen=0;
//        Map<Character, Integer> map = new HashMap<>();
//        int left=0, right=0;
//        while(right<s.length()){
//            if(map.size()<=k) { // move right
//                char c=s.charAt(right);
//                map.put(c, map.getOrDefault(c, 0)+1);
//                if (map.size()<=k) maxLen = Math.max(maxLen, right-left+1);
//                right++;
//            } else { // move left    注意！！ 这种写法对于LongestXXX没问题，但对于ShortestXXX就会出错，所以还是采用标准模板用法更合适！！
//                char c=s.charAt(left);
//                map.put(c, map.get(c)-1);
//                if(map.get(c)==0) map.remove(c);
//                left++;
//            }
//        }
//        return maxLen;
//    }

    // use the template !! good!!
    public int lengthOfLongestSubstringKDistinct_x2(String s, int k) {
        Map<Character, Integer> map = new HashMap<>();
        int l=0, r=0, maxLen = 0, distincts=0;
        while (r<s.length()) {
            // move right p
            char rc = s.charAt(r);
            map.put(rc, map.getOrDefault(rc, 0)+1);
            if (map.get(rc)==1) distincts++;
            if (distincts<=k) maxLen = Math.max(maxLen, r-l+1);
            r++;

            // move left p
            while (distincts>k) {
                char lc = s.charAt(l);
                map.put(lc, map.get(lc)-1);
                if (map.get(lc)==0) distincts--;
                l++;
            }
        }
        return maxLen;
    }


//    public int lengthOfLongestSubstringKDistinct_x1(String s, int k) {
//        // write your code here
//        Map<Character, Integer> map = new HashMap<>();
//        int maxLen=0, l=0, counter=0;
//        for (int r=0;r<s.length();r++) {
//            if (!map.containsKey(s.charAt(r)) || map.get(s.charAt(r)) == 0) {counter++;} // update counter
//            map.put(s.charAt(r), map.getOrDefault(s.charAt(r),0)+1);
//
//            while(counter > k) {  // move l to right most
//                if (map.get(s.charAt(l)) == 1) {counter--;}
//                map.put(s.charAt(l), map.get(s.charAt(l))-1);
//                l++;
//            }
//
//            maxLen = Math.max(maxLen, r-l+1); // update max
//        }
//        return maxLen;
//    }


    public static void main(String[] args) {
        String s="eqgkcwGFvjjmxutystqdfhuMblWbylgjxsxgnoh";
        int k=16;
        System.out.println(new LongestSubStringWithAtMostKDistinctCharacters().lengthOfLongestSubstringKDistinct_x2(s, k));
//        System.out.println(new LongestSubStringWithAtMostKDistinctCharacters().lengthOfLongestSubstringKDistinct_x3(s, k));
    }

}
