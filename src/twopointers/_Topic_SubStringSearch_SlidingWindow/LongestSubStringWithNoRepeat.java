package twopointers._Topic_SubStringSearch_SlidingWindow;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class LongestSubStringWithNoRepeat {
    // use the template!
    public int lengthOfLongestSubstring_x2(String s) {
        Map<Character, Integer> seen = new HashMap<>();
        int l=0, r=0, maxLen = 0, repeats=0;
        while (r<s.length()) {
            // move r
            char rc= s.charAt(r);
            seen.put(rc, seen.getOrDefault(rc, 0)+1);
            if (seen.get(rc)>1) repeats++;
            if (repeats==0) maxLen=Math.max(maxLen, r-l+1);
            r++;

            // move l
            while (repeats!=0) {
                char lc = s.charAt(l);
                seen.put(lc, seen.get(lc)-1);
                if (seen.get(lc)==1) repeats--;
                l++;  // 一种优化，如果保存有重复元素的index，可以跳进...
            }
        }
        return maxLen;
    }

    // n steps (improved version)
    public int lengthOfLongestSubstring_v2(String s) {
        Map<Character, Integer> map = new HashMap<>();  // key: char  value: the index of the char
        int l=0, maxLen=0;
        for (int r = 0; r < s.length(); r++) {
            if (map.containsKey(s.charAt(r))) {
                l = Math.max(l, map.get(s.charAt(r))+1); // direct jump over the position of first repeated elem to avoid repeat
            }
            maxLen = Math.max(maxLen, r-l+1);
            map.put(s.charAt(r), r);
        }
        return maxLen;
    }

    // 2n steps
    public int lengthOfLongestSubstring(String s) {
        Set<Character> set = new HashSet<>();
        int l=0, r=0, maxLen = 0;

        while (r<s.length()) {
            if (!set.contains(s.charAt(r))) {
                set.add(s.charAt(r));
                maxLen = Math.max(maxLen, r-l+1); // not r-l+1
                r++;
            } else {
                set.remove(s.charAt(l++));
            }

        }
        return maxLen;
    }

    public static void main(String[] args) {
        System.out.println(new LongestSubStringWithNoRepeat().lengthOfLongestSubstring_x2("abcabcbb"));
    }


/*
Given a string, find the length of the longest substring without repeating characters.

Example 1:

Input: "abcabcbb"
Output: 3
Explanation: The answer is "abc", with the length of 3.
Example 2:

Input: "bbbbb"
Output: 1
Explanation: The answer is "b", with the length of 1.
Example 3:

Input: "pwwkew"
Output: 3
Explanation: The answer is "wke", with the length of 3.
Note that the answer must be a substring, "pwke" is a subsequence and not a substring.
*/























//    public int lengthOfLongestSubstring(String s) {
//        Map<Character, Integer> countMap = new HashMap<>();
//        int l=0, maxLen=0;
//        for (int r=0;r<s.length();r++) {
//            countMap.put(s.charAt(r), countMap.getOrDefault(s.charAt(r),0)+1); // update
//
//            while (countMap.get(s.charAt(r))>1) {// move l ro rightmost pos (to avoid repeat) !!!!
//                countMap.put(s.charAt(l), countMap.get(s.charAt(l))-1);
//                l++;
//            }
//
//            maxLen = Math.max(maxLen, r-l+1); // update max
//        }
//
//        return maxLen;
//    }

//    public int lengthOfLongestSubstring_v2(String s) {
//        int max=0, l=0, r=0;
//        Map<Character, Integer> map = new HashMap<>();
//        while( l < s.length() && r<s.length()) {
//            if (map.containsKey(s.charAt(r))) {
//                // map.remove(s.charAt(l)); l++; // slower
//                int newL = map.get(s.charAt(r))+1; // do jumping to improve!!
//                for (int i=l;i<newL; i++) { map.remove(s.charAt(i));}
//                l = newL;
//            }
//            else {
//                map.put(s.charAt(r), r);
//                max = Math.max(r-l+1, max);
//                r++;
//            }
//        }
//
//        return max;
//
//    }
}
