//Given an array A of positive integers, call a (contiguous, not necessarily distinct) subarray of A good if the number of different integers in that subarray is exactly K.
//
// (For example, [1,2,3,1,2] has 3 different integers: 1, 2, and 3.)
//
// Return the number of good subarrays of A.
//
//
//
// Example 1:
//
//
//Input: A = [1,2,1,2,3], K = 2
//Output: 7
//Explanation: Subarrays formed with exactly 2 different integers: [1,2], [2,1], [1,2], [2,3], [1,2,1], [2,1,2], [1,2,1,2].
//
//
// Example 2:
//
//
//Input: A = [1,2,1,3,4], K = 3
//Output: 3
//Explanation: Subarrays formed with exactly 3 different integers: [1,2,1,3], [2,1,3], [1,3,4].
//
//
//
//
// Note:
//
//
// 1 <= A.length <= 20000
// 1 <= A[i] <= A.length
// 1 <= K <= A.length

package twopointers._Topic_SubStringSearch_SlidingWindow._Case_ConvertTarget;

/*
    注意!!!
    Sliding Window 适用条件为“一个指针移动趋向于满足目标，另一个指针移动趋向于背离目标”。
    如果直接算 SubArrays With K Distincts，不满足该条件，需要先转化为 SubArrays With At Most K Distincts。
 */
public class SubarraysWithKDifferentIntegers {
    public int subarraysWithKDistinct(int[] A, int K) {
        return atMost(A,K)-atMost(A,K-1);
    }

    int atMost(int[] A, int K) {
        int n = A.length, cnt = 0;
        int[] freq = new int[n+1];
        int l=0, r=0, distinct=0;
        while(r<n) {
            freq[A[r]]++;
            if(freq[A[r]]==1) distinct++;
            r++;

            while(distinct>K) {
                freq[A[l]]--;
                if(freq[A[l]]==0) distinct--;
                l++;
            }

            cnt += r-l;
        }
        return cnt;
    }

    public static void main(String[] a) {
        System.out.println(new SubarraysWithKDifferentIntegers().subarraysWithKDistinct(new int[]{1,2,1,2,3}, 2));
        System.out.println(new SubarraysWithKDifferentIntegers().subarraysWithKDistinct(new int[]{1,2,1,3,4}, 3));
    }

}
