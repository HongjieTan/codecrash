// Given an array of integers nums and an integer k. A subarray is called nice if there are k odd numbers on it.
//
// Return the number of nice sub-arrays.
//
//
// Example 1:
//
//
//Input: nums = [1,1,2,1,1], k = 3
//Output: 2
//Explanation: The only sub-arrays with 3 odd numbers are [1,1,2,1] and [1,2,1,1].
//
//
// Example 2:
//
//
//Input: nums = [2,4,6], k = 1
//Output: 0
//Explanation: There is no odd numbers in the array.
//
//
// Example 3:
//
//
//Input: nums = [2,2,2,1,2,2,1,2,2,2], k = 2
//Output: 16
//
//
//
// Constraints:
//
//
// 1 <= nums.length <= 50000
// 1 <= nums[i] <= 10^5
// 1 <= k <= nums.length
//

package twopointers._Topic_SubStringSearch_SlidingWindow._Case_ConvertTarget;

public class CounterNumberOfNiceSubarrays {
    public int numberOfSubarrays(int[] nums, int k) {
        return atMost(nums, k) - atMost(nums, k-1);
    }

    int atMost(int[] nums, int k) {
        int n = nums.length;
        int l=0, r=0, cnt=0, odds=0;
        while(r<n) {
            if(nums[r]%2==1) odds++;
            r++;

            while(odds>k) {
                if(nums[l]%2==1) odds--;
                l++;
            }
            cnt+=r-l;
        }
        return cnt;
    }

    public static void main(String[] as) {
        int nums[] = {1,1,2,1,1}, k = 3;
//        nums = [2,4,6], k = 1
//        nums = [2,2,2,1,2,2,1,2,2,2], k = 2
    }
}
