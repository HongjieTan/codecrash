// You are given a string containing only 4 kinds of characters 'Q', 'W', 'E' and 'R'.
//
// A string is said to be balanced if each of its characters appears n/4 times where n is the length of the string.
//
// Return the minimum length of the substring that can be replaced with any other string of the same length to make the original string s balanced.
//
// Return 0 if the string is already balanced.
//
//
// Example 1:
//
//
// Input: s = "QWER"
// Output: 0
// Explanation: s is already balanced.
//
// Example 2:
//
//
// Input: s = "QQWE"
// Output: 1
// Explanation: We need to replace a 'Q' to 'R', so that "RQWE" (or "QRWE") is balanced.
//
//
// Example 3:
//
//
// Input: s = "QQQW"
// Output: 2
// Explanation: We can replace the first "QQ" to "ER".
//
//
// Example 4:
//
//
// Input: s = "QQQQ"
// Output: 3
// Explanation: We can replace the last 3 'Q' to make s = "QWER".
//
//
//
// Constraints:
//
//
// 1 <= s.length <= 10^5
// s.length is a multiple of 4
// s contains only 'Q', 'W', 'E' and 'R'.
//

package twopointers._Topic_SubStringSearch_SlidingWindow;


// sliding window , 注意这里是统计window外面的情况！！
class ReplaceTheSubstringForBalancedString {
    public int balancedString(String s) {
        int n = s.length();
        int[] freq = new int[128];
        char[] chs = s.toCharArray();
        for(char c:chs) freq[c]++;

        int l=0, r=0, minlen=n;
        while(r<n) {
            // move l
            freq[chs[r]]--;
            r++;

            // move r
            while(l<n && ok(freq,n)) {
                minlen = Math.min(minlen, r-l);
                freq[chs[l]]++;
                l++;
            }
        }
        return minlen;
    }
    boolean ok(int[] freq, int n) {
        return freq['Q']<=n/4 && freq['W']<=n/4 && freq['E']<=n/4 && freq['R']<=n/4;
    }

    public static void main(String[] as) {
        System.out.println(new ReplaceTheSubstringForBalancedString().balancedString("QWER"));
        System.out.println(new ReplaceTheSubstringForBalancedString().balancedString("QQWE"));
        System.out.println(new ReplaceTheSubstringForBalancedString().balancedString("QQQW"));
        System.out.println(new ReplaceTheSubstringForBalancedString().balancedString("QQQQ"));
    }
}

/*

by lee:
Intuition
We want a minimum length of substring,
which leads us to the solution of sliding window.
Specially this time we don't care the count of elements inside the window,
we want to know the count outside the window.


Explanation
One pass the all frequency of "QWER".
Then slide the window in the string s.

Imagine that we erase all character inside the window,
as we can modify it whatever we want,
and it will always increase the count outside the window.

So we can make the whole string balanced,
as long as max(count[Q],count[W],count[E],count[R]) <= n / 4.


Complexity:
Time O(N), one pass for counting, one pass for sliding window
Space O(1)


Java

    public int balancedString(String s) {
        int[] count = new int[128];
        int n = s.length(), res = n, i = 0;
        for (int j = 0; j < n; ++j) {
            ++count[s.charAt(j)];
        }
        for (int j = 0; j < n; ++j) {
            --count[s.charAt(j)];
            while (i < n && count['Q'] <= n / 4 && count['W'] <= n / 4 && count['E'] <= n / 4 && count['R'] <= n / 4) {
                res = Math.min(res, j - i + 1);
                ++count[s.charAt(i++)];
            }
        }
        return res;
    }
C++

    int balancedString(string s) {
        unordered_map<int, int> count;
        int n = s.length(), res = n, i = 0;
        for (int j = 0; j < n; ++j) {
            count[s[j]]++;
        }
        for (int j = 0; j < n; ++j) {
            count[s[j]]--;
            while (i < n && count['Q'] <= n / 4 && count['W'] <= n / 4 && count['E'] <= n / 4 && count['R'] <= n / 4) {
                res = min(res, j - i + 1);
                count[s[i++]] += 1;
            }
        }
        return res;
    }
Python:

    def balancedString(self, s):
        count = collections.Counter(s)
        res = n = len(s)
        i = 0
        for j, c in enumerate(s):
            count[c] -= 1
            while i < n and all(n / 4 >= count[c] for c in 'QWER'):
                res = min(res, j - i + 1)
                count[s[i]] += 1
                i += 1
        return res

More Similar Sliding Window Problems
Here are some similar sliding window problems.
Also find more explanations.
Good luck and have fun.

1234. Replace the Substring for Balanced String
992. Subarrays with K Different Integers
904. Fruit Into Baskets


by uwi:
class Solution {
	    public int balancedString(String s_) {
	    	char[] s = s_.toCharArray();
	    	int n = s.length;

	    	int[][] pre = new int[4][n+1];
	    	for(int i = 0;i < n;i++){
	    		for(int j = 0;j < 4;j++){
	    			pre[j][i+1] = pre[j][i];
	    		}
	    		pre["QWER".indexOf(s[i])][i+1]++;
	    	}

	    	int[][] suf = new int[4][n+1];
	    	for(int i = 0;i < n;i++){
	    		for(int j = 0;j < 4;j++){
	    			suf[j][i+1] = suf[j][i];
	    		}
	    		suf["QWER".indexOf(s[n-1-i])][i+1]++;
	    	}

	    	int p = 0;
	    	int ret = n;
	    	for(int i = 0;i <= n;i++){
	    		while(p <= i){
	    			boolean ok = true;
	    			for(int k = 0;k < 4;k++){
	    				if(pre[k][p] + suf[k][n-i] <= n/4){
	    				}else{
	    					ok = false;
	    				}
	    			}
	    			if(ok){
	    				p++;
	    			}else{
	    				break;
	    			}
	    		}
	    		if(p > 0){
	    			ret = Math.min(ret, i-p+1);
	    		}
	    	}
	    	return ret;
	    }
	}
*/