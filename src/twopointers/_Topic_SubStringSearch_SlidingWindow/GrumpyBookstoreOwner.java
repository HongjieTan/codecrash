package twopointers._Topic_SubStringSearch_SlidingWindow;

public class GrumpyBookstoreOwner {
    public int maxSatisfied(int[] customers, int[] grumpy, int X) {
        int cur = 0, n=customers.length;
        for(int i=0;i<n;i++) {
            if(grumpy[i]==0) cur+=customers[i];
        }

        int res=cur, l=0,r=0;
        while(r<n) {
            // move r
            if(grumpy[r]==1)  cur+=customers[r];
            r++;

            // move l
            while(r-l==X) {
                res = Math.max(res, cur);
                if(grumpy[l]==1) cur-=customers[l];
                l++;
            }
        }
        return res;
    }
}
