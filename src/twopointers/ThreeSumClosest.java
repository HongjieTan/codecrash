package twopointers;

import java.util.Arrays;

/**
 * Created by thj on 29/05/2018.
 *
 *   O(n^2)/O(1) !!!
 *
 */
public class ThreeSumClosest {
    public int threeSumClosest(int[] nums, int target) {
        Arrays.sort(nums);
        int closestDiff = Integer.MAX_VALUE;

        for (int i = 0; i<nums.length-2;i++) {
            int l = i+1;
            int r = nums.length-1;
            int minPositiveDiff = Integer.MAX_VALUE, maxNegativeDiff = - Integer.MAX_VALUE; // NOTICE!!! If Integer.MIN_VALUE, it will cause numeric overflow !!!!
            while(l<r) {
                int diff = nums[l] + nums[r] + nums[i] - target;
                if (diff > 0 ) {
                    minPositiveDiff = Math.min(minPositiveDiff, diff);
                    r--;
                }
                else {
                    maxNegativeDiff = Math.max(maxNegativeDiff, diff);
                    l++;
                }
            }
            if (minPositiveDiff > Math.abs(maxNegativeDiff)) {
                if (Math.abs(closestDiff) > Math.abs(maxNegativeDiff)) {
                    closestDiff = maxNegativeDiff;
                }
            } else {
                if (Math.abs(closestDiff) > minPositiveDiff) {
                    closestDiff = minPositiveDiff;
                }
            }
        }

        return closestDiff+target;

    }
}
