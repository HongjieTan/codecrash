package twopointers;

/**
 * Created by thj on 29/05/2018.
 */
public class ContainerWithMostWater {
    public int maxArea(int[] height) {
        int l = 0, r = height.length-1;
        int maxArea = 0, curArea;

        while(l<r) {
            curArea = getArea(height[l], height[r], r-l);
            maxArea = Math.max(maxArea, curArea);
            if (height[l] <= height[r]) {
                l++;
            } else {
                r--;
            }
        }

        return maxArea;
    }

    private int getArea(int h1, int h2, int w) {
        return w * Math.min(h1, h2);
    }
}
