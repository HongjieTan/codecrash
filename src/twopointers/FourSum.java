package twopointers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by thj on 23/05/2018.
 */
public class FourSum {

    public List<List<Integer>> fourSum(int[] nums, int target) {
        List<List<Integer>> res = new ArrayList<>();
        Arrays.sort(nums);

        for (int aIdx=0; aIdx<nums.length; aIdx++) {
            if ( aIdx>0&&nums[aIdx] == nums[aIdx-1]) continue; // handle dup

            List<List<Integer>> subRes = threeSum(nums, aIdx+1, nums.length, target-nums[aIdx]);
            for (List<Integer> subResItem: subRes) {
                List<Integer> newList = new ArrayList<>();
                newList.add(nums[aIdx]);
                newList.addAll(subResItem);
                res.add(newList);
            }
        }

        return res;

    }

    private List<List<Integer>> threeSum(int[] nums, int start, int end, int target) {

        List<List<Integer>> res = new ArrayList<>();

//        Arrays.sort(nums); //!

        for(int aIdx = start; aIdx < end; aIdx++) {
            if (aIdx>start &&nums[aIdx]==nums[aIdx-1]) continue; // handle duplicate!
            int target2Sum = target-nums[aIdx];

            // use TwoSum (sorted)
            int l = aIdx+1, r=end-1;
            while (l<r) {
                int sum = nums[l] + nums[r];
                if (sum == target2Sum) {
                    res.add(Arrays.asList(nums[aIdx], nums[l], nums[r]));
                    l++; r--;
                    while (l<r && nums[l]==nums[l-1]) l++; // handle duplicate! notice 'l<r' to avoid array index error!
                    while (l<r && nums[r]==nums[r+1]) r--;
                }
                else if (sum > target2Sum) {r--;}
                else {l++;}
            }
        }

        return res;

    }

}
