package twopointers;

import java.util.*;

/**
 * Created by thj on 23/05/2018.
 */
public class ThreeSum {
    public List<List<Integer>> threeSum(int[] nums) {

        List<List<Integer>> res = new ArrayList<>();

        Arrays.sort(nums); //!

        for(int aIdx = 0; aIdx < nums.length; aIdx++) {
            if (aIdx>0 && nums[aIdx]==nums[aIdx-1]) continue; // handle duplicate!
            int target = -nums[aIdx];

            // use TwoSum (sorted)
            int l = aIdx+1, r=nums.length-1;
            while (l<r) {
                int sum = nums[l] + nums[r];
                if (sum == target) {
                    res.add(Arrays.asList(nums[aIdx], nums[l], nums[r]));
                    l++; r--;
                    while (l<r && nums[l]==nums[l-1]) l++; // handle duplicate! notice 'l<r' to avoid array index error!
                    while (l<r && nums[r]==nums[r+1]) r--;
                }
                else if (sum > target) {r--;}
                else {l++;}
            }
        }
        return res;
    }

    // use set to avoid dup
//    public List<List<Integer>> threeSum_v2(int[] numbers) {
//        // write your code here
//        List<List<Integer>> res = new ArrayList<>();
//        Arrays.sort(numbers);
//
//        Set<String> set = new HashSet<>();
//
//        for (int i = 0; i < numbers.length; i++) {
//            int target = -numbers[i];
//            int l = i+1, r = numbers.length-1;
//            while(l<r) {
//                int sum = numbers[l] + numbers[r];
//                if (sum == target) {
//                    if (!set.contains(getKey(numbers[i], numbers[l], numbers[r]))) {
//                        res.add(Arrays.asList(numbers[i], numbers[l], numbers[r]));
//                        set.add(getKey(numbers[i], numbers[l], numbers[r]));
//                    }
//                    l++;r--;
//                }
//                else if ( sum > target) r--;
//                else if (sum < target) l++;
//            }
//        }
//
//        return res;
//    }
//
//    private String getKey(int a, int b, int c) {
//        return a+"#"+b+"#"+c;
//    }
}
