package twopointers;

import java.util.Arrays;

/**
 * Created by thj on 05/06/2018.
 */
public class ThreeSumSmaller {
    public int threeSumSmaller(int[] nums, int target) {
        // Write your code here
        Arrays.sort(nums);
        int cnt=0;
        for (int i=0;i<nums.length;i++) {
            int remains = target-nums[i];
            int l=i+1, r=nums.length-1;
            while (l<r) {
                int subSum = nums[l] + nums[r];
                if (subSum < remains) {
                    cnt+=r-l;
                    l++;
                } else {
                    r--;
                }
            }
        }
        return cnt;
    }
}
