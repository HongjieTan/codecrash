package _OODesign;

/**
 *
 * TODO
 *  check prepare-notes.pdf
 *
 *
 */
public class VendingMachineSystem {


    interface State {
        public void insertCoin() throws Exception;
        public void pressButton() throws Exception;
        public void dispense() throws Exception;
    }

    class CoinInsertedState implements State {
        VendingMachine machine = null;
        public CoinInsertedState(VendingMachine machine){
            this.machine = machine;
        }
        @Override
        public void insertCoin() throws Exception {
            throw new Exception("Coin is already inserted");
        }

        @Override
        public void pressButton() throws Exception {
//            machine.setState();
        }

        @Override
        public void dispense() throws Exception {

        }
    }

    class VendingMachine {
        private State state;

        public void setState(State state) {
            this.state = state;
        }
    }

    // ....


}
