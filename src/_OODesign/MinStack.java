package _OODesign;

import java.util.Stack;

/**
 * Created by thj on 2018/7/21.
 */
public class MinStack {

    /** initialize your data structure here. */
    class Node {
        int value;
        int min;
        public Node(int value, int min) {this.value=value; this.min=min;}
    }

    private Stack<Node> stack;


    public MinStack() {
        stack = new Stack<>();
    }

    public void push(int x) {
        if (stack.isEmpty()) {
            stack.push(new Node(x, x));
        } else {
            stack.push(new Node(x, Math.min(getMin(), x)));
        }
    }

    public void pop() {
        stack.pop();
    }

    public int top() {
        if (stack.isEmpty()) return -1;
        return stack.peek().value;
    }

    public int getMin() {
        return stack.peek().min;
    }

    public static void main(String[] args) {
        MinStack minStack = new MinStack();
        minStack.push(-2);
        minStack.push(0);
        minStack.push(-3);
        System.out.println(minStack.getMin());  //--> Returns -3.
        minStack.pop();
        System.out.println(minStack.top());   //--> Returns 0.
        System.out.println(minStack.getMin());  //--> Returns -2.
    }
}

/**
 * Your MinStack object will be instantiated and called as such:
 * MinStack obj = new MinStack();
 * obj.push(x);
 * obj.pop();
 * int param_3 = obj.top();
 * int param_4 = obj.getMin();
 */

/**  sample:
 MinStack minStack = new MinStack();
 minStack.push(-2);
 minStack.push(0);
 minStack.push(-3);
 minStack.getMin();   --> Returns -3.
 minStack.pop();
 minStack.top();      --> Returns 0.
 minStack.getMin();   --> Returns -2.
 */
