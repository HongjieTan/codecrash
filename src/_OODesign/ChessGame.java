package _OODesign;

import java.util.List;

/***************************

Game:
 - board
 - player1, player2
 - turn
 - commands // for history tracking
 - handle(player, command)

 Board:
 - spotMatrix
 - initPieces()
 - movePiece()
 - removePiece()

 Spot:
 - x,y
 - piece
 - occupySpot()
 - releaseSpot()

 Piece (Abstract):
 - color
 - isMoveValid(board, fromX, fromY, toX, toY) (abstract)

 KingPiece extends Piece:
 - ...

 SoliderPiece extends Piece:
 - ...

 Player:
 - color
 - pieces


 Command:
 - piece
 - fromX, fromY. toX, toY

****************************/
public class ChessGame {

}

enum Color {While, Black}

class Game {
    private Board board;
    private Player p1;
    private Player p2;
    private Color turn;
    private List<Command> commands;

    public Game(){
        //...
    }

    public boolean handle(Player player, Command command) {
        if (player.getColor() != turn) return false;

        board.movePiece(command.getPiece(), command.getFromX(), command.getFromY(), command.getToX(), command.getToY());
        commands.add(command);

        if (board.getWinner()!=null) showWinner(board.getWinner());

        return true;
    }

    public void showWinner(Color color) {
        //...
    }

}

class Board {

    private Spot_[][] spotMatrix;
    private Color winner;

    public Board(){
        //...
    }

    public boolean movePiece(Piece piece, int fromX, int fromY, int toX, int toY) {
        if (!piece.isMoveValid(this, fromX, fromY, toX, toY))
            return false;

        if (spotMatrix[toX][toY] != null
                && spotMatrix[toX][toY].getPiece().getColor() == piece.getColor())
            return false;


        Piece eaten = spotMatrix[toX][toY].occupySpot(piece);
        if (eaten!=null && eaten.getName().endsWith("king")) this.winner = piece.getColor();

        spotMatrix[toX][toY].releaseSpot();

        return true;
    }

    public Color getWinner() {
        return this.winner;
    }
}

class Spot_ {
    private Piece piece;

    public Piece occupySpot(Piece piece) {
        Piece oldPiece = this.piece;
        this.piece = piece;
        return oldPiece;
    }

    public Piece releaseSpot() {
        Piece releasedPiece = this.piece;
        this.piece = null;
        return releasedPiece;
    }

    public Piece getPiece() {
        return piece;
    }
}

abstract class Piece {
    private int x, y;
    private Color color;
    private String name;

    public abstract boolean isMoveValid(Board board, int fromX, int fromY, int toX, int toY);

    public Color getColor() {
        return color;
    }
    public String getName() {
        return name;
    }
}

class KingPiece extends Piece{

    @Override
    public boolean isMoveValid(Board board, int fromX, int fromY, int toX, int toY) {
        // concrete rules...
        return true;
    }
}

class Player {
    private Color color;

    public Color getColor() {
        return color;
    }
}

class Command {
    private Piece piece;
    private int fromX, fromY, toX,toY;

    public Piece getPiece() {
        return piece;
    }

    public int getFromX() {
        return fromX;
    }

    public int getFromY() {
        return fromY;
    }

    public int getToX() {
        return toX;
    }

    public int getToY() {
        return toY;
    }
}



