package _OODesign;

import java.util.ArrayList;
import java.util.List;

/**
 *
 https://www.geeksforgeeks.org/design-parking-lot-using-object-oriented-principles/
 http://www.andiamogo.com/S-OOD.pdf

 The wording of this question is vague, just as it would be in an actual interview. This requires you to have a conversation
 with your interviewer about what types of vehicles it can support, whether the parking lot has multiple levels, and so on.
 For our purposes right now, we'll make the following assumptions. We made these specific assumptions to add a bit of complexity
 to the problem without adding too much. If you made different assumptions, that's totally fine.
 • The parking lot has multiple levels. Each level has multiple rows of spots.
 • The parking lot can park motorcycles, cars, and buses.
 • The parking lot has motorcycle spots, compact spots, and large spots.
 • A motorcycle can park in any spot.
 • A car can park in either a single compact spot or a single large spot.
 • A bus can park in five large spots that are consecutive and within the same row. It
 cannot park in small spots.
 In the below implementation, we have created an abstract class Vehicle, from which Car, Bus, and Motorcycle inherit.
 To handle the different parking spot sizes, we have just one class ParkingSpot which has a member variable indicating the size.
 */


/**
 *
 * user case:
 *
 *   1. park:     partingLot.park(vehicle)
 *   2. leave:    vehicle.leave()
 *
 * core class hierarchy
 *
 *   ParkingLot:
 *     - levels
 *     + parkVehicle
 *   Level:
 *     - spots(or avaiableSpots)
 *     + parkVehicle
 *     + spotFreed
 *   Spot:
 *     - size
 *     - vehicle
 *     + occupy
 *     + release
 *
 *   Vehicle:(abstract)
 *     - licensePlate(id)
 *     - size
 *     - spotsNeeded
 *     + park
 *     + leave
 *   Moto extends Vehicle:
 *      - size=S
 *      - spotsNeeded=1
 *   Car extends Vehicle:
 *      - size=M
 *      - spotsNeeded=1
 *   Bus extends Vehicle:
 *      - size=L
 *      - spotsNeeded=5
 *
 */


public class ParkingLot {

    private Level[] levels;
//    private final int NUM_LEVELS = 5;

    public ParkingLot(){
        // init levels...
    }

    public boolean parkVehicle(Vehicle vehicle) {
        // 1. to pick a level accoring to some strategy
        Level pickedLevel = levels[0];

        // 2. picked level call parkVehicle
        return pickedLevel.parkVehicle(vehicle);
    }
}

class Level {
    // if we use spots array here, the find is linear time complexity!
    // if no 'bus need consecutive 5 spots', we can use que/stack/... such as FreeSpotsQue to make find constant time!
    private Spot[] spots;
    private int avaiableSpots = 0;

    public Level(int numberSpots){
        // init spots...
    }

    public boolean parkVehicle(Vehicle vehicle) { // consider thread safe here...
        //...
        synchronized(this) {
            // 1. find avaliable spots ...
            // 2. spot.occupy
            return true;
        }
    }

    public void spotReleased(Spot spot) {
        // after spot freed, update resources in level...
        avaiableSpots++;
    }

    private int findAvaibleSpots() {
        // find start index of spots array where spot/spots is avaliable
        return 3;
    }

}

class Spot {
    private Vehicle vehicle;
    private Size spotSize;
//    private int row;
//    private int spotNumber;
    private Level level;

    public Spot(Level level, int row, int spotNumber, Size size) {
        //...
    }

    /* core functions */
    public boolean parkVehicle(Vehicle vehicle) {
        // 1. update spot status
        if (!canFitVehicle(vehicle)) {
            return false;
        }
        this.vehicle = vehicle;


        // 2. vehicle.park()
        this.vehicle.park(this);
        return true;
    }

    public void release() {
        // 1. update spot status
        this.vehicle = null;

        // 2. notify level to update status
        level.spotReleased(this);
    }


    /* other functions */
    public Size getSize(){
        return this.spotSize;
    }

    private boolean canFitVehicle(Vehicle vehicle) {
        return isAvailable() && vehicle.canFitInSpot(this);
    }

    public boolean isAvailable() {
        return vehicle == null;
    }

}


enum Size {
    Small, Middle, Large
}

abstract class Vehicle {
    protected String licensePlate;
    protected Size size;
    protected int spotsNeeded;
    protected List<Spot> spotsTaken = new ArrayList<>();

    /* core funcs */
    public void park(Spot spot) {
        spotsTaken.add(spot);
    }

    public void leave() {
        for (int i = 0; i < spotsTaken.size(); i++) {
            spotsTaken.get(i).release();
        }
        spotsTaken.clear();
    }

    abstract boolean canFitInSpot(Spot spot);
}

class Motocycle extends Vehicle {
    public Motocycle(String licensePlate){
        this.licensePlate = licensePlate;
        this.size = Size.Small;
        this.spotsNeeded = 1;
    }

    @Override
    boolean canFitInSpot(Spot spot) {
        return true;
    }
}

class Car extends Vehicle {

    public Car(String licensePlate){
        this.licensePlate = licensePlate;
        this.size = Size.Middle;
        this.spotsNeeded = 1;
    }

    @Override
    boolean canFitInSpot(Spot spot) {
        return spot.getSize()==Size.Middle || spot.getSize()==Size.Large;
    }
}

class Bus extends Vehicle {
    public Bus(String licensePlate){
        this.licensePlate = licensePlate;
        this.size = Size.Large;
        this.spotsNeeded = 5;
    }

    @Override
    boolean canFitInSpot(Spot spot) {
        return spot.getSize() == Size.Large;
    }
}
