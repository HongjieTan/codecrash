http://massivetechinterview.blogspot.com/2015/07/thought-works-object-oriented-design.html

### Use cases:
User
- presses the floor button to summon the lift
- presses the elevator button to make the lift move to the desired floor

Floor Button/Elevator Button
- illuminates when pressed
- places a elevator request when pressed

Elevator
- Moves up/down as per instruction
- Opens/closes the door


### class hierarchy:

<img src="elevator-class-diagram.png" />

### class description:
- ElevatorRequests Class:
Each button press results in an elevator request which has to be served. Each of these requests is tracked at a global place. ElevatorRequests, the class which stores elevator requests can use different strategies to schedule the elevator requests.
- ElevatorController:
The elevator is controlled by a controller class which we call ElevatorController. The elevator controller instructs the elevator what to do and also can shutdown/start up the elevator of the building. The elevator controller reads the next elevator request to be processed and serves it.
- Button (Abstract) Class:
Button is abstract class defining common behavior like illuminate, doNotIlluminate. FloorButton, ElevatorButton extend Button type and define placeRequest() which is invoked when the button is pressed.

In conclusion, ElevatorController runs the show by reading the ElevatorRequests to process and instructing the Elevator what to do. User send request by pressingButtons.

### Extend the answer to multiple elevators
- Each elevator have 1 controller.
- Floor based requests can be served by any elevator, thus these requests are added to a common area accessible by all controllers.
- Each elevator controller runs as a separate thread and checks if it can process a floor request. Mind synchronization issues.