//package _OODesign;
//
//import java.util.linkedlist_queue;
//import java.util.List;
//import java.util.Queue;
//
//public class Elevator implements IElevator{
//
//    private Integer currentFloor;
//    private Queue<Integer> targetFloors;
//
//    public Elevator() {
//        this.currentFloor = 0;
//        this.targetFloors = new linkedlist_queue<>();
//    }
//
//    public int nextDestination() {
//        return this.targetFloors.peek();
//    }
//
//    public int currentFloor(){
//        return this.currentFloor;
//    }
//
//    public void popDestination(){
//        this.targetFloors.remove();
//    }
//
//    @Override
//    public void moveUp() {
//        currentFloor++;
//    }
//
//    @Override
//    public void moveDown() {
//        currentFloor--;
//    }
//
//    @Override
//    public void addNewDestination(Integer destination) {
//        this.targetFloors.add(destination);
//    }
//
//    @Override
//    public ElevatorDirection direction() {
//        if (targetFloors.size() > 0) {
//            if (currentFloor < targetFloors.peek()) {
//                return ElevatorDirection.UP;
//            } else if (currentFloor > targetFloors.peek()) {
//                return ElevatorDirection.DOWN;
//            }
//        }
//        return ElevatorDirection.HOLD;
//    }
//
//    @Override
//    public ElevatorStatus status() {
//        return (targetFloors.size() > 0)?ElevatorStatus.OCCUPIED:ElevatorStatus.EMPTY;
//    }
//}
//
//class Controller implements IController {
//
////    private static final int MAX_ELEVATORS = 16;
//    private int numOfElevators = 0;
//    private int numOfFloors = 0;
//    private List<Elevator> elevators;
//    private Queue<Integer> pickupLocations;
//
//
//    public Controller(int numOfElevators, int numOfFloors) {
//        this.numOfElevators = numOfElevators;
//        this.numOfFloors = numOfFloors;
//        for(int i=0; i<this.numOfElevators; i++) {
//            this.elevators.add(new Elevator());
//        }
//        this.pickupLocations = new linkedlist_queue<>();
//    }
//
//    @Override
//    public void pickUp(Integer pickUpFloor) {
//        this.pickupLocations.add(pickUpFloor);
//    }
//
//    @Override
//    public void destination(Integer elevatorIdx, Integer destinationFloor) {
//        this.elevators.get(elevatorIdx).addNewDestination(destinationFloor);
//    }
//
//    @Override
//    public void step() {
//        for (Elevator elevator: this.elevators) {
//            if (elevator.status() == ElevatorStatus.EMPTY) { // choose empty elevator
//                if(!pickupLocations.isEmpty()) elevator.addNewDestination(pickupLocations.poll());
//            } else { // move occupied elevator
//                switch (elevator.direction()) {
//                    case UP:
//                        elevator.moveUp();
//                        break;
//                    case DOWN:
//                        elevator.moveDown();
//                        break;
//                    case HOLD:
//                        elevator.popDestination();
//                        break;
//                }
////                if (elevator.direction() == ElevatorDirection.UP) //？
////                    break;
//            }
//        }
//    }
//}
//
//
//
//interface IElevator {
//    public void moveUp();
//    public void moveDown();
//    public void addNewDestination(Integer destination);
//    public ElevatorDirection direction();
//    public ElevatorStatus status();
//}
//
//interface IController {
//    public void pickUp(Integer pickUpFloor);
//    public void destination(Integer elevatorId, Integer destinationFloor);
//    public void step();
//}
//
//enum ElevatorDirection {
//    UP, DOWN, HOLD
//}
//
//enum ElevatorStatus {
//    OCCUPIED,
//    EMPTY;
//}
//
//
//
//
