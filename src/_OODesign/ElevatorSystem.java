package _OODesign;


import java.util.LinkedList;
import java.util.Queue;

/**
 *
 * class design:
 *
 * Elevator:
 *  - status
 *  - curFloor
 *  - targetFloors
 *  + moveUp()
 *  + moveDown()
 *  + addTargetFloor()
 *
 *
 * Controller:
 *  - elevators
 *  + makeRequest(targetFloor)
 *  + step()
 *
 * Button:
 *  - controller
 *  + buttonCommonMethods....
 *
 * FloorButton > Button:
 *  - floor
 *  + makeRequest()
 *
 * ElevatorButton > Button:
 *  + makeRequest(floor)
 *
 *
 */
public class ElevatorSystem {
}

enum Status {
    UP, DOWN, HOLD, IDLE
}

class Elevator {
    private int currentFloor;
    private Queue<Integer> targetFloors;
//    private Status status;

    public Elevator(){
        this.currentFloor = 1;
        this.targetFloors = new LinkedList<>();
    }

    public void moveUp() {
        this.currentFloor++;
    }

    public void moveDown() {
        this.currentFloor--;
    }

    public void addTargetFloor(int targetFloor) {
        this.targetFloors.add(targetFloor);
    }

    public void pollTargetFloor() {
        this.targetFloors.poll();
    }

    public Status getStatus(){
//        return this.status;
        if (this.targetFloors.size() >0) {
            if (targetFloors.peek() > currentFloor) {
                return Status.UP;
            } else if (targetFloors.peek() < currentFloor) {
                return Status.DOWN;
            } else {
                return Status.HOLD;
            }
        }
        return Status.IDLE;

    }
}


class Controller {
    private Elevator[] elevators;
    private Queue<Integer> reqFloors;

    public void makeRequest(int targetFloor) {
        this.reqFloors.add(targetFloor);
    }

    public void step() {
        for (Elevator elevator: elevators) {
            switch (elevator.getStatus()) {
                case UP:
                    elevator.moveUp();
                    break;
                case DOWN:
                    elevator.moveDown();
                    break;
                case HOLD:
                    elevator.pollTargetFloor();
                    break;
                case IDLE:
                    if (reqFloors.size()>0) elevator.addTargetFloor(reqFloors.poll());
                    break;
            }
        }
    }
}


abstract class Button {
    protected Controller controller;

    public void someCommonButtonMethods(){
        // ...
    }
}

class FloorButton extends Button {

    private int floor;

    public void makeRequest() {
        this.controller.makeRequest(floor);
    }
}


class ElevatorButton extends Button {

    public void makeRequest(int floor) {
        this.controller.makeRequest(floor);
    }
}




