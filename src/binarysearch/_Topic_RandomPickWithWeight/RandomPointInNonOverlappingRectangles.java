package binarysearch._Topic_RandomPickWithWeight;

import java.util.Random;
import java.util.TreeMap;


/**
 * LC850 Rectangle Area II （如果有重复部分，思路和此题打碎矩形去掉重复部分的思路一样）
 * LC497 Random Point in Non-overlapping Rectangles （没有重复部分的原题）
 * LC528 Random Pick with Weight （类似题目）
 *
 *  binary search +  prefix sum
 *     - v2:  use TreeMap directly!... code is concise...
 *     - v1:  implement binary search by hand
 */
public class RandomPointInNonOverlappingRectangles {

    // binary search v2. use TreeMap directly... code is concise...
    TreeMap<Integer, Integer> treeMap; // presum -> rect
    int[][] rects;
    public RandomPointInNonOverlappingRectangles(int[][] rects) {
        treeMap=new TreeMap<>();
        int sum = 0;
        for(int i=0;i<rects.length;i++) {
            int[] rect = rects[i];
            sum+=(rect[2]-rect[0]+1)*(rect[3]-rect[1]+1);
            treeMap.put(sum, i);
        }
        this.rects=rects;
    }
     public int[] pick() {
        Random random = new Random();
        // step1 pick a rect
        int r = random.nextInt(treeMap.lastKey())+1;
        int index = treeMap.get(treeMap.ceilingKey(r));
        int[] rect = rects[index];

        // step2 pick inside a rect
        int x = rect[0]+random.nextInt(rect[2]-rect[0]+1);
        int y = rect[1]+random.nextInt(rect[3]-rect[1]+1);
        return new int[]{x,y};
     }


    // binary search v1, implement binary search by hand
//    int[] presum;
//    int[][] rects;
//    public binarysearch._Topic_RandomPickWithWeight.RandomPointInNonOverlappingRectangles(int[][] rects) {
//        this.rects = rects;
//        presum = new int[rects.length];
//        presum[0] = pointsInRect(rects[0]);
//        for(int i=1;i<rects.length;i++) presum[i]=presum[i-1]+pointsInRect(rects[i]);
//    }
//
//    int pointsInRect(int[] rect) {
//        return (rect[2]-rect[0]+1)*(rect[3]-rect[1]+1);
//    }
//
//    public int[] pick() {
//        int target=new Random().nextInt(presum[presum.length-1])+1;
//        int l=0, r=presum.length-1, mid=0;
//        while(l<=r) {
//            mid=l+(r-l)/2;
//
//            if(mid==0 && presum[mid]>=target) break;
//            if(presum[mid]>=target && presum[mid-1]<target) break;
//
//            if(presum[mid]>target) r=mid-1;
//            else l=mid+1;
//        }
//
//        int[] rect = rects[mid];
//        int x = new Random().nextInt(rect[2]-rect[0]+1) + rect[0];
//        int y = new Random().nextInt(rect[3]-rect[1]+1) + rect[1];
//        return new int[]{x,y};
//    }

    // brute force: Memory Limit Exceeded!
//    List<int[]> points = new ArrayList<>();
//    public binarysearch._Topic_RandomPickWithWeight.RandomPointInNonOverlappingRectangles(int[][] rects) {
//        for(int[] rect: rects) {
//            int x1=rect[0], y1=rect[1], x2=rect[2], y2=rect[3];
//            for(int i=x1; i<=x2; i++)
//                for(int j=y1; j<=y2; j++)
//                    points.add(new int[]{i,j});
//        }
//    }
//    public int[] pick() { // O（1）
//        return points.get(new Random().nextInt(points.size()));
//    }

    public static void main(String[] args) {
        int[][] rects = new int[][] {
            new int[]{-2,-2,-1,-1},
            new int[]{1,0,3,0},
        };
        RandomPointInNonOverlappingRectangles solution = new RandomPointInNonOverlappingRectangles(rects);
        solution.pick();
        solution.pick();
        solution.pick();
        solution.pick();
        solution.pick();


    }
}

/*
Given a list of non-overlapping axis-aligned rectangles rects,
write a function pick which randomly and uniformily picks an integer point in the space covered by the rectangles.

Note:

An integer point is a point that has integer coordinates.
A point on the perimeter of a rectangle is included in the space covered by the rectangles.
ith rectangle = rects[i] = [x1,y1,x2,y2], where [x1, y1] are the integer coordinates of the bottom-left corner,
and [x2, y2] are the integer coordinates of the top-right corner.
length and width of each rectangle does not exceed 2000.
1 <= rects.length <= 100
pick return a point as an array of integer coordinates [p_x, p_y]
pick is called at most 10000 times.
Example 1:

Input:
["binarysearch._Topic_RandomPickWithWeight.Solution","pick","pick","pick"]
[[[[1,1,5,5]]],[],[],[]]
Output:
[null,[4,1],[4,1],[3,3]]
Example 2:

Input:
["binarysearch._Topic_RandomPickWithWeight.Solution","pick","pick","pick","pick","pick"]
[[[[-2,-2,-1,-1],[1,0,3,0]]],[],[],[],[],[]]
Output:
[null,[-1,-2],[2,0],[-2,-1],[3,0],[-2,-2]]
Explanation of Input Syntax:

The input is two lists: the subroutines called and their arguments. binarysearch._Topic_RandomPickWithWeight.Solution's constructor has one argument,
the array of rectangles rects. pick has no arguments. Arguments are always wrapped with a list, even if there aren't any.
*/

/*
class binarysearch._Topic_RandomPickWithWeight.Solution {
    TreeMap<Integer, Integer> map;
    int[][] arrays;
    int sum;
    Random rnd= new Random();

    public binarysearch._Topic_RandomPickWithWeight.Solution(int[][] rects) {
        arrays = rects;
        map = new TreeMap<>();
        sum = 0;

        for(int i = 0; i < rects.length; i++) {
            int[] rect = rects[i];

            // the right part means the number of points can be picked in this rectangle
            sum += (rect[2] - rect[0] + 1) * (rect[3] - rect[1] + 1);

            map.put(sum, i);
        }
    }

    public int[] pick() {
        // nextInt(sum) returns a num in [0, sum -1]. After added by 1, it becomes [1, sum]
        int c = map.ceilingKey( rnd.nextInt(sum) + 1);

        return pickInRect(arrays[map.get(c)]);
    }

    private int[] pickInRect(int[] rect) {
        int left = rect[0], right = rect[2], bot = rect[1], top = rect[3];

        return new int[]{left + rnd.nextInt(right - left + 1), bot + rnd.nextInt(top - bot + 1) };
    }
}
*/