//Given an array w of positive integers, where w[i] describes the weight of index i, write a function pickIndex which randomly picks an index in proportion to its weight.
//
// Note:
//
//
// 1 <= w.length <= 10000
// 1 <= w[i] <= 10^5
// pickIndex will be called at most 10000 times.
//
//
// Example 1:
//
//
//Input:
//["Solution","pickIndex"]
//[[[1]],[]]
//Output: [null,0]
//
//
//
// Example 2:
//
//
//Input:
//["Solution","pickIndex","pickIndex","pickIndex","pickIndex","pickIndex"]
//[[[1,3]],[],[],[],[],[]]
//Output: [null,0,1,1,1,0]
//
//
// Explanation of Input Syntax:
//
// The input is two lists: the subroutines called and their arguments. Solution's constructor has one argument, the array w. pickIndex has no arguments. Arguments are always wrapped with a list, even if there aren't any.
//

package binarysearch._Topic_RandomPickWithWeight;

import java.util.Random;
import java.util.TreeMap;


/**
 *  presum + binary search （can use TreeMap directly ...）
 *
 *   - 直接用presum数组，构建时候O(logn), 查询O(logn)
 *   - 用treemap， 构建时候O(nlogn）,查询O(logn)，coding更方便
 *
 */
public class RandomPickWithWeight {}

class Solution {

    TreeMap<Integer, Integer> treeMap;
    Random random = new Random();
    public Solution(int[] w) {
        treeMap=new TreeMap<>();
        int sum=0;
        for(int i=0;i<w.length;i++) {
            sum+=w[i];
            treeMap.put(sum, i);
        }
    }

    public int pickIndex() {
        int x = random.nextInt(treeMap.lastKey())+1;
        return treeMap.get(treeMap.ceilingKey(x));
    }

}

/**
 * Your binarysearch._Topic_RandomPickWithWeight.Solution object will be instantiated and called as such:
 * binarysearch._Topic_RandomPickWithWeight.Solution obj = new binarysearch._Topic_RandomPickWithWeight.Solution(w);
 * int param_1 = obj.pickIndex();
 */

