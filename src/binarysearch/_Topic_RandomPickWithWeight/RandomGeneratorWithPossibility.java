package binarysearch._Topic_RandomPickWithWeight;

import java.util.Random;

/*
Given n numbers, each with some frequency of occurrence.
Return a random number with probability proportional to its frequency of occurrence.

example:

Let following be the given numbers.
  arr[] = {10, 30, 20, 40}

Let following be the frequencies of given numbers.
  freq[] = {1, 6, 2, 1}

The output should be
  10 with probability 1/10
  30 with probability 6/10
  20 with probability 2/10
  40 with probability 1/10

 */

/**
 *  good question!!  presum + binarysearch !!!
 *
 *  build period: O(n)   generate operation: O(logn), binary search...
 */
public class RandomGeneratorWithPossibility {
    public int myRand(int arr[], int freq[], int n) {
        int[] presum = new int[n];
        presum[0] = freq[0];
        for (int i = 1; i < freq.length; i++) {
            presum[i]=presum[i-1]+freq[i];
        }

        // prefix[n-1] is sum of all frequencies.
        // Generate a random number with value from 1 to this sum
        int r = new Random().nextInt(presum[n-1])+1;

        // Find index of ceiling of r in prefix sum array
        int indexc = findCeil(presum, r);
        return arr[indexc];
    }

    // do binary search here!
    int findCeil(int[] presum, int target) {
        int l=0, r=presum.length-1;
        while (l<=r) {
            int mid = l+(r-l)/2;

            if (mid==0 && target<=presum[mid]) return mid;
//            if (mid==presum.length-1 && target<=presum[mid]) return mid;
            if (presum[mid-1]<target && target<=presum[mid]) return mid;

            if(presum[mid]>target) r=mid-1;
            else l=mid+1;
        }
        return -1;
    }

    public static void main(String[] args) {
        int[] arr = new int[]{10, 20, 30};
        int[] freq = new int[]{2,3,1};
        System.out.println(new RandomGeneratorWithPossibility().myRand(arr, freq, 3));
        System.out.println(new RandomGeneratorWithPossibility().myRand(arr, freq, 3));
        System.out.println(new RandomGeneratorWithPossibility().myRand(arr, freq, 3));
        System.out.println(new RandomGeneratorWithPossibility().myRand(arr, freq, 3));
        System.out.println(new RandomGeneratorWithPossibility().myRand(arr, freq, 3));
        System.out.println(new RandomGeneratorWithPossibility().myRand(arr, freq, 3));
    }

}
