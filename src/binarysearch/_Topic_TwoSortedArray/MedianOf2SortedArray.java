package binarysearch._Topic_TwoSortedArray;

/**

   - binary search directly
   - make use of find KthLargest in two sorted array : good!


 */
public class MedianOf2SortedArray {

    /*
      make use of Kth_In_2_Sorted_Array
     */
    public double findMedianSortedArrays_usingKth(int[] nums1, int[] nums2) {
        int m=nums1.length, n=nums2.length;
        if ((m+n)%2==0) {
            return (findKthHelper(nums1, 0,nums2,0, (m+n)/2)
                    + findKthHelper(nums1, 0,nums2,0, (m+n)/2+1))*0.5;
        } else {
            return findKthHelper(nums1, 0, nums2, 0,(m+n)/2+1);
        }
    }

    private int findKthHelper(int[] A, int aStart, int[] B, int bStart, int k){
        if(aStart >= A.length) return B[bStart + k - 1];
        if(bStart >= B.length) return A[aStart + k - 1];
        if(k == 1) return Math.min(A[aStart], B[bStart]);
        int aMid = aStart + k/2 - 1;
        int bMid = bStart + k/2 - 1;
        int aVal = aMid >= A.length ? Integer.MAX_VALUE : A[aMid];
        int bVal = bMid >= B.length ? Integer.MAX_VALUE : B[bMid];
        if(aVal <= bVal) return findKthHelper(A, aMid + 1, B, bStart, k - k/2);
        else return findKthHelper(A, aStart, B, bMid + 1, k - k/2);
    }


    /**
     *  a[0, ..., i-1],  |  a[i, ... , m-1]
     *  b[0, ..., j-1],  |  b[j, ... , n-1]
     *
     *  find i in [0,m] that:
     *    i+j = m+n-i-j + 1 => j=(m+n+1)/2 - i
     *    a[i] <= b[j+1]
     *    b[j] <= a[i+1]
     */
    public double findMedianSortedArrays(int a[], int b[]) {
        if (a.length > b.length) { // keep m <= n
            int temp[] = a; a = b; b = temp;
        }
        int m = a.length, n = b.length, halfLen = (m+n+1)/2;

        int start = 0, end=m;
        while(start <= end) {
            int i = (start+end)/2;
            int j = halfLen - i;
            if (i>0 && a[i-1] > b[j]) { // i>0 and m<=n => j<n
                end = end-1;
            } else if (i<m && b[j-1] > a[i]) { // i<m and m<=n => j>0
                start = start+1;
            } else { // i is perfect!
                double maxLeft, minRight;
                if (i == 0) {maxLeft = b[j-1];}
                else if (j == 0){maxLeft = a[i-1];}
                else { maxLeft = Math.max(a[i-1], b[j-1]);}

                if (i==m && j==n) {minRight = 0;} // notice this case!
                else if (i==m ) {minRight = b[j];}
                else if (j==n ){minRight = a[i];}
                else {minRight = Math.min(a[i], b[j]);}

                if ((m+n)%2 == 0) {return (maxLeft + minRight)/2d;}
                else {return maxLeft;}
            }
        }
        return -1d;
    }



}
