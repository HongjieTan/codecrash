package binarysearch._Topic_TwoSortedArray;

/**
 *
 *  good!!! need to keep in mind!!
 *
 *  - binary search: O(logk)
 *  - brute force: O(k)
 *
 */
public class KthIn2SortedArray {

    // recursive, code is concise!!
    public int findKth_recursive(int[] A, int[] B, int k) {
        return findKthHelper(A, 0, B, 0, k);
    }
    private int findKthHelper(int[] A, int aStart, int[] B, int bStart, int k){
        if(aStart >= A.length) return B[bStart + k - 1];
        if(bStart >= B.length) return A[aStart + k - 1];
        if(k == 1) return Math.min(A[aStart], B[bStart]);
        int aMid = aStart + k/2 - 1;
        int bMid = bStart + k/2 - 1;
        int aVal = aMid >= A.length ? Integer.MAX_VALUE : A[aMid];
        int bVal = bMid >= B.length ? Integer.MAX_VALUE : B[bMid];
        if(aVal <= bVal) return findKthHelper(A, aMid + 1, B, bStart, k - k/2);
        else return findKthHelper(A, aStart, B, bMid + 1, k - k/2);
    }

    // iterative
    public int findKth_iterative(int[] A, int[] B, int k) {
        if (A == null || B == null || k <= 0) return -1;
        int m = A.length, n = B.length;
        int la = 0, lb = 0;
        while (la < m && lb < n) {
            if (k==1) return Math.min(A[la],B[lb]);
            int midA = la+k/2-1; // split by k/2!
            int midB = lb+k/2-1;
            int valA = midA >= A.length ? Integer.MAX_VALUE : A[midA];
            int valB = midB >= B.length ? Integer.MAX_VALUE : B[midB];
            if(valA <= valB) {
                la = midA+1;  // kick off left part of A
                k-=k/2;
            } else {
                lb = midB+1; // kick off left part of B
                k-=k/2;
            }
        }
        if (la<m) return A[la+k-1];
        if (lb<n) return B[lb+k-1];
        return -1;
    }

    // iterative
//    public int findKth_v2(int[] A, int[] B, int k) {
//        if (A==null || B==null || k<=0) return -1;
//        int m=A.length, n=B.length;
//        int la=0, lb=0;
//
//        while (la<m && lb<n) {
//            // target found
//            if (k==1) return Math.min(A[la],B[lb]);
//
//            int midA = la+k/2-1; // split by k/2!
//            int midB = lb+k/2-1;
//
//            // case1. size of arr A is less than k/m2
//            if (midA>=m) {
//                if (A[m-1] < B[midB]) { // kick off all A
//                    k-=(m-la);
//                    la=m;
//                } else { // kick off left side of B
//                    k -= k/2;
//                    lb = midB+1;
//                }
//            }
//            // case2. size of arr B is less than k/2
//            else if (midB>=n) {
//                if (B[n-1] < A[midA]) { //  kick off all B
//                    k-=(n-lb);
//                    lb=n;
//                } else { // kick off left side of A
//                    la = midA+1;
//                    k -= k/2;
//                }
//            }
//            // case3. both size no less than k/2
//            else {
//                if (A[midA] < B[midB]) { // kick off left side of A
//                    la = midA+1;
//                    k -= k/2;
//                } else { // kick off left side of B
//                    lb = midB+1;
//                    k -= k/2;
//                }
//            }
//        }
//
//        if (la<m) return A[la+k-1];
//        if (lb<n) return B[lb+k-1];
//        return -1;
//    }


    public static void main(String[] args) {
        int[] A = new int[]{2,3,6,7,9};
        int[] B = new int[]{1,4,8,10,11,20};
        int k = 9;
        System.out.println(new KthIn2SortedArray().findKth_recursive(A, B, k));
    }
}