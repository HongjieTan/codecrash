//Write an efficient algorithm that searches for a value in an m x n matrix. This matrix has the following properties:
//
//
// Integers in each row are sorted from left to right.
// The first integer of each row is greater than the last integer of the previous row.
//
//
// Example 1:
//
//
//Input:
//matrix = [
//  [1,   3,  5,  7],
//  [10, 11, 16, 20],
//  [23, 30, 34, 50]
//]
//target = 3
//Output: true
//
//
// Example 2:
//
//
//Input:
//matrix = [
//  [1,   3,  5,  7],
//  [10, 11, 16, 20],
//  [23, 30, 34, 50]
//]
//target = 13
//Output: false
package binarysearch._Topic_FindSomething;

/**
 * Created by thj on 2018/7/25.
 */
public class Search2DMatrix {


    // 1. treat matrix as sorted 1d array
    public boolean searchMatrix_v2(int[][] matrix, int target) {
        if (matrix.length==0) return false;
        int m = matrix.length, n=matrix[0].length;
        int l=0, r=m*n-1;
        while (l<=r) {
            int mid = l+(r-l)/2;
            int row = mid/n, col = mid%n;
            if (matrix[row][col] == target) return true;
            else if (matrix[row][col] > target) r=mid-1;
            else l=mid+1;
        }
        return false;
    }

    // 2. first search row, then search column
    public boolean searchMatrix(int[][] matrix, int target) {
        if (matrix.length==0) return false;
        int rows = matrix.length;
        int l=0, r=rows-1;
        while (l<r) {
            int mid = l+(r-l)/2;

            if (matrix[mid][0]==target) return true;
            if (matrix[mid][0] < target && (mid>=rows || matrix[mid+1][0] > target)) {l=mid;break;}

            if (matrix[mid][0] > target) r=mid-1;
            else l=mid+1;
        }

        int[] arr = matrix[l];
        l=0; r=arr.length-1;
        while (l<=r) {
            int mid = l+(r-l)/2;
            if (arr[mid]==target) return true;
            if (arr[mid]>target) r = mid-1;
            else l=mid+1;
        }

        return false;
    }

    public static void main(String[] args) {
//        int[][] matrix = new int[][]{
//                {1, 2, 4, 6},
//                {8, 10, 12, 14},
//                {21, 23, 25, 29}};

        int[][] matrix = new int[][]{
                {1},
                {3}
        };

        System.out.println(new Search2DMatrix().searchMatrix(matrix, 3));
//        System.out.println(new Search2DMatrix().searchMatrix(matrix, 2));
//        System.out.println(new Search2DMatrix().searchMatrix(matrix, 11));
    }



}
