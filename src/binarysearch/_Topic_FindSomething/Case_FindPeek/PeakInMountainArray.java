//Let's call an array A a mountain if the following properties hold:
//
//
// A.length >= 3
// There exists some 0 < i < A.length - 1 such that A[0] < A[1] < ... A[i-1] < A[i] > A[i+1] > ... > A[A.length - 1]
//
//
// Given an array that is definitely a mountain, return any i such that A[0] < A[1] < ... A[i-1] < A[i] > A[i+1] > ... > A[A.length - 1].
//
// Example 1:
//
//
//Input: [0,1,0]
//Output: 1
//
//
//
// Example 2:
//
//
//Input: [0,2,1,0]
//Output: 1
//
//
// Note:
//
//
// 3 <= A.length <= 10000
// 0 <= A[i] <= 10^6
// A is a mountain, as defined above.
//
//

package binarysearch._Topic_FindSomething.Case_FindPeek;

public class PeakInMountainArray {

    // v2 is better then v1, see example in FindPeekElement.java
    public int peakIndexInMountainArray_v2(int[] A) {
        int l=0, r=A.length-1,m;
        while(l<r) {
            m = l + (r-l)/2;
            if(A[m]<A[m+1]) l=m+1;
            else r=m;
        }
        return r;
    }

    public int peakIndexInMountainArray_v1(int[] A) {
        int l=0, r=A.length-1;
        while(l<=r) {
            int mid = l + (r-l)/2;
            if(A[mid]<A[mid+1]) l=mid+1;
            else r=mid-1;
        }
        return l;
    }

    public static void main(String[] as) {
        int[] arr1 = {0,1,0};
        int[] arr2 = {0,2,1,0};
        int[] arr3 = {0,1,2,0};
        PeakInMountainArray su = new PeakInMountainArray();
        System.out.println(su.peakIndexInMountainArray_v1(arr1));
        System.out.println(su.peakIndexInMountainArray_v1(arr2));
        System.out.println(su.peakIndexInMountainArray_v1(arr3));

        System.out.println(su.peakIndexInMountainArray_v2(arr1));
        System.out.println(su.peakIndexInMountainArray_v2(arr2));
        System.out.println(su.peakIndexInMountainArray_v2(arr3));
    }
}

/*
int lo = 0, hi = A.length - 1;
        while (lo < hi) {
            int mid = lo + (hi - lo) / 2;
            if (A[mid] < A[mid + 1]) { // peak index is after mid.
                lo = mid + 1;
            }else if (A[mid -1] > A[mid]) { // peak index is before mid.
                hi = mid;
            }else { // peak index is mid.
                return mid;
            }
        }
        return -1; // no peak.


int lo = 0, hi = A.length - 1;
while (lo < hi) {
    int mid = lo + (hi - lo) / 2;
    if (A[mid] < A[mid + 1]) { lo = mid + 1; } // peak index is after mid.
    else{ hi = mid; } // peak index <= mid.
}
return lo; // found peak index.
*/