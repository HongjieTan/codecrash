//(This problem is an interactive problem.)
//
// You may recall that an array A is a mountain array if and only if:
//
//
// A.length >= 3
// There exists some i with 0 < i < A.length - 1 such that:
//
// A[0] < A[1] < ... A[i-1] < A[i]
// A[i] > A[i+1] > ... > A[A.length - 1]
//
//
//
//
// Given a mountain array mountainArr, return the minimum index such that mountainArr.get(index) == target. If such an index doesn't exist, return -1.
//
// You can't access the mountain array directly. You may only access the array using a MountainArray interface:
//
//
// MountainArray.get(k) returns the element of the array at index k (0-indexed).
// MountainArray.length() returns the length of the array.
//
//
// Submissions making more than 100 calls to MountainArray.get will be judged Wrong Answer. Also, any solutions that attempt to circumvent the judge will result in disqualification.
//
//
//
//
//
// Example 1:
//
//
//Input: array = [1,2,3,4,5,3,1], target = 3
//Output: 2
//Explanation: 3 exists in the array, at index=2 and index=5. Return the minimum index, which is 2.
//
// Example 2:
//
//
//Input: array = [0,1,2,4,2,1], target = 3
//Output: -1
//Explanation: 3 does not exist in the array, so we return -1.
//
//
//
// Constraints:
//
//
// 3 <= mountain_arr.length() <= 10000
// 0 <= target <= 10^9
// 0 <= mountain_arr.get(index) <= 10^9
//
//
package binarysearch._Topic_FindSomething.Case_FindPeek;

/**
 * // This is MountainArray's API interface.
 * // You should not implement it, or speculate about its implementation
 * interface MountainArray {
 *     public int get(int index) {}
 *     public int length() {}
 * }
 */
interface MountainArray {
    public int get(int index);
    public int length();
}

class MountainArray1 implements MountainArray{
    int[] arr;
    MountainArray1(int[] arr) {
        this.arr = arr;
    }
    public int get(int index) {
        return arr[index];
    }
    public int length(){
        return arr.length;
    }
}

public class FindInMountainArray {
    public int findInMountainArray(int target, MountainArray ma) {
        int n = ma.length();
        int l=0, r=n-1, mid;
        while(l<r) {
            mid = l+(r-l)/2;
            int x=ma.get(mid);
            int y=ma.get(mid+1);
            if (x<y) l=mid+1;
            else r=mid;
        }
        int top = r;

        // find in left part
        l=0;
        r=top;
        while(l<=r) {
            mid = l+(r-l)/2;
            int x=ma.get(mid);
            if( x == target) return mid;
            else if( x > target) r=mid-1;
            else l=mid+1;
        }

        // find in right part
        l = top+1;
        r = n-1;
        while(l<=r) {
            mid = l + (r-l)/2;
            int x=ma.get(mid);
            if (x==target) return mid;
            else if(x>target) l=mid+1;
            else r=mid-1;
        }
        return -1;
    }

    public static void main(String[] a) {
        int[] arr = {1,2,3,5,3};
        int target = 0;
        MountainArray ma = new MountainArray1(arr);
        System.out.println(new FindInMountainArray().findInMountainArray(target, ma));
    }
}


/*
by uwi:

public int findInMountainArray(int target, MountainArray mountainArr) {
    int n = mountainArr.length();
    int top = -1;
    {
        int low = 0, high = n;
        while(high - low > 1){
            int h = high+low-1>>1;
            if(mountainArr.get(h) < mountainArr.get(h+1)){
                low = h+1;
            }else{
                high = h+1;
            }
        }
        top = low;
    }
    if(mountainArr.get(top) == target)return top;
    {
        int low = 0, high = top+1;
        while(high - low > 0){
            int h = high+low>>1;
            int v = mountainArr.get(h);
            if(v == target)return h;
            if(v < target){
                low = h+1;
            }else{
                high = h;
            }
        }
    }
    {
        int low = top, high = n;
        while(high - low > 0){
            int h = high+low>>1;
            int v = mountainArr.get(h);
            if(v == target)return h;
            if(v > target){
                low = h+1;
            }else{
                high = h;
            }
        }
    }
    return -1;
}


by lee:
Triple Binary Search, Triple Happiness.

Intuition
Binary find peak in the mountain.
852. Peak Index in a Mountain Array
Binary find the target in strict increasing array
Binary find the target in strict decreasing array


Personally, (just a tip)
If I want find the index, I always use while (left < right)
If I may return the index during the search, I'll use while (left <= right)

Complexity
Time O(logN) Space O(1)


Some Improvement
Cache the result of get, in case we make the same calls.
In sacrifice of O(logN) space for the benefit of less calls.
Binary search of peak is unnecessary, just easy to write.

C++/Java

    int findInMountainArray(int target, MountainArray A) {
        int n = A.length(), l, r, m, peak = 0;
        // find index of peak
        l  = 0;
        r = n - 1;
        while (l < r) {
            m = (l + r) / 2;
            if (A.get(m) < A.get(m + 1))
                l = peak = m + 1;
            else
                r = m;
        }
        // find target in the left of peak
        l = 0;
        r = peak;
        while (l <= r) {
            m = (l + r) / 2;
            if (A.get(m) < target)
                l = m + 1;
            else if (A.get(m) > target)
                r = m - 1;
            else
                return m;
        }
        // find target in the right of peak
        l = peak;
        r = n - 1;
        while (l <= r) {
            m = (l + r) / 2;
            if (A.get(m) > target)
                l = m + 1;
            else if (A.get(m) < target)
                r = m - 1;
            else
                return m;
        }
        return -1;
    }
Python:

    def findInMountainArray(self, target, A):
        n = len(A)
        # find index of peak
        l, r = 0, n - 1
        while l < r:
            m = (l + r) / 2
            if A.get(m) < A.get(m + 1):
                l = peak = m + 1
            else:
                r = m
        # find target in the left of peak
        l, r = 0, peak
        while l <= r:
            m = (l + r) / 2
            if A.get(m) < target:
                l = m + 1
            elif A.get(m) > target:
                r = m - 1
            else:
                return m
        # find target in the right of peak
        l, r = peak, n - 1
        while l <= r:
            m = (l + r) / 2
            if A.get(m) > target:
                l = m + 1
            elif A.get(m) < target:
                r = m - 1
            else:
                return m
        return -1
*/