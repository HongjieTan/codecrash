package binarysearch._Topic_FindSomething;

/**
 * Created by thj on 22/05/2018.
 */
public class FindMinInRoatedSortedArray {
    public int findMin(int[] nums) {
        int start=0, end=nums.length-1, mid=0;

        while(start<=end) {
            if (nums[start] <= nums[end] ) return nums[start]; // all sorted, return ans

            mid = (start+end)/2;

            if (nums[start] < nums[mid]) { // left part sorted
                start = mid+1;
            } else if (nums[start] > nums[mid]){ // right part sorted
                end = mid;
            } else {
                start++; // nums[start] == nums[mid]
            }
        }
        return -1;

    }
}
