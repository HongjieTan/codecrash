package binarysearch._Topic_FindSomething.good_cases;

/*
Given a sorted array consisting of only integers where every element appears twice except for one element which appears once.
Find this single element that appears only once.
Example 1:
Input: [1,1,2,3,3,4,4,8,8]
Output: 2
Example 2:
Input: [3,3,7,7,10,11,11]
Output: 10
Note: Your solution should run in O(log n) time and O(1) space
*/
public class SingleElementInSortedArray {

    public int singleNonDuplicate(int[] nums) {
        int l=0, r=nums.length-1;
        while (l<=r) {
            int mid = l + (r-l)/2;
            if (mid==0 || mid==nums.length-1) return nums[mid];
            if (nums[mid]!=nums[mid+1] && nums[mid]!=nums[mid-1]) return nums[mid];

            if ( (mid%2==0 && nums[mid]==nums[mid-1]) || (mid%2!=0 && nums[mid]!=nums[mid-1])  ) r=mid-1;
            else  l=mid+1;
        }
        return -1;
    }

    public static void main(String[] args) {
        System.out.println(new SingleElementInSortedArray().singleNonDuplicate(new int[]{1,1,2,3,3,4,4,8,8}));
    }
}
