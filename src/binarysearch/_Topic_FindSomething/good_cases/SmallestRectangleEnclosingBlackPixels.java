package binarysearch._Topic_FindSomething.good_cases;

public class SmallestRectangleEnclosingBlackPixels {
    public int minArea(char[][] image, int x, int y) {
        // left border
        int l=0, r=y;
        while (l<=r) {
            int mid=l+(r-l)/2;
            if (hasPixelInCol(image, mid)) r=mid-1;
            else l=mid+1;
        }
        int left = l;

        // right border
        l=y; r=image[0].length-1;
        while (l<=r) {
            int mid=l+(r-l)/2;
            if (hasPixelInCol(image, mid)) l=mid+1;
            else r=mid-1;
        }
        int right = r;

        // top border
        l=0; r=x;
        while (l<=r) {
            int mid=l+(r-l)/2;
            if (hasPixelInRow(image, mid)) r=mid-1;
            else l=mid+1;
        }
        int top = l;

        // bottom border
        l=x; r=image.length-1;
        while (l<=r) {
            int mid=l+(r-l)/2;
            if (hasPixelInRow(image, mid)) l=mid+1;
            else r=mid-1;
        }
        int bottom = r;

        return (right-left+1)*(bottom-top+1);
    }

    boolean hasPixelInRow(char[][] image, int row) {
        for (int x: image[row]) if(x=='1') return true;
        return false;
    }
    boolean hasPixelInCol(char[][] image, int col) {
        for (int i=0;i<image.length;i++) if(image[i][col]=='1') return true;
        return false;
    }

    public static void main(String[] args) {
        char[][] image = new char[][]{
                "0010".toCharArray(),
                "0110".toCharArray(),
                "0100".toCharArray(),
        };
        System.out.println(new SmallestRectangleEnclosingBlackPixels().minArea(image, 0, 2));
    }
}


/*
An image is represented by a binary matrix with 0 as a white pixel and 1 as a black pixel.
The black pixels are connected, i.e., there is only one black region. Pixels are connected horizontally and vertically.
Given the location (x, y) of one of the black pixels,
return the area of the smallest (axis-aligned) rectangle that encloses all black pixels.

Example
For example, given the following image:

[
  "0010",
  "0110",
  "0100"
]
and x = 0, y = 2,
Return 6.
*/