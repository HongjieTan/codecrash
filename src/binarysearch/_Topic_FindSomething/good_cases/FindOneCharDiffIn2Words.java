package binarysearch._Topic_FindSomething.good_cases;

public class FindOneCharDiffIn2Words {
}

/*
两个string找多出来的char

2.第二题是个东南亚老大哥 亚裔，贼严肃，也不说话，上来就说题目， 然后题目是 给你两个string s1 s2, s2会比s1多一个char，
要你找出来那个char的index 比如 today, todxay, return 3
O(1) space better than O(N) time,我想了半天，巧妙一点的二分，follow up是如果两个string 乱序怎么办，就先sort


思路：
如果没有duplicate的话，根据时间空间复杂度要求自然联想到二分查找：
在长的string上二分
mid = l + (r - l) / 2
每次比较长的mid位置上的char和短的mid位置上的char是否一样，因为最多只会被插入一个字符，一样的话，说明左边部分没被插入东西，target一定右边，否则target在左边

注意：空串需要特殊处理，不然charAt会抛异常

如果有duplicate，感觉无法better than O(N)，一个极端例子dddddddddd, dddddddddde,如果不check每个位置的话，无法找到多余的那个字符

code
Provider: null
   public int findExtra(String s1, String s2) {
    	if(s1.length() == 0) return 0;
    	if(s2.length() == 0) return 0;
    	String longer = s1.length() > s2.length() ? s1 : s2;
    	String shorter = s1.length() > s2.length() ? s2 : s1;
    	int l = 0, r = longer.length() - 1;
    	while(l + 1 < r) {
    		int mid = l + (r - l) / 2;
    		if(longer.charAt(mid) == shorter.charAt(mid)) {
    			l = mid;
    		} else {
    			r = mid;
    		}
    	}
    	if(longer.charAt(l) != shorter.charAt(l)) return l;
    	else return r;
   }


def find_extra(str1, str2):
    if len(str1) > len(str2):
        str1, str2 = str2, str1
    lo, hi = 0, len(str1)
    while lo < hi:
        mid = (lo + hi) >> 1
        if str1[mid] != str2[mid]:
            hi = mid
        else:
            lo = mid + 1
    return lo


- 如果两个字符串分别是"BBB"和"ABBB"，这个算法得出的结果就不对了
- 不考虑有duplicates的情况，因为允许有重复的话没法better than O(n)，面试官要求比O(n)时间好

*/