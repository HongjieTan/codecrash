package binarysearch._Topic_FindSomething;

/**
 * Created by thj on 21/05/2018.
 */
public class SearchRoatedSortedArrayWithDup {
    public boolean search(int[] nums, int target) {
        int start = 0, end = nums.length-1;
        while (start <= end) {
            int mid = (start + end ) /2;
            if (nums[mid] == target) return true;

            // the only difference, just add this to handle dup
            if (nums[start] == nums[mid] && nums[mid] == nums[end]){start++;end--;}
            else if (nums[start] <= nums[mid]) { // left part is sorted
                if (target >= nums[start] && target < nums[mid]) {
                    end = mid-1;
                } else {
                    start = mid+1;
                }
            } else { // right part sorted
                if (target > nums[mid] && target <= nums[end]) {
                    start = mid + 1;
                } else {
                    end = mid - 1;
                }
            }
        }

        return false;
    }
}
