package binarysearch._Topic_FindSomething;

/**
 * Created by thj on 22/05/2018.
 */
public class SearchRange {

    public int[] searchRange(int[] nums, int target) {

        int[] res = new int[]{-1,-1};
        int leftIndex = leftInsertionIndex(nums, target);
        if (leftIndex == -1) return res;
        int rightIndex = rightInsertionIndex(nums, target);
        res[0] = leftIndex; res[1] = rightIndex;
        return res;
    }

    private int leftInsertionIndex(int[] nums, int target) {
        int start = 0, end = nums.length-1;
        while(start <= end) {
            int mid = (start+end)/2;
            if (nums[mid] == target) {
                if (start == end) return mid; // found
                end = mid;
            } else if (nums[mid] > target) {
                end = mid-1;
            } else {
                start = mid+1;
            }
        }
        return -1;
    }

    private int rightInsertionIndex(int[] nums, int target) {
        int start = 0, end = nums.length-1;
        while(start <= end) {
            int mid = (start+end+1)/2; // bias to right!!! to ensure search range shrink every time!!
            if (nums[mid] == target) {
                if (start == end) return mid; // found
                start = mid;
            } else if (nums[mid] > target) {
                end = mid-1;
            } else {
                start = mid+1;
            }
        }
        return -1;
    }

}
