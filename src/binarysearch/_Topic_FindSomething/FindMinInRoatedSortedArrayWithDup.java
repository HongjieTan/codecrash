package binarysearch._Topic_FindSomething;

/**
 * Created by thj on 22/05/2018.
 */
public class FindMinInRoatedSortedArrayWithDup {
    public int findMin(int[] nums) {
        int start = 0, end=nums.length-1, mid=0;

        while(start <= end) {
            mid = (start+end)/2;

            if (nums[start] < nums[end]) return nums[start];
            if (start==end) return nums[start];

            if (nums[start] > nums[mid]) { // sorted in right part
                end = mid;
            } else if (nums[start] < nums[mid]){ // sorted in left part
                start = mid+1;
            } else {
                start++;
            }
        }

        return -1;
    }
}
