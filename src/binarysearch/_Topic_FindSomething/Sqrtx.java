package binarysearch._Topic_FindSomething;

/**
 * Created by thj on 03/07/2018.
 */
public class Sqrtx {

    public int mySqrt(int x) {
        if (x==0) return 0;
        int i=1, j=x, mid;
        while(i<=j) {
            mid = i + (j-i)/2; // i+j may overflow!!
            if (mid<=x/mid && (mid+1)>x/(mid+1)) return mid; // found!
            if (mid>x/mid) j=mid-1;
            else i=mid+1;
        }
        return -1;
    }

    public static void main(String[] args) {
        System.out.println(new Sqrtx().mySqrt(9));
    }
}
