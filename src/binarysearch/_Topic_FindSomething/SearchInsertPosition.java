package binarysearch._Topic_FindSomething;

/**
 * Created by thj on 2018/7/19.
 *
 *
 */
public class SearchInsertPosition {

//    public int searchInsertNoDup(int[] nums, int target) {
//        int l=0, r=nums.length-1;
//        while(l<=r) {
//            int mid = l+(r-l)/2;
//            if (nums[mid]==target) return mid;
//            if (nums[mid] > target) r = mid-1;
//            else l=mid+1;
//        }
//        return l;
//    }

    public int searchInsertWithDup(int[] nums, int target) {
        int l=0, r=nums.length-1;
        while(l<=r) {
            int mid = l+(r-l)/2;
            if (nums[mid] >= target) r = mid-1;
            else l=mid+1;
        }
        return l;
    }

    public static void main(String[] args) {
//        System.out.println(new SearchInsertPosition().searchInsertNoDup(new int[]{1,2,3,6}, 4));
        System.out.println(new SearchInsertPosition().searchInsertWithDup(new int[]{1,2,2,3,6}, 2));
    }
}
