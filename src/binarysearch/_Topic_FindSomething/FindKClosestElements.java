package binarysearch._Topic_FindSomething;

import java.util.*;

/**
 *  binary search + 2p
 */
public class FindKClosestElements {

    public static void main(String[] args) {
        int[] arr = new int[]{0,1,3,5};
        int k=2;
        int x=2;
        List<Integer> ans = new FindKClosestElements().findClosestElements_v2(arr, k,x);
        for(int i:ans) System.out.println(i);
    }

    // concise version using api
    public List<Integer> findClosestElements_v2(int[] arr, int k, int x) {
        // binary search
//        int index = Arrays.binarySearch(arr, x);  // use api
//        if (index<0) index=-index-1;
        int l=0, r=arr.length-1, index=-1; // index ->insert point
        while (l<=r) {
            int mid=l+(r-l)/2;
            if (arr[mid]==x) {index=mid; break;}
            if (arr[mid]>x) {
                r=mid-1;
            } else {
                l=mid+1;
            }
        }
        if (index==-1) index=l;

        // 2p
        l=Math.max(0,index-k); r=l;
        int sum=0, minL=l, minSum=Integer.MAX_VALUE;
        while (r<Math.min(index+k, arr.length)) {
            if (r-l+1<=k) { // move r
                sum+=Math.abs(x-arr[r++]);
                if (r-l+1>k) {
                    if (sum<minSum) {
                        minSum=sum;
                        minL=l;
                    }
                }
            } else { // move l
                sum-=Math.abs(x-arr[l++]);
            }
        }

        List<Integer> ans = new ArrayList<>();
        for (int i = minL; i < minL+k; i++) ans.add(arr[i]);
        return ans;
    }

    public List<Integer> findClosestElements_v1(int[] arr, int k, int x) {
        int l=0, r=arr.length-1, mid=0;
        while (l<=r) {
            mid = l+(r-l)/2;
            if(mid==0 && arr[mid]>=x) break;
            if(mid==arr.length-1 && arr[mid]<x) break;
            if(arr[mid-1]<x && arr[mid]>=x) break;

            if(arr[mid]>x) r=mid-1;
            else l=mid+1;
        }

        int p=Math.max(0, mid-k);
        int curDist = 0;
        for (int i = p; i < p+k; i++) curDist+=Math.abs(arr[i]-x);
        int minP = p, minDist=curDist;
        p++;

        while (p<=mid && p+k-1<arr.length) {
            curDist-=Math.abs(arr[p-1]-x);
            curDist+=Math.abs(arr[p+k-1]-x);
            if (curDist<minDist) {
                minP = p;
                minDist = curDist;
            }
            p++;
        }

        List<Integer> ans = new ArrayList<>();
        for (int i = minP; i < minP+k; i++) ans.add(arr[i]);
        return ans;
    }
}

/*
Given a sorted array, two integers k and x, find the k closest elements to x in the array.
The result should also be sorted in ascending order. If there is a tie, the smaller elements are always preferred.

Example 1:
Input: [1,2,3,4,5], k=4, x=3
Output: [1,2,3,4]
Example 2:
Input: [1,2,3,4,5], k=4, x=-1
Output: [1,2,3,4]

Note:
The value k is positive and will always be smaller than the length of the sorted array.
Length of the given array is positive and will not exceed 10^4
Absolute value of elements in the array and x will not exceed 10^4
UPDATE (2017/9/19):
The arr parameter had been changed to an array of integers (instead of a list of integers).
Please reload the code definition to get the latest changes.
*/