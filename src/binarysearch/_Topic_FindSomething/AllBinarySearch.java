package binarysearch._Topic_FindSomething;

/**
 * Created by thj on 2018/7/25.
 *
 *
 * summary: the critical point solution is more clear, all we need is to clearly define the critical point!!
 *
 *
 */
public class AllBinarySearch {


    public static boolean searchTarget(int nums[], int target){
        int l=0, r=nums.length-1;
        while (l<=r) {
            int mid=l+(r-l)/2;

            // the critical point
            if (nums[mid]==target) return true;

            else if (nums[mid]>target) r=mid-1;
            else l=mid+1;
        }
        return false;
    }

    // https://leetcode.com/problems/search-insert-position/description/
    // may have dup
    // [1,3,5,5,5,7,9]
    // 0 -> 0
    // 1 -> 0
    // 2 -> 1
    // 5 -> 2
    // 10 -> 7

    // more clean code, but a little tricky
    public static int searchInsertPositionV1(int nums[], int target) {
        int l=0, r=nums.length-1;
        while (l<=r) {
            int mid = l+(r-l)/2;
            if (nums[mid]>=target) r=mid-1;
            else l=mid+1;
        }
        return l;
    }

    // more code, but critical point is clear
    public static int searchInsertPositionV2(int[] nums, int target) {
        int l=0, r=nums.length-1;
        while (l<=r) {
            int mid = l + (r-l)/2;

            // the critical point
            if ( mid==0 && nums[mid] >= target) return 0;
            if ( mid==nums.length-1 && nums[mid] < target) return nums.length;
            if ( nums[mid] < target && nums[mid+1] >= target) return mid+1;

            if (nums[mid] >= target) r=mid-1;
            else l=mid+1;
        }
        return l;
    }

    public static int sqrt(int n) {
        if (n==0) return 0;
        int l=1, r=n;
        while (l<=r) {
            int mid = l + (r-l)/2;

            // the critical point
            if (mid<=n/mid && (mid+1)>n/(mid+1)) return mid;

            else if(mid>n/mid) r=mid-1;
            else l=mid+1;
        }
        return l;
    }

    public static boolean search2DMatrix(int[][] matrix, int target) {
        if(matrix.length==0) return false;
        if(matrix[0].length==0) return false;
        int m=matrix.length, n=matrix[0].length;
        int l=0,r=m-1, row=0;
        while (l<=r) {
            int mid=l+(r-l)/2;

            // the critical point
            if (matrix[mid][0] == target) return true;
            if (mid==0 && matrix[mid][0] > target) return false;
            if (mid==m-1 && matrix[mid][0] < target) {row=mid; break;}
            if (matrix[mid][0] < target && matrix[mid+1][0]> target) {row=mid; break;}

            if (matrix[mid][0] > target) r=mid-1;
            else l=mid+1;
        }

        int[] arr = matrix[row];
        l=0; r=n-1;
        while (l<=r) {
            int mid=l+(r-l)/2;
            if (arr[mid]==target) return true;
            if (arr[mid]>target) r=mid-1;
            else l=mid+1;
        }
        return false;
    }

    public static boolean search2DMatrixV2(int[][] matrix, int target) {
        if(matrix.length==0) return false;
        if(matrix[0].length==0) return false;
        int m=matrix.length, n=matrix[0].length;
        int l=0, r=m*n-1;
        while (l<=r) {
            int mid=l+(r-l)/2;
            int row=mid/n, col=mid%n;
            if (matrix[row][col] == target) return true;
            if (matrix[row][col] > target) r=mid-1;
            else l=mid+1;
        }
        return false;
    }

    // https://leetcode.com/problems/search-in-rotated-sorted-array/description/
    public static int searchRotatedSortedArrayNoDup(int[] nums, int target) {
        int l=0, r=nums.length-1;
        while (l<=r) {
            int mid=l+(r-l)/2;

            // the critical point
            if (nums[mid] == target) return mid;

            if (nums[l]<=nums[mid]) { // left part sorted
                if (nums[mid] > target && nums[l] <= target) r=mid-1;
                else l=mid+1;
            } else { // right part sorted
                if (nums[mid] < target && nums[r] >= target) l=mid+1;
                else r=mid-1;
            }
        }
        return -1;
    }

    public static boolean searchRotatedSortedArrayWithDup(int[] nums, int target) {
        int l=0, r=nums.length-1;
        while (l<=r) {
            int mid=l+(r-l)/2;

            // target point
            if (nums[mid] == target) return true;

            if (nums[l] < nums[mid] || nums[mid] > nums[r]) { // left part sorted
                if (nums[l]<=target && nums[mid]>target) r=mid-1;
                else l=mid+1;
            } else if (nums[l] > nums[mid] || nums[mid]<nums[r]){ // right part sorted
                if (nums[mid]<target && nums[r]>=target) l=mid+1;
                else r=mid-1;
            } else { // nums[l]==nums[mid] && nums[mid]==nums[r]
                l++;r--;
            }
        }
        return false;
    }

    public static void main(String[] args) {
//        int[] nums = new int[]{1,2,3,4,6,7};
//        System.out.println(searchTarget(nums, 2));
//        System.out.println(searchTarget(nums, 5));

//        int[] nums = new int[]{1,3,5,5,5,7,9};
//        System.out.println(searchInsertPosition(nums, 0));
//        System.out.println(searchInsertPosition(nums, 1));
//        System.out.println(searchInsertPosition(nums, 2));
//        System.out.println(searchInsertPosition(nums, 5));
//        System.out.println(searchInsertPosition(nums, 10));

//        System.out.println(sqrt(1));
//        System.out.println(sqrt(2));
//        System.out.println(sqrt(3));
//        System.out.println(sqrt(4));

//        int[][] matrix = new int[][] {
//                {1, 3},
//                {10, 11}
//        };
//        System.out.println(search2DMatrix(matrix, 3));

        int[] arr = new int[]{1,1,3,1};
//        System.out.println(searchRotatedSortedArrayNoDup(arr, 1));
        System.out.println(searchRotatedSortedArrayWithDup(arr, 3));



    }

}
