package binarysearch._Topic_SpecialCase;



/*
https://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=489043

Desc1
假设给你一个有序数组，，那么如果二分搜索其中的每个数，必定都能找到。
如果给你同样的数组，但是却是打乱的，然后使用同样的二分搜索来搜索其中的每个数，会有些数找不到。
请你用 O(N)，返回哪些数找不到。

Desc2:
第一轮：算法。应该是新题? 上来先让我描述一下二分法的算法。
题目是给定一个无序无重复数组，对数组中的每一个数字，用二分法能不能在数组中找到，返回一个boolean数组显示能不能找到。
比如数组[3,8,6,7,5]，
target 3, 先找到6再找左边找到3，为true；
target 8, 先找到6，再找右边，而8在左边，右边永远找不到，为false；
target 6, 第一次二分就能找到，以此类推，
最终返回[true,false,true,true,false]。
我写了一个暴力解。他好像觉得时间复杂度过高。
*/

/*

    有点类似RedundantConnectionInBST !!

    - best solution: O(n), good!!! 带着范围递归地验证！！！
    - brute force: O(nlogn)

 */
public class BinarySearchInUnSortedArray {
    // 需要clarify一些情形，比如有数字无重复，二分找是否找到即停止等。。。
    public boolean[] findable(int[] arr) {
        boolean[] res = new boolean[arr.length];
        helper(arr, 0, arr.length-1, Integer.MIN_VALUE, Integer.MAX_VALUE, res);
        return res;
    }

    void helper(int[] arr, int l, int r, int min, int max, boolean[] res) {
        if (l>r)return;
        int mid = l+(r-l)/2;
        if(arr[mid]<=max && arr[mid]>=min) res[mid]=true;
        helper(arr, l, mid-1, min, arr[mid], res);
        helper(arr, mid+1, r, arr[mid], max, res);
    }

    public static void main(String[] agrs) {
        boolean[] res = new BinarySearchInUnSortedArray().findable(new int[]{3,8,6,7,5});
        for(boolean x:res) System.out.println(x);
    }

}

/*
void CheckHidden(const std::vector<int> &data,
                 std::vector<bool> &hidden,
                 const int lo, const int hi,
                 const int min_bound, const int max_bound) {
  if (lo > hi) {
    return;
  }

  const int mid = (lo + hi) / 2;-baidu 1point3acres
  const int value = data[mid];

  // 打印更新边界的过程
  std::cout << "data[" << mid << "] = " << data[mid] << ", bound = "
            << "[" << min_bound << ", " << max_bound << "]]\n";

  // hidden记录每个位置的值是否能被查找到
  hidden[mid] = (value <= min_bound || value >= max_bound);

  CheckHidden(data, hidden, lo, mid-1, min_bound, std::min(value, max_bound));
  CheckHidden(data, hidden, mid+1, hi, std::max(min_bound, value), max_bound);
}

// Wrapper
void CheckHidden(const std::vector<int> &data, std::vector<bool> &hidden) {
  // 初始边界是负无穷到正无穷
  CheckHidden(data, hidden, 0, data.size() - 1,
              std::numeric_limits<int>::min(),. From 1point 3acres bbs
              std::numeric_limits<int>::max());
}
*/

/*
using namespace std;

#define INT_MIN 0x80000000
#define INT_MAX 0x7fffffff

class Solution {
public:
    vector<int> cantSearch(const vector<int>& nums) {
        vector<bool> canSearch(nums.size(), false);
        search(nums, 0, nums.size()-1, INT_MIN, INT_MAX, canSearch);
        vector<int> res;
        for (int i = 0; i < nums.size(); ++i) {
            if (!canSearch[i]) res.push_back(i);
        }
        return res;
    }

private:
    void search(const vector<int>& nums, int s, int e,
                int min_val, int max_val, vector<bool>& canSearch) {
        if (s > e || min_val > max_val) return;
        int mid = (s + e) / 2;
        if (nums[mid] < max_val && nums[mid] > min_val) canSearch[mid] = true;
        search(nums, s, mid-1, min_val, min(max_val, nums[mid]), canSearch);
        search(nums, mid+1, e, max(min_val, nums[mid]), max_val, canSearch);
    }
};

- 像楼上说的有几个需要clarify的地方，这个代码前提是是基于简单 (s+e)/2 二分查找，找到就停止。
  输出的是找不到的数的下标。 如果需要输出具体哪些数字找不到，还要考虑有些数字出现多次，
  有些下标找不到有些下标找得到的情况，就需要最后过一遍数组，用一个hashset保存找不到的数。

*/