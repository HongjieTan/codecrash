package binarysearch._Topic_CompareByFunc;

/*
   https://www.1point3acres.com/bbs/thread-499490-1-1.html
   https://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=499453


   一道关于切巧克力的题。题目是给定一个数组（一板巧克力），数组里的每一个数字都表示其中一块的甜度。有n个人来分享这巧克力，需要切n-1刀。
   其他n-1个人会把甜度高的portion都拿走 剩下的留给你。问如何切割这块巧克力你自己尽可能能拿到最高的甜度。
   比如数组[3,2,3,1,4],3个人分，切成[3,2 |3,1 |4]，你能拿到最多的4。后来问了朋友，听说可以用二分答案来做？

    followup可能有：
    如果最高的甜度是按照max而非sum
 */
public class Chocolate_WithFollowUps {

    // dp[i][j] = max{ min{dp[k][j-1],sum(sweets[k+1...i])} } , k=j-1...i-1
    public int maxMinSweet_dp(int[] sweets, int m) {
        int n = sweets.length;
        int[][] dp = new int[n][m+1]; // dp[i][j]:  maxMinSweet of sweets[0...i] into m persons
        int[] presum = new int[n];
        presum[0]=sweets[0];
        for(int i=1;i<n;i++) presum[i]+=sweets[i]+presum[i-1];

        for(int i=0;i<n;i++) {
            for(int j=1;j<=m;j++) {
                if(j==1) dp[i][j]=presum[i];
                else {
                    int max=0;
                    for(int k=j-1; k<=i-1; k++) {
                        int min = Math.min(dp[k][j-1], presum[i]-presum[k]); // 如果最高甜度定义是max而非sum，这里改成max(sweets[k+1...i])
                        max = Math.max(min, max);
                    }
                    dp[i][j]=max;
                }
            }
        }
        return dp[n-1][m];
    }

    // bs
    public int maxMinSweet_bs(int[] sweets, int m) {
        int sum = 0, min=Integer.MAX_VALUE;
        for(int x: sweets){
            sum+=x;
            min=Math.min(x, min);
        }

        int l=min, r=sum;
        while (l<=r) {
            int mid = l+(r-l)/2;
            if (isValid(sweets, m, mid)) {
                l=mid+1;
            } else {
                r=mid-1;
            }
        }
        return r;
    }
    boolean isValid(int[] sweets, int m, int minSweet) {
        int count=0;  // 满足minSweet情况下要能够分m个人
        int sum=0;
        for (int x: sweets) {
            sum+=x;
            if (sum>=minSweet) {
                sum=0;
                count++;
            }
        }
        return count>=m;
    }

    public static void main(String[] args) {
        int[] sweets = new int[]{3,2,3,1,4};
        int m = 3;
//        int[] sweets = new int[]{1,5,9,3,4,8,4,7,4,9};
//        int m = 5;
        System.out.println(new Chocolate_WithFollowUps().maxMinSweet_dp(sweets, m));
        System.out.println(new Chocolate_WithFollowUps().maxMinSweet_bs(sweets, m));

    }
}


