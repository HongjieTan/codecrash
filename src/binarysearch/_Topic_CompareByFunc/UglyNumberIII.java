// Write a program to find the n-th ugly number.
//
// Ugly numbers are positive integers which are divisible by a or b or c.
//
//
// Example 1:
//
//
//Input: n = 3, a = 2, b = 3, c = 5
//Output: 4
//Explanation: The ugly numbers are 2, 3, 4, 5, 6, 8, 9, 10... The 3rd is 4.
//
// Example 2:
//
//
//Input: n = 4, a = 2, b = 3, c = 4
//Output: 6
//Explanation: The ugly numbers are 2, 3, 4, 6, 8, 9, 10, 12... The 4th is 6.
//
//
// Example 3:
//
//
//Input: n = 5, a = 2, b = 11, c = 13
//Output: 10
//Explanation: The ugly numbers are 2, 4, 6, 8, 10, 11, 12, 13... The 5th is 10.
//
//
// Example 4:
//
//
//Input: n = 1000000000, a = 2, b = 217983653, c = 336916467
//Output: 1999999984
//
//
//
// Constraints:
//
//
// 1 <= n, a, b, c <= 10^9
// 1 <= a * b * c <= 10^18
// It's guaranteed that the result will be in range [1, 2 * 10^9]
//
package binarysearch._Topic_CompareByFunc;


/*
    - binary search: O(log(2*10^9)), good!!
    - dp: O(n)
 */
public class UglyNumberIII {
    // O(log(2*10^9))
    public int nthUglyNumber(int n, int a, int b, int c){
        long l=1, r=2*1000000000;
        while(l<=r) {
            long mid = l + (r-l)/2;
            if(f(mid,a,b,c) >= n) {
                r=mid-1;
            } else if(f(mid,a,b,c) < n) {
                l=mid+1;
            }
        }
        return (int)l;
    }

    long f(long x, long a, long b, long c) {
        long cnt=0;
        cnt += x/a;
        cnt += x/b;
        cnt += x/c;
        cnt -= x/lcm(a, b);
        cnt -= x/lcm(b, c);
        cnt -= x/lcm(a, c);
        cnt += x/lcm(a,lcm(b,c));
        return cnt;
    }
    long gcd(long a, long b) {
        if(b==0) return a;
        return gcd(b, a%b);
    }
    long lcm(long a, long b) {
        return a*b/gcd(a,b);
    }


    // O(n), TLE...
//    public int nthUglyNumber(int n, int a, int b, int c) {
//        Queue<int[]> pq = new PriorityQueue<>((i, j)->i[1]-j[1]);
//        pq.add(new int[]{a,a});
//        pq.add(new int[]{b,b});
//        pq.add(new int[]{c,c});
//        int prev=0;
//        while(n>0) {
//            int[] cur = pq.remove();
//            pq.add(new int[]{cur[0], cur[0]+cur[1]});
//            if(cur[1]==prev) continue;
//            prev = cur[1];
//            n--;
//        }
//        return prev;
//    }

    public static void main(String[] as) {
//        Input: n = 3, a = 2, b = 3, c = 5
////Output: 4
////
//        Input: n = 4, a = 2, b = 3, c = 4
////Output: 6
////
//        Input: n = 5, a = 2, b = 11, c = 13
////Output: 10
////
//        Input: n = 1000000000, a = 2, b = 217983653, c = 336916467
////Output: 1999999984
////
        System.out.println(new UglyNumberIII().nthUglyNumber(3,2,3,5));
        System.out.println(new UglyNumberIII().nthUglyNumber(4,2,3,4));
        System.out.println(new UglyNumberIII().nthUglyNumber(5,2,11,13));
        System.out.println(new UglyNumberIII().nthUglyNumber(1000000000,2,217983653,336916467));
    }
}
