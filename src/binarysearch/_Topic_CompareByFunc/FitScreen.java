package binarysearch._Topic_CompareByFunc;


public class FitScreen {
    public int fitScreen(int screenWith, int screenHight, String words, int minFontSize, int maxFontSize, Font font) {
        int l=minFontSize, r=maxFontSize;
        while (l<=r) {
            int mid = l+(r-l)/2;
            if (fit(screenWith, screenHight, words, mid, font)) {
                l=mid+1;
            } else {
                r=mid-1;
            }
        }
        return r;
    }

    boolean fit(int screenWith, int screenHight, String string, int fontSize, Font font) {
        return false; //skip...
    }
}

interface Font {
    int getHeight(int fontSize);
    int getWidth(char c, int fontSize);
}

/*
   已知screen的高和宽，给你最小和最大的fontSize，要求给定一个string，将string用尽可能大的fontSize显示在screen里。
   已知两个API getHeight(int fontSize), getWidth(char c, int fontSize) ，可以得到每个character在不同fontSize下的高和宽。
   和面试官交流后，确认string可以拆分成几行显示在screen中

   思路：先提出暴力解法，然后用二分法优化

Provider：Qiaoqian Lin
def fitScreen(screen_width, sucreen_height, num_chars, font_sizes):
	def ok_or_not(width, height, screen_width, screen_height, num_chars):
		num_chars_per_line = screen_width // width
		num_chars_per_column = screen_height // height
		num_chars_can_fit = num_chars_per_line * num_chars_per_column
		return num_chars_can_fit >= num_chars

	lo, hi = 0, len(font_sizes) - 1
	while lo < hi:
		mid = (lo + hi + 1) // 2
		font_size = font_sizes[mid]
		width, height = API(font_size)
		size_ok = ok_or_not(width, height, screen_width, screen_height, num_chars)
		if not size_ok:
			hi = mid -1
		else:
			lo = mid
	return lo
屏幕超大，如何speedup?

给的API里面每个char的width是不同的，下述方法还需要额外计算一下
 */