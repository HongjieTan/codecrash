package binarysearch._Topic_CompareByFunc;

/**
 *     refs http://www.cnblogs.com/grandyang/p/8970057.html
 *
 *    1. DP:  O(k*k*n)   good!!!
 *    2. binarysearch: O(n*log(MaxDist))    good!!!
 *
 */
public class MinimizeMaxDistanceToGasStation {

    //  dp[i][j]:  minmax distance of stations[0...i] after added j stations
    //   dp[i][j] = min{ max{dp[i-1][j-k], len(i)/(k+1)} | k=0...K }
    public double minmaxGasDist_dp(int[] stations, int K) {
        int n=stations.length;
        double[][] dp = new double[n][K+1];

        for(int i=1;i<n;i++) dp[i][0]=Math.max(dp[i-1][0], stations[i]-stations[i-1]);

        for(int j=1;j<=K;j++) {
            for(int i=1;i<n;i++) {
                double min = Double.MAX_VALUE;
                for(int k=0;k<=j;k++) {
//                    double headMax = dp[i-1][j-k];
//                    double tailMax = (stations[i]-stations[i-1])*1d/(k+1);
//                    min = Math.min(min, Math.max(headMax, tailMax));
                    double maxD = Math.max(dp[i-1][j-k], (stations[i]-stations[i-1])*1d/(k+1));
                    min = Math.min(min, maxD);
                }
                dp[i][j]=min;
            }
        }
        return dp[n-1][K];
    }

    public double minmaxGasDist_binarysearch(int[] stations, int k) {
        double max = 0;
        for(int i=0;i<stations.length-1;i++) {
            max = Math.max(max,stations[i+1]-stations[i]);
        }
        double diff = Math.pow(10,-6);
        double l=0, r=max;
        while(l<=r) {
            double mid = l+(r-l)/2;
            if(isValid(stations, k, mid)) {
                r=mid-diff;
            } else {
                l=mid+diff;
            }
        }
        return l;
    }
    boolean isValid(int[] stations, int k, double maxD) { // 即在保证maxD的情况下，能否k刀内切完...
        for(int i=0;i<stations.length-1;i++) {
            double dist = (double)(stations[i+1]-stations[i]);
            int splits = dist>=maxD?(int) (dist/maxD):0;
            if(dist%maxD==0) --splits;
            k-=splits;
            if(k<0) return false;
        }
        return true;
    }

    // binarysearch: O( n * log(maxDist))
//    public double minmaxGasDist(int[] stations, int k) {
//        int n=stations.length;
//        double left=0, right=stations[n-1]-stations[0];
//        double diff =  Math.pow(10,-6);//10^-6
//        while(left<=right) {
//            double mid = left+(right-left)/2;
//            if(isValid(stations, k, mid)) {
//                right=mid-diff;
//            } else {
//                left=mid+diff;
//            }
//        }
//        return right;
//    }
//
//    boolean isValid(int[] stations, int k, double maxDist) {
//        // 能保证所有间距<=maxDist的最少新加stations数是否<=k
//        int count=0;
//        for(int i=0;i<stations.length-1;i++) {
//            int d = stations[i+1]-stations[i];
//            int need = (int) (d/maxDist);
//            if(need*maxDist==d) need--;
//            count+= need;
//        }
//        return count<=k;
//    }

    public static void main(String[] args) {
//        [5,9,30,48,52,54,59,85,88,96]
//        4
//        Output
//        13.00
//        Expected
//        10.50

//        [9,34,46,51,66,81,83,84,85,91]
//        8
//        Output
//        5.00
//        Expected
//        6.25

        int[] stations = new int[]{9,34,46,51,66,81,83,84,85,91};
        int K=8;
        System.out.println(new MinimizeMaxDistanceToGasStation().minmaxGasDist_binarysearch(stations,K));
        System.out.println(new MinimizeMaxDistanceToGasStation().minmaxGasDist_dp(stations,K));
    }
}


/*
On a horizontal number line, we have gas stations at positions stations[0], stations[1], ..., stations[N-1], where N = stations.length.

Now, we add K more gas stations so that D, the maximum distance between adjacent gas stations, is minimized.

Return the smallest possible value of D.

1.stations.length will be an integer in range [10, 2000].
2.stations[i] will be an integer in range [0, 10^8].
3.K will be an integer in range [1, 10^6].
4.Answers within 10^-6 of the true value will be accepted as correct.

Example
Given: stations = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10], K = 9
Return: 0.500000
*/
/*
refs
X. DP
http://hehejun.blogspot.com/2018/02/leetcodeminimize-max-distance-to-gas.html
这道题和Split Array Largest Sum十分类似。首先我们也可以考虑区间DP的解法。对于前i个interval(i + 1个加油站)，我们额外增加j个加油站，相邻加油站间距的最大值为x，那么在在所有可能中x的最小值我们用DP[i][j]表示。我们有递推公式：

dp[i][j] = min(max(dp[i - 1][j - k], (stations[i] - stations[i - 1]) / (k + 1)))
base case: dp[i][0] = max(stations[1] - stations[0], ..., stations[i] - stations[i - 1])
公式中的(stations[i] - stations[i - 1]) / (k + 1)可以这么理解: 把一个interval用k个节点分为k + 1的子区间，那么minimize子区间最大值的方法就是吧interval等分。假设输入为n个加油站的位置，我们额外增加k个，算法时间复杂度和空间复杂度均为O(n * k)。代码如下：
    double minmaxGasDist(vector<int>& stations, int K) {
        int len = stations.size();
        //dp[i][j] represnets minimum max distance when adding j new gas stations to first i intervals
        //dp[i][j] = min(max(dp[i - 1][j - k], (stations[i] - stations[i - 1]) / (k + 1)))
        //base case: dp[i][0] = max(stations[0], ..., stations[i - 1]), dp[0][j] = 0
        vector<vector<double>> dp(len, vector<double>(K + 1, 0));
        for (int i = 1; i < len; ++i)dp[i][0] = max(dp[i - 1][0], static_cast<double>(stations[i] - stations[i - 1]));
        for (int i = 1; i < len; ++i)
        {
            for (int j = 1; j <= K; ++j)
            {
                dp[i][j] = numeric_limits<double>::max();
                for (int k = 0; k <= j; ++k)
                {
                    dp[i][j] = min(max(dp[i - 1][j - k], (stations[i] - stations[i - 1]) / (k + 1.0)), dp[i][j]);
                }
            }
        }
        return dp[len - 1][K];
    }
*/