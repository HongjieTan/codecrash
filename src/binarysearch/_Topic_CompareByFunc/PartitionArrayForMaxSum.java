//Given an integer array A, you partition the array into (contiguous) subarrays of length at most K.
// After partitioning, each subarray has their values changed to become the maximum value of that subarray.
//
// Return the largest sum of the given array after partitioning.
//
//
//
// Example 1:
//
//
//Input: A = [1,15,7,9,2,5,10], K = 3
//Output: 84
//Explanation: A becomes [15,15,15,9,10,10,10]
//
//
//
// Note:
//
//
// 1 <= K <= A.length <= 500
// 0 <= A[i] <= 10^6
//
package binarysearch._Topic_CompareByFunc;


/*
   dp: good...
   bs: not work here!
 */
public class PartitionArrayForMaxSum {

    public int maxSumAfterPartitioning(int[] A, int K) {
        int n = A.length, dp[] = new int[n+1];
        for(int i=1;i<=n;i++) {
            int curmax = A[i-1];
            for(int len=1;len<=K && i-len>=0;len++) {
                curmax = Math.max(curmax, A[i-len]);
                dp[i] = Math.max(dp[i], dp[i-len]+len*curmax);
            }
        }
        return dp[n];
    }

    /*
    def maxSumAfterPartitioning(self, A, K):
        n = len(A)
        dp = [0]*(n+1)
        for i in range(1, n+1):
            curmax = A[i-1]
            for len in range(1, min(K, i)+1):
                curmax = max(curmax, A[i-len])
                dp[i] = max(dp[i], dp[i-len]+curmax*len)
        return dp[n]
    */

    // my first dp solution: O(N*K*K)
//    public int maxSumAfterPartitioning(int[] A, int K) {
//        int n = A.length;
//        int[][] memo = new int[n][n];
//        for (int i = 0; i < n; i++) {
//            memo[i][i] = A[i];
//            for (int j = i+1; j < n; j++) {
//                memo[i][j] = Math.max(memo[i][j-1], A[j]);
//            }
//        }
//
//        int[][] dp = new int[n+1][K+1];
//        for(int i=1; i<=n; i++) {
//            for (int j=0; j<=K; j++) {
//                for (int len=1;len<=K;len++) {
//                    if (i-len<0) continue;
//                    int curmax = memo[i-len][i-1];
//                    dp[i][j] = Math.max(dp[i][j], dp[i-len][j] + curmax*len);
//                }
//            }
//        }
//        return dp[n][K];
//    }


    public static void main(String[] a) {
        System.out.println(new PartitionArrayForMaxSum().maxSumAfterPartitioning(new int[]{1,15,7,9,2,5,10}, 3));
    }



}


/*
lee's solution:

Explanation
dp[i] record the maximum sum we can get considering A[0] ~ A[i]
To get dp[i], we will try to change k last numbers separately to the maximum of them,
where k = 1 to k = K.

Complexity
Time O(NK), Space O(N)


Java:
    public int maxSumAfterPartitioning(int[] A, int K) {
        int N = A.length, dp[] = new int[N];
        for (int i = 0; i < N; ++i) {
            int curMax = 0;
            for (int k = 1; k <= K && i - k + 1 >= 0; ++k) {
                curMax = Math.max(curMax, A[i - k + 1]);
                dp[i] = Math.max(dp[i], (i >= k ? dp[i - k] : 0) + curMax * k);
            }
        }
        return dp[N - 1];
    }
C++:
    int maxSumAfterPartitioning(vector<int>& A, int K) {
        int N = A.size();
        vector<int> dp(N);
        for (int i = 0; i < N; ++i) {
            int curMax = 0;
            for (int k = 1; k <= K && i - k + 1 >= 0; ++k) {
                curMax = max(curMax, A[i - k + 1]);
                dp[i] = max(dp[i], (i >= k ? dp[i - k] : 0) + curMax * k);
            }
        }
        return dp[N - 1];
    }
Python:

    def maxSumAfterPartitioning(self, A, K):
        N = len(A)
        dp = [0] * (N + 1)
        for i in xrange(N):
            curMax = 0
            for k in xrange(1, min(K, i + 1) + 1):
                curMax = max(curMax, A[i - k + 1])
                dp[i] = max(dp[i], dp[i - k] + curMax * k)
        return dp[N - 1]



 */