/*
Given a m x n matrix mat and an integer threshold.
Return the maximum side-length of a square with a sum less than or equal to threshold or return 0 if there is no such square.

Example 1:
https://assets.leetcode.com/uploads/2019/12/05/e1.png

Input: mat = [[1,1,3,2,4,3,2],[1,1,3,2,4,3,2],[1,1,3,2,4,3,2]], threshold = 4
Output: 2
Explanation: The maximum side length of square with sum less than 4 is 2 as shown.
Example 2:

Input: mat = [[2,2,2,2,2],[2,2,2,2,2],[2,2,2,2,2],[2,2,2,2,2],[2,2,2,2,2]], threshold = 1
Output: 0
Example 3:

Input: mat = [[1,1,1,1],[1,0,0,0],[1,0,0,0],[1,0,0,0]], threshold = 6
Output: 3
Example 4:

Input: mat = [[18,70],[61,1],[25,85],[14,40],[11,96],[97,96],[63,45]], threshold = 40184
Output: 2


Constraints:

1 <= m, n <= 300
m == mat.length
n == mat[i].length
0 <= mat[i][j] <= 10000
0 <= threshold <= 10^5
*/
package binarysearch._Topic_CompareByFunc;


/*
    #prefixsum #bianrysearch

    - prefixsum + binarysearch:  O(m * n * log(min(m,n))), good!
    - prefixsum only:  O(m * n), good!
    - brute force:  O(m * n * min(m,n))


 */
public class MaxSideLengthOfSquareWithSumLessThanOrEqualToThreshold {

    // O(m * n)
    public int maxSideLength_v3(int[][] mat, int threshold) {
        int n=mat.length, m=mat[0].length;
        int[][] cum = new int[n+1][m+1];
        int max = 0;
        for(int i=0;i<n;i++) {
            for(int j=0;j<m;j++) {
                cum[i+1][j+1] = cum[i][j+1] + cum[i+1][j] - cum[i][j] + mat[i][j];
                if (i-max>=0 && j-max>=0 &&
                        cum[i+1][j+1]-cum[i-max][j+1]-cum[i+1][j-max]+cum[i-max][j-max]<=threshold) {
                    max++;
                }
            }
        }
        return max;
    }

    // O(m * n * log(min(m,n))), binarysearch, good!!
    int n, m;
    public int maxSideLength_v2(int[][] mat, int threshold) {
        n=mat.length;
        m=mat[0].length;
        int[][] cum = new int[n+1][m+1];
        for(int i=0;i<n;i++) {
            for(int j=0;j<m;j++) {
                cum[i+1][j+1] = cum[i][j+1] + cum[i+1][j] - cum[i][j] + mat[i][j];
            }
        }

        int l=1, r=Math.min(m,n);
        while(l<=r) {
            int mid = l+(r-l)/2;
            if(ok(cum, mid, threshold)) {
                l = mid+1;
            } else {
                r = mid-1;
            }
        }
        return r;
    }
    boolean ok(int[][] cum, int side, int threshold){
        for(int i=0;i+side<=n;i++) {
            for(int j=0;j+side<=m;j++) {
                if(cum[i+side][j+side]-cum[i][j+side]-cum[i+side][j]+cum[i][j]<=threshold) {
                    return true;
                }
            }
        }
        return false;
    }


    // O(m * n * min(m,n))
    public int maxSideLength_v1(int[][] mat, int threshold) {
        int n=mat.length, m=mat[0].length;
        int[][] cum = new int[n+1][m+1];
        for(int i=0;i<n;i++) {
            for(int j=0;j<m;j++) {
                cum[i+1][j+1] = cum[i][j+1] + cum[i+1][j] - cum[i][j] + mat[i][j];
            }
        }


        int ret = 0;
        for(int side=1; side<=Math.min(m,n);side++) {
            for(int i=0;i<n;i++) {
                for(int j=0;j<m;j++) {
                    int ni = i+side-1, nj = j+side-1;
                    if(ni>=n || nj>=m) continue;
                    if(cum[ni+1][nj+1] - cum[i][nj+1] - cum[ni+1][j] + cum[i][j] <= threshold) {
                        ret = side;
                    }
                }
            }
            if(side!=ret) return ret;
        }
        return ret;
    }



}
