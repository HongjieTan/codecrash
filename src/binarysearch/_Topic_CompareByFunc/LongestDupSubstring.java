// Given a string S, consider all duplicated substrings: (contiguous) substrings of S that occur 2 or more times.
// (The occurrences may overlap.)
//
// Return any duplicated substring that has the longest possible length.
// (If S does not have a duplicated substring, the answer is "".)
//
//
//
// Example 1:
//
//
//Input: "banana"
//Output: "ana"
//
//
// Example 2:
//
//
//Input: "abcd"
//Output: ""
//
//
//
//
// Note:
//
//
// 2 <= S.length <= 10^5
// S consists of lowercase English letters.
//
// hint1: Binary search for the length of the answer. (If there's an answer of length 10, then there are answers of length 9, 8, 7, ...)
// hint2: To check whether an answer of length K exists, we can use Rabin-Karp 's algorithm.
// tags: hashtable, binary search...

package binarysearch._Topic_CompareByFunc;


import java.util.*;

public class LongestDupSubstring {
    // binarysearch + rolling hash, O(nlogn)
    String ret;
    public String longestDupSubstring(String S) {
        int n = S.length();
        ret = "";
        int l=1, r=n;
        while(l<=r) {
            int mid=l+(r-l)/2;
            if(ok(S, mid)) {
                l=mid+1;
            } else {
                r=mid-1;
            }
        }
        return ret;
    }

    boolean ok(String s, int len) {
        Map<Integer, List<Integer>> seen = new HashMap<>();
        int  d = 128, hash = 0, h = 1, n = s.length();
        int mod = 10000000;
        for(int i=0;i<len-1;i++) h=(h*d)%mod;
        for(int i=0;i<len;i++) hash=(hash*d+s.charAt(i))%mod;

        for(int i=0;i+len-1<n;i++) {
            if(seen.containsKey(hash)) {
                for (int start: seen.get(hash)) {
                    if(s.substring(start, start+len).equals(s.substring(i,i+len))){
                        ret = s.substring(i,i+len);
                        return true;
                    }
                }
                seen.get(hash).add(i);
            } else {
                seen.put(hash, new ArrayList<>());
                seen.get(hash).add(i);
            }

            if(i+len<n) hash = ((hash-(s.charAt(i)*h)%mod)*d+s.charAt(i+len))%mod; // (s.charAt(i)*h)%mod 这一步要mod？
            if(hash<0) hash+=mod;
        }
        return false;
    }

    // suffix array, todo

    public static void main(String[] as) {
        LongestDupSubstring su = new LongestDupSubstring();
        String S = "ababdaebdabedeabbdddbcebaccececbccccebbcaaabaadcadccddaedaacaeddddeceedeaabbbbcbacdaeeebaabdabdbaebadcbdebaaeddcadebedeabbbcbeadbaacdebceebceeccddbeacdcecbcdbceedaeebdaeeabccccbcccbceabedaedaacdbbdbadcdbdddddcdebbcdbcabbebbeabbdccccbaaccbbcecacaebebecdcdcecdeaccccccdbbdebaaaaaaeaaeecdecedcbabedbabdedbaebeedcecebabedbceecacbdecabcebdcbecedccaeaaadbababdccedebeccecaddeabaebbeeccabeddedbeaadbcdceddceccecddbdbeeddabeddadaaaadbeedbeeeaaaeaadaebdacbdcaaabbacacccddbeaacebeeaabaadcabdbaadeaccaecbeaaabccddabbeacdecadebaecccbabeaceccaaeddedcaecddaacebcaecebebebadaceadcaccdeebbcdebcedaeaedacbeecceeebbdbdbaadeeecabdebbaaebdddeeddabcbaaebeabbbcaaeecddecbbbebecdbbbaecceeaabeeedcdecdcaeacabdcbcedcbbcaeeebaabdbaadcebbccbedbabeaddaecdbdbdccceeccaccbdcdadcccceebdabccaebcddaeeecddddacdbdbeebdabecdaeaadbadbebecbcacbbceeabbceecaabdcabebbcdecedbacbcccddcecccecbacddbeddbbbadcbdadbecceebddeacbeeabcdbbaabeabdbbbcaeeadddaeccbcdabceeabaacbeacdcbdceebeaebcceeebdcdcbeaaeeeadabbecdbadbebbecdceaeeecaaaedeceaddedbedbedbddbcbabeadddeccdaadaaeaeeadebbeabcabbdebabeedeeeccadcddaebbedadcdaebabbecedebadbdeacecdcaebcbdababcecaecbcbcdadacaebdedecaadbaaeeebcbeeedaaebbabbeeadadbacdedcbabdaabddccedbeacbecbcccdeaeeabcaeccdaaaddcdecadddabcaedccbdbbccecacbcdecbdcdcbabbeaacddaeeaabccebaaaebacebdcdcbbbdabadeebbdccabcacaacccccbadbdebecdaccabbecdabdbdaebeeadaeecbadedaebcaedeedcaacabaccbbdaccedaedddacbbbdbcaeedbcbecccdbdeddcdadacccdbcdccebdebeaeacecaaadccbbaaddbeebcbadceaebeccecabdadccddbbcbaebeaeadacaebcbbbdbcdaeadbcbdcedebabbaababaacedcbcbceaaabadbdcddadecdcebeeabaadceecaeccdeeabdbabebdcedceaeddaecedcdbccbbedbeecabaecdbba";
        System.out.println(su.longestDupSubstring(S));
    }
}



/*
by lee:
Intuition
Suffix array is typical solution for this problem.
The fastest way is to copy a template form the Internet.
The code will be quite long.
Here I want to share a binary search solution.

Explanation
Binary search the length of longest duplicate substring and call the help function test(L).
test(L) slide a window of length L,
rolling hash the string in this window,
record the seen string in a hashset,
and try to find duplicated string.

I give it a big mod for rolling hash and it should be enough for this problem.
Actually there could be hash collision.
One solution is to have two different mod for hash.
Or we can use a hashmap to record the index of string.


Complexity
Binary Search in range 1 and N, so it's O(logN)
Rolling hash O(N)
Overall O(NlogN)
SpaceO(N)


Python:

    def longestDupSubstring(self, S):
        A = [ord(c) - ord('a') for c in S]
        mod = 2**63 - 1

        def test(L):
            p = pow(26, L, mod)
            cur = reduce(lambda x, y: (x * 26 + y) % mod, A[:L], 0)
            seen = {cur}
            for i in xrange(L, len(S)):
                cur = (cur * 26 + A[i] - A[i - L] * p) % mod
                if cur in seen: return i - L + 1
                seen.add(cur)
        res, lo, hi = 0, 0, len(S)
        while lo < hi:
            mi = (lo + hi + 1) / 2
            pos = test(mi)
            if pos:
                lo = mi
                res = pos
            else:
                hi = mi - 1
        return S[res:res + lo]


by java:
There are two key concepts here. First, in order to discover the max length, we can do binary search for it.
Binary search works because if we find a certain length L doesn't work, we can quickly eliminate anything longer than it.
And if a certain length does work, we can quickly eliminate everything that is shorter than it.
This is the monotonic increasing/decreasing property that all binary search approaches require to get at the solution.
Basically, true on L sets a floor, and false on L sets a ceiling.

Second, when you want to try a particular length L, you need to find any duplicate substrings of length L.
We cannot afford to compare every substring to every other substring. So the key is to calculate a rolling window hash of the substring that is in the window.
Now, it is tempting to just calculate this rolling window hash as the sum of characters inside.
However, this causes TLE because many of the corner cases try to trick you by ensuring the same set of character quantities inside the rolling window as you slide it through the string.
So what we particularly need is a sequence-dependent rolling hash signature.
Instead of computing this as I go, I instead calculate the cumulative hash from beginning of string to each end character.
Then, later, to get the sequence-dependent hash signature of a substring,
I calculate it on demand. I use a HashSet to keep track of the signatures we have seen,
and I define a CompressedString type which is nothing more than a substring representation where the hashcode can be computed on demand.
Since hash collisions are theoretically possible, I also implement the equals() method to check for substring equality.

Hopefully my description is enough for you to try it, but here is working code implementing the idea.

class Solution {

    // Represents a substring and its sequence-dependent hash signature.
    private class CompressedString {
        int leftIncl, rightIncl;
        CompressedString(int li, int ri) { leftIncl=li; rightIncl=ri; }
        // In case of equal signatures, we still need to double-check
        // for actual substring equality, for rare false collisions.
        @Override public boolean equals(Object obj) {
            CompressedString that = (CompressedString)obj;
            int len = rightIncl-leftIncl+1;
            for(int offset = 0; offset < len; offset++) {
               if (S.charAt(this.leftIncl+offset) != S.charAt(that.leftIncl+offset)) {
                   return false;
               }
            }
            return true;
        }
        // Derive the sequence-dependent rolling hash signature of the substring.
        @Override public int hashCode() {
            // To use the cumSum to get our hash, we have to subtract all the
            // contribution to the cumSum from before the substring begins,
            // which would have grown by 26^length.
            long leftSumExcl = leftIncl == 0 ? 0 : cumSum[leftIncl-1];
            leftSumExcl = (leftSumExcl * pow26[rightIncl-leftIncl+1]) % 1000003L;
            return (int)((cumSum[rightIncl] - leftSumExcl + 1000003L) % 1000003L);
        }
    }

    private String S;
    private long[] cumSum; // base-26 sum of the substring [0,idx]
    private long[] pow26;  // just powers of 26 pre-computed

    private void precomputations() {
        int curr = 0;
        final int Slen = S.length();
        // Compute base-26 sum of every substring from the beginning to end (idx).
        for(int idx = 0; idx < Slen; idx++) {
            int val = S.charAt(idx)-'a';
            curr = (curr*26 + val) % 1000003;
            cumSum[idx] = curr;
        }
        pow26 = new long[Slen];
        pow26[0] = 1;
        for(int pow=1; pow < Slen; pow++) {
            pow26[pow] = (pow26[pow-1]*26L) % 1000003L;
        }
    }

    public String longestDupSubstring(String S) {
        this.S = S;
        this.cumSum = new long[S.length()];
        precomputations();
        int Lmin = 0;
        int Lmax = S.length()-1;
        String bestDupSubstring = null;

        // Do the binary search on the substring length.
        // Lmin and Lmax are the range we are searching, but the
        // ideal solution may already have been found outside this range.
        while (Lmin <= Lmax) {
            int Lmid = (Lmin+Lmax)/2;
            String possible = tryfind(Lmid);
            if (possible == null) {
                Lmax = Lmid-1;
            }
            else {
                if (bestDupSubstring == null || possible.length() > bestDupSubstring.length()) {
                    bestDupSubstring = possible;
                }
                Lmin = Lmid+1;
            }
        }
        return bestDupSubstring == null ? "" : bestDupSubstring;
    }

    // See if we can find any duplicate substring of length L.
    private String tryfind(int L) {
        if (L == 0) { return null; }
        final int Slen = S.length();
        // Save the substring signatures as we see them.
        Set<CompressedString> have = new HashSet<>();
        // Slide over every substring of length L.
        for(int leftIncl = 0; leftIncl <= Slen-L; leftIncl++) {
            int rightIncl = L - 1 + leftIncl;
            CompressedString curr = new CompressedString(leftIncl, rightIncl);
            if (have.contains(curr)) {
                return S.substring(leftIncl, rightIncl+1);
            }
            have.add(curr);
        }
        return null;
    }
}
*/

/*
by uwi:
class Solution {
    public String longestDupSubstring(String S) {
        int[] sa = suffixArray(S.toCharArray(), 26);
        int n = S.length();
        int[] lcp = buildLCP(S.toCharArray(), sa);
        int[] isa = new int[n];
        for(int i = 0;i < n;i++)isa[sa[i]] = i;
        int max = 0;
        int arg = -1;
        for(int i = 1;i < n;i++){
            if(lcp[i] > max){
                max = lcp[i];
                arg = i;
            }
        }
        if(arg == -1)return "";
        return new String(S.toCharArray(), sa[arg-1], max);
    }

    public int[] buildLCP(char[] str, int[] sa)
    {
        int n = str.length;
        int h = 0;
        int[] lcp = new int[n];
        int[] isa = new int[n];
        for(int i = 0;i < n;i++)isa[sa[i]] = i;
        for(int i = 0;i < n;i++){
            if(isa[i] > 0){
                for(int j = sa[isa[i]-1]; j+h<n && i+h<n && str[j+h] == str[i+h]; h++);
                lcp[isa[i]] = h;
            }else{
                lcp[isa[i]] = 0;
            }
            if(h > 0)h--;
        }
        return lcp;
    }


    public int[] suffixArray(char[] str, int W)
    {
        int n = str.length;
        if(n <= 1)return new int[n];
        int[] sa = new int[n];
        int[] s = new int[n+3];
        for(int i = 0;i < n;i++)s[i] = str[i] - 'a' + 1;
        suffixArray(s, sa, n, W+1);
        return sa;
    }

    public void suffixArray(int[] s, int[] sa, int n, int K)
    {
        int n0 = (n+2)/3, n1 = (n+1)/3, n2 = n/3, n02 = n0 + n2;
        int[] s12 = new int[n02+3];
        int[] sa12 = new int[n02+3];
        int[] s0 = new int[n0];
        int[] sa0 = new int[n0];

        // generate positions of mod 1 and mod 2 suffixes
        // the "+(n0-n1)" adds a dummy mod 1 suffix if n%3 == 1
        int sup = n+(n0-n1);
        for(int i = 0, j = 0;i < sup;i+=3){
            if(i+1 < sup)s12[j++] = i+1;
            if(i+2 < sup)s12[j++] = i+2;
        }

        // lsb radix sort the mod 1 and mod 2 triples
        radixPass(s12, sa12, s, 2, n02, K);
        radixPass(sa12, s12, s, 1, n02, K);
        radixPass(s12, sa12, s, 0, n02, K);

        // find lexicographic names of triples
        int name = 0, c0 = -1, c1 = -1, c2 = -1;
        for(int i = 0;i < n02;i++){
            if(s[sa12[i]] != c0 || s[sa12[i]+1] != c1 || s[sa12[i]+2] != c2){
                name++;
                c0 = s[sa12[i]];
                c1 = s[sa12[i]+1];
                c2 = s[sa12[i]+2];
            }
            if(sa12[i] % 3 == 1){
                s12[sa12[i]/3] = name; // left half
            }else{
                s12[sa12[i]/3 + n0] = name; // right half
            }
        }

        // recurse if names are not yet unique
        if(name < n02){
            suffixArray(s12, sa12, n02, name);
            // store unique names in s12 using the suffix array
            for(int i = 0;i < n02;i++)s12[sa12[i]] = i+1;
        }else{
            // generate the suffix array of s12 directly
            for(int i = 0;i < n02;i++)sa12[s12[i]-1] = i;
        }

        // stably sort the mod 0 suffixes from sa12 by their first character
        for(int i = 0, j = 0;i < n02;i++){
            if(sa12[i] < n0)s0[j++] = 3 * sa12[i];
        }
        radixPass(s0, sa0, s, 0, n0, K);

        // merge sorted sa0 suffixes and sorted sa12 suffixes
        for(int p = 0, t = n0-n1, k = 0;k < n;k++){
            int i = sa12[t] < n0 ? sa12[t] * 3 + 1 : (sa12[t] - n0) * 3 + 2; // pos of current offset 12 suffix
            int j = sa0[p]; // pos of current offset 0 suffix
            if(sa12[t] < n0 ?
                    (s[i] < s[j] || s[i] == s[j] && s12[sa12[t]+n0] <= s12[j/3]) :
                    (s[i] < s[j] || s[i] == s[j] && (s[i+1] < s[j+1] || s[i+1] == s[j+1] && s12[sa12[t]-n0+1] <= s12[j/3+n0]))
                    ){
                // suffix from a12 is smaller
                sa[k] = i;
                t++;
                if(t == n02){
                    // done --- only sa0 suffixes left
                    for(k++;p < n0;p++,k++)sa[k] = sa0[p];
                }
            }else{
                // suffix from sa0 is smaller
                sa[k] = j; p++;
                if(p == n0){
                    // done --- only sa12 suffixes left
                    for(k++; t < n02;t++,k++)sa[k] = sa12[t] < n0 ? sa12[t] * 3 + 1 : (sa12[t] - n0) * 3 + 2;
                }
            }
        }
    }

    public void radixPass(int[] a, int[] b, int[] r, int l, int n, int K)
    {
        int[] c = new int[K+1]; // counter array
        for(int i = 0;i < n;i++)c[r[l+a[i]]]++; // count occurrences
        for(int i = 0, sum = 0;i <= K;i++){ // exclusive prefix sums
            int t = c[i]; c[i] = sum; sum += t;
        }
        for(int i = 0;i < n;i++)b[c[r[l+a[i]]]++] = a[i];
    }

}
*/