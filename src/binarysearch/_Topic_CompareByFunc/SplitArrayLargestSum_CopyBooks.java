/*
Description
Given n books and the ith book has A[i] pages. You are given k people to copy the n books.
n books list in a row and each person can claim a continous range of the n books.
For example one copier can copy the books from ith to jth continously, but he can not copy the 1st book, 2nd book and 4th book (without 3rd book).
They start copying books at the same time and they all cost 1 minute to copy 1 page of a book.
What's the best strategy to assign books so that the slowest copier can finish at earliest time?

Example
Given array A = [3,2,4], k = 2.

Return 5( First person spends 5 minutes to copy book 1 and book 2 and second person spends 4 minutes to copy book 3. )

Challenge
时间复杂度 O(nk)
 */

/*
Given an array which consists of non-negative integers and an integer m, you can split the array into m non-empty continuous subarrays.
Write an algorithm to minimize the largest sum among these m subarrays.

Note:
If n is the length of array, assume the following constraints are satisfied:

1 ≤ n ≤ 1000
1 ≤ m ≤ min(50, n)
Examples:

Input:
nums = [7,2,5,10,8]
m = 2

Output:
18

Explanation:
There are four ways to split nums into two subarrays.
The best way is to split it into [7,2,5] and [10,8],
where the largest sum among the two subarrays is only 18.
 */

package binarysearch._Topic_CompareByFunc;

/**
 *  - DP:    O(n^2 * m)
 *  - Binary Search + greedy:    O(n * log(Sum(nums)))
 */
public class SplitArrayLargestSum_CopyBooks {
    // dp[i][j]: min max sum of nums[0...i] into j subarrays
    //   dp[i][j] = min{ max{dp[k][j-1], sum(nums[k+1:i])} },   k=0...i(或者k=j-2...i-1)
    public int splitArray_dp_x2(int[] nums, int m) {
        int n=nums.length;
        long[][] dp = new long[n][m+1];

        long[] presum = new long[n];
        presum[0]=nums[0];
        for(int i=1;i<n;i++) presum[i]+=nums[i]+presum[i-1];

        for(int i=0;i<n;i++) {
            for(int j=1;j<=m;j++) {
                if(j==1) dp[i][j] = presum[i];
                else {
                    long min=Integer.MAX_VALUE;
                    for(int k=j-2;k<=i-1;k++) {
                        long maxSum = Math.max(dp[k][j-1], presum[i]-presum[k]);
                        min = Math.min(min, maxSum);
                    }
                    dp[i][j] = min;
                }
            }
        }
        return (int)(dp[n-1][m]);
    }

    // dp: O(n^2 * m), calculate range sum using prefix sum...
    /*
        dp[i][j]  maxSum of j splits of arr[0...i-1],  i:1-n j:1-m

        dp[i][j] = min{ max{dp[k][j-1], sum(arr[k+1:i-1])}, k=j-1...i-1 }

        i=1, dp[i][j]=arr[0]
        j=1, dp[i][j]=sum(arr[0...i-1])

     */
    public int splitArray_dp(int[] nums, int m) {
        int n = nums.length;
        int[] presum = new int[n];
        presum[0] = nums[0];
        for (int i = 1; i < n; i++) presum[i]=presum[i-1]+nums[i];
        int[][] dp = new int[n+1][m+1];
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= m; j++) {
                if (j==1) dp[i][j]=presum[i-1];
                else {
                    dp[i][j]=Integer.MAX_VALUE;
                    for (int k = j-1; k <=i-1 ; k++) {
                        int maxSum = Math.max(dp[k][j-1], presum[i-1]-presum[k-1]);
                        dp[i][j] = Math.min(maxSum, dp[i][j]);
                    }
                }
            }
        }
        return dp[n][m];
    }

    /*
       binary search: O(n * log(Sum(nums)))
     */
    public int splitArray_bs(int[] nums, int m) {
        int max=-1, sum=0;
        for(int x: nums) {
            max = Math.max(max, x);
            sum+=x;
        }
        int l=max, r=sum; // search range max -> sum
        while (l<=r) {
            int mid=l+(r-l)/2;
            if (canSplit(nums, mid, m)) r=mid-1;
            else l=mid+1;
        }
        return l;
    }

    boolean canSplit(int[] nums, int maxSum, int m) {
        int splits = 1, sum=0;
        for (int x: nums) {
            if (sum+x>maxSum) {
                sum = 0;
                splits++;
                if (splits>m) return false;
            }
            sum += x;
        }
        return true;
    }

//    public int copyBooks(int[] pages, int k) {
//        // write your code here
//        return -1;
//    }

    public static void main(String[] args) {
        int[] arr = new int[]{1,4,4};
        int k=3;
        System.out.println(new SplitArrayLargestSum_CopyBooks().splitArray_bs(arr, k));
        System.out.println(new SplitArrayLargestSum_CopyBooks().splitArray_dp(arr, k));
        System.out.println(new SplitArrayLargestSum_CopyBooks().splitArray_dp_x2(arr, k));
    }
}

