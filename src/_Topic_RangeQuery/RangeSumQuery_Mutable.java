package _Topic_RangeQuery;

/**
 * Created by thj on 2018/10/9.
 *
 *
 * consider 3 cases:
 *  1. number of query is large, number of update is few  -> query should be fast  -> pre sum: query O(1), update O(n)
 *  2. number of query is few, number of update is large  -> update should be fast -> brute force: query O(n), update O(1)
 *  3. number of query and update is distributed evenly   -> both should be fast   -> segment tree: query O(logn), update O(logn)
 *      - segment tree (the idea is simpler...) (the choice!)
 *          Time: O(N)build, O(logN)search, O(logN) update
 *      - binary indexed tree (the code is simpler...)
 *          Time: O(NlogN)build, O(logN) search, O(logN) update
 * 　　　　　　
 */
public class RangeSumQuery_Mutable {

    // segment tree (tree node version): code is cleaner...
    class NumArray_v2 {
        class STNode {
            int start,end,sum;
            STNode left,right;
        }

        STNode segTree;

        public NumArray_v2(int[] nums) {
            if (nums==null||nums.length==0) return;
            segTree = buildSegTree(0, nums.length-1, nums);
        }

        private STNode buildSegTree(int start, int end, int[] nums) {
            if (start == end) {
                STNode node = new STNode();
                node.start = start;
                node.end = end;
                node.sum = nums[start];
                return node;
            } else {
                int mid = start + (end-start)/2;
                STNode node = new STNode();
                node.start = start;
                node.end = end;
                node.left = buildSegTree(start, mid, nums);
                node.right = buildSegTree(mid+1, end, nums);
                node.sum = node.left.sum + node.right.sum;
                return node;
            }
        }

        public void update(int i, int val) {
            update(segTree, i, val);
        }

        private void update(STNode node, int i, int val) {
//        if (node == null) return;
            if (node.start == node.end ) {
                node.sum = val;
            } else {
                int mid = node.start + (node.end - node.start)/2;
                if(i<=mid) update(node.left, i, val);
                else update(node.right, i, val);
                node.sum = node.left.sum + node.right.sum;
            }
        }

        public int sumRange(int i, int j) {
            return sumRange(segTree, i, j);
        }

        private int sumRange(STNode node, int i, int j) {
            if ( i <= node.start && node.end <= j) return node.sum;
            else if (  j < node.start || i>node.end ) return 0;
            else return sumRange(node.left, i, j)+sumRange(node.right, i, j);
        }

    }


    // binary indexed tree: easiest to implement (a little hard to understand at first...)
//   https://www.topcoder.com/community/competitive-programming/tutorials/binary-indexed-trees/
//
//  To isolate last bit of num:
//   1. num & (num ^ (num-1)):  good!!! -> easy to understand!!!
//      num: xxx1000,  num-1:xxx0111, num ^ (num-1):0001111, num & (num ^ (num-1)):0001000
//
//   2. num & -num
//      -num = (a1b)¯ + 1 = a¯0b¯ + 1 = a¯0(0...0)¯ + 1 = a¯0(1...1) + 1 = a¯1(0...0) = a¯1b. (b means: 0...0)
//      a¯1b & a1b = (0...0)1(0...0)
//      so: num & -num = (0...0)1(0...0)
    static class NumArray {

        int[] BIT;
        int[] originNums;

        public NumArray(int[] nums) {
            if (nums==null || nums.length==0) return;
            BIT = new int[nums.length+1];
            this.originNums = new int[nums.length+1];

            for (int i = 0; i < nums.length; i++) {
                update(i, nums[i]);
            }
        }

        public void update(int i, int val) {
            i++; // convert index to 1-based

            int diff = val-this.originNums[i];
            this.originNums[i] = val;
            while (i<BIT.length) {
                BIT[i] += diff;
                i += isolateLastBit(i);
            }
        }

        public int sumRange(int i, int j) {
            return cumulativeSum(j) - cumulativeSum(i-1);
        }

        private int cumulativeSum(int i) {
            int sum = 0;
            i++; // convert index to 1-based
            while(i>0) {
                sum += BIT[i];
                i -= isolateLastBit(i);
            }
            return sum;
        }

        private int isolateLastBit(int x) {
            return x & (x^(x-1));
//        return x & -x;
        }


    }






    // segment tree (array version)
    class NumArray_v1 {


        int[] segTree;
        int[] numArr;
        int n;

        public NumArray_v1(int[] nums) {
            //  seg tree => full tree => leaves: n, internal nodes: n-1 !
            //  " In a Full Binary, number of leaf nodes is number of internal nodes plus 1 "
            if (nums==null || nums.length==0) return;
            numArr = nums;
            n = nums.length;

            // Allocate memory for segment tree
            int height = (int) (Math.ceil(Math.log(n) / Math.log(2)));
            int max_size = 2 * (int) Math.pow(2, height) - 1;
            segTree = new int[max_size];

            buildSegTree(nums, 0, n-1, 0);
        }

        private int buildSegTree(int[] nums, int start, int end, int nodeIdx) {
            if (start == end) {
                segTree[nodeIdx] = nums[start];
            } else {
                int mid = start + (end-start)/2;
                int sum = buildSegTree(nums, start, mid, nodeIdx*2+1) + buildSegTree(nums, mid+1, end, nodeIdx*2+2);
                segTree[nodeIdx] = sum;
            }
            return segTree[nodeIdx];
        }


        public void update(int i, int val) {
            int diff = val - numArr[i];
            updateUtil(0, 0, n-1, i, diff);
            numArr[i] = val; // do not forget!
        }

        private void updateUtil(int nodeIdx, int ss, int se, int i, int diff) {
            if (i>=ss && i<=se) {
                segTree[nodeIdx] += diff;
                if (nodeIdx*2+2<segTree.length) {
                    int mid = ss + (se-ss)/2;
                    if (i<=mid) updateUtil(nodeIdx*2+1, ss, mid, i, diff);
                    else updateUtil(nodeIdx*2+2, mid+1, se, i, diff);
                }
            }
        }


        public int sumRange(int i, int j) {
            return sumUtil(i, j, 0, n-1, 0);
        }


        private int sumUtil(int qs, int qe, int ss, int se, int nodeIdx) {
            // node index -> node range : recursion...

            if (qs<=ss && se<=qe) {
                return segTree[nodeIdx];
            }

            if (qe<ss || se<qs) {
                return 0;
            }

            int mid = ss + (se-ss)/2;
            int sum = sumUtil(qs, qe, ss, mid, nodeIdx*2+1) + sumUtil(qs, qe, mid+1, se, nodeIdx*2+2);
            return sum;
        }

    }
}



