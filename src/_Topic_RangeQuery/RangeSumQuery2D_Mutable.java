package _Topic_RangeQuery;

/**
 *
 *  compare segtree vs binary indexed tree 时间复杂度对比: ??
 *      https://www.cnblogs.com/EdwardLiu/p/5138208.html
 *      seg tree:  build: O(m*n) ，update: O（logm * logn）or O(logmn) ?   ， query: O（logm * logn）or O(logmn)?
 *      bit tree:   build: O(m*n*logm*logn)  update:O(logm*logn)  query: O(logm*logn)
 *
 *      but why SegTree is slower than BIT in lintcode??
 *
 *  1. Segment Tree  the choice!
 *      https://stackoverflow.com/questions/25121878/2d-segment-quad-tree-explanation-with-c/25122078#25122078
 *
 *  2. Binary Indexed Tree (code is cleaner) good!!!
 *      https://www.topcoder.com/community/competitive-programming/tutorials/binary-indexed-trees/
 *
 */
public class RangeSumQuery2D_Mutable {

    // 2d segment tree!!
    static class NumMatrix {

        class STNode {
            int row1, row2, col1, col2, sum;
            STNode c1, c2, c3, c4;
        }

        STNode segTree;

        public NumMatrix(int[][] matrix) {
            segTree = buildSegTree(matrix, 0, matrix.length-1, 0, matrix[0].length-1);
        }

        private STNode buildSegTree(int[][] matrix, int row1, int row2, int col1, int col2) {
            if (row1>row2 || col1 > col2) return null;

            STNode node = new STNode();
            node.row1 = row1;
            node.col1 = col1;
            node.row2 = row2;
            node.col2 = col2;
            if (row1 == row2 && col1 == col2) {
                node.sum = matrix[row1][col1];
            } else {
                int rowMid = row1+(row2-row1)/2;
                int colMid = col1+(col2-col1)/2;
                node.c1 = buildSegTree(matrix, row1, rowMid, col1, colMid);
                node.c2 = buildSegTree(matrix, row1, rowMid, colMid+1, col2);
                node.c3 = buildSegTree(matrix,rowMid+1, row2, colMid+1, col2);
                node.c4 = buildSegTree(matrix,rowMid+1, row2, col1, colMid);
                if (node.c1 != null) node.sum+=node.c1.sum;
                if (node.c2 != null) node.sum+=node.c2.sum;
                if (node.c3 != null) node.sum+=node.c3.sum;
                if (node.c4 != null) node.sum+=node.c4.sum;
            }
            return node;
        }


        public int sumRegion(int row1, int col1, int row2, int col2) {
            return sumRegion(segTree, row1, col1, row2, col2);
        }

        private int sumRegion(STNode node, int row1, int col1, int row2, int col2) {
            if (node==null) return 0;
            if (row1<=node.row1 && node.row2<=row2 && col1<=node.col1 && node.col2<=col2) return node.sum;
            if (row1>node.row2 || row2<node.row1 || col1>node.col2 || col2<node.col1) return 0;
            return sumRegion(node.c1, row1, col1, row2, col2)
                    + sumRegion(node.c2, row1, col1, row2, col2)
                    + sumRegion(node.c3, row1, col1, row2, col2)
                    + sumRegion(node.c4, row1, col1, row2, col2);
        }


        public void update(int row, int col, int val) {
            update(segTree, row, col, val);
        }

        private void update(STNode node, int row, int col, int val) {
            if (node==null) return;
            if (node.row1 == node.row2 && node.col1 == node.col2) {
                node.sum = val;
                return;
            }

            int rowMid = node.row1+(node.row2-node.row1)/2;
            int colMid = node.col1+(node.col2-node.col1)/2;
            if (node.row1<=row && row<=rowMid && node.col1<=col && col<=colMid) update(node.c1, row, col, val);
            if (node.row1<=row && row<=rowMid && colMid+1<=col && col<=node.col2) update(node.c2, row, col, val);
            if (rowMid+1<=row && row<=node.row2 && colMid+1<=col && col<=node.col2) update(node.c3, row, col, val);
            if (rowMid+1<=row && row<=node.row2 && node.col1<=col && col<=colMid) update(node.c4, row, col, val);
            node.sum = 0;
            node.sum += node.c1 == null ? 0: node.c1.sum;
            node.sum += node.c2 == null ? 0: node.c2.sum;
            node.sum += node.c3 == null ? 0: node.c3.sum;
            node.sum += node.c4 == null ? 0: node.c4.sum;
        }
    }

    // binary indexed tree
    static class NumMatrix_v2 {

        int[][] BIT;
        int[][] originMatrix;
        int m,n;

        public NumMatrix_v2(int[][] matrix) {
            m = matrix.length;
            n = matrix[0].length;
            BIT = new int[m+1][n+1];
            originMatrix = new int[m+1][n+1];

            for (int i = 0; i < m; i++) {
                for (int j = 0; j < n; j++) {
                    update(i, j, matrix[i][j]);
                }
            }
        }

        public void update(int row, int col, int val) {
            row++; col++; // convert to 1-based index
            int diff = val - originMatrix[row][col];
            originMatrix[row][col] = val;
            while (row <= m) {
                int curCol = col;
                while (curCol <= n) {
                    BIT[row][curCol] += diff;
                    curCol += isolateLastBit(curCol);
                }
                row += isolateLastBit(row);
            }
        }

        public int sumRegion(int row1, int col1, int row2, int col2) {
            return cumulativeSum(row2, col2) - cumulativeSum(row1-1, col2) - cumulativeSum(row2, col1-1)
                    + cumulativeSum(row1-1, col1-1);
        }

        private int cumulativeSum(int row, int col) {
            row++; col++; // convert to 1-based index
            int sum = 0;
            while(row>0) {
                int curCol = col;
                while (curCol > 0) {
                    sum += BIT[row][curCol];
                    curCol -= isolateLastBit(curCol);
                }
                row -= isolateLastBit(row);
            }
            return sum;
        }

        private int isolateLastBit(int x) {
            return x & (x ^ (x-1));
        }
    }

    public static void main(String[] args) {
//        NumMatrix([[3,0,1,4,2],[5,6,3,2,1],[1,2,0,1,5],[4,1,0,1,7],[1,0,3,0,5]])
//        sumRegion(2, 1, 4, 3)
//        update(3, 2, 2)
//        sumRegion(2, 1, 4, 3)

        
//        int[][] matrix = new int[][]{
//                {1,2,3,4,5},
//                {2,3,4,5,6},
//                {3,4,5,6,7},
//                {4,5,6,7,8}
//        };
        int[][] matrix = new int[][] {
                {3,0,1,4,2},
                {5,6,3,2,1},
                {1,2,0,1,5},
                {4,1,0,1,7},
                {1,0,3,0,5}
        };

        NumMatrix nm = new NumMatrix(matrix);
        System.out.println(nm.sumRegion(2, 1,4,3));
        nm.update(3, 2, 2);
        System.out.println(nm.sumRegion(2, 1, 4,3));

//        System.out.println(nm.sumRegion(0,0,1,1));

    }
}
