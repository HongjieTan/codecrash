package _Topic_RangeQuery;

/**
 * Created by thj on 2018/10/9.
 */
public class RangeSumQuery_Immutable {

    class NumArray {

        private int[] presum;

        public NumArray(int[] nums) {
            presum = new int[nums.length];
            for (int i = 0; i < nums.length; i++) {
                presum[i] = nums[i] + i>0?presum[i-1]:0;
            }
        }

        public int sumRange(int i, int j) {
            return presum[j] - i>0?presum[i-1]:0;
        }
    }
}


