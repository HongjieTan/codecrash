package _Topic_RangeQuery;

/**
 *
 * https://www.geeksforgeeks.org/segment-tree-set-1-range-minimum-query/
 * https://practice.geeksforgeeks.org/problems/range-minimum-query/1
 *
 *
 * 1. segment tree!!!!:     query: O(logn), preprocess: O(nlogn)
 * 2. build map,(i,j)->min):query: O(1),    preprocess: O(n^2), space: O(n^2),  if updated, need to repreprocess!
 * 3. brute force:          query: O(n),    preprocess: no need
 */
public class RangeMinQuery {

    static class STNode {
        int start, end, min;
        STNode left, right;
    }

    static STNode segTree;
    public static void constructSegTree(int arr[], int n) {
        segTree = constructSegTree(arr, 0, arr.length-1);
    }
    private static STNode constructSegTree(int arr[], int start, int end) {
        if (start == end) {
            STNode node = new STNode();
            node.start = start;
            node.end = end;
            node.min = arr[start];
            return node;
        } else {
            int mid = start + (end - start)/2;
            STNode node = new STNode();
            node.start = start;
            node.end = end;
            node.left = constructSegTree(arr, start, mid);
            node.right = constructSegTree(arr, mid+1, end);
            node.min = Math.min(node.left.min, node.right.min);
            return node;
        }
    }

    public static int rangeMinQuery(int i, int j) {
        return rangeMinQuery(segTree, i, j);
    }

    private static int rangeMinQuery(STNode node, int i, int j) {
        if ( i <= node.start && node.end <= j) return node.min;
        else if (  j < node.start || i>node.end ) return Integer.MAX_VALUE;
        else return Math.min(rangeMinQuery(node.left, i, j),rangeMinQuery(node.right, i, j));
    }


    public static void update(int i, int val) {
        update(segTree, i, val);
    }

    private static void update(STNode node, int i, int val) {
        if (node.start == node.end) { // node.start == i must be true...
            node.min = val;
            return;
        }
        if (node.start <= i && i<= node.end) {
            int mid = node.start + (node.end - node.start)/2;
            if(i<=mid) update(node.left, i, val);
            else update(node.right, i, val);
            node.min = Math.min(node.left.min, node.right.min);
        }
    }



    public static void main(String[] args) {
        constructSegTree(new int[]{1,2,3,4,5,6,7}, 7);
    }

}
