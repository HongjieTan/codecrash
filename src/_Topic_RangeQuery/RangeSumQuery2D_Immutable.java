package _Topic_RangeQuery;


/**
 *  use presum (cumulative sum) ...
 */
public class RangeSumQuery2D_Immutable {

    public static void main(String[] args) {
//        int[][] matrix = new int[][]{
//                {1,2,3,4,5},
//                {2,3,4,5,6},
//                {3,4,5,6,7},
//                {4,5,6,7,8}
//        };
    }

    static class NumMatrix {

        int[][] sum;

        public NumMatrix(int[][] matrix) {
            if (matrix==null || matrix.length==0) return;

            sum = new int[matrix.length][matrix[0].length];

            for (int i = 0; i < matrix.length; i++) {
                for (int j = 0; j < matrix[0].length; j++) {
                    if (i==0 && j==0) sum[i][j] = matrix[i][j];
                    else if (i==0) sum[i][j] =  matrix[i][j] + sum[i][j-1];
                    else if (j==0) sum[i][j] =  matrix[i][j] + sum[i-1][j];
                    else {
                        sum[i][j] = matrix[i][j] + sum[i-1][j] + sum[i][j-1] - sum[i-1][j-1];
                    }
                }
            }
        }

        public int sumRegion(int row1, int col1, int row2, int col2) {

            int res = sum[row2][col2];
            if (row1 > 0 && col1>0) {
                res -= sum[row1-1][col2];
                res -= sum[row2][col1-1];
                res += sum[row1-1][col1-1];
            } else if (row1 > 0) {
                res -= sum[row1-1][col2];
            } else if (col1 > 0) {
                res -= sum[row2][col1-1];
            }
            return res;
        }
    }

}


